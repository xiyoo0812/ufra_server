@echo off

cd .\quanta

del .\temp /f /s /q

set path==%path%;C:\Program Files\Microsoft Visual Studio\2022\Community\Common7\IDE
devenv.com .\quanta.sln /Build

cd ../
mkdir bin
xcopy .\quanta\bin\*.dll  .\bin  /y /s
xcopy .\quanta\bin\*.exe  .\bin  /y /s

pause
