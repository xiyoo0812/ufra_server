

@echo off
setlocal enabledelayedexpansion

set /p CONF=please input your conf name:

rmdir /Q /S eufaula
md eufaula

cd template

set SCRIPT=../../quanta/extend/lmake/ltemplate.lua
set ENVIRON=../config/%CONF%.conf
for %%i in (*.conf) do (
    ..\lua.exe %SCRIPT% %%i ..\eufaula\%%i %ENVIRON%
)

pause

