taskkill /f /im quanta.exe

::start "accord"  quanta.exe eufaula/accord.conf  --index=1 --port=1
start "monitor" quanta.exe eufaula/monitor.conf --index=1 --port=1
start "router1" quanta.exe eufaula/router.conf  --index=1 --port=1
start "cache1"  quanta.exe eufaula/cache.conf   --index=1 --port=1
start "mongo"   quanta.exe eufaula/mongo.conf   --index=1 --port=1
start "redis"   quanta.exe eufaula/redis.conf   --index=1 --port=1
start "center"  quanta.exe eufaula/center.conf  --index=1 --port=1
start "lobby"   quanta.exe eufaula/lobby.conf   --index=1 --port=1
start "mirror"   quanta.exe eufaula/mirror.conf   --index=1 --port=1
::start "world"   quanta.exe eufaula/world.conf   --index=1 --port=1
start "report"   quanta.exe eufaula/report.conf   --index=1 --port=1
start "worldm"   quanta.exe eufaula/worldm.conf   --index=1 --port=1
start "worlda"   quanta.exe eufaula/worlda.conf   --index=1 --port=1
::start "bilog"   quanta.exe eufaula/bilog.conf   --index=1 --port=1
start "login"   quanta.exe eufaula/login.conf   --index=1 --port=1
