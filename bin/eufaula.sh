kill -9 quanta

export LD_LIBRARY_PATH=`pwd`

ulimit -c unlimited

./quanta ./eufaula/monitor.conf --index=1 --port=1&
./quanta ./eufaula/router.conf  --index=1 --port=1&
./quanta ./eufaula/router.conf  --index=2 --port=2&
./quanta ./eufaula/mongo.conf   --index=1 --port=1&
./quanta ./eufaula/redis.conf   --index=1 --port=1&
./quanta ./eufaula/cache.conf   --index=1 --port=1&
./quanta ./eufaula/center.conf  --index=1 --port=1&
./quanta ./eufaula/lobby.conf   --index=1 --port=1&
./quanta ./eufaula/report.conf   --index=1 --port=1&
./quanta ./eufaula/mirror.conf   --index=1 --port=1&
./quanta ./eufaula/worldm.conf   --index=1 --port=1&
./quanta ./eufaula/worlda.conf   --index=1 --port=1&
./quanta ./eufaula/login.conf    --index=1 --port=1&