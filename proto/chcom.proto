syntax = "proto3";
import "player.proto";

package ncmd_cs;

// 成员分组
enum chcom_member_group
{
    CMT_APPLY                           = 0;    // 申请
    CMT_FORMAL                          = 1;    // 正式
}

// 商会状态
enum chcom_status
{
    CHS_BIZZ                            = 0;    //营业
    CHS_DISS                            = 1;    //解散
}

// 主题类型
enum chcom_theme_kind 
{
    CTK_OS                              = 0;    // 占位(无实际用途)
    CTK_NONE                            = 1;    // 无规则
    CTK_CAS                             = 2;    // 生产
    CTK_FAF                             = 4;    // 亲友团
    CTK_EXP                             = 8;    // 探索
    CTK_DUN                             = 10;   // 副本战斗
}

// 身份类型
enum chcom_persona_kind
{
    CPK_MEMBER                          = 0;    // 成员
    CPK_ADMIN                           = 1;    // 管理员
    CPK_LORD                            = 2;    // 会长
}

// 事件
enum chcom_event_kind
{
    CEK_ESTD                            = 0;    // 成立(示例:[name]成立了商会)
    CEK_DISS                            = 1;    // 解散(示例:[name]解散了商会,将在 diss_time 完成解散)
    CEK_CADS                            = 2;    // 取消解散(示例:[name]取消了解散)
    CEK_JOIN                            = 3;    // 加入商会(示例:[name]加入商会)
    CEK_EXIT                            = 4;    // 离开商会(示例:[name]离开商会)
    CEK_PROMO                           = 5;    // 晋升(示例:[name]晋升为管理员)
    CEK_ABDI                            = 6;    // 转让商会(示例:[name]晋升为会长)
    CEK_KO                              = 7;    // 踢出成员(示例:[name]被踢出商会)
    CEK_DISM                            = 8;    // 免职(示例:[name]被免职)
}

//文字过滤类型
enum wfiltr_kind
{
    WFK_PASS                            = 0;    // 通过
    WFK_OCCUPY                          = 1;    // 占用
    WFK_SENTVE                          = 2;    // 敏感
}

// 离开方式
enum chcom_leave_mode
{
    CLM_ALONE                           = 0;    // 主动
    CLM_KICK                            = 1;    // 踢出
    CLM_DISS                            = 2;    // 解散
}

// 商会简要信息
message chcom_base_info
{
    uint32  id                          = 1;    // id
    uint32  icon                        = 2;    // 图标
    string  name                        = 3;    // 名称
    string  code                        = 4;    // 代号
    uint64  theme                       = 5;    // 主题
    uint32  level                       = 6;    // 等级
    uint64  prosper                     = 7;    // 繁荣度
    uint32  count                       = 8;    // 成员数量
    bool    audit                       = 9;    // 审核
    string  language                    = 10;    // 语言
    uint64  owner_id                    = 11;   // 会长标识
    string  owner_name                  = 12;   // 会长名称
    string  remarks                     = 13;   // 描述
    uint64  create_time                 = 14;   // 创建时间
}

// 成员信息
message chcom_member
{
    player_base_info    base_info       = 1;    // 基础信息
    online_status       os_status       = 2;    // 在线状态
    chcom_persona_kind  persona         = 3;    // 职位
    chcom_member_group  group           = 4;    // 分组
    uint64              dau             = 5;    // 日活跃
    uint64              wau             = 6;    // 周活跃
    uint64              time            = 7;    // 时间
    uint64              aly_time        = 8;    // 申请时间
}

// 商会事件
message chcom_event
{
    uint64              id              = 1;    // 记录id
    chcom_event_kind    kind            = 2;    // 种类
    repeated string     fields          = 3;    // 文本映射
    uint64              time            = 4;    // 时间
}

// 搜索商会
// NID_CHCOM_SEARCH_REQ
message chcom_search_req
{
    string  name                        = 1;    // 名称或代号
}

// NID_CHCOM_SEARCH_RES
message chcom_search_res
{
    uint32      error_code              = 1;
    repeated    chcom_base_info list    = 2;    // 列表
}

// 推荐列表
// NID_CHCOM_RECLIST_REQ
message chcom_reclist_req
{
}

// NID_CHCOM_RECLIST_RES
message chcom_reclist_res
{
    uint32  error_code                  = 1;
    repeated    chcom_base_info list    = 2;    // 列表
}

// 文字过滤结果
message wfiltr_result
{
    string val                          = 1;    // 值
    wfiltr_kind type                    = 2;    // 结果类型(0 通过 1 占用 2 涉敏)
}

// 违规词检索
// NID_CHCOM_WFILTR_REQ
message chcom_wfiltr_req
{
   map<string,string>   words           = 1;    // 文字列表
}

// NID_CHCOM_WFILTR_RES
message chcom_wfiltr_res
{
    uint32  error_code                      = 1;
    map<string,wfiltr_result> wfiltr        = 2;    // 文字过滤结果
}

// 创建
// NID_CHCOM_CREATE_REQ
message chcom_create_req
{
    string  name                        = 1;    // 名称
    string  code                        = 2;    // 代号
    bool    audit                       = 3;    // 审核(false 自由加入 true 审批加入)
    uint32  icon                        = 4;    // 图标
    uint64  theme                       = 5;    // 主题
    string  language                    = 6;    // 语言
    string  remarks                     = 7;    // 说明
}

// NID_CHCOM_CREATE_RES
message chcom_create_res
{
    uint32  error_code                  = 1;
    map<string,wfiltr_result>  wfiltr   = 2;
}

//属性定义
message chcom_attr
{
    string key                          = 1;       // 属性名称
    oneof value {
        string attr_s                   = 2;       // 字符串属性
        int64 attr_i                    = 3;       // 整数属性
        bool attr_b                     = 4;       // bool属性
    }
}

// 编辑属性
// NID_CHCOM_EDATTRS_REQ
message chcom_edattrs_req
{
    repeated chcom_attr attrs           = 1;       // 属性列表
}

message chcom_edattrs_res
{
    uint32  error_code                  = 1;
    map<string,string>  savrs           = 2;
}

// 商会属性同步
// NID_CHCOM_ATTRS_NTF
message chcom_attrs_ntf
{
    repeated chcom_attr attrs           = 1;       // 属性列表
}

// 商会成员属性同步
// NID_CHCOM_MBRATTRS_NTF
message chcom_mbrattrs_ntf
{
    uint32   id                         = 1;       // 成员id
    repeated chcom_attr attrs           = 2;       // 属性列表
}

// 申请记录同步
// NID_CHCOM_APPLYREC_NTF
message chcom_applyrec_ntf
{
    repeated uint64 ids                 = 1;       // 商会id列表
}

// 邀请加入
// NID_CHCOM_INVITE_REQ
message chcom_invite_req
{
    string  name                        = 1;    //昵称或ID
}

// NID_CHCOM_INVITE_RES
message chcom_invite_res
{
    uint32  error_code                  = 1;
}

// 邀请回复
// NID_CHCOM_INVRPL_REQ
message chcom_invrpl_req
{
    uint64  id                          = 1;    // 商会标识
    bool    reply                       = 2;    // reply(false 拒绝 true 同意)
}

// NID_CHCOM_INVRPL_RES
message chcom_invrpl_res
{
    uint32  error_code                  = 1;
}

// 加入申请
// NID_CHCOM_JOINRQ_REQ
message chcom_joinrq_req
{
    uint64  id                          = 1;    //商会标识
}

// NID_CHCOM_JOINRQ_RES
message chcom_joinrq_res
{
    uint32  error_code                  = 1;
}

// 加入审核
// NID_CHCOM_JOINAD_REQ
message chcom_joinad_req
{
    uint64  id                          = 1;
    bool    reply                       = 2;  // reply(false 拒绝 true 同意)
}

// NID_CHCOM_JOINAD_RES
message chcom_joinad_res
{
    uint32 error_code                   = 1;
}

// 设置权限
// NID_CHCOM_PERSONA_REQ
message chcom_persona_req
{
    uint64  target_id                   = 1;
    chcom_persona_kind  persona         = 2;
}

// NID_CHCOM_PERSONA_RES
message chcom_persona_res
{
    uint32 error_code                   = 1;
}

// 踢出成员
// NID_CHCOM_KOMBR_REQ
message chcom_kombr_req
{
    uint64  target_id                   = 1;
}

// NID_CHCOM_KOMBR_RES
message chcom_kombr_res
{
    uint32 error_code                   = 1;
}

// 离开商会
// NID_CHCOM_EXIT_REQ
message chcom_exit_req
{
}

// NID_CHCOM_EXIT_RES
message chcom_exit_res
{
    uint32 error_code                   = 1;
}

// 解散商会
// NID_CHCOM_DISS_REQ
message chcom_diss_req
{
}

// NID_CHCOM_DISS_RES
message chcom_diss_res
{
    uint32 error_code                   = 1;
}

// 取消解散
// NID_CHCOM_CANDS_REQ
message chcom_cands_req
{
}

// NID_CHCOM_CANDS_RES
message chcom_cands_res
{
    uint32 error_code                   = 1;
}

// 商会信息下发
// NID_CHCOM_INFO_NTF
message chcom_info_ntf
{
    uint32  id                          = 1;    // id
    uint32  icon                        = 2;    // 图标
    string  name                        = 3;    // 名称
    string  code                        = 4;    // 代号
    bool    audit                       = 5;    // 审核
    uint32  level                       = 6;    // 等级
    uint32  count                       = 7;    // 人数
    uint64  theme                       = 8;    // 主题
    string  language                    = 9;    // 语言
    string  remarks                     = 10;   // 描述
    string  notices                     = 11;   // 公告
    uint64  prosper                     = 12;   // 繁荣度
    uint64  owner_id                    = 13;   // 所有者
    string  owner_name                  = 14;   // 会长名称
    uint32  status                      = 15;   // 商会状态
    uint64  create_time                 = 16;   // 创建时间
    uint64  diss_time                   = 17;   // 解散时间
}

// 邀请下发
// NID_CHCOM_INVITE_NTF
message chcom_invite_ntf
{
    uint64  id                          = 1;  // 商会id
    string  name                        = 2;  // 商会名称
    uint64  invite_id                   = 3;  // 邀请人id
    string  invite_name                 = 4;  // 邀请人昵称
    uint64  time                        = 5;  // 邀请时间
}

// 邀请回复下发
// NID_CHCOM_INVRPL_NTF
message chcom_invrpl_ntf
{
    uint64  id                          = 1;  // 商会id
    string  name                        = 2;  // 商会名称
    uint64  player_id                   = 3;  // 用户id
    string  player_name                 = 4;  // 用户昵称
    bool    reply                       = 5;  // reply(false 拒绝 true 同意)
    uint64  time                        = 6;  // 回复时间
}

// 审核结果下发
// NID_CHCOM_AUDIT_NTF
message chcom_audit_ntf
{
    uint64  id                          = 1;
    string  name                        = 2;
    uint64  player_id                   = 3;
    string  palyer_name                 = 4;
    bool    reply                       = 5;  // reply(false 拒绝 true 同意)
}

// 成员列表下发
// NID_CHCOM_MEMBER_NTF
message chcom_member_ntf
{
    repeated chcom_member list          = 1;  // 成员信息
}

// 删除成员下发
// NID_CHCOM_DELMBR_NTF
message chcom_delmbr_ntf
{
    repeated uint64 ids                 = 1;  // id列表
}

// 商会事记下发
// NID_CHCOM_EVENT_NTF
message chcom_event_ntf
{
    repeated chcom_event list         = 1;    //商会事件
}

// 离开商会下发
// NID_CHCOM_LEAVE_NTF
message chcom_leave_ntf
{
    uint64  id                          = 1;    // 商会id
    string  name                        = 2;    // 商会名称
    uint64  time                        = 3;    //解散时间
    uint32  leave_mode                  = 4;    //离开方式
}

// 商会解散下发
// NID_CHCOM_DISS_NTF
message chcom_diss_ntf
{
    uint64  id                          = 1;    // 商会id
    string  name                        = 2;    // 商会名称
    uint64  time                        = 3;    // 解散时间
}