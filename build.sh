@echo off

cd quanta

make all

cp -fv ./bin/*.so ../bin/
cp -fv ./bin/lua ../bin/
cp -fv ./bin/luac ../bin/
cp -fv ./bin/quanta ../bin/
