--mail_mgr.lua
import("center/mail/mail_dao.lua")

local log_err                   = logger.err
local log_debug                 = logger.debug
local tarray                    = qtable.array

local monitor                   = quanta.get("monitor")
local mail_dao                  = quanta.get("mail_dao")
local timer_mgr                 = quanta.get("timer_mgr")
local router_mgr                = quanta.get("router_mgr")
local update_mgr                = quanta.get("update_mgr")

local DAY_S                     = quanta.enum("PeriodTime", "DAY_S")
local SECOND_10_MS              = quanta.enum("PeriodTime", "SECOND_10_MS")

local MailMgr     = singleton()
local prop        = property(MailMgr)
prop:reader("mails", {})
--全局邮件版本号
prop:reader("mail_version", 0)
--是否已加载全局邮件
prop:reader("load_mail_status", false)

function MailMgr:__init()
    -- 关注 mongo服务 事件
    monitor:watch_service_ready(self, "mongo")
end

-- mongo服务已经ready
function MailMgr:on_service_ready(id, service_name)
    if self.load_mail_status then
        return
    end
    log_debug("[MailMgr][on_service_ready]->id:{}, service_name:{}", id, service_name)
    --加载全局邮件
    timer_mgr:once(SECOND_10_MS, function()
        self:load_mails()
        --分钟定时器 检查全局邮件过期
        update_mgr:attach_minute(self)
    end)
end

--保存全局邮件
function MailMgr:add_mail(mail)
    local ok, data =  mail_dao:add_mail(mail)
    if not ok then
        log_err("[MailMgr][add_mail] add failed! ok:{}", ok)
        return false
    end
    self.mail_version = self.mail_version + 1
    self.mails[mail.guid] = mail
    return ok, data
end

--删除全局邮件
function MailMgr:del_mail(guid)
    local ok, code = mail_dao:del_mail(guid)
    if not ok then
        log_err("[MailMgr][del_mail] del failed! ok:{} code:{}", ok, code)
        return false
    end
    self.mails[guid] = nil
    return ok, code
end

--获取全局邮件
function MailMgr:get_mails(version)
    if version == self.mail_version then
        return self.mail_version
    end
    return self.mail_version, tarray(self.mails)
end

--加载全局邮件
function MailMgr:load_mails()
    local ok, data = mail_dao:get_mails()
    if not ok then
        log_err("[MailMgr][load_mails] load global mail failed! ok:{}", ok)
        return
    end
    for _, mail in pairs(data) do
        self.mails[mail.guid] = mail
    end
    self.mail_version = self.mail_version + 1
    --广播给大厅
    self:broadcast_lobby("rpc_reload_global_mails")
    self.load_mail_status = true
end

--邮件附件json与表互转
function MailMgr:json_2attach(json_attach)
    log_debug("[MailMgr][json_2attach] json_attach:{}", json_attach)
    local json_attach_array = json_attach
    local attaches = {}
    for _, item in ipairs(json_attach_array or {}) do
        attaches[tonumber(item.itemId)] = item.itemCount
    end
    return attaches
end

--广播rpc到大厅
function MailMgr:broadcast_lobby(rpc, ...)
    local ok, codeoe = router_mgr:call_lobby_all( rpc , ...)
    if not ok then
        log_err("[MailMgr][broadcast_lobby] rpc_command_execute failed! rpc={}", rpc)
        return { code = 1, msg = codeoe }
    end
    return { code = codeoe, msg = "success" }
end

----------------------内部---------------------------------
--分钟定时器
function MailMgr:on_minute()
    --检查全局邮件失效
    local now = quanta.now
    for _, gmail in pairs(self.mails) do
        if gmail.delete_time and gmail.delete_time < quanta.now then
            self:del_mail(gmail.guid)
            goto continue
        end
        --失效邮件先标记 14天后删除
        if gmail.expire_mail_time < now and not gmail.delete_time then
            gmail.delete_time = quanta.now + 14 * DAY_S
            mail_dao:update_mail(gmail)
            --广播给大厅
            self:broadcast_lobby("rpc_del_global_mail", gmail.guid, false)
        end
        ::continue::
    end

end

-- export
quanta.mail_mgr = MailMgr()

return MailMgr