--mail_gm.lua
local u8len                     = utf8.len
local tunpack                   = table.unpack
local sformat                   = string.format
local tsize                     = qtable.size
local log_err                   = logger.err
local log_debug                 = logger.debug
local guid_new                  = codec.guid_new

local gm_mgr                    = quanta.get("gm_mgr")
local mail_mgr                  = quanta.get("mail_mgr")
local event_mgr                 = quanta.get("event_mgr")
local config_mgr                = quanta.get("config_mgr")
local protobuf_mgr              = quanta.get("protobuf_mgr")

local mail_db                   = config_mgr:init_table("mail", "id")
local utility_db                = config_mgr:init_table("utility", "key")

local ITEM_LIMIT                = utility_db:find_integer("value", "mail_item_limit")

local LOCAL                     = quanta.enum("GMType", "LOCAL")
local MAIL_TYPE_SYS             = protobuf_mgr:enum("mail_type", "MAIL_TYPE_SYS")
local FRAME_SUCCESS             = protobuf_mgr:error_code("FRAME_SUCCESS")
local FRAME_FAILED              = protobuf_mgr:error_code("FRAME_FAILED")

local MailGM    = singleton()
function MailGM:__init()
    --注册GM
    self:register()
end

--GM服务器
function MailGM:register()
    local cmd_list = {
        {
            name = "gm_send_global_mail",
            gm_type = LOCAL,
            group = "邮件",
            desc = "发送全局邮件",
            args = "guid|string from_player_name|string title|string content|string expire|integer json_attach|table",
            example= "",
            tip = ""
        },
        {
            name = "gm_del_global_mail",
            gm_type = LOCAL,
            group = "邮件",
            desc = "回收全局邮件",
            args = "guid|string",
            example= "",
            tip = ""
        },
        {
            name = "gm_send_global_config_mail",
            gm_type = LOCAL,
            group = "邮件",
            desc = "发送全局模板邮件",
            args = "proto_id|integer",
            example= "",
            tip = ""
        },
    }

    gm_mgr:rpc_register_command(cmd_list)
    for _, cmd in ipairs(cmd_list) do
        event_mgr:add_listener(self, cmd.name)
    end
end

-- 删除全局邮件
function MailGM:gm_del_global_mail(guid)
    log_debug("[MailGM][gm_del_global_mail] guid:{}", guid)
    --撤销操作
    local result = event_mgr:notify_listener("on_del_global_mail", guid)
    local call_ok, status_ok = tunpack(result)
    if not call_ok or not status_ok then
        return FRAME_FAILED, "fail"
    end
    --分发到大厅
    mail_mgr:broadcast_lobby("rpc_del_global_mail", guid, true)
    return FRAME_SUCCESS, "success"
end

--发送全局配置邮件
function MailGM:gm_send_global_config_mail(mail_id)
    local mail_conf = mail_db:find_one(mail_id)
    if mail_conf == nil then
        log_err("[MailGM][gm_send_global_config_mail] mail_id:{} not find config", mail_id)
        return
    end
    local expire = 0
    if mail_conf.expire ~= 0 then
        expire = quanta.now + mail_conf.expire
    end
    local guid = guid_new()
    self:gm_send_global_mail(guid,mail_id, mail_conf.source, mail_conf.title, mail_conf.content, expire, mail_conf.attaches)
    return FRAME_SUCCESS, "success"
end

-- 发送全局邮件
function MailGM:gm_send_global_mail(guid, mail_id, from_player_name, title, content, expire, json_attach)
    log_debug("[MailGM][gm_send_mail] ({}, {}, {}, {}, {})", from_player_name, title, content, expire, json_attach)
    --参数检查
    if not self:check_title_content( title, content) then
        return FRAME_FAILED, "fail, title or content out of limit"
    end
    local attaches = nil
    if tsize(json_attach) > 0 then
        attaches = mail_mgr:json_2attach(json_attach)
        --附件个数
        if tsize(attaches) > ITEM_LIMIT then
            return FRAME_FAILED, sformat("parse failed. item_list size > %s", ITEM_LIMIT)
        end
    end

    if expire ~= 0 then
        expire = quanta.now + expire
    end

    local rpc_req          = {
        type                = MAIL_TYPE_SYS,
        from                = from_player_name,
        create_mail_time    = quanta.now_ms,
        title               = title,
        content             = content,
        expire_mail_time    = expire,
        attaches            = attaches,
        guid                = guid or guid_new(),
        proto_id            = mail_id or 0
    }
    --入库操作
    local result = event_mgr:notify_listener("on_save_global_mail", rpc_req)
    local call_ok, status_ok = tunpack(result)
    if not call_ok or not status_ok then
        return FRAME_FAILED, "fail"
    end
    --分发到大厅
    mail_mgr:broadcast_lobby("rpc_add_global_mail", rpc_req)
    return FRAME_SUCCESS, "success"
end

--检查标题和正文合法性
function MailGM:check_title_content(title, content)
    if not title or not content then
        return false
    end
    --标题不超过50字符 内容不超过1000
    if u8len(title) > 50 or u8len(content) > 1000 then
        return false
    end
    return true
end

-- 导出
quanta.mail_gm = MailGM()

return MailGM
