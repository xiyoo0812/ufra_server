--mail_dao.lua
import("agent/mongo_agent.lua")

local log_err           = logger.err
local qfailed           = quanta.failed

local mongo_agent       = quanta.get("mongo_agent")

local MailDao = singleton()
function MailDao:__init()
end

-- 新增全局邮件
function MailDao:add_mail(mail)
    local ok, rcode, data = mongo_agent:insert({ "global_mails", mail},nil)
    if not ok or qfailed(rcode) then
        log_err("[MailDao][add_mail] add_mail failed! guid:{}, code: {}, res: {}", mail.guid,  rcode, rcode)
        return false, rcode
    end
    return ok, data
end

-- 查询全局邮件
function MailDao:get_mails()
    local ok, code, data = mongo_agent:find({ "global_mails", { } },nil)
    if not ok or qfailed(code) then
        return false, code
    end
    return ok, data
end

-- 删除全局邮件
function MailDao:del_mail(guid)
    local ok, code, res = mongo_agent:delete({ "global_mails", { guid = guid }},nil)
    if not ok or qfailed(code) then
        log_err("[MailDao][del_mail] guid:{} delete failed! code: {}, res: {}", guid, code, res)
        return false,code
    end
    return true,code
end

-- 更新全局邮件
function MailDao:update_mail(gmail)
    local udata = { ["$set"] = gmail }
    local ok, code, res = mongo_agent:update({ "global_mails", udata, { guid = gmail.guid } })
    if qfailed(code, ok) then
        log_err("[MailDao][update_mail] guid:{} update failed! code: {}, res: {}", gmail.guid, code, res)
        return false
    end
    return true
end

quanta.mail_dao = MailDao()

return MailDao
