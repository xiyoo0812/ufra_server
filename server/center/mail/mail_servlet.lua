-- mail_servlet.lua
import("center/mail/mail_mgr.lua")
import("center/mail/mail_gm.lua")

local log_err               = logger.err
local log_debug             = logger.debug

local event_mgr             = quanta.get("event_mgr")
local mail_mgr              = quanta.get("mail_mgr")

local MailServlet = singleton()
function MailServlet:__init()
    --事件监听
    event_mgr:add_listener(self, "on_save_global_mail")
    event_mgr:add_listener(self, "on_del_global_mail")
    --rpc监听
    event_mgr:add_listener(self, "rpc_get_global_mail")
end

--查询全局邮件
function MailServlet:rpc_get_global_mail()
    return mail_mgr:get_mails()
end

--保存全局邮件
function MailServlet:on_save_global_mail(mail)
    log_debug("[MailServlet][on_save_global_mail]  mail:{}", mail)
    local ok = mail_mgr:add_mail(mail)
    if not ok then
        log_err("[MailServlet][on_save_global_mail] add global mail failed! guid:{}", mail[2])
        return false
    end
    return true
end

--删除全服邮件
function MailServlet:on_del_global_mail(guid)
    log_debug("[MailServlet][on_del_global_mail]  guid:{}", guid)
    local ok = mail_mgr:del_mail(guid)
    if not ok then
        return false
    end
    return true
end

quanta.mail_servlet = MailServlet()

return MailServlet
