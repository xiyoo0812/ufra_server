-- bgip_role.lua
local log_debug        = logger.debug

local sformat          = string.format
local slower           = string.lower

local bgip_mgr         = quanta.get("bgip_mgr")
local event_mgr        = quanta.get("event_mgr")
local protobuf_mgr     = quanta.get("protobuf_mgr")
local activity_gm      = quanta.get("activity_gm")

local FRAME_SUCCESS    = protobuf_mgr:error_code("FRAME_SUCCESS")

local BgipActivity     = singleton()
function BgipActivity:__init()
    --活动列表
    event_mgr:add_listener(self, "on_bgip_activity_list")
    --开启活动
    event_mgr:add_listener(self, "on_bgip_activity_open")
    --关闭活动
    event_mgr:add_listener(self, "on_bgip_activity_close")
    --设置时间
    event_mgr:add_listener(self, "on_bgip_activity_time")
    --重置配置
    event_mgr:add_listener(self, "on_bgip_activity_reset")

    --问卷记录
    event_mgr:add_listener(self, "on_bgip_survey_search")
    event_mgr:add_listener(self, "on_bgip_survey_list")
    --cdkey领取记录
    event_mgr:add_listener(self, "on_bgip_cdkey_search")
    event_mgr:add_listener(self, "on_bgip_cdkey_list")

    --cdkey信息
    event_mgr:add_listener(self, "on_bgip_cdkey_info")

    bgip_mgr:add_idempot("on_bgip_activity_list")
    bgip_mgr:add_idempot("on_bgip_activity_close")
    bgip_mgr:add_idempot("on_bgip_activity_time")
    bgip_mgr:add_idempot("on_bgip_activity_reset")
    bgip_mgr:add_idempot("on_bgip_survey_search")
    bgip_mgr:add_idempot("on_bgip_survey_list")
    bgip_mgr:add_idempot("on_bgip_cdkey_search")
    bgip_mgr:add_idempot("on_bgip_cdkey_list")
    bgip_mgr:add_idempot("on_bgip_cdkey_info")
end

-------------------------------------------------------------------
--活动列表
function BgipActivity:on_bgip_activity_list(bgip_common, bgip_req)
    log_debug("[BgipActivity][on_bgip_activity_list] bgip_common:{} bgip_req:{}", bgip_common, bgip_req)
    local rpc_command = sformat("rpc_bgip_%s", slower(bgip_common.command))
    local exec_ok, msg
    exec_ok, msg = bgip_mgr:exec_master_lobby_cmd(rpc_command)
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    return FRAME_SUCCESS, msg.list or {}
end

--开启活动
function BgipActivity:on_bgip_activity_open(bgip_common, bgip_req)
    log_debug("[BgipActivity][on_bgip_activity_open] bgip_common:{} bgip_req:{}", bgip_common, bgip_req)
    local id          = tonumber(bgip_req.id)
    local rpc_command = sformat("rpc_bgip_%s", slower(bgip_common.command))
    local exec_ok, msg
    exec_ok, msg      = bgip_mgr:exec_lobby_broadcast(rpc_command, id)
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    return FRAME_SUCCESS, msg
end

--关闭活动
function BgipActivity:on_bgip_activity_close(bgip_common, bgip_req)
    log_debug("[BgipActivity][on_bgip_activity_close] bgip_common:{} bgip_req:{}", bgip_common, bgip_req)
    local id          = tonumber(bgip_req.id)
    local rpc_command = sformat("rpc_bgip_%s", slower(bgip_common.command))
    local exec_ok, msg
    exec_ok, msg      = bgip_mgr:exec_lobby_broadcast(rpc_command, id)
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    return FRAME_SUCCESS, msg
end

--设置时间
function BgipActivity:on_bgip_activity_time(bgip_common, bgip_req)
    log_debug("[BgipActivity][on_bgip_activity_time] bgip_common:{} bgip_req:{}", bgip_common, bgip_req)
    local id          = tonumber(bgip_req.id)
    local start_time  = bgip_req.start_time
    local end_time    = bgip_req.end_time
    local rpc_command = sformat("rpc_bgip_%s", slower(bgip_common.command))
    local exec_ok, msg
    exec_ok, msg      = bgip_mgr:exec_lobby_broadcast(rpc_command, id, start_time, end_time)
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    return FRAME_SUCCESS, msg
end

--问卷调查搜索
function BgipActivity:on_bgip_survey_search(bgip_common, bgip_req)
    log_debug("[BgipActivity][on_bgip_survey_search] bgip_common:{} bgip_req:{}", bgip_common, bgip_req)
    local id = tonumber(bgip_req.id)
    local player_id = tonumber(bgip_req.player_id)
    local size = tonumber(bgip_req.size)
    local page = tonumber(bgip_req.page)
    local ok, data = activity_gm:survey_search(id, player_id, page, size)
    if not ok then
        return FRAME_SUCCESS, "fail"
    end
    return FRAME_SUCCESS, data
end

--问卷调查列表
function BgipActivity:on_bgip_survey_list(bgip_common, bgip_req)
    log_debug("[BgipActivity][on_bgip_survey_list] bgip_common:{} bgip_req:{}", bgip_common, bgip_req)
    local id = tonumber(bgip_req.id)
    local size = tonumber(bgip_req.size)
    local page = tonumber(bgip_req.page)
    local ok, data = activity_gm:survey_list(id, page, size)
    if not ok then
        return FRAME_SUCCESS, "fail"
    end
    return FRAME_SUCCESS, data
end

--cdkey搜索
function BgipActivity:on_bgip_cdkey_search(bgip_common, bgip_req)
    log_debug("[BgipActivity][on_bgip_cdkey_search] bgip_common:{} bgip_req:{}", bgip_common, bgip_req)
    local id = tonumber(bgip_req.id)
    local name = bgip_req.name
    local size = tonumber(bgip_req.size)
    local page = tonumber(bgip_req.page)
    local ok, data = activity_gm:cdkey_search(id, name, page, size)
    if not ok then
        return FRAME_SUCCESS, "fail"
    end
    return FRAME_SUCCESS, data
end

--cdkey列表
function BgipActivity:on_bgip_cdkey_list(bgip_common, bgip_req)
    log_debug("[BgipActivity][on_bgip_cdkey_list] bgip_common:{} bgip_req:{}", bgip_common, bgip_req)
    local id = tonumber(bgip_req.id)
    local size = tonumber(bgip_req.size)
    local page = tonumber(bgip_req.page)
    local start_time = bgip_req.start_time
    local end_time = bgip_req.end_time
    local ok, data = activity_gm:cdkey_list(id, start_time, end_time, page, size)
    if not ok then
        return FRAME_SUCCESS, "fail"
    end
    return FRAME_SUCCESS, data
end

--cdkey信息
function BgipActivity:on_bgip_cdkey_info(bgip_common, bgip_req)
    log_debug("[BgipActivity][on_bgip_cdkey_info] bgip_common:{} bgip_req:{}", bgip_common, bgip_req)
    local id = tonumber(bgip_req.id)
    local ok, data = activity_gm:cdkey_info(id)
    if not ok then
        return FRAME_SUCCESS, "fail"
    end
    return FRAME_SUCCESS, data
end

--重置配置
function BgipActivity:on_bgip_activity_reset(bgip_common, bgip_req)
    log_debug("[BgipActivity][on_bgip_activity_reset] bgip_common:{} bgip_req:{}", bgip_common, bgip_req)
    local id          = tonumber(bgip_req.id)
    local rpc_command = sformat("rpc_bgip_%s", slower(bgip_common.command))
    local exec_ok, msg
    exec_ok, msg      = bgip_mgr:exec_lobby_broadcast(rpc_command, id)
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    return FRAME_SUCCESS, msg
end
quanta.bgip_activity = BgipActivity()

return BgipActivity
