-- bgip_role.lua
local log_debug         = logger.debug

local sformat           = string.format
local slower            = string.lower
local jdecode           = json.decode
local json_encode       = json.encode
local tinsert           = table.insert
local mtointeger        = math.tointeger

local bgip_mgr          = quanta.get("bgip_mgr")
local event_mgr         = quanta.get("event_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")
local config_mgr        = quanta.get("config_mgr")
local cache_db          = config_mgr:init_table("cache", "sheet")

local FRAME_SUCCESS     = protobuf_mgr:error_code("FRAME_SUCCESS")
local PARAMS_ERR        = protobuf_mgr:error_code("BGIP_PARAMS_ERR")

local BgipRole = singleton()
function BgipRole:__init()
    --修改角色名称
    event_mgr:add_listener(self, "on_bgip_updaterolename")
    --处罚角色
    event_mgr:add_listener(self, "on_bgip_punishrole")
    --踢全服玩家下线
    event_mgr:add_listener(self, "on_bgip_kickall")
    --完成任务
    event_mgr:add_listener(self, "on_bgip_finishtask")
    --接取任务
    event_mgr:add_listener(self, "on_bgip_accepttask")
    --客服红点
    event_mgr:add_listener(self, "on_bgip_pushreddot")
    --信息查询
    event_mgr:add_listener(self, "on_bgip_playerspecificinfo")
    --查询缓存
    event_mgr:add_listener(self, "on_bgip_querycache")
    --删除缓存
    event_mgr:add_listener(self, "on_bgip_delcache")
    --更新缓存
    event_mgr:add_listener(self, "on_bgip_updatecache")
    --更新sheet缓存
    event_mgr:add_listener(self, "on_bgip_updatesheetcache")
    --移除缓存
    event_mgr:add_listener(self, "on_bgip_removecache")
    --复制单表缓存
    event_mgr:add_listener(self, "on_bgip_copycache")
    --复制玩家数据
    event_mgr:add_listener(self, "on_bgip_onekeycopy")
    --查询sheet表
    event_mgr:add_listener(self, "on_bgip_querysheet")
    --删除角色
    event_mgr:add_listener(self, "on_bgip_signed")
    --踢角色下线
    event_mgr:add_listener(self, "on_bgip_kickout")
    -- 查询账号信息
    event_mgr:add_listener(self, "on_bgip_accountinfo")
    --添加幂等特性
    bgip_mgr:add_idempot("on_bgip_updaterolename")
    bgip_mgr:add_idempot("on_bgip_punishrole")
    bgip_mgr:add_idempot("on_bgip_finishtask")
    bgip_mgr:add_idempot("on_bgip_accepttask")
    bgip_mgr:add_idempot("on_bgip_pushreddot")
    bgip_mgr:add_idempot("on_bgip_kickall")
    bgip_mgr:add_idempot("on_bgip_delcache")
    bgip_mgr:add_idempot("on_bgip_updatecache")
    bgip_mgr:add_idempot("on_bgip_removecache")
    bgip_mgr:add_idempot("on_bgip_copycache")
    bgip_mgr:add_idempot("on_bgip_onekeycopy")
    bgip_mgr:add_idempot("on_bgip_signed")
    bgip_mgr:add_idempot("on_bgip_kickout")
    bgip_mgr:add_idempot("on_bgip_accountinfo")
end

-------------------------------------------------------------------

--踢角色下线
function BgipRole:on_bgip_kickout(bgip_common, bgip_req)
    local player_id = tonumber(bgip_req.role_id)
    return self:kick_out(player_id)
end

--删除玩家角色
function BgipRole:on_bgip_signed(bgip_common, bgip_req)
    local player_id = tonumber(bgip_req.role_id)
    --玩家踢下线
    local exec_ok, msg = self:kick_out(player_id)
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end

    --注销
    local rpc_command = sformat("rpc_bgip_%s", slower(bgip_common.command))
    exec_ok, msg  = bgip_mgr:exec_cache_cmd(rpc_command, player_id)
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    return FRAME_SUCCESS, msg
end

--复制玩家数据
function BgipRole:on_bgip_onekeycopy(bgip_common, bgip_req)
    local src_role_id = tonumber(bgip_req.src_role_id)
    local dst_role_id = tonumber(bgip_req.dst_role_id)

    --玩家踢下线
    local exec_ok, msg = self:kick_out(dst_role_id)
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    --复制
    local rpc_command   = sformat("rpc_bgip_%s", slower(bgip_common.command))
    exec_ok, msg  = bgip_mgr:exec_cache_cmd(rpc_command, dst_role_id, src_role_id)
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    return FRAME_SUCCESS, msg
end

--复制玩家单表数据
function BgipRole:on_bgip_copycache(bgip_common, bgip_req)
    local src_role_id = tonumber(bgip_req.src_role_id)
    local dst_role_id = tonumber(bgip_req.dst_role_id)

    --玩家踢下线
    local exec_ok, msg = self:kick_out(dst_role_id)
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end

    local sheet = bgip_req.sheet
    local rpc_command   = sformat("rpc_bgip_%s", slower(bgip_common.command))
    exec_ok, msg  = bgip_mgr:exec_cache_cmd(rpc_command, dst_role_id, src_role_id, sheet)
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    return FRAME_SUCCESS, msg
end

--删除缓存
function BgipRole:on_bgip_delcache(bgip_common, bgip_req)
    local primary_id = tonumber(bgip_req.primary_id)
    local sheet = bgip_req.sheet
    return self:bgip_cache(bgip_common, primary_id, sheet)
end

--更新缓存
function BgipRole:on_bgip_updatecache(bgip_common, bgip_req)
    --已和sheetupdate整合
    return self:on_bgip_updatesheetcache(bgip_common, bgip_req)
end

--查询所有sheet表名
function BgipRole:on_bgip_querysheet(bgip_common, bgip_req)
    local cache_sheets = {}
    for _, conf in cache_db:iterator() do
        tinsert(cache_sheets, conf.sheet)
    end
    return FRAME_SUCCESS, {datas = json_encode(cache_sheets)}
end

--更新sheet缓存
function BgipRole:on_bgip_updatesheetcache(bgip_common, bgip_req)
    local data = bgip_req.data
    local sheet = bgip_req.sheet
    local primary_id = mtointeger(bgip_req.primary_id) or bgip_req.primary_id
    log_debug("[BgipRole][on_bgip_updatesheetcache] primary_id:{} sheet:{} data:{}",primary_id, sheet, data)

    local ok, datas = pcall(jdecode, data, true)
    if not ok or not datas then
        return PARAMS_ERR, {msg = sformat("parse failed. data:%s", data)}
    end
    log_debug("[BgipRole][on_bgip_updatesheetcache] primary_id:{} sheet:{} data:{}",primary_id, sheet, datas)
    return self:bgip_cache(bgip_common, primary_id, sheet, datas)
end


--移除缓存
function BgipRole:on_bgip_removecache(bgip_common, bgip_req)
    local sheet = bgip_req.sheet
    local field = bgip_req.field
    local primary_id = mtointeger(bgip_req.primary_id) or bgip_req.primary_id
    return self:bgip_cache(bgip_common, primary_id, sheet, field)
end

--查询缓存
function BgipRole:on_bgip_querycache(bgip_common, bgip_req)
    local sheet = bgip_req.sheet
    local primary_id = mtointeger(bgip_req.primary_id) or bgip_req.primary_id
    return self:bgip_cache(bgip_common, primary_id, sheet)
end

--缓存操作
function BgipRole:bgip_cache(bgip_common, primary_id, sheet_name, ...)
    local rpc_command   = sformat("rpc_bgip_%s", slower(bgip_common.command))
    local exec_ok, msg  = bgip_mgr:exec_cache_cmd(rpc_command, primary_id, sheet_name, ...)
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    return FRAME_SUCCESS, msg
end

--信息查询
function BgipRole:on_bgip_playerspecificinfo(bgip_common, bgip_req)
    local player_id = tonumber(bgip_req.role_id)
    local query_target = bgip_req.query_target
    local rpc_command   = sformat("rpc_bgip_%s", slower(bgip_common.command))
    local exec_ok, msg
    --不在线
    if not bgip_mgr:is_online(player_id) then
        exec_ok, msg = bgip_mgr:exec_master_lobby_cmd(rpc_command, player_id, query_target)
    else
        exec_ok, msg  = bgip_mgr:exec_lobby_cmd(rpc_command, player_id, query_target)
    end

    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    return FRAME_SUCCESS, msg
end

function BgipRole:on_bgip_accountinfo(bgip_common, bgip_req)
    local open_id = bgip_req.open_id
    local rpc_command   = sformat("rpc_bgip_%s", slower(bgip_common.command))
    local exec_ok, msg = bgip_mgr:exec_master_lobby_cmd(rpc_command, 0, open_id)
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    return FRAME_SUCCESS, msg
end

--推送红点
function BgipRole:on_bgip_pushreddot(bgip_common, bgip_req)
    --role_ids只有一条
    local player_id = tonumber(bgip_req.role_ids)
    local num = bgip_req.num

    local rpc_command   = sformat("rpc_bgip_%s", slower(bgip_common.command))
    local exec_ok, msg  = bgip_mgr:exec_lobby_cmd(rpc_command, player_id, num)
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    return FRAME_SUCCESS, msg
end

--完成任务
function BgipRole:on_bgip_finishtask(bgip_common, bgip_req)
    local player_id = tonumber(bgip_req.role_id)
    local task_id = bgip_req.task_id

    local rpc_command   = sformat("rpc_bgip_%s", slower(bgip_common.command))
    local exec_ok, msg  = bgip_mgr:exec_lobby_cmd(rpc_command, player_id, task_id)
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    return FRAME_SUCCESS, msg
end

--接取任务
function BgipRole:on_bgip_accepttask(bgip_common, bgip_req)
    local player_id = tonumber(bgip_req.role_id)
    local task_id = bgip_req.task_id
    local rpc_command   = sformat("rpc_bgip_%s", slower(bgip_common.command))
    local exec_ok, msg  = bgip_mgr:exec_lobby_cmd(rpc_command, player_id, task_id)
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    return FRAME_SUCCESS, msg
end


--踢全服玩家下线
function BgipRole:on_bgip_kickall(bgip_common, bgip_req)
    local rpc_command   = sformat("rpc_bgip_%s", slower(bgip_common.command))
    local exec_ok, msg  = bgip_mgr:exec_lobby_broadcast(rpc_command)
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end

    return FRAME_SUCCESS, msg
end

--修改角色名称
function BgipRole:on_bgip_updaterolename(bgip_common, bgip_req)
    local player_id     = tonumber(bgip_req.role_id)
    local current_name  = bgip_req.current_name

    local rpc_command   = sformat("rpc_bgip_%s", slower(bgip_common.command))
    local exec_ok, msg
    --不在线
    if not bgip_mgr:is_online(player_id) then
        exec_ok, msg = bgip_mgr:exec_master_lobby_cmd(rpc_command, player_id, current_name)
    else
        exec_ok, msg  = bgip_mgr:exec_lobby_cmd(rpc_command, player_id, current_name)
    end

    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    return FRAME_SUCCESS, msg
end

--处罚角色
function BgipRole:on_bgip_punishrole(bgip_common, bgip_req)
    local player_id = tonumber(bgip_req.roleid)
    local punish_info_id = bgip_req.punish_info_id
    local operate_type = bgip_req.operate_type
    local extra = bgip_req.extra
    local ban_time = bgip_req.ban_time


    --解封操作
    if operate_type == -1 then
        return FRAME_SUCCESS, {roleid = sformat("%s", player_id), ban_time = ban_time or 0}
    end

    local rpc_command   = sformat("rpc_bgip_%s", slower(bgip_common.command))
    local exec_ok, msg
    --玩家是否在线
    if not bgip_mgr:is_online(player_id) then
        exec_ok, msg  = bgip_mgr:exec_master_lobby_cmd(rpc_command, player_id, punish_info_id, operate_type, extra, ban_time)
    else
        exec_ok, msg  = bgip_mgr:exec_lobby_cmd(rpc_command, player_id, punish_info_id, operate_type, extra, ban_time)
    end

    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    return FRAME_SUCCESS, msg
end

--内部方法
-------------------------------------------------------------------
--踢玩家下线
function BgipRole:kick_out(player_id)
    --玩家踢下线
    local rpc_command   = "rpc_bgip_kickout"
    local exec_ok, msg
    --在线
    if bgip_mgr:is_online(player_id) then
        exec_ok, msg  = bgip_mgr:exec_lobby_cmd(rpc_command, player_id)
        if exec_ok ~= FRAME_SUCCESS then
            return exec_ok, msg
        end
    end
    return FRAME_SUCCESS, msg
end

quanta.bgip_role = BgipRole()

return BgipRole
