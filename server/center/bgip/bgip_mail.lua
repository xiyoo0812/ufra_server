-- bgip_mail.lua

local jdecode           = json.decode
local sformat           = string.format
local slower            = string.lower
local log_debug         = logger.debug
local u8len             = utf8.len
local tsize             = qtable.size

local protobuf_mgr      = quanta.get("protobuf_mgr")
local bgip_mgr          = quanta.get("bgip_mgr")
local config_mgr        = quanta.get("config_mgr")
local utility_db        = config_mgr:init_table("utility", "key")
local ITEM_LIMIT        = utility_db:find_integer("value", "mail_item_limit")
local WEEK_S            = quanta.enum("PeriodTime", "WEEK_S")

local FRAME_SUCCESS     = protobuf_mgr:error_code("FRAME_SUCCESS")
local PARAMS_ERR        = protobuf_mgr:error_code("BGIP_PARAMS_ERR")

local event_mgr         = quanta.get("event_mgr")

local BgipMail = singleton()
function BgipMail:__init()
    --单人邮件通知
    event_mgr:add_listener(self, "on_bgip_sendnoticeaward")
    event_mgr:add_listener(self, "on_bgip_sendnoticeawardgroup")
    event_mgr:add_listener(self, "on_bgip_usermailrevoke")
    event_mgr:add_listener(self, "on_bgip_usermailattachcheck")
    event_mgr:add_listener(self, "on_bgip_usermaillist")
    event_mgr:add_listener(self, "on_bgip_sendsinglemail")
    event_mgr:add_listener(self, "on_bgip_sendservermail")

    --添加幂等特性
    bgip_mgr:add_idempot("on_bgip_sendnoticeaward")
    bgip_mgr:add_idempot("on_bgip_sendnoticeawardgroup")
    bgip_mgr:add_idempot("on_bgip_usermailrevoke")
    bgip_mgr:add_idempot("on_bgip_sendsinglemail")
    bgip_mgr:add_idempot("on_bgip_sendservermail")
end

-------------------------------------------------------------------

--多语言全局邮件
function BgipMail:on_bgip_sendservermail(bgip_common, bgip_req)
    local msg_type          = bgip_req.msg_type
    local default_lang      = bgip_req.default_language
    local mail_bodies       = bgip_req.mail_bodies
    --语言参数校验
    local lang_code, msg = self:check_language(default_lang, msg_type, mail_bodies)
    if lang_code ~= FRAME_SUCCESS then
        return lang_code, msg
    end
    --检查参数合法性
    if not self:check_mail_params(mail_bodies) then
        return PARAMS_ERR, {msg = "title or content out of limit"}
    end
    --判定是否有附件
    local items
    if bgip_req.item_list and bgip_req.item_list ~= "" then
        local item_code, items_json = pcall(jdecode, bgip_req.item_list, true)
        if not item_code then
            return PARAMS_ERR, { msg = sformat("parse failed. item_list:%s", bgip_req.item_list)}
        end
        items = items_json
        --附件个数
        if tsize(items) > ITEM_LIMIT then
            return PARAMS_ERR, {msg = sformat("parse failed. item_list size > %s", ITEM_LIMIT)}
        end
    end

    local mail_ttl = bgip_req.mail_ttl or WEEK_S * 2
    --rpc调用
    local rpc_command = sformat("rpc_bgip_%s", slower(bgip_common.command))
    local exec_ok, exec_msg = bgip_mgr:exec_center_cmd(rpc_command, default_lang, mail_bodies, items, mail_ttl)
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, exec_msg
    end
    return FRAME_SUCCESS, exec_msg
end

--多语言单人邮件
function BgipMail:on_bgip_sendsinglemail(bgip_common, bgip_req)
    local msg_type          = bgip_req.msg_type
    local default_lang      = bgip_req.default_language
    local mail_bodies       = bgip_req.mail_bodies
    --语言参数
    local lang_code, msg = self:check_language(default_lang, msg_type, mail_bodies)
    if lang_code ~= FRAME_SUCCESS then
        return lang_code, msg
    end
    --检查参数合法性
    if not self:check_mail_params(mail_bodies) then
        return PARAMS_ERR, {msg = "title or content out of limit"}
    end
    local player_id = tonumber(bgip_req.role_id)
    local mail_ttl  = bgip_req.mail_ttl or WEEK_S * 2
    local item_list = bgip_req.item_list

    --判定是否有附件
    local items
    if item_list and item_list ~= "" then
        local item_code, items_json = pcall(jdecode, item_list, true)
        if not item_code then
            return PARAMS_ERR, {msg = sformat("parse failed. item_list:%s", item_list)}
        end
        items = items_json
        --附件个数
        if tsize(items) > ITEM_LIMIT then
            return PARAMS_ERR, {msg = sformat("parse failed. item_list size > %s", ITEM_LIMIT)}
        end
    end

    local rpc_command = sformat("rpc_bgip_%s", slower(bgip_common.command))
    local exec_ok, exec_msg
    if not bgip_mgr:is_online(player_id) then
        exec_ok, exec_msg = bgip_mgr:exec_master_lobby_cmd(rpc_command, player_id, default_lang, mail_bodies, items, mail_ttl)
    else
        exec_ok, exec_msg = bgip_mgr:exec_lobby_cmd(rpc_command, player_id, default_lang, mail_bodies, items, mail_ttl)
    end
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, exec_msg
    end
    return FRAME_SUCCESS, exec_msg
end

--查询邮件列表
function BgipMail:on_bgip_usermaillist(bgip_common, bgip_req)
    local role_id   = tonumber(bgip_req.role_id)
    local page_size = bgip_req.page_size
    local page_num  = bgip_req.page_num
    local rpc_command = sformat("rpc_bgip_%s", slower(bgip_common.command))
    local exec_ok, msg
    if not bgip_mgr:is_online(role_id) then
        exec_ok, msg = bgip_mgr:exec_master_lobby_cmd(rpc_command, role_id, page_size, page_num)
    else
        exec_ok, msg = bgip_mgr:exec_lobby_cmd(rpc_command, role_id, page_size, page_num)
    end
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    return FRAME_SUCCESS, msg
end

--查询邮件附件状态
function BgipMail:on_bgip_usermailattachcheck(bgip_common, bgip_req)
    local role_id   = tonumber(bgip_req.role_id)
    local email_id  = tonumber(bgip_req.email_id)

    local rpc_command = sformat("rpc_bgip_%s", slower(bgip_common.command))
    local exec_ok, msg
    if not bgip_mgr:is_online(role_id) then
        exec_ok, msg = bgip_mgr:exec_master_lobby_cmd(rpc_command, role_id, email_id)
    else
        exec_ok, msg = bgip_mgr:exec_lobby_cmd(rpc_command, role_id, email_id)
    end
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    return FRAME_SUCCESS, msg
end

--撤回邮件
function BgipMail:on_bgip_usermailrevoke(bgip_common, bgip_req)
    local role_id   = tonumber(bgip_req.role_id)
    --1：单人邮件，2：区服邮件
    local is_users = bgip_req.is_users
    --邮件 id 列表，用逗号分隔，由于历史原因注意该参数目前只会发送一个 email_id，并不会发送多个 id
    local mail_id = tonumber(bgip_req.email_id_lists)
    if is_users == 1 and not role_id then
        return PARAMS_ERR, {msg = "is_users = 1 but role_id is nil"}
    end

    local rpc_command = sformat("rpc_bgip_%s", slower(bgip_common.command))
    if is_users == 1 then
        --单人邮件
        log_debug("[BgipMail][on_bgip_usermailrevoke] mail_id:{}", mail_id)
        local exec_ok, msg
        if not bgip_mgr:is_online(role_id) then
            exec_ok, msg = bgip_mgr:exec_master_lobby_cmd(rpc_command, role_id, mail_id)
        else
            exec_ok, msg = bgip_mgr:exec_lobby_cmd(rpc_command, role_id, mail_id)
        end
        if exec_ok ~= FRAME_SUCCESS then
            return exec_ok, msg
        end
        return FRAME_SUCCESS, msg
    else
        --全服邮件id是int
        local exec_ok, msg = bgip_mgr:exec_center_cmd(rpc_command, mail_id)
        if exec_ok ~= FRAME_SUCCESS then
            return exec_ok, msg
        end
        return FRAME_SUCCESS, msg
    end
end

--发送全服邮件
function BgipMail:on_bgip_sendnoticeawardgroup(bgip_common, bgip_req)
    local title = bgip_req.title
    local content = bgip_req.content
    local msg_type = bgip_req.msg_type
    --参数校验 当 msg_type=1 或 2 时title content必须
    if (msg_type == "1" or msg_type == "2") and (not content and not title) then
        return PARAMS_ERR, {msg = "when msg_type is 1 or 2, the title and content required"}
    end
    --检查参数合法性
    if not self:check_title_content(title, content) then
        return PARAMS_ERR, {msg = "title or content out of limit"}
    end
    local sender = bgip_req.sender or "admin"
    local mail_ttl = bgip_req.mail_ttl or  WEEK_S * 2
    local footer_desc = bgip_req.footer_desc
    local footer_url = bgip_req.footer_desc
    local black_list = bgip_req.black_list

    --判定是否有附件
    local items
    if bgip_req.item_list and bgip_req.item_list ~= ""  then
        local ok, items_json = pcall(jdecode, bgip_req.item_list, true)
        if not ok then
            return PARAMS_ERR, {msg = sformat("parse failed. item_list:%s", bgip_req.item_list)}
        end
        items = items_json
        --附件个数
        if tsize(items) > ITEM_LIMIT then
            return PARAMS_ERR, {msg = sformat("parse failed. item_list size > %s", ITEM_LIMIT)}
        end
    end

    local rpc_command = sformat("rpc_bgip_%s", slower(bgip_common.command))
    local exec_ok, msg = bgip_mgr:exec_center_cmd(rpc_command, title, content, sender, items, mail_ttl, footer_desc, footer_url, black_list)
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    return FRAME_SUCCESS, msg
end

--发送单人邮件
function BgipMail:on_bgip_sendnoticeaward(bgip_common, bgip_req)
    local title     = bgip_req.title
    local content   = bgip_req.content
    --检查参数合法性
    if not self:check_title_content(title, content) then
        return PARAMS_ERR, {msg = "title or content out of limit"}
    end

    local player_id = tonumber(bgip_req.role_id)
    local sender    = bgip_req.sender or "admin"
    local mail_ttl  = bgip_req.mail_ttl or WEEK_S * 2
    local footer_desc   = bgip_req.footer_desc
    local footer_url    = bgip_req.footer_desc
     --判定是否有附件
    local items
    if bgip_req.item_list  and bgip_req.item_list ~= "" then
        local ok, items_json = pcall(jdecode, bgip_req.item_list, true)
        if not ok then
            return PARAMS_ERR, {msg = sformat("parse failed. item_list:%s", bgip_req.item_list)}
        end
        items = items_json
        --附件个数
        if tsize(items) > ITEM_LIMIT then
            return PARAMS_ERR, {msg = sformat("parse failed. item_list size > %s", ITEM_LIMIT)}
        end
    end

    local rpc_command   = sformat("rpc_bgip_%s", slower(bgip_common.command))
    local exec_ok, msg
    if not bgip_mgr:is_online(player_id) then
        exec_ok, msg  = bgip_mgr:exec_master_lobby_cmd(rpc_command, player_id, title, content, sender, items,
                                                mail_ttl, footer_desc, footer_url)
    else
        exec_ok, msg  = bgip_mgr:exec_lobby_cmd(rpc_command, player_id, title, content, sender, items,
                                                mail_ttl, footer_desc, footer_url)
    end
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    return FRAME_SUCCESS, msg
end

--检查语言
function BgipMail:check_language(default_language, msg_type, mail_bodies)
    --检查参数
    local default
    for _, mail in pairs(mail_bodies) do
        if mail.language == default_language then
            default = mail
        end
    end
    if not default then
        return PARAMS_ERR, {msg = sformat("parse failed. cant find default language: %s", default_language)}
    end
    --参数校验 当 msg_type=1 或 2 时title content必须
    if (msg_type == "1" or msg_type == "2") and (not default.content and not default.title) then
        return PARAMS_ERR, {msg = "when msg_type is 1 or 2, the title and content required"}
    end
    return FRAME_SUCCESS
end

--检查多语言邮件参数
function BgipMail:check_mail_params(mail_bodies)
    if not mail_bodies then
        return false
    end
    for _, mail in pairs(mail_bodies) do
        if not self:check_title_content(mail.title, mail.content) then
            return false
        end
    end
    return true
end

--检查标题和正文合法性
function BgipMail:check_title_content(title, content)
    if not title or not content then
        return false
    end
    --标题不超过50字符 内容不超过1000
    if u8len(title) > 50 or u8len(content) > 1000 then
        return false
    end
    return true
end

quanta.bgip_mail = BgipMail()

return BgipMail
