-- bgip_packet.lua

local slower            = string.lower
local sformat           = string.format
local jdecode           = json.decode

local bgip_mgr          = quanta.get("bgip_mgr")
local event_mgr         = quanta.get("event_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")

local FRAME_SUCCESS     = protobuf_mgr:error_code("FRAME_SUCCESS")
local PARAMS_ERR        = protobuf_mgr:error_code("BGIP_PARAMS_ERR")

local BgipPacket = singleton()
function BgipPacket:__init()
    --添加道具
    event_mgr:add_listener(self, "on_bgip_additem")
    --回收道具
    event_mgr:add_listener(self, "on_bgip_recycleitem")
    --更新背包
    event_mgr:add_listener(self, "on_bgip_updateitem")

    --添加幂等特性
    bgip_mgr:add_idempot("on_bgip_additem")
    bgip_mgr:add_idempot("on_bgip_recycleitem")
    bgip_mgr:add_idempot("on_bgip_updateitem")
end

-------------------------------------------------------------------
--道具公用
function BgipPacket:packet_item(bgip_common, bgip_req)
    local ok, items = pcall(jdecode, bgip_req.item_list, true)
    if not ok then
        return PARAMS_ERR, {msg = sformat("parse failed. item_list:%s", bgip_req.item_list)}
    end
    local item_list = {}
    for _, item in pairs(items) do
        item_list[tonumber(item.itemId)] = item.itemCount
    end

    local player_id = tonumber(bgip_req.role_id)
    local rpc_command   = sformat("rpc_bgip_%s", slower(bgip_common.command))

    local exec_ok, msg
    if not bgip_mgr:is_online(player_id) then
        exec_ok, msg  = bgip_mgr:exec_master_lobby_cmd(rpc_command, player_id, item_list)
    else
        exec_ok, msg  = bgip_mgr:exec_lobby_cmd(rpc_command, player_id, item_list)
    end

    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    return FRAME_SUCCESS, msg
end

--新增道具
function BgipPacket:on_bgip_additem(bgip_common, bgip_req)
    return self:packet_item(bgip_common, bgip_req)
end

--回收道具
function BgipPacket:on_bgip_recycleitem(bgip_common, bgip_req)
    return self:packet_item(bgip_common, bgip_req)
end

--更新背包
function BgipPacket:on_bgip_updateitem(bgip_common, bgip_req)
    return self:packet_item(bgip_common, bgip_req)
end

quanta.bgip_packet = BgipPacket()

return BgipPacket
