--bgip_notice.lua
local sformat = string.format
local slower = string.lower
local bgip_mgr = quanta.get("bgip_mgr")
local event_mgr = quanta.get("event_mgr")

local BgipNotice = singleton()
function BgipNotice:__init()
    event_mgr:add_listener(self, "on_bgip_sendnotice")
    event_mgr:add_listener(self, "on_bgip_delnotice")

    bgip_mgr:add_idempot("on_bgip_sendnotice")
    bgip_mgr:add_idempot("on_bgip_delnotice")
end

function BgipNotice:on_bgip_sendnotice(bgip_common, bgip_req)
    local rpc_command = sformat("rpc_bgip_%s", slower(bgip_common.command))
    return bgip_mgr:exec_center_cmd(rpc_command, bgip_req)
end

function BgipNotice:on_bgip_delnotice(bgip_common, bgip_req)
    local rpc_command = sformat("rpc_bgip_%s", slower(bgip_common.command))
    return bgip_mgr:exec_center_cmd(rpc_command, bgip_req.guid)
end

quanta.bgip_notice = BgipNotice()
return BgipNotice