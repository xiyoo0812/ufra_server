--bgip_gm.lua

local gm_mgr        = quanta.get("gm_mgr")
local event_mgr     = quanta.get("event_mgr")

local LOCAL         = quanta.enum("GMType", "LOCAL")
local bgip_mgr      = quanta.get("bgip_mgr")

local BgipGM    = singleton()
function BgipGM:__init()
    --注册GM
    self:register()
end

--GM服务器
function BgipGM:register()
    local cmd_list = {
        {
            name = "gm_shutdown",
            gm_type = LOCAL,
            group = "运维",
            desc = "服务器停服",
            args = "",
            example= "",
            tip = ""
        },
        {
            name = "gm_account_limit",
            gm_type = LOCAL,
            group = "运维",
            desc = "修改进入账号上限",
            args = "package_channel|string limit|integer",
            example= "gm_account_limit aab 20",
            tip = "修改进入账号上限"
        },
    }

    gm_mgr:rpc_register_command(cmd_list)
    for _, cmd in ipairs(cmd_list) do
        event_mgr:add_listener(self, cmd.name)
    end
end

--gm停服
function BgipGM:gm_shutdown()
    bgip_mgr:on_bgip_shutdown()
    return 0, "success"
end

--修改进入账号上限
function BgipGM:gm_account_limit(package_channel, limit)
    --模拟bgip指令
    local bgip_req = {
        account_limit = limit,
        package_channel = package_channel
    }
    bgip_mgr:on_bgip_account_limit(nil, bgip_req)

    return 0, "success"
end

-- 导出
quanta.bgip_gm = BgipGM()

return BgipGM
