--bgip_servlet.lua

import("network/http_client.lua")
import("center/bgip/bgip_mgr.lua")
import("center/bgip/bgip_role.lua")
import("center/bgip/bgip_mail.lua")
import("center/bgip/bgip_packet.lua")
import("center/bgip/bgip_notice.lua")
import("center/bgip/bgip_activity.lua")
import("center/notice/notice_mgr.lua")

local log_debug         = logger.debug
local log_err           = logger.err
local tsize             = qtable.size
local guid_new          = codec.guid_new
local tunpack           = table.unpack
local sformat           = string.format
local guid_string       = codec.guid_string
local service, index    = quanta.service, quanta.index
local qfailed           = quanta.failed

local LOGIN             = quanta.enum("Service", "LOGIN")
local SECOND_10_MS      = quanta.enum("PeriodTime", "SECOND_10_MS")
local REGION            = environ.number("QUANTA_REGION")

local monitor           = quanta.get("monitor")
local mail_mgr          = quanta.get("mail_mgr")
local event_mgr         = quanta.get("event_mgr")
local timer_mgr         = quanta.get("timer_mgr")
local router_mgr        = quanta.get("router_mgr")
local http_client       = quanta.get("http_client")
local protobuf_mgr      = quanta.get("protobuf_mgr")
local notice_mgr        = quanta.get("notice_mgr")
local redis_agent       = quanta.get("redis_agent")

local MAIL_TYPE_SYS     = protobuf_mgr:enum("mail_type", "MAIL_TYPE_SYS")
local FRAME_SUCCESS     = protobuf_mgr:error_code("FRAME_SUCCESS")
local EXEC_ERR          = protobuf_mgr:error_code("BGIP_EXEC_ERR")
local AC_LIMIT_KEY      = "account_limmit"

-- bgip服务
local BgipServlet = singleton()
local prop = property(BgipServlet)
prop:reader("monitors", {})
function BgipServlet:__init()
    self:setup()

    monitor:watch_service_ready(self, "monitor")
    monitor:watch_service_close(self, "monitor")

    event_mgr:add_listener(self, "on_center_command")
end

function BgipServlet:setup()
    -- rpc消息
    event_mgr:add_listener(self, "rpc_bgip_sendnoticeawardgroup")
    event_mgr:add_listener(self, "rpc_bgip_usermailrevoke")
    event_mgr:add_listener(self, "rpc_bgip_sendservermail")
    event_mgr:add_listener(self, "rpc_bgip_shutdown")
    event_mgr:add_listener(self, "rpc_bgip_startup")

    event_mgr:add_listener(self, "rpc_bgip_delnotice")
    event_mgr:add_listener(self, "rpc_bgip_sendnotice")
    event_mgr:add_listener(self, "rpc_bgip_account_limit")
end

--判定是否需要额外处理
function BgipServlet:on_center_command(cmd, args)
    if cmd == "gm_send_global_mail" then
        return self:fill_global_mail_args(args)
    end
    return false, args
end

--全局邮件
--cmd|string guid|string from_player_name|string title|string content|string expire|integer json_attach|table
function BgipServlet:fill_global_mail_args(args)
    log_debug("[BgipServlet][gm_send_global_mail] args:{}", args)
    --入库,生成全局邮件ID
    args[2] = guid_string(service, index)
    return true, args
end

--gm指令
---------------------------------------------------------------------------
--修改账号进入限制命令
function BgipServlet:rpc_bgip_account_limit(package_channel, num)
    local key = sformat("%s_%s", AC_LIMIT_KEY, package_channel)
    local ok, code = redis_agent:save_msic(key, num)
    if qfailed(code, ok) then
        log_err("[LoginDao][save_ac_limit] save failed code:{}", code)
        return EXEC_ERR, {msg = "failed"}
    end
    return FRAME_SUCCESS, {msg = "success"}
end
--开服命令
function BgipServlet:rpc_bgip_startup()
    log_debug("[BgipServlet][rpc_bgip_startup] start")
    --发送所有login开启登录
    self:exec_login_start()
    return FRAME_SUCCESS, {msg = "success"}
end

--停服命令
function BgipServlet:rpc_bgip_shutdown(exit_process)
    log_debug("[BgipServlet][rpc_bgip_shutdown] start")
    --发送所有login关闭登录
    self:exec_login_close()
    --发送所有lobby停服
    local exec_ok, msg = self:exec_lobby_broadcast("rpc_server_close")
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    --延迟处理
    timer_mgr:once(SECOND_10_MS, function()
        --退出进程
        if exit_process then
            timer_mgr:once(SECOND_10_MS, function()
                --退出进程命令
                self:exec_monitor_broadcast()
            end)
        end
    end)
    return FRAME_SUCCESS, {msg = "success"}
end

function BgipServlet:rpc_bgip_sendnotice(request)
    log_debug("[BgipServlet][rpc_bgip_sendnotice] ({})", request)
    local notice = {
        id = request.id,
        role_id = request.role_id,
        content = request.content,
        interval = request.interval,
        begin_time = request.begin_time,
        end_time = request.end_time,
        show = request.show or 1,
        level_limit = request.level_limit or 1,
    }

    notice_mgr:add_notice(notice)
    return FRAME_SUCCESS, {msg = "success"}
end

function BgipServlet:rpc_bgip_delnotice(guid)
    log_debug("[BgipServlet][rpc_bgip_delnotice] ({})", guid)
    if notice_mgr:del_notice(guid) then
        log_debug("[BgipServlet][rpc_bgip_delnotice] delete ({}) successful", guid)
    end
    return FRAME_SUCCESS, {msg = "success"}
end

--多语言全服邮件
function BgipServlet:rpc_bgip_sendservermail(default_lang, mail_bodies, items, mail_ttl)
    log_debug("[BgipServlet][rpc_bgip_sendservermail] default_lang:{}", default_lang)
    return self:rpc_bgip_sendnoticeawardgroup(nil, nil, nil,
        items, mail_ttl, nil,nil, nil,mail_bodies, default_lang)
end

--撤回全局邮件
function BgipServlet:rpc_bgip_usermailrevoke(guid)
    log_debug("[BgipServlet][rpc_bgip_usermailrevoke] guid:{}", guid)
    --撤销操作
    local result = event_mgr:notify_listener("on_del_global_mail", guid)
    local call_ok, status_ok = tunpack(result)
    if not call_ok or not status_ok then
        return EXEC_ERR, {msg = sformat("failed call_ok:{} status_ok:{}", call_ok, status_ok)}
    end
    --分发到大厅
    mail_mgr:broadcast_lobby("rpc_del_global_mail", guid, true)
    return FRAME_SUCCESS, {msg = "success"}
end

--全局邮件
function BgipServlet:rpc_bgip_sendnoticeawardgroup(title, content, sender, item_list, mail_ttl, footer_desc, footer_url, black_list, mail_bodies, default_lang)
    log_debug("[BgipServlet][rpc_bgip_sendnoticeawardgroup] ({}, {}, {}, {}, {}, {}, {})", title, content, sender, item_list, mail_ttl, footer_desc, footer_url)
    local attaches = nil
    if tsize(item_list) > 0 then
        attaches = mail_mgr:json_2attach(item_list)
    end
    local expire = 0
    if mail_ttl then
        expire = quanta.now + mail_ttl
    end

    local rpc_req          = {
        sender              = sender,
        type                = MAIL_TYPE_SYS,
        black_list          = black_list,
        create_mail_time    = quanta.now_ms,
        title               = title,
        content             = content,
        expire_mail_time    = expire,
        attaches            = attaches,
        guid                = guid_new(),
        mail_bodies         = mail_bodies,
        default_lang        = default_lang
    }
    --入库操作
    local result = event_mgr:notify_listener("on_save_global_mail", rpc_req)
    local call_ok, status_ok = tunpack(result)
    if not call_ok or not status_ok then
        return EXEC_ERR, {msg = "fail save"}
    end
    --分发到大厅
    local ok, code =  mail_mgr:broadcast_lobby("rpc_add_global_mail", rpc_req)
    if not ok and not code then
        return EXEC_ERR, {msg = "failed broadcast"}
    end
    --构建回复
    local mail_result = {
        server_id   = REGION,
        code        = 1,                                               --区服邮件的发送结果，枚举：1 表示成功，2 表示失败
        mail_id     = sformat("%s", rpc_req.guid)        --字节定义为string
    }
    return FRAME_SUCCESS, {mail_result = {mail_result}}
end

-------------------------------内部命令------------------------------------
--大厅广播命令
function BgipServlet:exec_lobby_broadcast(cmd_name, ...)
    local ok, code, msg = router_mgr:call_lobby_all(cmd_name, ...)
    if not ok then
        log_err("[BgipServlet][exec_lobby_broadcast] rpc_command_execute failed!cmd_name={}", cmd_name)
        return EXEC_ERR, {msg = "rpc failed"}
    end
    return code,  msg
end

--login广播命令
function BgipServlet:exec_login_close()
    router_mgr:broadcast(LOGIN, "rpc_shield_service_proto", 0, true)
end

--login开服命令
function BgipServlet:exec_login_start()
    router_mgr:broadcast(LOGIN, "rpc_shield_service_proto", 0, false)
end

--cache广播命令
function BgipServlet:exec_cache_broadcast(cmd_name, ...)
    local ok, code, msg = router_mgr:call_cache_all(cmd_name, ...)
    if not ok then
        log_err("[BgipServlet][exec_cache_broadcast] rpc_command_execute failed!cmd_name={}", cmd_name)
        return EXEC_ERR, {msg = "rpc failed"}
    end
    return code,  msg
end

--monitor停服命令
function BgipServlet:exec_monitor_broadcast()
    log_debug("[BgipServlet][exec_monitor_broadcast] quit")
    for _, monitor_url in pairs(self.monitors) do
        local url = sformat("%s%s", monitor_url, "/shutdown")
        log_debug("[BgipServlet][exec_monitor_broadcast] monitor_url:{}", url)
        http_client:call_post(url)
    end
end

function BgipServlet:on_service_close(id, name)
    log_debug("[BgipServlet][on_service_close] node: {}-{}", name, id)
    self.monitors[id] = nil
end

function BgipServlet:on_service_ready(id, name, info)
    log_debug("[BgipServlet][on_service_ready] node: {}-{}, info: {}", name, id, info)
    self.monitors[id] = sformat("%s:%s", info.ip, info.port)
end

quanta.bgip_servlet = BgipServlet()

return BgipServlet
