--bgip_mgr.lua
import("basic/cmdline.lua")
import("agent/online_agent.lua")
local ldir                  = stdfs.dir
local lfilename             = stdfs.filename
local lb64decode            = ssl.b64_decode
local tunpack               = table.unpack
local log_debug             = logger.debug
local jdecode               = json.decode
local json_encode           = json.encode
local sformat               = string.format
local slower                = string.lower
local log_err               = logger.err
local tab_insert            = table.insert
local make_sid              = service.make_sid
local name2sid              = service.name2sid

local event_mgr             = quanta.get("event_mgr")
local protobuf_mgr          = quanta.get("protobuf_mgr")
local router_mgr            = quanta.get("router_mgr")
local gm_mgr                = quanta.get("gm_mgr")
local online                = quanta.get("online")
local config_mgr            = quanta.get("config_mgr")

local CLUSTER               = environ.get("QUANTA_CLUSTER")
local REGION                = environ.number("QUANTA_REGION")

local FRAME_SUCCESS         = protobuf_mgr:error_code("FRAME_SUCCESS")
local EXEC_ERR              = protobuf_mgr:error_code("BGIP_EXEC_ERR")
local USER_NOT_ONLINE_ERR   = protobuf_mgr:error_code("BGIP_USER_NOT_ONLINE_ERR")
local PARAMS_ERR            = protobuf_mgr:error_code("BGIP_PARAMS_ERR")
local IDEMPOT_ERR           = protobuf_mgr:error_code("BGIP_IDEMPOT_ERR")

local HttpServer            = import("network/http_server.lua")

local BgipMgr = singleton()
local prop = property(BgipMgr)
prop:reader("exec_cmds", {})        --已执行指令
prop:reader("need_idempot", {})     --需要检查幂等性接口
prop:reader("http_server", nil)

function BgipMgr:__init()
    --创建HTTP服务器
    local server = HttpServer(environ.get("QUANTA_BGIP_HTTP"))
    service.modify_host(server:get_port())
    self.http_server = server

    --监听bgip请求
    server:register_post("/bytedance", "on_bytedance", self)

    --注册bgip请求处理事件
    --服务器健康状态
    event_mgr:add_listener(self, "on_bgip_healthcheck")
    --停服命令
    event_mgr:add_listener(self, "on_bgip_shutdown")
    --开服命令
    event_mgr:add_listener(self, "on_bgip_startup")
    --GM开关命令
    event_mgr:add_listener(self, "on_bgip_gmserver")
    event_mgr:add_listener(self, "on_bgip_gmclient")
    event_mgr:add_listener(self, "on_bgip_set_log_level")
    event_mgr:add_listener(self, "on_bgip_config_file")
    event_mgr:add_listener(self, "on_bgip_config_text")
    event_mgr:add_listener(self, "on_bgip_account_limit")
    event_mgr:add_listener(self, "on_bgip_queryren")
    event_mgr:add_listener(self, "on_bgip_snapshot")
    event_mgr:add_listener(self, "on_bgip_runinfo")

    --添加幂等特性
    self:add_idempot("on_bgip_shutdown")
    self:add_idempot("on_bgip_startup")
    self:add_idempot("on_bgip_gmserver")
    self:add_idempot("on_bgip_gmclient")
    self:add_idempot("on_bgip_snapshot")
    self:add_idempot("on_bgip_runinfo")
    self:add_idempot("on_bgip_account_limit")
end

function BgipMgr:on_bgip_runinfo(bgip_common, bgip_req)
    local redis_agent = quanta.get("redis_agent")
    local key = sformat("QUANTA:%s:SERVICE", CLUSTER)
    local ok, code, res = redis_agent:execute({"HGETALL", key} )
    print(ok, code, res)
    if not ok then
        log_err("[BgipMgr][on_bgip_runinfo]")
        return EXEC_ERR, { msg = "rpc fail"}
    end
    return FRAME_SUCCESS, res
end

function BgipMgr:on_bgip_snapshot(bgip_common, bgip_req)
    local index = bgip_req.index
    local service_name = bgip_req.name
    local quanta_id = make_sid(name2sid(service_name), index)
    if service_name == "router" then
        local ok, codeoe, res = router_mgr:call_router_id(quanta_id, "rpc_show_snapshot")
        if not ok then
            log_err("[BgipMgr][on_bgip_snapshot] exec service={}-{} failed! codeoe={},res={}", service_name, index, codeoe, res)
        end
        return FRAME_SUCCESS, res
    end
    local ok, codeoe, res = router_mgr:call_target(quanta_id, "rpc_show_snapshot")
    if not ok then
        log_err("[CenterGM][show_snapshot] exec service={}-{} failed! codeoe={},res={}", service_name, index, codeoe, res)
    end
    return FRAME_SUCCESS, res
end

function BgipMgr:on_bgip_config_file(bgip_common, bgip_req)
    local file_dirs = { }
    local files = ldir("../server/config")
    for _, file in pairs(files or {}) do
        local fullname = file.name
        if file.type ~= "directory" then
            local file_name = lfilename(fullname)
            local sub_name = string.gsub(file_name, "_cfg.lua$", "")
            tab_insert(file_dirs, sub_name)
        end
    end
    return FRAME_SUCCESS, file_dirs
end

function BgipMgr:on_bgip_config_text(bgip_common, bgip_req)
    local tab = bgip_req.tab
    local tab_config = config_mgr:init_table(tab, "id")
    if tab_config == nil then
        return EXEC_ERR, { msg = "not find tab"}
    end
    --return FRAME_SUCCESS, tab_config:get_rows()
    local result = { }
    for _, conf in tab_config:iterator() do
        tab_insert(result, conf)
    end
    return FRAME_SUCCESS, result
end

--rpc
-------------------------------------------------------------------
--是否在线
function BgipMgr:is_online(player_id)
    local _, lobby = online:query_lobby(player_id)
    if not lobby or lobby == 0 then
        return false
    end
    return true
end

--master Bi命令
function BgipMgr:exec_master_bi_cmd(cmd_name, ...)
    local ok, code, msg = router_mgr:call_bilog_master(cmd_name, ...)
    if not ok then
        log_err("[BgipMgr][exec_master_bi_cmd] rpc_command_execute failed! cmd_name={}", cmd_name)
        return EXEC_ERR, {msg = "rpc failed"}
    end
    return code,  msg
end

--master大厅命令
function BgipMgr:exec_master_lobby_cmd(cmd_name, ...)
    local ok, code, msg = router_mgr:call_lobby_master(cmd_name, ...)
    if not ok then
        log_err("[BgipMgr][exec_master_lobby_cmd] rpc_command_execute failed! cmd_name={}", cmd_name)
        return EXEC_ERR, {msg = "rpc failed"}
    end
    return code,  msg
end

--center命令
function BgipMgr:exec_center_cmd(cmd_name, ...)
    local ok, code, msg = router_mgr:call_center_master(cmd_name, ...)
    if not ok then
        log_err("[BgipMgr][exec_center_cmd] rpc_command_execute failed!cmd_name={}", cmd_name)
        return EXEC_ERR, {msg = "rpc failed"}
    end
    return code,  msg
end

--大厅广播命令
function BgipMgr:exec_lobby_broadcast(cmd_name, ...)
    local ok, code, msg = router_mgr:call_lobby_all(cmd_name, ...)
    if not ok then
        log_err("[BgipMgr][exec_lobby_broadcast] rpc_command_execute failed!cmd_name={}", cmd_name)
        return EXEC_ERR, {msg = "rpc failed"}
    end
    return code,  msg
end

--大厅命令
function BgipMgr:exec_lobby_cmd(cmd_name, player_id, ...)
    if not self:is_online(player_id) then
        return USER_NOT_ONLINE_ERR, {msg = "role not online"}
    end
    local ok, code, msg = online:call_lobby(player_id, cmd_name, player_id, ...)
    if not ok then
        log_err("[BgipMgr][exec_lobby_cmd] rpc_command_execute failed! cmd_name={} player_id={}", cmd_name, player_id)
        return EXEC_ERR, {msg = "rpc failed"}
    end
    return code,  msg
end

--Cache命令
function BgipMgr:exec_cache_cmd(cmd_name, player_id, ...)
    --根据主键直接hash 不需要判定在线
    local ok, code, msg = router_mgr:call_cache_hash(player_id, cmd_name, player_id, ...)
    if not ok then
        log_err("[BgipMgr][exec_cache_cmd] rpc_command_execute failed! cmd_name={} player_id={}", cmd_name, player_id)
        return EXEC_ERR, {msg = "rpc failed"}
    end
    return code,  msg
end

--场景命令
function BgipMgr:exec_scene_cmd(cmd_name, player_id, ...)
    if not self:is_online(player_id) then
        return USER_NOT_ONLINE_ERR, {msg = "role not online"}
    end
    local ok, code, msg = online:call_scene(player_id, cmd_name, player_id, ...)
    if not ok then
        log_err("[BgipMgr][exec_scene_cmd] rpc_command_execute failed! cmd_name={} player_id={}", cmd_name, player_id)
        return EXEC_ERR, {msg = "rpc failed"}
    end
    return code,  msg
end

--login广播命令
function BgipMgr:exec_login_broadcast(cmd_name, ...)
    local ok, code, msg = router_mgr:call_login_all(cmd_name, ...)
    if not ok then
        log_err("[BgipMgr][exec_login_broadcast] rpc_command_execute failed!cmd_name={}", cmd_name)
        return EXEC_ERR, {msg = "rpc failed"}
    end
    return code,  msg
end

--http 回调
----------------------------------------------------------------------
--字节调用，json格式
function BgipMgr:on_bytedance(url, req_body, request)
    log_debug("[BgipMgr][on_bytedance] body: {} \n\n", req_body)
    --TODO 验签 白名单等等
    local body = lb64decode(req_body)
    local ok, cmd_req = pcall(jdecode, body)
    if not ok then
        return self:build_res(PARAMS_ERR, sformat("parse failed. body:%s", body))
    end
    local bgip_req = cmd_req.command_request
    local bgip_common = cmd_req.common
    log_debug("[BgipMgr][on_bytedance] cmd_req: {}", cmd_req)

    --转为center内部命令
    local command = sformat("on_bgip_%s", slower(bgip_common.command))
    --幂等性
    if self.need_idempot[command] then
        if self:is_repeat(bgip_common) then
            return self:build_res(IDEMPOT_ERR, "repeat serial")
        end
        self:add_exec_cmd(bgip_common)
    end

    local result = event_mgr:notify_listener(command, bgip_common, bgip_req)
    local call_ok, status_ok, res = tunpack(result)
    --调用失败
    if not call_ok then
        return self:build_res(EXEC_ERR, sformat("rpc execute failed. call_ok:%s", call_ok))
    end
    --处理失败
    if status_ok ~= FRAME_SUCCESS then
        return self:build_res(status_ok, sformat("execute failed. reason:%s", res.msg))
    end
    --成功
    return self:build_res(FRAME_SUCCESS, "success", res)
end

--构建应答参数
function BgipMgr:build_res(code, msg, res)
    local resp = {
        common = {
            result_code = code,
            error_msg   = msg
        },
        command_response = res
    }
    local resp_json = json_encode(resp, 1)
    log_debug("[BgipMgr][build_res] resp_json:{}", resp_json)
    return resp_json
end

--添加需要幂等性接口
function BgipMgr:add_idempot(cmd)
    self.need_idempot[cmd] = 1
end

--检查幂等性是否重复
function BgipMgr:is_repeat(bgip_common)
    --已执行
    if self.exec_cmds[bgip_common.serial] then
        return true
    end
    return false
end

--记录需要幂等性命令
function BgipMgr:add_exec_cmd(bgip_common)
    self.exec_cmds[bgip_common.serial] = bgip_common
end

--事件 回调
----------------------------------------------------------------------
--健康检查
function BgipMgr:on_bgip_healthcheck(bgip_common, bgip_req)
    local result = {
        server_id = REGION
    }
    return FRAME_SUCCESS, result
end

--查询留存命令
function BgipMgr:on_bgip_queryren(bgip_common, bgip_req)
    log_debug("[BgipMgr][on_bgip_queryren]")
    local rpc_command = "rpc_bgip_queryren"
    --解析年月日
    local year, month, day = bgip_req.ren_date:match("(%d+)-(%d+)-(%d+)")
    local exec_ok, msg = self:exec_master_bi_cmd(rpc_command, tonumber(year), tonumber(month), tonumber(day), bgip_req.diff_day)
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end

    return FRAME_SUCCESS, msg
end

--修改账号进入限制命令
function BgipMgr:on_bgip_account_limit(bgip_common, bgip_req)
    log_debug("[BgipMgr][on_bgip_account_limit]")
    local rpc_command = "rpc_bgip_account_limit"
    local exec_ok, msg = self:exec_center_cmd(rpc_command, bgip_req.package_channel, bgip_req.account_limit)
    if exec_ok ~= FRAME_SUCCESS then
    return exec_ok, msg
    end
    --继续分发给login
    exec_ok, msg = self:exec_login_broadcast(rpc_command, bgip_req.package_channel, bgip_req.account_limit)
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    return FRAME_SUCCESS, msg
end

--停服命令
function BgipMgr:on_bgip_shutdown(bgip_common, bgip_req)
    log_debug("[BgipMgr][on_bgip_shutdown]")
    local rpc_command = "rpc_bgip_shutdown"
    local exec_ok, msg = self:exec_center_cmd(rpc_command)
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    return FRAME_SUCCESS, msg
end

--开服命令
function BgipMgr:on_bgip_startup(bgip_common, bgip_req)
    log_debug("[BgipMgr][on_bgip_startup]")
    local rpc_command = "rpc_bgip_startup"
    local exec_ok, msg = self:exec_center_cmd(rpc_command)
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    return FRAME_SUCCESS, msg
end

--GM服务器开关状态切换
function BgipMgr:on_bgip_gmserver(bgip_common, bgip_req)
    log_debug("[BgipMgr][on_bgip_gmserver]")
    --1开启 0关闭
    local gm_status = gm_mgr:gm_switch( bgip_req.status == 1)
    return FRAME_SUCCESS, {msg = sformat("gm_status:%s", gm_status)}
end

--GM客户端开关
function BgipMgr:on_bgip_gmclient(bgip_common, bgip_req)
    log_debug("[BgipMgr][on_bgip_gmclient]")
    local rpc_command = "rpc_bgip_gm_client"
    --1开启 0关闭
    local exec_ok, msg = self:exec_lobby_broadcast(rpc_command, bgip_req.status == 1)
    if exec_ok ~= FRAME_SUCCESS then
        return exec_ok, msg
    end
    return FRAME_SUCCESS, {msg = sformat("gm_status:%s", bgip_req.status == 1)}
end

function BgipMgr:on_bgip_set_log_level(bgip_common, bgip_req)
    local cmd_name = "rpc_set_logger_level"
    local func = sformat("call_%s_all", bgip_req.name)
    local call_back = router_mgr[func]
    local ok, code, msg = call_back(router_mgr, cmd_name, bgip_req.level)
    if not ok then
        log_err("[BgipMgr][on_bgip_set_log_level] rpc_command_execute failed!cmd_name={}", cmd_name)
        return EXEC_ERR, {msg = "rpc failed"}
    end
    log_debug("[BgipMgr][on_bgip_set_log_level] name:{} level:{}", bgip_req.name, bgip_req.level)
    return code, msg
end

quanta.bgip_mgr = BgipMgr()

return BgipMgr