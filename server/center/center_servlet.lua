-- center_servlet.lua
local log_debug      = logger.debug
local log_warn       = logger.warn
local sformat        = string.format

local event_mgr      = quanta.get("event_mgr")
local update_mgr     = quanta.get("update_mgr")
local invcode_mgr    = quanta.get("invcode_mgr")
local protobuf_mgr   = quanta.get("protobuf_mgr")

local FRAME_SUCCESS  = protobuf_mgr:error_code("FRAME_SUCCESS")
local FRAME_FAILED   = protobuf_mgr:error_code("FRAME_FAILED")
local NOT_INVCODE    = protobuf_mgr:error_code("WORLDM_NOT_INVCODE")

local CenterServlet  = singleton()
local prop           = property(CenterServlet)
prop:accessor("locks", {}) -- 全局锁

function CenterServlet:__init()
    --rpc
    event_mgr:add_listener(self, "rpc_query_lock")     -- 查询锁
    event_mgr:add_listener(self, "rpc_invcode_query")  -- 查询邀请码
    event_mgr:add_listener(self, "rpc_invcode_allot")  -- 分配邀请码
    event_mgr:add_listener(self, "rpc_invcode_delete") -- 删除邀请码
    -- 定时器
    update_mgr:attach_minute(self)
end

-- 定时器
function CenterServlet:on_minute()
    for id, _ in pairs(self.locks) do
        local lock = self.locks[id]
        if lock.time < quanta.now then
            self.locks[id] = nil
        end
    end
end

-- 查询锁
function CenterServlet:rpc_query_lock(lock_kind, primary_id, time)
    local lock_id = sformat("%s_%s", lock_kind, primary_id)
    local lock = self.locks[lock_id]
    if lock and quanta.now < lock.time then
        return FRAME_SUCCESS, true
    end
    self.locks[lock_id] = { time = quanta.now + time and time or 10 }
    return FRAME_SUCCESS, false
end

-- 查询邀请码
function CenterServlet:rpc_invcode_query(invcode)
    log_debug("[CenterServlet][rpc_invcode_query] invcode:{}", invcode)
    local world_id = invcode_mgr:query(invcode)
    if not world_id then
        log_warn("[CenterServlet][rpc_invcode_query] world_id is nil invcode:{}", invcode)
        return NOT_INVCODE
    end
    log_debug("[CenterServlet][rpc_invcode_query] success invcode:{} world_id:{}", invcode, world_id)
    return FRAME_SUCCESS, world_id
end

-- 分配邀请码
function CenterServlet:rpc_invcode_allot(world_id)
    log_debug("[CenterServlet][rpc_invcode_allot] world_id:{}", world_id)
    local invcode = invcode_mgr:allot(world_id)
    if not invcode then
        log_warn("[CenterServlet][rpc_invcode_allot] invcode is nil world_id:{}", world_id)
        return FRAME_FAILED
    end
    return FRAME_SUCCESS, invcode
end

-- 删除邀请码
function CenterServlet:rpc_invcode_delete(invcode)
    log_debug("[CenterServlet][rpc_invcode_delete] invcode:{}", invcode)
    invcode_mgr:delete(invcode)
    return FRAME_SUCCESS
end

quanta.center_servlet = CenterServlet()

return CenterServlet
