--init.lua
--center service includes
import("business/dao/activity_dao.lua")
import("worldm/dao/world_dao.lua")
import("agent/redis_agent.lua")
import("center/core/map_mgr.lua")
import("center/core/chcom_mgr.lua")
import("center/core/invcode_mgr.lua")
import("center/core/activity_gm.lua")
import("center/center_servlet.lua")
import("center/mail/mail_servlet.lua")
import("center/bgip/bgip_servlet.lua")
import("center/bgip/bgip_gm.lua")
