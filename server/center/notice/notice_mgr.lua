-- notice_mgr.lua
local log_debug     = logger.debug
local table_insert  = table.insert
local to_number     = tonumber
local online        = quanta.get("online")
local bgip_mgr      = quanta.get("bgip_mgr")
local timer_mgr     = quanta.get("timer_mgr")
local update_mgr    = quanta.get("update_mgr")
local router_mgr    = quanta.get("router_mgr")
local SECOND_10_MS  = quanta.enum("PeriodTime", "SECOND_10_MS")

local NoticeMgr = singleton()

local prop = property(NoticeMgr)
prop:reader("notices", { })

function NoticeMgr:__init()
    timer_mgr:once(SECOND_10_MS, function()
        --分钟定时器 检查全局邮件过期
        update_mgr:attach_second5(self)
    end)
end

function NoticeMgr:add_notice(notice)

    notice.last_send_time = 0
    if notice.begin_time >= quanta.now then
        if notice.interval > 0 then
            self.notices[notice.id] = notice
        end
        self:broadcast_client(notice)
    else
        self.notices[notice.id] = notice
    end
end

function NoticeMgr:del_notice(guid)
    if self.notices[guid] then
        self.notices[guid] = nil
        return true
    end
    return false
end

function NoticeMgr:broadcast_client(notice)
    notice.last_send_time = quanta.now
    if #notice.role_id > 0 then
        local player_id = to_number(notice.role_id)
        if bgip_mgr:is_online(player_id) then
            local message = {
                data = {
                    show = notice.show,
                    content = notice.content
                }
            }
            online:send_client(player_id, player_id, "NID_MARQUEE_SEND_NTF", message)
        end
        return
    end
    router_mgr:call_gateway_all("rpc_broadcast_client", "NID_MARQUEE_SEND_NTF", {
        data = {
            show = notice.show,
            content = notice.content
        }
    })
end

function NoticeMgr:on_second5()
    local now = quanta.now
    local del_notices = { }
    for guid, notice in pairs(self.notices) do
        if notice.begin_time < now then --时间还没到
            goto continue
        elseif notice.end_time > 0 and notice.end_time > now then
            table_insert(del_notices, guid)
        elseif now - notice.last_send_time >= notice.interval then
            self:broadcast_client(notice)
            if notice.interval == 0 then
                table_insert(del_notices, guid)
            end
        end
        :: continue ::
    end
    for _, guid in ipairs(del_notices) do
        self.notices[guid] = nil
        log_debug("del_notice guid={}", guid)
    end
end

quanta.notice_mgr = NoticeMgr()

return NoticeMgr