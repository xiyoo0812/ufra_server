--activity_gm.lua
local log_warn      = logger.warn

local gm_mgr        = quanta.get("gm_mgr")
local event_mgr     = quanta.get("event_mgr")
local activity_dao  = quanta.get("activity_dao")
local config_mgr    = quanta.get("config_mgr")
local LOCAL         = quanta.enum("GMType", "LOCAL")

local cdkeys_db     = config_mgr:init_table("activity_cdkey", "id")

local ActivityGM      = singleton()
function ActivityGM:__init()
    local cmd_list = {
        {
            name = "survey_search",
            gm_type = LOCAL,
            group = "活动",
            desc = "问卷调查-搜索",
            args = "activity_id|integer player_id|integer page|integer size|integer",
            example= "survey_search 1060 10010101 1 10",
            tip = ""
        },
        {
            name = "survey_list",
            gm_type = LOCAL,
            group = "活动",
            desc = "问卷调查-列表",
            args = "activity_id|integer page|integer size|integer",
            example= "survey_list 1060 1 10",
            tip = ""
        },
        {
            name = "cdkey_search",
            gm_type = LOCAL,
            group = "活动",
            desc = "cdkey记录搜索",
            args = "activity_id|integer name|string page|integer size|integer",
            example= "cdkey_search 10000 1 10",
            tip = "name:player_id|cdkey|device_id"
        },
        {
            name = "cdkey_list",
            gm_type = LOCAL,
            group = "活动",
            desc = "cdkey领取列表",
            args = "activity_id|integer start_time|string end_time|string page|integer size|integer",
            example= "cdkey_list 1060 2024/01/11-11:00:00 2024/01/11-20:00:00 1 10",
            tip = ""
        },
        {
            name = "cdkey_info",
            gm_type = LOCAL,
            group = "活动",
            desc = "cdkey信息",
            args = "id|integer",
            example= "cdkey_info 1060",
            tip = ""
        }
    }
    gm_mgr:rpc_register_command(cmd_list)
    for _, cmd in ipairs(cmd_list) do
        event_mgr:add_listener(self, cmd.name)
    end
end

-- 字符串转时间戳
function ActivityGM:str_to_timestamp(str)
    local pattern = "(.-)%/(.-)%/(.-) (.-)%:(.-):(.+)"
    local year, month, day, hour, min, sec = str:match(pattern)
    if year == nil or month == nil or day == nil or min==nil or sec == nil then
        return nil
    end
    local timestamp = os.time({
        year = year,
        month = month,
        day = day,
        hour = hour,
        min = min,
        sec = sec
    })
    return timestamp
end

-- 问卷调查列表
function ActivityGM:survey_list(id, page, size)
    local ok,data = activity_dao:load_survery_list(id, size, page)
    if not ok then
        log_warn("[ActivityGM][survey_list] load_data fail")
        return
    end
    return true, data
end

-- 问卷调查搜索
function ActivityGM:survey_search(id, player_id, page, size)
    local ok,data = activity_dao:survey_search(id, player_id, size, page)
    if not ok then
        log_warn("[ActivityGM][survey_search] load_data fail")
        return
    end
    return true, data
end

-- cdkeys记录
function ActivityGM:cdkey_list(id, start_time, end_time, page, size)
    local start_timestamp = self:str_to_timestamp(string.gsub(start_time, "-", " "))
    local end_timestamp = self:str_to_timestamp(string.gsub(end_time, "-", " "))
    local ok,data = activity_dao:load_cdkey_list(id, start_timestamp, end_timestamp, size, page)
    if not ok then
        log_warn("[ActivityGM][cdkey_list] load_data fail")
        return
    end
    return true, data
end

-- 问卷调查搜索
function ActivityGM:cdkey_search(id, name, page, size)
    local ok,data = activity_dao:cdkey_search(id, name, size, page)
    if not ok then
        log_warn("[ActivityGM][cdkey_search] load_data fail")
        return
    end
    return true, data
end

-- 获取cdkey信息
function ActivityGM:cdkey_info(id)
    local ok, recv_count = activity_dao:get_cdkey_count()
    if not ok then
        log_warn("[ActivityGM][cdkey_info] get_cdkey_count fail")
        return
    end
    local sum_count = 0
    for _, conf in cdkeys_db:iterator() do
        if conf.activity_id == id then
            sum_count = sum_count + 1
        end
    end
    local result = {
        sum_count = sum_count,
        recv_count = recv_count,
        last_count = sum_count - recv_count
    }
    return true, result;
end

quanta.activity_gm = ActivityGM()
return ActivityGM
