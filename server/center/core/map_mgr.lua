--map_mgr.lua
local log_debug     = logger.debug

local monitor       = quanta.get("monitor")
local event_mgr     = quanta.get("event_mgr")
local router_mgr    = quanta.get("router_mgr")

local SUCCESS       = quanta.enum("KernCode", "SUCCESS")

local MapMgr = singleton()
local prop = property(MapMgr)
prop:reader("max_index", 0)
prop:reader("scene_maps", {})

function MapMgr:__init()
    -- rpc
    event_mgr:add_listener(self, "rpc_alloc_scene_line")
    -- 关注scene服务
    monitor:watch_service_ready(self, "scene")
end

--场景服变动
function MapMgr:on_service_ready(id, name, info)
    log_debug("[SceneProxy][on_service_ready] node: {}-{}, info: {}", name, id, info)
    self:alloc_line(id)
    router_mgr:call_scene_all("rpc_notify_scene_line", self.scene_maps)
end

-- 服务开启
function MapMgr:rpc_alloc_scene_line(scene_id)
    log_debug("[MapMgr][rpc_alloc_scene_line] scene_id: {}", scene_id)
    self:alloc_line(scene_id)
    return SUCCESS, self.scene_maps
end

function MapMgr:alloc_line(scene_id)
    for i = 1, self.max_index do
        local svr_id = self.scene_maps[i]
        if svr_id and svr_id == scene_id then
            return i
        end
    end
    for i = 1, self.max_index do
        if not self.scene_maps[i] then
            self.scene_maps[i] = scene_id
            return i
        end
    end
    local nindex = self.max_index + 1
    self.scene_maps[nindex] = scene_id
    self.max_index = nindex
    return nindex
end

-- export
quanta.map_mgr = MapMgr()

return MapMgr
