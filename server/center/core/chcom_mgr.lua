--chcom_mgr.lua
local log_debug     = logger.debug
local log_info      = logger.info
local log_err       = logger.err
local trandom       = qtable.random
local qfailed       = quanta.failed

local monitor       = quanta.get("monitor")
local event_mgr     = quanta.get("event_mgr")
local router_mgr    = quanta.get("router_mgr")

local SUCCESS       = quanta.enum("KernCode", "SUCCESS")

local ChcomMgr      = singleton()
local prop          = property(ChcomMgr)
-- 商会映射
prop:reader("chcoms", {})
prop:reader("chcomsvr_maps", {})

function ChcomMgr:__init()
    --rpc
    event_mgr:add_listener(self, "rpc_query_chcomsrv") -- 查询商会服务
    event_mgr:add_listener(self, "rpc_sync_chcomsrv")  -- 同步商会服务

    -- 关注chcom服务
    monitor:watch_service_ready(self, "chcom")
    monitor:watch_service_close(self, "chcom")
end

-- 分配商会服务
function ChcomMgr:rpc_query_chcomsrv(chcom_id)
    log_debug("[ChcomMgr][rpc_query_chcomsrv] chcom(%s)", chcom_id)
    local chcomsrv = self:query_chcom(chcom_id)
    return SUCCESS, chcomsrv
end

-- 同步商会服务
function ChcomMgr:rpc_sync_chcomsrv(chcom_id, chcomsrv)
    log_debug("[ChcomMgr][rpc_sync_chcomsrv] chcom(%s) chcomsrv(%s)", chcom_id, chcomsrv)
    self:sync_chcom(chcom_id, chcomsrv)
    return SUCCESS
end

-- 服务开启
function ChcomMgr:on_service_ready(id, name, info)
    log_debug("[ChcomMgr][on_service_ready] node: %s-%s, info: %s", name, id, info)
    self.chcoms[id] = info
end

-- 服务关闭
function ChcomMgr:on_service_close(id, name)
    log_debug("[ChcomMgr][on_service_close] node: %s-%s", name, id)
    self.chcoms[id] = nil
    for key, val in pairs(self.chcomsvr_maps) do
        if val == id then
            self.chcomsvr_maps[key] = nil
        end
    end
end

-- 同步商会服务
function ChcomMgr:sync_chcom(chcom_id, chcomsrv)
    self.chcomsvr_maps[chcom_id] = chcomsrv
end

-- 查询商会服务
function ChcomMgr:query_chcom(chcom_id)
    local chcomsrv_id = self.chcomsvr_maps[chcom_id]
    if chcomsrv_id then
        return chcomsrv_id
    end

    -- 随机分配商会服务
    chcomsrv_id = trandom(self.chcoms)
    -- 通知商会服务初始化商会
    local ok, code = router_mgr:call_target_hash(chcomsrv_id, chcom_id, "rpc_chcom_load", chcom_id)
    if qfailed(code, ok) then
        log_err("[ChcomMgr][query_chcom] fail chcom(%s) chcomsrv(%s)", chcom_id, chcomsrv_id)
        return nil
    end
    self.chcomsvr_maps[chcom_id] = chcomsrv_id
    log_info("[ChcomMgr][query_chcom] success chcom(%s) chcomsrv(%s)", chcom_id, chcomsrv_id)
    return chcomsrv_id
end

-- export
quanta.chcom_mgr = ChcomMgr()
return ChcomMgr
