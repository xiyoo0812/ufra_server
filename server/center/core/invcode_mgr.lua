--invcode_mgr.lua
local log_debug     = logger.debug

local event_mgr     = quanta.get("event_mgr")
local monitor       = quanta.get("monitor")
local thread_mgr    = quanta.get("thread_mgr")
local world_dao     = quanta.get("world_dao")
local protobuf_mgr  = quanta.get("protobuf_mgr")

local CODE_LEN      = 6
local SECOND_3_MS   = quanta.enum("PeriodTime", "SECOND_3_MS")
local FRAME_SUCCESS = protobuf_mgr:error_code("FRAME_SUCCESS")

local InvCodeMgr    = singleton()
local prop          = property(InvCodeMgr)
prop:reader("inv_codes", {})
prop:reader("worlds", {})
prop:reader("init_flag", false)
function InvCodeMgr:__init()
    -- 监听
    monitor:watch_service_ready(self, "mongo")
    -- rpc
    event_mgr:add_listener(self, "rpc_invcode_allot")
    event_mgr:add_listener(self, "rpc_invcode_delete")
    event_mgr:add_listener(self, "rpc_invcode_query")
end

function InvCodeMgr:on_service_ready(id, name, info)
    log_debug("[InvCodeMgr][on_service_ready] id:{}, name:{} info:{}", id, name, info)
    thread_mgr:success_call(SECOND_3_MS, function()
        return self:load()
    end)
end

function InvCodeMgr:check_init()
    return self.init_flag
end

function InvCodeMgr:load()
    local ok, dbdata = world_dao:load_list({
        ["id"] = 1,
        ["invcode"] = 1
    })
    if not ok then
        return false
    end
    for _, data in pairs(dbdata) do
        if data.invcode and data.invcode ~= "" then
            self.inv_codes[data.invcode] = data.id
            self.worlds[data.id] = data.invcode
        end
    end
    self.init_flag = true
    return true
end

function InvCodeMgr:allot(world_id)
    if not world_id then
        return nil
    end
    local invcode = self.worlds[world_id]
    if invcode then
        return FRAME_SUCCESS, invcode
    end
    invcode = self:create()
    if not invcode then
        return nil
    end
    self.inv_codes[invcode] = world_id
    self.worlds[world_id] = invcode
    return invcode
end

function InvCodeMgr:delete(code)
    if not code or code == "" then
        return
    end
    local world_id = self.inv_codes[code]
    if world_id then
        self.worlds[world_id] = nil
    end
    self.inv_codes[code] = nil
end

function InvCodeMgr:query(code)
    return self.inv_codes[code]
end

function InvCodeMgr:generate_invcode(len)
    local chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    local invite_code = ""
    for _ = 1, len do
        local random_index = math.random(1, #chars)
        invite_code = invite_code .. string.sub(chars, random_index, random_index)
    end
    return invite_code
end

function InvCodeMgr:create()
    for _ = 1, 100 do
        local invcode = self:generate_invcode(CODE_LEN)
        if not self.inv_codes[invcode] then
            return invcode
        end
    end
    return nil
end

quanta.invcode_mgr = InvCodeMgr()
return InvCodeMgr
