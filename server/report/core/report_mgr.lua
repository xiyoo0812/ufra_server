--report_mgr.lua

local env_get                  = environ.get
local qfailed                  = quanta.failed
local json_encode              = json.encode
local lb64encode               = ssl.b64_encode
local guid_string              = codec.guid_string
local service_id, index        = quanta.service, quanta.index


local PRIVATE_KEY              = [[
MIICWwIBAAKBgQCWKUc5BTsvNKLv389mqShFhg7lHbG8SyyAiHZ5gMMMoBGayBGg
OCGXHDRDUabr0E8xFtSApu9Ppuj3frzwRDcj4Q69yXc/x1+a18Jt96DI/DJEkmkm
o/Mr+pmY4mVFk4a7pxnXpynBUz7E7vp9/XvMs84LDFqqvGiSmW/YKJfsAQIDAQAB
AoGANhfDnPJZ+izbf07gH0rTg4wB5J5YTwzDiL/f8fAlE3C8NsZYtx9RVmamGxQY
bf158aSYQ4ofTlHBvZptxJ3GQLzJQd2K15UBzBe67y2umN7oP3QD+nUhw83PnD/R
A+aTmEiujIXS9aezbfaADYGd5fFr2ExUPvw9t0Pijxjw8WMCQQDDsGLBH4RTQwPe
koVHia72LF7iQPP75AaOZIuhCTffaLsimA2icO+8/XT2yaeyiXqHn1Wzyk1ZrGgy
MTeTu9jPAkEAxHDPRxNpPUhWQ6IdPWflecKpzT7fPcNJDyd6/Mg3MghWjuWc1xTl
nmBDdlQGOvKsOY4K4ihDZjVMhBnqp16CLwJAOvaT2wMHGRtxOAhIFnUa/dwCvwO5
QGXFv/P1ypD/f9aLxHGycga7heOM8atzVy1reR/+b8z+H43+W1lPGLmaKwJAJ2zA
nPIvX+ZBsec6WRWd/5bq/09L/JhR9GGnFE6WjUsRHDLHDH+cKfIF+Bya93+2wwJX
+tW72Sp/Rc/xwU99bwJAfUw9Nfv8llVA2ZCHkHGNc70BjTyaT/TxLV6jcouDYMTW
RfSHi27F/Ew6pENe4AwY2sfEV2TXrwEdrvfjNWFSPw==
]]

local router_mgr               = quanta.get("router_mgr")

local TOKEN_EXP_TIME           = 86400 * 3
local RESPONSE_CODE_OK         = 0
local RESPONSE_CODE_ARGS_ERROR = 1  --请求参数错误
local RESPONSE_CODE_NOT_AUTH   = 2  --未验证的请求
local RESPONSE_CODE_TOKEN_EXP  = 3  --token过期
local RESPONSE_CODE_RPC_ERROR  = 4  --内部rpc调用错误

local ReportMgr = singleton()
local prop = property(ReportMgr)
prop:reader("tokens",  {})
prop:reader("rsa_key", nil)

function ReportMgr:__init()
    --rsa key
    self.rsa_key = ssl.rsa_init_prikey(PRIVATE_KEY)

    local HTTP_HOST = env_get("QUANTA_REGISTRY_HTTP")
    local HttpServer = import("network/http_server.lua")

    local http_server = HttpServer(HTTP_HOST)
    http_server:register_post("/registry/push", "on_push", self)
    http_server:register_post("/registry/get_token", "get_token", self)
    http_server:register_post("/world/player_count", "on_player_count", self)
    http_server:register_post("/world/change_state", "on_state_change", self)
    http_server:register_post("/registry/refresh_token", "refresh_token", self)
    quanta.registry_server = http_server
end

function ReportMgr:new_token()
    return guid_string(service_id, index)
end

function ReportMgr:encode_token(token)
    if token then
        local b64_token = self.rsa_key.pri_encode(token)
        if b64_token then
            return lb64encode(b64_token)
        end
    end
end

function ReportMgr:on_player_count(url, body, request, header)
    local response = { }
    response.code = self:auth_token(header)
    if response.code ~= RESPONSE_CODE_OK then
        return json_encode(response)
    end
    local token = header["Token"]
    local ok, code, result = router_mgr:call_worldm_hash(service_id, "rpc_agent_sync", {
        id = token,
        worlds = body
    })
    if not ok or qfailed(code) then
        response.code = RESPONSE_CODE_RPC_ERROR
    else
        response.data = result
    end
    return json_encode(response)
end

function ReportMgr:on_state_change(url, body, request, header)
    local response = { }
    response.code = self:auth_token(header)
    if response.code ~= RESPONSE_CODE_OK then
        return json_encode(response)
    end
    local token = header["Token"]
    local ok, code, result = router_mgr:call_worldm_hash(service_id, "rpc_agent_state", {
        id = token,
        worlds = body
    })
    if not ok or qfailed(code) then
        response.code = RESPONSE_CODE_RPC_ERROR
    else
        response.data = result
    end
    return json_encode(response)
end

function ReportMgr:get_token(url, body, request, header)
    local agent_id = body.agent_id
    local agent_url = body.agent_url
    local response = { code = RESPONSE_CODE_OK }
    if agent_id == nil then
        response.code = RESPONSE_CODE_ARGS_ERROR
        return json_encode(response)
    elseif agent_url == nil then
        response.code = RESPONSE_CODE_ARGS_ERROR
        return json_encode(response)
    end
    local exp_time = quanta.now + TOKEN_EXP_TIME
    local token = self:new_token()

    local ok, code, result = router_mgr:call_worldm_hash(service_id, "rpc_agent_register", {
        id = token,
        agent_id = agent_id,
        agent_url = agent_url
    })
    if not ok or qfailed(code) then
        response.code = RESPONSE_CODE_RPC_ERROR
    else
        response.data = result
        response.code = RESPONSE_CODE_OK
    end
    if response.code == RESPONSE_CODE_OK then
        self.tokens[token] = {
            token = token,
            exp_time = exp_time
        }
        response.exp_time = exp_time
        response.token = self:encode_token(token) --私钥加密
    end
    return json_encode(response)
end

function ReportMgr:auth_token(header)
    local token = header["Token"]
    if token == nil then
        return RESPONSE_CODE_ARGS_ERROR
    end
    local token_info = self.tokens[token]
    if token_info == nil then
        return RESPONSE_CODE_NOT_AUTH
    end
    local now_time = quanta.now
    if now_time >= token_info.exp_time then
        return RESPONSE_CODE_TOKEN_EXP
    end
    return RESPONSE_CODE_OK, token_info
end

function ReportMgr:refresh_token(url, body, request, header)
    local response = {}
    response.code = self:auth_token(header)
    if response.code ~= RESPONSE_CODE_OK then
        return json_encode(response)
    end
    local new_token = self:new_token()
    local old_token = header["Token"]
    local exp_time = quanta.now + TOKEN_EXP_TIME
    self.tokens[old_token] = nil
    self.tokens[new_token] = {
        token = new_token,
        exp_time = exp_time
    }
    response.exp_time = exp_time
    response.token = self:encode_token(new_token)
    return json_encode(response)
end

function ReportMgr:on_push(_, body, request, header)
    local response = { }
    response.code = self:auth_token(header)
    if response.code ~= RESPONSE_CODE_OK then
        return json_encode(response)
    end
    local ok, code, result = router_mgr:call_worldm_hash(service_id, "rpc_agent_heartbeat", body)
    if not ok or qfailed(code) then
        response.code = RESPONSE_CODE_RPC_ERROR
    else
        response.data = result
    end
    return json_encode(response)
end

quanta.report_mgr = ReportMgr()

return ReportMgr
