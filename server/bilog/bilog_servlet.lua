-- bilog_servlet.lua
local log_err               = logger.err
local log_debug             = logger.debug
local tsize                 = qtable.size
local bi_dao                = quanta.get("bi_dao")
local event_mgr             = quanta.get("event_mgr")
local config_mgr            = quanta.get("config_mgr")
local safe_agent            = quanta.get("safe_agent")
local protobuf_mgr          = quanta.get("protobuf_mgr")
local bilog_mgr             = quanta.get("bilog_mgr")

local FRAME_SUCCESS         = protobuf_mgr:error_code("FRAME_SUCCESS")
local FRAME_FAILED          = protobuf_mgr:error_code("FRAME_FAILED")
local SAFE_REJECT_ERR       = protobuf_mgr:error_code("SAFE_REJECT_ERR")
local language_db           = config_mgr:init_table("language", "lang")

local BilogServlet = singleton()
function BilogServlet:__init()
    -- 事件监听
    event_mgr:add_listener(self, "rpc_safe_check")
    event_mgr:add_listener(self, "rpc_safe_check_decision")
    -- 查询留存
    event_mgr:add_listener(self, "rpc_bgip_queryren")
end

--查询留存
function BilogServlet:rpc_bgip_queryren(year, month, day, diff_day)
    log_debug("[BilogServlet][rpc_bgip_queryren] year:{},month:{} day:{}, diff:{}", year, month, day, diff_day)
    local _, ren_rate = bi_dao:statis_day(year, month, day, diff_day)
    return FRAME_SUCCESS, {msg = ren_rate}
end

--内容安全检查
function BilogServlet:rpc_safe_check(user_id, text, msg_type, replace, extension)
    log_debug("[BilogServlet][rpc_safe_check] user_id:{},replace:{} text:{}, msg_type:{}", user_id, replace, text, msg_type)
    local user = bilog_mgr:get_user(user_id)
    if not user then
        log_err("[BilogServlet][rpc_safe_check] user_id({}) not found", user_id)
        return FRAME_FAILED
    end
    local ip = user.ip
    local lang = user.lang
    local dev_plat = user.dev_plat
    return self:safe_check(ip, lang, replace or false, text, dev_plat, msg_type, extension)
end

--带决策内容安全检查
function BilogServlet:rpc_safe_check_decision(user_id, text, msg_type, extension)
    log_debug("[BilogServlet][rpc_safe_check_decision] user_id:{}, text:{}, msg_type:{}", user_id, text, msg_type)
    local user = bilog_mgr:get_user(user_id)
    if not user then
        log_err("[BilogServlet][rpc_safe_check_decision] user_id({}) not found", user_id)
        return FRAME_FAILED
    end
    local ip = user.ip
    local lang = user.lang
    local dev_plat = user.dev_plat
    return self:safe_check_decision(ip, lang, text, dev_plat, msg_type, extension)
end

--内部调用
------------------------------------------------

function BilogServlet:check(ip, language, text, dev_plat, msg_type, extension)
    --语言转码为内容安全接口值
    local lang_safe = language_db:find_one(language)
    --兜底语言
    local lang_safe_num = 1
    if lang_safe then
        lang_safe_num = lang_safe.lang_safe
    end
    local ok, res = safe_agent:bytedance_safe_check(ip, lang_safe_num, text, dev_plat, msg_type, extension)
    if not ok then
        return FRAME_FAILED
    end
    return FRAME_SUCCESS, res
end

--内容安全检查
function BilogServlet:safe_check(ip, language, replace, text, dev_plat, msg_type, extension)
    log_debug("[BilogServlet][safe_check] ip:{}, language:{}, text:{}, dev_plat:{}", ip, language, text, dev_plat)
    local ok, res = self:check(ip, language, text, dev_plat, msg_type, extension)
    if ok ~= FRAME_SUCCESS then
        return FRAME_FAILED
    end
    log_debug("[BilogServlet][safe_check] res:{}", res)
    --非替换
    if not replace then
        --检查是否包含敏感字(check_code=pass但仍然会有wordMatches)
        for _, word in pairs(res.data.contents) do
            if tsize(word.wordIndexes) > 0 then
                return SAFE_REJECT_ERR
            end
        end
        return FRAME_SUCCESS, res.data.contents[1].content
    end
    --替换
    return FRAME_SUCCESS, res.data.contents[1].filteredText
end

function BilogServlet:safe_check_decision(ip, language, text, dev_plat, msg_type, extension)
    log_debug("[BilogServlet][safe_check_decision] ip:{}, language:{}, text:{}, dev_plat:{}", ip, language, text, dev_plat)
    local ok, res = self:check(ip, language, text, dev_plat, msg_type, extension)
    if ok ~= FRAME_SUCCESS then
        return FRAME_FAILED
    end
    log_debug("[BilogServlet][safe_check_decision] res:{}", res)
    return FRAME_SUCCESS, res
end

quanta.bilog_servlet = BilogServlet()

return BilogServlet
