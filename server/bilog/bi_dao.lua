--bi_dao.lua
import("agent/mongo_agent.lua")

local log_err       = logger.err
local qfailed       = quanta.failed
local os_date       = os.date
local os_time       = os.time
local log_debug     = logger.debug
local json_encode   = json.encode
local sformat       = string.format

local mongo_agent   = quanta.get("mongo_agent")

local BiDao = singleton()

function BiDao:__init()
end

function BiDao:load_total_player()
    local ok, code, adata = mongo_agent:count({"player"})
    if qfailed(code, ok) then
        log_err("[BiDao][load_total_player] find failed! code: {}, res: {}", code, adata)
        return false
    end
    return true, adata
end

--登录日志
function BiDao:login_log(player_id, create_time, login_time)
    local date = os_date("%Y-%m-%d", quanta.now)
    --是否已有数据
    local ok, code, data = mongo_agent:find_one({"player_login_log", {["player_id"] = player_id, ["date"] = date}})
    log_err("[BiDao][login_log] ok:{} code: {}, data: {}", ok, code, data)
    if qfailed(code, ok) then
        log_err("[BiDao][login_log] find failed! code: {}, res: {}", code, data)
        return
    end
    if not data then
        --新增
        local insert_data = {["date"]=date, ["player_id"] = player_id, ["create_time"]=create_time, ["login_time"] = login_time }
        ok, code, data = mongo_agent:insert({"player_login_log", insert_data})
        if qfailed(code, ok) then
            log_err("[BiDao][login_log] insert failed! code: {}, res: {}", code, data)
            return
        end
        return
    end
    --修改
    local udata = { ["$set"] = {["login_time"] = login_time} }
    ok, code, data = mongo_agent:update({"player_login_log", udata, {["player_id"] = player_id , ["date"] = date}, true})
    if qfailed(code, ok) then
        log_err("[BiDao][login_log] update failed! code: {}, res: {}", code, data)
        return
    end
end

--统计指定日期次日留存
function BiDao:statis_day1(year, month, day)
    return self:statis_day(year, month, day, 1)
end
--统计指定日期diff_day(1-次日 3-3日 7-7日)留存
function BiDao:statis_day(year, month, day, diff_day)
    local target_date = self:get_date_of(year, month, day, 0)
    local start_time, end_time = self:get_start_end(year, month, day)
    --统计当日新增
    local target_selecter = {
                                ["$and"] = {
                                            { date = target_date},
                                            { create_time = {["$lte"] = end_time } },
                                            { create_time = {["$gt"] = start_time } }
                                        },
                            }

    local ok, code, total_num = mongo_agent:count({"player_login_log",target_selecter})
    if qfailed(code, ok) then
        log_err("[BiDao][statis_day] find failed! code: {}, res: {}", code, total_num)
        return
    end
    log_err("target_date:{} total_register_num:{}", target_date, total_num)
    if not total_num then
        total_num = 0
    end

    --统计当日新增次日登录过的用户数量
    local next_date = self:get_date_of(year, month, day, diff_day)
    local next_year, next_month, next_day = next_date:match("(%d+)-(%d+)-(%d+)")
    local next_start_time, next_end_time = self:get_start_end(next_year, next_month, next_day)

    target_selecter = {
        ["$and"] = {
                    { date = next_date },
                    { login_time = {["$lte"] = next_end_time } },
                    { login_time = {["$gt"] = next_start_time } },
                    { create_time = {["$lte"] = end_time } },
                    { create_time = {["$gt"] = start_time } }
                },
    }

    local r_ok, r_code, r_active_num = mongo_agent:count({"player_login_log",target_selecter})
    if qfailed(r_code, r_ok) then
        log_err("[BiDao][statis_day] find failed! r_code: {}, r_active_num: {}", r_code, r_active_num)
        return
    end
    log_debug("[BiDao][statis_day] date:{} diff_day:{} register_num:{}, next_active_num:{}",target_date, diff_day, total_num, r_active_num)
    local rate = sformat("%.2f", r_active_num * 100 / total_num)
    if total_num <= 0 then
        rate = sformat("%.2f", 0)
    end
    local return_table = {
        date = target_date,
        type = sformat("%s-day", diff_day),
        register_num = total_num,
        retention_num = r_active_num,
        retention_rate = sformat("%s%%",rate)
    }
    return json_encode(return_table), return_table.retention_rate
end

--获取指定日期的前后日期
function BiDao:get_date_of(year, month, day, diff_day)
    local date = os_date("*t", os.time{year=year, month=month, day=day, hour = 23, min = 59, sec = 59})
    -- 计算指定日期的时间戳
    local timestamp = os_time(date)
    -- 偏移时间戳
    local next_day_timestamp = timestamp + diff_day * 24 * 60 * 60
    -- 格式化为日期字符串
    local next_day_date = os_date("%Y-%m-%d", next_day_timestamp)
    log_debug("[BiDao][get_date_of] year:{} month:{}, day:{} diff_day:{} next_day_date:{}", year, month, day, diff_day, next_day_date)
    return next_day_date
end

--获取指定日期的开始时间和结束时间
function BiDao:get_start_end(year, month, day)
    local start_time = os_time({year = year, month = month, day = day, hour = 0, min = 0, sec = 0})
    local end_time = os_time({year = year, month = month, day = day, hour = 23, min = 59, sec = 59})
    log_debug("[BiDao][get_start_end] year:{} month:{}, day:{} start_time:{} end_time:{}", year, month, day, start_time, end_time)
    return start_time, end_time
end

quanta.bi_dao = BiDao()

return BiDao
