-- bilog_mgr.lua
-- 本模块维护了字节统一日志输出 注:这里的role 角色代表SDK账号
local RpcServer         = import("network/rpc_server.lua")
local Queue             = import("container/queue.lua")

local pairs             = pairs
local os_time           = os.time
local os_date           = os.date
local mrandom           = math.random
local log_err           = logger.err
local log_info          = logger.info
local log_debug         = logger.debug
local sformat           = string.format
local tsize             = qtable.size
local tcopy             = qtable.copy
local json_encode       = json.encode
local env_addr          = environ.addr
local guid_string       = codec.guid_string
local service, index    = quanta.service, quanta.index
local http_client       = quanta.get("http_client")

local protobuf_mgr      = quanta.get("protobuf_mgr")
local event_mgr         = quanta.get("event_mgr")
local update_mgr        = quanta.get("update_mgr")
local bi_dao            = quanta.get("bi_dao")

local APP_ID            = environ.number("QUANTA_APP_ID")
local BILOG_PATH        = environ.get("QUANTA_BILLOG_PATH")
local BILOG_TIME        = environ.get("QUANTA_BILLOG_TIME")
local REGION            = environ.number("QUANTA_REGION")
local CLUSTER           = environ.number("QUANTA_CLUSTER")
local REPORT_URL        = sformat("%s/user_stat", environ.get("QUANTA_REPORT_URL"))

local MINUTE_5_S        = quanta.enum("PeriodTime", "MINUTE_5_S")
local log_dump          = logfeature.dump("bilogs", BILOG_PATH, true, BILOG_TIME)

local DEF_POSTION_JSON  = '{"x":0, "y":0, "z":0}'

local BilogMgr = singleton()
local prop = property(BilogMgr)
prop:reader("users", {})
prop:reader("roles", {})
prop:reader("lobby", {})
prop:reader("rpc_server", nil)
prop:reader("offline_queue", nil)     --已下线玩家

function BilogMgr:__init()
    --创建rpc服务器
    local ip, port = env_addr("QUANTA_BILOG_HOST")
    self.rpc_server = RpcServer(self, ip, port)
    -- 注册事件
    event_mgr:add_listener(self, "rpc_update_user")
    event_mgr:add_listener(self, "rpc_bilog")
    event_mgr:add_listener(self, "rpc_update_player")
    event_mgr:add_listener(self, "rpc_logout_player")
    event_mgr:add_listener(self, "rpc_server_player_cnt")
    update_mgr:attach_minute(self)
    update_mgr:attach_hour(self)
    self.offline_queue = Queue("user_id")
end

function BilogMgr:on_client_accept(client)
end

-- 心跳
function BilogMgr:on_client_beat(client)
end

function BilogMgr:on_client_register(client, node)
    log_info("[BilogMgr][on_client_register] node:{}", client.name)
end

-- 会话关闭回调
function BilogMgr:on_client_error(client, token, err)
    log_info("[BilogMgr][on_client_error] node:{}", client.name)
end

-- 公共用户参数
function BilogMgr:public_role_params(user, user_id, role_id)
    return {
        user_id = user_id,
        role_login_time = 0,
        --公参
        log_params = {
            role_level = 0,
            role_name = '',
            total_cost = 0,
            app_id = APP_ID,
            role_id = role_id,
            server_id = user.server_id,
            create_role_time = 0,
            device_id = user.device_id,
            user_unique_id = user.open_id,
        }
    }
end

-- 测试打印
function BilogMgr:print(role_id)
    log_info("------------------------------[BilogMgr][print] start----------------------------")
    -- 遍历更新
    local data = self.roles[role_id]
    if not data then
        log_err("[BilogMgr][print] role_id:{} load failed", role_id)
        return
    end
    log_debug("[BilogMgr][print] data:{}", data)
    log_info("------------------------------[BilogMgr][print] end----------------------------")
end

-- rpc协议处理
------------------------------------------------------------------------------
--用户更新
function BilogMgr:rpc_update_user(user_id, device_id, open_id, is_user_first,
                                    server_id, ip, lang, dev_plat)
    local data = {  device_id = device_id, open_id = open_id,
                    is_user_first = is_user_first, server_id = server_id,
                    ip = ip, lang = lang,
                    dev_plat = dev_plat
                }
    self.users[user_id] = data
    self.users[user_id].players = {}
    if self.offline_queue:find(user_id) then
        self.offline_queue:remove_by_index(user_id)
    end
    log_debug("[BilogMgr][rpc_update_user] user_id:{}, device_id:{}, open_id:{} is_user_first:{} ip:{}, lang:{}, dev_plat:{}",
        user_id, device_id, open_id, is_user_first, ip, lang, dev_plat)
end

-- 角色更新
function BilogMgr:rpc_update_player(user_id, role_id, player)
    log_debug("[BilogMgr][rpc_update_player] role_id: {} attribute update!", role_id)
    if self.offline_queue:find(user_id) then
        self.offline_queue:remove_by_index(user_id)
    end
    local data = self.roles[role_id]
    if not data then
        local user = self.users[user_id]
        if not user then
            log_err("[BilogMgr][rpc_update_player] failed! cant find user_id:{}", user_id)
            return
        end
        data = self:public_role_params(user, user_id, role_id)
        user.players[role_id] = data
        self.roles[role_id] = data
    end
    -- 遍历更新
    for key, value in pairs(player) do
        if data[key] ~= nil then
            data[key] = value
        end
        -- 更新公参
        if data.log_params[key] ~= nil then
            data.log_params[key] = value
            log_debug("[BilogMgr][rpc_update_player] update key:{} value:{}", key, value)
        end
    end
    self:print(role_id)
end

--获取用户信息
function BilogMgr:get_user(user_id)
    return self.users[user_id]
end


--真实下线
function BilogMgr:offline(user_id, role_id)
    log_debug("[BilogMgr][offline] user_id:{} role_id:{}", user_id, role_id)
    self.roles[role_id] = nil
    local user = self.users[user_id]
    if not user then
        log_err("[BilogMgr][rpc_logout_player] cant found user_id:{} role_id:{}", user_id, role_id)
        return
    end
    user.players[role_id] = nil
    --如果角色全部退出则清空users
    if tsize(user.players) <= 0 then
        self.users[user_id] = nil
    end
end

-- 角色退出
function BilogMgr:rpc_logout_player(user_id, role_id)
    log_debug("[BilogMgr][rpc_logout_player] role_id: {} log_out", role_id)
    --需要延迟>=login_token有效期清理 否则会出现账号登陆后一直不点进入游戏导致数据被清理
    self.offline_queue:push_back({user_id = user_id, role_id = role_id, offline_time = quanta.now + MINUTE_5_S})
end

-- 服务器在线
function BilogMgr:rpc_server_player_cnt(client, lobby_id, cur_player_cnt, max_player_cnt, min_player_cnt)
    --记录lobby数据
    self.lobby[lobby_id] = {cur_player_cnt = cur_player_cnt, max_player_cnt = max_player_cnt, min_player_cnt = min_player_cnt};
    log_info("[BilogMgr][rpc_server_player_cnt] lobby_id:{}, cur:{}, max:{}, min:{}", index, cur_player_cnt, max_player_cnt, min_player_cnt)
end

--日志格式转换
function BilogMgr:convert_log_format(log_data)
    --转为字符
    log_data.role_id = tostring(log_data.role_id)
    return log_data
end

--上报虚拟上下线
function BilogMgr:report_virtual_login_out()
    log_debug("[BilogMgr][report_virtual_login_out]...")
    local pre_time, today_time = self:get_virtual_time()
    for _, role in pairs(self.roles) do
        --离线信息
        local offline_data = {
            is_real = 0,
            total_dur = 0,
            time = pre_time,
            coin1 = role.coin1,
            coin2 = role.coin2,
            position = DEF_POSTION_JSON,
            duration = quanta.now - role.role_login_time,
            uuid = guid_string(service, index),
            event = "role_logout"
        }
        tcopy(role.log_params, offline_data)
        offline_data = self:convert_log_format(offline_data)
        log_dump(json_encode(offline_data))
        --上线信息
        local online_data = {
            is_real = 0,
            guild_id = 0,
            total_friends = 0,
            time = today_time,
            coin1 = role.coin1,
            coin2 = role.coin2,
            uuid = guid_string(service, index),
            event = "role_login_success"
        }
        tcopy(role.log_params, online_data)
        online_data = self:convert_log_format(online_data)
        log_dump(json_encode(online_data))

        --重置角色上线时间
        role.role_login_time = today_time
        --记录留存数据
        self:record_retention(role.log_params.role_id)
        log_debug("[BilogMgr][report_virtual_login_out] role:{}", role.log_params.role_id)
    end
end

--获取虚拟上报时间
function BilogMgr:get_virtual_time()
    local temp_date = os_date("*t")
    local today_time = os_time({year = temp_date.year, month = temp_date.month, day = temp_date.day, hour = 0, min = 1, sec = 0})
    --前推1小时肯定是前一天
    local pre_date = os_date("*t", today_time - 60*1000)
    local pre_time = os_time({year = pre_date.year, month = pre_date.month, day = pre_date.day, hour = 23, min = 59, sec = 0})
    return pre_time, today_time
end

--上报服务器在线
function BilogMgr:report_server_user_cnt()
    local total_average_user_cnt = 0
    local total_peak_user_cnt = 0
    local total_user_cnt = 0
    for k, v in pairs(self.lobby) do
        --计算平均值
        local average_user_cnt = (v.max_player_cnt + v.min_player_cnt) / 2
        total_average_user_cnt = total_average_user_cnt + average_user_cnt
        total_peak_user_cnt = total_peak_user_cnt + v.max_player_cnt
        total_user_cnt = total_user_cnt + v.cur_player_cnt
    end
    --统计值
    local log_data = {
        event                   = "server_user_cnt",
        user_cnt                = total_user_cnt,
        average_user_cnt        = total_average_user_cnt,
        peak_user_cnt           = total_peak_user_cnt,
        local_server_time_ms    = quanta.now_ms,
        time                    = quanta.now,
        server_id               = REGION,
        user_unique_id          = sformat("%d___", mrandom(0, 1000)),
        app_id                  = APP_ID,
        uuid                    = guid_string(service, index),
        object_id               = "1",
        cluster_id              = CLUSTER
    }
    --重置
    self.lobby = {}

    log_dump(json_encode(log_data))

    --通知字节agent
    --总注册玩家数
    local ok, data = bi_dao:load_total_player()
    local total_player_cnt
    if not ok then
        total_player_cnt = 0
    else
        total_player_cnt = data
    end

    local devops_data = {
        server_id               = REGION,
        user_online             = total_user_cnt,
        user_total              = total_player_cnt,
        object_id               = "1",
    }
    log_debug("[BilogMgr][report_server_user_cnt] report_url:{} devops_data:{}", REPORT_URL, devops_data)
    http_client:call_post(REPORT_URL, devops_data)
end

-- 分钟定时器
function BilogMgr:on_minute()
    self:report_server_user_cnt()
    --玩家下线
    local cur_time = quanta.now
    local info = self.offline_queue:head()
    while info do
        if info.offline_time > cur_time then
            break
        end
        self.offline_queue:pop_front()
        self:offline(info.user_id, info.role_id)
        info = self.offline_queue:head()
    end
end

-- 小时定时器
function BilogMgr:on_hour()
    --判定是否为0点
    local timetable = os_date("*t", os_time());
    if timetable.hour == 0 then
        self:report_virtual_login_out()
    end
end


-- 角色退出
function BilogMgr:role_logout(role_id, log_data, is_real, duration, coin1, coin2, total_dur, position)
    log_data.is_real    = is_real
    log_data.duration   = duration
    log_data.coin1      = coin1
    log_data.coin2      = coin2
    log_data.total_dur  = total_dur
    log_data.position   = position
end

--采集流水
function BilogMgr:collect_flow(role_id, log_data, collection_type,collection_id,
                                collection_uid, tool_item_id, item_list, game_scene_id,
                                position, collect_duration, event_seq)
        log_data.collection_type    = collection_type
        log_data.collection_id      = collection_id
        log_data.collection_uid     = collection_uid
        log_data.tool_item_id       = tool_item_id
        log_data.item_list          = item_list
        log_data.game_scene_id      = game_scene_id
        log_data.position           = position
        log_data.collect_duration   = collect_duration
        log_data.event_seq          = event_seq
end

--建造完成度流水
function BilogMgr:building_stage_flow(role_id, log_data, building_type, building_id, building_uid, building_level,
                                     building_time, building_item, event_seq, real_time)

        log_data.building_type      = building_type
        log_data.building_id        = building_id
        log_data.building_uid       = building_uid

        log_data.building_time      = building_time
        log_data.building_item      = building_item



        log_data.event_seq          = event_seq
        log_data.real_time          = real_time or 0
        log_data.building_level     = building_level
end

--生产道具流水
function BilogMgr:produce_item_flow(role_id, log_data, action, machine_level, item_type, produce_item_id,
                                    produce_num, speedup_type, coin1_use, produce_time,
                                    required_item, event_seq, real_time)
        log_data.action             = action
        log_data.machine_level      = machine_level
        log_data.item_type          = item_type
        log_data.produce_item_id    = produce_item_id
        log_data.produce_num        = produce_num
        log_data.speedup_type       = speedup_type
        log_data.coin1_use          = coin1_use
        log_data.produce_time       = produce_time
        log_data.required_item       = required_item
        log_data.event_seq          = event_seq
        log_data.real_time          = real_time
end

-- 捏脸埋点
function BilogMgr:character_building(role_id, log_data, gender, custom)
    log_data.gender = gender
    local role_custom = protobuf_mgr:decode_byname("ncmd_cs.rolemodel", custom)
    log_data.color = role_custom.color
    log_data.face_type = role_custom.head
end

-- 角色登陆
function BilogMgr:role_login_success(role_id, log_data, is_real, guild_id, total_friends, coin1, coin2, total_dur, user_id)
    --判定是否首次登陆
    local is_user_first
    if self.users[user_id] and self.users[user_id].is_user_first == 1 then
        is_user_first = 1
    else
        is_user_first = 0
    end
    log_data.is_user_first  = is_user_first
    log_data.is_real        = is_real
    log_data.coin2          = coin2
    log_data.total_dur      = total_dur
    log_data.guild_id       = guild_id
    log_data.total_friends  = total_friends
    log_data.coin1          = coin1

    --更新bilog对象
    self.roles[role_id].role_login_time = quanta.now

    --记录留存数据
    self:record_retention(role_id)
end

--记录留存历史数据
function BilogMgr:record_retention(role_id, extra)
    local create_time = self.roles[role_id].log_params.create_role_time
    local login_time = self.roles[role_id].role_login_time or quanta.now
    bi_dao:login_log(role_id, create_time ,login_time)
end

--创建角色
function BilogMgr:create_role_success(role_id, log_data, is_user_first)
    log_data.is_user_first = is_user_first
end

--货币流水
function BilogMgr:money_flow(role_id, log_data, coin_type, action, amount, after_amount, reason, event_seq, sub_reason)
    log_data.coin_type      = coin_type
    log_data.action         = action
    log_data.amount         = amount
    log_data.after_amount   = after_amount
    log_data.reason         = reason
    log_data.event_seq      = event_seq
    log_data.sub_reason     = sub_reason
    --更新bilog对象
    if coin_type == 2 then
        self.roles[role_id].coin2 = after_amount
    else
        self.roles[role_id].coin1 = after_amount
    end
end

--经验流水
function BilogMgr:exp_flow(role_id, log_data, event_seq, exp_type, original_exp, current_exp, is_levelup,
                            original_level, current_level, total_time, reason, exp_amount)

    log_data.exp_type       = exp_type
    log_data.original_exp   = original_exp
    log_data.current_exp    = current_exp
    log_data.is_levelup     = is_levelup
    log_data.original_level = original_level
    log_data.current_level  = current_level
    log_data.total_time     = total_time
    log_data.exp_amount     = exp_amount
    log_data.reason         = reason
    log_data.event_seq      = event_seq
end



--道具流水
function BilogMgr:item_flow(role_id, log_data, event_seq, item_type, item_name, item_id, before_amount,
                            amount, remain_amount, action, sub_reason, item_effect_days, reason)
    log_data.event_seq          = event_seq
    log_data.item_type          = item_type
    log_data.item_name          = item_name
    log_data.item_id            = item_id
    log_data.before_amount      = before_amount
    log_data.amount             = amount
    log_data.remain_amount      = remain_amount
    log_data.action             = action
    log_data.sub_reason         = sub_reason
    log_data.item_effect_days   = item_effect_days
    log_data.reason             = reason
end

--商店流水
function BilogMgr:shop_flow(role_id, log_data, event_seq, shop_type, product_real_price, product_unit_price,
                            discount, product_type, product_total_price, product_name, product_id, product_count,
                            item_effect_days, price_type)
    log_data.event_seq              = event_seq
    log_data.shop_type              = shop_type
    log_data.product_real_price     = product_real_price
    log_data.product_unit_price     = product_unit_price
    log_data.discount               = discount
    log_data.product_type           = product_type
    log_data.product_total_price    = product_total_price
    log_data.product_name           = product_name
    log_data.product_id             = product_id
    log_data.product_count          = product_count
    log_data.item_effect_days       = item_effect_days
    log_data.price_type             = price_type
end

--任务流水
function BilogMgr:task_flow(role_id, log_data, event_seq, task_type, task_name, task_id,
                            task_status, real_time, task_sub_id, target_ids, target_items)
    log_data.event_seq = event_seq
    log_data.task_type = task_type
    log_data.task_name = task_name
    log_data.task_id   = task_id
    log_data.task_status = task_status
    log_data.real_time = real_time
    log_data.task_sub_id = task_sub_id
    log_data.target_ids = target_ids
    log_data.target_items = target_items
end

--邮件流水
function BilogMgr:mail_flow(role_id, log_data, title, content, id, mail_type, is_annex, status, annex_detail)
    log_data.mail_title = title
    log_data.mail_content = content
    log_data.mail_id = id
    log_data.mail_type = mail_type
    log_data.is_annex = is_annex
    log_data.status = status
    log_data.annex_detail = annex_detail
end

--签到流水
function BilogMgr:sign_in_flow(role_id, log_data, sign_id, sign_in_seq)
    log_data.sign_id = sign_id
    log_data.sign_in_seq = sign_in_seq
end

--解锁背包
function BilogMgr:unlock_package(role_id, log_data, unlock_package_space, after_package_space)
    log_data.unlock_package_space = unlock_package_space
    log_data.after_package_space = after_package_space
end

--npc招募
function BilogMgr:npc_recruit(role_id, log_data, pool_id,is_free, type, p_used_cnt, json_costs, json_result)
    log_data.pool_id = pool_id
    log_data.is_free = is_free
    log_data.type = type
    log_data.p_used_cnt = p_used_cnt
    log_data.json_costs = json_costs
    log_data.json_result = json_result
end

--解锁地皮
function BilogMgr:unlock_land(role_id, log_data, unlock_type, land_id, size, block_id)
    log_data.unlock_type = unlock_type
    log_data.land_id = land_id
    log_data.size = size
    log_data.block_id = block_id or 0
end

--战斗流水
function BilogMgr:battle_end_flow(role_id, log_data, action, enemy_id, weapon_id, game_scene_id, position)
    log_data.action = action
    log_data.enemy_id = enemy_id
    log_data.weapon_id = weapon_id
    log_data.game_scene_id = game_scene_id
    log_data.position = position
end

--npc派遣埋点
function BilogMgr:npc_dispatch(role_id, log_data, action, npc_id, npc_level, dispatch_type, detail_id)
    log_data.action = action
    log_data.npc_id = npc_id
    log_data.npc_level = npc_level
    log_data.dispatch_type = dispatch_type
    log_data.detail_id = detail_id
end

--成就流水
function BilogMgr:achievement_flow(role_id, log_data, achievement_id, achievement_level)
    log_data.achievement_id = achievement_id
    log_data.achievement_level = achievement_level
end

--npc养成埋点
function BilogMgr:npc_flow(role_id, log_data, action, npc_id, npc_reason, npc_level,reason,
                            original_exp,exp_amount, current_exp, is_levelup,
                            original_level, current_level)
    log_data.action = action
    log_data.npc_id = npc_id
    log_data.npc_reason = npc_reason
    log_data.npc_level = npc_level
    log_data.reason = reason
    log_data.original_exp = original_exp
    log_data.exp_amount = exp_amount
    log_data.current_exp = current_exp
    log_data.is_levelup = is_levelup
    log_data.original_level = original_level
    log_data.current_level = current_level
end

--社交埋点
function BilogMgr:social_flow(role_id, log_data, action, social_type, flow_uid, target_role_id, target_role_guild_id)
    log_data.action = action
    log_data.social_type = social_type
    log_data.flow_uid = flow_uid
    log_data.target_role_id = target_role_id
    log_data.target_role_guild_id = target_role_guild_id
end

--能量埋点
function BilogMgr:energy_flow(role_id, log_data, action, amount, after_amount, reason)
    log_data.amount = amount
    log_data.after_amount = after_amount
    log_data.reason = reason
    log_data.action = action
end

-- 日志输出
function BilogMgr:rpc_bilog(role_id, event, ...)
    local rpcParams = {...}
    log_debug("role_id: {} event:{} rpcParams:{}", role_id, event, rpcParams)
    local log_data = {
        time = quanta.now,
        uuid = guid_string(service, index),
        event = event
    }
    -- 公共参数
    local role = self.roles[role_id]
    if not role then
        log_err("[BilogMgr][rpc_bilog] log failed! cant found role_id :{}", role_id)
        return
    end
    local data = role.log_params
    tcopy(data, log_data)
    -- 生成数据
    self[event](self, role_id, log_data, ...)
    log_data = self:convert_log_format(log_data)
    log_dump(json_encode(log_data))
end

-- export
quanta.bilog_mgr = BilogMgr()

return BilogMgr
