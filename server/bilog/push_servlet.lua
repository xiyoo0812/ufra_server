-- push_servlet.lua
import("network/http_client.lua")

local sformat               = string.format
local log_debug             = logger.debug
local lhex_encode           = ssl.hex_encode
local lhmac_sha256          = ssl.hmac_sha256
local json_encode           = json.encode
local tinsert               = table.insert
local log_err               = logger.err

local http_client           = quanta.get("http_client")
local event_mgr             = quanta.get("event_mgr")
local CLUSTER               = environ.number("QUANTA_CLUSTER")
local APP_ID                = environ.number("QUANTA_APP_ID")
local SECRET_KEY            = environ.get("QUANTA_SECRET_KEY")
local ACCESS_KEY            = environ.get("QUANTA_ACCESS_KEY")
local SDK_EXPIRE_TIME       = environ.get("QUANTA_SDK_EXPIRE_TIME")
local SDK_SANDBOX           = environ.status("QUANTA_SDK_SANDBOX")
local PUSH_OPEN_ID_URL      = sformat("%spush/notify", environ.get("QUANTA_PUSH_URL"))
local PUSH_ROLE_ID_URL      = sformat("%spush/notify/role", environ.get("QUANTA_PUSH_URL"))
local PUSH_OPEN_ID_SANDBOX_URL   = sformat("%spush/notify", environ.get("QUANTA_PUSH_SANDBOX_URL"))
local PUSH_ROLE_ID_SANDBOX_URL   = sformat("%spush/notify/role", environ.get("QUANTA_PUSH_SANDBOX_URL"))


local PushServlet = singleton()
function PushServlet:__init()
    -- 事件监听
    event_mgr:add_listener(self, "rpc_push_sdk_id")
    event_mgr:add_listener(self, "rpc_push_role_id")
end

--根据sdk_open_id推送
function PushServlet:rpc_push_sdk_id(ids, template_id, push_arg, push_msg)
    log_debug("[PushServlet][rpc_push_sdk_id] ids:{},template_id :{} push_arg:{} push_msg:{}",
    ids, template_id, push_arg, push_msg)

    if #ids <= 0 then
        log_err("[PushServlet][rpc_push_sdk_id] push failed! ids size <=0")
    end

    push_msg.template_id = template_id
    local users = {}
    for _, sdk_open_id in pairs(ids) do
        tinsert(users, {sdk_open_id = tostring(sdk_open_id)})
    end
    local post_url = SDK_SANDBOX and PUSH_OPEN_ID_URL or PUSH_OPEN_ID_SANDBOX_URL
    local data = {
        push_arg = push_arg,
        push_msg = push_msg
    }
    data.users = users
    return self:push(post_url, data)
end

--根据角色ID推送
function PushServlet:rpc_push_role_id(ids, template_id, push_arg, push_msg)
    log_debug("[PushServlet][rpc_push_role_id] ids:{},template_id:{} push_arg:{} push_msg:{}",
    ids, template_id, push_arg, push_msg)
    if #ids <= 0 then
        log_err("[PushServlet][rpc_push_sdk_id] push failed! ids size <=0")
    end
    push_msg.template_id = template_id
    local users = {}
    for _, role_id in pairs(ids) do
        tinsert(users, {
            role = {
                server_id = CLUSTER,
                role_id = tostring(role_id)
            }})
    end
    local post_url = SDK_SANDBOX and PUSH_ROLE_ID_URL or PUSH_ROLE_ID_SANDBOX_URL
    local data = {
        push_arg = push_arg,
        push_msg = push_msg
    }
    data.users = users
    return self:push(post_url, data)
end

--推送
function PushServlet:push(post_url, data)
    data.push_arg.app_id = APP_ID
    local ts = quanta.now
    --table转json
    local json_data = json_encode(data)
    -- 计算签名
    local signKeyInfo = sformat("auth-v2/%s/%d/%d", ACCESS_KEY, ts, SDK_EXPIRE_TIME);
    local signKey = lhex_encode(lhmac_sha256(SECRET_KEY, signKeyInfo))
    local signature = lhex_encode(lhmac_sha256(signKey, json_data))
    local result = sformat("%s/%s", signKeyInfo, signature)

    -- 构造http请求
    local headers = {
        ["Content-type"] = "application/json",
        ["Agw-Auth"] = result,
        ["AppId"] = APP_ID
    }
    log_debug("[PushServlet][push] post_url:{}, json_data:{} header:{}", post_url, json_data, headers)
    local ok, status, res = http_client:call_post(post_url, json_data, headers)
    log_debug("[PushServlet][push] ok:{}, status:{} res:{}", ok, status, res)
    return ok and status or 404, res
end

quanta.push_servlet = PushServlet()

return PushServlet
