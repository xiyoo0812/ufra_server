--bi_gm.lua

local gm_agent      = quanta.get("gm_agent")
local bi_dao        = quanta.get("bi_dao")

local GLOBAL         = quanta.enum("GMType", "GLOBAL")

local BiGM    = singleton()
function BiGM:__init()
    --注册GM
    self:register()
end

--GM服务器
function BiGM:register()
    local cmd_list = {
        {
            name = "query_ren",
            gm_type = GLOBAL,
            group = "运维",
            desc = "查询留存数据",
            args = "year|integer month|integer day|integer diff_day|integer",
            example= "2024 01 03 1",
            tip = "查询2024年1月3日次(1 3 7)日留存 2024 01 03 1"
        },
    }

    --注册GM
    gm_agent:insert_command(cmd_list)
    --注册GM回调
    for _, v in ipairs(cmd_list) do
        gm_agent:add_listener(self, v.name)
    end
end

--gm停服
function BiGM:query_ren(year, month, day, diff_day)
    local json = bi_dao:statis_day(year, month, day, diff_day)
    return json
end

-- 导出
quanta.bi_gm = BiGM()

return BiGM
