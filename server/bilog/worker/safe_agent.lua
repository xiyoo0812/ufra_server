--safe_agent.lua
local log_err       = logger.err
local log_debug     = logger.debug
local sformat       = string.format

local scheduler     = quanta.get("scheduler")
local SAFE_WORKER   = environ.number("QUANTA_BILOG_WORKER", 2)

local SafeAgent    = singleton()
local prop = property(SafeAgent)
prop:reader("workers", {})
prop:reader("index", 0)

function SafeAgent:__init()
    --启动内容安全线程
    for i = 1, SAFE_WORKER do
        local name = sformat("safe%s", i)
        scheduler:startup(name, "bilog.worker.bilog")
        self.workers[i] = name
    end
end

function SafeAgent:choose_worker()
    local index = self.index + 1
    local worker = self.workers[index]
    self.index = index >= SAFE_WORKER and 0 or index
    return worker
end

--字节安全服务
function SafeAgent:bytedance_safe_check(ip, language, text, dev_plat, msg_type, extension)
    log_debug("[SafeAgent][bytedance_safe_check] ip:{}, language:{}, text:{}, dev_plat:{}", ip, language, text, dev_plat)
    local worker = self:choose_worker()
    local ok, status, res, code = scheduler:call(worker, "bytedance_safe_check", ip, language, text, dev_plat, msg_type, extension)
    if not ok or status >= 300 then
        log_err("[LoginAgent][safe_check] failed! err: {} status:{} res:{}", res, status, res)
        return false, ok and res, code
    end
    return true, res, code
end

quanta.safe_agent = SafeAgent()

return SafeAgent
