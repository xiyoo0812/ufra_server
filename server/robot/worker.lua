--worker.lua
local config_mgr = quanta.get("config_mgr")
config_mgr:init_merge_table("utensil", "utensil", "id")
config_mgr:init_merge_table("building", "utensil", "id")

--common includes
import("common/constant.lua")
--module includs
import("robot/module/object_factory.lua")
import("robot/module/scene.lua")
import("robot/module/navigation.lua")
import("robot/module/packet.lua")
import("robot/module/recruit.lua")
import("robot/module/ground.lua")
import("robot/module/gather.lua")
import("robot/module/produce.lua")
import("robot/module/packbench.lua")
import("robot/module/workshop.lua")
import("robot/module/building.lua")
import("robot/module/packbench.lua")
import("robot/module/partner.lua")
import("robot/module/produce.lua")

