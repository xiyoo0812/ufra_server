--recruit.lua
local Random                = import("basic/random.lua")
local log_debug             = logger.debug
local qrandom               = qtable.random
local tkarray               = qtable.tkarray
local tsize                 = qtable.size

local RecruitModule = mixin()
local prop = property(RecruitModule)
prop:reader("pool_infos", {})    --招募池信息(key-招募池ID value-详细信息)
prop:reader("recr_rand", nil)    --随机

function RecruitModule:__init()
    --免费/付费权重随机
    self.recr_rand = Random()
    self.recr_rand:add_wheel(false, 90)
    self.recr_rand:add_wheel(true, 10)

    -- 消息处理器
    self:register_doer("NID_RECR_POOL_NTF", self, "on_recr_pool_ntf")
end

function RecruitModule:on_recr_pool_ntf(msg)
    log_debug("[RecruitModule][on_recr_pool_ntf] robot:{},  msg:{}", self.open_id, msg)
    for pool_id, pool_info in pairs(msg.pool_infos) do
        self.pool_infos[pool_id] = pool_info
    end
end

function RecruitModule:random_ten()
    return qrandom({true, false}) == 1
end

function RecruitModule:random_free()
    --控制free权重
    local random_free = self.recr_rand:rand_wheel()
    log_debug("[RecruitModule][random_free] self.open_id:{} random_free:{}", self.open_id, random_free)
    return random_free;
end

function RecruitModule:random_pool()
    local random_pool
    if tsize(self.pool_infos) > 0 then
        random_pool = qrandom(tkarray(self.pool_infos))
    else
        random_pool = qrandom({1,2})
    end

    log_debug("[RecruitModule][random_pool] self.open_id:{} random_pool:{} ", self.open_id, random_pool)
    return random_pool
end


local Robot = import("robot/robot.lua")
Robot:delegate(RecruitModule)

return RecruitModule

