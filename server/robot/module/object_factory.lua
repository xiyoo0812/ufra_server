--object_factory.lua
local log_err           = logger.err
local tinsert           = table.insert
local config_mgr        = quanta.get("config_mgr")
local formula_db        = config_mgr:init_table("formula", "id")
local utensil_db        = config_mgr:init_table("utensil", "id")
local blueprint_db      = config_mgr:init_table("blueprint", "id")
local robot_build_db    = config_mgr:init_table("robot_building", "id")
local block_db          = config_mgr:init_table("block", "id")
local collect_points_db = config_mgr:init_table("collect_points", "id")

local ObjectFactory     = singleton()
local prop              = property(ObjectFactory)
prop:reader("level_limit_config", {})

function ObjectFactory:__init()
    self:init_limit_config()
end

function ObjectFactory:init_limit_config()
    self.level_limit_config = {}
    for _, conf in utensil_db:iterator() do
        if conf.level_limit ~= nil then
            local tmp = {}
            for lv, limit in pairs(conf.level_limit) do
                table.insert(tmp, { lv, limit })
            end
            table.sort(tmp, function(element1, element2)
                return element1[1] < element2[1]
            end)
            self.level_limit_config[conf.id] = tmp
        end
    end
end

---@param id number
---@param level number
function ObjectFactory:get_utensil_limit(id, level)
    local limit_conf = self.level_limit_config[id]
    if limit_conf == nil then --不限制
        return -1
    end
    local count = 0
    for _, info in ipairs(limit_conf) do
        if level < info[1] then
            return count
        end
        count = info[2]
    end
    return count
end

function ObjectFactory:find_utensil(proto_id)
    return utensil_db:find_one(proto_id)
end

function ObjectFactory:find_utensil_group(group_id, proto_id)
    for _, conf in utensil_db:iterator() do
        if conf.group == group_id then
            if conf.id == proto_id then
                return conf
            end
        end
    end
    return nil
end

function ObjectFactory:find_utensil_item(proto_id)
    local utensil = utensil_db:find_one(proto_id)
    if not utensil then
        return nil
    end
    return utensil.item_id
end

function ObjectFactory:find_formula(proto_id)
    local formula = formula_db:find_one(proto_id)
    if formula then
        return formula
    end
end

function ObjectFactory:find_blueprint(proto_id)
    local blueprint = blueprint_db:find_one(proto_id)
    if blueprint then
        return blueprint
    end
end

function ObjectFactory:find_blueprint_costs(proto_id)
    local blueprint = self:find_blueprint(proto_id)
    if not blueprint then
        log_err("[ObjectFactory][find_blueprint_costs] is fail formula is nil proto_id:{}", proto_id)
        return nil
    end
    local makings = {}
    for _, making in pairs(blueprint.makings) do
        makings[making[0]] = making[1]
    end
    return makings
end

function ObjectFactory:find_formula_costs(proto_id, count)
    local formula = self:find_formula(proto_id)
    if not formula then
        log_err("[ObjectFactory][find_formula_costs] is fail formula is nil proto_id:{} count:{}", proto_id, count)
        return nil
    end
    local costs = {}
    for item_id, item_count in pairs(formula.costs) do
        costs[item_id] = item_count * count
    end
    return costs
end

function ObjectFactory:find_robot_build(proto_id, index)
    for _, conf in robot_build_db:iterator() do
        if conf.proto_id == proto_id and conf.index == index then
            return conf
        end
    end
end

function ObjectFactory:find_blocks(ground_id)
    local result = {}
    for id, conf in block_db:iterator() do
        if conf.ground_id == ground_id then
            tinsert(result, id)
        end
    end
    return result
end

function ObjectFactory:find_block_costs(block_ids)
    local result = {}
    for _, block_id in pairs(block_ids) do
        local block = block_db:find_one(block_id)
        for item_id, item_count in pairs(block.costs or {}) do
            if not result[item_id] then
                result[item_id] = 0
            end
            result[item_id] = result[item_id] + item_count
        end
    end
    return result
end

-- 物品源
function ObjectFactory:find_collect_points(id)
    local points = collect_points_db:find_one(id)
    if not points then
        return nil
    end
    return points.formulas
end

-- export
quanta.object_fty = ObjectFactory()
return ObjectFactory
