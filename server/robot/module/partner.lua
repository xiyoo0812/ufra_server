--partner.lua
local log_debug     = logger.debug
local log_warn      = logger.warn
local tinsert       = table.insert
local tsize         = qtable.size

local object_fty    = quanta.get("object_fty")
local thread_mgr    = quanta.get("thread_mgr")
local UtensilType   = enum("UtensilType")

local PartnerModule = mixin()
local prop          = property(PartnerModule)
prop:reader("partners", {})
prop:reader("partner_houses", {})

function PartnerModule:__init()
    self:register_doer("NID_PARTNER_LIST_NTF", self, "on_partner_list_ntf")
end

function PartnerModule:on_partner_list_ntf(msg)
    if msg.master ~= self.player_id then
        return
    end
    if not msg.part_sync then
        self.partners = {}
    end
    for uuid, partner in pairs(msg.partners) do
        partner.id = uuid
        self.partners[uuid] = partner
        if partner.house_id and not self.partner_houses[partner.house_id] then
            self.partner_houses[partner.house_id] = {}
        end
        if partner.house_id then
            self.partner_houses[partner.house_id][partner.house_pos] = partner;
        end
    end
end

-- 未入驻的npc
function PartnerModule:not_house_partners()
    local result = {}
    for _, partner in pairs(self.partners) do
        if not partner.house_id or partner.house_id == 0 then
            tinsert(result, partner.id)
        end
    end
    return result
end

-- 分配空闲的npc
function PartnerModule:allot_idle_partners()
    local result = {}
    for _, partner in pairs(self.partners) do
        if (partner.house_id and partner.house_id ~= 0) and (not partner.building_id or partner.building_id == 0) then
            tinsert(result, partner)
        end
    end
    return result
end

-- 获取建筑上的npc数量
function PartnerModule:build_partners(build_uuid)
    local result = {}
    for _, partner in pairs(self.partners) do
        if partner.building_id == build_uuid then
            result[partner.building_pos] = partner
        end
    end
    return result
end

-- 派驻npc
function PartnerModule:send_build_partners(build_id, build_index)
    if not self:check_send_partner(build_id, build_index) then
        return
    end
    local build = self:get_build_by_proto(build_id, build_index)
    local utensil = object_fty:find_utensil(build.proto_id)
    local partners = self:allot_idle_partners()
    local build_partners = self:build_partners(build.id)
    local npc_num = utensil.npc_num
    if utensil.fn_type == UtensilType.PROD or utensil.fn_type == UtensilType.COMPANY then
        npc_num = 1
    end

    if tsize(build_partners) >= npc_num then
        log_debug("[PartnerModule][send_build_partners] tsize(build_partners)>npc_num build_id:{} count:{} npc_num:{}",
            build_id, tsize(build_partners), npc_num)
        return
    end

    log_debug("[PartnerModule][partner_send_req] begin partners:{} npc_num:{} count:{}",tsize(partners),npc_num, tsize(build_partners))
    for pos=1,npc_num do
        if build_partners[pos] then
            goto continue
        end
        for index,partner in pairs(partners) do
            local ok, res = self:call("NID_PARTNER_SEND_REQ", {
                id=partner.id,
                building_pos=pos,
                building_id = build.id
            })
            if self:check_callback(ok, res) then
                log_warn("[PartnerModule][partner_send_req] robot:{}, ok:{}, res:{} partner:{} pos:{} build:{}-{}",
                    self.open_id, ok, res, partner, pos, build_id, build.id)
                break
            end
            partners[index] = nil
            log_debug("[PartnerModule][partner_send_req] success robot:{}, ok:{}, res:{} partner:{} pos:{} build:{}-{}",
                self.open_id, ok, res, partner.id, pos, build_id, build.id)
            break
        end
        thread_mgr:sleep(200)
        :: continue ::
    end
end

-- 检查空闲的npc
function PartnerModule:check_idle_partner()
    for _, partner in pairs(self.partners) do
        if not partner.building_id or partner.building_id == 0 then
            return true
        end
    end
    return false
end

function PartnerModule:check_send_partner(build_id, build_index)
    local build = self:get_build_by_proto(build_id, build_index)
    if not build then
        log_warn("[PartnerModule][check_send_partner] build is nil build_id:{} build_index:{}", build_id, build_index)
        return false
    end

    local utensil = object_fty:find_utensil(build.proto_id)
    if not utensil then
        log_warn("[PartnerModule][check_send_partner] utensil is nil build_id:{}", build_id)
        return false
    end

    if not utensil.npc_num or utensil.npc_num <= 0 then
        log_warn("[PartnerModule][check_send_partner] npc_num is nil build_id:{} npc_num:{}", build_id, utensil.npc_num)
        return false
    end

    local build_partners = self:build_partners(build.id)
    if tsize(build_partners) >= utensil.npc_num then
        log_warn("[PartnerModule][check_send_partner] count>npc_num build_id:{} count:{} npc_num:{}", build_id, tsize(build_partners), utensil.npc_num)
        return false
    end

    local partners = self:allot_idle_partners()
    if tsize(partners) == 0 or tsize(partners) < utensil.npc_num  then
        log_warn("[PartnerModule][check_send_partner] partners is nil, partners:{} npc_num:{}", tsize(partners), utensil.npc_num)
        return false
    end

    local npc_num = utensil.npc_num
    if utensil.fn_type == UtensilType.PROD or utensil.fn_type == UtensilType.COMPANY then
        npc_num = 1
    end

    if tsize(build_partners) >= npc_num then
        log_debug("[PartnerModule][send_build_partners] tsize(build_partners)>npc_num build_id:{} count:{} npc_num:{}",
            build_id, tsize(build_partners), npc_num)
        return false
    end
    return true
end

-- 获取民居上的npc
function PartnerModule:get_house_partners(build_uuid)
    return self.partner_houses[build_uuid.building_id]
end

-- 民居npc的数量
function PartnerModule:house_partner_count(house_id)
    local count = 0
    for _, partner in pairs(self.partners) do
        if partner.house_id == house_id then
            count = count + 1
        end
    end
    return count
end

-- 民居最大npc数量
function PartnerModule:house_max_count(build_id, index)
    local build = self:get_build_by_proto(build_id, index)
    if not build then
        log_warn("[PartnerModule][house_max_count] build is nil build_id:{} index:{}", build_id, index)
        return 0
    end
    local utensil = object_fty:find_utensil(build.proto_id)
    if not utensil then
        log_warn("[PartnerModule][house_max_count] utensil is nil build_id:{}", build_id)
        return 0
    end
    return utensil.args
end

-- 民居剩余位置
function PartnerModule:house_last_count(build_id, index)
    local build = self:get_build_by_proto(build_id, index)
    if not build then
        log_warn("[PartnerModule][house_last_count] build is nil build_id:{} index:{}", build_id, index)
        return 0
    end
    local max_count = self:house_max_count(build_id, index)
    local partner_count = self:house_partner_count(build.id)
    local last_count = max_count - partner_count
    log_debug("[PartnerModule][house_last_count] build:{} max_count:{} partner_count:{} last_count:{}", build.proto_id,
        max_count, partner_count, last_count)
    return last_count
end

-- 检查民居npc是否已满
function PartnerModule:check_hourse_full(build_id, index)
    log_debug("[PartnerModule][check_hourse_full] build_id:{} index:{}", build_id, index)
    local last_count = self:house_last_count(build_id, index)
    if last_count > 0 then
        log_debug("[PartnerModule][check_hourse_full] false last_count:{}", last_count)
        return false
    end
    log_debug("[PartnerModule][check_hourse_full] true last_count:{}", last_count)
    return true
end

function PartnerModule:check_partner_count(build_id, index)
    log_debug("[PartnerModule][check_partner_count] build_id:{} index:{}", build_id, index)
    local last_count = self:house_last_count(build_id, index)
    local partners = self:not_house_partners()
    local count = tsize(partners)
    if count < last_count then
        return false
    end
    return true
end

function PartnerModule:enter_partner(build_id, index)
    log_debug("[PartnerModule][enter_partner] build_id:{} index:{}", build_id, index)
    local build = self:get_build_by_proto(build_id, index)
    if not build then
        log_warn("[PartnerModule][enter_partner] build is nil build_id:{} index:{}", build_id, index)
        return false
    end
    local partners = self:not_house_partners()
    if not partners or not next(partners) then
        log_warn("[PartnerModule][enter_partner] partners is nil build_id:{} index:{}", build_id, index)
        return false
    end
    log_debug("[PartnerModule][enter_partner] partners:{}", partners)
    log_debug("[PartnerModule][enter_partner] partner_build:{}", self.partner_houses[build.id])
    local max_count = self:house_max_count(build_id, index)
    for i = 1, max_count do
        if self.partner_houses[build.id] and self.partner_houses[build.id][i] then
            log_debug("[PartnerModule][enter_partner] build:{} pos:{}", build.id, i)
            goto continue
        end
        for k, partner_id in pairs(partners) do
            log_debug("[PartnerModule][enter_partner] partner:{} build:{} pos:{}", partner_id, build.id, i)
            self:call("NID_BUILDING_CHECKIN_REQ", { partner_id = partner_id, house_id = build.id, house_pos = i })
            partners[k] = nil
            break
        end
        :: continue ::
    end
end

local Robot = import("robot/robot.lua")
Robot:delegate(PartnerModule)
return PartnerModule
