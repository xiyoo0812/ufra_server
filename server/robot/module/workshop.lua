--workshop.lua
local log_err      = logger.err
local log_debug    = logger.debug
local tinsert      = table.insert

local object_fty   = quanta.get("object_fty")
local protobuf_mgr = quanta.get("protobuf_mgr")

local WorkShopModule     = mixin()
function WorkShopModule:__init()
end

function WorkShopModule:work_check_item(proto_id)
    local item_id = object_fty:find_utensil_item(proto_id)
    if item_id then
        return self:check_items({[item_id]=1})
    end
    return false
end

function WorkShopModule:work_add_item(proto_id)
    local item_id = object_fty:find_utensil_item(proto_id)
    if item_id then
        self:gm_add_item(item_id, 1)
        return true
    end
    return false
end

function WorkShopModule:work_find_detail(proto_id, index)
    local conf = object_fty:find_robot_build(proto_id, index or 1)
    if not conf then
        log_debug("[work_find_detail] conf is nil proto_id:{} index:{}", proto_id, index)
        return nil
    end
    local detail = conf.detail
    local bytes = protobuf_mgr:encode_byname("ncmd_cs.build_detail", detail)
    log_debug("[work_find_detail] proto_id:{} detail:{} bytes:{}", proto_id, detail, bytes)
    return bytes
end

-- 获取未解锁的地块
function WorkShopModule:work_not_unlock_block(proto_id, index)
    if not index then
        index = 1
    end
    local conf = object_fty:find_robot_build(proto_id, index)
    if not conf then
        log_err("[work_not_unlock_block] conf is nil proto_id:{} index:{}", proto_id, index)
        return nil
    end
    local ground_id = conf.ground_id
    local blocks = object_fty:find_blocks(ground_id)
    if not blocks then
        log_err("[work_not_unlock_block] conf is nil proto_id:{} index:{} ground_id:{}", proto_id, index, ground_id)
        return nil
    end
    if not next(blocks) then
        return nil
    end
    local result = {}
    for _,block_id in pairs(blocks) do
        if not self:check_blocks({block_id}) then
            tinsert(result, block_id)
        end
    end
    return result
end

local Robot = import("robot/robot.lua")
Robot:delegate(WorkShopModule)
return WorkShopModule
