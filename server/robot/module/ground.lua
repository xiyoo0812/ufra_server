--ground.lua
local log_debug   = logger.debug

local tinsert        = table.insert

local object_fty = quanta.get("object_fty")

local GroundModule = mixin()
local prop        = property(GroundModule)
prop:reader("grounds", {})
prop:reader("blocks", {})
function GroundModule:__init()
    self:register_doer("NID_BUILDING_GROUND_NTF", self, "on_ground_ntf")
    self:register_doer("NID_BUILDING_BLOCK_NTF", self, "on_block_ntf")
end

function GroundModule:on_ground_ntf(msg)
    log_debug("[GroundModule][on_ground_ntf] robot:{} msg:{}", self.open_id, msg)
    if msg.master ~= self.player_id then
        return
    end
    if not msg.part_sync then
        self.grounds = {}
        self.blocks = {}
    end
    for uuid, ground in pairs(msg.grounds) do
        self.grounds[uuid] = ground
        for block_id,time in pairs(ground.blocks) do
            self.blocks[block_id] = time
        end
    end
end

function GroundModule:on_block_ntf(msg)
    log_debug("[GroundModule][on_block_ntf] robot:{} msg:{}", self.open_id, msg)
    if msg.master ~= self.player_id then
        return
    end
    if not msg.part_sync then
        self.blocks = {}
    end
    for block_id, time in pairs(msg.blocks) do
        self.blocks[block_id] = time
    end
end

function GroundModule:check_blocks(block_ids)
    for _,block_id in pairs(block_ids) do
        if not self.blocks[block_id] then
            return false
        end
    end
    return true
end

function GroundModule:find_block_costs(block_ids)
    local result = {}
    for _,block_id in pairs(block_ids) do
        if not self.blocks[block_id] then
            tinsert(result, block_id)
        end
    end
    local costs = object_fty:find_block_costs(result)
    return costs
end

function GroundModule:check_block_costs(block_ids)
    local costs = self:find_block_costs(block_ids)
    if not costs or not next(costs) then
        return true
    end
    return false
end

function GroundModule:block_unlock(block_ids)
    log_debug("[block_unlock] block_ids:{}", block_ids)
    for _,block_id in pairs(block_ids) do
        if not self.blocks[block_id] then
            self:call("NID_BLOCK_UNLOCK_REQ", {block_id = block_id, cost=1})
        end
    end
end

function GroundModule:block_unlock_finish(block_ids)
    log_debug("[block_unlock] block_ids:{}", block_ids)
    for _,block_id in pairs(block_ids) do
        if not self.blocks[block_id] then
            self:call("NID_BLOCK_FINISH_REQ", {block_id = block_id})
        end
    end
end

local Robot = import("robot/robot.lua")
Robot:delegate(GroundModule)
return GroundModule
