--gather.lua
local log_debug             = logger.debug
local qrandom               = qtable.random
local config_mgr            = quanta.get("config_mgr")
local home_db               = config_mgr:init_table("home", "id")

local GatherModule = mixin()
local prop = property(GatherModule)
prop:reader("gather_target", nil) -- 目标
prop:reader("static_resource", {})  -- 静态资源

function GatherModule:__init()
    -- 配置处理
    for _, conf in home_db:iterator() do
        --静态资源
        if not conf.dynamic and conf.type == 4 then
            self.static_resource[conf.id] = conf
        end
    end

    -- 消息处理器
end

--寻找采集目标
function GatherModule:gather_drop()
    log_debug("[GatherModule][gather_drop] robot:{}", self.open_id)
    --先用随机
    local entity = self:random_resource()
    local pos = {x = entity.pos[1], y = entity.pos[2], z = entity.pos[3]}
    log_debug("[GatherModule][gather_drop] ready move to ({})", pos)
    self:set_nav_target(pos)
    local uuid = self:get_map_id() << 20 | entity.id
    local step = 1
    self.gather_target = {uuid = uuid, step = step}
end

--随机寻找一个资源
function GatherModule:random_resource()
    local id = qrandom(self.static_resource)
    local static_res =  self.static_resource[id]
    -- log_debug("[GatherModule][random_resource] static_res:{}", static_res)
    return static_res
end

local Robot = import("robot/robot.lua")
Robot:delegate(GatherModule)

return GatherModule

