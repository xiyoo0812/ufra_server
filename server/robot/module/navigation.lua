--navigation.lua
local log_debug             = logger.debug
local log_warn              = logger.warn
local qrandom_array         = qtable.random_array
local mrandom               = math.random
local mfloor                = math.floor
local tinsert               = table.insert

import("aisvr/core/nav_mgr.lua")
---@type nav_geometry
local nav_geometry          = import('common/nav_geometry.lua')

local protobuf_mgr          = quanta.get("protobuf_mgr")
---@type NavMgr
local nav_mgr               = quanta.get("nav_mgr")
local thread_mgr            = quanta.get("thread_mgr")

local MOVE_OVER_SPEED       = protobuf_mgr:error_code("SCENE_MOVE_OVER_SPEED")
local ACTION_MOVE           = quanta.enum("ActionType", "Move")
local AL_WALK               = quanta.enum("ActionLimit", "WALK")
local AL_RUN                = quanta.enum("ActionLimit", "RUN")
local AL_FASTRUN            = quanta.enum("ActionLimit", "FASTRUN")

local MOVE_SPEED_MAP = {
    [AL_WALK] = 600,
    [AL_RUN] = 1000,
    [AL_FASTRUN] = 1700,
}

local NavigationModule = mixin()
local prop = property(NavigationModule)
prop:reader("init_ms", {}) -- 行为同步需要的时间记录
prop:reader("nav_target", nil) -- 寻路目标
prop:reader("nav_info", nil) -- 寻路信息
prop:reader("move_tests",{
    {pos={z=82269,y=12455,x=127998},dir={z=0,y=9446,x=0},action_limit=1},
    {pos={z=82266,y=12458,x=128048},dir={z=0,y=9446,x=0},action_limit=2},
    {pos={z=82259,y=12457,x=128117},dir={z=0,y=9446,x=0},action_limit=2},
    {pos={z=82255,y=12457,x=128142},dir={z=0,y=9446,x=0},action_limit=2},
    {pos={z=82253,y=12457,x=128167},dir={z=0,y=9447,x=0},action_limit=2},
    {pos={z=82253,y=12457,x=128207},dir={z=0,y=9446,x=0},action_limit=2},
    {pos={z=82251,y=12457,x=128254},dir={z=0,y=9446,x=0},action_limit=2},
    {pos={z=82245,y=12457,x=128300},dir={z=0,y=9446,x=0},action_limit=2},
    {pos={z=82242,y=12457,x=128313},dir={z=0,y=9446,x=0},action_limit=2},
    {pos={z=82242,y=12457,x=128360},dir={z=0,y=9446,x=0},action_limit=2},
})

function NavigationModule:__init()
    -- 时间记录
    self.init_ms = quanta.now_ms
    -- 行为限制类型
    self.action_limit_list = { AL_WALK, AL_RUN, }
    -- 消息处理器
    self:register_doer("NID_ACTOR_ACTION_NTF", self, "on_actor_action_ntf")
end

function NavigationModule:on_actor_action_ntf(msg)
    log_debug("[NavigationModule][on_actor_action_ntf] robot:%s, id: %s", self.open_id, msg.entity_id)
    if msg.entity_id == self.player_id then
        self:update_pos(msg.pos)
    else
        local entity = self:find_entity(msg.id)
        if entity then
            entity:update_pos(msg.pos)
        end
    end
end

-- 移动请求
function NavigationModule:action_move_req(pos, dir_y, move_type)
    local payload = {
        entity_id = self.player_id,
        pos = { x = pos.x, y = pos.y, z = pos.z },
        dir = { x = 0, y = dir_y, z = 0 },
        timestamp = self:get_timestamp(),
        full_sync = true,
        action_limit = move_type,
        actions = { { type = ACTION_MOVE, move = { path = {}, }, }, },
    }
    local ok, res = self:call("NID_ACTOR_ACTION_REQ", payload)
    if self:check_callback(ok, res) then
        log_warn("[NavigationModule][action_move_req] robot:{}, ok={}, res={}", self.open_id, ok, res)
        if res.valid_pos and res.error_code == MOVE_OVER_SPEED then
            local nav_target = self.nav_target
            self:stop_move()
            self:update_pos(res.valid_pos)
            self:move(nav_target)
        end
        return false
    end
    log_debug("[NavigationModule][action_move_req] robot:{} success", self.open_id)
    return true
end

-- 移动测试
function NavigationModule:action_move_test(asc)
    if asc then
        for i=1,#self.move_tests do
            local point = self.move_tests[i]
            self:action_move_req(point.pos, point.dir.y, point.action_limit)
            thread_mgr:sleep(500)
        end
    else
        for i=#self.move_tests,1,-1 do
            local point = self.move_tests[i]
            self:action_move_req(point.pos, point.dir.y, point.action_limit)
            thread_mgr:sleep(500)
        end
    end
end

function NavigationModule:get_timestamp()
    return quanta.now_ms - self.init_ms
end

function NavigationModule:get_random_point(radius)
    if not radius then
        radius = self:get_random_radius()
    end
    return nav_mgr:around_point(self.map_id, { x = - self.pos_x, y = self.pos_y, z = self.pos_z }, radius)
end

function NavigationModule:get_random_radius()
    return mrandom(1, 40 * 100) -- 1cm ~ 40m
end

function NavigationModule:judge_position()
    local latest_point = self.nav_info.points[self.nav_info.pindex] -- 最近经过的点
    if not latest_point then
        -- 数据无效
        return nil
    end
    local now_ms = quanta.now_ms
    local time_pass = now_ms - self.nav_info.start_ms -- 距离开始移动后流逝的总时间
    local distance_pass = (time_pass / 1000) * self.nav_info.speed -- 开始移动后应该移动了的总距离
    :: check_next_segment ::
    if self.nav_info.pindex >= #self.nav_info.points then
        -- 已经到了终点
        self.nav_info.pos = latest_point
        self.nav_info.dir = self.nav_info.dirs[self.nav_info.pindex]
        return false
    end
    if self.nav_info.distances[self.nav_info.pindex + 1] > distance_pass then
        -- 当前段未走完
        self.nav_info.dir = self.nav_info.dirs[self.nav_info.pindex + 1]
        local distance = distance_pass  - self.nav_info.distances[self.nav_info.pindex]
        self.nav_info.pos = {
            x = latest_point.x + self.nav_info.dir.x * distance,
            y = latest_point.y + self.nav_info.dir.y * distance,
            z = latest_point.z + self.nav_info.dir.z * distance,
        }
        return true
    end
    self.nav_info.pindex = self.nav_info.pindex + 1 -- 段记录+1
    latest_point = self.nav_info.points[self.nav_info.pindex]
    goto check_next_segment
end

function NavigationModule:set_nav_target(target)
    if not target then
        local counter = 3 -- 查找随机位置，最多找3次
        local random_pos
        repeat
            counter = counter - 1
            random_pos = self:get_random_point()
        until random_pos or counter < 0
        if not random_pos then
            log_warn("[NavigationModule][set_nav_target] robot:{} get random point failed", self.open_id)
            return
        end
        target = { x = - mfloor(random_pos.x), y = mfloor(random_pos.y), z = mfloor(random_pos.z), }
    end
    self.nav_target = target
end

function NavigationModule:start_move()
    if not self.nav_target then
        log_warn("[NavigationModule][start_move] robot:{} nav target not exist", self.open_id)
        return
    end
    local start = { x = - self.pos_x, y = self.pos_y, z = self.pos_z, }
    local stop = { x = - self.nav_target.x, y = self.nav_target.y, z = self.nav_target.z, }
    local points = nav_mgr:find_path(self.map_id, start, stop)
    if not (points and #points >= 2) then
        log_warn("[NavigationModule][start_move] robot:{} find path failed", self.open_id)
        return
    end
    for idx, point in ipairs(points) do
        points[idx].x = - point.x
        points[idx].y = point.y
        points[idx].z = point.z
    end
    log_debug("[NavigationModule][start_move] robot:{} move from ({}) to ({}) with {} points", self.open_id, points[1], points[#points], #points)
    local move_type = qrandom_array(self.action_limit_list)
    local speed = MOVE_SPEED_MAP[move_type]
    local now_ms = quanta.now_ms
    -- 移动寻路信息
    self.nav_info = {
        move_type = move_type,
        speed = speed,
        points = points, -- 寻路路径
        pos = points[1], -- 当前位置
        dir = { x = 0, y = 0, z = 0, }, -- 当前朝向
        distances = {}, -- 每个点距离起始点的距离
        dirs = {}, -- 每个段的移动朝向
        pindex = 1, -- 当前路径点索引
        start_ms = now_ms, -- 当前时间
        update_ms = now_ms, -- 更新时间
    }
    -- 缓存每个点距离起始点的距离，当前朝向，加速计算
    local distance = 0
    local sum_distance = distance
    local dir = {x=0,y=0,z=0}
    for idx, point in ipairs(points) do
        if idx > 1 then
            distance = nav_geometry.calc_distance(point, points[idx - 1])
            sum_distance = sum_distance + distance
            dir = nav_geometry.calc_dir(points[idx - 1], point)
        end
        tinsert(self.nav_info.distances, sum_distance)
        tinsert(self.nav_info.dirs, dir)
    end
end

function NavigationModule:update_move_pos()
    if not self.nav_info then
        return
    end
    local has_next = self:judge_position()
    if has_next == nil then
        self:stop_move()
        return
    end
    -- local move_type = self.nav_info.move_type
    local move_type = AL_FASTRUN
    local pos = { x = mfloor(self.nav_info.pos.x), y = mfloor(self.nav_info.pos.y), z = mfloor(self.nav_info.pos.z), }
    local dir_y = mfloor(nav_geometry.dir_to_euler_angle(self.nav_info.dir) * 100)
    if not has_next then
        self:stop_move()
    end
    local success, err = self:action_move_req(pos, dir_y, move_type)
    if success then
        self:update_pos(pos)
    end
    return success, err
end

function NavigationModule:stop_move()
    self.nav_target = nil
    self.nav_info = nil
end

function NavigationModule:move(target)
    if not self.nav_target then
        self:set_nav_target(target)
    end
    if not self.nav_info then
        self:start_move()
    end
    return self:update_move_pos()
end

function NavigationModule:update_pos(pos)
    self.pos_x = pos.x
    self.pos_y = pos.y
    self.pos_z = pos.z
end

local Robot = import("robot/robot.lua")
Robot:delegate(NavigationModule)

return NavigationModule
