--packet.lua
local log_warn              = logger.warn
local log_debug             = logger.debug
local protobuf_mgr          = quanta.get("protobuf_mgr")

local PACKET_ITEM           = protobuf_mgr:enum("packet_type", "PACKET_ITEM")
local PACKET_BANK           = protobuf_mgr:enum("packet_type", "PACKET_BANK")

local PacketModule = mixin()
local prop = property(PacketModule)
prop:reader("packets", {})    --packets

function PacketModule:__init()
    self:register_doer("NID_PACKET_ITEM_SYNC_NTF", self, "on_item_sync_ntf")
    self:register_doer("NID_PACKET_ITEM_OBTAIN_NTF", self, "on_item_obtain_ntf")
end

function PacketModule:on_item_sync_ntf(msg)
    log_debug("[PacketModule][on_item_sync_ntf] robot:{} msg: {}", self.open_id, msg)
    local packet_id = msg.packet_id
    if msg.full_sync or not self.packets[packet_id] then
        self.packets[packet_id] = { items = msg.items, capacity = msg.capacity }
        return
    end
    if msg.capacity > 0 then
        self.packets[packet_id].capacity = msg.capacity
    end
    for item_id, num in pairs(msg.items) do
        self.packets[packet_id].items[item_id] = num
    end
end

function PacketModule:on_item_obtain_ntf(msg)
    -- log_debug("[PacketModule][on_item_obtain_ntf] robot:{}, id: {}", self.open_id, msg)
end

function PacketModule:drop_item_req()
    local drop_items = {}
    for _, packet in pairs(self.packets) do
        for _, item in pairs(packet) do
            drop_items = { item.uuid }
            break
        end
    end
    if next(drop_items) then
        local ok, res = self:call("NID_PACKET_ITEM_DROP_REQ", drop_items)
        if self:check_callback(ok, res) then
            log_warn("[PacketModule][drop_item_req] robot:{}, ok={}, res={}", self.open_id, ok, res)
            return false
        end
        log_debug("[PacketModule][drop_item_req] robot:{} success", self.open_id)
    end
    return true
end

function PacketModule:sort_item_req()
    local ok, res = self:call("NID_PACKET_ITEM_SORT_REQ", { packet_id = 1 })
    if self:check_callback(ok, res) then
        log_warn("[PacketModule][sort_item_req] robot:{}, ok={}, res={}", self.open_id, ok, res)
        return false
    end
    log_debug("[PacketModule][sort_item_req] robot:{} success", self.open_id)
    return true
end

function PacketModule:unlock_bag_req()
    local capacity = self.packets[1].capacity
    if capacity < 60 then
        local ok, res = self:call("NID_PACKET_GRID_UNLOCK_REQ", { packet_id = 1, count = capacity + 1 })
        if self:check_callback(ok, res) then
            log_warn("[PacketModule][unlock_bag_req] robot:{}, ok={}, res={}", self.open_id, ok, res)
            return false
        end
        log_debug("[PacketModule][unlock_bag_req] robot:{} success", self.open_id)
    end
    return true
end

function PacketModule:swap_item_req()
    local uuid, src_pack_id = 0, 0
    for id, packet in pairs(self.packets) do
        for _, item in pairs(packet) do
            src_pack_id = id
            uuid = item.uuid
            break
        end
    end
    if uuid > 0 then
        local tar_pack_id = src_pack_id == 1 and 2 or 1
        local ok, res = self:call("NID_PACKET_ITEM_MOVE_REQ", { uuid = uuid, tar_pos = 1, tar_packet_id = tar_pack_id })
        if self:check_callback(ok, res) then
            log_warn("[PacketModule][swap_item_req] robot:{}, ok={}, res={}", self.open_id, ok, res)
            return false
        end
        log_debug("[PacketModule][swap_item_req] robot:{} success", self.open_id)
    end
    return true
end

function PacketModule:install_equip_req(item_id)
    local ok, res = self:call("NID_PACKET_EQUIP_INSTALL_REQ", { item_id = item_id })
    if self:check_callback(ok, res) then
        log_warn("[PacketModule][install_equip_req] robot:{}, ok={}, res={}", self.open_id, ok, res)
        return false
    end
    log_debug("[PacketModule][install_equip_req] robot:{} success", self.open_id)
    return true
end

function PacketModule:uninstall_equip_req()
    local uuid = 0
    for _, item in pairs(self.packets[3]) do
        uuid = item.uuid
        break
    end
    if uuid > 0 then
        local ok, res = self:call("NID_PACKET_EQUIP_UNINSTALL_REQ", { uuid = uuid })
        if self:check_callback(ok, res) then
            log_warn("[PacketModule][uninstall_equip_req] robot:{}, ok={}, res={}", self.open_id, ok, res)
            return false
        end
        log_debug("[PacketModule][uninstall_equip_req] robot:{} success", self.open_id)
    end
    return true
end

--背包中是否拥有招募券
function PacketModule:has_recr_cost()
    local normal_recr = self.packets[1].items[103026] or 0
    local extra_recr = self.packets[1].items[103027] or 0
    if normal_recr < 10 then
        log_debug("[PacketModule][has_recr_cost] open_id:{} normal_recr:{}", self.open_id, normal_recr)
        return false
    end
    if extra_recr < 10 then
        log_debug("[PacketModule][has_recr_cost] open_id:{} extra_recr:{}", self.open_id, extra_recr)
        return false
    end
    log_debug("[PacketModule][has_recr_cost] open_id:{} has enough recruit cost normal:{} extra:{}", self.open_id,normal_recr, extra_recr)
    return true
end

--穿戴采集装备
function PacketModule:install_gather_equip()
    --镐子
    local equip_8_id,equip_9_id,equip_11_id = 200011, 200012, 200013
    local gather_equip_8 = self.packets[3].items[equip_8_id] or 0
    if gather_equip_8 == 0 then
        self:gm_add_item(equip_8_id, 1)
        self:install_equip_req(equip_8_id)
    end
    --斧子
    local gather_equip_9 = self.packets[3].items[equip_9_id] or 0
    if gather_equip_9 == 0 then
        self:gm_add_item(equip_9_id, 1)
        self:install_equip_req(equip_9_id)
    end
    --铲子
    local gather_equip_11 = self.packets[3].items[equip_11_id] or 0
    if gather_equip_11 == 0 then
        self:gm_add_item(equip_11_id, 1)
        self:install_equip_req(equip_11_id)
    end
    log_debug("[PacketModule][install_gather_equip] install gather equip success", self.open_id)
end

--GM获得物品
function PacketModule:gm_add_item(item_id, num)
    local command = string.format("add_item %s %s %s", self.player_id, item_id, num)
    self:call("NID_UTILITY_GM_COMMAND_REQ", { command = command })
end

--GM获得物品
function PacketModule:gm_add_items(items)
    for item_id,item_count in pairs(items) do
        local command = string.format("add_item %s %s %s", self.player_id, item_id, item_count)
        self:call("NID_UTILITY_GM_COMMAND_REQ", { command = command })
    end
end

-- 清理背包
function PacketModule:clear_packet(packet_id)
    log_debug("[PacketModule][clear_packet] packet_id:{}", packet_id)
    if packet_id == PACKET_ITEM then
        local command = string.format("clear_backpack %s", self.player_id)
        self:call("NID_UTILITY_GM_COMMAND_REQ", { command = command })
        self.packets[packet_id].items = {}
    elseif packet_id == PACKET_BANK then
        local command = string.format("clear_bank %s", self.player_id)
        self:call("NID_UTILITY_GM_COMMAND_REQ", { command = command })
        self.packets[packet_id].items = {}
    end
end

-- 检查物品是否足够
function PacketModule:check_items(items, packet_id)
    if not packet_id then
        packet_id = 0
    end
    if not self.packets[packet_id] then
        return false
    end
    for item_id,item_count in pairs(items) do
        local count = self.packets[packet_id].items[item_id]
        if count < item_count then
            return false
        end
    end
    return true
end

local Robot = import("robot/robot.lua")
Robot:delegate(PacketModule)

return PacketModule

