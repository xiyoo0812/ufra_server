--packbench.lua
local PackBenchModule = mixin()
local prop            = property(PackBenchModule)
prop:reader("produces", {})

function PackBenchModule:__init()

end

local Robot = import("robot/robot.lua")
Robot:delegate(PackBenchModule)
return PackBenchModule
