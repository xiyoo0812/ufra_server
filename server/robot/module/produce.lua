--produce.lua
local log_warn      = logger.warn
local log_debug     = logger.debug
local mrandom       = math.random
local tinsert       = table.insert
local mfloor        = math.floor
local tsize         = qtable.size

local object_fty    = quanta.get("object_fty")
local thread_mgr    = quanta.get("thread_mgr")
local protobuf_mgr  = quanta.get("protobuf_mgr")

local UtensilType   = enum("UtensilType")
local PAY           = protobuf_mgr:enum("queue_untype", "PST_PAY")

local ProduceModule = mixin()
local prop          = property(ProduceModule)
prop:reader("produces", {})
function ProduceModule:__init()
    self:register_doer("NID_PRODUCE_PRODUCE_NTF", self, "on_produce_ntf")
    self:register_doer("NID_PRODUCE_PRODUCT_NTF", self, "on_product_ntf")
    self:register_doer("NID_PRODUCE_UNCONF_NTF", self, "on_unconf_ntf")
end

function ProduceModule:on_produce_ntf(msg)
    log_debug("[ProduceModule][on_produce_ntf] begin msg:{}", msg)
    if msg.master ~= self.player_id then
        return
    end
    if not msg.part_sync then
        self.produces = {}
    end
    for uuid, produce in pairs(msg.elems) do
        produce.id = uuid;
        self.produces[uuid] = produce;
        log_debug("[ProduceModule][on_produce_ntf] log produce:{}", produce)
    end
end

function ProduceModule:on_product_ntf(msg)
    log_debug("[ProduceModule][on_product_ntf] begin msg:{}", msg)
    local produce = self.produces[msg.id]
    if not produce then
        log_warn("[ProduceModule][on_product_ntf] is fail player:{} msg:{}", self.player_id, msg)
        return
    end
    produce.products[msg.uuid] = msg.product;
    log_debug("[ProduceModule][on_produce_ntf] log product:{}", msg.product)
end

function ProduceModule:on_unconf_ntf(msg)
    log_debug("[ProduceModule][on_unconf_ntf] begin msg:{}", msg)
    local produce = self.produces[msg.id]
    if not produce then
        log_warn("[ProduceModule][on_product_ntf] is fail player:{} msg:{}", self.player_id, msg)
        return
    end
    produce.unconfs = msg.unconfs;
    log_debug("[ProduceModule][on_produce_ntf] log unconfs:{}", msg.unconfs)
end

function ProduceModule:find_formulas(proto_id)
    local utensil = object_fty:find_utensil(proto_id)
    if not utensil then
        log_warn("[ProduceModule][find_formulas] is fail player:{} proto_id:{}", self.player_id, proto_id)
        return nil
    end
    local formulas = {}
    if utensil.fn_type == UtensilType.GATHER then
        local collect_points = utensil.collect_points
        for _, point_id in pairs(collect_points or {}) do
            local formula_ids = object_fty:find_collect_points(point_id)
            for _, formula_id in pairs(formula_ids) do
                tinsert(formulas, formula_id)
            end
        end
    else
        formulas = utensil.formulas
    end
    return formulas
end

function ProduceModule:get_produce(id)
    return self.produces[id]
end

function ProduceModule:rand_formula(proto_id)
    local formulas = self:find_formulas(proto_id)
    if not formulas then
        log_warn("[ProduceModule][rand_formula] is fail player:{} proto_id:{}", self.player_id, proto_id)
        return nil
    end
    local index = mrandom(1, #formulas)
    local formula = formulas[index]
    return formula
end

function ProduceModule:get_idle_queue(id)
    log_debug("[ProduceModule][get_idle_queue] id:{}", id)
    local produce = self.produces[id];
    if not produce then
        log_warn("[ProduceModule][get_idle_queue] is fail player:{} id:{}", self.player_id, id)
        return 0
    end
    local product_count = tsize(produce.products)
    log_debug("[ProduceModule][get_idle_queue] max_queue_count:{} queue_count:{} product_count:{} produce:{}",
        produce.max_queue_count, produce.queue_count, product_count, produce)
    local queue_count = produce.queue_count - product_count
    return queue_count
end

function ProduceModule:check_idle_queue(proto_id, index)
    log_debug("[ProduceModule][check_idle_queue] proto_id:{} index:{}", proto_id, index)
    local build = self:get_build_by_proto(proto_id, index)
    if not build then
        log_warn("[ProduceModule][check_idle_queue] not build player:{} proto_id:{}", self.player_id, proto_id)
        return false
    end
    local count = self:get_idle_queue(build.id)
    if count <= 0 then
        log_debug("[ProduceModule][check_idle_queue] false count:{}", count)
        return false
    end
    log_debug("[ProduceModule][check_idle_queue] true count:{}", count)
    return true
end

function ProduceModule:get_formula_costs(formula_id, formula_count)
    local formula = object_fty:find_formula(formula_id)
    if not formula then
        log_warn("[ProduceModule][get_formula_costs] is fail player:{} formula:{}", self.player_id, formula_id)
        return nil
    end
    local costs = {}
    for item_id, item_count in pairs(formula.costs) do
        costs[item_id] = item_count * formula_count
    end
    return costs
end

function ProduceModule:check_formula_items(formula_id, formula_count)
    local formula = object_fty:find_formula(formula_id)
    if not formula then
        log_warn("[ProduceModule][check_formula_items] is fail player:{} formula:{}", self.player_id, formula_id)
        return false
    end

    local costs = {}
    for item_id, item_count in pairs(formula.costs) do
        costs[item_id] = item_count * formula_count
    end
    if self:check_items(costs) then
        return true
    end
    return false
end

function ProduceModule:recv_produce(build_id, index)
    log_debug("[ProduceModule][recv_produce] build_id:{} index:{}", build_id, index)
    local build = self:get_build_by_proto(build_id, index)
    if not build then
        log_warn("[ProduceModule][recv_produce] build is nil player:{} build_id:{} index:{}", self.player_id, build_id,
            index)
        return
    end
    local produce = self:get_produce(build.id)
    if not produce then
        log_warn("[ProduceModule][recv_produce] produce is nil player:{} build_id:{}", self.player_id, build_id)
        return
    end
    for _, store_box in pairs(produce.store_boxs) do
        local ok, res = self:call("NID_PRODUCE_RECEIVE_REQ", {
            id = build.id,
            store_box_id = store_box.id
        })
        if self:check_callback(ok, res) then
            log_warn("[ProduceModule][recv_produce] produce_receive robot:{}, ok:{}, res:{} build:{} store_box_id:{}",
                self.open_id, ok, res, build.id, store_box.id)
            return
        end
        thread_mgr:sleep(200)
    end
    log_debug("[ProduceModule][recv_produce] success build_id:{} index:{}", build_id, index)
end

function ProduceModule:cancel_produce(build_id, index)
    log_debug("[ProduceModule][cancel_produce] build_id:{} index:{}", build_id, index)
    local build = self:get_build_by_proto(build_id, index)
    if not build then
        log_warn("[ProduceModule][cancel_produce] build is nil player:{} build_id:{} index:{}", self.player_id, build_id,
            index)
        return
    end
    local produce = self:get_produce(build.id)
    if not produce then
        log_warn("[ProduceModule][cancel_produce] produce is nil player:{} build_id:{}", self.player_id, build_id)
        return
    end

    local uuids = {}
    for uuid, _ in pairs(produce.products or {}) do
        tinsert(uuids, uuid)
    end

    local ok, res = self:call("NID_PRODUCE_CANCEL_REQ", {
        id = build.id,
        uuids = uuids
    })
    if self:check_callback(ok, res) then
        log_warn("[ProduceModule][cancel_produce] produce_cancel robot:{}, ok:{}, res:{} build:{} uuids:{}",
            self.open_id, ok, res, build.id, uuids)
        return
    end
    log_debug("[ProduceModule][cancel_produce] success", build_id, index)
end

function ProduceModule:check_unqueue(build_id, index)
    log_debug("[ProduceModule][check_unqueue] build_id:{} index:{}", build_id, index)
    local build = self:get_build_by_proto(build_id, index)
    if not build then
        log_warn("[ProduceModule][check_unqueue] build is nil player:{} build_id:{} index:{}", self.player_id, build_id,
            index)
        return false
    end
    local produce = self:get_produce(build.id)
    if not produce then
        log_warn("[ProduceModule][check_unqueue] produce is nil player:{} build_id:{}", self.player_id, build_id)
        return false
    end
    if produce.queue_count >= produce.max_queue_count then
        log_debug("[ProduceModule][check_unqueue] queue_count >= max_queue_count player:{} build_id:{}", self.player_id, build_id)
        return false
    end
    local utensil = object_fty:find_utensil(build_id)
    if not utensil then
        log_warn("[ProduceModule][check_unqueue] utensil is nil player:{} build_id:{} index:{}", self.player_id, build_id,
            index)
        return false
    end
    for _, unconf in pairs(produce.unconfs) do
        if unconf.type == PAY and unconf.unlock == false and utensil.level >= unconf.unlevel then
            log_debug("[ProduceModule][check_unqueue] true build_id:{} index:{}", build_id, index)
            return true
        end
    end
    log_debug("[ProduceModule][check_unqueue] false build_id:{} index:{}", build_id, index)
    return false
end

function ProduceModule:unlock_produce_queue(build_id, index)
    log_debug("[ProduceModule][unlock_produce_queue] build_id:{} index:{}", build_id, index)
    local build = self:get_build_by_proto(build_id, index)
    if not build then
        log_warn("[ProduceModule][unlock_produce_queue] build is nil player:{} build_id:{} index:{}", self.player_id, build_id,
            index)
        return
    end
    local produce = self:get_produce(build.id)
    if not produce then
        log_warn("[ProduceModule][unlock_produce_queue] produce is nil player:{} build_id:{}", self.player_id, build_id)
        return
    end
    local utensil = object_fty:find_utensil(build_id)
    if not utensil then
        log_warn("[ProduceModule][unlock_produce_queue] utensil is nil player:{} build_id:{} index:{}", self.player_id, build_id,
            index)
        return
    end

    for _, unconf in pairs(produce.unconfs) do
        if unconf.type == PAY and unconf.unlock == false and utensil.level >= unconf.unlevel then
            local costs = utensil.pay_queue_costs[unconf.queue_id]
            if not costs or not next(costs) then
                goto continue
            end
            if not self:check_items(costs) then
                self:gm_add_items(costs)
                local ok, res = self:call("NID_PRODUCE_UNQUEUE_REQ", {
                    id = build.id,
                    queue_id = unconf.queue_id,
                })
                if self:check_callback(ok, res) then
                    log_warn("[ProduceModule][unlock_produce_queue] robot:{}, ok:{}, res:{} unconf:{}", self.open_id, ok, res, unconf)
                    goto continue
                end
                log_debug("[ProduceModule][check_unqueue] succss costs:{} queue_id:{}", costs, unconf.queue_id)
                thread_mgr:sleep(200)
            end
            :: continue ::
        end
    end
end

-- 切换配送
function ProduceModule:switch_produce_delive(enable)
    for _, produce in pairs(self.produces) do
        local ok, res = self:call("NID_PRODUCE_SWDELIVE_REQ", {
            id = produce.id,
            enable = enable,
        })
        if self:check_callback(ok, res) then
            log_warn("[ProduceModule][open_produce_delive] robot:{}, ok:{}, res:{}", self.open_id, ok, res)
            goto continue
        end
        thread_mgr:sleep(200)
        :: continue ::
    end
end

function ProduceModule:add_produce(build_id, index)
    local build = self:get_build_by_proto(build_id, index)
    if not build then
        log_warn("[ProduceModule][add_produce] build is nil player:{} build_id:{} index:{}", self.player_id, build_id,
            index)
        return
    end

    local queue_count = self:get_idle_queue(build.id)
    if queue_count <= 0 then
        log_warn("[ProduceModule][add_produce] queue_count <= 0 player:{} build_id:{} queue_count:{}", self.player_id,
            build_id, queue_count)
        return
    end

    local utensil = object_fty:find_utensil(build_id)
    if not utensil then
        log_warn("[ProduceModule][find_utensil] utensil is nil player:{} build_id:{}", self.player_id, build_id)
        return
    end

    if utensil.fn_type == UtensilType.GATHER then
        self:produce_gather(build)
    else
        self:produce_make(build, queue_count)
    end
end

function ProduceModule:produce_make(build, queue_count)
    for i = 1, queue_count do
        local formula_id = self:rand_formula(build.proto_id)
        if not formula_id then
            log_warn("[ProduceModule][produce_make] formulas is nil player:{} build_id:{}", self.player_id,
                build.proto_id)
            goto continue
        end
        local formula = object_fty:find_formula(formula_id);
        if not formula then
            log_warn("[ProduceModule][produce_make] formulas is nil player:{} build_id:{} formula_id:{}", self.player_id,
                build.proto_id, formula_id)
            goto continue
        end
        log_debug("[ProduceModule][produce_make] formulas player:{} build_id:{} formula_id:{}", self.player_id,
            build.proto_id, formula_id)
        if not self:check_formula_items(formula.id, formula.max) then
            local costs = self:get_formula_costs(formula.id, formula.max)
            if not costs then
                log_warn("[ProduceModule][produce_make] costs is nil player:{} formula_id:{}", self.player_id, formula
                .id)
                goto continue
            end
            self:gm_add_items(costs)
            thread_mgr:sleep(3000)
        end
        local ok, res = self:call("NID_PRODUCE_MAKE_REQ", {
            id = build.id,
            formula_id = formula.id,
            count = formula.max
        })
        if self:check_callback(ok, res) then
            log_warn("[ProduceModule][produce_make] robot:{}, ok:{}, res:{}", self.open_id, ok, res)
            goto continue
        end
        thread_mgr:sleep(200)
        :: continue ::
    end
end

function ProduceModule:produce_gather(build)
    log_debug("[ProduceModule][produce_gather] formulas player:{} build:{}", self.player_id,
            build.proto_id)
    local utensil = object_fty:find_utensil(build.proto_id)
    if not utensil then
        log_warn("[ProduceModule][produce_gather] utensil is nil player:{} build_id:{}", self.player_id, build.proto_id)
        return
    end
    local prod_cap = utensil.prod_cap
    if not prod_cap or prod_cap <= 0 then
        log_warn("[ProduceModule][produce_gather] utensil is nil player:{} build_id:{} prod_cap:{}", self.player_id,
            build.proto_id, prod_cap)
        return
    end
    local formula_id = self:rand_formula(build.proto_id)
    if not formula_id then
        log_warn("[ProduceModule][produce_gather] formula is nil player:{} formula_id:{}", self.player_id, formula_id)
        return
    end
    local formula = object_fty:find_formula(formula_id);
    if not formula then
        log_warn("[ProduceModule][produce_gather] formulas is nil player:{} build_id:{} formula_id:{}", self.player_id,
            build.proto_id, formula_id)
        return
    end
    local formula_count = mfloor(prod_cap / formula.worth)
    local ok, res = self:call("NID_PRODUCE_GATHER_REQ", {
        id = build.id,
        formulas = { [formula.id] = formula_count }
    })
    if self:check_callback(ok, res) then
        log_warn("[ProduceModule][produce_gather] robot:{}, ok:{}, res:{}", self.open_id, ok, res)
        return
    end
    log_debug("[ProduceModule][produce_gather] success player:{} build:{} formula:{} count:{}", self.player_id,
            build.proto_id, formula.id, formula_count)
end

local Robot = import("robot/robot.lua")
Robot:delegate(ProduceModule)
return ProduceModule
