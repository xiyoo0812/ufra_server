--building.lua
local log_debug       = logger.debug
local log_warn        = logger.warn
local tinsert         = table.insert

local object_fty      = quanta.get("object_fty")
local protobuf_mgr    = quanta.get("protobuf_mgr")
local BUILDING_RECAST = protobuf_mgr:enum("building_status", "BUILDING_RECAST")

local BuildModule     = mixin()
local prop            = property(BuildModule)
prop:reader("builds", {})
prop:reader("build_map", {})

function BuildModule:__init()
    self:register_doer("NID_BUILDING_BUILDING_NTF", self, "on_building_ntf")
end

function BuildModule:on_building_ntf(msg)
    if msg.master ~= self.player_id then
        return
    end
    if not msg.part_sync then
        self.builds = {}
    end
    for uuid, build in pairs(msg.elems) do
        build.id = uuid;
        if build.proto_id ~= 0 then
            self.builds[uuid] = build
            self.build_map[build.proto_id] = build
            log_debug("[BuildModule][on_building_ntf] set uuid:{} build:{}", uuid, build)
        else
            local item_build = self.builds[uuid]
            if self.builds[uuid] then
                self.builds[uuid] = nil
                self.build_map[item_build.proto_id] = nil
                log_debug("[BuildModule][on_building_ntf] del build:{}", uuid)
            end
        end
    end
end

function BuildModule:get_build(uuid)
    return self.builds[uuid]
end

function BuildModule:get_build_detail(proto_id)
    log_debug("[BuildModule][get_build_detail] begin proto_id:{}", proto_id)
    local utensil = object_fty:find_utensil(proto_id)
    if not utensil then
        log_debug("[BuildModule][get_build_detail] utensil is nil proto_id:{}", proto_id)
        return nil
    end
    local group = utensil.group
    local conf = object_fty:find_robot_build(group, self.work_build_index)
    if not conf then
        log_debug("[BuildModule][get_build_detail] conf is nil proto_id:{} group:{}", proto_id, group)
        return nil
    end
    log_debug("[BuildModule][get_build_detail] proto_id:{} detail:{}", proto_id, conf.detail)
    local bytes = protobuf_mgr:encode_byname("ncmd_cs.build_detail", conf.detail)
    return bytes
end

function BuildModule:count_proto_build(proto_id)
    local count = 0
    for _, build in pairs(self.builds) do
        if build.proto_id == proto_id then
            count = count + 1
        end
    end
    return count
end

function BuildModule:count_group_build(group_id)
    local count = 0
    for _, build in pairs(self.builds) do
        local utensil = object_fty:find_utensil(build.proto_id)
        if utensil and utensil.group == group_id then
            count = count + 1
        end
    end
    return count
end

function BuildModule:get_build_by_proto(proto_id, index)
    log_debug("get_build_by_proto proto_id:{} index:{}", proto_id, index)
    local result = {}
    local conf = object_fty:find_utensil(proto_id)
    if not conf then
        return nil
    end
    for _, build in pairs(self.builds) do
        local utensil = object_fty:find_utensil(build.proto_id)
        if utensil and utensil.group == conf.group then
            tinsert(result, build)
        end
    end
    return result[index]
end

function BuildModule:get_build_conf(proto_id, index)
    local build = self:get_build_by_proto(proto_id, index)
    if not build then
        log_warn("get_build_conf build is fail proto_id:{} index:{}", proto_id, index)
        return nil
    end
    local utensil = object_fty:find_utensil(build.proto_id)
    if not utensil then
        log_warn("get_build_conf utensil is fail proto_id:{}", proto_id)
        return nil
    end
    return utensil
end

function BuildModule:check_build_by_proto(proto_id, index)
    local count = self:count_proto_build(proto_id)
    if count < index then
        return false
    end
    return true
end

function BuildModule:check_build_limit(proto_id)
    local utensil = object_fty:find_utensil(proto_id)
    if not utensil then
        return false
    end
    local num = self:count_group_build(utensil.group)
    local level = self:get_attr(129)
    local limit = object_fty:get_utensil_limit(proto_id, level)
    if limit == -1 then --不限制
        return false
    end
    if num + 1 > limit then
        return true
    end
    return false
end

function BuildModule:check_build_upgrade(proto_id, index, up_proto_id)
    log_debug("check_build_upgrade proto_id:{} index:{} up_proto_id:{}", proto_id, index, up_proto_id)
    local build = self:get_build_by_proto(proto_id, index)
    if not build then
        return false
    end
    if build.proto_id == up_proto_id then
        return false
    end
    local conf = object_fty:find_utensil(proto_id)
    if not conf then
        return false
    end
    local next = object_fty:find_utensil_group(conf.group, up_proto_id)
    if not next then
        return false
    end
    return true
end

function BuildModule:check_build_finish(proto_id, index)
    log_debug("check_build_finish proto_id:{} index:{}", proto_id, index)
    local build = self:get_build_by_proto(proto_id, index)
    if not build then
        return false
    end
    if build.status == BUILDING_RECAST and build.timestamp >= quanta.now then
        return true
    end
    return false
end

function BuildModule:check_build_recast(proto_id, index)
    log_debug("check_build_recast proto_id:{} index:{}", proto_id, index)
    local build = self:get_build_by_proto(proto_id, index)
    if not build then
        return false
    end
    if build.status == BUILDING_RECAST then
        return true
    end
    return false
end

function BuildModule:build_upgrade_costs(proto_id, up_proto_id)
    log_debug("build_upgrade_costs proto_id:{} up_proto_id:{}", proto_id, up_proto_id)
    local conf = object_fty:find_utensil(proto_id)
    if not conf then
        log_debug("build_upgrade_costs not conf")
        return nil
    end
    local next = object_fty:find_utensil_group(conf.group, up_proto_id)
    if not next then
        log_debug("build_upgrade_costs not next")
        return nil
    end
    return next.costs or {}
end

local Robot = import("robot/robot.lua")
Robot:delegate(BuildModule)
return BuildModule
