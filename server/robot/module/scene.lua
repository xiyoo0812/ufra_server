--scene.lua
local log_debug             = logger.debug
local qrandom               = qtable.random

local protobuf_mgr          = quanta.get("protobuf_mgr")
local config_mgr            = quanta.get("config_mgr")
local newbee_db             = config_mgr:init_table("newbee", "key")

local ENTITY_PLAYER         = protobuf_mgr:enum("entity_type", "ENTITY_PLAYER")
local ENTITY_NPC            = protobuf_mgr:enum("entity_type", "ENTITY_NPC")
local ENTITY_MONSTER        = protobuf_mgr:enum("entity_type", "ENTITY_MONSTER")
local ENTITY_RESOURCE       = protobuf_mgr:enum("entity_type", "ENTITY_RESOURCE")

local BORN_MAP              = newbee_db:find_integer("value", "born_map_id")
local BORN_X                = newbee_db:find_integer("value", "born_pox_x")
local BORN_Y                = newbee_db:find_integer("value", "born_pox_y")
local BORN_Z                = newbee_db:find_integer("value", "born_pox_z")

local ClientEntity          = import("robot/object/client_entity.lua")

local SceneModule = mixin()
local prop = property(SceneModule)
prop:reader("attrs", {})    --attrs
prop:reader("map_id", nil)
prop:reader("pos_x", nil)
prop:reader("pos_y", nil)
prop:reader("pos_z", nil)
prop:reader("players", {})
prop:reader("npcs", {})
prop:reader("monsters", {})
prop:reader("resources", {})

function SceneModule:__init()
    -- 属性初始化
    self.map_id = BORN_MAP
    self.pos_x = BORN_X
    self.pos_y = BORN_Y
    self.pos_z = BORN_Z
    -- 消息处理器
    self:register_doer("NID_ENTITY_ATTR_UPDATE_NTF", self, "on_attr_update_ntf")
    self:register_doer("NID_ENTITY_ENTER_SCENE_NTF", self, "on_enter_scene_ntf")
    self:register_doer("NID_ENTITY_LEAVE_SCENE_NTF", self, "on_leave_scene_ntf")
end

function SceneModule:update_entity_scene_info(entity, data)
    entity.map_id = data.map_id
    entity.pos_x = data.pos_x
    entity.pos_y = data.pos_y
    entity.pos_z = data.pos_z
    self:update_entity_attrs(entity, data)
end

function SceneModule:update_entity_attrs(entity, data)
    for _, attr in pairs(data.attrs) do
        local attr_id, value = attr.attr_id, attr[attr.value]
        -- log_debug("[SceneModule][update_entity_attrs] entity:%s, attr_id: %s-%s", data.id, attr_id, value)
        entity:set_attr(attr_id, value)
    end
end

function SceneModule:find_entity(entity_id)
    local entity = self.players[entity_id]
    if not entity then
        entity = self.npcs[entity_id]
    end
    if not entity then
        entity = self.monsters[entity_id]
    end
    if not entity then
        entity = self.resources[entity_id]
    end
    return entity
end

-- (按类型)搜索实体
function SceneModule:search_entity(entity_type)
    local entity = nil
    if entity_type == ENTITY_PLAYER then
        entity = qrandom(self.players)
    elseif entity_type == ENTITY_NPC then
        entity = qrandom(self.npcs)
    elseif entity_type == ENTITY_MONSTER then
        entity = qrandom(self.monsters)
    elseif entity_type == ENTITY_RESOURCE then
        entity = qrandom(self.resources)
    end
    return entity
end

function SceneModule:on_enter_scene_ntf(msg)
    if msg.id == self.player_id then
        log_debug("[SceneModule][on_enter_scene_ntf] robot:%s id: %s, type: %s", self.open_id, msg.id, msg.type)
        self:update_entity_scene_info(self, msg)
        self.login_success = true
        return
    end
    local entity = ClientEntity(msg.id, msg.type)
    self:update_entity_scene_info(entity, msg)
    if msg.type == ENTITY_PLAYER then
        self.players[msg.id] = entity
    elseif msg.type == ENTITY_NPC then
        self.npcs[msg.id] = entity
    elseif msg.type == ENTITY_MONSTER then
        self.monsters[msg.id] = entity
    elseif msg.type == ENTITY_RESOURCE then
        self.resources[msg.id] = entity
    end
end

function SceneModule:on_leave_scene_ntf(msg)
    log_debug("[SceneModule][on_leave_scene_ntf] robot:%s, id: %s", self.open_id, msg.id)
    if msg.id == self.player_id then
        self.login_success = false
        -- TODO: remove robot?
        return
    end
    local entity = self:find_entity(msg.id)
    if entity then
        entity:destory()
        if msg.type == ENTITY_PLAYER then
            self.players[msg.id] = nil
        elseif msg.type == ENTITY_NPC then
            self.npcs[msg.id] = nil
        elseif msg.type == ENTITY_MONSTER then
            self.monsters[msg.id] = nil
        elseif msg.type == ENTITY_RESOURCE then
            self.resources[msg.id] = nil
        end
    end
end

function SceneModule:on_attr_update_ntf(msg)
    log_debug("[SceneModule][on_attr_update_ntf] robot:{}, id:{} msg:{}", self.open_id, msg.id, msg)
    if msg.id == self.player_id then
        self:update_entity_attrs(self, msg)
    else
        local entity = self:find_entity(msg.id)
        if entity then
            self:update_entity_attrs(entity, msg)
        end
    end
end

function SceneModule:set_attr(attr_id, value)
    self.attrs[attr_id] = value
end

function SceneModule:get_attr(attr_id)
    return self.attrs[attr_id]
end

local Robot = import("robot/robot.lua")
Robot:delegate(SceneModule)

return SceneModule

