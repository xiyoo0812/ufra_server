-- client_entity.lua

local ClientEntity = class()
local prop = property(ClientEntity)
prop:reader("entity_id", nil)
prop:reader("type", nil)
prop:reader("attrs", {})
prop:reader("map_id", nil)
prop:reader("pos_x", nil)
prop:reader("pos_y", nil)
prop:reader("pos_z", nil)

function ClientEntity:__init(entity_id, entity_type)
    self.entity_id = entity_id
    self.type = entity_type
end

function ClientEntity:set_attr(attr_id, value)
    self.attrs[attr_id] = value
end

function ClientEntity:get_attr(attr_id)
    return self.attrs[attr_id]
end

function ClientEntity:update_pos(pos)
    self.pos_x = pos.x
    self.pos_y = pos.y
    self.pos_z = pos.z
end

function ClientEntity:destory()
    self.attrs = nil
end

return ClientEntity
