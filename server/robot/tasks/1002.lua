
return {
    id=1001,
    ip="127.0.0.1",
    port=20013,
    time=1000 * 600,
    start_time=1703572200,
    tasks = {
        [1] = {
            script="login_ai",
            count=1,
            rate=2500,
            start_open_id=4002,
        },
        [2] = {
            script="scene_ai",
            count=1,
            rate=2500,
            start_open_id=1000,
        },
        [3] = {
            script="move_ai",
            count=1,
            rate=2500,
            start_open_id=4008,
        },
        [4] = {
            script="gather_ai",
            count=1,
            rate=2500,
            start_open_id=1000,
        },
        [5] = {
            script="skill_ai",
            count=1,
            rate=2500,
            start_open_id=1000,
        },
        [6] = {
            script="recruit_ai",
            count=1,
            rate=2500,
            start_open_id=3000,
        },
        [7] = {
            script="gather_ai",
            count=1,
            rate=2500,
            start_open_id=1000,
        },
    }
}