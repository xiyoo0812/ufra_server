--gather_ai.lua
-- run: quanta.exe .\eufaula\robot.conf --index=4 --task_id=1002
return {
    name = "gather_ai",
    root = 1,
    rewind = 3,
    nodes = {
        [1] = {
            type = "CASE",
            case = "login_base",
            next = 2
        },
        [2] = {
            type = "WAIT",
            time = 500,
            after = [[robot:install_gather_equip()]], --穿戴采集装备
            next = 3
        },
        [3] = {
            type = "CASE",
            before = [[robot:gather_drop()]], --寻找目标
            case = "move_base",
            next = 4
        },
        [4] = {
            type = "REQ",
            cmd_id = "NID_SKILL_GATHER_REQ",
            inputs = {
                uuid = { type = "lua", value = "robot.gather_target.uuid" },
                step = { type = "lua", value = "robot.gather_target.step" },
            }
        },
    }
}