--move_ai.lua
-- run: quanta.exe .\eufaula\robot.conf --index=3 --task_id=1002
return {
    name = "recruit_ai",
    root = 1,
    rewind = 5,
    nodes = {
        [1] = {
            type = "CASE",
            case = "login_base",
            next = 2
        },
        [2] = {
            type = "NTF",
            cmd_id = "NID_RECR_POOL_NTF",
            next = 3
        },
        [3] = {
            type = "NTF",
            cmd_id = "NID_PACKET_ITEM_SYNC_NTF",
            next = 4
        },
        [4] = {
            type = "COND",
            cond = "robot:has_recr_cost()",
            result = { success = 7, failed = 5 }
        },
        [5] = {
            type = "GM",
            cmds = { 'add_item %s 103026 10' },
            time = 200,
            next = 6
        },
        [6] = {
            type = "GM",
            cmds = { 'add_item %s 103027 10' },
            time = 200,
            next = 7
        },
        [7] = {
            type = "REQ",
            cmd_id = "NID_RECR_START_REQ",
            inputs = {
                pool_id = { type = "lua", value = "robot:random_pool()" },
                ten = { type = "lua", value = "robot:random_ten()"},
                free = { type = "lua", value = "robot:random_free()" },
            },
            next = 8
        },
        [8] = {
            type = "REQ",
            cmd_id = "NID_RECR_REC_REQ",
            inputs = {
                page_num = { type = "const", value = 1 },
            }
        },
    }
}