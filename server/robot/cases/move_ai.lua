--move_ai.lua
-- run: quanta.exe .\eufaula\robot.conf --index=3 --task_id=1002
return {
    name = "move_ai",
    root = 1,
    rewind = 3,
    nodes = {
        [1] = {
            type = "CASE",
            case = "login_base",
            next = 2
        },
        [2] = {
            type = "WAIT",
            time = 500,
            next = 3
        },
        [3] = {
            type = "CASE",
            before = "robot:set_nav_target()", -- 设置寻路目标(eg: robot:set_nav_target({x=0,y=0,z=0}), 为空则自动随机)
            case = "move_base",
        },
    }
}