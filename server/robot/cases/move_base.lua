--move_base.lua
return {
    name = "move_base",
    root = 1,
    nodes = {
        [1] = {
            type = "COND",
            cond = "robot.nav_target~=nil", -- 拥有寻路目标
            result = { success = 2, failed = 0 } -- 注: 0, 特殊值, 用不存在的 index 表示结束
        },
        [2] = {
            type = "COND",
            before = [[robot:move()]], -- 移动(计算寻路信息并开始移动, 或更新寻路移动位置)
            cond = "robot.nav_info~=nil", -- 拥有寻路信息(true: 寻路中, false: 失败或完成)
            result = { success = 3, failed = 0 }
        },
        [3] = {
            type = "WAIT",
            time = 150,
            next = 1
        },
    }
}