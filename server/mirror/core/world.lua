--world.lua
local log_debug         = logger.debug
local qfailed           = quanta.failed

local client_mgr        = quanta.get("client_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")

local FRAME_FAILED      = protobuf_mgr:error_code("FRAME_FAILED")

local World = class()
local prop = property(World)
prop:reader("id", nil)
prop:reader("session", nil)
prop:reader("rpc_server", nil)
prop:reader("clients", {})

function World:__init(world_id, rpc_server, session)
    self.id = world_id
    self.session = session
    self.rpc_server = rpc_server
end

function World:notify(player_id, cmd_id, body)
    local client = self.clients[player_id]
    if client then
        client_mgr:send(client, cmd_id, body)
    end
end

function World:call(player_id, service_type, cmd_id, body, session_id)
    local client = self.clients[player_id]
    if not client then
        client_mgr:callback_errcode(client, cmd_id, FRAME_FAILED, session_id)
        client_mgr:close_session(client)
        return false
    end
    local ok, cb_data = self.rpc_server:wait_call(self.session, session_id, "rpc_transfer_message", player_id, service_type, cmd_id, body, session_id)
    if not ok then
        client_mgr:callback_errcode(client, cmd_id, FRAME_FAILED, session_id)
        return false
    end
    client_mgr:callback_by_id(client, cmd_id, cb_data , session_id)
    return true, client
end

function World:inline_call(client, rpc, player_id, cmd_id, body, session_id)
    log_debug("[World][inline_call] cmd_id:{} body:{} session_id:{}", cmd_id, body, session_id)
    local ok, cb_data = self.rpc_server:wait_call(self.session, session_id, rpc, player_id, 0, cmd_id, body, session_id)
    if not ok then
        client_mgr:callback_errcode(client, cmd_id, FRAME_FAILED, session_id)
        return false
    end
    if qfailed(cb_data.error_code) then
        client_mgr:callback_errcode(client, cmd_id, cb_data.error_code, session_id)
        return false
    end
    return true, cb_data
end

function World:login(client, cmd_id, body, session_id, token)
    log_debug("[World][login] cmd_id:{} body:{} session_id:{} token:{}", cmd_id, body, session_id, token)
    local ok, cb_data = self:inline_call(client, "rpc_transfer_login", body.role.character_id, cmd_id, body, session_id, token)
    if ok then
        local player_id = cb_data.role_id
        client.world_id = self.id
        client.player_id = player_id
        self.clients[player_id] = client
        client_mgr:callback_by_id(client, cmd_id, cb_data , session_id)
    end
end

function World:logout(client, player_id, cmd_id, body, session_id)
    local ok, cb_data = self:inline_call(client, "rpc_transfer_logout", player_id, cmd_id, body, session_id)
    if ok then
        client_mgr:callback_by_id(client, cmd_id, cb_data , session_id)
        client_mgr:close_session(client)
        self.clients[player_id] = nil
        client.player_id = nil
        client.world_id = nil
    end
end

function World:reload(client, player_id, cmd_id, body, session_id)
    local ok, cb_data = self:inline_call(client, "rpc_transfer_reload", player_id, cmd_id, body, session_id)
    if ok then
        client.world_id = self.id
        client.player_id = player_id
        self.clients[player_id] = client
        client_mgr:callback_by_id(client, cmd_id, cb_data , session_id)
    end
end

function World:offline(player_id, token, err)
    self.rpc_server:send(self.session, "rpc_transfer_offline", player_id, token, err)
    self.clients[player_id] = nil
end

function World:kickout(player_id)
    self.clients[player_id] = nil
end

function World:destory()
    for _, client in pairs(self.clients) do
        client_mgr:close_session(client)
    end
    self.clients = {}
end

return World
