--transfer_mgr.lua
local log_warn     = logger.warn
local log_debug    = logger.debug

local event_mgr    = quanta.get("event_mgr")
local client_mgr   = quanta.get("client_mgr")
local config_mgr   = quanta.get("config_mgr")
local mirror_mgr   = quanta.get("mirror_mgr")

local protobuf_mgr = quanta.get("protobuf_mgr")

local DOMAIN_ADDR  = environ.get("QUANTA_DOMAIN_ADDR")
local FRAME_UPHOLD = protobuf_mgr:error_code("FRAME_UPHOLD")

local filter       = config_mgr:init_table("filter", "name")

local HttpServer   = import("network/http_server.lua")

local TransferMgr  = singleton()
local prop         = property(TransferMgr)
prop:reader("ignore_cmds", {}) --日志过滤

function TransferMgr:__init()
    --创建HTTP服务器
    self.http_server = HttpServer(environ.get("QUANTA_MIRROR_HTTP"))
    self.http_server:register_post("/request-world", "on_request_world", self)
    --设置domain
    local nport = self.http_server:get_port()
    service.modify_host(nport, DOMAIN_ADDR)

    -- 协议过滤
    self:init_filter()
    -- 网络事件监听
    event_mgr:add_listener(self, "on_socket_cmd")
    event_mgr:add_listener(self, "on_socket_error")
    event_mgr:add_listener(self, "on_socket_accept")
    event_mgr:add_listener(self, "on_transfer_message")
    event_mgr:add_listener(self, "on_transfer_close")
end

function TransferMgr:on_transfer_close(session, world_id, player_id)
    local world = mirror_mgr:get_world(world_id)
    if world then
        world:kickout(player_id)
    end
end

function TransferMgr:on_transfer_message(session, world_id, player_id, cmd_id, body)
    local world = mirror_mgr:get_world(world_id)
    if world then
        if self:is_print_cmd(cmd_id) then
            log_debug("[TransferMgr][on_transfer_message] player({}) send message({}-{}) !", player_id, cmd_id, body)
        end
        world:notify(player_id, cmd_id, body)
    end
end

function TransferMgr:on_request_world(url, body, params, headers)
    log_debug("TransferMgr:on_request_world: url={}, body={}, params={}, headers={}", url, body, params, headers)
    local world = mirror_mgr:get_world(body.world_id)
    if world then
        local result = {
            code = 0,
            host = DOMAIN_ADDR,
            port = client_mgr:get_port(),
            world_id = body.world_id,
            name = body.name,
            max_count = body.max_count,
            user_id = body.master,
            world_type = body.type,
            is_password = false
        }
        if body.user_id ~= body.master and (body.token and body.token ~= "") then
            result.is_password = true
        end
        log_debug("TransferMgr:on_request_world: result:{}", result)
        return result
    else
        log_warn("TransferMgr:on_request_world world is nil: world:{}", body.world_id)
    end
    return { code = -1 }
end

---是否输出CMD消息的内容
function TransferMgr:is_print_cmd(cmd_id)
    if self.ignore_cmds[cmd_id] then
        return false
    end
    return true
end

---是否输出CMD消息的内容
function TransferMgr:init_filter()
    self:on_cfg_filter_changed()
    event_mgr:add_trigger(self, "on_cfg_filter_changed")
end

---日志忽略网络消息通知名
function TransferMgr:on_cfg_filter_changed()
    for cmd_name, conf in filter:iterator() do
        local cmd_id = protobuf_mgr:msg_id(cmd_name)
        if conf.log then
            self.ignore_cmds[cmd_id] = true
            self.ignore_cmds[cmd_name] = true
        end
    end
end

--客户端连上
function TransferMgr:on_socket_accept(session)
    log_debug("[TransferMgr][on_socket_accept] {} connected!", session.token)
end

--客户端连接断开
function TransferMgr:on_socket_error(session, token, err)
    local player_id = session.player_id
    log_debug("[TransferMgr][on_socket_error] (t:{}-u:{}) lost, because: {}!", token, player_id, err)
    local world = mirror_mgr:get_world(session.world_id)
    if world then
        world:offline(player_id, token, err)
    end
end

--客户端消息分发
function TransferMgr:on_socket_cmd(session, service_type, cmd_id, body, session_id)
    if service_type == 0 then
        event_mgr:notify_command(cmd_id, session, cmd_id, body, session_id)
        return
    end
    local world = mirror_mgr:get_world(session.world_id)
    if not world then
        log_warn("[TransferMgr][on_socket_cmd] on_proto_filter false, cmd_id={}", cmd_id)
        client_mgr:callback_errcode(session, cmd_id, FRAME_UPHOLD, session_id)
        return
    end
    world:call(session.player_id, service_type, cmd_id, body, session_id)
end

quanta.transfer_mgr = TransferMgr()

return TransferMgr
