--mirror_mgr.lua
local log_debug    = logger.debug
local log_warn     = logger.warn
local service_id   = quanta.service
local qsuccess     = quanta.success

local event_mgr    = quanta.get("event_mgr")
local router_mgr   = quanta.get("router_mgr")
local protobuf_mgr = quanta.get("protobuf_mgr")

local WS_ONLINE    = protobuf_mgr:enum("world_state", "WS_ONLINE")
local WS_OFFLINE   = protobuf_mgr:enum("world_state", "WS_OFFLINE")

local World        = import("mirror/core/world.lua")
local RpcServer    = import("network/rpc_server.lua")

local MirrorMgr    = singleton()
local prop         = property(MirrorMgr)
prop:reader("worlds", {})
prop:reader("rpc_server", nil)

function MirrorMgr:__init()
    --创建rpc服务器
    local ip, port = environ.addr("QUANTA_MIRROR_RPC")
    local rpc_server = RpcServer(self, ip, port)
    service.modify_option(rpc_server:get_port())
    log_debug("[MirrorMgr][__init] ip:{} port:{} modify_option:{}", ip, port, rpc_server:get_port())
    self.rpc_server = rpc_server
    event_mgr:add_listener(self, "rpc_player_count")
end

function MirrorMgr:rpc_player_count(client, id, count)
    log_debug("[MirrorMgr][rpc_player_count] wid:{} count:{}", id, count)
    local ok, code, result = router_mgr:call_worldm_hash(service_id, "rpc_agent_sync",
        { id = quanta.id, worlds = { { id = id, amount = count } } })
    if not qsuccess(code, ok) then
        log_warn('[MirrorMgr]:[rpc_player_count] failed, ok: {}, code: {}, result: {}', ok, code, result)
        return
    end
end

function MirrorMgr:get_world(world_id)
    return self.worlds[world_id]
end

-- 心跳
function MirrorMgr:on_client_beat(client)
end

function MirrorMgr:on_client_accept(client)
    log_debug("[MirrorMgr][on_client_accept]: token={}", client.token)
end

function MirrorMgr:sync_world_state(id, state)
    local ok, code, result = router_mgr:call_worldm_hash(service_id, "rpc_agent_state",
        { id = quanta.id, worlds = { { id = id, state = state } } })
    if not qsuccess(code, ok) then
        log_warn('[MirrorMgr]:[sync_world_state] failed, ok: {}, code: {}, result: {}', ok, code, result)
        return
    end
end

function MirrorMgr:on_client_register(client, node)
    log_debug("[MirrorMgr][on_client_register]: node={}", node)
    local world_id = node.id
    local world = World(world_id, self.rpc_server, client)
    self.worlds[world_id] = world
    client.world_id = world_id
    self:sync_world_state(world_id, WS_ONLINE)
end

function MirrorMgr:on_client_error(client, token, err)
    log_debug("[MirrorMgr][on_client_error] node:{}, token:{}", client.name, token)
    local world_id = client.world_id
    local world = self.worlds[world_id]
    if world then
        self.worlds[world_id] = nil
        world:destory()
    end
    self:sync_world_state(world_id, WS_OFFLINE)
end

quanta.mirror_mgr = MirrorMgr()

return MirrorMgr
