--transfer_servlet.lua

local log_info          = logger.info
local log_debug         = logger.debug

local client_mgr        = quanta.get("client_mgr")
local mirror_mgr        = quanta.get("mirror_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")

local FRAME_UPHOLD      = protobuf_mgr:error_code("FRAME_UPHOLD")

local TransferServlet = singleton()

function TransferServlet:__init()
    --CS协议
    protobuf_mgr:register(self, "NID_HEARTBEAT_REQ", "on_heartbeat_req")
    protobuf_mgr:register(self, "NID_LOGIN_ROLE_LOGIN_REQ", "on_role_login_req")
    protobuf_mgr:register(self, "NID_LOGIN_ROLE_LOGOUT_REQ", "on_role_logout_req")
    protobuf_mgr:register(self, "NID_LOGIN_ROLE_RELOAD_REQ", "on_role_reload_req")
end

--心跳
function TransferServlet:on_heartbeat_req(session, cmd_id, body, session_id)
    local world = mirror_mgr:get_world(session.world_id)
    if not world then
        client_mgr:callback_errcode(session, cmd_id, FRAME_UPHOLD, session_id)
        return
    end
    if session.player_id then
        world:call(session.player_id, 0, cmd_id, body, session_id)
    end
end

function TransferServlet:on_role_login_req(session, cmd_id, body, session_id)
    local open_id, world_id = body.open_id, body.world_id
    log_debug("[TransferServlet][on_role_login_req] open_id({}) body({})  login req!", open_id, body)
    local world = mirror_mgr:get_world(world_id)
    if not world then
        log_debug("[TransferServlet][on_role_login_req] not world:{}!", world_id)
        client_mgr:callback_errcode(session, cmd_id, FRAME_UPHOLD, session_id)
        client_mgr:close_session(session)
        return
    end
    world:login(session, cmd_id, body, session_id, session.token)
end

function TransferServlet:on_role_logout_req(session, cmd_id, body, session_id)
    local player_id = body.role_id
    log_debug("[TransferServlet][on_role_logout_req] player({}) logout req!", player_id)
    local world = mirror_mgr:get_world(session.world_id)
    if not world then
        client_mgr:callback_errcode(session, cmd_id, FRAME_UPHOLD, session_id)
        client_mgr:close_session(session)
        return
    end
    world:logout(session, player_id, cmd_id, body, session_id)
    log_info("[TransferServlet][on_role_logout_req] player({}) logout success!", player_id)
end

function TransferServlet:on_role_reload_req(session, cmd_id, body, session_id)
    local player_id = body.role_id
    log_debug("[TransferServlet][on_role_reload_req] player({}) reload req!", player_id)
    local world = mirror_mgr:get_world(session.world_id)
    if not world then
        client_mgr:callback_errcode(session, cmd_id, FRAME_UPHOLD, session_id)
        client_mgr:close_session(session)
        return
    end
    world:reload(session, player_id, cmd_id, body, session_id)
end

quanta.transfer_servlet = TransferServlet()

return TransferServlet
