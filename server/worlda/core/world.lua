--world.lua
local log_debug         = logger.debug

local World = class()
local prop = property(World)
prop:reader("id", nil)
prop:reader("type", nil)
prop:reader("name", nil)
prop:reader("user_id", nil)
prop:reader("setting", nil)
prop:reader("max_count", nil)
prop:reader("token", nil)
prop:reader("index", nil)
prop:accessor("state", nil)
prop:accessor("session", nil)
prop:accessor("amount", nil)
prop:accessor("node", nil)
prop:reader("rpc_server", nil)

function World:__init(id, rpc_server, data)
    self.id = id
    self.rpc_server = rpc_server
    self.type = data.type
    self.name = data.name
    self.user_id = data.user_id
    self.setting = data.setting
    self.state = data.state
    self.max_count = data.max_count
    self.token = data.token
    self.index = data.index
    self.amount = data.amount
end

function World:send(rpc, ...)
    if not self.session then
        log_debug("[World][send] is fail world:{}", self.id)
        return
    end
    local ok,a,b,c = self.rpc_server:send(self.session, rpc, ...)
    if not ok then
        log_debug("[World][send] is fail world:{} rpc:{}-{}-{}-{}", self.id, rpc,a,b,c)
        return
    end
    log_debug("[World][send] is success world:{} rpc:{}", self.id, rpc)
end

return World
