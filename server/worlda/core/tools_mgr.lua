--tools_mgr.lua
local log_debug         = logger.debug
local lb64decode        = ssl.b64_decode

local PUBLIC_KEY               = [[
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCWKUc5BTsvNKLv389mqShFhg7l
HbG8SyyAiHZ5gMMMoBGayBGgOCGXHDRDUabr0E8xFtSApu9Ppuj3frzwRDcj4Q69
yXc/x1+a18Jt96DI/DJEkmkmo/Mr+pmY4mVFk4a7pxnXpynBUz7E7vp9/XvMs84L
DFqqvGiSmW/YKJfsAQIDAQAB
]]

local ToolsMgr = singleton()
local prop = property(ToolsMgr)
prop:reader("rsa_key", nil)

function ToolsMgr:__init()
    self.rsa_key = ssl.rsa_init_pubkey(PUBLIC_KEY)
end

function ToolsMgr:decode_token(token)
    if not token or token == "" then
        return ""
    end
    local decode = lb64decode(token)
    local result = self.rsa_key.pub_decode(decode)
    log_debug("[ToolsMgr][decode_token] decode:{} result:{}", decode, result)
    return result
end

quanta.tools_mgr = ToolsMgr()
return ToolsMgr
