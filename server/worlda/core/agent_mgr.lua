--agent_mgr.lua
local log_err           = logger.err
local log_debug         = logger.debug
local oexec             = os.execute
local tpack             = table.pack
local tunpack           = table.unpack
local tremove           = table.remove
local tinsert           = table.insert
local sformat           = string.format

local event_mgr         = quanta.get("event_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")
local router_mgr        = quanta.get("router_mgr")
local thread_mgr        = quanta.get("thread_mgr")
local tools_mgr         = quanta.get("tools_mgr")

local RpcServer         = import("network/rpc_server.lua")
local World             = import("worlda/core/world.lua")

local WS_ONLINE         = protobuf_mgr:enum("world_state", "WS_ONLINE")
local WS_OFFLINE        = protobuf_mgr:enum("world_state", "WS_OFFLINE")

local CODE_OK           = 0
local CODE_ARGS_ERROR   = 1 --请求参数错误

local AgentMgr = singleton()
local prop = property(AgentMgr)
prop:reader("host", nil)
prop:reader("port", 0)
prop:reader("worlds", {})
prop:reader("index_idle", {})
prop:reader("wait_list", {})    --startup等待列表
prop:reader("max_index", 1)     --world最大索引(自增)

function AgentMgr:__init()
    --创建rpc服务器
    local ip, port = environ.addr("QUANTA_WORLDA_ADDR")
    self.rpc_server = RpcServer(self, ip, port)
    --设置参数
    self.host = environ.get("QUANTA_HOST_IP")
    self.port = port
    --rpc
    event_mgr:add_listener(self, "rpc_world_login")
    event_mgr:add_listener(self, "rpc_player_count")
    event_mgr:add_listener(self, "rpc_proxy_call")
    --indexs
    for i = 0x3fff, 1, -1 do
        tinsert(self.index_idle, i)
    end
end

function AgentMgr:get_index()
    return tremove(self.index_idle)
end

function AgentMgr:get_world(wid)
    return self.worlds[wid]
end

function AgentMgr:get_world_by_index(index)
    for _,world in pairs(self.worlds) do
        if world.index  == index then
            return world
        end
    end
    return nil
end

-- 心跳
function AgentMgr:on_client_beat(client)
end

function AgentMgr:on_client_accept(client)
    log_debug("AgentMgr:on_client_accept: token={}", client.token)
end

function AgentMgr:on_client_register(client, node)
    log_debug("AgentMgr:on_client_register: node={}", node)
    local session_id = self.wait_list[node.index]
    if session_id then
        client.index = node.index
        self.wait_list[node.index] = nil
        thread_mgr:response(session_id, true, node)
    end
    local world = self:get_world_by_index(node.index)
    if not world then
        log_debug("[AgentMgr][on_client_register] world is nil index:{}", node.index)
        return
    end
    world:set_session(client)
end

function AgentMgr:on_client_error(client, token, err)
    log_debug("[AgentMgr][on_client_error] node:{}, token:{}", client.name, token)
    if client.index > 0 then
       tinsert(self.index_idle, client.index)
       for wid, world in pairs(self.worlds) do
           if world.index == client.index then
               self:on_world_logout(wid)
               self.worlds[wid] = nil
               break
           end
       end
       local world = self:get_world_by_index(client.index)
       if not world then
           log_debug("[AgentMgr][on_client_error] world is nil index:{}", client.index)
           return
       end
       world:set_session(nil)
    end
end

function AgentMgr:get_world_addr(index, wid)
    local session_id = thread_mgr:build_session_id()
    self.wait_list[index] = session_id
    return thread_mgr:yield(session_id, wid, 5000)
end

--启动world
function AgentMgr:launch_world(wid, name, index, wtype, max_count, user_id, token)
    log_debug("[AgentMgr][launch_world] wid:{}, name:{} index:{} wtype:{} max_count:{} user_id:{} token:{}",
        wid, name, index, wtype, max_count, user_id, token)
    local tmpl
    local cmd_world
    if quanta.platform == 'linux' then
        tmpl = 'nohup %s > /dev/null 2>&1 &'
        cmd_world = './quanta ./eufaula/world.conf'
    elseif quanta.platform == 'windows' then
        tmpl = 'start %s'
        cmd_world = 'quanta.exe eufaula//world.conf'
    end
    if not (tmpl and cmd_world) then
        return
    end
    local fmt = '%s --index=%d --port=%d --wtype=%d --max=%d --user=%d --wid=%d --wname=%s --wtoken=%s'
    local command = sformat(fmt, cmd_world, index, index, wtype, max_count, user_id, wid, name, token)
    log_debug("AgentMgr:launch_world: command={}", command)
    oexec(sformat(tmpl, command))
end

--停止world
function AgentMgr:stop_world(wid)
    -- TODO: send rpc[stop] to world
end

function AgentMgr:rpc_world_login(data)
    log_debug("[AgentMgr][rpc_world_login] data:{}", data)
    self:allocate_world(data.world_id, data)
    return 0
end

function AgentMgr:on_world_logout(wid)
    local world = self.worlds[wid]
    if world then
        world:set_amount(0)
        world:set_state(WS_OFFLINE)
    end
    event_mgr:notify_trigger("on_world_logout", wid)
    log_debug("[AgentMgr][on_world_logout] wid:{} worlds:{}", wid, self.worlds)
end

function AgentMgr:rpc_player_count(client, wid, count)
    local world = self.worlds[wid]
    if world == nil then
        log_err("[AgentMgr][rpc_player_count] wid:{} count:{}", wid, count)
    else
        world:set_amount(count)
        log_debug("[AgentMgr][rpc_player_count] wid:{} player_count:{}", wid, count)
    end
    event_mgr:notify_trigger("on_world_player_count", wid, count)
    return 0
end

function AgentMgr:rpc_proxy_call(client, rpc, ...)
    log_debug("[AgentMgr][rpc_proxy_call] rpc:{}, data:{}", rpc, {...})
    local res = tpack(router_mgr[rpc](router_mgr, ...))
    return tunpack(res, 2)
end

function AgentMgr:allocate_world(world_id, wdata)
    local token = tools_mgr:decode_token(wdata.token)
    log_debug("[AgentMgr][allocate_world] world_id:{}, wdata:{} token:{}", world_id, wdata, token)
    local world = self.worlds[world_id]
    if not world then
        world = World(world_id, self.rpc_server, {
            type = wdata.type,
            name = wdata.name,
            user_id = wdata.master,
            setting = wdata.setting,
            state = WS_ONLINE,
            max_count = wdata.max_count,
            token = token,
            index = self:get_index(),
            amount = 0,
        })
        self.worlds[world_id] = world
        log_debug("[AgentMgr][allocate_world] launch_world world_id:{}, world:{}", world_id, world)
        self:launch_world(world_id, world.name, world.index, world.type, world.max_count, world.user_id, world.token)
        event_mgr:notify_trigger("on_world_login", world_id)
    else
        world.name = wdata.name
        world.setting = wdata.setting
        world.max_count = wdata.max_count
        world.token = token
        log_debug("[AgentMgr][allocate_world] rpc_world_sync world_id:{}, wdata:{}", world_id, wdata)
        -- 通知world同步更新数据
        world:send("rpc_world_sync", world_id, {
            name = world.name,
            setting = world.setting,
            max_count = world.max_count,
            token = world.token,
        })
    end
    return world
end

function AgentMgr:request_world_addr(data)
    local world = self:allocate_world(data.world_id, data)
    if world and not world.node then
        local ok, waddr = self:get_world_addr(world.index, world.id)
        if not ok then
            return { code = CODE_ARGS_ERROR }
        end
        world:set_node({
            code = CODE_OK,
            host = waddr.host,
            port = waddr.port,
            world_id = world.id,
            name = world.name,
            max_count=world.max_count,
            user_id=world.user_id,
            world_type = world.type,
            is_password = false,
        })
    end
    if world.user_id ~= data.user_id and (world.token and world.token ~= "") then
        world.node.is_password = true
    end
    return world.node
end

quanta.agent_mgr = AgentMgr()

return AgentMgr
