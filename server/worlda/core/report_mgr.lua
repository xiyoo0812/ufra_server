--report_mgr.lua
import("network/http_client.lua")

local log_debug         = logger.debug
local log_info          = logger.info
local log_warn          = logger.warn
local log_err           = logger.errt
local env_get           = environ.get
local env_status        = environ.status
local json_decode       = json.decode
local tinsert           = table.insert
local new_guid          = codec.guid_new
local qsuccess          = quanta.success
local service_id        = quanta.service
local env_number        = environ.number

local agent_mgr         = quanta.get("agent_mgr")
local timer_mgr         = quanta.get("timer_mgr")
local event_mgr         = quanta.get("event_mgr")
local router_mgr        = quanta.get("router_mgr")
local thread_mgr        = quanta.get("thread_mgr")
local http_client       = quanta.get("http_client")
local tools_mgr         = quanta.get("tools_mgr")

local HttpServer        = import("network/http_server.lua")

local RESPONSE_CODE_OK  = 0
-- local RESPONSE_CODE_ARGS_ERROR  = 1 --请求参数错误
local RESPONSE_CODE_NOT_AUTH    = 2 --未验证的请求
local RESPONSE_CODE_TOKEN_EXP   = 3 --token过期
-- local RESPONSE_CODE_RPC_ERROR   = 4 --内部rpc调用错误

local QUANTA_MONITOR    = env_status("QUANTA_MONITOR")

local ReportMgr = singleton()
local prop = property(ReportMgr)
prop:reader("registry_addr", nil)   --registry地址
prop:reader("registry_token", nil)  -- registry token
prop:reader("registry_expire", nil) -- registry token expire time
prop:reader("ready_time", nil) --agent启动时间
prop:reader("agent_id", nil)
prop:reader("host_ip", nil)
prop:reader("agent_url", nil)

function ReportMgr:__init()
    self.agent_id = new_guid()
    self.ready_time = quanta.now
    --创建HTTP服务器
    self.http_server = HttpServer(env_get("QUANTA_WORLDA_HTTP"))
    self.http_server:register_post("/request-world", "on_request_world", self)
    --设置domain
    local nport = self.http_server:get_port()
    self.host_ip = env_get("QUANTA_DOMAIN_ADDR")
    service.modify_host(nport, self.host_ip)
    -- 定期检测上报
    self.agent_url = string.format("http://%s:%s/request-world", self.host_ip, nport)
    self.registry_addr = env_get("QUANTA_AGENT_REPORT", "http://127.0.0.1:10010")
    local interval = env_number("QUANTA_REPORT_INTERVAL", 30000)
    self:check_token()
    timer_mgr:loop(interval, function()
        self:check_token()
        self:call_report()
    end)
    event_mgr:add_trigger(self, "on_world_login")
    event_mgr:add_trigger(self, "on_world_logout")
    event_mgr:add_trigger(self, "on_world_player_count")
end

function ReportMgr:on_world_login(wid)
    log_debug("[ReportMgr][on_world_login] wid:{}", wid)
    local world = agent_mgr:get_world(wid)
    if not world then
        log_warn("[ReportMgr][on_world_login] wid:{}", wid)
        return
    end
    local worlds = { { id = wid, state = world.state } }
    self:call("/world/change_state", "rpc_agent_state", worlds)
end

function ReportMgr:on_world_logout(wid)
    self:on_world_login(wid)
end

function ReportMgr:on_world_player_count(wid, count)
    local worlds = { { id = wid, amount = count } }
    self:call("/world/player_count", "rpc_agent_sync", worlds)
end

function ReportMgr:call(url, rpc, data)
    if QUANTA_MONITOR then
        local ok, code, result = router_mgr:call_worldm_hash(service_id, rpc, { id = quanta.id, worlds = data })
        if not qsuccess(code, ok) then
            log_warn('[ReportMgr]:[rpc_report] rpc_agent_heartbeat failed, ok: {}, code: {}, result: {}', ok, code, result)
        end
        return
    end
    self:send_registry_post(url, data)
end


function ReportMgr:call_report()
    local worlds = {}
    for wid, world in pairs(agent_mgr:get_worlds()) do
        tinsert(worlds, {
            id = wid,
            state = world.state,
            amount = world.amount or 0,
        })
    end
    self:report(worlds)
end

function ReportMgr:on_request_world(url, body, params, headers)
    log_debug("ReportMgr:on_request_world: url={}, body={}, params={}, headers={}", url, body, params, headers)
    local result = agent_mgr:request_world_addr(body)
    log_debug("[ReportMgr][on_request_world] result:{}", result)
    return result
end

--检测token是否过期
function ReportMgr:check_token()
    if QUANTA_MONITOR or (not self.registry_addr) then
        -- 未配置上报地址或者是官方服(有monitor)时，不需要获取token
        return
    end
    if not self.registry_expire or not self.registry_token then
        -- 未获取过token时，获取token
        self:request_token()
        return
    end
    if self.registry_expire and self.registry_expire > self.ready_time then
        -- 在token过期时间不足30分钟时，刷新一次token
        if self.registry_expire - quanta.now <= 1800 then
            self:refresh_token()
        end
    end
end

function ReportMgr:send_registry_post(url_path, body, callback, fail_callback)
    local url = self.registry_addr .. url_path
    local headers = {}
    thread_mgr:entry(url, function()
        if self.registry_token then
            headers["Token"] = self.registry_token
        end
        logger.dump('headers: {}', headers)
        local ok, status, response = http_client:call_post(url, body, headers)
        if not ok then
            log_warn('[ReportMgr]:[send_registry_post] request [{}] failed, status: {}, response: {}', url, status, response)
            if type(fail_callback) == 'function' then
                fail_callback(self)
            end
            return
        end
        if type(callback) == 'function' then
            local resp = json_decode(response)
            if resp and resp.code == RESPONSE_CODE_OK then
                callback(self, resp)
            else
                log_warn('[ReportMgr]:[send_registry_post] request [{}] failed, response: {}', url, response)
                if type(fail_callback) == 'function' then
                    fail_callback(self, resp)
                end
            end
        end
    end)
end

--汇报
function ReportMgr:report(worlds)
    local body = {
        id = self.registry_token or quanta.id,
        host = self.host_ip,
        agent_url = self.agent_url,
        worlds = worlds,
    }
    if QUANTA_MONITOR then
        -- 是官方服(有monitor)时, 采用RPC方式上报
        self:rpc_report(body)
        return
    end
    self:send_registry_post("/registry/push", body, self.report_callback, function(this, resp)
        if resp == nil or resp.code == RESPONSE_CODE_NOT_AUTH then
            this:request_token()
        elseif resp.code == RESPONSE_CODE_TOKEN_EXP then
            this:refresh_token()
        end
    end)
end

function ReportMgr:rpc_report(body)
    local ok, code, result = router_mgr:call_worldm_hash(service_id, "rpc_agent_heartbeat", body)
    if not qsuccess(code, ok) then
        log_warn('[ReportMgr]:[rpc_report] rpc_agent_heartbeat failed, ok: {}, code: {}, result: {}', ok, code, result)
    end
end

--汇报回调
function ReportMgr:report_callback(response)
    if response.code == 0 then
        log_info("[ReportMgr][report_callback] success!")
        logger.dump("[ReportMgr][report_callback] response: {}", response)
    else
        log_err("[ReportMgr][report_callback] failed! code:{} msg:{}", response.code, response.msg)
    end
end

--请求token
function ReportMgr:request_token()
    local body = {
        id = quanta.id,
        agent_id = self.agent_id,
        agent_url = self.agent_url
    }
    self:send_registry_post("/registry/get_token", body, self.request_token_callback)
end

--请求token回调
function ReportMgr:request_token_callback(response)
    if response.token and response.exp_time then
        self.registry_token = tools_mgr:decode_token(response.token)
        self.registry_expire = response.exp_time
        logger.dump("[ReportMgr][request_token_callback] token: {}", self.registry_token)
    end
end

--刷新token
function ReportMgr:refresh_token()
    local body = { }
    self:send_registry_post("/registry/refresh_token", body, self.refresh_token_callback, self.refresh_token_failed)
end

--刷新token回调
function ReportMgr:refresh_token_callback(response)
    logger.dump("[ReportMgr][refresh_token_callback] response: {}", response)
    if response.token and response.exp_time then
        self.registry_expire = response.exp_time
        self.registry_token = tools_mgr:decode_token(response.token)
    end
end

function ReportMgr:refresh_token_failed()
    -- 请求刷新token失败后, token可能已失效, 需要重新请求token
    self.registry_expire = nil
    self.registry_token = nil
end

quanta.report_mgr = ReportMgr()

return ReportMgr
