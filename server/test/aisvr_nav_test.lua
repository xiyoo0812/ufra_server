--aisvr_nav_test.lua
-- --------------------------------------------------------------------------------------
---@type nav_geometry
local nav_geometry          = import('common/nav_geometry.lua')

---@type MonsterAiEntity
local MonsterAiEntity = import('aisvr/object/monster_ai_entity.lua')

local dir1 = {x=1,y=0,z=1}
logger.dump('dir1: ' .. nav_geometry.vector3_to_str(dir1))

local angle = nav_geometry.dir_to_euler_angle(dir1)
logger.dump('angle: ' .. tostring(angle))

local dir2 = nav_geometry.euler_angle_to_dir(angle)
logger.dump('dir2: ' .. nav_geometry.vector3_to_str(dir2))

local angle2 = nav_geometry.dir_to_euler_angle(dir2)
logger.dump('angle2: ' .. tostring(angle2))
logger.dump('angle2: ' .. tostring(math.floor(angle2)))

-- --------------------------------------------------------------------------------------
local ENTITY_MONSTER = 3
local speed = 155
local spawn_pos = {x=7200, y=0, z=-10000}

local entity = MonsterAiEntity(1005, 12345, ENTITY_MONSTER, 30001, spawn_pos, 0, 666, 666, speed, spawn_pos, nil, 1005)

entity:nav_move({
    1,
    spawn_pos,
    {x=19500, y=0, z=-7400},
    {x=39600, y=0, z=-14300},
    {x=37300, y=0, z=-24300},
    {x=26300, y=0, z=-42700},
    {x=66000, y=0, z=-52500},
})
