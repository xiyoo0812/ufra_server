--aisvr_navmesh_test.lua
-- --------------------------------------------------------------------------------------
---@type nav_geometry
local nav_geometry  = import('common/nav_geometry.lua')

import('aisvr/core/nav_mgr.lua')

---@type NavMgr
local nav_mgr = quanta.nav_mgr

local map_id = 1003
local pos = {x=27195,y=22007,z=-27626}
-- local map_id = 1005
-- local pos = {x=2360,y=1,z=-2730}
local nav_pos = {x=-pos.x,y=pos.y,z=pos.z}
local can_nav = nav_mgr:can_nav(map_id, nav_pos)
logger.dump('pos={}, can_nav={}', pos, can_nav)


local pos_start = { x = 26810, y = 22101, z = -31150, }
-- x传入detour时需要正负取反
local nav_start = { x = -pos_start.x, y = pos_start.y, z = pos_start.z, }

local pos_target = { x = 38820, y = 22093, z = -38040, }
-- x传入detour时需要正负取反
local nav_target = { x = -pos_target.x, y = pos_target.y, z = pos_target.z, }

local is_ok, x, y, z = nav_mgr:find_valid_point(map_id, nav_start, nav_target)
logger.dump('pos_start={}, pos_target={}->', pos_start, pos_target)
logger.dump('is_ok={}, target valid_point: {{x={}, y={}, z={}}}', is_ok, -(x or 0), y, z)

local new_nav_pos = { x=-124492, y=12457-500, z=85095, }
local new_pos = nav_mgr:find_ground_point(map_id, new_nav_pos, 600)
local distance = new_pos and nav_geometry.calc_distance(new_nav_pos, new_pos)
logger.dump('new_nav_pos={} --> {}: {}', new_nav_pos, new_pos, distance)
