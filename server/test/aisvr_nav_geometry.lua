--aisvr_nav_geometry.lua
--------------------------------------------------------------------------------------
import('aisvr/core/nav_mgr.lua')
---@type NavMgr
local nav_mgr = quanta.nav_mgr

---@type nav_geometry
local nav_geometry          = import('common/nav_geometry.lua')

local map_id = 1003
local pos = {x=27195,y=22007,z=-27626}
-- local map_id = 1005
-- local pos = {x=2360,y=1,z=-2730}

local nav_pos = {x=-pos.x,y=pos.y,z=pos.z}
local can_nav = nav_mgr:can_nav(map_id, nav_pos)
logger.dump('pos={}, can_nav={}', pos, can_nav)

local counter = 99
local radius = 1500
local distance_list = {}
local failed_count = 0
for i = 1, counter, 1 do
    local point = nav_mgr:around_point(map_id, nav_pos, radius)
    if not point then
        -- logger.dump('point: {}', point)
        failed_count = failed_count + 1
        goto continue
    end
    -- logger.dump('point: {}', point)
    local around = {x=-point.x,y=point.y,z=point.z}
    local distance = nav_geometry.calc_distance(pos, around)
    -- local idx = string.rep('0', string.len(tostring(counter)) - string.len(tostring(i))) .. tostring(i)
    -- logger.dump('[{}] radius={}, distance={}', idx, radius, distance)
    table.insert(distance_list, math.floor(distance))
    :: continue ::
end
logger.dump('failed_count: {}', failed_count)
table.sort(distance_list, function(a, b) return a > b end)
local limit = 10
for i, distance in ipairs(distance_list) do
    local idx = string.rep('0', string.len(tostring(limit)) - string.len(tostring(i))) .. tostring(i)
    logger.dump('[{}] radius={}, distance={}', idx, radius, distance)
    if i > 10 then
        break
    end
end