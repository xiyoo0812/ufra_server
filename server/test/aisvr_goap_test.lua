--goap_test.lua
-- --------------------------------------------------------------------------------------
import("common/constant.lua")
import("aisvr/core/goap_mgr_v2.lua")
-- --------------------------------------------------------------------------------------
local log_warn      = logger.warn
-- local log_dump      = logger.dump
-- local log_info      = logger.info
local log_debug     = logger.debug

---@type GoapMgrV2
local goap_mgr_v2       = quanta.get("goap_mgr_v2")

---@type GoapContext
local GoapContext = import("aisvr/goap/goap_context.lua")

-- --------------------------------------------------------------------------------------

local monster_id = 30003
local entity_id = 32345
local ai_cfg = nil
local owner_entity = {
    ['hp'] = 100,
    ['is_suppressing'] = false,
    ['state'] = 'goto_spawn',
    ['is_activated'] = true,
    ['is_hp_full'] = false,
    ['standoff_countdown'] = 0,
    ['is_skill_using'] = false,
    ['is_skill_available'] = true,
    ['can_standoff_horizontal'] = false,
    ['can_use_skill'] = true,
}
local owner_spawn = {
    ['distance'] = 2100,
}
local owner_enemy = {
    ['distance'] = 22080,
    ['hp'] = 10,
}
local context = GoapContext(entity_id, ai_cfg, owner_entity, owner_spawn, owner_enemy)
local goal_state = 'goto_spawn'
local plan_result = goap_mgr_v2:plan(monster_id, goal_state, context)
-- 判断plan是否规划成功
if plan_result ~= nil and plan_result:is_valid() then
    -- 显示plan
    local action_names = {}
    for _, action in ipairs(plan_result.actions) do
        table.insert(action_names, '[ ' .. action.name .. ' ]')
    end
    log_warn('plan: ' .. table.concat(action_names, ' -> '))
else
    log_debug('goal: {}, context: {}', goal_state, context)
    log_warn('[aisvr_goap_test] monster_id={}, plan_result is nil or empty, state=[{} -> {}]', monster_id, owner_entity['state'], goal_state)
end
