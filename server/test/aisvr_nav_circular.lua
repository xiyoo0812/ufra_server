--aisvr_nav_circular.lua
-- --------------------------------------------------------------------------------------
---@type nav_geometry
local nav_geometry          = import('common/nav_geometry.lua')

-- 定义点A和点B的坐标
local pointA = { x = 0, y = 0, z = 1 }
local pointB = { x = 3, y = 2, z = 7 }

-- 计算C点的坐标
local distance_to_A = 20
local pointCenter = nav_geometry.online_at_pos(pointA, pointB, distance_to_A, true)

logger.dump('pointA: ' .. nav_geometry.vector3_to_str(pointA))
logger.dump('pointB: ' .. nav_geometry.vector3_to_str(pointB))
logger.dump('pointCenter: ' .. nav_geometry.vector3_to_str(pointCenter))

-- 计算2.5秒后怪物的坐标
local duration = 2.5
local new_pos = nav_geometry.circular_move2(pointA, pointCenter, distance_to_A, 15, duration, true)

logger.dump('new_pos: ' .. nav_geometry.vector3_to_str(new_pos))
logger.dump('pointA->pointCenter: ' .. tostring(nav_geometry.calc_distance(pointA, pointCenter)))
logger.dump('new_pos->pointCenter: ' .. tostring(nav_geometry.calc_distance(new_pos, pointCenter)))

local pos = { x = 0, y = 0, z = 1 }
local center = { x = 0, y = 0, z = 0 }
local new_pos2 = nav_geometry.circular_move2(pos, center, 1, math.pi/2, 1, false)
logger.dump('new_pos: ' .. nav_geometry.vector3_to_str(new_pos2))
