--test.lua
-- quanta.exe local\test.conf
import("kernel.lua")

quanta.startup(function()--初始化test
    -- require("socket.core") -- 加载luasocket库
    -- require("LuaPanda").start("127.0.0.1", 8818) -- 连接调试器
    --加载协议
    import("kernel/protobuf_mgr.lua")
    --加载测试
    -- import("test/aisvr_goap_test.lua")
    -- import("test/aisvr_monster_ai_test.lua")
    -- import("test/aisvr_nav_test.lua")
    -- import("test/aisvr_nav_geometry.lua")
    import("test/aisvr_navmesh_test.lua")
    -- import("test/aisvr_nav_circular.lua")
end)