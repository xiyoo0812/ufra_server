--bgip_servlet.lua

local log_debug                 = logger.debug
local sformat                   = string.format
local guid_encode               = codec.guid_encode
local qfailed                   = quanta.failed

local event_mgr                 = quanta.get("event_mgr")
local protobuf_mgr              = quanta.get("protobuf_mgr")
local player_mgr                = quanta.get("player_mgr")
local store_mgr                 = quanta.get("store_mgr")
local cache_agent               = quanta.get("cache_agent")

local UiNode                    = enum("UiNode")

local FRAME_SUCCESS             = protobuf_mgr:error_code("FRAME_SUCCESS")
local EXEC_ERR                  = protobuf_mgr:error_code("BGIP_EXEC_ERR")
local NOT_ONLINE_ERR            = protobuf_mgr:error_code("BGIP_USER_NOT_ONLINE_ERR")
local ACCOUTN_BANS              = protobuf_mgr:error_code("KICK_ACCOUTN_BANS")

-- bgip服务
local BgipRole = singleton()
function BgipRole:__init()
    self:setup()
end

function BgipRole:setup()
    -- rpc消息
    event_mgr:add_listener(self, "on_bgip_updaterolename")
    event_mgr:add_listener(self, "on_bgip_punishrole")
    event_mgr:add_listener(self, "on_bgip_kickall")
    event_mgr:add_listener(self, "on_bgip_finishtask")
    event_mgr:add_listener(self, "on_bgip_pushreddot")
    event_mgr:add_listener(self, "on_bgip_kickout")
    event_mgr:add_listener(self, "on_bgip_accepttask")
end

--踢玩家下线
function BgipRole:on_bgip_kickout(bgip_common, bgip_req)
    local player_id = tonumber(bgip_req.role_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return NOT_ONLINE_ERR, {msg = "player not online"}
    end
    player_mgr:kick_out(player, player_id)
    return FRAME_SUCCESS, { msg = "success" }
end

--推送红点
function BgipRole:rpc_bgip_pushreddot(bgip_common, bgip_req)

    local player_id = tonumber(bgip_req.role_ids)
    local num = bgip_req.num
    local player = player_mgr:get_entity(player_id)
    if not player then
        return NOT_ONLINE_ERR, {msg = "player not online"}
    end
    local rid = UiNode.UINI_HALL_MEMU_CUSTOMER
    if num > 0 then
        player:add_new_redot(rid, num)
    else
        player:del_reddot_by_rid(rid)
    end
    return FRAME_SUCCESS, { ec = FRAME_SUCCESS, em = "success" }
end

--完成任务
function BgipRole:on_bgip_finishtask(_, bgip_req)

    local player_id = tonumber(bgip_req.role_id)
    local task_id = bgip_req.task_id

    local player = player_mgr:get_entity(player_id)
    if not player then
        return NOT_ONLINE_ERR, {msg = "player not online"}
    end
    local task = player:get_task(task_id)
    if not task then
        return EXEC_ERR, { msg = "task not exist!"}
    end
    log_debug("[BgipRole][rpc_bgip_finishtask] player({}), task_id({})!", player_id, task_id)
    task:complete()
    return FRAME_SUCCESS, { msg = "success" }
end

function BgipRole:on_bgip_accepttask(bgip_common, bgip_req)

    local player_id = tonumber(bgip_req.role_id)
    local task_id = bgip_req.task_id
    local player = player_mgr:get_entity(player_id)
    if not player then
        return NOT_ONLINE_ERR, {msg = "player not online"}
    end
    local task = player:get_task(task_id)
    if task then
        if task:is_init() then
            task:active()
        end
        return FRAME_SUCCESS, { msg = "success" }
    end
    local story = player:get_accept_task(task_id)
    if story then
        player:receive_task(story, task_id)
        player:sync_task(story)
    else
        return EXEC_ERR, { msg = "create task error" }
    end
    return FRAME_SUCCESS, { msg = "success" }
end

--踢全服玩家下线
function BgipRole:on_bgip_kickall()
    player_mgr:kick_all()
    return FRAME_SUCCESS, { msg = "success" }
end

--离线修改角色名
function BgipRole:offline_change_name(player_id, open_id, name)
    store_mgr:load_impl(open_id, "player")
    local _, data = store_mgr:load_impl(open_id, "account")

    local role = data.roles[player_id]
    local commits = {{{},'roles',{[player_id]={custom = role.custom, user_id = role.user_id, name = name, gender = role.gender}}}}
    local code, _ = cache_agent:update(open_id, "account", commits)
    if qfailed(code) then
        return EXEC_ERR, {msg = "update failed"}
    end

    commits = {{{},nil,{nick=name}}}
    code, _ = cache_agent:update(player_id, "player", commits)
    if qfailed(code) then
        return EXEC_ERR, {msg = "update failed"}
    end
    return FRAME_SUCCESS
end

--玩家角色处罚
function BgipRole:on_bgip_punishrole(_, bgip_req )
    --处罚
    local player_id = tonumber(bgip_req.roleid)
    local punish_info_id = bgip_req.punish_info_id
    -- local operate_type = bgip_req.operate_type
    -- local extra = bgip_req.extra
    local ban_time = bgip_req.ban_time

    local player = player_mgr:get_entity(player_id)
    if not player then
        --屏蔽昵称
        return self:random_role_name(punish_info_id, player_id, ban_time)
    end
    --封禁账号，sdk已闭环处理,这里需要踢下线
    if punish_info_id == 1001 or punish_info_id == 1002 or
            punish_info_id == 1009 or punish_info_id == 1003 or
            punish_info_id == 1075 then
        player:send("NID_POPUP_NTF", {error_code = ACCOUTN_BANS})
        player_mgr:kick_out(player, player_id)
        return FRAME_SUCCESS, {roleid = sformat("%s", player_id), ban_time = ban_time or 0}
    end
    --强制屏蔽昵称
    if punish_info_id == 1011 then
        local role_name = guid_encode()
        player:update_name(role_name)
    end

    return FRAME_SUCCESS, {roleid = sformat("%s", player_id), ban_time = ban_time or 0}
end

--修改角色名
function BgipRole:on_bgip_updaterolename(_, bgip_req)

    local player_id     = tonumber(bgip_req.role_id)
    local current_name  = bgip_req.current_name
    log_debug("[BgipRole][rpc_bgip_updaterolename]->player_id:{}, current_name:{}", player_id, current_name)
    local player = player_mgr:get_entity(player_id)
    if not player then
        --离线
        local ok, data = store_mgr:load_impl(player_id, "player")
        if not ok  or not data then
            return EXEC_ERR,{msg = "load data failed"}
        end
        local open_id = data.open_id
        self:offline_change_name(player_id, open_id, current_name)
        return FRAME_SUCCESS, {msg = "success"}
    end
    --在线
    player:update_name(current_name)
    log_debug("[BgipRole][rpc_bgip_updaterolename]->player_id:{}, current_name:{} update success", player_id, current_name)
    return FRAME_SUCCESS, {msg = "success"}
end

-----------------------内部-----------------------
function BgipRole:random_role_name(punish_info_id, player_id, ban_time)
    if punish_info_id == 1011 then
        --离线
        local ok, data = store_mgr:load_impl(player_id, "player")
        if not ok  or not data then
            return EXEC_ERR,{msg = "load data failed"}
        end
        local role_name = guid_encode()
        local open_id = data.open_id
        self:offline_change_name(player_id, open_id, role_name)
        return FRAME_SUCCESS, {roleid = sformat("%s", player_id), ban_time = ban_time or 0}
    end
    return NOT_ONLINE_ERR, {msg = "player not online"}
end
quanta.bgip_role = BgipRole()

return BgipRole