--bgip_mgr.lua
import("basic/cmdline.lua")
import("agent/online_agent.lua")

local lb64decode            = ssl.b64_decode
local tunpack               = table.unpack
local log_debug             = logger.debug
local jdecode               = json.decode
local json_encode           = json.encode
local sformat               = string.format
local slower                = string.lower

local event_mgr             = quanta.get("event_mgr")
local protobuf_mgr          = quanta.get("protobuf_mgr")
local gm_mgr                = quanta.get("gm_mgr")

local FRAME_SUCCESS         = protobuf_mgr:error_code("FRAME_SUCCESS")
local EXEC_ERR              = protobuf_mgr:error_code("BGIP_EXEC_ERR")
local PARAMS_ERR            = protobuf_mgr:error_code("BGIP_PARAMS_ERR")

local BgipMgr = singleton()
local prop = property(BgipMgr)
prop:reader("exec_cmds", {})        --已执行指令
prop:reader("http_server", nil)

function BgipMgr:__init()
    --创建HTTP服务器
   gm_mgr:register_post("/bgip", "on_bytedance", self)
end

--http 回调
----------------------------------------------------------------------
--字节调用，json格式
function BgipMgr:on_bytedance(url, req_body)
    log_debug("[BgipMgr][on_bytedance] body: {} \n\n", req_body)
    --TODO 验签 白名单等等
    local body = lb64decode(req_body)
    local ok, cmd_req = pcall(jdecode, body)
    if not ok then
        return self:build_res(PARAMS_ERR, sformat("parse failed. body:%s", body))
    end
    local bgip_req = cmd_req.command_request
    local bgip_common = cmd_req.common
    log_debug("[BgipMgr][on_bytedance] cmd_req: {}", cmd_req)

    --转为center内部命令
    local command = sformat("on_bgip_%s", slower(bgip_common.command))
    local result = event_mgr:notify_listener(command, bgip_common, bgip_req)
    local call_ok, status_ok, res = tunpack(result)
    --调用失败
    if not call_ok then
        return self:build_res(EXEC_ERR, sformat("rpc execute failed. call_ok:%s", call_ok))
    end
    --处理失败
    if status_ok ~= FRAME_SUCCESS then
        return self:build_res(status_ok, sformat("execute failed. reason:%s", res.msg))
    end
    --成功
    return self:build_res(FRAME_SUCCESS, "success", res)
end

--构建应答参数
function BgipMgr:build_res(code, msg, res)
    local resp = {
        common = {
            result_code = code,
            error_msg   = msg
        },
        command_response = res
    }
    local resp_json = json_encode(resp, 1)
    log_debug("[BgipMgr][build_res] resp_json:{}", resp_json)
    return resp_json
end

quanta.bgip_mgr = BgipMgr()

return BgipMgr