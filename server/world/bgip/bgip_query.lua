--bgip_servlet.lua
local log_debug                 = logger.debug
local tinsert                   = table.insert
local sformat                   = string.format
local qfailed                   = quanta.failed
local json_encode               = json.encode
local log_err                   = logger.err
local task_mgr                  = quanta.get("task_mgr")
local event_mgr                 = quanta.get("event_mgr")
local protobuf_mgr              = quanta.get("protobuf_mgr")
local store_mgr                 = quanta.get("store_mgr")
local config_mgr                = quanta.get("config_mgr")
local player_mgr                = quanta.get("player_mgr")
local mongo_agent               = quanta.get("mongo_agent")

local PACKET_ITEM               = protobuf_mgr:enum("packet_type", "PACKET_ITEM")
local PACKET_EQUIP              = protobuf_mgr:enum("packet_type", "PACKET_EQUIP")

local npc_db                    = config_mgr:init_table("npc", "id")
local building_db               = config_mgr:init_table("building", "id")
local item_db                   = config_mgr:get_table("item")
local FRAME_SUCCESS             = protobuf_mgr:error_code("FRAME_SUCCESS")
local EXEC_ERR                  = protobuf_mgr:error_code("BGIP_EXEC_ERR")

-- bgip服务
local BgipServlet = singleton()
function BgipServlet:__init()
    self:setup()
end

function BgipServlet:setup()
    -- rpc消息
    event_mgr:add_listener(self, "on_bgip_accountinfo")
    event_mgr:add_listener(self, "on_bgip_playerspecificinfo")
    -- 停服
    event_mgr:add_listener(self, "rpc_server_close")
end

--停服
function BgipServlet:rpc_server_close()
    --所有玩家下线
    player_mgr:kick_all()
    --立即保存玩家数据
    return FRAME_SUCCESS, { msg = "success" }
end

--玩家信息查询
function BgipServlet:on_bgip_playerspecificinfo(_, request)
    --查询类直接走数据库
    local player_id = tonumber(request.role_id)
    local query_target = request.query_target

    print(player_id, query_target)
    if query_target == "role_info" then
        return self:query_player_info(player_id)
    elseif query_target == "bag_info" then
        return self:query_player_bag(player_id)
    elseif query_target == "hero_info" then
        return self:query_hero_info(player_id)
    elseif query_target == "building_info" then
        return self:query_building_info(player_id)
    elseif query_target == "equip_info" then
        return self:query_equip_info(player_id)
    elseif query_target == "task_info" then
        return self:query_task_info(player_id)
    elseif query_target == "task_cond" then
        return self:query_task_cond(player_id)
    end
end

function BgipServlet:on_bgip_accountinfo(_, request)

    local open_id = request.open_id
    local ok, code, data = mongo_agent:find({ "player", { open_id = open_id }})
    if qfailed(code, ok) then
        log_err("[BgipServlet][rpc_bgip_accountinfo] open_id: {} find failed! code: {}, res: {}", open_id, code, data)
        return EXEC_ERR, { msg = "查询信息失败"}
    end
    for _, item in ipairs(data) do
        item.routers = nil
    end
    return FRAME_SUCCESS, data
end

function BgipServlet:query_equip_info(player_id)
    local exec_ok, info_list = self:query_bag(player_id, PACKET_EQUIP)
    if not exec_ok then
        return exec_ok, info_list
    end
    local info_map = {key_name = "equip_info",key_desc = "装备信息", info_list = info_list}
    return FRAME_SUCCESS, {info_map = {info_map}}
end

--查询玩家建筑信息
function BgipServlet:query_building_info(player_id)
    log_debug("[BgipServlet][query_hero_info] player:{}", player_id)
    local ok ,data = store_mgr:load_impl(player_id, "home_building")
    if not ok or not data then
        return EXEC_ERR,{msg = "load data failed"}
    end
    local building_info = { }
    if data and data.buildings then
        for uuid, building in pairs(data.buildings) do
            local proto_id = building.proto_id
            local building_conf = building_db:find_one(proto_id)
            if building_conf then
                local info = {
                    uuid = uuid,
                    proto_id = proto_id,
                    level = building_conf.level,
                    name = building_conf.cn_name,
                    status = building.status,
                    group_id = building_conf.group,
                    desc = sformat("原型ID:%s 等级:%s 名字:%s 状态:%s 组ID:%s", proto_id,
                            building_conf.level, building_conf.desc,  building.status, building_conf.group)
                }
                local partner_info = {
                    info_key = sformat("%s", uuid),
                    info_key_desc = json_encode(info),
                    info_value = "1"
                }
                tinsert(building_info, partner_info)
            end
        end
    end
    log_err("{}", building_info)
    local info_map = {key_name = "building_info",key_desc = "建筑信息", info_list = building_info}
    return FRAME_SUCCESS, {info_map = {info_map}}
end

--查询玩家npc信息
function BgipServlet:query_hero_info(player_id)
    log_debug("[BgipServlet][query_hero_info] player:{}", player_id)
    local ok ,data = store_mgr:load_impl(player_id, "player_partner")
    if not ok or not data then
        return EXEC_ERR,{msg = "load data failed"}
    end
    --封装data
    local info_list = {}
    if data and data.partners then
        local partners = data.partners
        for uuid, partner in pairs(partners) do
            local partner_conf = npc_db:find_one(partner.proto_id)
            local npc_info = {
                uuid = uuid,
                name = partner_conf.name,
                proto_id = partner.proto_id,
                building_id = partner.building_id,
                state = partner.state,
                level = partner_conf.leve,
                star_lv = partner.star_lv,
                exp = partner.exp,
                affinity_lv = partner.affinity_lv,
                desc = sformat("%s(%s) 原型ID:%s 工作建筑ID:%s 状态:%s 等级:%s 星级:%s 好感等级:%s 经验:%s",
                        partner_conf.name, uuid, partner.proto_id, partner.building_id, partner.state, partner_conf.level,
                        partner.star_lv, partner.affinity_lv, partner.exp)
            }

            local partner_info = {
                info_key = sformat("%s", partner_conf.name),
                info_key_desc = json_encode(npc_info),
                info_value = "1"
            }
            tinsert(info_list, partner_info)
        end
    end
    local info_map = {key_name = "hero_info",key_desc = "npc信息", info_list = info_list}
    return FRAME_SUCCESS, {info_map = {info_map}}
end

--查询玩家背包信息
function BgipServlet:query_bag(player_id, type)
    local ok, data = store_mgr:load_impl(player_id, "player_item")
    if not ok or not data then
        return EXEC_ERR,{msg = "load data failed"}
    end
    --封装data
    local info_list = {}
    if data and data.packets then
        local packets = data.packets
        --背包
        local bag_info = packets[type]
        local capacity = bag_info.capacity
        local capacity_info = {
            info_key = "capacity",
            info_key_desc = sformat("容量:%s", capacity),
            info_value = sformat("%s", capacity)
        }
        tinsert(info_list, capacity_info)

        for item_id, item_num in pairs(bag_info.items or {}) do
            local item_conf = item_db:find_one(item_id)
            local item_info = {
                info_key = sformat("%s", item_id),
                info_key_desc = sformat("名称:%s*%s", item_conf.name, item_num),
                info_value = sformat("%s",item_num)
            }
            tinsert(info_list, item_info)
        end
    end
    return FRAME_SUCCESS, info_list
end

--查询玩家道具背包信息
function BgipServlet:query_player_bag(player_id)
    local exec_ok, info_list = self:query_bag(player_id, PACKET_ITEM)
    if not exec_ok then
        return exec_ok, info_list
    end
    local info_map = {key_name = "bag_info",key_desc = "背包信息", info_list = info_list}
    return FRAME_SUCCESS, {info_map = {info_map}}
end

--查询玩家基础信息
function BgipServlet:query_player_info(player_id)
    local ok ,data = store_mgr:load_impl(player_id, "player")
    if not ok or not data then
        return EXEC_ERR,{msg = "load data failed"}
    end
    local player = data
    local attr_ok, attr_data = store_mgr:load_impl(player_id, "player_attr")
    if not attr_ok then
        return EXEC_ERR,{msg = "load attr data failed"}
    end
    local player_attr = attr_data.attrs
    log_debug("[BgipServlet][query_player_info] attr_data:{}", attr_data)
    if not player_attr then
        return EXEC_ERR,{msg = "load attr data failed"}
    end
    --封装data
    local info_list = {}
    --昵称
    local nick_info = {info_key = "nick", info_key_desc = "昵称", info_value = player.nick }
    tinsert(info_list, nick_info)
    --性别
    local gender_info = {info_key = "gender", info_key_desc = "性别", info_value = sformat("%s",player.gender)}
    tinsert(info_list, gender_info)
    --等级
    local level_info = {info_key = "level", info_key_desc = "等级", info_value =  sformat("%s", player_attr[129] or 0) }
    tinsert(info_list, level_info)
    --hp
    local hp_info = {info_key="hp", info_key_desc="生命", info_value= sformat("%s", player_attr[1] or 0 )}
    tinsert(info_list, hp_info)
    --金币
    local coin_info = {info_key="coin", info_key_desc="金币", info_value = sformat("%s", player_attr[10] or 0)}
    tinsert(info_list, coin_info)
    --钻石
    local diamond_info = {info_key="diamond", info_key_desc="钻石", info_value = sformat("%s", player_attr[12] or 0)}
    tinsert(info_list, diamond_info)
    --幸运值
    local lucky_info = {info_key="lucky", info_key_desc="幸运值", info_value = sformat("%s", player_attr[14] or 0)}
    tinsert(info_list, lucky_info)
    --能量
    local energy_info = {info_key="energy", info_key_desc="能量", info_value = sformat("%s", player_attr[2] or 0)}
    tinsert(info_list, energy_info)
    return FRAME_SUCCESS, {info_list = info_list}
end

function BgipServlet:query_task_info(player_id)
    local ok ,data = store_mgr:load_impl(player_id, "player_task")
    if not ok or not data then
        return EXEC_ERR,{msg = "load data failed"}
    end
    local task_list = { }
    if data and data.tasks then
        for task_id, task_info in pairs(data.tasks) do
            tinsert(task_list, {
                task_id = task_id,
                type = task_info.type,
                status = task_info.status,
                child_id = task_info.child_id,
                target = json_encode(task_info.targets)
            })
        end
    end
    return FRAME_SUCCESS, task_list
end

function BgipServlet:query_task_cond(player_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return EXEC_ERR,{msg = "player not online"}
    end
    return FRAME_SUCCESS, task_mgr:get_task_cond_list(player)
end

quanta.bgip_servlet = BgipServlet()

return BgipServlet