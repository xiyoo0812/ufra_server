--utensil.lua
local qfailed         = quanta.failed

local object_fty      = quanta.get("object_fty")
local protobuf_mgr    = quanta.get("protobuf_mgr")
local event_mgr       = quanta.get("event_mgr")
local SUCCESS         = protobuf_mgr:error_code("FRAME_SUCCESS")
local FAILED          = protobuf_mgr:error_code("FRAME_FAILED")

local COST_BUILDING   = protobuf_mgr:enum("cost_reason", "NID_COST_BUILDING")
local OBTAIN_BUILDING = protobuf_mgr:enum("obtain_reason", "NID_OBTAIN_BUILDING")

local Utensil         = class()
local prop            = property(Utensil)
prop:reader("id", nil)
prop:reader("prototype", nil)
prop:accessor("scene", nil)  --scene
prop:accessor("master", nil) --master

local dbprop = db_property(Utensil, "home_utensil")
dbprop:store_value("proto_id", 0)
dbprop:store_value("detail", "") --detail
dbprop:store_value("name", nil)

function Utensil:__init(id, conf)
    self.id = id
    self.prototype = conf
    self.proto_id = conf.id
end

--从DB加载
function Utensil:load_db(master, data)
    self.master = master
    self.name = data.name
    self.detail = data.detail
end

function Utensil:get_type()
    return self.prototype.type
end

function Utensil:get_struct_type()
    return self.prototype.struct_type
end

function Utensil:get_tab()
    return self.prototype.tab
end

function Utensil:is_worn()
    return self.prototype.worn
end

function Utensil:startup(town, detail)
    self.scene = town
    self.detail = detail
    self.master = town:get_master()
end

function Utensil:place_costs()
    return { cost = { [self.prototype.item_id] = 1 }, reason = COST_BUILDING }
end

function Utensil:tackback_obtain()
    return { drop = { [self.prototype.item_id] = 1 }, reason = OBTAIN_BUILDING }
end

--拆除扣除繁荣度
function Utensil:takeback_town_exp()
    return -self.prototype.drop_town_exp
end

--检查能否升级
function Utensil:check_upgrade()
    if self.prototype.upgrade_id then
        return SUCCESS
    end
    return FAILED
end

--获取关联组
function Utensil:get_group()
    return self.prototype.group
end

--获取个数上限
function Utensil:get_limit()
    return self.prototype.limit
end

function Utensil:get_upcost(conf)
    return conf.costs or {}
end

function Utensil:update()
end

function Utensil:is_working()
    return false
end

function Utensil:check_takeback(player)
    if object_fty:is_storebox(self.prototype) then
        local code = player:execute_check_packet(self.id)
        if qfailed(code) then
            return code
        end
    end
    return SUCCESS
end

-- 升级
function Utensil:upgrade(player, proto_id, detail)
    if detail and detail ~= "" then
        self:save_detail(detail)
    end
    local upgrade_id = self.prototype.upgrade_id
    self.prototype = object_fty:find_utensil(upgrade_id)
    self:save_proto_id(upgrade_id)
    self:on_upgrade(player)
    self:sync_changed()
end

-- 修复
function Utensil:repair(player, proto_id, detail)
    self:upgrade(player, proto_id, detail)
end

function Utensil:update_detail(detail)
    self:save_detail(detail)
    self:sync_changed()
end

function Utensil:display_detail(scene, player, opid)
    scene:save_utensils_elem(self.id, self)
    self:on_display(player)
    self:sync_changed(opid)

    --掉落繁荣度
    local drop_town_exp = self.prototype.drop_town_exp
    if drop_town_exp then
        event_mgr:notify_trigger("on_town_exp_drop", player, drop_town_exp, OBTAIN_BUILDING)
    end
    return true
end

function Utensil:takeback_detail(scene, player)
    scene:del_utensils_elem(self.id)
    self:on_takeback(player)
    self:sync_destory()
    --扣除繁荣度
    local cost_town_exp = self:takeback_town_exp()
    if cost_town_exp then
        event_mgr:notify_trigger("on_town_exp_cost", player, cost_town_exp, COST_BUILDING)
    end
    return true
end

function Utensil:sync_changed(opid)
    local datas = { utensils = { [self.id] = self:pack2db() }, master = self.master, part_sync = true, opid = opid }
    self.scene:broadcast_message("NID_UTENSIL_LIST_NTF", datas)
end

function Utensil:sync_destory()
    local datas = { utensils = { [self.id] = { proto_id = 0 } }, master = self.master, part_sync = true }
    self.scene:broadcast_message("NID_UTENSIL_LIST_NTF", datas)
end

function Utensil:pack2db()
    return {
        name = self.name,
        detail = self.detail,
        proto_id = self.proto_id
    }
end

--子类实现
----------------------------------------------------------
function Utensil:on_upgrade(player)
    if object_fty:is_storebox(self.prototype) then
        player:execute_update_packet(self.id, self.prototype.args)
    end
end

function Utensil:on_display(player)
    if object_fty:is_storebox(self.prototype) then
        player:execute_add_packet(self.id, self.prototype.args)
    end
end

function Utensil:on_takeback(player)
    if object_fty:is_storebox(self.prototype) then
        player:execute_remove_packet(self.id)
    end
end

return Utensil
