--goap_mgr.lua
local log_warn           = logger.warn
local log_debug          = logger.debug

local event_mgr          = quanta.get("event_mgr")
local config_mgr         = quanta.get("config_mgr")

local cfg_monster_ai     = config_mgr:init_table("monster_ai", "monster_id")

---@type MonsterAiConfig
local MonsterAiConfig = import("world/ai/object/monster_ai_config.lua")

---@class GoapMgr
---@field ai_config_map table<integer, MonsterAiConfig> ai配置集合
local GoapMgr = singleton()
local prop = property(GoapMgr)
prop:reader("ai_config_map", {})

function GoapMgr:__init()
    --配置初始化
    self:on_cfg_monster_ai_changed(true)
    --事件监听
    event_mgr:add_trigger(self, "on_cfg_monster_ai_changed")
end

---状态数据预处理(读取/更新配置表)
---@param is_first_run boolean 是否首次运行
function GoapMgr:on_cfg_monster_ai_changed(is_first_run)
    self.ai_config_map = {}
    log_debug('[GoapMgr]:[on_cfg_monster_ai_changed()]')
    for monster_id, conf in cfg_monster_ai:iterator() do
        local config = MonsterAiConfig.from_config(monster_id, conf)
        if config then
            self.ai_config_map[monster_id] = config
        end
    end
    if not is_first_run then
        event_mgr:notify_trigger("on_ai_config_reload")
    end
end

---@param monster_id integer 怪物Id
---@param current_state string 当前状态
---@param initial_state GoapContext 初始上下文状态
---@return GoapPlan
function GoapMgr:plan(monster_id, current_state, initial_state)
    local ai_config = self:get_ai_config(monster_id)
    if not ai_config then
        return nil
    end
    local ai_state = ai_config.states[current_state]
    if ai_state and ai_state.enable_goal then
        initial_state:with_base_config(ai_config.base_config)
        -- log_warn("[GoapMgr]:[plan] monster_id={}, current_state={}", monster_id, current_state)
        return ai_config.planner:plan(initial_state, ai_state.enable_goal)
    end
end

---@param monster_id integer 怪物Id
---@return MonsterAiConfig
function GoapMgr:get_ai_config(monster_id)
    if monster_id and type(monster_id) == 'number' then
        local ai_config = self.ai_config_map[monster_id]
        if ai_config and ai_config.monster_id == monster_id and ai_config.states ~= nil then
            return ai_config
        end
        local reason = ai_config == nil and "not found" or "invalid"
        log_warn("[GoapMgr]:[get_ai_config] ai_config for monster_id={}: {}", monster_id, reason)
        return nil
    end
    log_warn("[GoapMgr]:[get_ai_config] parameter 'monster_id' is invalid: {}", monster_id)
    return nil
end

quanta.goap_mgr = GoapMgr()

return GoapMgr
