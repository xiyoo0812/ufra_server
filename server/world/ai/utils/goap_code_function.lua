--goap_code_function.lua
local log_debug  = logger.debug

local SHOW_LOG = false

---@class GoapCodeFunction
---@field activate function
---@field standby function
---@field move_wander function
---@field move_to_enemy function
---@field move_to_spawn function
---@field standoff_back function
---@field standoff_horizontal function
---@field attack_enemy function
---@field fully_hp function
local GoapCodeFunction = singleton()

---激活AI
---@param context GoapContext
---@param entity AiComponent|AiHateComponent|AiNavComponent|AiGoapComponent
function GoapCodeFunction.activate(context, entity)
    if SHOW_LOG then
        log_debug('[GoapCodeFunction] entity({}) call action.func: activate()', entity:get_id())
    end
    entity:activate(true)
end

---原地待机
---@param context GoapContext
---@param entity AiComponent|AiHateComponent|AiNavComponent|AiGoapComponent
function GoapCodeFunction.standby(context, entity)
    if SHOW_LOG then
        log_debug('[GoapCodeFunction] entity({}) call action.func: standby()', entity:get_id())
    end
end

---漫游待机移动
---@param context GoapContext
---@param entity AiComponent|AiHateComponent|AiNavComponent|AiGoapComponent
function GoapCodeFunction.move_wander(context, entity)
    if SHOW_LOG then
        log_debug('[GoapCodeFunction] entity({}) call action.func: move_wander()', entity:get_id())
    end
    entity:move_wander()
end

---移动到敌人位置
---@param context GoapContext
---@param entity AiComponent|AiHateComponent|AiNavComponent|AiGoapComponent
function GoapCodeFunction.move_to_enemy(context, entity)
    if SHOW_LOG then
        log_debug('[GoapCodeFunction] entity({}) call action.func: move_to_enemy()', entity:get_id())
    end
    entity:move_to_enemy()
end

---移动到出生点
---@param context GoapContext
---@param entity AiComponent|AiHateComponent|AiNavComponent|AiGoapComponent
function GoapCodeFunction.move_to_spawn(context, entity)
    if SHOW_LOG then
        log_debug('[GoapCodeFunction] entity({}) call action.func: move_to_spawn()', entity:get_id())
    end
    entity:move_to_spawn()
end

---对峙后退
---@param context GoapContext
---@param entity AiComponent|AiHateComponent|AiNavComponent|AiGoapComponent
function GoapCodeFunction.standoff_back(context, entity)
    if SHOW_LOG then
        log_debug('[GoapCodeFunction] entity({}) call action.func: standoff_back()', entity:get_id())
    end
    entity:standoff_back()
end

---对峙横向移动
---@param context GoapContext
---@param entity AiComponent|AiHateComponent|AiNavComponent|AiGoapComponent
function GoapCodeFunction.standoff_horizontal(context, entity)
    if SHOW_LOG then
        log_debug('[GoapCodeFunction] entity({}) call action.func: standoff_horizontal()', entity:get_id())
    end
    entity:standoff_horizontal()
end

---对敌人进行攻击
---@param context GoapContext
---@param entity AiComponent|AiHateComponent|AiNavComponent|AiGoapComponent
function GoapCodeFunction.attack_enemy(context, entity)
    if SHOW_LOG then
        log_debug('[GoapCodeFunction] entity({}) call action.func: attack_enemy()', entity:get_id())
    end
    -- 攻击
    entity.is_skill_using = entity:attack_hate_entity()
end

---血量回满
---@param context GoapContext
---@param entity AiComponent|AiHateComponent|AiNavComponent|AiGoapComponent
function GoapCodeFunction.fully_hp(context, entity)
    if SHOW_LOG then
        log_debug('[GoapCodeFunction] entity({}) call action.func: fully_hp()', entity:get_id())
    end
    entity:fully_hp()
end

---战斗站立
---@param context GoapContext
---@param entity AiComponent|AiHateComponent|AiNavComponent|AiGoapComponent
function GoapCodeFunction.combat_standby(context, entity)
    if SHOW_LOG then
        log_debug('[GoapCodeFunction] entity({}) call action.func: combat_standby()', entity:get_id())
    end
end

---沉默站立
---@param context GoapContext
---@param entity AiComponent|AiHateComponent|AiNavComponent|AiGoapComponent
function GoapCodeFunction.silence_standby(context, entity)
    if SHOW_LOG then
        log_debug('[GoapCodeFunction] entity({}) call action.func: silence_standby()', entity:get_id())
    end
end

return GoapCodeFunction
