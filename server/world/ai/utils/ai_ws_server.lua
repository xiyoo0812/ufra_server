--ai_ws_server.lua
local log_debug         = logger.debug
local log_warn          = logger.warn
local json_encode       = json.encode
local json_decode       = json.decode

local WSServer          = import("network/ws_server.lua")

---@type AiMgr
local ai_mgr    = quanta.get("ai_mgr")
local event_mgr         = quanta.get("event_mgr")

local AiWsServer = class(WSServer)
local prop = property(AiWsServer)
prop:reader("token_entity_map", nil) --token与附加的entity映射

function AiWsServer:__init(ws_addr)
    WSServer.setup(self, ws_addr)
    local WheelMap = import("container/wheel_map.lua")
    self.token_entity_map = WheelMap(10)
    --事件监听
    event_mgr:add_trigger(self, "on_attach_ai_notify")
    event_mgr:add_trigger(self, "on_attach_ai_added")
    event_mgr:add_trigger(self, "on_attach_ai_removed")
end

function AiWsServer:on_socket_error(socket, token, err)
    WSServer.on_socket_error(self, socket, token, err)
    log_debug('[AiWsServer][on_socket_error] token: {}, err: {}', token, err)
    self:on_entity_detach(socket, nil, nil, token)
end

function AiWsServer:on_socket_recv(wsocket, token, message)
    if message == 'ping' then
        wsocket:send_frame("pong")
        return
    end
    local parse_ok, parsed = pcall(json_decode, message)
    if not parse_ok then
        log_warn("[AiWsServer][on_socket_recv] client(token:{}) json_decode(message) failed: {}!", token, parsed)
        wsocket:send_frame(json_encode({
            code = 1,
            msg = "json decode error: " .. parsed
        }))
        return
    end
    -- logger.dump('parsed: {}', parsed)
    if parsed.action == nil then
        log_warn("[AiWsServer][on_socket_recv] client(token:{}) action is nil!", token)
        wsocket:send_frame(json_encode({
            code = 2,
            msg = "action is nil"
        }))
        return
    elseif parsed.action == 'monster_ai_attach' then
        self:on_entity_attach(wsocket, parsed.data.scene_id, parsed.data.entity_id, token)
        return
    elseif parsed.action == 'monster_ai_detach' then
        self:on_entity_detach(wsocket, parsed.data.scene_id, parsed.data.entity_id, token)
        return
    end
    local entity = self.token_entity_map:get(token)
    if not entity then
        wsocket:send_frame(json_encode({
            code = 11,
            msg = "entity not found"
        }))
        return
    end
    self:on_ws_request(wsocket, parsed, token, entity)
end

function AiWsServer:on_ws_request(wsocket, parsed, token)
    log_debug('[AiWsServer][on_ws_request] parsed: {}, token: {}', parsed, token)
end

function AiWsServer:on_entity_attach(wsocket, scene_id, entity_id, token)
    log_debug('[AiWsServer:on_entity_attach] scene_id: {}, entity_id: {}, token: {}', scene_id, entity_id, token)
    if type(scene_id) == 'string' then
        scene_id = tonumber(scene_id)
    end
    if type(entity_id) == 'string' then
        entity_id = tonumber(entity_id)
    end
    local entity = ai_mgr:get_ai_entity(scene_id, entity_id)
    if entity then
        self.token_entity_map:set(token, entity)
        entity:attach(token)
    else
        wsocket:send_frame(json_encode({
            code = 12,
            msg = "entity not found"
        }))
    end
end

function AiWsServer:on_entity_detach(wsocket, scene_id, entity_id, token)
    log_debug('[AiWsServer:on_entity_detach] scene_id: {}, entity_id: {}, token: {}', scene_id, entity_id, token)
    if token == nil then
        return
    end
    if type(scene_id) == 'string' then
        scene_id = tonumber(scene_id)
    end
    if type(entity_id) == 'string' then
        entity_id = tonumber(entity_id)
    end
    local entity = self.token_entity_map[token]
    if not entity and (scene_id and entity_id) then
        entity = ai_mgr:get_ai_entity(scene_id, entity_id)
    end
    if entity then
        entity:detach(false)
    end
    self.token_entity_map:set(token, nil)
end

function AiWsServer:on_attach_ai_notify(token, action, data)
    if not token then
        return
    end
    local wsocket = self.clients[token]
    if not wsocket then
        return
    end
    data.action = action
    wsocket:send_frame(json_encode({
        code = 0,
        data = data,
    }))
end

function AiWsServer:on_attach_ai_added(scene_id, entity_id, scene_holder, scene_map_id)
    local ws_data = json_encode({
        code = 0,
        data = {
            action = 'monster_ai_added',
            scene_id = tostring(scene_id),
            scene_holder = tostring(scene_holder),
            scene_map_id = scene_map_id,
            entity_id = tostring(entity_id),
        },
    })
    for token, wsocket in pairs(self.clients) do
        if wsocket and wsocket.alive then
            wsocket:send_frame(ws_data)
        end
    end
end

function AiWsServer:on_attach_ai_removed(scene_id, entity_id, token, is_attaching)
    log_debug('[AiDebugMgr:on_attach_ai_removed] token: {}', token)
    local ws_data = json_encode({
        code = 0,
        data = {
            action = 'monster_ai_removed',
            scene_id = tostring(scene_id),
            entity_id = tostring(entity_id),
        },
    })
    for tk, wsocket in pairs(self.clients) do
        if wsocket and wsocket.alive then
            wsocket:send_frame(ws_data)
            if token and tk == token and is_attaching then
                -- wsocket:close() -- 关闭socket(暂时不关，前端体验更好)
                self:on_entity_detach(wsocket, scene_id, entity_id, token)
            end
        end
    end
end

return AiWsServer
