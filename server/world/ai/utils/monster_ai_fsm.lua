--monster_ai_fsm.lua
-- local log_debug     = logger.debug
local log_warn      = logger.warn
local tinsert       = table.insert

---@class MonsterAiFsm
---@field states table<string, GoapState>
---@field current GoapState
---@field start_ms integer
---@field idle_timeout integer
local MonsterAiFsm = class()
local prop = property(MonsterAiFsm)
prop:reader("states", {})
prop:reader("current", nil)
prop:reader("start_ms", nil) -- 当前状态开始时间
prop:reader("idle_timeout", nil) -- 非战斗待机状态改变限制(秒)
prop:reader("can_repeat_state_names", {
    ['standoff'] = true,
    ['rest_idle'] = true,
    ['wander_idle'] = true,
}) -- 可重复的状态名称

---@param ai_state_map table<integer, GoapState>
function MonsterAiFsm:__init(ai_state_map, idle_timeout)
    self:init_states(ai_state_map)
    self.idle_timeout = idle_timeout
end

function MonsterAiFsm:is_running()
    return self.start_ms ~= nil and self.current ~= nil
end

---@param ai_state_map table<string, GoapState>
function MonsterAiFsm:init_states(ai_state_map)
    self.states = {}
    for state_name, state in pairs(ai_state_map) do
        self.states[state_name] = state
    end
end

function MonsterAiFsm:has_state(state_name)
    return self.states[state_name] ~= nil
end

---@return boolean
function MonsterAiFsm:transition_to(state_name)
    local new_state = self.states[state_name]
    if new_state ~= nil then
        -- log_debug('[MonsterAiFsm][transition_to] new state is [{}]', state_name)
        self.current = new_state
        self.start_ms = quanta.now_ms
        return true
    end
    return false
end

---@param context GoapContext
function MonsterAiFsm:run_forward(context)
    local next_state = nil
    local need_next = false
    if not self:is_running() then
        need_next = true
        next_state = self:get_next_state(context, true)
        log_warn('[MonsterAiFsm][run_forward] not is_running')
    elseif self:should_transition(context) then
        need_next = true
        next_state = self:get_next_state(context, true)
    end
    if next_state ~= nil then
        return self:transition_to(next_state.name), false
    elseif need_next then
        log_warn('[MonsterAiFsm][run_forward] no next state, current: {}', (self.current or {}).name)
    end
    if need_next then
        need_next = not self.can_repeat_state_names[(self.current or {}).name or '']
    end
    return false, need_next
end

---@param context GoapContext
---@return boolean
function MonsterAiFsm:should_transition(context)
    local goap_state = self.states[self.current.name]
    if goap_state:transition_match(context) then
        return true
    end
    local has_hate_target = context.owner_enemy ~= nil and context.owner_enemy['entity_id'] ~= nil
    local diff_s = (quanta.now_ms - self.start_ms) / 1000
    return (not has_hate_target) and (diff_s > self.idle_timeout)
end

---@param context GoapContext
---@param can_repeat boolean
---@return GoapState[]
function MonsterAiFsm:filter_available(context, can_repeat)
    local state_list = {}
    for name, state in pairs(self.states) do
        -- 下一次的状态应该跟当前不同
        if self.current ~= nil and state.name == self.current.name and not (can_repeat and self.can_repeat_state_names[name]) then
            goto continue
        end
        if state:enter_match(context) then
            -- log_debug('[MonsterAiFsm:filter_available] filter_available: {}', name)
            tinsert(state_list, state)
        end
        :: continue ::
    end
    return state_list
end

---@param context GoapContext
---@param can_repeat boolean
---@return GoapState
function MonsterAiFsm:get_next_state(context, can_repeat)
    local state_list = self:filter_available(context, can_repeat)
    if #state_list == 0 then
        return nil
    end
    -- 根据优先级(同优先级时考虑权重)
    local weight = 0
    local priority = 0
    local index = 1
    for idx, state in ipairs(state_list) do
        if state.priority < priority then
            goto continue
        end
        -- 更高优先级
        if state.priority > priority then
            priority = state.priority
            weight = state.weight
            index = idx
            goto continue
        end
        -- 更高权重
        if state.weight > weight then
            weight = state.weight
            index = idx
        end
        :: continue ::
    end
    return state_list[index]
end

return MonsterAiFsm
