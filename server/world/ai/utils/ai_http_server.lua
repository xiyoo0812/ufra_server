--ai_http_server.lua
-- local log_debug     = logger.debug
local HttpServer    = import("network/http_server.lua")

local AiHttpServer = class(HttpServer)

function AiHttpServer:on_socket_recv(socket, method, url, params, headers, body)
    -- log_debug("[HttpServer][on_socket_recv] recv: {}, {}, {}, {}, {}!", method, url, params, headers, body)
    local handlers = self.handlers[method]
    if not handlers then
        self:response(socket, 404, "this http method hasn't suppert!")
        return
    end
    self:on_http_request(handlers, socket, url, body, params, headers)
end

return AiHttpServer
