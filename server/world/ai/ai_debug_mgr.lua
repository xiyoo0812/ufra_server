--ai_debug_mgr.lua
local log_debug         = logger.debug
local log_warn          = logger.warn
local env_get           = environ.get
local env_status        = environ.status
local json_encode       = json.encode
local iopen             = io.open
local ioclose           = io.close
local tinsert           = table.insert

local AiHttpServer      = import("world/ai/utils/ai_http_server.lua")
local AiWsServer        = import("world/ai/utils/ai_ws_server.lua")

---@type AiMgr
local ai_mgr            = quanta.get("ai_mgr")

local AiDebugMgr = singleton()

function AiDebugMgr:__init()
    if not env_status("QUANTA_AI_DEBUG_ENABLED") then
        return
    end
    --服务初始化
    self:init_http_server()
    self:init_ws_server()
end

function AiDebugMgr:init_http_server()
    --创建HTTP服务器
    local sync_addr = env_get("QUANTA_AI_SYNC_ADDR", "127.0.0.1:14001")
    local server = AiHttpServer(sync_addr)
    -- 注册处理逻辑
    server:register_post("/monster_ai_sync", "http_monster_ai_sync", self)
    server:register_post("/monster_ai_scene_entities", "http_monster_ai_scene_entities", self)
    self.http_server = server
end

function AiDebugMgr:init_ws_server()
    --创建WS服务器
    local debug_addr = env_get("QUANTA_AI_DEBUG_ADDR", "127.0.0.1:14002")
    local server = AiWsServer(debug_addr)
    server.on_ws_request = function(wserver, wsocket, message, token, entity)
        self:on_ws_request(wserver, wsocket, message, token, entity)
    end
    self.ws_server = server
end

function AiDebugMgr:on_ws_request(wserver, wsocket, message, token, entity)
    log_debug("[AiDebugMgr][on_ws_request] client(token: {}) msg:{}!", token, message)
    local response = {
        code = 0,
        msg = '',
    }
    -- 检查处理器
    local handler = self['ws_' .. message.action]
    if handler == nil then
        response.code = 3
        response.msg = "action not found"
    end
    if response.code ~= 0 then
        wsocket:send_frame(json_encode(response))
        return
    end
    -- 处理请求
    if response.data ~= nil then
        if type(response.data.scene_id) == 'string' then
            response.data.scene_id = tonumber(response.data.scene_id)
        end
        if type(response.data.entity_id) == 'string' then
            response.data.entity_id = tonumber(response.data.entity_id)
        end
    end
    handler(self, response, message.data, entity)
    -- 发送响应
    local payload = json_encode(response)
    wsocket:send_data(0x01, payload)
end

---获取GOAPContext
---@param response table
---@param data table
---@param entity table
function AiDebugMgr:ws_monster_ai_dump_context(response, data, entity)
    local context = entity:get_context()
    context.id = tostring(context.id)
    local scene = entity:get_scene()
    local scene_id = scene:get_id()
    local entity_id = entity:get_id()
    response.data = {
        action = 'monster_ai_dump_context',
        scene_id = tostring(scene_id),
        entity_id = tostring(entity_id),
        context = context,
    }
end

function AiDebugMgr:http_monster_ai_sync(url, body, params, headers)
    local luaContent = body["lua"]
    local response = {
        code = 0,
        msg = "success",
        data = {}
    }
    if not luaContent then
        response.code = 1
        response.msg = "lua content is nil"
    else
        local configFilePath = package.searchpath( "config.monster_ai_cfg", package.path)
        local f = iopen(configFilePath, "r")
        if f ~= nil and ioclose(f) then
            f = iopen(configFilePath, "w+")
            f:write(luaContent)
            ioclose(f)
        else
            response.code = 2
            response.msg = "config/monster_ai_cfg.lua not exists"
        end
    end
    return json_encode(response)
end

function AiDebugMgr:http_monster_ai_scene_entities(url, body, params, headers)
    local proto_id = body["proto_id"]
    local response = {
        code = 0,
        msg = "success",
        data = {}
    }
    if not proto_id then
        response.code = 1
        response.msg = "lua content is nil"
    else
        response.data = {rows = {}}
        -- 注: 前端JS中对于 long 类型的数据会有精度问题, 所以需要转成 string 类型
        local scene_entities = ai_mgr:get_scene_entities(proto_id, true)
        for scene_id, scene_info in pairs(scene_entities) do
            tinsert(response.data.rows, {
                scene_id = tostring(scene_id),
                scene_holder = tostring(scene_info.scene_holder),
                scene_map_id = scene_info.scene_map_id,
                entities = scene_info.entities,
            })
        end
        log_warn('[AiDebugMgr:http_monster_ai_scene_entities] proto_id: {}, response: {}', proto_id, response)
    end
    return json_encode(response)
end

quanta.ai_debug_mgr = AiDebugMgr()

return AiDebugMgr
