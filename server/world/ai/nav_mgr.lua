--ai_mgr.lua
local create_mesh   = detour.create_mesh
local iopen         = io.open

-- local log_debug     = logger.debug
local log_warn      = logger.warn
local log_err       = logger.err
local tinsert       = table.insert

local config_mgr    = quanta.get("config_mgr")
local map_db        = config_mgr:init_table("map", "map_id")

local nav_geometry  = import('common/nav_geometry.lua')

---@class NavMgr
local NavMgr = singleton()
local prop = property(NavMgr)
prop:reader("navmeshs", {}) -- navmeshs

function NavMgr:__init()
    self:setup()
end

--初始化
function NavMgr:setup()
    -- 加载 navmesh
    self.navmeshs = {}

    for _, conf in map_db:iterator() do
        local map_id = conf.map_id
        local navmesh_path = conf.navmesh
        if not navmesh_path then
            -- 跳过未配置 navmesh 的地图
            goto continue
        end
        local navmesh_file = navmesh_path:gsub("^{}*(.-){}*$", "%1") -- 去首尾空格
        local file  = iopen(navmesh_file, "rb")
        if not file then
            log_err("open navmesh bin file({}) failed!", navmesh_file)
            goto continue
        end
        local content = file:read("*all")
        file:close() -- 关闭文件
        local mesh = create_mesh(content, #content)
        if not mesh then
            log_err("create navmesh(file: {}) failed!", navmesh_file)
            goto continue
        end
        -- log_debug("load navmesh(file: {}) success, tiles : {}!", navmesh_file, mesh.get_max_tiles())
        local max_nodes = 256
        local scale = 100 -- 米->厘米
        local query = mesh.create_query(max_nodes, scale)
        if not query then
            log_err("create navmesh(file: {})  query failed!", navmesh_file)
            goto continue
        end
        -- 文件路径->文件名->去掉后缀名
        local file_base = navmesh_file:gsub("(.*/)(.*)", "%2"):gsub("(.*)%.(.*)", "%1")
        -- 缓存mesh和相关的区域边界(目前地形俯视图都是矩形)
        self.navmeshs[map_id] = {
            ['map_id'] = map_id,
            ['name'] = file_base,
            ['mesh'] = mesh,
            ['query'] = query,
        }
        ::continue::
    end
end

---寻路
---@param map_id integer
---@param start vector3 起点
---@param target vector3 终点
---@return vector3[]
function NavMgr:find_path(map_id, start, target)
    local navmesh = self.navmeshs[map_id]
    if navmesh then
        local count, result = navmesh['query'].find_path(start.x, start.y, start.z, target.x, target.y, target.z)
        if count > 0 and count * 3 == #result then
            -- 将一维位置数组转为点数组
            local points = {}
            for idx = 1, #result, 3 do
                local x = result[idx]
                local y = result[idx + 1]
                local z = result[idx + 2]
                tinsert(points, { x = x, y = y, z = z, })
            end
            return points
        end
    end
end

---生成一个随机可寻路点
---@param map_id integer
function NavMgr:random_point(map_id)
    local navmesh = self.navmeshs[map_id]
    if navmesh then
        local x, y, z = navmesh['query'].random_point()
        if x == false and y == nil and z == nil then
            return nil
        end
        return { x = x, y = y, z = z, }
    end
end

---生成一个指定圆心，指定半径内的随机可寻路坐标点
---@param map_id integer
---@param pos vector3 圆心
---@param radius integer 半径(单位: cm)
function NavMgr:around_point(map_id, pos, radius)
    local navmesh = self.navmeshs[map_id]
    if navmesh then
        local x, y, z = navmesh['query'].around_point(pos.x, pos.y, pos.z, radius)
        if x == false and y == nil and z == nil then
            -- log_warn('[NavMgr:around_point] failed: radius={}, x={}, y={}, z={}', radius, x, y, z)
            return nil
        end
        local new_pos = { x = x, y = y, z = z, }
        local distance = nav_geometry.calc_distance(pos, new_pos)
        if distance > radius then
            log_warn('[NavMgr:around_point] failed: distance({}) > radius({})', distance, radius)
            return nil
        end
        return new_pos
    end
end

---判断点是否可寻路
---@param map_id integer
function NavMgr:can_nav(map_id, pos)
    local navmesh = self.navmeshs[map_id]
    if navmesh then
        return navmesh['query'].point_valid(pos.x , pos.y, pos.z)
    end
    return false
end

---射线
---@param map_id integer
function NavMgr:raycast(map_id, start_pos, end_pos)
    local navmesh = self.navmeshs[map_id]
    if navmesh then
        return navmesh['query'].raycast(start_pos.x, start_pos.y, start_pos.z, end_pos.x, end_pos.y, end_pos.z)
    end
    return false
end

---寻找目标点对应的有效点
---@param map_id integer
---@param start_pos vector3 起始点
---@param end_pos vector3 目标点
function NavMgr:find_valid_point(map_id, start_pos, end_pos, is_along_surface)
    local navmesh = self.navmeshs[map_id]
    if navmesh then
        return navmesh['query'].find_valid_point(start_pos.x, start_pos.y, start_pos.z, end_pos.x, end_pos.y, end_pos.z, (is_along_surface or false))
    end
    return false
end

---查找点所在的地面点
---@param map_id integer
---@param start_pos vector3 查找点
---@param y_offset integer 偏移量(单位: cm)
function NavMgr:find_ground_point(map_id, pos, y_offset)
    local navmesh = self.navmeshs[map_id]
    if navmesh then
        local x, y, z = navmesh['query'].find_ground_point(pos.x, pos.y, pos.z, y_offset)
        if x == false or y == nil or z == nil then
            return nil
        end
        return { x = x, y = y, z = z, }
    end
    return false
end

quanta.nav_mgr = NavMgr()

return NavMgr
