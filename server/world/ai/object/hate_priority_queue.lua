--hate_priority_queue.lua
-- 仇恨仇恨值队列
local qtkarray = qtable.tkarray
local tremove  = table.remove
local tinsert  = table.insert
local tsort    = table.sort
local log_warn = logger.warn

---@class HatePriorityQueue 仇恨值队列
---@field elements HateEntity[] 队列元素
---@field entity_map table<integer, integer> entity_id到elements的index映射
local HatePriorityQueue = class()

function HatePriorityQueue:__init()
    self.elements = {}
    self.entity_map = {}
end

--- [优先级队列通用方法]
---入队列
---@param element HateEntity 入队元素
function HatePriorityQueue:push(element, not_resort)
    -- 将元素插入到数组中，并按照仇恨值排序
    tinsert(self.elements, element)
    if not_resort then
        return
    end
    self:resort()
end

---出队列
---@return HateEntity 出队元素
function HatePriorityQueue:pop(not_resort)
    -- 移除仇恨值最大的元素并返回
    local removed = tremove(self.elements, 1)
    if not not_resort then
        self:resort()
    end
    return removed
end

---队列是否为空
---@return boolean 队列是否为空
function HatePriorityQueue:empty()
    -- 判断队列是否为空
    return self:count() <= 0
end

---队列数量
function HatePriorityQueue:count()
    return #self.elements
end

---队列-首元素
function HatePriorityQueue:first()
    if self:empty() then
        return nil
    end
    return self.elements[1]
end

---队列-尾元素
function HatePriorityQueue:last()
    if self:empty() then
        return nil
    end
    return self.elements[self:count()]
end

---清空队列
function HatePriorityQueue:clear()
    while not self:empty() do
        self:pop(true)
    end
    self.entity_map = {}
end

--- [扩展方法]
---重排序
function HatePriorityQueue:resort()
    tsort(self.elements, function(a, b)
        return (a.hate_value or 0) > (b.hate_value or 0)
    end)
    self.entity_map = {}
    for idx, entity in ipairs(self.elements) do
        self.entity_map[entity.entity_id] = idx
    end
end

function HatePriorityQueue:force_set(idx, element, not_resort)
    local e_idx = self.entity_map[element.entity_id]
    if not e_idx or e_idx ~= idx then
        return false
    end
    self.elements[idx] = element
    if not not_resort then
        self:resort()
    end
    return true
end

---插入或更新
function HatePriorityQueue:insert_or_update(element, not_resort)
    local idx = self.entity_map[element.entity_id]
    if not idx then
        self:push(element, true)
    else
        self.elements[idx] = element
    end
    if not_resort then
        return
    end
    self:resort()
end

---查找元素
---@return integer, HateEntity
function HatePriorityQueue:find_entity(entity_id)
    for idx, entity in ipairs(self.elements) do
        if entity.entity_id == entity_id then
            return idx, entity
        end
    end
    return nil, nil
end

---迭代
function HatePriorityQueue:iterator()
    local index = nil
    local elements = self.elements
    local function iter()
        index = next(elements, index)
        if index then
            return index, elements[index]
        end
    end
    return iter
end

---移除元素
function HatePriorityQueue:remove_entity(entity_id, not_resort)
    local idx = self.entity_map[entity_id]
    if not idx then
        log_warn("[HatePriorityQueue][remove_entity] entity_id={} not found", entity_id)
        return
    end
    tremove(self.elements, idx)
    if not_resort then
        return
    end
    self:resort()
end

--判断元素是否存在
function HatePriorityQueue:has_entity(entity_id)
    return self.entity_map[entity_id] ~= nil
end

---更新元素的hate_value
function HatePriorityQueue:update_hate(entity_id, hate_value, not_resort)
    local idx = self.entity_map[entity_id]
    if not idx then
        return
    end
    self.elements[idx].hate_value = hate_value
    if not_resort then
        return
    end
    self:resort()
end

function HatePriorityQueue:keys()
    return qtkarray(self.entity_map)
end

return HatePriorityQueue
