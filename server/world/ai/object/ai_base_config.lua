--ai_base_config.lua
---@class AiBaseConfig
---@field remark string                    备注
---@field wander boolean                   能否漫游待机
---@field amuse boolean                    能否娱乐待机
---@field standoff_duration integer[]      对峙总时长
---@field standoff_back_distance integer   对峙保持距离
---@field standoff_back_precision integer  对峙距离精度
---@field standoff_move_duration integer[] 横向移动时长
---@field standoff_move_radius integer     横向移动半径
---@field wander_radius integer            漫游半径
---@field hate_attenuate integer           仇恨衰减率
---@field return_spawn_speed integer       回出生点速度
---@field chase_speed integer              追击速度
---@field chase_distance integer           追击距离
---@field chase_scope integer              追击范围
---@field nearby_enemy_distance integer    接近敌人(判定)距离
---@field nearby_spawn_distance integer    接近出生点(判定)距离
---@field guard_circle_radius integer      警戒圆形半径
---@field guard_sector_radius integer      警戒扇形半径
---@field guard_sector_angle integer       警戒扇形角度
---@field combat_delivery_radius integer   战斗响应传递圆形半径
---@field combat_delivery_duration integer 战斗响应持续触发时间
---@field combat_delivery_amount integer   战斗响应触发次数
---@field patrol_type integer              巡逻类型
---@field patrol_path vector3[]            巡逻路径
---@field state_duration integer[]         状态持续时间
local AiBaseConfig = class()
local prop = property(AiBaseConfig)
prop:reader("remark", nil)                   --备注
prop:reader("wander", nil)                   --能否漫游待机
prop:reader("amuse", nil)                    --能否娱乐待机
prop:reader("standoff_duration", nil)        --对峙总时长
prop:reader("standoff_back_distance", nil)   --对峙保持距离
prop:reader("standoff_back_precision", nil)  --对峙距离精度
prop:reader("standoff_move_duration", nil)   --横向移动时长
prop:reader("standoff_move_radius", nil)     --横向移动半径
prop:reader("wander_radius", nil)            --漫游半径
prop:reader("hate_attenuate", nil)           --仇恨衰减率
prop:reader("return_spawn_speed", nil)       --回出生点速度
prop:reader("chase_speed", nil)              --追击速度
prop:reader("chase_distance", nil)           --追击距离
prop:reader("chase_scope", nil)              --追击范围
prop:reader("nearby_enemy_distance", nil)    --接近敌人(判定)距离
prop:reader("nearby_spawn_distance", nil)    --接近出生点(判定)距离
prop:reader("guard_circle_radius", nil)      --警戒圆形半径
prop:reader("guard_sector_radius", nil)      --警戒扇形半径
prop:reader("guard_sector_angle", nil)       --警戒扇形角度
prop:reader("combat_delivery_radius", nil)   --战斗响应传递圆形半径
prop:reader("combat_delivery_duration", nil) --战斗响应持续触发时间
prop:reader("combat_delivery_amount", nil)   --战斗响应触发次数
prop:reader("patrol_type", nil)              --巡逻类型
prop:reader("patrol_path", nil)              --巡逻路径
prop:reader("state_duration", nil)           --状态持续时间

function AiBaseConfig:__init()
end

return AiBaseConfig
