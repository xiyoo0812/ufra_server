--monster_ai_config.lua
---@type GoapAction
local GoapAction = import("world/ai/goap/goap_action.lua")

---@type GoapState
local GoapState = import("world/ai/goap/goap_state.lua")

---@type GoapPlanner
local GoapPlanner = import("world/ai/goap/goap_planner.lua")

---@type GoapConditionV2
local GoapConditionV2 = import("world/ai/goap/goap_condition_v2.lua")

---@type AiBaseConfig
local AiBaseConfig = import("world/ai/object/ai_base_config.lua")

---@class MonsterAiConfig
---@field monster_id integer       怪物(原型)id
---@field base_config AiBaseConfig 基础配置
---@field planner GoapPlanner      GOAP规划器
---@field states table<string, GoapState>   状态集合
local MonsterAiConfig = class()
local prop = property(MonsterAiConfig)
prop:reader("monster_id", nil)      --怪物(原型)id
prop:reader('base_config', nil)     --基础配置
prop:reader("planner", nil)         --GOAP规划器
prop:reader("states", nil)          --状态集合
prop:reader("statets_count", 0) --状态数量(用于监测AI是否有效配置)
prop:reader("actions_count", 0) --行为数量(用于监测AI是否有效配置)

---@param monster_id integer
---@param base_config AiBaseConfig
function MonsterAiConfig:__init(monster_id, base_config)
    self.monster_id = monster_id
    self.base_config = base_config
end

---@param monster_id integer
---@param config table
---@return GoapMgrV2
function MonsterAiConfig.from_config(monster_id, config)
    if monster_id == nil or config == nil or config.monster_id ~= monster_id then
        return nil
    end
    local base_config = AiBaseConfig()
    base_config.remark = config.remark
    base_config.wander = config.wander
    base_config.amuse = config.amuse
    base_config.standoff_duration = config.standoff_duration
    base_config.standoff_back_distance = config.standoff_back_distance
    base_config.standoff_back_precision = config.standoff_back_precision
    base_config.standoff_move_duration = config.standoff_move_duration
    base_config.standoff_move_radius = config.standoff_move_radius
    base_config.wander_radius = config.wander_radius
    base_config.hate_attenuate = config.hate_attenuate
    base_config.return_spawn_speed = config.return_spawn_speed
    base_config.chase_speed = config.chase_speed
    base_config.chase_distance = config.chase_distance
    base_config.chase_scope = config.chase_scope
    base_config.nearby_enemy_distance = config.nearby_enemy_distance
    base_config.nearby_spawn_distance = config.nearby_spawn_distance
    base_config.guard_circle_radius = config.guard_circle_radius
    base_config.guard_sector_radius = config.guard_sector_radius
    base_config.guard_sector_angle = config.guard_sector_angle
    base_config.combat_delivery_radius = config.combat_delivery_radius
    base_config.combat_delivery_duration = config.combat_delivery_duration
    base_config.combat_delivery_amount = config.combat_delivery_amount
    base_config.patrol_type = config.patrol_type
    base_config.patrol_path = config.patrol_path
    base_config.state_duration = config.state_duration
    local conf = MonsterAiConfig(monster_id, base_config)
    conf:with_config_actions(config.actions)
    conf:with_config_states(config.states)
    return conf
end

function MonsterAiConfig:with_config_actions(config_actions)
    self.planner = GoapPlanner()
    self.actions_count = 0
    for _, config_action in ipairs(config_actions) do
        local func_name = config_action['code']
        local name = '[' .. func_name .. ']' .. config_action['name']
        local precondition = GoapConditionV2.from_config(config_action['precondition'])
        local effect = GoapConditionV2.from_config(config_action['effect'])
        self.planner:add_action(GoapAction(name, func_name, precondition, effect, config_action['cost'], config_action['priority']))
        self.actions_count = self.actions_count + 1
    end
end

function MonsterAiConfig:with_config_states(config_states)
    self.states = {}
    self.statets_count = 0
    for _, config_state in ipairs(config_states) do
        local name = config_state['name']
        local priority = config_state['priority']
        local weight = config_state['weight']
        local precondition = GoapConditionV2.from_config(config_state['precondition'])
        local postcondition = GoapConditionV2.from_config(config_state['postcondition'])
        local enable_goal = GoapConditionV2.from_config(config_state['enable_goal'])
        local description = config_state['description']
        self.states[name] = GoapState(name, priority, weight, precondition, postcondition, enable_goal, description)
        self.statets_count = self.statets_count + 1
    end
end

return MonsterAiConfig
