--hate_entity.lua
-- local log_debug          = logger.debug

-- 仇恨实体(主要是player和npc)
---@class HateEntity (怪物)视野内实体(主要是player和npc)
---@field entity_id integer 仇恨目标-实体Id
---@field hate_value number 仇恨值
---@field increase_time number 仇恨值最近一次增加的时间
local HateEntity = class()

function HateEntity:__init(entity_id)
    self.entity_id = entity_id
    self.increase_time = 0
    self.hate_value = 1
end

---增加仇恨值
---@param value number 增加量
function HateEntity:add_hate_value(value)
    self.hate_value = self.hate_value + value
    self.increase_time = quanta.now
end

---尝试减小仇恨值
---@param value number 减小量
---@return boolean 仇恨是否解除
function HateEntity:try_reduce_hate_value(value)
    if self.hate_value <= 0 then
        return true
    end
    local now = quanta.now
    if now - self.increase_time < 2 then
        return false
    end
    -- log_debug('[HateEntity][try_reduce_hate_value()] entity_id={}, hate_value=[{} - {}]', self.entity_id, self.hate_value, value)
    -- 超过2秒未活动的仇恨实体才开始衰减仇恨值
    self.hate_value = self.hate_value - value
    return self.hate_value <= 0
end

return HateEntity
