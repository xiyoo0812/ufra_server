-- goap_expression.lua
local log_warn = quanta.warn
local log_err = quanta.err

---判断t是否是一个空的table
---@param t table
local function is_table_empty(t)
    return type(t) == 'table' and next(t) == nil
end

---@param value    number    计算值
---@param compare  number[]  范围值
local function calculator_range(value, compare)
    if value == nil or compare[1] == nil or compare[2] == nil then
        return false
    end
    return compare[1] <= value and value <= compare[2]
end

---@param value    number  计算值
---@param compare  number  比较值
local function calculator_lt(value, compare)
    if value == nil or compare == nil then
        return false
    end
    return value < compare
end

---@param value    number  计算值
---@param compare  number  比较值
local function calculator_let(value, compare)
    if value == nil or compare == nil then
        return false
    end
    return value <= compare
end

---@param value    boolean|number|string|function  计算值
---@param compare  boolean|number|string  比较值
local function calculator_eq(value, compare)
    if compare == nil then
        if is_table_empty(value) then
            return true
        end
    end
    return value == compare
end

---@param value    boolean|number|string  计算值
---@param compare  boolean|number|string  比较值
local function calculator_ne(value, compare)
    if compare == nil then
        if is_table_empty(value) then
            return false
        end
    end
    return value ~= compare
end

---@param value    number  计算值
---@param compare  number  比较值
local function calculator_gt(value, compare)
    if value == nil or compare == nil then
        return false
    end
    return value > compare
end

---@param value    number  计算值
---@param compare  number  比较值
local function calculator_egt(value, compare)
    if value == nil or compare == nil then
        return false
    end
    return value >= compare
end

local calculator_map = {
    ['range'] = calculator_range,     -- range: 范围(例 {value} range 3,9)
    ['lt'] = calculator_lt,           -- lt: 小于(例: {value} lt 3)
    ['<'] = calculator_lt,
    ['let'] = calculator_let,         -- let: 小于或等于(例: {value} let 3)
    ['<='] = calculator_let,
    ['eq'] = calculator_eq,           -- eq: 等于(例: {value} eq 3)
    ['=='] = calculator_eq,
    ['ne'] = calculator_ne,           -- ne: 不等于(例: {value} ne 3)
    ['!='] = calculator_ne,
    ['gt'] = calculator_gt,           -- gt: 大于(例: {value} gt 3)
    ['>'] = calculator_gt,
    ['egt'] = calculator_egt,         -- egt: 等于或大于(例: {value} egt 3)
    ['>='] = calculator_egt,
}

local ASSUME_PRECISION = 1E-9

local function assumer_range(compare)
    -- 取中间值
    return (compare[1] + compare[2]) / 2
end

local function assumer_lt(compare)
    return compare - ASSUME_PRECISION
end

local function assumer_let(compare)
    return compare - ASSUME_PRECISION
end

local function assumer_eq(compare)
    return compare
end

local function assumer_gt(compare)
    return compare + ASSUME_PRECISION
end

local function assumer_egt(compare)
    return compare + ASSUME_PRECISION
end

local assumer_map = {
    ['range'] = assumer_range,
    ['lt'] = assumer_lt,
    ['<'] = assumer_lt,
    ['let'] = assumer_let,
    ['<='] = assumer_let,
    ['eq'] = assumer_eq,
    ['=='] = assumer_eq,
    ['gt'] = assumer_gt,
    ['>'] = assumer_gt,
    ['egt'] = assumer_egt,
    ['>='] = assumer_egt,
}
local symbol_list = {}
for key, _ in pairs(calculator_map) do
    table.insert(symbol_list, key)
end

local OWNER = {
    SPAWN = 'spawn',
    ENTITY = 'entity',
    ENEMY = 'enemy',
}

---@class GoapExpression GOAP条件表达式
---@field property any 状态名(属性)
---@field symbol   string 运算符号
---@field compare  any 比较值
---@field owner    string property归属
---@field demand_count integer 需要满足的条件数量
local GoapExpression = class()

---判断symbol是否定义了
---@param value string 要判断的symbol
function GoapExpression.is_symbol_defined(value)
    return calculator_map[value] ~= nil
end

---获取已定义的symbol列表
function GoapExpression.get_symbol_list()
    return symbol_list
end

function GoapExpression:__init(property, symbol, compare, owner)
    if not self.is_symbol_defined(symbol) then
        error(string.format('symbol "{}" is undefined', symbol), 1)
    end
    self.property = property
    self.symbol = symbol
    self.compare = compare
    self.owner = owner or OWNER.ENTITY -- 如果未提供，默认为自己
    self.demand_count = 1
end

---表达式求值
---@param context GoapContext 世界状态上下文
---@return boolean 表达式是否为真
function GoapExpression:evaluate(context)
    -- local value = get_property_value(context, self.property, self.owner)
    local value = nil
    if self.owner == OWNER.SPAWN then
        -- context.owner_spawn 不为空
        value = context.owner_spawn[self.property]
    elseif self.owner == OWNER.ENTITY then
        value = context.owner_entity[self.property]
        -- 如果动态属性为空，则尝试取配置属性
        if value == nil and context.config[self.property] ~= nil then
            value = context.config[self.property]
        end
    elseif self.owner == OWNER.ENEMY then
        if self.property ~= nil then
            value = context.owner_enemy[self.property]
        else
            if (context.owner_enemy or {})['__assume_enemy'] == true then
                return true
            else
                value = context.owner_enemy
            end
        end
    end

    local calculator = calculator_map[self.symbol]
    -- logger.dump('[GoapExpression:evaluate] {}.{} {} {}, value={}', self.owner, self.property, self.symbol, self.compare, value)
    return calculator(value, self.compare)
end

---@param context GoapContext 世界状态上下文
function GoapExpression:_assume_ne_property_nil(context)
    if self.compare == nil then
        context['owner_' .. self.owner] = {
            ['__assume_' .. self.owner] = true
        }
    else
        -- 按需处理
        log_err('_assume_ne_property_nil(property={}, compare={}) not implemented', self.property, self.compare)
    end
end

---@param context GoapContext 世界状态上下文
function GoapExpression:_assume_ne_property_not_nil(context)
    if self.compare == nil then
        context['owner_' .. self.owner][self.property] = {
            ['__assume_' .. self.owner .. '_' .. self.property] = true
        }
    else
        -- 按需处理
        log_err('_assume_ne_property_not_nil(property={}, compare={}) not implemented', self.property, self.compare)
    end
end

---@param context GoapContext 世界状态上下文
function GoapExpression:_assume_ne(context)
    if self.owner == OWNER.ENEMY then
        if self.property == nil then
            self:_assume_ne_property_nil(context)
        else
            self:_assume_ne_property_not_nil(context)
        end
        return
    end
    if self.owner == OWNER.SPAWN or self.owner == OWNER.ENTITY then
        local compare_type = type(self.compare)
        if compare_type == 'nil' then
            context['owner_' .. self.owner][self.property] = {
                ['__assume_' .. self.owner .. '_' .. self.property] = true
            }
        elseif compare_type == 'boolean' then
            context['owner_' .. self.owner][self.property] = not context['owner_' .. self.owner][self.property]
        elseif compare_type == 'number' then
            context['owner_' .. self.owner][self.property] = context['owner_' .. self.owner][self.property] + ASSUME_PRECISION
        elseif compare_type == 'string' then
            context['owner_' .. self.owner][self.property] = context['owner_' .. self.owner][self.property] .. '_'
        else
            -- 按需处理
            log_err('_assume_ne(property={}, compare={}) not implemented', self.property, self.compare)
        end
    end
end

---除不等于之外的动态条件假设
---@param context GoapContext 世界状态上下文
function GoapExpression:_assume_not_ne_dynamic(context, assumed_value)
    if self.property == nil then
        if assumed_value == nil and type(context['owner_' .. self.owner]) == 'table' then
            assumed_value = {}
        end
        context['owner_' .. self.owner] = assumed_value
    else
        -- context.hate_target 和 context.patrol_point 可能为空
        if context['owner_' .. self.owner] == nil then
            context['owner_' .. self.owner] = {}
        else
            -- 当有真实值设置时, 清除属性假设
            context['owner_' .. self.owner]['__assume_' .. self.owner] = nil
            context['owner_' .. self.owner]['__assume_' .. self.owner .. '_' .. self.property] = nil
        end
        context['owner_' .. self.owner][self.property] = assumed_value
    end
end

---修改世界状态使表达式为真
---@param context GoapContext 世界状态上下文
function GoapExpression:assume(context)
    local assumer = assumer_map[self.symbol]
    if assumer == nil then
        -- 不等于: ne 或 !=
        self:_assume_ne(context)
        return
    end
    local assumed_value = assumer(self.compare)

    if self.owner == OWNER.SPAWN then
        -- context.owner_spawn 不为空
        context.owner_spawn[self.property] = assumed_value
    elseif self.owner == OWNER.ENTITY then
        -- context.owner_entity 不为空
        if context.config[self.property] ~= nil then
            log_warn('assume() blocked: property [{}.{}] is in config, so not allowed to change', self.owner, self.property)
            -- 配置项目不允许修改
            return
        end
        context.owner_entity[self.property] = assumed_value
    elseif self.owner == OWNER.ENEMY then
        self:_assume_not_ne_dynamic(context, assumed_value)
    end
end

---判断两个表达式相等
---@param other GoapExpression 其他表达式
function GoapExpression:equal(other)
    return self.property == other.property and self.symbol == other.symbol and self.compare == other.compare
end

---@return GoapExpression
function GoapExpression:clone()
    return GoapExpression(self.property, self.symbol, self.compare, self.owner)
end

return GoapExpression
