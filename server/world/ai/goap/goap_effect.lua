--goap_effect.lua
local tinsert = table.insert

---@class GoapEffect Goap的效果
---@field expressions GoapExpression[] 条件表达式
local GoapEffect = class()

function GoapEffect:__init(expressions)
    self.expressions = expressions or {}
end

---应用效果
---@param context GoapContext 世界状态上下文
function GoapEffect:apply(context)
    for _, expression in ipairs(self.expressions) do
        expression:assume(context)
    end
end

---@return GoapEffect
function GoapEffect:clone()
    local expressions = {}
    for _, expression in ipairs(self.expressions) do
        tinsert(expressions, expression:clone())
    end
    return GoapEffect(expressions)
end

return GoapEffect
