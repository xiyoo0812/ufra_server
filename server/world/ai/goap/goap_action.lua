-- goap_action.lua
local log_err = logger.err

---@type GoapCodeFunction
local GOAP_CODE_FUNCTION = import("world/ai/utils/goap_code_function.lua")

--注释参考: https://github.com/LuaLS/lua-language-server/wiki/Annotations
---@class GoapAction GOAP行动
---@field name          string Action名
---@field func          string 执行操作的函数名
---@field precondition GoapConditionV2 前置条件
---@field effect       GoapConditionV2 后置效果
---@field cost        number 执行花费
---@field priority      number 优先级
local GoapAction = class()

---@param name string
---@param func string
---@param precondition GoapConditionV2
---@param effect GoapConditionV2
---@param cost number
---@param priority number
function GoapAction:__init(name, func, precondition, effect, cost, priority)
    self.name = name
    self.func = func
    self.precondition = precondition or {}
    self.effect = effect or {}
    self.cost = cost or 1
    self.priority = priority or 1
end

---@param context GoapContext 世界状态上下文
function GoapAction:execute_forward(context, entity)
    local code_function = GOAP_CODE_FUNCTION[self.func]
    if code_function ~= nil then
        -- 注: 规划时满足，运行时不一定会满足
        -- 运行时需要判断条件是否满足
        if not self:match(context) then
            return
        end
        code_function(context, entity)
    else
        log_err('code_function GOAP_CODE_FUNCTION[{}] not defined', self.func)
    end
end

---判断前置条件是否匹配
---@param context GoapContext 世界状态上下文
---@return boolean 前置条件是否匹配
function GoapAction:match(context)
    -- (根据当前上下文状态)判断前置条件
    return self.precondition:evaluate(context)
end

---应用效果
---@param context GoapContext 世界状态上下文
function GoapAction:apply(context)
    self.effect:apply(context)
end

---@return GoapAction
function GoapAction:clone()
    return GoapAction(self.name, self.func, self.precondition:clone(), self.effect:clone(), self.cost, self.priority)
end

return GoapAction
