-- goap_context.lua
local tsort   = table.sort
local tclone  = qtable.deep_copy

---判断t是否是table
---@param t any 要判断的变量
local function is_table(t)
    return t ~= nil and type(t) == 'table'
end

---判断table是否是数组
---@param t table 要判断的table
local function table_is_array(t)
    if #t > 0 then
        return true
    end

    local idx = 0
    for k, v in pairs(t) do
        if type(k) ~= 'string' or  k ~= v then
            return false
        end
        idx = idx + 1
        if k ~= idx then
            return false
        end
    end
    return true
end

---判断table是否是字典
---@param t table 要判断的table
local function table_is_map(t)
    if #t > 0 then
        return false
    end
    for _, _ in pairs(t) do
        return true
    end
    return false
end

---比较两个table是否相等-预先定义
---@param t1 table 第一个table
---@param t2 table 第二个table
local function table_equal(t1, t2)
    return false
end

---判断两个array类型的table是否相等
---@param t1 table 第一个table
---@param t2 table 第二个table
local function array_equal(t1, t2)
    local st1 = tclone(t1)
    tsort(st1)
    local st2 = tclone(t2)
    tsort(st2)
    for i, v in ipairs(st1) do
        local typ1 = type(v)
        local typ2 = type(st2[i])
        if typ1 == 'table' and typ2 == 'table' then
            if not table_equal(v, st2[i]) then
                return false
            end
        elseif typ1 ~= typ2 then
            return false
        end
        if v ~= st2[i] then
            return false
        end
    end
    return true
end

---判断两个map类型的table是否相等
---@param t1 table 第一个table
---@param t2 table 第二个table
local function map_equal(t1, t2)
    for k, v in pairs(t1) do
        local typ1 = type(v)
        local typ2 = type(t2[k])
        if typ1 == 'table' and typ2 == 'table' then
            if not table_equal(v, t2[k]) then
                return false
            end
        elseif typ1 ~= typ2 then
            return false
        end
        if v ~= t2[k] then
            return false
        end
    end
    return true
end

---比较两个table是否相等-正式实现
---@param t1 table 第一个table
---@param t2 table 第二个table
table_equal = function(t1, t2)
    -- 如果两个table的引用相同，则它们一定相等
    if t1 == t2 then
        return true
    end
    if not (is_table(t1) and is_table(t2)) then
        return false
    end
    -- 如果两个table的类型不同，则它们一定不相等
    local t1_is_table = table_is_array(t1)
    local t1_is_map = table_is_map(t1)
    local t2_is_table = table_is_array(t2)
    local t2_is_map = table_is_map(t2)
    if (t1_is_table and t2_is_map) or (t1_is_map and t2_is_table) then
        return false
    end
    if t1_is_table and t2_is_table then
        return array_equal(t1, t2)
    end

    if t1_is_map and t2_is_map then
        return map_equal(t1, t2)
    end

    return false
end

---@class GoapContext Goap状态
---@field id                 integer Entity的Id
---@field config             table<string, any> 配置
---@field owner_entity         table<string, any> 自己的属性
---@field owner_spawn        table<string, any> 对出生点的感知属性
---@field owner_enemy  table<string, any> 对仇恨目标的感知属性
---@field debug_info    table<string, any> 调试信息
local GoapContext = class()

function GoapContext:__init(id, config, owner_entity, owner_spawn, owner_enemy, debug_info)
    self.id = id
    self.config = config or {}
    self.__config_is_empty = next(self.config) == nil
    self.owner_entity = owner_entity or {}
    self.owner_spawn = owner_spawn or {}
    self.owner_enemy = owner_enemy or {}
    self.debug_info = debug_info or {}
end

function GoapContext:with_base_config(base_config)
    if self.__config_is_empty then
        self.__config_is_empty = false
        self.config = base_config
    end
end

---比较相对
---@param other GoapContext 比较对象
---@return boolean 是否相等
function GoapContext:equal(other)
    if self.id ~= other.id then
        return false
    end
    if not table_equal(self.owner_entity, other.owner_entity) then
        return false
    end
    if not table_equal(self.owner_spawn, other.owner_spawn) then
        return false
    end
    if not table_equal(self.owner_enemy, other.owner_enemy) then
        return false
    end
    return true
end

---拷贝
---@return GoapContext 克隆对象
function GoapContext:clone()
    return GoapContext(
        self.id,
        tclone(self.config or {}),
        tclone(self.owner_entity or {}),
        tclone(self.owner_spawn or {}),
        tclone(self.owner_enemy or {})
    )
end

return GoapContext
