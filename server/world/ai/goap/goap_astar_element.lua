-- goap_astar_element.lua
---@type GoapPlan
-- local GoapPlan = import('world/ai/goap/goap_plan.lua')

---@type GoapContext
-- local GoapContext = import("world/ai/goap/goap_context.lua")

---@class GoapAStarElement Goap的A星节点
---@field cost number 代价(花费)
---@field state GoapContext 状态上下文
---@field plan GoapPlan 计划
local GoapAStarElement = class()

function GoapAStarElement:__init(cost, state, plan)
    self.cost = cost
    self.plan = plan
    self.state = state
end

return GoapAStarElement
