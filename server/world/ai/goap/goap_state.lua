--goap_state.lua
---@class GoapState Goap状态
---@field name          string State名
---@field priority      integer 优先级
---@field weight        integer 权重
---@field precondition  GoapConditionV2 状态匹配(进入)条件
---@field postcondition GoapConditionV2 状态(改变)条件
---@field enable_goal GoapConditionV2 ai使能的目标条件
---@field description string 状态描述
local GoapState = class()

function GoapState:__init(name, priority, weight, precondition, postcondition, enable_goal, description)
    self.name = name
    self.priority = priority
    self.weight = weight
    self.precondition = precondition or {}
    self.postcondition = postcondition or {}
    self.enable_goal = enable_goal or {}
    self.description = description or ""
end

---判断状态条件是否匹配
---@param context GoapContext 世界状态上下文
---@return boolean 前置条件是否匹配
function GoapState:enter_match(context)
    -- (根据当前上下文状态)判断进入条件
    return self.precondition:evaluate(context)
end

---是否满足状态改变条件改变
---@param context GoapContext 世界状态上下文
---@return boolean 前置条件是否匹配
function GoapState:transition_match(context)
    -- (根据当前上下文状态)判断离开条件
    return self.postcondition:evaluate(context)
end

---@return GoapState
function GoapState:clone()
    local preconditions = self.precondition:clone()
    local postconditions = self.postcondition:clone()
    local goal_conditions = self.enable_goal:clone()
    return GoapState(self.name, self.priority, self.weight, preconditions, postconditions, goal_conditions, self.description)
end

return GoapState
