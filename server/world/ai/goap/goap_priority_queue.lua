--goap_priority_queue.lua
local tremove = table.remove
local tinsert = table.insert
local tsort   = table.sort

---@class GoapPriorityQueue 优先级队列
---@field elements GoapAStarElement[] 队列元素
local GoapPriorityQueue = class()

function GoapPriorityQueue:__init()
    self.elements = {}
end

---入队列
---@param element GoapAStarElement 入队元素
function GoapPriorityQueue:push(element)
    -- 将元素插入到数组中，并按照优先级排序
    tinsert(self.elements, element)
    tsort(self.elements, function(a, b) return a.cost < b.cost end)
end

---出队列
---@return GoapAStarElement 出队元素
function GoapPriorityQueue:pop()
    -- 移除优先级最小的元素并返回
    return tremove(self.elements, 1)
end

---队列是否为空
---@return boolean 队列是否为空
function GoapPriorityQueue:empty()
    -- 判断队列是否为空
    return #self.elements <= 0
end

---清空队列
function GoapPriorityQueue:clear()
    while not self:empty() do
        self:pop()
    end
end

return GoapPriorityQueue
