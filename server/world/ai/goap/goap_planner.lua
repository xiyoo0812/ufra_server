-- goap_planner.lua
local tinsert = table.insert

-- local log_debug = logger.debug

---@type GoapPriorityQueue
local GoapPriorityQueue = import('world/ai/goap/goap_priority_queue.lua')

---@type GoapPlan
local GoapPlan = import('world/ai/goap/goap_plan.lua')

---@type GoapAStarElement
local GoapAStarElement = import('world/ai/goap/goap_astar_element.lua')

---@class GoapPlanner GOAP规划器
---@field actions        GoapAction[] 可用的Action集合
local GoapPlanner = class()

function GoapPlanner:__init(actions)
    self.actions = actions or {}
end

---添加Action
---@param action GoapAction 可用的Action
function GoapPlanner:add_action(action)
    tinsert(self.actions, action)
end

---作规划
---@param initial_state GoapContext 当前状态上下文
---@param goal_condition GoapConditionV2 目标状态
---@return GoapPlan 作出的规划
function GoapPlanner:plan(initial_state, goal_condition)
    local countdown = 999 -- 限定次数，防止策划配置错误而导致死循环

    --- A* open 集合
    ---@type GoapPriorityQueue
    local open_set = GoapPriorityQueue()
    --- A* closed 集合
    ---@type GoapContext[]
    local closed_set = {}

    -- 用A*算法搜索
    local initial_element = GoapAStarElement(0, initial_state:clone(), GoapPlan())
    open_set:push(initial_element)
    repeat
        if countdown <= 0 then
            break
        end
        countdown = countdown - 1
        -- log_debug(countdown)

        local element = open_set:pop()

        if goal_condition:evaluate(element.state) then
            return element.plan
        end

        -- 将当前状态添加到 closed 集合中
        tinsert(closed_set, element.state)

        -- 遍历可行的Action
        for _, action in ipairs(self.actions) do
            -- 如果动作的前置条件不满足，则跳过
            if not action:match(element.state) then
                goto continue_action
            end

            -- 应用Action的后置效果, 得到新的状态和Plan
            local new_state = element.state:clone()
            action:apply(new_state)

            local new_plan = element.plan:clone()
            new_plan:add_action(action)

            -- 计算新计划的代价
            local new_cost = element.cost + action.cost

            -- 如果新状态已经存在于 closed 集合中，则跳过
            for _, close in ipairs(closed_set) do
                if close:equal(new_state) then
                    goto continue_action
                end
            end

            -- 计算新的估价值
            local heuristic = 0
            -- for _, goal in ipairs(goal_condition) do
            --     if not goal:evaluate(new_state) then
            --         heuristic = heuristic + 1
            --     end
            -- end
            if not goal_condition:evaluate(new_state) then
                heuristic = heuristic + 1
            end
            local f_score = new_cost + heuristic * action.priority
            -- 将新状态和新计划添加到 open 集合中
            open_set:push(GoapAStarElement(f_score, new_state, new_plan))

            :: continue_action ::
        end
    until open_set:empty()

    -- 如果没有找到合适的 Plan，则返回空
    return nil
end

return GoapPlanner
