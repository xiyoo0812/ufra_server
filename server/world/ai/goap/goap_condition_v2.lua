--goap_condition_v2.lua
local tinsert           = table.insert
local mmax              = math.max
-- local qtrandom_array    = qtable.random_array

---@type GoapExpression
local GoapExpression = import("world/ai/goap/goap_expression.lua")

---@class GoapConditionV2 Goap的A星节点
---@field expression_tree table 条件表达式树
---@field demand_count integer 求值需要满足的表达式数量
local GoapConditionV2 = class()
local prop = property(GoapConditionV2)
prop:reader("expression_tree", {})               --条件表达式树
prop:reader("demand_count", 1)                   --求值需要满足的表达式数量

---@param expression_tree table 条件表达式
function GoapConditionV2:__init(expression_tree)
    self.expression_tree = expression_tree
    self.demand_count = GoapConditionV2.calc_demand_count(self)
end

---(根据当前世界状态)判断条件是否满足
---@param context GoapContext 世界状态上下文
---@return boolean 条件是否满足
function GoapConditionV2:evaluate(context)
    return GoapConditionV2.evaluate_condition(context, self)
end

---@param config table
---@return GoapConditionV2
function GoapConditionV2.from_config(config)
    if not config or type(config) ~= 'table' then
        return
    end
    local tree
    if config['_and'] then
        tree = {['_and'] = {}}
        for _, item in ipairs(config['_and']) do
            tinsert(tree['_and'], GoapConditionV2.from_config(item))
        end
    elseif config['_or'] then
        tree = {['_or'] = {}}
        for _, item in ipairs(config['_or']) do
            tinsert(tree['_or'], GoapConditionV2.from_config(item))
        end
    else
        tree = GoapExpression(config['property'], config['operator'], config['value'], config['owner'])
    end
    return GoapConditionV2(tree)
end

---计算必须要执行的表达式的数量
---@param condition GoapConditionV2
---@return integer
function GoapConditionV2.calc_demand_count(condition)
    local demand_count = 0
    if condition.expression_tree['_and'] then
        for _, item in ipairs(condition.expression_tree['_and']) do
            demand_count = demand_count + GoapConditionV2.calc_demand_count(item)
        end
    elseif condition.expression_tree['_or'] then
        for _, item in ipairs(condition.expression_tree['_or']) do
            demand_count = mmax(demand_count, GoapConditionV2.calc_demand_count(item))
        end
    else
        demand_count = condition.demand_count
    end
    return demand_count
end

function GoapConditionV2.evaluate_and(context, condition_and)
    for _, sub_condition in pairs(condition_and) do
        if not GoapConditionV2.evaluate_condition(context, sub_condition) then
            return false
        end
    end
    return true
end

function GoapConditionV2.evaluate_or(context, condition_or)
    for _, sub_condition in ipairs(condition_or) do
        if GoapConditionV2.evaluate_condition(context, sub_condition) then
            return true
        end
    end
    return false
end

---@param context GoapContext
---@param condition GoapConditionV2
---@return boolean
function GoapConditionV2.evaluate_condition(context, condition)
    local satisfied
    if condition.expression_tree['_and'] then
        satisfied = GoapConditionV2.evaluate_and(context, condition.expression_tree['_and'])
    elseif condition.expression_tree['_or'] then
        satisfied = GoapConditionV2.evaluate_or(context, condition.expression_tree['_or'])
    else
        satisfied = condition.expression_tree:evaluate(context)
    end
    return satisfied
end

---@return GoapConditionV2
function GoapConditionV2:clone()
    return GoapConditionV2.clone_condition(self)
end

function GoapConditionV2.clone_and(condition_and)
    local and_conditions = {}
    for _, sub_condition in ipairs(condition_and) do
        tinsert(and_conditions, GoapConditionV2.clone_condition(sub_condition))
    end
    return GoapConditionV2({ ['_and'] = and_conditions, })
end

function GoapConditionV2.clone_or(condition_or)
    local or_conditions = {}
    for _, sub_condition in ipairs(condition_or) do
        tinsert(or_conditions, GoapConditionV2.clone_condition(sub_condition))
    end
    return GoapConditionV2({ ['_or'] = or_conditions, })
end

function GoapConditionV2.clone_condition(condition)
    if condition.expression_tree['_and'] then
        return GoapConditionV2.clone_and(condition.expression_tree['_and'])
    end
    if condition.expression_tree['_or'] then
        return GoapConditionV2.clone_or(condition.expression_tree['_or'])
    end
    if condition.expression_tree.__addr then
        return GoapConditionV2(condition.expression_tree:clone())
    end
end

---应用效果
---@param context GoapContext 世界状态上下文
function GoapConditionV2:apply(context)
    GoapConditionV2.apply_condition(self, context)
end

function GoapConditionV2.apply_condition(condition, context)
    if condition.expression_tree['_and'] then
        GoapConditionV2.apply_condition_and(condition.expression_tree['_and'], context)
    elseif condition.expression_tree['_or'] then
        GoapConditionV2.apply_condition_or(condition.expression_tree['_or'], context)
    elseif condition.expression_tree.__addr then
        condition.expression_tree:assume(context)
    end
end

function GoapConditionV2.apply_condition_and(condition_and, context)
    for _, sub_condition in ipairs(condition_and) do
        GoapConditionV2.apply_condition(sub_condition, context)
    end
end

function GoapConditionV2.apply_condition_or(condition_or, context)
    -- 任意一种情况满足
    -- local sub_condition = qtrandom_array(condition_or)
    -- if sub_condition then
    --     GoapConditionV2.apply_condition(sub_condition, context)
    -- end
    -- 全部条件满足
    for _, sub_condition in ipairs(condition_or) do
        GoapConditionV2.apply_condition(sub_condition, context)
    end
end

return GoapConditionV2
