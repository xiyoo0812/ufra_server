--goap_plan.lua
local tinsert = table.insert
local tconcat = table.concat

---@class GoapPlan Goap的计划
---@field actions GoapAction[] 计划中的Action
---@field cost number 计划的代价
---@field progress integer 进度(执行阶段，执行到哪一个action了)
local GoapPlan = class()

function GoapPlan:__init()
    self.actions = {}
    self.cost = 0
    self.progress = 0
end

---是否完成
---@param context GoapContext 世界状态上下文
---@param goal_condition GoapConditionV2 要实现的目标
function GoapPlan:is_completed(context, goal_condition)
    if self.progress > #self.actions then
        return true
    end
    if not self:is_running() then
        return false
    end
    local next_action = self.actions[self.progress + 1]
    -- 如果存在下一个action，在判断其是否已满足执行条件
    if next_action ~= nil then
        return next_action:match(context)
    end
    -- 判断目标是否满足
    return goal_condition:evaluate(context)
end

---是否有效
function GoapPlan:is_valid()
    return #self.actions > 0
end

---是否在运行
function GoapPlan:is_running()
    return self.progress > 0
end

---开始执行
function GoapPlan:start()
    self.progress = 1
end

---@param context GoapContext 世界状态上下文
function GoapPlan:forward_action(context, entity)
    local action = self.actions[self.progress]
    action:execute_forward(context, entity)
end

---添加Action
---@param action GoapAction 要添加的Action
function GoapPlan:add_action(action)
    tinsert(self.actions, action)
    self.cost = self.cost + action.cost
end

---应用效果
---@param context GoapContext 世界状态上下文
function GoapPlan:apply(context)
    for _, action in ipairs(self.actions) do
        action:apply(context)
    end
end

---@return GoapPlan
function GoapPlan:clone()
    local actions = {}
    for _, action in ipairs(self.actions) do
        tinsert(actions, action:clone())
    end
    local new_plan = GoapPlan()
    new_plan.actions = actions
    new_plan.cost = self.cost
    return new_plan
end

function GoapPlan:tostring()
    local action_funcs = {}
    for _, action in ipairs(self.actions) do
        tinsert(action_funcs, '[' .. action.func .. ']')
    end
    return tconcat(action_funcs, ' -> ')
end

return GoapPlan
