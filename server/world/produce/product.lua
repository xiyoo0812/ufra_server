--product.lua
local log_debug      = logger.debug
local protobuf_mgr   = quanta.get("protobuf_mgr")
local object_fty     = quanta.get("object_fty")

local mmax           = math.max
local mmin           = math.min
local mfloor         = math.floor

local FREE           = protobuf_mgr:enum("product_status", "PS_FREE")
local WORK           = protobuf_mgr:enum("product_status", "PS_WORK")
local FULL           = protobuf_mgr:enum("product_status", "PS_FULL")
local PAUSE          = protobuf_mgr:enum("product_status", "PS_PAUSE")
local END            = protobuf_mgr:enum("product_status", "PS_END")

local Product        = class()
local prop           = property(Product)

prop:reader("parent", nil)
prop:accessor("id", 0)           -- id
prop:accessor("uuid", 0)         -- uuid
prop:accessor("prototype", nil)  -- prototype
prop:accessor("master", 0)       -- master
prop:accessor("overlie", 0)      -- overlie
prop:accessor("wheel_count", 0)  -- 完成轮数
prop:accessor("skill_items", {}) -- 技能产出(可不足1,需要累计,如:0.8个对应80)

local dbprop = db_property(Product, "home_produce")
dbprop:store_value("rem_count", 0)  -- 剩余生产数量
dbprop:store_value("fin_count", 0)  -- 已完成的数量
dbprop:store_value("tgt_count", 0)  -- 目标数量
dbprop:store_value("status", FREE)  -- status
dbprop:store_value("costs", {})     -- 一轮的消耗
dbprop:store_value("prod_count", 1) -- 一轮生产的数量
dbprop:store_value("prod_time", 0)  -- 一轮的生产时间
dbprop:store_value("done_time", 0)  -- 当前轮完成时间(BUFF提供的特殊效果,每轮开始,提前完成的时间)
dbprop:store_value("work_time", 0)  -- 开始工作的时间
dbprop:store_value("start_time", 0) -- 当前轮开始时间
dbprop:store_value("end_time", 0)   -- 当前轮结束时间
dbprop:store_value("last_time", 0)  -- 当前轮剩余时间(暂停时记录剩余时间)
dbprop:store_value("extd_time", 0)  -- 继承的时间(一次性)
dbprop:store_value("sort", 0)       -- 排序值

function Product:__init(uuid, conf, overlie)
    self.uuid = uuid
    self.id = conf.id
    self.prototype = conf
    self.overlie = overlie
end

function Product:load_data(parent, data)
    self.parent = parent
    self.master = data.master
    self.rem_count = data.rem_count
    self.fin_count = data.fin_count
    self.tgt_count = data.tgt_count
    self.status = data.status or FREE
    self.costs = data.costs
    self.prod_count = data.prod_count
    self.prod_time = data.prod_time
    self.done_time = data.done_time or 0
    self.work_time = data.work_time
    self.start_time = data.start_time
    self.end_time = data.end_time
    self.extd_time = data.extd_time
    self.sort = data.sort
end

function Product:startup(parent, data)
    self.parent = parent
    self.master = data.master
    self:set_rem_count(data.rem_count or 1)
    self:set_tgt_count(data.tgt_count or 1)
    self:set_fin_count(data.fin_count or 0)
    self:set_costs(data.costs or {})
    self:set_prod_count(data.prod_count or 1)
    self:set_prod_time(data.prod_time or 0)
    self:set_done_time(data.done_time or 0)
    self:set_extd_time(data.extd_time or 0)
    self:set_sort(data.sort or 0)
end

-- 修改完成数量
function Product:change_fin_count(count)
    if self.fin_count > self.overlie then
        self:save_status(FULL)
        return
    end
    local fin_count = mmin(count, self.overlie - self.fin_count)
    if fin_count > 0 then
        -- 轮次
        self.wheel_count = self.wheel_count + mfloor(count / self.prod_count)
        self:save_fin_count(self.fin_count + count)
        self:change_rem_count(count)
    end
end

-- 修改剩余数量
function Product:change_rem_count(count)
    if object_fty:is_gather(self.parent.prototype) then
        return
    end
    self:save_rem_count(self.rem_count - count)
end

-- 获取技能物品数量
function Product:get_skitem_count(item_id)
    return self.skill_items[item_id] or 0
end

-- 设置技能物品数量
function Product:set_skitem_count(item_id, item_count)
    self.skill_items[item_id] = item_count
end

-- 最大生产数量
function Product:max_prod_count(count)
    return mmin(self.rem_count, count)
end

-- 更新效果
function Product:up_effects(prod_time, done_time, tgt_count)
    log_debug("[Product][up_effects] prod_time:{}-{} done_time:{} tgt_count:{}",
    self.prod_time,prod_time, done_time, tgt_count)
    local index = 0
    if self.prod_time ~= prod_time then
        self:save_prod_time(prod_time)
        index = index + 1
    end
    if self.done_time ~= done_time then
        self:save_done_time(done_time)
        index = index + 1
    end
    if self.tgt_count ~= tgt_count then
        self:save_tgt_count(tgt_count)
        index = index + 1
    end
    return index > 0
end

-- 离线生产
function Product:off_update(now, elapse_time, in_prod_time)
    log_debug("[Product][off_update] begin player:{} product:{} now:{} elapse_time:{} in_prod_time:{}",
        self.master, self.id, now, elapse_time, in_prod_time)

    if self:is_work() then
        elapse_time = elapse_time + mmin((now - self.start_time), self.end_time - self.start_time)
        log_debug(
            "[Product][off_update] work player:{} product:{} now:{} start_time:{} end_time:{} elapse_time:{} in_prod_time:{}",
            self.master, self.id, now, self.start_time, self.end_time, elapse_time, in_prod_time)
    end
    local prod_time = in_prod_time - self.done_time
    if elapse_time <= prod_time then
        if self:is_work() then
            log_debug("[Product][off_update] is_work")
            return false, self.status == END, 0
        end
        if self:is_free() then
            self:save_end_time(now + prod_time - elapse_time)
            log_debug(
                "[Product][off_update] is_free now:{} prod_time:{} elapse_time:{} start_time:{} end_time:{} last_time:{}",
                now, prod_time, elapse_time, self.start_time, self.end_time, self.end_time - self.start_time)
        elseif self:is_pause() then
            if elapse_time >= self.last_time then
                self:change_fin_count(self.prod_count)
                log_debug(
                    "[Product][off_update] is_pause elapse_time >= self.last_time elapse_time:{} last_time:{} prod_count:{} fin_count:{} rem_count:{}",
                    elapse_time, self.last_time, self.prod_count, self.fin_count, self.rem_count)
                if self.rem_count <= 0 then
                    self:save_status(END)
                    return true, self.status == END, 0
                end
            end
            self:save_end_time(now + prod_time - elapse_time - self.last_time)
            log_debug(
                "[Product][off_update] is_pause now:{} prod_time:{} elapse_time:{} start_time:{} end_time:{} last_time:{}",
                now, prod_time, elapse_time, self.start_time, self.end_time, self.end_time - self.start_time)
        end
        self:save_start_time(now)
        self:save_status(WORK)
        return true, false, 0
    end

    local fcount = self:max_prod_count(elapse_time // mmax(prod_time,1))
    local ltime = elapse_time - (prod_time * fcount)
    self:change_fin_count(fcount * self.prod_count)
    log_debug("[Product][off_update] player:{} product:{} prod_time:{} elapse_time:{} fcount:{} prod_count:{}",
        self.master, self.id, prod_time, elapse_time, fcount, self.prod_count)
    if self.rem_count > 0 then
        self:save_start_time(now)
        self:save_end_time(now + prod_time - ltime)
        ltime = 0
    else
        self:save_status(END)
    end
    return true, self.status == END, ltime
end

--检查能否取消
function Product:check_cancel()
    return true
    -- return self.fin_count == 0
end

--空闲验证
function Product:is_free()
    return self.status == FREE
end

--工作验证
function Product:is_work()
    return self.status == WORK
end

--暂停验证
function Product:is_pause()
    return self.status == PAUSE
end

--生产满了验证
function Product:is_full()
    return self.status == FULL
end

--设置已满
function Product:set_full()
    self:save_status(FULL)
end

--结束验证
function Product:is_end()
    return self.status == END
end

--修改索引
function Product:change_sort(sort)
    if self.sort ~= sort then
        self:save_sort(sort)
        return true
    end
    return false
end

-- 开始工作
function Product:start_work()
    log_debug("[Product][start_work] player:{} product:{}", self.master, self.id)
    if not self:is_pause() then
        self:save_status(WORK)
        self:save_work_time(quanta.now)
        local start_time = quanta.now - self.done_time
        local end_time = quanta.now + mmax(self.prod_time - self.done_time, 1)
        -- 继承时间
        if self.extd_time > 0 then
            start_time = start_time - self.extd_time
            end_time = end_time - self.extd_time
            self:save_extd_time(0)
        end
        self:save_start_time(start_time)
        self:save_end_time(end_time)
        -- 暂停逻辑
    else
        self:save_status(WORK)
        self:save_start_time(quanta.now)
        self:save_end_time(quanta.now + self.last_time)
    end
end

-- 下一轮工作
function Product:next_work()
    local now = quanta.now
    if self.end_time <= now then
        self:save_start_time(quanta.now - self.done_time)
        self:save_end_time(quanta.now + mmax(self.prod_time - self.done_time, 1))
    end
end

-- 停止工作
function Product:stop_work(status)
    log_debug("[Product][stop_work] player:{} product:{} status:{}", self.master, self.id, status)
end

-- 暂停工作
function Product:pause_work()
    if not self:is_work() then
        return false
    end
    self:save_status(PAUSE)
    self:save_last_time(mmax(self.end_time - quanta.now, 1))
    return true
end

--取消返回材料
function Product:cancel()
    local items = {}
    for item_id, item_count in pairs(self.costs or {}) do
        items[item_id] = self.rem_count * item_count
    end
    return items
end

-- 获取产品id
function Product:get_prod_id()
    return self.prototype.produce_id
end

--获取产品
function Product:goods()
    local fin_count = self.fin_count
    if fin_count > 0 then
        local goods = {}
        local conf = self.prototype
        goods[conf.produce_id] = fin_count
        return goods
    end
end

--领取
function Product:receive()
    self.wheel_count = 0
    self:save_fin_count(0)
end

-- 获取剩余生产时间
function Product:get_rem_time()
    return (self.rem_count * self.prod_time) - (quanta.now - self.start_time)
end

--加速
function Product:speedup(time)
    local now = quanta.now
    local prod_time = self.prod_time - self.done_time
    -- 当前轮生产剩余时间
    local last_time = self.end_time - now
    -- 当前轮已逝时间
    local elapse_time = prod_time - last_time
    if time <= prod_time then
        -- 加速时间
        local speedup_time = mmin(last_time, time)
        if speedup_time >= last_time then
            self:change_fin_count(self.prod_count)
        else
            self:save_end_time(self.end_time - speedup_time)
            return
        end
        time = time - speedup_time
    elseif time >= self:get_rem_time() then
        self:change_fin_count(self.rem_count * self.prod_count)
    else
        local fcount = mmin(self.rem_count, time // prod_time)
        time = (time - (prod_time * fcount))
        if time > last_time then
            fcount = mmin(fcount + 1, self.rem_count)
            time = 0
            elapse_time = 0
        end
        self:change_fin_count(fcount * self.prod_count)
    end

    if self.rem_count > 0 then
        self:save_start_time(now)
        self:save_end_time(now + prod_time - time - elapse_time)
    else
        self:save_status(END)
    end

    if self.fin_count > 0 then
        self.parent:done_product(self)
    end
end

--刷新
function Product:update(now)
    if not self:is_work() then
        return
    end
    if now >= self.end_time then
        self:change_fin_count(self.prod_count)
        if self.rem_count <= 0 then
            self:save_status(END)
        end
        self.parent:done_product(self)
    end
end

--序列化
function Product:pack2db()
    local result = {
        id = self.id,
        master = self.master,
        rem_count = self.rem_count,
        fin_count = self.fin_count,
        tgt_count = self.tgt_count,
        status = self.status,
        costs = self.costs,
        prod_count = self.prod_count,
        prod_time = self.prod_time,
        done_time = self.done_time,
        work_time = self.work_time,
        start_time = self.start_time,
        end_time = self.end_time,
        extd_time = self.extd_time,
        sort = self.sort
    }
    return result
end

--序列化
function Product:pack_client()
    local result = {
        id = self.id,
        master = self.master,
        rem_count = self.rem_count,
        fin_count = self.fin_count,
        tgt_count = self.tgt_count,
        status = self.status,
        prod_count = self.prod_count,
        prod_time = mmax(self.prod_time - self.done_time, 1),
        last_time = self.last_time,
        work_time = self.work_time,
        start_time = self.start_time,
        end_time = self.end_time,
        sort = self.sort
    }
    return result
end

return Product
