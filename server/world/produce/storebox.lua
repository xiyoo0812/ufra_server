-- 储物箱
local config_mgr = quanta.get("config_mgr")
local item_db    = config_mgr:get_table("item")

local mmin         = math.min

local StoreBox   = class()
local prop       = property(StoreBox)
prop:reader("id", 0)
prop:reader("overlies", {})
prop:reader("sum_count", 0)

local dbprop = db_property(StoreBox, "home_produce")
dbprop:store_value("prod_id", 0)
dbprop:store_value("prod_count", 0)
dbprop:store_values("extras", {})

function StoreBox:__init(id, prod_id)
    self.id = id
end

function StoreBox:load_data(data)
    self.prod_id = data.prod_id or 0
    self.prod_count = data.prod_count or 0
    self:set_extras(data.extras or {})
    -- 初始化物品上限
    for item_id,_ in pairs(self.extras) do
        if not self.overlies[item_id] then
            local conf = item_db:find_one(item_id)
            if conf then
                self.overlies[item_id] = conf.overlie
            end
        end
    end
    if not self.overlies[self.prod_id] then
        local prod_conf = item_db:find_one(self.prod_id)
        if prod_conf then
            self.overlies[self.prod_id] = prod_conf.overlie
        end
    end
end

-- 检查解除绑定
function StoreBox:check_unbind()
    if self.prod_count > 0 or next(self.extras) then
        return false
    end
    return true
end

-- 设置产品id
function StoreBox:set_prod_id(prod_id)
    self.prod_id = prod_id
end

-- 检查满了
function StoreBox:check_full()
    if self.prod_id == 0 then
        return false
    end
    if self.prod_count >= (self.overlies[self.prod_id] or 0) then
        return true
    end
    return false
end

-- 检查是否有物品
function StoreBox:check_have()
    if self.prod_count > 0 then
        return true
    end
    if next(self.extras) then
        return true
    end
    return false
end

-- 获取物品
function StoreBox:goods()
    local good_count = 0
    local goods = {[self.prod_id]=self.prod_count}
    for id,count in pairs(self.extras) do
        goods[id] = (goods[id] or 0) + count
        good_count = good_count+count
    end
    good_count = good_count + self.prod_count
    return goods, good_count
end

-- 存储物品
function StoreBox:putin(items)
    local surplus_items = {}
    if not next(items) then
        return surplus_items
    end
    for item_id,item_count in pairs(items) do
        if item_id == self.prod_id then
            if not self.overlies[item_id] then
                local conf = item_db:find_one(item_id)
                if conf then
                    self.overlies[item_id] = conf.overlie
                end
            end

            local overlie = self.overlies[item_id] or 0
            local change_count = mmin(overlie - self.prod_count, item_count)
            if change_count > 0 then
                self:save_prod_count(self.prod_count+change_count)
            end
            if change_count < item_count then
                surplus_items[item_id] = item_count - change_count
            end
        end
    end
    return surplus_items
end

-- 存储额外物品
function StoreBox:putin_extra(items)
    for item_id,item_count in pairs(items) do
        if not self.overlies[item_id] then
            local conf = item_db:find_one(item_id)
            if conf then
                self.overlies[item_id] = conf.overlie
            end
        end
        local count = self.extras[item_id] or 0
        local overlie = self.overlies[item_id] or 0
        item_count = mmin(overlie - count, item_count)
        if item_count > 0 then
            self:save_extras_field(item_id, count + item_count)
        end
    end
end

-- 领取物品
function StoreBox:takeout()
    self:save_prod_count(0)
    self:save_extras({})
end

--序列化
function StoreBox:pack2db()
    return {
        prod_id = self.prod_id,
        prod_count = self.prod_count,
        extras = self.extras
    }
end

--序列化
function StoreBox:pack_client()
    local result = {
        id = self.id,
        items = {},
        extras = self.extras
    }
    if self.prod_id > 0  and self.prod_count > 0 then
        result.items[self.prod_id] = self.prod_count
    end
    return result
end

return StoreBox
