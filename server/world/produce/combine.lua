--combine.lua
local qmin  = qmath.min

local Combine = class()
local prop = property(Combine)
prop:reader("id", 0)            --id
prop:reader("uuid", 0)          --uuid
prop:reader("prototype", nil)   --prototype
prop:accessor("basetime", 0)    --basetime

local dbprop = db_property(Combine, "home_produce")
dbprop:store_values("makings", {})  --makings

function Combine:__init(uuid, conf)
    self.uuid = uuid
    self.id = conf.id
    self.prototype = conf
end

function Combine:load_data(data)
    self.makings = data.makings
    self.basetime = data.basetime
end

function Combine:fillin(item_id, count)
    local old_count = self.makings[item_id] or 0
    self:save_makings_field(item_id, old_count + count)
end

function Combine:takeout(item_id)
    local count = self.makings[item_id]
    if count then
        self:del_makings_field(item_id)
        return { [item_id] = count }
    end
end

-- 获取填入物品
function Combine:get_fill_items()
    local result = {}
    for _, arr in pairs(self.prototype.makings) do
        local item_id = arr[1]
        local item_count = arr[2]
        result[item_id] = item_count
    end
    return result
end

--检查能否填充
function Combine:check_fill(makings)
    local costs = {}
    local proto_items = self:get_fill_items()
    for item_id, count in pairs(makings) do
        local rcount = proto_items[item_id]
        if not rcount then
            return false
        end
        local ccount = self.makings[item_id] or 0
        if ccount == rcount then
            return false
        end
        costs[item_id] = qmin(count, rcount - ccount)
    end
    return true, costs
end

--检查能否取消
function Combine:check_cancel()
    return true
end

--获取产品
function Combine:goods()
    local proto_items =self:get_fill_items()
    for item_id, count in pairs(proto_items) do
        if self.makings[item_id] ~= count then
            return
        end
    end
    local goods = {}
    goods[self.prototype.produce_id] = 1
    -- 额外奖励
    for item_id, count in pairs(self.prototype.rewards or {}) do
        goods[item_id] = count
    end
    return goods
end

--取消返回材料
function Combine:cancel(item_id)
    if item_id then
        return { [item_id] = self.makings[item_id] }
    end
    return self.makings
end

--序列化
function Combine:pack2db()
    return {
        id = self.id,
        makings = self.makings,
        basetime = self.basetime
    }
end

return Combine
