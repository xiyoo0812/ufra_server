-- prode_gather.lua
-- 采集生产
local log_debug           = logger.debug
local log_err             = logger.err
local log_warn            = logger.warn
local mfloor              = math.floor
local mmin                = math.min
local mmax                = math.max
local tinsert             = table.insert
local tsize               = qtable.size

local object_fty          = quanta.get("object_fty")
local protobuf_mgr        = quanta.get("protobuf_mgr")
local config_mgr          = quanta.get("config_mgr")
local event_mgr           = quanta.get("event_mgr")

local utility_db          = config_mgr:init_table("utility", "key")
local FULL                = protobuf_mgr:enum("product_status", "PS_FULL")

local GATHER_OFFLINE_TIME = utility_db:find_integer("value", "gather_offline_time") or 0
local GATHER_CYCLE_TIME   = utility_db:find_integer("value", "gather_cycle_time") or 0

local FRAME_FAILED        = protobuf_mgr:error_code("FRAME_FAILED")
local FRAME_SUCCESS       = protobuf_mgr:error_code("FRAME_SUCCESS")
local PRODUCT_NUM_ERR     = protobuf_mgr:error_code("WORKS_PRODUCT_NUM_ERR")

local ProdeBase           = import("world/produce/prode_base.lua")

local ProdeGather         = class(ProdeBase)
local dbprop              = db_property(ProdeGather, "home_produce")
dbprop:store_value("last_sum_cap", 0)  -- 记录最后一次总产能
dbprop:store_value("last_cost_cap", 0) -- 记录最后一次消耗的产能

function ProdeGather:__init()
end

-- 加载db数据
function ProdeGather:load_db(dbdata)
    ProdeBase.load_db(self, dbdata)
    self.last_sum_cap = dbdata.last_sum_cap or 0
    self.last_cost_cap = dbdata.last_cost_cap or 0
end

-- 初始化配方
function ProdeGather:init_formulas()
    log_debug("[ProdeGather][init_formulas]...")
    self.formulas = {}
    local collect_points = self.prototype.collect_points or {}
    for _, id in pairs(collect_points) do
        log_debug("[ProdeGather][init_formulas] begin id:{}", id)
        if self.scene:check_collpoint(id) then
            log_debug("[ProdeGather][init_formulas] activie id:{}", id)
            -- 根据采集点,读取配方
            local conf = object_fty:find_collpoint_id(id)
            if conf then
                for _, formula_id in pairs(conf.formulas or {}) do
                    self.formulas[formula_id] = formula_id
                end
            end
        end
    end
end

function ProdeGather:on_setup()
    log_debug("[ProdeGather][on_setup]...")
    self:init_formulas()
end

function ProdeGather:on_startup()
    self:update_effects()
    self:off_update()
    self:start_produce()
end

-- 完成事件
function ProdeGather:on_done_event(product, stash_full)
    local storebox = self:get_storebox(product)
    if storebox then
        if stash_full then
            if not self:check_storebox_capacity() then
                self:pause_product()
                self:change_status(FULL)
            end
        end
    end
end

-- 激活采集点事件
function ProdeGather:on_active_collpoint(proto_id)
    local conf = object_fty:find_collpoint_id(proto_id)
    if conf then
        for _, formula_id in pairs(conf.formulas or {}) do
            self.formulas[formula_id] = formula_id
        end
    end
end

-- 派驻npc事件
function ProdeGather:on_send_partner()
    -- 开始生产
    self:start_produce()
end

-- 升级检测
function ProdeGather:check_upgrade()
    return true
end

-- 召回npc事件
function ProdeGather:on_recall_partner()
    for uuid, product in pairs(self.products) do
        self:remove_product(product, uuid)
    end
end

-- 更新效果事件
function ProdeGather:on_update_effects()
    -- 更新满意度配置
    local sat_confs = self.effects.stage_capacity_per or {}
    log_debug("[ProdeGather][on_update_effects] sat_confs:{}", sat_confs)
    for id, value in pairs(sat_confs) do
        if self.sat_confs[id] ~= value then
            self:save_sat_confs_field(id, value)
        end
    end
end

-- 清理产品
function ProdeGather:clear_product()
    ProdeBase.clear_product(self)
    self:save_last_sum_cap(0)
    self:save_last_cost_cap(0)
end

-- 属性变更
function ProdeGather:on_effects_change()
    if self:get_product_count() == 0 or self.last_cost_cap == 0 or not self.is_startup then
        return
    end

    self:update_effects()
    -- 当前总产能
    local sum_cap = self:prod_cap()
    -- 更新
    self:up_product_effect(sum_cap)
end

-- 更新产品效果
function ProdeGather:up_product_effect(sum_cap)
    local low_sum_cap = self.last_sum_cap
    if low_sum_cap == 0 then
        log_err(
            "[ProdeGather][up_product_effect] low_sum_cap == 0 master:{} id:{} low_sum_cap:{} sum_cap:{}",
            self.master, self.id, low_sum_cap, sum_cap)
        low_sum_cap = sum_cap
    end
    self:save_last_sum_cap(sum_cap)
    -- 产能没有变化
    if low_sum_cap == sum_cap then
        log_debug(
            "[ProdeGather][up_product_effect] diff_prod_cap == sum_cap master:{} id:{} low_sum_cap:{} sum_cap:{}",
            self.master, self.id, low_sum_cap, sum_cap)
        return
    end

    local cost_sum_cap = 0
    for _, product in pairs(self.products) do
        local formula = object_fty:find_formula(product.id)
        if formula then
            log_debug(
                "[ProdeGather][up_product_effect] master:{} id:{} low_sum_cap:{} sum_cap:{} prod_time:{} tgt_count:{}",
                self.master, self.id, low_sum_cap, sum_cap, product:get_prod_time(), product:get_tgt_count())
            -- 消耗的产能
            local cost_cap = formula.worth * product.tgt_count
            -- 产能占比
            local cap_rate = cost_cap / low_sum_cap
            -- 转换的产能
            local cur_cost_cap = self.last_sum_cap * cap_rate
            -- 目标数量
            local tgt_count = cur_cost_cap / formula.worth
            -- 产能未超出的情况下采用四舍五入
            if (mfloor(tgt_count + 0.5) * formula.worth) <= self.last_sum_cap then
                tgt_count = mfloor(tgt_count + 0.5)
            else
                tgt_count = mfloor(tgt_count)
            end
            local prod_time = mfloor(GATHER_CYCLE_TIME / tgt_count)
            log_debug(
                "[ProdeGather][up_product_effect] source_tgt_count:{} cost_cap:{} low_sum_cap:{} cap_rate:{} cur_cost_cap:{} last_sum_cap:{}",
                product.tgt_count, cost_cap, low_sum_cap, cap_rate, cur_cost_cap, self.last_sum_cap)
            log_debug("[ProdeGather][up_product_effect] master:{} id:{} prod_time:{} worth:{} tgt_count:{} prod_time:{}",
                self.master, self.id, prod_time, formula.worth, cap_rate, tgt_count, prod_time)
            -- 更新属性
            if product:up_effects(prod_time, product:get_done_time(), tgt_count) then
                self:sync_product(product)
            end
            cost_sum_cap = cost_sum_cap + (tgt_count * formula.worth)
        end
    end
    self:save_last_cost_cap(cost_sum_cap)
end

-- 生产产能
function ProdeGather:prod_cap(in_cap_per)
    local build_cap_num = self.prototype.prod_cap or 0
    local capacity_per = in_cap_per and in_cap_per or (self.effects.capacity_per or 0)
    local capacity_num = self.effects.capacity_num or 0
    local npc_cap = mfloor(build_cap_num * (capacity_per / 100))
    local sum_cap_num = build_cap_num + npc_cap + capacity_num
    log_debug(
        "[ProdeGather][prod_cap] master:{} in_cap_per:{} build_cap_num:{} capacity_per:{} capacity_num:{} npc_cap:{} sum_cap_num:{}",
        self.master, in_cap_per, self.prototype.prod_cap, capacity_per, capacity_num, npc_cap, sum_cap_num)
    return sum_cap_num
end

-- 配方产能消耗
function ProdeGather:formula_costcap(formulas)
    local cost_cap = 0
    for id, count in pairs(formulas) do
        if count and count > 0 then
            local conf = object_fty:find_formula(id)
            if not conf then
                return 0
            end
            cost_cap = cost_cap + (conf.worth * count)
        end
    end
    return cost_cap
end

-- 检测采集清理
function ProdeGather:check_gather_clear(formulas)
    if not formulas or not next(formulas) then
        return true
    end
    for _, count in pairs(formulas) do
        if count > 0 then
            return false
        end
    end
    return true
end

-- 检测生产满载
function ProdeGather:check_produce_full()
    for _, product in ipairs(self.product_list) do
        if product:get_status() ~= FULL then
            return false
        end
    end
    return true
end

-- 检查产品满载
function ProdeGather:check_product_full(storebox, stash_full)
    if stash_full and not self:check_storebox_capacity() then
        return true
    end
    return ProdeBase.check_product_full(self, storebox, stash_full)
end

-- npc召回验证
function ProdeGather:check_npc_recall()
    return true
end

function ProdeGather:check_queue(formula)
    if self.storeboxs[formula.produce_id] then
        return true
    end
    local store_count = tsize(self.storeboxs)
    local queue_count = self:get_queue_count()
    return queue_count - store_count > 0
end

-- 构建产能配置
function ProdeGather:build_procap_conf(formulas)
    if not GATHER_CYCLE_TIME or GATHER_CYCLE_TIME <= 0 then
        return FRAME_FAILED, nil, 0
    end

    local sum_prod_cap = self:prod_cap()
    local sum_cost_cap = self:formula_costcap(formulas)
    -- 临时代码
    if sum_cost_cap > sum_prod_cap and  1== 2 then
        log_warn("[ProdeGather][build_procap_conf] is fail,confs is error,master:{} sum_cost_cap:{} sum_prod_cap:{}",
            self.master, sum_cost_cap, sum_prod_cap)
        return PRODUCT_NUM_ERR, nil, 0
    end
    local confs = {}
    local cost_sum_cap = 0
    for id, count in pairs(formulas) do
        if count and count > 0 then
            local formula = object_fty:find_formula(id)
            if formula then
                confs[id] = {
                    time = mfloor(GATHER_CYCLE_TIME / count),
                    count = count
                }
                cost_sum_cap = cost_sum_cap + (formula.worth * count)
            end
        end
    end
    return FRAME_SUCCESS, confs, cost_sum_cap
end

-- 最大离线时间
function ProdeGather:max_off_time(time)
    return mmin(time, GATHER_OFFLINE_TIME)
end

-- 获取储物箱
function ProdeGather:get_storebox(product)
    return self.storeboxs[product:get_prod_id()]
end

-- 获取储物箱容量
function ProdeGather:store_cap()
    return self.prototype.store_cap or 0
end

-- 获取储物箱剩余容量
function ProdeGather:storebox_lastcap()
    local store_cap = self:store_cap()
    local sum_prod_count = 0
    for _, storebox in pairs(self.storeboxs) do
        local prod_count = storebox:get_prod_count()
        sum_prod_count = sum_prod_count + prod_count
    end
    return mmax(store_cap - sum_prod_count, 0)
end

-- 检查储物箱容量
function ProdeGather:check_storebox_capacity()
    return self:storebox_lastcap() > 0
end

-- 储物箱id
function ProdeGather:storebox_id(product)
    return product:get_prod_id()
end

-- 添加产品
function ProdeGather:add_product(confs, cost_sum_cap, done_ratios)
    local count = 0
    self:save_last_cost_cap(cost_sum_cap)
    self:save_last_sum_cap(self:prod_cap())
    for id, conf in pairs(confs or {}) do
        local product = object_fty:create_product(self.prototype.fn_type, id)
        if not product then
            log_err("[ProdeGather][add_product] is fail master:{} id:{} proto_id:{}", self.master, self.id, id)
            goto continue
        end

        -- 继承时间
        local extd_time = 0
        local ratio = done_ratios[id]
        if ratio then
            extd_time = mfloor(conf.time * (ratio / 100)) or 0
        end
        product:startup(self, {
            prod_time = conf.time,
            tgt_count = conf.count,
            extd_time = extd_time
        })
        tinsert(self.product_list, product)
        self:save_products_elem(product.uuid, product)
        count = count + 1
        -- 开始生产
        self:start_produce()
        -- 同步产品
        self:sync_product(product)
        ::continue::
    end
    return count >= 1 and true or false
end

-- 解除绑定
function ProdeGather:storebox_unbind(storebox)
    if not storebox then
        return
    end
    local id = storebox:get_id()
    for _, product in pairs(self.products) do
        if product:get_prod_id() == id then
            return
        end
    end
    if storebox:check_unbind() then
        self:del_storeboxs_elem(id)
    end
end

-- 获取物品数量
function ProdeGather:get_items_count(items)
    local count = 0
    for _, value in pairs(items) do
        count = count + value
    end
    return count
end

-- 存入物品
function ProdeGather:storebox_putin(storebox, items, prod_end)
    local last_space = self:storebox_lastcap()
    if last_space <= 0 then
        return items
    end
    local putint = false
    local surplus_items = {}
    local drop_count = self:get_items_count(items)
    if drop_count <= last_space then
        putint = true
        surplus_items = storebox:putin(items)
    else
        for id, count in pairs(items) do
            -- 生产占比
            local scale = count / drop_count
            -- 物品数量(四舍五入)
            local item_count = mfloor(last_space * scale + 0.5)
            if item_count > 0 then
                surplus_items[id] = count - item_count
                items[id] = item_count
            else
                items[id] = nil
            end
        end
        if next(items) then
            putint = true
            surplus_items = storebox:putin(items)
        end
    end

    if putint and prod_end then
        event_mgr:notify_listener("on_produce_storebox_putin", self.master, self:get_group(), self.id)
    end

    self:storebox_sync({ storebox })
    return surplus_items
end

-- 离线生产
function ProdeGather:off_update(player)
    if #self.product_list == 0 then
        return
    end

    -- 满意度时间阶段
    local sat_off_times = player:get_satoff_time()
    log_debug("[ProdeGather][off_update] begin master:{} id:{} sat_off_times:{}",
        self.master, self.id, sat_off_times)
    for _, product in ipairs(self.product_list) do
        local storebox = self:storebox_allot(product)
        if storebox then
            -- 返回的数据倒序处理
            if sat_off_times and next(sat_off_times) then
                for _, conf in pairs(sat_off_times) do
                    local sat_val = self.sat_confs[conf.id]
                    if not sat_val then
                        log_err("[ProdeGather][off_update] sat_val is nill, master:{} id:{} sat_confs:{}-{}", self.master,
                            self.sat_confs, conf.id)
                        goto continue
                    end
                    local sum_cap = self:prod_cap(sat_val)
                    self:up_product_effect(sum_cap)
                    local off_time = self:max_off_time(conf.time)
                    log_debug(
                        "[ProdeGather][off_update] log master:{} id:{} off_time:{} prod_time:{} done_time:{} sat_off_times:{}",
                        self.master, self.id, off_time, product:get_prod_time(), product:get_done_time(), sat_off_times)
                    local result = product:off_update(quanta.now, off_time, product:get_prod_time())
                    if result then
                        self:sync_product(product)
                    end
                end
            else
                local base_time = quanta.now
                local elapse_time = self:max_off_time(quanta.now - base_time)
                local result = product:off_update(quanta.now, elapse_time, product:get_prod_time())
                if result then
                    self:sync_product(product)
                end
            end
            :: continue ::
        end
    end
    self:off_drop()
end

-- 产品完成比
function ProdeGather:pack_done_ratios()
    local result = {}
    local now = quanta.now
    for _, product in pairs(self.products) do
        local done_time = 0
        if product:is_work() then
            done_time = now - product.start_time
        elseif product:is_pause() then
            done_time = product.prod_time - product.last_time
        end
        if done_time > 0 then
            result[product.id] = mfloor((done_time / product.prod_time) * 100)
        end
    end
    return result
end

--序列化
function ProdeGather:pack2db()
    local dbdata = ProdeBase.pack2db(self)
    dbdata.last_sum_cap = self.last_sum_cap
    dbdata.last_cost_cap = self.last_cost_cap
    dbdata.storeboxs = self:get_storeboxs()
    return dbdata
end

return ProdeGather
