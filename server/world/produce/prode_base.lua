-- prod_base.lua
local log_err                = logger.err
local log_warn               = logger.warn
local log_debug              = logger.debug
local tinsert                = table.insert
local tremove                = table.remove
local tsort                  = table.sort

local object_fty             = quanta.get("object_fty")
local protobuf_mgr           = quanta.get("protobuf_mgr")
local event_mgr              = quanta.get("event_mgr")

local FREE                   = protobuf_mgr:enum("product_status", "PS_FREE")
local WORK                   = protobuf_mgr:enum("product_status", "PS_WORK")
local FULL                   = protobuf_mgr:enum("product_status", "PS_FULL")
local NPC                    = protobuf_mgr:enum("product_status", "PS_NPC")
local SERIAL                 = protobuf_mgr:enum("produce_mode", "PM_SERIAL")
local PARALLEL               = protobuf_mgr:enum("produce_mode", "PM_PARALLEL")

local FRAME_SUCCESS          = protobuf_mgr:error_code("FRAME_SUCCESS")
local PRODUCT_PRODUCE        = protobuf_mgr:error_code("WORKS_PRODUCT_PRODUCE")
local PRODUCT_NOT_RECV       = protobuf_mgr:error_code("WORKS_PRODUCT_NOT_RECV")

local ProdeStoreboxComponent = import("world/component/prode_storebox_component.lua")
local ProdeSkillComponent    = import("world/component/prode_skill_component.lua")
local ProdeOffLineComponent  = import("world/component/prode_offline_component.lua")

local ProdeBase              = class(nil, ProdeOffLineComponent, ProdeStoreboxComponent, ProdeSkillComponent)
local prop                   = property(ProdeBase)
prop:reader("id", nil)
prop:reader("prototype", nil)
prop:accessor("proto_id", nil)
prop:accessor("master", nil)
prop:accessor("scene", nil)
prop:accessor("building", nil)
prop:reader("interval", 60)
prop:reader("last_time", 0)
prop:reader("formulas", {})       --当前等级的配方
prop:reader("product_list", {})   --生产列表
prop:reader("online", true)       --在线状态
prop:reader("is_startup", false)  --是否启动
prop:reader("max_queue_count", 0) --最大队列数量

local dbprop = db_property(ProdeBase, "home_produce")
dbprop:store_value("status", FREE)         -- 生产状态
dbprop:store_objects("products", {})       -- 生产列表
dbprop:store_value("queue_count", 0)       -- 队列数量(保留升级后最大的队列数量)
dbprop:store_value("delive_status", true)  -- 配送购买状态(false 未解锁 true 已解锁)
dbprop:store_value("delive_enable", false) -- 配送开启状态(false 关闭 true 开启)
dbprop:store_values("pay_queues", {})      -- 购买的队列

function ProdeBase:__init(id, conf)
    self.id = id
    self.prototype = conf
    self.proto_id = conf.id
    self.max_queue_count = object_fty:find_utensil_maxqueue(self.prototype.group)
end

------------------------------------------------------------------------------------
-- 初始化数据
------------------------------------------------------------------------------------
-- 加载db数据
function ProdeBase:load_db(dbdata)
    log_debug("[ProdeBase][load_db] id:{} dbdata:{}", self.id, dbdata)
    self:set_status(dbdata.status or FREE)
    self:set_queue_count(dbdata.queue_count or (self.prototype.queue or 0))
    if self.queue_count <=0 then
        self:set_queue_count(self.prototype.queue)
    end
    self:set_delive_status(dbdata.delive_status or true)
    self:set_delive_enable(dbdata.delive_enable or false)
    self:set_pay_queues(dbdata.pay_queues or {})
    for uuid, item in pairs(dbdata.products or {}) do
        local product = object_fty:create_product(self.prototype.fn_type, item.id, uuid)
        self:set_products_elem(product.uuid, product)
        product:load_data(self, item)
        tinsert(self.product_list, product)
    end
    tsort(self.product_list, function(a, b)
        return a.sort < b.sort
    end)
    self:invoke("_load_db", dbdata)
end

-- 安装
function ProdeBase:setup(town, building)
    self.scene = town
    self.master = town:get_master()
    self.building = building
    self:on_setup()
    if self:get_product_count() == 0 then
        self.is_startup = true
    end
    self:startup()
end

-- 启动
function ProdeBase:startup()
    if self.is_startup then
        log_debug("[ProdeBase][startup] is_startup=true master:{} id:{}", self.master, self.id)
        return
    end
    log_debug("[ProdeBase][startup] log master:{} id:{}", self.master, self.id)
    self:on_startup()
    self.is_startup = true
end

------------------------------------------------------------------------------------
-- 属性数据
------------------------------------------------------------------------------------
-- 设置在线状态
function ProdeBase:set_online(online)
    self.online = online
end

-- 生产类型
function ProdeBase:is_produce()
    return true
end

-- 检测串行生产
function ProdeBase:check_serial()
    return self.prototype.prod_mode == SERIAL
end

-- 检测并行生产
function ProdeBase:check_parallel()
    return self.prototype.prod_mode == PARALLEL
end

-- 检测生产满载
function ProdeBase:check_produce_full()
    local full_count = self:get_product_count(FULL)
    local queue_count, _ = self:get_queue_count()
    if full_count == queue_count then
        return true
    end
    return false
end

-- npc召回验证
function ProdeBase:check_npc_recall()
    if self.building:get_partner_count() > 1 then
        return true
    end
    if self:get_product_count() > 0 then
        return false
    end
    return true
end

-- 检查回收
function ProdeBase:check_takeback()
    -- 产品检查
    if next(self.products) then
        return PRODUCT_PRODUCE
    end
    if not self:storebox_check_takeback() then
        return PRODUCT_NOT_RECV
    end
    return FRAME_SUCCESS
end

-- 产品数量
function ProdeBase:get_product_count(status)
    if not status then
        return #self.product_list
    else
        local count = 0
        for _, product in ipairs(self.product_list) do
            if product:get_status() == status then
                count = count + 1
            end
        end
        return count
    end
end

-- 获取分组id
function ProdeBase:get_group()
    return self.prototype.group
end

-- 队列数量
function ProdeBase:get_queue_count()
    return self.queue_count, self.max_queue_count
end

-- 返回物品
function ProdeBase:tackback_obtain(costs, reason)
    -- 未解锁
    if self.delive_status == false then
        return nil
    end
    return { drop = costs, reason = reason }
end

-- 升级检测
function ProdeBase:check_upgrade(conf)
    local formulas = {}
    for _, formula_id in pairs(conf.formulas) do
        formulas[formula_id] = formula_id
    end
    for _, product in ipairs(self.product_list) do
        if not formulas[product:get_id()] then
            return false
        end
    end
    return true
end

-- 配方是否合法
function ProdeBase:check_formula(formulas)
    for _, formula_id in pairs(formulas) do
        if self.formulas[formula_id] then
            return true
        end
    end
    return false
end

------------------------------------------------------------------------------------
-- 建筑事件
------------------------------------------------------------------------------------
-- 升级
function ProdeBase:on_upgrade(conf)
    self.prototype = conf
    self:invoke("_on_upgrade", conf)
    -- 更新队列数量
    if self.prototype.queue ~= self.queue_count then
        self:save_queue_count(self.prototype.queue)
    end
    self:init_formulas()
    self:on_effects_change()
    self:start_produce()
    if self.sync_uqconfs and next(self.sync_uqconfs) then
        self:sync_unlock_conf(self.sync_uqconfs)
    end
end

------------------------------------------------------------------------------------
-- 解锁相关逻辑
------------------------------------------------------------------------------------
-- 检查队列
function ProdeBase:check_queue()
    local count = self:get_queue_count()
    return count > #self.product_list
end

-- 分配队列
function ProdeBase:allot_queue()
    return #self.product_list + 1
end

-- 解锁配送
function ProdeBase:unlock_delive()
    self:save_delive_status(true)
end

-- 切换配送
function ProdeBase:switch_delive_enable(enable)
    if enable ~= self.delive_enable then
        self:save_delive_enable(enable)
        return true
    end
    return false
end

------------------------------------------------------------------------------------
-- 产品相关
------------------------------------------------------------------------------------
-- 获取头部产品
function ProdeBase:head_product()
    local index = next(self.product_list)
    if not index then
        return nil
    end
    return self.product_list[index]
end

-- 检查产品满载
function ProdeBase:check_product_full(storebox, stash_full)
    if not storebox:check_full() then
        return false
    end
    -- 开启配送时(验证储物箱与仓库是否都满了)
    return stash_full
end

-- 交换位置
function ProdeBase:product_swap(source_uuid, target_uuid)
    local s_product = self.products[source_uuid]
    if not s_product then
        log_debug("[ProdeBase][product_swap] s_product is nil master:{} id:{} source_uuid:{}", self.master, self.id,
            source_uuid)
        return false
    end
    local t_product = self.products[target_uuid]
    if not t_product then
        log_debug("[ProdeBase][product_swap] t_product is nil master:{} id:{} target_uuid:{}", self.master, self.id,
            target_uuid)
        return false
    end
    if s_product:get_uuid() == t_product:get_uuid() then
        log_debug("[ProdeBase][product_swap] cur_sort == sort master:{} id:{} source_uuid:{} target_uuid:{}", self
            .master,
            self.id, source_uuid, target_uuid)
        return true
    end
    local products = { s_product, t_product }
    for _, product in pairs(products) do
        if product:is_work() then
            local storebox = self:get_storebox(product)
            if storebox and storebox:check_unbind() then
                self:del_storeboxs_elem(storebox:get_id())
            end
            product:pause_work()
            self:sync_product(product)
        end
    end
    tremove(self.product_list, s_product:get_sort())
    tinsert(self.product_list, t_product:get_sort(), s_product)
    self:product_sort()
    self:start_produce()
    return true
end

-- 产品排序
function ProdeBase:product_sort()
    -- 同步产品排序
    local result = {}
    local sort = 1
    for _, product in ipairs(self.product_list) do
        if product:change_sort(sort) then
            result[product:get_uuid()] = sort
        end
        sort = sort + 1
    end
    -- 同步数据
    if next(result) then
        local sync_datas = { id = self.id, sorts = result }
        log_debug("[ProdeBase][product_sort] master:{} id:{} sync_datas:{}", self.master, self.id, sync_datas)
        self.scene:broadcast_message("NID_PRODUCE_SORT_NTF", sync_datas)
    end
end

-- 产品加速
function ProdeBase:product_speedup(product, time)
    product:speedup(time)
    if self:get_products(product:get_uuid()) then
        self:sync_product(product)
    end
end

-- 添加产品
function ProdeBase:add_product(proto_id, opts)
    local product = object_fty:create_product(self.prototype.fn_type, proto_id)
    if not product then
        log_err("[ProdeBase][add_product] is fail master:{} id:{} proto_id:{}", self.master, self.id, proto_id)
        return nil
    end
    product:startup(self, opts)
    tinsert(self.product_list, product)
    self:save_products_elem(product.uuid, product)
    self:start_produce()
    self:sync_product(product)
    return product
end

-- 完成生产
function ProdeBase:done_product(product)
    local conf = product:get_prototype()
    event_mgr:notify_listener("on_product_done", self.master, product:get_prod_id(), product:get_prod_count(), conf.group)
    local goods = product:goods()
    local storebox = self:get_storebox(product)
    if not storebox then
        log_warn("[ProdeBase][done_product] storebox_allot is fail master:{} id:{} product_uuid:{}",
            self.master, self.id, product:get_uuid())
        return
    end
    -- 技能生产
    local skill_goods = self:skill_effect(product)
    product:receive()
    -- 自动配送(产出物品)
    local stash_full = self:storebox_auto_delive(storebox, goods, product:is_end())
    self:storebox_auto_delive_extra(storebox, skill_goods)
    if not product:is_end() and self:check_product_full(storebox, stash_full) then
        product:set_full()
        self:sync_product(product)
        if self:check_serial() then
            self:change_status(FULL)
        elseif self:check_parallel() and self:check_produce_full() then
            self:change_status(FULL)
        end
    end
    -- 生产结束,删除生产
    if product:is_end() then
        self:remove_product(product, product:get_uuid())
        self:start_produce()
    else
        if product:is_work() then
            product:next_work()
        end
        self:sync_product(product)
    end
    -- 完成事件
    self:on_done_event(product, stash_full)
    return true
end

-- 领取产品
function ProdeBase:recv_product(product)
    product:receive()
    self:sync_product(product)
end

-- 清理产品
function ProdeBase:clear_product()
    for uuid, product in pairs(self.products) do
        self:remove_product(product, uuid)
    end
end

-- 暂停产品
function ProdeBase:pause_product(product)
    if product then
        product:pause_work()
    else
        for _, item in pairs(self.products) do
            item:pause_work()
            self:sync_product(item)
        end
    end
end

-- 移除产品
function ProdeBase:remove_product(product, product_uid)
    log_debug("[ProdeBase][remove_product] begin product_uid:{} length:{}", product_uid, #self.product_list)
    self:del_products_elem(product_uid)
    for i = #self.product_list, 1, -1 do
        local item = self.product_list[i]
        if item and item.uuid == product_uid then
            tremove(self.product_list, i)
        end
    end
    self:product_sort()
    log_debug("[ProdeBase][remove_product] end product_uid:{} length:{}", product_uid, #self.product_list)
    local storebox = self:get_storebox(product)
    self:storebox_unbind(storebox)
    self.scene:broadcast_message("NID_PRODUCE_PRODUCT_NTF", { id = self.id, uuid = product_uid })
end

-- 修改生产状态
function ProdeBase:change_status(status)
    if self:get_status() ~= status then
        self:save_status(status)
        self:sync_status()
    end
end

-- 串行生产
function ProdeBase:serial()
    local product = self:head_product()
    if not product then
        log_warn("[ProdeBase][serial] product is nil master:{} id:{}", self.master, self.id)
        return
    end
    if product:is_work() or product:is_end() then
        return
    end
    local storebox = self:get_storebox(product)
    if not storebox then
        storebox = self:storebox_allot(product)
    end
    if not storebox then
        self:change_status(FULL)
        log_warn("[ProdeBase][serial] storebox is nil master:{} id:{} product:{}", self.master, self.id,
            product:get_uuid())
        return
    end
    if product:is_full() and self:check_product_full(storebox, not self:check_delive()) then
        self:change_status(FULL)
        return
    end
    product:start_work()
    self:change_status(WORK)
    self:sync_product(product)
end

-- 并行生产
function ProdeBase:parallel()
    for _, product in ipairs(self.product_list) do
        if product:is_work() or product:is_end() then
            goto continue
        end
        local storebox = self:get_storebox(product)
        if not storebox then
            storebox = self:storebox_allot(product)
        end
        if not storebox then
            self:change_status(FULL)
            log_warn("[ProdeBase][parallel] storebox is nil master:{} id:{} product:{}", self.master, self.id,
                product:get_uuid())
            goto continue
        end
        if product:is_full() and self:check_product_full(storebox, not self:check_delive()) then
            product:set_full()
            self:sync_product(product)
            goto continue
        end
        product:start_work()
        self:change_status(WORK)
        self:sync_product(product)
        ::continue::
    end
end

-- 开始生产
function ProdeBase:start_produce()
    log_debug("[ProdeBase][start_produce] begin master:{} id:{}", self.master, self.id)
    -- 队列验证
    if #self.product_list == 0 then
        if not self:storebox_check_free() then
            self:change_status(FULL)
        else
            self:change_status(FREE)
        end
        return
    end
    -- npc验证
    if not self.building or not self.building:check_partner() then
        self:change_status(NPC)
        log_warn("[ProdeBase][start_produce] check_partner false master:{} id:{}", self.master, self.id)
        return
    end
    -- 串行
    if self:check_serial() then
        self:serial()
        -- 并行
    elseif self:check_parallel() then
        self:parallel()
    end
end

-- 同步生产
function ProdeBase:sync_changed()
    self.scene:broadcast_message("NID_PRODUCE_PRODUCE_NTF",
        { master = self.master, elems = { [self.id] = self:packclient()}, part_sync = true })
end

-- 同步生产状态
function ProdeBase:sync_status()
    local sync_datas = { id = self.id, status = self:get_status() }
    self.scene:broadcast_message("NID_PRODUCE_STATUS_NTF", sync_datas)
end

-- 同步产品
function ProdeBase:sync_product(product)
    local sync_datas = { id = self.id, uuid = product.uuid, product = product:pack_client(), part_sync = true }
    self.scene:broadcast_message("NID_PRODUCE_PRODUCT_NTF", sync_datas)
end

-- 更新定时器
function ProdeBase:update(now)
    if (not self.online or not self.is_startup) then
        return
    end
    if self:check_serial() then
        local product = self:head_product()
        if product then
            if product:is_work() then
                self:change_status(WORK)
                product:update(now)
                -- 托底逻辑
            elseif now - self.last_time > self.interval then
                self:start_produce()
                self.last_time = now
            end
        end
    elseif self:check_parallel() then
        for _, product in ipairs(self.product_list) do
            if product:is_work() then
                self:change_status(WORK)
                product:update(now)
            end
        end
    end
end

-- 产品列表
function ProdeBase:pack_products()
    local products = {}
    for uuid, product in pairs(self.products) do
        products[uuid] = product:pack_client()
    end
    return products
end

-- 生产信息
function ProdeBase:packclient()
    local queue_count, max_queue_count = self:get_queue_count()
    local data = {
        queue_count = queue_count,
        max_queue_count = max_queue_count,
        delive_status = self.delive_status,
        delive_enable = self.delive_enable,
        products = self:pack_products(),
        unconfs = self:pack_unconfs(),
        store_boxs = self:pack_storeboxs(),
        status = self:get_status()
    }
    return data
end

--序列化
function ProdeBase:pack2db()
    local dbdata = {}
    dbdata.products = self:pack_products()
    dbdata.status = self.status
    dbdata.queue_count = self.queue_count
    dbdata.delive_status = self.delive_status
    dbdata.delive_enable = self.delive_enable
    dbdata.pay_queues = self.pay_queues
    return dbdata
end

--解锁配置
function ProdeBase:pack_unconfs()
    return {}
end

--子类实现
------------------------------------------------
function ProdeBase:on_setup()
end

function ProdeBase:on_startup()
end

function ProdeBase:on_done_event(product, stash_full)
end

function ProdeBase:on_effects_change()
end

return ProdeBase
