--packbench.lua
-- 组装台建筑,提供功能:根据蓝图组装建筑
local log_debug     = logger.debug
local Queue         = import("container/queue.lua")
local object_fty    = quanta.get("object_fty")

local protobuf_mgr  = quanta.get("protobuf_mgr")
local FRAME_SUCCESS = protobuf_mgr:error_code("FRAME_SUCCESS")

local Packbench     = class()
local prop          = property(Packbench)
prop:reader("id", nil)
prop:reader("prototype", nil)
prop:accessor("proto_id", nil)
prop:accessor("master", nil)
prop:accessor("scene", nil)
prop:reader("blueprints", {})     --蓝图列表
prop:reader("produce_queue", nil) --生成队列

local dbprop = db_property(Packbench, "home_produce")
dbprop:store_objects("products", {}) -- 生产队列

function Packbench:__init(id, conf)
    self.id = id
    self.prototype = conf
    self.proto_id = conf.id
    self.produce_queue = Queue("id")
    --读取蓝图列表
    for _, bid in pairs(conf.formulas or {}) do
        self.blueprints[bid] = true
    end
    --前置蓝图列表
    for _, mid in pairs(conf.prepose or {}) do
        local utensil = object_fty:find_utensil(mid)
        if utensil then
            for _, bid in pairs(utensil.formulas or {}) do
                self.blueprints[bid] = true
            end
        end
    end
end

--从DB加载
function Packbench:load_db(data)
    for uuid, pdata in pairs(data.products or {}) do
        local combine = object_fty:create_combine(pdata.id, uuid)
        combine:load_data(pdata)
        self.produce_queue:push_back(combine)
        self:set_products_elem(combine.uuid, combine)
    end
end

-- 派驻npc事件
function Packbench:on_send_partner()
end

-- 属性变化事件
function Packbench:on_effects_change()
end

-- 满意度变化
function Packbench:on_sat_change()
end

-- 检查回收
function Packbench:check_takeback()
    return FRAME_SUCCESS
end

-- 返回物品
function Packbench:tackback_obtain()
    return nil
end

-- 安装
function Packbench:setup(town)
    self.scene = town
    self.master = town:get_master()
end

-- 启动
function Packbench:startup()
end

-- 升级验证
function Packbench:check_upgrade()
    return true
end

function Packbench:on_upgrade()
    local conf = self.prototype
    for _, fid in pairs(conf.formulas or {}) do
        self.blueprints[fid] = true
    end
    return true
end

function Packbench:is_working()
    if self.produce_queue:size() > 0 then
        return true
    end
    return false
end

function Packbench:is_produce()
    return false
end

function Packbench:check_queue_num(proto_id, player_id)
    return self.produce_queue:size() < self.prototype.queue
end

--添加制作配方
function Packbench:add_product(blue_id)
    local combine = object_fty:create_combine(blue_id)
    if not combine then
        log_debug("[Packbench][add_product] combine nil master(%s) id(%s) blue_id(%s)", self.master, self.id, blue_id)
        return false
    end
    if combine then
        combine:set_basetime(quanta.now)
        self.produce_queue:push_back(combine)
        self:save_products_elem(combine.uuid, combine)
        self:sync_combine(combine)
    end
    log_debug("[Packbench][add_product] %s-%s-%s", self.id, blue_id, combine.uuid)
    return true
end

--移除配方
function Packbench:remove_product(combine, combine_uid)
    self:del_products_elem(combine_uid)
    self.produce_queue:remove_by_index(combine.id)
    self.scene:broadcast_message("NID_PRODUCE_PRODUCT_NTF", { id = self.id, uuid = combine_uid })
end

--领取产出
function Packbench:recv_product(combine, combine_uid)
    self:remove_product(combine, combine_uid)
end

--同步配方
function Packbench:sync_combine(combine)
    local sync_datas = { id = self.id, uuid = combine.uuid, product = combine:pack2db() }
    self.scene:broadcast_message("NID_PRODUCE_PRODUCT_NTF", sync_datas)
end

function Packbench:fillin(combine, makings)
    for item_id, count in pairs(makings) do
        combine:fillin(item_id, count)
    end
    self:sync_combine(combine)
end

function Packbench:takeout(combine, item_id)
    combine:takeout(item_id)
    self:sync_combine(combine)
end

function Packbench:update()
end

-- 队列数量
function Packbench:get_queue_count()
    return 0,0
end

-- 解锁数据包
function Packbench:pack_unconfs()
    return {}
end

-- 储物箱
function Packbench:pack_storeboxs()
end

--序列化
function Packbench:pack2db()
    local dbdata = {}
    dbdata.products = {}
    for _, combine in self.produce_queue:iter() do
        dbdata.products[combine.uuid] = combine:pack2db()
    end
    return dbdata
end

-- 产品列表
function Packbench:pack_products()
    local products = {}
    for uuid, product in pairs(self.products) do
        products[uuid] = product:pack2db()
    end
    return products
end

--序列化
function Packbench:packclient()
    return {
        products = self:pack_products()
    }
end

return Packbench
