-- prod_raw.lua
-- 原料生产
local ProdeBase             = import("world/produce/prode_base.lua")
local ProdeUnQueueComponent = import("world/component/prode_unqueue_component.lua")

local ProdeRaw              = class(ProdeBase, ProdeUnQueueComponent)
function ProdeRaw:__init()
end

-- 加载db数据
function ProdeRaw:load_db(dbdata)
    ProdeBase.load_db(self, dbdata)
end

-- 初始化配方
function ProdeRaw:init_formulas()
    self.formulas = {}
    for _, formula_id in pairs(self.prototype.formulas or {}) do
        self.formulas[formula_id] = formula_id
    end
end

function ProdeRaw:on_setup()
    self:init_formulas()
end

function ProdeRaw:on_startup()
    self:update_effects()
    self:off_update()
    self:start_produce()
end

-- 属性变化事件
function ProdeRaw:on_effects_change()
    if self:get_product_count() == 0 or not self.is_startup then
        return
    end
    self:update_effects()
    for _, product in pairs(self.products) do
        local prod_time, done_time = self:formula_time(product.prototype.time)
        if product:up_effects(prod_time, done_time, product:get_tgt_count()) then
            self:sync_product(product)
        end
    end
end

-- 储物箱id
function ProdeRaw:storebox_id(product)
    return product:get_uuid()
end

-- 最大离线时间
function ProdeRaw:max_off_time(time)
    return time
end

--序列化
function ProdeRaw:pack2db()
    local dbdata = ProdeBase.pack2db(self)
    dbdata.storeboxs = self:get_storeboxs()
    return dbdata
end


return ProdeRaw
