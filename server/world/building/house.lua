--house.lua
local log_debug     = logger.debug

local Building  = import("world/building/building.lua")

local House     = class(Building)
local dbprop = db_property(House, "home_building")
dbprop:store_values("house", {}) --自定义字段

function House:__init(id, conf)

end

function House:on_load_data(data)
    log_debug("[House][on_load_data]")
    self.scene:add_human_limit(self.prototype.args)
    if data.house then
        self:set_house(data.house)
    end

end

function House:on_startup()
    self.scene:add_human_limit(self.prototype.args)
end

function House:on_destory()
    self.scene:add_human_limit(-self.prototype.args)
end

--房间号是否合法
function House:check_house_pos(pos)
    local max_num = self.prototype.args
    if pos <= 0 or pos > max_num then
        return false
    end
    return true
end

--自动获取房间号
function House:auto_house_pos()
    for i = 1, self.prototype.args, 1 do
        if self.partners[i] == nil then
            return i
        end
    end
    return nil
end

--获取房间号中的npc
function House:get_partner_id(house_pos)
    for pos, partner_id in pairs(self.partners) do
        log_debug("pos:{} partner_id:{}", pos, partner_id)
    end
    return self.partners[house_pos]
end

--检查房间号是否空位
function House:check_house_pos_empty(pos)
    if not self:check_house_pos(pos) then
        return false
    end
    return self.partners[pos] == nil
end

function House:packclient()
    local data = Building.packclient(self)
    data.house = self.house
    log_debug("[House][packclient] house:{}", data)
    return data
end

function House:packdb()
    local dbdata = Building.packdb(self)
    dbdata.house = self.house
    return dbdata
end

--修改名称
function House:change_name(name)
    self:save_house_field("house_name", name)
end

--入住民居
function House:check_in(partner, pos)
    partner:save_house_id(self.id)
    partner:save_house_pos(pos)
    self:save_partners_field(pos, partner:get_id())
    log_debug("[House][check_in] partner_id:{} check in house:{}-{}", partner:get_id(), self.id, pos)
end

--退出民居
function House:check_out(partner)
    log_debug("[House][check_out] partner_id:{} check out house:{}-{}", partner:get_id(), self.id, partner:get_house_pos())
    self:del_partners_field(partner:get_house_pos())
    partner:drive_away()
end

return House
