--building.lua
local BuffBox            = import("world/entity/buff_box.lua")
local mfloor             = math.floor

local log_debug          = logger.debug
local log_warn           = logger.warn
local log_err            = logger.err
local tsize              = qtable.size
local tcopy              = qtable.copy
local tdeep_copy         = qtable.deep_copy
local tinsert            = table.insert
local tdelete            = qtable.delete
local indexof            = qtable.indexof

local timer_mgr        = quanta.get("timer_mgr")
local object_fty       = quanta.get("object_fty")
local protobuf_mgr     = quanta.get("protobuf_mgr")
local event_mgr        = quanta.get("event_mgr")
local effect_mgr       = quanta.get("effect_mgr")
local sat_mgr          = quanta.get("sat_mgr")

local BUILDING_PLACE     = quanta.enum("BuildingOpt", "PLACE")
local SUCCESS            = quanta.enum("KernCode", "SUCCESS")

local BUILDING_FINISH    = protobuf_mgr:enum("building_status", "BUILDING_FINISH")
local BUILDING_WAITING   = protobuf_mgr:enum("building_status", "BUILDING_WAITING")
local BUILDING_PROCESS   = protobuf_mgr:enum("building_status", "BUILDING_PROCESS")
local BUILDING_REMOVE    = protobuf_mgr:enum("building_status", "BUILDING_REMOVE")
local BUILDING_RECAST    = protobuf_mgr:enum("building_status", "BUILDING_RECAST")
local BUT_UPGRADE        = protobuf_mgr:enum("build_upgrade_type", "BUT_UPGRADE")
local BUT_TOGGLE         = protobuf_mgr:enum("build_upgrade_type", "BUT_TOGGLE")
local BUT_REMOLD         = protobuf_mgr:enum("build_upgrade_type", "BUT_REMOLD")

local PT_PRODUCE         = protobuf_mgr:enum("product_type", "PT_PRODUCE")
local PT_COLLECT         = protobuf_mgr:enum("product_type", "PT_COLLECT")
local PT_RESEARCH        = protobuf_mgr:enum("product_type", "PT_RESEARCH")
local PT_BUSS            = protobuf_mgr:enum("product_type", "PT_BUSS")
local PT_FIGHT           = protobuf_mgr:enum("product_type", "PT_FIGHT")

local BUILDING_PARTNER   = protobuf_mgr:error_code("WORKS_BUILDING_PARTNER")
local BUILD_NOT_FINISH   = protobuf_mgr:error_code("WORKS_BUILD_NOT_FINISH")
local BUILD_CANT_UP      = protobuf_mgr:error_code("WORKS_BUILD_CANT_UP")
local BUILD_TOWN_LV      = protobuf_mgr:error_code("WORKS_BUILD_TOWN_LV")
local FORMULA_NOT_NORMAL = protobuf_mgr:error_code("WORKS_FORMULA_NOT_NORMAL")
local PARENT_NOT_UNLOCK  = protobuf_mgr:error_code("WORKS_BUILD_PARENT_NOT_UNLOCK")

local OBTAIN_BUILDING    = protobuf_mgr:enum("obtain_reason", "NID_OBTAIN_BUILDING")
local COST_BUILDING      = protobuf_mgr:enum("cost_reason", "NID_COST_BUILDING")

local Building           = class(nil, BuffBox)
local prop               = property(Building)
prop:reader("id", nil)
prop:reader("prototype", nil)
prop:reader("timer_id", nil)     --timer_id
prop:accessor("master", nil)     --master
prop:accessor("scene", nil)      --scene
prop:reader("partner_buffs", {}) --npc提供的buff
--buff影响属性
prop:accessor("effects", {})     --效果
prop:reader("effect_status", {}) --执行成功效果
prop:reader("prog_per", 0)       --完成进度百分比
prop:reader("time_per", 0)       --生产时间缩短百分比
prop:reader("partner_timer", {}) --伙伴加成时间
prop:reader("time_num", 0)       --生产时间缩短(秒)
prop:reader("cost_per", {})      --消耗物抵扣百分比(key-效果ID value-效果参数)
prop:reader("prod_per", {})      --产出物提升百分比(key-效果ID value-效果参数)

prop:reader("capacity_per", 0)   --产能提升百分比
prop:reader("partner_capacity", {}) --伙伴产能时间
prop:reader("capacity_num", 0)   --产能提升(个)
prop:reader("stage_time_per", {})     --生产时间满意度加成3阶段
prop:reader("stage_capacity_per", {}) --产能满意度加成3阶段


local dbprop = db_property(Building, "home_building")
dbprop:store_value("status", 0)     --status
dbprop:store_value("detail", "")    --detail
dbprop:store_value("proto_id", 0)   --建筑原型id
dbprop:store_value("timestamp", 0)  --timestamp
dbprop:store_values("partners", {}) --npc
dbprop:store_values("upnodes", {})  --升级节点
dbprop:store_value("upgrade_id", 0) --升级节点id

function Building:__init(id, conf)
    self.id = id
    self.prototype = conf
    self.proto_id = conf.id
end

-- 显示效果
function Building:print_effects()
    log_debug("[Building][print_effects] master({}) build_id({}) partner_buffs:{}", self.master, self.id,
        self.partner_buffs)
    log_debug("[Building][print_effects] master({}) build_id({}) effects:{}", self.master, self.id, self.effects)
    log_debug("[Building][print_effects] master({}) build_id({}) prog_per:{}", self.master, self.id, self.prog_per)
    log_debug("[Building][print_effects] master({}) build_id({}) time_per:{}", self.master, self.id, self.time_per)
    log_debug("[Building][print_effects] master({}) build_id({}) time_num:{}", self.master, self.id, self.time_num)
    log_debug("[Building][print_effects] master({}) build_id({}) cost_per:{}", self.master, self.id, self.cost_per)
    log_debug("[Building][print_effects] master({}) build_id({}) prod_per:{}", self.master, self.id, self.prod_per)
    log_debug("[Building][print_effects] master({}) build_id({}) capacity_per:{}", self.master, self.id,
        self.capacity_per)
    log_debug("[Building][print_effects] master({}) build_id({}) capacity_num:{}", self.master, self.id,
        self.capacity_num)
    -- log_debug("[Building][print_effects] master({}) build_id({}) all_buffs:{}", self.master, self.id, self.all_buffs)
    log_debug("[Building][print_effects] master({}) build_id({}) partner_buffs:{}", self.master, self.id,
        self.partner_buffs)
    log_debug("[Building][print_effects] master({}) build_id({}) cur_sat_lv:{}", self.master, self.id,
        self.scene:get_sat_lv())
end

function Building:load_building_db(master, data)
    log_debug("[Building][load_db] build_id({}) data({})", self.id, data)
    self.master = master
    self.name = data.name
    self.status = data.status
    self.detail = data.detail
    self.timestamp = data.timestamp
    self.partners = data.partners or {}
    self.upnodes = data.upnodes or {}
    self.upgrade_id = data.upgrade_id or 0
    self:update_timer()
    log_debug("[Building][load_db] self.master:{} building_id:{}", self.master, self.id)
end

function Building:on_load_data()

end

function Building:load_db(master, data)
    -- 初始化建筑db数据
    self:load_building_db(master, data)
    self:on_load_data(data)
end

--添加完成进度百分比
function Building:add_prog_per(prog_per)
    self.prog_per = self.prog_per + prog_per
end

--生产时间满意度3阶段
function Building:add_stage_time_per(stages, revert)
    for stage, value in pairs(stages or {}) do
        if revert then
            value = -value
        end
        self.stage_time_per[stage] = (self.stage_time_per[stage] or 0) + value
    end
end

--产能满意度3阶段
function Building:add_stage_capacity_per(stages, revert)
    for stage, value in pairs(stages or {}) do
        if revert then
            value = -value
        end
        self.stage_capacity_per[stage] = (self.stage_capacity_per[stage] or 0) + value
    end
end

--添加生产时间缩短百分比
function Building:add_time_per(time_per, partner_id)
    --伙伴带来的效果需要记录来源
    if partner_id then
        --添加
        if time_per > 0 then
            --是否添加过
            if not self.partner_timer[partner_id] then
                self.partner_timer[partner_id] = time_per
            else
                time_per = 0
            end
        else
            --扣除
            self.partner_timer[partner_id] = nil
        end
    end
    self.time_per = self.time_per + time_per
    log_debug("[Building][add_time_per] building_id:{} now time_per:{} add_time_per:{} partner_id:{}",
             self.id, self.time_per, time_per, partner_id)
end

--添加生产时间缩短(秒)
function Building:add_time_num(time_num)
    self.time_num = self.time_num + time_num
    log_debug("[Building][add_time_num] building_id:{} now time_num:{} add_time_num:{}", self.id, self.time_num, time_num)
end

--添加消耗物抵扣百分比
function Building:add_cost_per(effect_id, effect)
    local effects = self.cost_per[effect_id]
    if effects then
        effects.num =  effects.num + 1
    else
        self.cost_per[effect_id] = effect
    end
end

--减少消耗物抵扣效果
function Building:reduce_cost_per(effect_id)
    local effects = self.cost_per[effect_id]
    if not effects then
        return
    end
    effects.num = effects.num - 1
    if effects.num <= 0 then
        self.cost_per[effect_id] = nil
    end
end

--添加产出物增加百分比
function Building:add_prod_per(effect_id, effect)
    local effects = self.prod_per[effect_id]
    if effects then
        effects.num =  effects.num + 1
    else
        self.prod_per[effect_id] = effect
    end
end

--减少产出物增加效果
function Building:reduce_prod_per(effect_id)
    local effects = self.prod_per[effect_id]
    if not effects then
        return
    end
    effects.num = effects.num - 1
    if effects.num <= 0 then
        self.prod_per[effect_id] = nil
    end
end

--添加产能提升百分比
function Building:add_capacity_per(per, partner_id)
    --伙伴带来的效果需要记录来源
    if partner_id then
        if  per > 0 then
            if not self.partner_capacity[partner_id] then
                self.partner_capacity[partner_id] = per
            else
                per = 0
            end
        else
            self.partner_capacity[partner_id] = nil
        end
    end
    self.capacity_per = self.capacity_per + per
    log_debug("[Building][add_capacity_per] building_id:{} now capacity_per:{} add_capacity_per:{}", self.id, self.capacity_per, per)

end

--添加产能提升百分比
function Building:add_capacity_num(num)
    self.capacity_num = self.capacity_num + num
    log_debug("[Building][add_capacity_num] building_id:{} now capacity_num:{} add_capacity_num:{}", self.id, self.capacity_num, num)

end

function Building:get_type()
    return self.prototype.type
end

function Building:get_struct_type()
    return self.prototype.struct_type
end

function Building:get_tab()
    return self.prototype.tab
end

function Building:is_finish()
    return self.status == BUILDING_FINISH
end

function Building:is_worn()
    return self.prototype.worn
end

function Building:close()
    if self.timer_id then
        timer_mgr:unregister(self.timer_id)
    end
end

--处理定时器
function Building:update_timer()
    if self.status == BUILDING_REMOVE then
        local diff_time = self.timestamp - quanta.now
        local over_time = (diff_time > 0 and diff_time or 1) * 1000
        self.timer_id = timer_mgr:once(over_time, function()
            self:destory()
        end)
    end
end

function Building:startup(town, detail, status)
    self.scene = town
    self.detail = detail
    self.master = town:get_master()
    self.status = status or BUILDING_WAITING
    self:on_startup()
    log_debug("[Building][startup] self.master:{} building_id:{}", self.master, self.id)
end

-- 激活等级树
function Building:actived_upnode(conf)
    local ids = self:get_upnodes(conf.level)
    if not ids then
        ids = {}
    else
        for _, id in pairs(ids) do
            if id == conf.id then
                return
            end
        end
    end
    tinsert(ids, conf.id)
    self:save_upnodes_field(conf.level, ids)
end

function Building:finish(player)
    local is_upgrade = false
    local prototype = self.prototype
    local drop_town_exp = prototype.drop_town_exp or 0
    if self.status == BUILDING_RECAST then
        local upgrade_id = self.upgrade_id
        local conf = object_fty:find_utensil(upgrade_id)
        if not conf then
            log_err("[Building][finish] not conf player:{} build:{} proto_id:{} upgrade_id:{}", self.master, self.id, self.proto_id, upgrade_id)
            return BUILD_CANT_UP
        end
        self:save_proto_id(upgrade_id)
        self.prototype = conf
        self:save_upgrade_id(0)
        self:actived_upnode(conf)
        --掉落繁荣度计算
        local after_exp = conf.drop_town_exp or 0
        drop_town_exp = after_exp - drop_town_exp
        is_upgrade = true
    end
    self:save_timestamp(0)
    self:save_status(BUILDING_FINISH)
    self:on_finish(player, drop_town_exp)
    self:sync_changed()
    return SUCCESS, is_upgrade
end

-- 升级节点还原(临时方案,待接入建筑背包回收后,需要修改这部分代码)
function Building:upnode_restore()
    if self.prototype.level > 1 then
        local confs = object_fty:find_utensil_group(self.prototype.group)
        local conf_map = {}
        local ids = {}
        for _, item in pairs(confs) do
            conf_map[item.id] = item
            ids[item.level] = {}
        end
        local conf = self.prototype
        tinsert(ids[conf.level], conf.id)
        if conf.parent_id > 0 then
            for i = 0, #confs, 1 do
                conf = conf_map[conf.parent_id]
                if not conf then
                    break
                end
                if conf.level ~= 1 then
                    tinsert(ids[conf.level], conf.id)
                end
            end
        end
        for level, _ids in pairs(ids) do
            if #_ids > 0 then
                self:save_upnodes_field(level, _ids)
            end
        end
    end
end

function Building:fillin()
    self:upnode_restore()
    --填充完成
    self:save_timestamp(quanta.now + self.prototype.build_time)
    self:save_status(self.status == BUILDING_WAITING and BUILDING_PROCESS or BUILDING_RECAST)
    self:sync_changed()
end

-- 修复
function Building:repair(player, proto_id, detail)
    if detail and detail ~= "" then
        self:save_detail(detail)
    end
    self.prototype = object_fty:find_utensil(proto_id)
    self:save_proto_id(proto_id)
    self:sync_changed()
    self:on_upgrade(player)
end

function Building:upgrade(player, proto_id, detail)
    if detail and detail ~= "" then
        self:save_detail(detail)
    end
    self:save_upgrade_id(proto_id)
    self:save_timestamp(quanta.now + self.prototype.build_time)
    self:save_status(BUILDING_RECAST)
    self:sync_changed()
    self:on_upgrade(player)
end

function Building:on_upgrade(player)
end

--工作中
function Building:check_producing()
    if self.status == BUILDING_PROCESS or
        self.status == BUILDING_REMOVE or
        self.status == BUILDING_RECAST then
        return true
    end
    return false
end

--检查能否升级
function Building:check_upgrade(player, scene, conf)
    local produce = scene:get_produces(self.id)
    if produce and conf and not produce:check_upgrade(conf) then
        return FORMULA_NOT_NORMAL
    end

    if conf.level > 1 then
        -- 获取父节点
        local parent_conf = object_fty:find_parent_build(conf)
        -- 检测节点是否激活
        if not self:check_node_activate(parent_conf.level, parent_conf.id) then
            return PARENT_NOT_UNLOCK
        end
    end

    -- 城镇等级
    local town_level = player:get_scene_level()
    if self.prototype.up_town_lv and town_level < self.prototype.up_town_lv then
        return BUILD_TOWN_LV
    end
    -- 城镇状态验证
    if self.status ~= BUILDING_FINISH then
        return BUILD_CANT_UP
    end
    return SUCCESS
end

--检查是否完成
function Building:check_complete()
    if self.status == BUILDING_PROCESS or self.status == BUILDING_RECAST then
        return self.timestamp <= quanta.now
    end
    return false
end

-- 升级消耗
function Building:get_upcost(conf)
    if conf.level == 1 then
        return conf.toggles, BUT_TOGGLE
    end
    local upnodes = self:get_upnodes(conf.level) or {}
    -- 等级节点首次激活
    if #upnodes == 0 then
        return conf.costs, BUT_UPGRADE
        -- 节点已激活,切换费用
    elseif (indexof(upnodes, conf.id)) then
        return conf.toggles, BUT_TOGGLE
    end
    -- 改建
    return conf.remolds, BUT_REMOLD
end

-- 获取加速消耗
function Building:get_sp_costs()
    if not next(self.prototype.sp_costs) then
        return false
    end
    if not self.prototype.sp_unit or self.prototype.sp_unit <= 0 then
        return false
    end

    local costs = {}
    -- 剩余的时间
    local last_time = quanta.now - self.timestamp
    -- 消耗的加速单位
    local cost_unit = math.max(math.ceil(last_time / self.prototype.sp_unit), 1)
    for item_id, count in pairs(self.prototype.sp_costs or {}) do
        costs[item_id] = count * cost_unit
    end
    return true, costs
end

--获取关联组
function Building:get_group()
    return self.prototype.group
end

--获取个数上限
function Building:get_limit()
    return self.prototype.limit
end

function Building:place_costs()
    return { cost = { [self.prototype.item_id] = 1 }, reason = COST_BUILDING }
end

function Building:tackback_obtain()
    return { drop = self.prototype.obtains, reason = OBTAIN_BUILDING }
end

function Building:get_level()
    return self.prototype.level
end

--拆除扣除繁荣度
function Building:takeback_town_exp()
    return -self.prototype.drop_town_exp
end

--派驻位是否可用
function Building:is_valid_pos(pos)
    if pos <= 0 or pos > self.prototype.npc_num then
        return
    end
    local uuid = self.partners[pos]
    if uuid then
        return pos, uuid
    end
    return pos
end

--派驻
function Building:send_partner(pos, partner)
    self:save_partners_field(pos, partner:get_id())
    self:on_send_partner(partner)
end

--召回
function Building:recall_partner(partner)
    local pos = partner:get_building_pos()
    self:del_partners_field(pos)
    self:on_recall_partner(partner)
end

function Building:check_takeback()
    --是否有派驻人员
    if tsize(self.partners) > 0 then
        return BUILDING_PARTNER
    end

    if self.status ~= BUILDING_FINISH then
        return BUILD_NOT_FINISH
    end
    return SUCCESS
end

-- npc检测
function Building:check_partner()
    if tsize(self.partners) > 0 then
        return true
    end
    return false
end

-- 检查节点是否激活
function Building:check_node_activate(level, proto_id)
    if level == 1 then
        return true
    end
    local upnodes = self:get_upnodes(level) or {}
    for _, v in pairs(upnodes) do
        if v == proto_id then
            return true
        end
    end
    return false
end

-- npc数量
function Building:get_partner_count()
    return tsize(self.partners)
end


function Building:destory()
    self:on_destory()
    self.scene:del_buildings_elem(self.id)
    self:sync_destory()
end

function Building:update_detail(detail)
    self:save_detail(detail)
    self:sync_changed()
end

function Building:display_detail(scene, player, opid)
    scene:save_buildings_elem(self.id, self)
    self:sync_changed(opid)
    event_mgr:notify_trigger("on_building_stage", player, BUILDING_PLACE, self.id, self.prototype)
    return true
end

function Building:takeback_detail(scene, player)
    self.status = BUILDING_REMOVE
    self.timestamp = quanta.now + self.prototype.des_time
    self:update_timer()
    self:sync_changed()
    --扣除繁荣度
    local cost_town_exp = self:takeback_town_exp()
    if cost_town_exp then
        event_mgr:notify_trigger("on_town_exp_cost", player, cost_town_exp, COST_BUILDING)
    end
    --居民未安置状态
    if object_fty:is_house(self.prototype) then
        event_mgr:notify_trigger("on_house_takeback", player, self.id)
    end
    return true
end

function Building:sync_changed(opid)
    local datas = { elems = { [self.id] = self:pack2client() }, master = self.master, part_sync = true, opid = opid }
    self.scene:broadcast_message("NID_BUILDING_BUILDING_NTF", datas)
end

function Building:sync_destory()
    local datas = { elems = { [self.id] = { proto_id = 0 } }, master = self.master, part_sync = true }
    self.scene:broadcast_message("NID_BUILDING_BUILDING_NTF", datas)
end

function Building:pack_client_upnodes()
    local result = {}
    for level, proto_ids in pairs(self.upnodes) do
        tinsert(result, { level = level, proto_ids = proto_ids })
    end
    return result
end

function Building:packclient()
    local building = {
        name = self.name,
        detail = self.detail,
        status = self.status,
        proto_id = self.proto_id,
        timestamp = self.timestamp,
        upnodes = self:pack_client_upnodes(),
        upgrade_id = self.upgrade_id
    }
    return building
end

--序列化
function Building:pack2client()
    log_debug("[Building][pack2client] id({})", self.id)
    return self:packclient()
end

function Building:packdb()
    local building = {
        name = self.name,
        detail = self.detail,
        status = self.status,
        proto_id = self.proto_id,
        timestamp = self.timestamp,
        partners = self.partners,
        upnodes = self.upnodes,
        upgrade_id = self.upgrade_id
    }
    return building
end

--序列化
function Building:pack2db()
    log_debug("[Building][pack2client] id({})", self.id)
    return self:packdb()
end

-- 效果
function Building:pack_effects()
    local attrs = {
        prog_per = self.prog_per,
        time_per = self.time_per,
        time_num = self.time_num,
        cost_per = {},
        prod_per = {},
        capacity_per = self.capacity_per,
        capacity_num = self.capacity_num,
        stage_time_per = self.stage_time_per,
        stage_capacity_per = self.stage_capacity_per,
    }
    tdeep_copy(self.cost_per, attrs.cost_per)
    tdeep_copy(self.prod_per, attrs.prod_per)
    return attrs
end

--子类实现
----------------------------------------------------------
function Building:on_finish(player, drop_town_exp)
    local prototype = self.prototype
    if object_fty:is_recruit(prototype) then
        --通知建筑功能
        event_mgr:notify_trigger("on_building_recr_finish", self.master, prototype.level)
    end
    if object_fty:is_company(prototype) then
        event_mgr:notify_trigger("on_building_company_finish", self.master, prototype.args)
    end
    if object_fty:is_shop(prototype) then
        event_mgr:notify_trigger("on_shop_finish", self.master, self:get_level(), prototype.custom)
    end
    --通用建筑建造/升级完成
    event_mgr:notify_trigger("on_building_finish", self.master, self)

    --繁荣度奖励
    event_mgr:notify_trigger("on_town_exp_drop", player, drop_town_exp, OBTAIN_BUILDING)
    --建筑完成奖励
    local rewards = {}
    tcopy(prototype.rewards, rewards)
    --首次完成奖励
    if prototype.first_rewards and player:check_history(prototype.id) then
        tcopy(prototype.first_rewards, rewards)
        player:add_history(prototype.id)
    end
    if next(rewards) then
        local drops = { drop = rewards, reason = OBTAIN_BUILDING }
        player:execute_drop(drops)
    end
end

function Building:on_destory()
    local prototype = self.prototype
    if object_fty:is_recruit(prototype) then
        event_mgr:notify_trigger("on_building_recr_destory", self.master)
    end
    if object_fty:is_company(prototype) then
        event_mgr:notify_trigger("on_building_company_destory", self.master)
    end
    if object_fty:is_shop(prototype) then
        event_mgr:notify_trigger("on_shop_destory", self.master, prototype.custom)
    end
end

function Building:on_send_partner(partner)
    if object_fty:is_recruit(self.prototype) then
        event_mgr:notify_trigger("on_recr_send_partner", self.master, partner)
    end
    log_debug("[Building][on_send_partner] self.master:{}, partner_id:{}", self.master, partner:get_id())
    event_mgr:notify_trigger("on_send_partner", self.master, partner, self)
end

function Building:on_recall_partner(partner)
    if object_fty:is_recruit(self.prototype) then
        event_mgr:notify_trigger("on_recr_recall_partner", self.master, partner:get_id())
    end
    event_mgr:notify_trigger("on_recall_partner", self.master, partner, self)
end

--添加效果
function Building:add_effect(effect_id)
    log_debug("[Building][add_effect] building_id:{} effect_id:{}", self:get_id(), effect_id)
    tinsert(self.effects, effect_id)
end

--删除效果
function Building:remove_effect(effect_id)
    tdelete(self.effects, effect_id)
end

--执行效果
function Building:execute_effect()
    for _, effect_id in pairs(self.effects or {}) do
        effect_mgr:execute(self, effect_id, 1, self)
    end
end

--复原效果
function Building:revert_effect()
    for _, effect_id in pairs(self.effects or {}) do
        effect_mgr:revert(self, effect_id, 1, self)
    end
end

--添加buff
function Building:add_partner_buff(partner_id, buff_id)
    local ok, uuid = self:add_buff(buff_id)
    if not ok then
        log_warn("[Building][add_partner_buff] failed building_id:{} buff_id:{}", self.id, buff_id)
        return
    end
    local buffs = self.partner_buffs[partner_id] or {}
    if next(buffs) then
        tinsert(buffs, uuid)
    else
        buffs = { uuid }
    end
    self.partner_buffs[partner_id] = buffs
end

--删除buff
function Building:remove_partner_buff(partner_id)
    log_debug("[Building][remove_partner_buff] building_id:{} partner_id:{}", self.id, partner_id)
    local partner_buffs = self.partner_buffs[partner_id]
    if not partner_buffs then
        return
    end

    for _, buff_uuid in pairs(partner_buffs) do
        self:remove_buff(buff_uuid)
    end
    self.partner_buffs[partner_id] = nil
end

function Building:update(now)
    BuffBox.update_buff(self, now)
end


--加载伙伴属性
function Building:load_partner_attr(partner)
    local time_per, stage_time_per = self:calc_partner_time(partner)
    log_debug("[Building][load_partner_attr] building:{} partner({}) time_per:{}", self.id, partner:get_proto_id(),
        time_per)
    self:add_time_per(time_per, partner:get_id())
    --产能属性
    local capacity_per, stage_capacity_per = self:calc_partner_capacity(partner)
    if capacity_per > 0 then
        log_debug("[Building][load_partner_attr]building:{} partner({}) capacity_per:{}",self.id,  partner:get_proto_id(), capacity_per)
        self:add_capacity_per(capacity_per)
    end
    self:add_stage_time_per(stage_time_per)
    self:add_stage_capacity_per(stage_capacity_per)
end


--计算伙伴提供的产能增益
function Building:calc_partner_capacity(partner)
    --转换规则--采集属性按照每10点采集属性，提高最大产能总值的2%
    local conver_per, conver_point = 2, 10
    local product_type = self.prototype.product_type or 0
    if product_type ~= PT_COLLECT then
        return 0
    end
    local partner_attr, base_attr = partner:get_collect()
    local capacity_per = mfloor((partner_attr / conver_point)) * conver_per
    local sat_capacity_per = sat_mgr:get_attr_stage(base_attr, conver_point, conver_per)
    log_debug("[Building][calc_partner_capacity] building_id:{} partner_attr:{} capacity_per:{}", self.id, partner_attr, capacity_per)
    return capacity_per, sat_capacity_per
end

--计算伙伴提供的时间增益
function Building:calc_partner_time(partner)
    --转换规则--A属性按照10点转为2%计算
    local conver_per, conver_point = 2, 10
    local product_type = self.prototype.product_type or 0
    local partner_attr, base_attr
    if product_type == PT_PRODUCE then
        partner_attr, base_attr = partner:get_produce()
    elseif product_type == PT_COLLECT then
        partner_attr, base_attr = partner:get_collect()
    elseif product_type == PT_RESEARCH then
        partner_attr, base_attr = partner:get_research()
    elseif product_type == PT_BUSS then
        partner_attr, base_attr = partner:get_buss()
    elseif product_type == PT_FIGHT then
        partner_attr, base_attr = partner:get_fight()
    end
    local time_per = mfloor((partner_attr / conver_point)) * conver_per
    local sat_time_per = sat_mgr:get_attr_stage(base_attr, conver_point, conver_per)
    log_debug("[Building][calc_partner_time] building_id:{} partner_attr:{} timer_per:{}",self.id, partner_attr, time_per)
    return time_per, sat_time_per
end

--卸载伙伴属性
function Building:unload_partner_attr(partner)
    local time_per, stage_time_per = self:calc_partner_time(partner)
    log_debug("[Building][unload_partner_attr]building_id:{} partner({}) timer_per:{}",self.id,  partner:get_proto_id(), -time_per)
    self:add_time_per(-time_per, partner:get_id())

    local capacity_per, stage_capacity_per = self:calc_partner_capacity(partner)
    log_debug("[Building][unload_partner_attr] building_id:{} partner({}) capacity_per:{}", self.id, partner:get_proto_id(), -capacity_per)
    self:add_capacity_per(-capacity_per)

    self:add_stage_time_per(stage_time_per, true)
    self:add_stage_capacity_per(stage_capacity_per, true)
end

--子类实现
------------------------------------------------------------------------------
function Building:on_load_data(data)

end

function Building:on_startup()

end

return Building
