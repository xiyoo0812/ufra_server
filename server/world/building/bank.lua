--bank.lua
local new_guid      = codec.guid_new
local tinsert       = table.insert
local tsort         = table.sort

local config_mgr    = quanta.get("config_mgr")
local utility_db    = config_mgr:init_table("utility", "key")
local HISTORY_COUNT = utility_db:find_integer("value", "bank_history_count")

local Building  = import("world/building/building.lua")
local Bank      = class(Building)

local prop      = property(Bank)
prop:reader("history_sorts", {})

function Bank:__init(id, conf)
end

-- 加载数据
function Bank:on_load_data(data)
    self:on_startup()
    local historys = self.scene:get_historys()
    for id,history in pairs(historys or {}) do
        tinsert(self.history_sorts, history)
    end
    -- 日志按升序排列
    if self.history_sorts then
        tsort(self.history_sorts, function(a, b)
            return a.time < b.time
        end)
    end
end

function Bank:on_startup()
    self.scene:set_bank_id(self.id)
end

function Bank:on_destory()
    self.scene:set_bank_id(0)
end

-- 添加记录
function Bank:add_history(type, entity, proto_id, items)
    local time = quanta.now
    local historys = {}
    for item_id,item_count in pairs(items) do
        local id = new_guid()
        local history = {
            id = id,
            build_id = self.id,
            type = type,
            entity = entity,
            proto_id = proto_id,
            time = time,
            item_id = item_id,
            item_count = item_count
        }
        self.scene:save_historys_field(id, history)
        tinsert(self.history_sorts, history)
        tinsert(historys, history)
    end
    self:sync_history(historys)
    -- 超出删除
    local exceed_count = #self.history_sorts - HISTORY_COUNT
    if exceed_count > 0 then
        for i=1,exceed_count do
            local history = self.history_sorts[1]
            if history then
                self.scene:del_historys_field(history.id)
                self.history_sorts[i] = nil
            end
        end
    end
end

-- 同步记录
function Bank:sync_history(historys)
    self.scene:broadcast_message("NID_BUILDING_HISTORYS_NTF", {historys=historys})
end

function Bank:pack_historys2client()
    return self.history_sorts or {}
end
return Bank
