--init.lua
--world service includes
local config_mgr = quanta.get("config_mgr")

config_mgr:init_table("utility", "key")

config_mgr:init_merge_table("item", "items", "id")
config_mgr:init_merge_table("equip", "items", "id")
config_mgr:init_merge_table("utensil", "utensil", "id")
config_mgr:init_merge_table("building", "utensil", "id")

config_mgr:init_enum_table("attribute", "AttrID", "id")
config_mgr:init_enum_table("behavior", "Behavior", "id")
config_mgr:init_enum_table("condition", "Condition", "id")

import("common/constant.lua")
import("driver/lmdb.lua")
import("driver/sqlite.lua")
import("driver/unqlite.lua")
import("agent/cache_agent.lua")
import("agent/redis_agent.lua")
import("store/store_mgr.lua")
import("agent/online_agent.lua")
import("business/admin/shield.lua")
import("business/effect/effect_mgr.lua")

import("world/agent/mirror_mgr.lua")
import("world/agent/world_agent.lua")
import("world/entity/object_factory.lua")

import("world/entity/player_mgr.lua")
import("world/world/newbee_mgr.lua")
import("world/scene/scene_mgr.lua")
import("world/world/world_mgr.lua")
import("world/core/sat_mgr.lua")
import("world/core/item_mgr.lua")
import("world/recruit/recruit_mgr.lua")
import("world/world/ai_mgr.lua")
import("world/copy/copy_mgr.lua")

import("business/condition_mgr.lua")
import("world/task/action_mgr.lua")
import("world/task/task_mgr.lua")
import("world/ai/nav_mgr.lua")
import("world/ai/goap_mgr.lua")
import("world/ai/ai_debug_mgr.lua")

--gm import
import("world/gm/gm_mgr.lua")
import("world/gm/task_gm.lua")
import("world/gm/packet_gm.lua")
import("world/gm/player_gm.lua")
import("world/gm/battle_gm.lua")
import("world/gm/copy_gm.lua")

--service import
import("world/servlet/packet_servlet.lua")
import("world/servlet/world_servlet.lua")
import("world/servlet/scene_servlet.lua")
import("world/servlet/utility_servlet.lua")
import("world/servlet/building_servlet.lua")
import("world/servlet/produce_servlet.lua")
import("world/servlet/packbench_servlet.lua")
import("world/servlet/partner_servlet.lua")
import("world/servlet/skill_servlet.lua")
import("world/servlet/recruit_servlet.lua")
import("world/servlet/task_servlet.lua")
import("world/servlet/copy_servlet.lua")

--bgip import
import("world/bgip/bgip_mgr.lua")
import("world/bgip/bgip_role.lua")
import("world/bgip/bgip_query.lua")
