--copy_mgr.lua
local log_debug         = logger.info
local log_err           = logger.err
local tunpack           = table.unpack

local scene_mgr         = quanta.get("scene_mgr")
local update_mgr        = quanta.get("update_mgr")
local event_mgr         = quanta.get("event_mgr")
local config_mgr        = quanta.get("config_mgr")
local newbee_mgr        = quanta.get("newbee_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")

local FRAME_SUCCESS     = protobuf_mgr:error_code("FRAME_SUCCESS")
local FRAME_FAILED      = protobuf_mgr:error_code("FRAME_FAILED")
local COPY_ALREADY_IN   = protobuf_mgr:error_code("COPY_ALREADY_IN")
local COPY_NOT_IN       = protobuf_mgr:error_code("COPY_NOT_IN")
local COPY_NOT_FOUND    = protobuf_mgr:error_code("COPY_NOT_FOUND")
local COPY_SCENE_ERROR  = protobuf_mgr:error_code("COPY_SCENE_ERROR")
local LEVEL_NOT_FOUND   = protobuf_mgr:error_code("COPY_NOT_FOUND_LEVEL")
local LEVEL_NO_NEXT     = protobuf_mgr:error_code("COPY_NO_NEXT_LEVEL")
local LEVEL_ERROR       = protobuf_mgr:error_code("COPY_LEVEL_ERROR")
local LEVEL_NO_CHANGE   = protobuf_mgr:error_code("COPY_LEVEL_NO_CHANGE")

local CLCT_ENTER        = protobuf_mgr:enum("copy_level_change_type", "ENTER")
local CLCT_ACHIEVE      = protobuf_mgr:enum("copy_level_change_type", "ACHIEVE")

local copy_db           = config_mgr:init_table("copy", "id")
local copy_levels_db    = config_mgr:init_table("copy_levels", "id")

-- 副本管理器
local CopyMgr = singleton()

local prop = property(CopyMgr)
prop:accessor("startup_loaded", false)

function CopyMgr:__init()
    -- 定时器
    update_mgr:attach_minute(self)
    -- 事件监听
    event_mgr:add_trigger(self, "on_copy_data_ready")
    event_mgr:add_trigger(self, "on_copy_level_complete")
    event_mgr:add_trigger(self, "on_plot_level_timeout")
    event_mgr:add_trigger(self, "on_copy_finish_timeout")
    event_mgr:add_trigger(self, "on_copy_online_check")
    -- 配置添加分组
    copy_levels_db:add_group("copy_id")
end

function CopyMgr:on_minute(clock_ms)
    -- 清理过期副本
    local now_ms = quanta.now_ms
    for scene_id, scene in pairs(scene_mgr:get_scenes()) do
        if scene:is_copy() and scene:is_data_loaded() then
            local expire_ms = scene:get_expire_ms()
            local expire_left_ms = expire_ms - now_ms
            -- 清理数据
            if expire_left_ms <= 0 then
                self:on_copy_expired(scene)
            end
        end
    end
end

function CopyMgr:on_enter_copy_falied(player_id, player, copy_id)
    -- 清理无效副本
    self:copy_data_clean(player, copy_id, nil)
    -- 传送回家园
    self:back_to_home(player_id, player)
end

function CopyMgr:back_to_home(player_id, player)
    newbee_mgr:init_position(player, {})
    local town = player:get_town()
    scene_mgr:goto_scene(player, town:get_id(), player_id)
end

function CopyMgr:back_from_copy(player_id, player, copy_config)
    if not copy_config then
        self:back_to_home(player_id, player)
        return
    end
    local dir_y = 0
    local tx, ty, tz = tunpack(copy_config.exit_pos)
    local exit_map_id = copy_config.exit_map
    if copy_config.exit_dir then
        dir_y = copy_config.exit_dir[2]
    end
    scene_mgr:trans_scene(player, exit_map_id, tx, ty, tz, dir_y)
end

function CopyMgr:copy_scene_clean(copy_scene, player_id)
    if player_id then
        copy_scene:unload_followers(player_id)
    end
    local id = copy_scene:get_id()
    local map_id = copy_scene:get_map_id()
    if id and map_id then
        scene_mgr:close_scene(copy_scene)
    end
end

function CopyMgr:copy_data_clean(player, copy_id, copy)
    player:exit_copy(copy_id)
    if copy then
        copy:destroy_data()
    end
end

function CopyMgr:on_enter_copy_expired(copy_id, copy, copy_config, player_id, player)
    -- 清理过期副本
    self:copy_data_clean(player, copy_id, copy)
    -- 传送到副本出口场景点
    self:back_from_copy(player_id, player, copy_config)
end

function CopyMgr:on_exit_level(player_id, player, copy, level)
    if not level:is_plot_level() then
        -- TODO: 多人副本时，判断是否还有玩家存在
        copy:unload_level_monsters(level)
    else
        -- 剧情关卡倒计时计时器清理
        player:countdown_stop()
    end
end

function CopyMgr:on_enter_level(player_id, player, copy, level)
    local copy_id = copy:get_copy_id()
    local config_id = copy:get_config_id()
    local copy_config = copy:get_copy_config()
    local level_config = level:get_level_config()
    local current_copy_id = player:get_current_copy_id()
    local current_copy_level = player:get_current_copy_level()
    if current_copy_id == copy_id and current_copy_level == level_config.level then
        -- 重入判定
        return
    end
    local is_temporary = copy:is_temporary_copy(copy_config)
    player:enter_copy(config_id, copy_id, level:get_id(), copy:get_map_id(), is_temporary, copy:find_config_followers())
    local current_scene = player:get_scene()
    if not current_scene or current_scene:get_id() ~= copy_id then
        -- Player 传送到副本场景
        local pos
        local dir
        local is_new_copy = player:is_current_copy_new()
        if is_new_copy then
            pos = copy_config.enter_pos
            dir = copy_config.enter_dir
        else
            -- pos = level_config.relive_pos -- 关卡复活点
            pos = { player:get_pos_x(), player:get_pos_y(), player:get_pos_z() } -- 玩家位置
            -- dir = level_config.relive_dir -- 关卡复活朝向
            dir = { 0, player:get_dir_y(), 0 } -- 玩家朝向
        end
        scene_mgr:trans_scene(player, copy_config.map_id, pos[1], pos[2], pos[3], dir[2])
        event_mgr:fire_second(function()
            -- 随从 NPC 添加到副本场景
            copy:load_followers(player, player_id)
        end)
    end
    -- 刷怪
    if not level:is_plot_level() then
        copy:load_level_monsters(level)
        -- 非剧本关卡依赖杀怪事件触发
        if (level:is_normal_level() or level:is_boss_level()) and not next(level:get_monsters()) then
            -- 没有怪物, 直接完成
            self:on_copy_level_complete(player, copy, level:get_id())
            return
        end
        if level:is_boss_level() then
            -- 传送到 BOSS 关卡
            event_mgr:fire_frame(function()
                local dir = level_config.relive_dir
                local pos = level_config.relive_pos
                if dir and pos then
                    copy:trans_entity(player, pos[1], pos[2], pos[3], dir[2])
                end
            end)
        end
    else
        -- 剧情关卡倒计时开始
        local level_duration = level_config.duration or 0
        if level_duration > 0 then
            player:countdown_start(level_duration * 1000, true)
        end
    end
end

function CopyMgr:on_copy_data_ready(copy)
    local copy_id = copy:get_copy_id()
    for player_id, player in pairs(copy:get_players()) do
        local copy_level = player:get_copy_level(copy_id)
        local copy_level_obj = copy:find_level(copy_level)
        if copy_level_obj then
            self:on_enter_level(player_id, player, copy, copy_level_obj)
        end
    end
end

function CopyMgr:on_copy_failed_notify(player, task_id)
    if not task_id then
        return
    end
    -- 注: 副本失败时, 通知任务系统重置任务
    player:fire_lobby_reliable("on_task_reset", task_id)
end

function CopyMgr:on_copy_expired(copy)
    local copy_id = copy:get_copy_id()
    local copy_config = copy:get_copy_config()
    local map_id = copy:get_map_id()
    for player_id, player in pairs(copy:get_players()) do
        self:on_copy_failed_notify(player, copy_config.task_id)
        local copy_level = player:get_copy_level(copy_id)
        local copy_level_obj = copy:find_level(copy_level)
        if copy_level_obj then
            -- on_exit_level: 普通关卡怪物销毁、剧情关卡(倒计时)计时器清理
            self:on_exit_level(player_id, player, copy, copy_level_obj)
            -- 随从 NPC 清理
            copy:unload_followers(player_id)
            -- Player 传送到副本出口场景点
            self:back_from_copy(player_id, player, copy_config)
        end
    end
    -- 场景实例销毁
    local scene_id = copy:get_id()
    if scene_id and map_id then
        scene_mgr:close_scene(copy)
    end
    -- 副本数据销毁
    copy:destroy_data()
end

--- (非剧情)关卡完成
function CopyMgr:on_copy_level_complete(player, copy, level_id)
    local copy_id = copy:get_copy_id()
    local copy_config_id = copy:get_config_id()
    local entity_id = player:get_id()
    event_mgr:fire_frame(function()
        -- 注: 副本关卡完成, 任务系统定义事件名为 on_dungeon_finish
        player:notify_event("on_dungeon_finish", entity_id, copy_config_id * 1000 + level_id)
        player:send("NID_COPY_LEVEL_CHANGE_NTF", {
            entity_id = entity_id,
            copy_id = copy_id,
            level_id = level_id,
            type = CLCT_ACHIEVE,
        })
    end)
    local result = self:forward_copy_level(player, entity_id)
    if result == LEVEL_NO_NEXT then
        self:finish_copy_normal(player, entity_id, copy)
    end
end

function CopyMgr:on_plot_level_timeout(player)
    local player_id = player:get_id()
    local result = self:forward_copy_level(player, player_id)
    if result == LEVEL_NO_NEXT then
        player:set_current_copy_finish(true)
        self:exit_copy(player, player_id)
    end
end

function CopyMgr:on_copy_finish_timeout(player)
    local player_id = player:get_id()
    self:exit_copy(player, player_id)
end

-- 重连上线处理
function CopyMgr:on_copy_online_check(player, copy_id, copy_level)
    local scene = player:get_scene()
    if scene and scene:get_id() == copy_id then
        local level = scene:find_level(copy_level)
        if level and level:is_plot_level() then
            local level_config = level:get_level_config()
            local level_duration = level_config.duration or 0
            if level_duration > 0 then
                player:countdown_start(level_duration * 1000, false)
            end
        end
    end
end

function CopyMgr:load_copies()
    if self.startup_loaded then
        return
    end
    -- TODO: 加载副本(仅限多人副本,单人副本和临时副本不需要提前加载)
    self.startup_loaded = true
end

function CopyMgr:get_copy_object(player_id, copy_config, copy_id)
    local copy_scene = self:get_copy_scene(player_id, copy_config.map_id)
    if not copy_scene then
        log_err("[CopyMgr][get_copy_via_config] copy_scene not exist!")
        return nil
    end
    if not copy_scene:apply_config(copy_config, player_id, copy_id or copy_scene:get_copy_id()) then
        self:copy_scene_clean(copy_scene, player_id)
        return nil
    end
    return copy_scene
end

-- 获取副本场景
function CopyMgr:get_copy_scene(player_id, map_id)
    local map_conf = scene_mgr:find_conf(map_id)
    if not map_conf then
        return
    end
    local copy_scene = scene_mgr:load_scene(map_conf, player_id)
    if not copy_scene then
        copy_scene = scene_mgr:create_copy(player_id, map_id)
    end
    if not copy_scene then
        log_err("[CopyMgr][get_copy_scene] create copy scene failed! mpa_id={}", map_id)
    end
    return copy_scene
end

function CopyMgr:get_copy_config(config_id)
    local conf = copy_db:find_one(config_id)
    if not conf then
        log_err("[CopyMgr][get_copy_config] conf {} not exist!", config_id)
        return nil
    end
    return conf
end

function CopyMgr:enter_copy(player, player_id, config_id, copy_id)
    local current_scene = player:get_scene() or player:get_town()
    if not current_scene then
        return COPY_SCENE_ERROR
    end
    if current_scene:is_copy() then
        return COPY_ALREADY_IN
    end
    local conf = self:get_copy_config(config_id)
    if not conf then
        return COPY_NOT_FOUND
    end
    local copy_scene = self:get_copy_object(player_id, conf, copy_id)
    if not copy_scene then
        return COPY_SCENE_ERROR
    end
    if copy_scene:is_data_loaded() then
        local copy_level = player:get_copy_level(copy_scene:get_copy_id())
        local copy_level_obj = copy_scene:find_level(copy_level)
        if not copy_level_obj then
            return LEVEL_NOT_FOUND
        end
        self:on_enter_level(player_id, player, copy_scene, copy_level_obj)
    end
    return FRAME_SUCCESS
end

function CopyMgr:exit_copy(player, player_id)
    local current_scene = player:get_scene()
    if not current_scene then
        self:copy_data_clean(player, nil, nil)
        self:back_from_copy(player_id, player, nil)
        return COPY_SCENE_ERROR
    end
    if not current_scene:is_copy() then
        return COPY_NOT_IN
    end
    current_scene:reset_config_followers()
    local copy_scene = current_scene
    local conf = current_scene:get_copy_config()
    local copy_id = copy_scene:get_copy_id()
    player:exit_copy(copy_id)
    -- 退出副本
    self:back_from_copy(player_id, player, conf)
    event_mgr:fire_frame(function()
        -- 一次性场景，清理数据
        if not copy_scene:is_period_copy(conf) then
            -- 非周期性场景, 销毁场景实例
            self:copy_scene_clean(copy_scene, player_id)
        end
        if copy_scene:is_once_copy(conf) then
            self:copy_data_clean(player, copy_id, copy_scene)
        end
    end)
    return FRAME_SUCCESS
end

function CopyMgr:reenter_copy(player, player_id, config_id, copy_id)
    local copy_config = self:get_copy_config(config_id)
    if not copy_config then
        self:on_enter_copy_falied(player_id, player, copy_id)
        return
    end
    local copy_scene = self:get_copy_object(player_id, copy_config, copy_id)
    if not copy_scene then
        self:on_copy_failed_notify(player, copy_config.task_id)
        self:on_enter_copy_falied(player_id, player, copy_id)
        return
    end
    local now_ms = quanta.now_ms
    local expire_ms = copy_scene:get_expire_ms()
    if now_ms > expire_ms then
        self:on_copy_failed_notify(player, copy_config.task_id)
        self:on_enter_copy_expired(copy_id, copy_scene, copy_config, player_id, player)
        return
    end
    if copy_scene:is_data_loaded() then
        local copy_level = player:get_copy_level(copy_id)
        local copy_level_obj = copy_scene:find_level(copy_level)
        if copy_level_obj then
            self:on_enter_level(player_id, player, copy_scene, copy_level_obj)
            player:send("NID_COPY_LEVEL_CHANGE_NTF", {
                entity_id = player_id,
                copy_id = copy_id,
                level_id = copy_level,
                type = CLCT_ENTER,
            })
        else
            self:on_copy_failed_notify(player, copy_config.task_id)
            self:on_enter_copy_falied(player_id, player, copy_id)
        end
    end
end

function CopyMgr:forward_copy_level(player, player_id)
    -- 推进关卡
    local current_scene = player:get_scene()
    if not current_scene then
        return COPY_SCENE_ERROR
    end
    if not current_scene:is_copy() then
        return COPY_NOT_IN
    end
    local copy_scene = current_scene
    local copy_id = copy_scene:get_copy_id()
    local copy_level = player:get_copy_level(copy_id)
    local next_level = copy_level + 1
    local result = self:switch_level(player, player_id, next_level)
    if result == LEVEL_NOT_FOUND then
        -- 没有下一关
        self:finish_copy_normal(player, player_id, copy_scene)
        result = LEVEL_NO_NEXT
    end
    return result
end

function CopyMgr:switch_level(player, player_id, level_id)
    -- 切换(进入)关卡
    local current_scene = player:get_scene()
    if not current_scene then
        return COPY_SCENE_ERROR
    end
    if not current_scene:is_copy() then
        return COPY_NOT_IN
    end
    local copy_scene = current_scene
    local copy_id = copy_scene:get_copy_id()
    -- 当前关卡
    local copy_level = player:get_copy_level(copy_id)
    -- 要切换到的关卡
    -- 当前关卡对象
    local copy_level_obj = copy_scene:find_level(copy_level)
    if not copy_level_obj then
        return LEVEL_ERROR
    end
    if copy_level == level_id then
        return LEVEL_NO_CHANGE
    end
    -- 要切换到的关卡对象
    local target_level_obj = copy_scene:find_level(level_id)
    if not target_level_obj then
        log_debug('[CopyMgr][switch_level] no next level')
        return LEVEL_NOT_FOUND
    end
    self:on_exit_level(player_id, player, copy_scene, copy_level_obj)
    self:on_enter_level(player_id, player, copy_scene, target_level_obj)
    log_debug('[CopyMgr][switch_level] copy_level: [{}->{}]', copy_level, level_id)
    if not player:update_copy_level(copy_id, level_id) then
        return FRAME_FAILED
    end
    return FRAME_SUCCESS
end

---剧情关卡完成
function CopyMgr:plot_level_achieve(player, player_id, level_id)
    local current_scene = player:get_scene()
    if not current_scene then
        return COPY_SCENE_ERROR
    end
    if not current_scene:is_copy() then
        return COPY_NOT_IN
    end
    local copy_scene = current_scene
    local copy_id = copy_scene:get_copy_id()
    local copy_level = player:get_copy_level(copy_id)
    local copy_level_obj = copy_scene:find_level(copy_level)
    if not copy_level_obj then
        return LEVEL_ERROR
    end
    if not copy_level_obj:is_plot_level() then
        return LEVEL_ERROR
    end
    if copy_level ~= level_id then
        return LEVEL_ERROR
    end
    -- 通知 Lobby 剧情关卡完成
    local copy_config_id = copy_scene:get_config_id()
    event_mgr:fire_frame(function()
        -- 注: 副本关卡完成, 任务系统定义事件名为 on_dungeon_finish
        player:notify_event("on_dungeon_finish", player_id, copy_config_id * 1000 + level_id)
        player:send("NID_COPY_LEVEL_CHANGE_NTF", {
            entity_id = player_id,
            copy_id = copy_id,
            level_id = level_id,
            type = CLCT_ACHIEVE,
        })
    end)
    -- 推进到下一关
    local result = self:switch_level(player, player_id, copy_level + 1)
    if result == LEVEL_NOT_FOUND then
        -- 没有下一关
        self:finish_copy_normal(player, player_id, copy_scene)
        result = LEVEL_NO_NEXT
    end
    return result
end

-- 副本正常结束
function CopyMgr:finish_copy_normal(player, player_id, scene)
    if player:is_current_copy_finish() then
        -- 防止 GM 指令导致倒计时重设
        return
    end
    player:set_current_copy_finish(true)
    local copy_config = scene:get_copy_config()
    local exit_timeout = copy_config.exit_timeout or 0
    log_debug('[CopyMgr][finish_copy_normal] player_id: {}, copy_id: {}, exit_timeout: {}s', player_id, scene:get_config_id(), exit_timeout)
    if exit_timeout > 0 then
        player:countdown_start(exit_timeout * 1000, true)
    else
        self:exit_copy(player, player_id)
    end
end

-- export
quanta.copy_mgr = CopyMgr()

return CopyMgr
