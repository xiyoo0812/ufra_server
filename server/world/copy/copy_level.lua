--copy_level.lua
local log_err           = logger.err

local config_mgr        = quanta.get("config_mgr")

local copy_levels_db    = config_mgr:init_table("copy_levels", "id")

local CLT_PLOT          = quanta.enum("CopyLevelType", "PLOT")
local CLT_NORMAL        = quanta.enum("CopyLevelType", "NORMAL")
local CLT_BOSS          = quanta.enum("CopyLevelType", "BOSS")

-- 副本关卡
local CopyLevel = class()

local prop = property(CopyLevel)
prop:accessor("copy_id", 0)           -- 所属副本id
prop:accessor("level_config", nil)    -- 关卡配置

local dbprop = db_property(CopyLevel, "copy")
dbprop:store_value("id", 0)         -- id
dbprop:store_values("monsters", {}) -- 关卡中存活的怪物(刷怪配置id)

function CopyLevel:__init(copy_id)
    self.copy_id = copy_id
end

function CopyLevel:load_db_data(data, copy_config_id)
    if self.level_config then
        return true
    end
    -- 内存属性
    self:set_level_config(self:get_level_from_config(data.id, copy_config_id))
    -- DB属性
    self:set_id(data.id)
    self:set_monsters(data.monsters)
end

function CopyLevel:init_config_data(config, monsters)
    if self.level_config then
        return true
    end
    self.level_config = config
    self:set_id(config.level)
    for sow_id, proto_id in pairs(monsters) do
        self:set_monsters_field(sow_id, proto_id)
    end
end

function CopyLevel:remove_monster(sow_entity_id)
    self:del_monsters_field(sow_entity_id)
end

function CopyLevel:pack2db()
    return {
        id = self:get_id(),
        monsters = self:get_monsters()
    }
end

function CopyLevel:get_level_from_config(level_id, copy_config_id)
    local level_config_id = copy_config_id * 1000 + level_id
    local conf = copy_levels_db:find_one(level_config_id)
    if not conf then
        log_err("[CopyLevel][get_level_from_config] (copy_config_id={}, level_id={}) level not exist!", copy_config_id, level_id)
    end
    return conf
end

function CopyLevel:get_level_type(level_config)
    return (self.level_config or level_config or {}).type
end

function CopyLevel:is_plot_level(level_config)
    return self:get_level_type(level_config) == CLT_PLOT
end

function CopyLevel:is_normal_level(level_config)
    return self:get_level_type(level_config) == CLT_NORMAL
end

function CopyLevel:is_boss_level(level_config)
    return self:get_level_type(level_config) == CLT_BOSS
end

return CopyLevel
