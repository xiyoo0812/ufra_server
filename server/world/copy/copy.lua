--copy.lua
local log_err           = logger.err
local tinsert           = table.insert
local tsort             = table.sort

local store_mgr         = quanta.get("store_mgr")
local config_mgr        = quanta.get("config_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")
local event_mgr         = quanta.get("event_mgr")

local copy_followers_db = config_mgr:init_table("copy_followers", "id")
local copy_levels_db    = config_mgr:get_table("copy_levels", "id")
local sow_area_db       = config_mgr:init_table("sow_area", "map_id", "id")
local sow_area_edb      = config_mgr:init_table("sow_area_entity", "map_id", "id")

local ENT_NPC           = protobuf_mgr:enum("entity_type", "ENTITY_NPC")
local CT_TEMPORARY      = quanta.enum("CopyType", "TEMPORARY")
local CT_ONCE           = quanta.enum("CopyType", "ONCE")
local CT_PERIOD         = quanta.enum("CopyType", "PERIOD")
local F_FOLLOWER        = quanta.enum("Faction", "FOLLOWER")

local Scene             = import("world/scene/scene.lua")

-- 副本
local Copy = class(Scene)

local prop = property(Copy)
prop:accessor("copy_config", nil) -- 副本配置
prop:reader("level_monster_areas", {}) -- 副本关卡刷怪区配置
prop:reader("config_followers", {}) -- 副本的随从配置

local edbprop = db_property(Copy, "copy", true)
edbprop:store_value("copy_id", nil)      -- 副本id
edbprop:store_value("master", nil)       -- 副本创建者Id(多人副本为第一个进入的玩家)
edbprop:store_value("config_id", nil)    -- 副本配置id
edbprop:store_value("create_ms", nil)    -- 创建时间(ms)
edbprop:store_value("expire_ms", nil)    -- 开始时间(ms)
edbprop:store_objects("levels", {})      -- 关卡

function Copy:__init(id, scene_conf)
    self.levels = {}
end

function Copy:load_sow_areas(aoi_radius)
    -- 副本不加载撒点刷怪区域，但是加载关卡刷怪点
    for _, area_conf in sow_area_db:iterator() do
        if area_conf.map_id == self.map_id then
            local level_monster_area = {}
            for _, entity_conf in sow_area_edb:iterator() do
                if entity_conf.map_id == area_conf.map_id and entity_conf.area_id == area_conf.id then
                    tinsert(level_monster_area, entity_conf)
                end
            end
            self.level_monster_areas[area_conf.id] = level_monster_area
        end
    end
end

function Copy:close()
    for eid, entity in pairs(self.entitys) do
        self:destory_entity(eid)
    end
    Scene:close(self)
end

function Copy:on_player_leave(player, player_id)
    self:unload_followers(player_id)
end

function Copy:is_data_loaded()
    if not self.copy_config then
        return false
    end
    if self:is_temporary_copy() then
        return true
    end
    return self:is_copy_loaded()
end

function Copy:apply_config(copy_config, player_id, copy_id)
    if self.copy_config then
        return true
    end
    self.copy_config = copy_config
    if not copy_id then
        -- 新副本
        copy_id = self:get_id() -- 使用场景id作为副本id
        self:set_copy_id(copy_id)
        self:set_master(player_id)
        self:set_config_id(self.copy_config.id)
        self:load_levels(nil)
        if self:is_temporary_copy(copy_config) then
            -- 临时副本
            return true
        end
        local now_ms = quanta.now_ms
        self:set_create_ms(now_ms)
        self:set_expire_ms(now_ms + self.copy_config.duration * 1000)
    else
        -- 已存在副本
        self:set_copy_id(copy_id)
    end
    -- 持久化副本
    if not store_mgr:load(self, copy_id, "copy") then
        return false
    end
    return true
end

function Copy:destroy_data()
    local copy_id = self:get_copy_id()
    if not copy_id then
        return
    end
    store_mgr:delete(copy_id, "copy")
end

function Copy:load_config(copy_id)
    if self.copy_config then
        return true
    end
    if not store_mgr:load(self, copy_id, "copy") then
        return false
    end
    return true
end

function Copy:load_levels(data_levels)
    local CopyLevel = import("world/copy/copy_level.lua")
    if not data_levels then
        -- 新副本加载关卡配置
        local configs = copy_levels_db:find_group(self.config_id)
        tsort(configs, function(conf1, con2)
            return conf1.level < con2.level
        end)
        for _, config in ipairs(configs) do
            local level = CopyLevel(self.copy_id)
            local monsters = {}
            for _, sow_area_id in ipairs(config.monster_areas or {}) do
                for _, sow_area_entity in pairs(self.level_monster_areas[sow_area_id] or {}) do
                    monsters[sow_area_entity.id] = sow_area_entity.proto_id
                end
            end
            level:init_config_data(config, monsters)
            self:set_levels_elem(level:get_id(), level)
        end
        return
    end
    -- 已存在副本加载关卡数据
    for _, data in pairs(data_levels) do
        local level = CopyLevel(self.copy_id)
        level:load_db_data(data, self.config_id)
        self:set_levels_elem(level:get_id(), level)
    end
end

function Copy:find_level(level_id)
    if level_id ~= nil then
        return self:get_levels(level_id)
    end
end

function Copy:load_level_monsters(level)
    for sow_entity_id, proto_id in pairs(level:get_monsters()) do
        local monster_conf = sow_area_edb:find_one(self.map_id, sow_entity_id)
        if monster_conf then
            self:create_entity(monster_conf)
        end
    end
end

function Copy:unload_level_monsters(level)
    for sow_entity_id, proto_id in pairs(level:get_monsters()) do
        for entity_id, entity in pairs(self.entitys) do
            if entity:is_monster() then
                local sow_conf = entity:get_sow_conf()
                if sow_conf and sow_conf.id == sow_entity_id then
                    self:destory_entity(entity_id)
                end
            end
        end
    end
end


function Copy:remove_level_monster(player, level_id, sow_entity_id)
    local copy_level_obj = self:find_level(level_id)
    if copy_level_obj then
        copy_level_obj:remove_monster(sow_entity_id)
        -- 是否需要判定(关卡完成)
        local need_judge = copy_level_obj:is_normal_level() or copy_level_obj:is_boss_level()
        if need_judge and not next(copy_level_obj:get_monsters()) then
            -- 关卡怪物杀完, 关卡完成
            event_mgr:notify_trigger("on_copy_level_complete", player, self, level_id)
        end
    end
end

--是否可复活
function Copy:can_relive(entity)
    if entity:is_monster() then
        return false
    end
    return true
end

function Copy:on_db_copy_load(data)
    if data.copy_id then
        -- 已存在的副本
        self:set_config_id(data.config_id)
        self:set_master(data.master)
        self:set_create_ms(data.create_ms)
        self:set_expire_ms(data.expire_ms)
        self:load_levels(data.levels)
        event_mgr:fire_second("on_copy_data_ready", self)
        return true
    end
    -- 新副本数据，存库
    self:flush_copy_db()
    return false
end

function Copy:find_config_followers()
    if next(self.config_followers) then
        return self.config_followers
    end
    local followers = {}
    for key, conf in copy_followers_db:iterator() do
        if conf.copy_id ~= self:get_config_id() then
            goto continue
        end
        -- 注意: 此处需要用新的 table 解除引用, 以防止污染配置表内存
        followers[conf.proto_id] = {
            refresh_time = 0,
            type = ENT_NPC,
            name = conf.name,
            dynamic = true,
            id = conf.id,
            proto_id = conf.proto_id,
            pos = { conf.pos[1], conf.pos[2], conf.pos[3], },
            dir = { conf.dir[1], conf.dir[2], conf.dir[3], },
            faction = F_FOLLOWER,
        }
        ::continue::
    end
    self.config_followers = followers
    return followers
end

function Copy:load_followers(player, player_id)
    local config_followers = self:find_config_followers()
    for proto_id, info in pairs(player:get_copy_followers() or {}) do
        local conf = config_followers[proto_id]
        if conf then
            conf.pos = { info.x, info.y, info.z, }
            conf.dir = { 0, info.d, 0, }
            local entity = self:create_entity(conf)
            entity:set_owner_id(player_id)
        else
            log_err('[Copy][load_followers] proto_id={} not in config_followers(copy_id={})', proto_id, self:get_config_id())
        end
    end
end

function Copy:find_followers(player_id)
    local npcs = {}
    local count = 0
    for entity_id, entity in pairs(self.entitys) do
        if entity:is_npc() and entity:is_owned_by(player_id) then
            tinsert(npcs, entity)
            count = count + 1
        end
    end
    if count > 0 then
        return npcs
    end
end

function Copy:reset_config_followers()
    self.config_followers = {}
end

function Copy:unload_followers(player_id)
    local npcs = self:find_followers(player_id)
    if not npcs then
        return
    end
    for _, npc in ipairs(npcs) do
        self:destory_entity(npc:get_id())
    end
end

function Copy:get_copy_type(copy_config)
    return (self.copy_config or copy_config or {}).type
end

function Copy:is_temporary_copy(copy_config)
    return self:get_copy_type(copy_config) == CT_TEMPORARY
end

function Copy:is_once_copy(copy_config)
    return self:get_copy_type(copy_config) == CT_ONCE
end

function Copy:is_period_copy(copy_config)
    return self:get_copy_type(copy_config) == CT_PERIOD
end

return Copy
