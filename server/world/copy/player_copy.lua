--player_copy.lua
local PlayerCopy = class()
local prop = property(PlayerCopy)
prop:accessor("temporary", false)   -- 是否是临时副本

local dbprop = db_property(PlayerCopy, "player_copy")
dbprop:store_value("copy_id", 0)         -- 副本id
dbprop:store_value("copy_config", 0)     -- 副本配置id(重入需要，避免等待查copy表后再读配置)
dbprop:store_value("copy_level", 0)      -- 当前关卡
dbprop:store_value("copy_map", 0)        -- 副本所属地图id(重入需要)
dbprop:store_values("followers", {})    -- 随从NPC信息(原型id、位置、朝向)

function PlayerCopy:__init()
end

function PlayerCopy:init_data(data)
    self:save_copy_id(data.copy_id)
    self:save_copy_config(data.copy_config)
    self:save_copy_level(data.copy_level)
    self:save_copy_map(data.copy_map)
    local followers = data.followers or {}
    for proto_id, follower_data in pairs(followers) do
        self:save_followers_field(proto_id, {
            x=follower_data.x,
            y=follower_data.y,
            z=follower_data.z,
            d=follower_data.d,
        })
    end
end

function PlayerCopy:load_data(data)
    self:set_copy_id(data.copy_id)
    self:set_copy_config(data.copy_config)
    self:set_copy_level(data.copy_level)
    self:set_copy_map(data.copy_map)
    self:set_followers(data.followers or {})
end

function PlayerCopy:upsert_follower(proto_id, pos, dir_y, force_save)
    local follower = self:get_followers(proto_id)
    local is_exists = (follower and next(follower)) or false
    -- 新数据
    follower = { x=pos.x, y=pos.y, z=pos.z, d=dir_y, }
    if not self:is_temporary() and (not is_exists or force_save) then
        -- 非临时副本 and (不存在 或 强制保存)
        self:save_followers_field(proto_id, follower)
        return
    end
    -- 更新但不存库
    self:set_followers_field(proto_id, follower)
end

return PlayerCopy
