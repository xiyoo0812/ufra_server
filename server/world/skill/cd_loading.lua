--cd_loading.lua

local timer_mgr = quanta.get("timer_mgr")

local CDLoading = class()
local prop = property(CDLoading)
prop:reader("breakin", 0)       --breakin
prop:reader("timer_id", nil)    --timer_id

function CDLoading:__init()
end

--startup
function CDLoading:startup(callback, time, breakin)
    self.breakin = breakin or 0
    self.timer_id = timer_mgr:once(time, function()
        self.timer_id = nil
        callback()
    end)
end

--stop
function CDLoading:stop(active)
    if self.timer_id then
        if self:check_breakin(active) then
            timer_mgr:unregister(self.timer_id)
            self.timer_id = nil
            return true
        end
        return false
    end
    return true
end

--check_breakin
function CDLoading:check_breakin(active)
    if self.breakin == 0 then
        return true
    end
    if active then
        return self.breakin == 1
    end
    return self.breakin == 2
end

return CDLoading
