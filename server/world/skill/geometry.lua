--geometry.lua
local mrad              = math.rad
local mcos              = math.cos
local msin              = math.sin
local mmax              = math.max
local mmin              = math.min

--- 转换为弧度
local function deg_to_rad(angle)
    -- return angle * math.pi / 180
    return mrad(angle)
end

--- 旋转点
local function rotate_point(x, z, angle)
    local rad = deg_to_rad(angle)
    local cos_angle = mcos(rad)
    local sin_angle = msin(rad)
    local rotated_x = x * cos_angle - z * sin_angle
    local rotated_z = x * sin_angle + z * cos_angle
    return rotated_x, rotated_z
end

--- 求点到线段最近的点
local function closest_point_on_segment(p, a, b)
    local ab = {x = b.x - a.x, y = b.y - a.y, z = b.z - a.z}
    local ap = {x = p.x - a.x, y = p.y - a.y, z = p.z - a.z}
    local dot = ap.x * ab.x + ap.y * ab.y + ap.z * ab.z
    local length_squared = ab.x * ab.x + ab.y * ab.y + ab.z * ab.z
    local t = mmax(0, mmin(1, dot / length_squared))

    return {
        x = a.x + t * ab.x,
        y = a.y + t * ab.y,
        z = a.z + t * ab.z
    }
end


---@class geometry
local geometry = {}

---@class vector3
---@field x number
---@field y number
---@field z number
-- local vector3 = {}

---米转厘米
---@param value number|vector3
function geometry.m2cm(value)
    if type(value) == "number" then
        return value * 100
    end
    if value.x and value.y and value.z then
        return {
            x = value.x * 100,
            y = value.y * 100,
            z = value.z * 100,
        }
    end
    return value
end

--- 判断点是否在球体内
---@param target_world_pos vector3 目标世界位置
---@param my_world_pos vector3 我的世界位置
---@param my_world_rot_y number 我的世界朝向y分量欧拉角
---@param local_sphere_center vector3 球体中心点本地坐标
---@param sphere_radius number 球体角度
---@return boolean
function geometry.is_point_inside_sphere(target_world_pos, my_world_pos, my_world_rot_y, local_sphere_center, sphere_radius)
    -- 将目标点从世界坐标系转换到我的局部坐标系
    local local_target_pos_x = target_world_pos.x - my_world_pos.x
    local local_target_pos_y = target_world_pos.y - my_world_pos.y
    local local_target_pos_z = target_world_pos.z - my_world_pos.z

    local rotated_x, rotated_z = rotate_point(local_target_pos_x, local_target_pos_z, -my_world_rot_y)

    local centered_x = rotated_x - local_sphere_center.x
    local centered_y = local_target_pos_y - local_sphere_center.y
    local centered_z = rotated_z - local_sphere_center.z

    local distance_squared = centered_x * centered_x + centered_y * centered_y + centered_z * centered_z
    local radius_squared = sphere_radius * sphere_radius

    return distance_squared <= radius_squared
end

--- 判断点是否在立方体内
---@param target_world_pos vector3 目标世界位置
---@param my_world_pos vector3 我的世界位置
---@param my_world_rot_y number 我的世界朝向y分量欧拉角
---@param local_cube_center vector3 立方体中心点本地坐标
---@param cube_extent vector3
---@return boolean
function geometry.is_point_inside_cube(target_world_pos, my_world_pos, my_world_rot_y, local_cube_center, cube_extent)
    local relative_pos_x = target_world_pos.x - my_world_pos.x
    local relative_pos_y = target_world_pos.y - my_world_pos.y
    local relative_pos_z = target_world_pos.z - my_world_pos.z

    local rotated_x, rotated_z = rotate_point(relative_pos_x, relative_pos_z, -my_world_rot_y)

    local local_pos_x = rotated_x - local_cube_center.x
    local local_pos_y = relative_pos_y - local_cube_center.y
    local local_pos_z = rotated_z - local_cube_center.z

    return local_pos_x >= -cube_extent.x and local_pos_x <= cube_extent.x
           and local_pos_y >= -cube_extent.y and local_pos_y <= cube_extent.y
           and local_pos_z >= -cube_extent.z and local_pos_z <= cube_extent.z
end

--- 判断点是否在胶囊体内
---@param target_world_pos vector3 目标世界位置
---@param my_world_pos vector3 我的世界位置
---@param my_world_rot_y number 我的世界朝向y分量欧拉角
---@param local_capsule_center1 vector3 球体中心点1本地坐标
---@param local_capsule_center2 vector3 球体中心点2本地坐标
---@param capsule_radius number 胶囊体半径
---@return boolean
function geometry.is_point_inside_capsule(target_world_pos, my_world_pos, my_world_rot_y, local_capsule_center1, local_capsule_center2, capsule_radius)
    -- 将目标点从世界坐标系转换到我的局部坐标系
    local local_target_pos_x = target_world_pos.x - my_world_pos.x
    local local_target_pos_y = target_world_pos.y - my_world_pos.y
    local local_target_pos_z = target_world_pos.z - my_world_pos.z

    local rotated_x, rotated_z = rotate_point(local_target_pos_x, local_target_pos_z, -my_world_rot_y)

    local local_target_pos = {x = rotated_x, y = local_target_pos_y, z = rotated_z}

    -- 计算目标点到胶囊体轴线段最近的点
    local closest_point = closest_point_on_segment(local_target_pos, local_capsule_center1, local_capsule_center2)

    -- 计算目标点到最近点的距离
    local dx = local_target_pos.x - closest_point.x
    local dy = local_target_pos.y - closest_point.y
    local dz = local_target_pos.z - closest_point.z
    local distance_squared = dx * dx + dy * dy + dz * dz

    return distance_squared <= capsule_radius * capsule_radius
end

return geometry
