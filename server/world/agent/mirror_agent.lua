--mirror_agent.lua

local MirrorAgent   = class()
local prop = property(MirrorAgent)
prop:reader("player_id", nil)
prop:reader("mirror", true)
prop:reader("token", nil)

function MirrorAgent:__init(token)
    self.token = token
end

return MirrorAgent
