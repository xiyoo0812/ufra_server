--world_agent.lua
local RpcClient    = import("network/rpc_client.lua")
local HttpServer   = import("network/http_server.lua")

local env_get      = environ.get
local env_addr     = environ.addr
local env_number   = environ.number
local log_info     = logger.info
local log_debug    = logger.debug
local log_err      = logger.err
local signal_quit  = signal.quit

local timer_mgr     = quanta.get("timer_mgr")
local event_mgr     = quanta.get("event_mgr")
local mirror_mgr    = quanta.get("mirror_mgr")
local unq_driver    = quanta.get("unq_driver")
local sdb_driver    = quanta.get("sdb_driver")
local mdb_driver    = quanta.get("mdb_driver")
local store_mgr     = quanta.get("store_mgr")
local client_mgr    = quanta.get("client_mgr")
local cache_agent   = quanta.get("cache_agent")
local redis_agent   = quanta.get("redis_agent")
local update_mgr    = quanta.get("update_mgr")
local protobuf_mgr  = quanta.get("protobuf_mgr")

local WD_NULL       = protobuf_mgr:enum("world_type", "WORLD_NULL")
local WD_AGENT      = protobuf_mgr:enum("world_type", "WORLD_AGENT")
local WD_OFFICIAL   = protobuf_mgr:enum("world_type", "WORLD_OFFICIAL")
local WD_DEDICATED  = protobuf_mgr:enum("world_type", "WORLD_DEDICATED")

local QUANTA_KVDB   = env_get("QUANTA_KVDB", nil)
local QUANTA_WTYPE  = env_number("QUANTA_WTYPE", WD_NULL)

local WorldAgent = singleton()
local prop = property(WorldAgent)
prop:reader("client", nil)
prop:reader("index", 0)

function WorldAgent:__init()
    local domain = env_get("QUANTA_DOMAIN_ADDR")
    --注册事件
    event_mgr:add_trigger(self, "on_login_success")
    event_mgr:add_trigger(self, "on_logout_success")
    if QUANTA_WTYPE == WD_DEDICATED or QUANTA_WTYPE == WD_OFFICIAL then
        --创建RPC连接
        local ip, port = env_addr("QUANTA_WORLDA_ADDR")
        self.client = RpcClient(self, ip, port)
        log_info("[WorldAgent][QUANTA_WORLDA_ADDR]: wtype: {}-{}-{}!", QUANTA_WTYPE, ip, port)

        --监听router
        if QUANTA_WTYPE == WD_OFFICIAL then
            cache_agent:set_proxy(self)
            redis_agent:set_proxy(self)
        end
        event_mgr:add_listener(self, "rpc_world_sync")
    else
        domain = luabus.host()
        --创建HTTP服务器
        self.http_server = HttpServer(env_get("QUANTA_WORLDA_HTTP"))
        self.http_server:register_post("/request-world", "on_request_world", self)
    end
    self:init_local_db()
    --设置domain
    service.modify_host(client_mgr:get_port(), domain)
    --默认60秒关闭
    local interval = env_number("QUANTA_CLOSE_WORLD", 60000)
    log_debug("WorldAgent:__init interval:{}", interval)
    timer_mgr:loop(interval, function()
        local player_mgr = quanta.get("player_mgr")
        self.index = 0
        log_debug("WorldAgent:loop size:{}", player_mgr:size())
        if player_mgr:size() == 0 then
            signal_quit()    --关闭当前进程
        end
    end)
    update_mgr:attach_second(self)
end

function WorldAgent:init_local_db()
    local wid = env_number("QUANTA_WID")
    if not wid or wid == 0 then
        log_err("[WorldAgent][init_local_db] wid err wid:{}", wid)
        return
    end
    local driver = nil
    if QUANTA_KVDB == "sqlite" then
        driver = sdb_driver
    elseif QUANTA_KVDB == "lmdb" then
        driver = mdb_driver
    elseif QUANTA_KVDB == "unqlite" then
        driver = unq_driver
    end
    if not driver then
        log_err("[WorldAgent][init_local_db] driver err wid:{} QUANTA_KVDB:{}", wid, QUANTA_KVDB)
    end
    store_mgr:open(driver, wid, wid)
end

-- 同步世界
function WorldAgent:rpc_world_sync(id, data)
    log_debug("[WorldAgent][rpc_world_sync] id:{} data:{}", id, data)
    quanta.setenv("QUANTA_WNAME", data.name or "")
    quanta.setenv("QUANTA_MAX", data.max_count or 0)
    quanta.setenv("QUANTA_WTOKEN", data.token or "")
    return 0
end

function WorldAgent:on_second()
    self.index = self.index + 1
    -- log_debug("WorldAgent:on_second size:{} index:{}", quanta.get("player_mgr"):size(), self.index)
end

function WorldAgent:get_world_info(req_user_id)
    local world_id = env_number("QUANTA_WID")
    local world_type = env_number("QUANTA_WTYPE")
    local world_max = env_number("QUANTA_MAX")
    local world_name = env_get("QUANTA_WNAME")
    local user_id = env_number("QUANTA_USER")
    local token = env_get("QUANTA_WTOKEN")
    local result = {
        code = 0,
        host = quanta.node_info.host,
        port = quanta.node_info.port,
        world_id = world_id,
        name = world_name,
        max_count = world_max,
        user_id = user_id,
        world_type = world_type,
        is_password = false
    }
    if req_user_id ~= user_id and (token and token ~= "") then
        result.is_password = true
    end
    log_debug("[WorldAgent][on_request_world] result:{}", result)
    return result
end

function WorldAgent:init_world_env(body)
    quanta.setenv("QUANTA_WTYPE", body.type or 0)
    quanta.setenv("QUANTA_WNAME", body.name or "")
    quanta.setenv("QUANTA_WID", body.world_id or 0)
    quanta.setenv("QUANTA_MAX", body.max_count or 0)
    quanta.setenv("QUANTA_USER", body.master or 0)
    quanta.setenv("QUANTA_WTOKEN", body.token or "")
end

function WorldAgent:up_world_env(body)
     -- 房主进入时,更新配置
     if body.user_id == body.master then
        log_debug("[WorldAgent][up_world_env] update world info")
        quanta.setenv("QUANTA_WNAME", body.name or "")
        quanta.setenv("QUANTA_MAX", body.max_count or 0)
        quanta.setenv("QUANTA_WTOKEN", body.token or "")
    end
end

function WorldAgent:on_request_world(url, body, params, headers)
    log_debug("WorldAgent:on_request_world url={}, body={}, params={}, headers={}", url, body, params, headers)
    local world_id = env_number("QUANTA_WID")
    -- 创建世界或通过记录进入的用户
    if body.world_id and body.world_id ~= 0 then
        if not world_id then
            if not body.world_id then
                log_debug("WorldAgent:on_request_world body.world_id is nil")
                return { code = -1 }
            end
            -- 初始化只能由房主初始化
            if body.user_id ~= body.master then
                log_debug("WorldAgent:on_request_world not master init fail user_id:{} master:{}", body.user_id, body.master)
                return { code = -1 }
            end
            world_id = body.world_id
            -- 初始化世界环境变量
            self:init_world_env(body)
            self:init_local_db()
            --开启代理穿透
            if body.type == WD_AGENT then
                mirror_mgr:setup(world_id, body.mirror_addr)
            end
        elseif body.world_id ~= world_id then
            log_debug("[WorldAgent][on_request_world] body.world_id ~= world_id body.world_id:{}", world_id, body.world_id)
            -- 不支持同时运行多个世界
            return { code = -1 }
        else
            self:up_world_env(body)
        end
    -- 通过局域网加入
    else
        -- 世界没有初始化好,不能进入
        if not world_id then
            log_debug("[WorldAgent][on_request_world] world not init world_id:{} body.world_id:{}", world_id, body.world_id)
            return { code = -1 }
        end
    end
    return self:get_world_info(body.user_id)
end

function WorldAgent:on_login_success(player_id, player)
    if self.client == nil then
        return
    end
    local player_mgr = quanta.get("player_mgr")
    local size = player_mgr:size()
    local wid = env_number("QUANTA_WID")
    self.client:call("rpc_player_count", wid, size)
end

function WorldAgent:on_logout_success(player_id, player)
    if self.client == nil then
        return
    end
    local player_mgr = quanta.get("player_mgr")
    local size = player_mgr:size()
    local wid = env_number("QUANTA_WID")
    self.client:call("rpc_player_count", wid, size)
end

-- 连接关闭回调
function WorldAgent:on_socket_error(client, token, err)
    log_info("[WorldAgent][on_socket_error]: connect lost!")
end

-- 连接成回调
function WorldAgent:on_socket_connect(client)
    log_info("[WorldAgent][on_socket_connect]: connect world agent success!")
    event_mgr:fire_frame(function()
        self.client:register()
    end)
end

function WorldAgent:proxy_call(rpc, ...)
    return self.client:call("rpc_proxy_call", rpc, ...)
end

quanta.world_agent = WorldAgent()

return WorldAgent
