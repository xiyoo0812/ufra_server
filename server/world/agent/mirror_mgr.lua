--mirror_mgr.lua
local log_debug         = logger.debug
local log_info          = logger.info
local saddr             = qstring.addr
local env_number        = environ.number

local event_mgr         = quanta.get("event_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")

local RpcClient         = import("network/rpc_client.lua")
local MirrorAgent       = import("world/agent/mirror_agent.lua")

local ROLE_NOT_EXIST    = protobuf_mgr:error_code("LOGIN_ROLE_NOT_EXIST")

local MirrorMgr   = singleton()
local prop = property(MirrorMgr)
prop:reader("client", nil)
prop:reader("world_id", nil)
prop:reader("mirrors", {})

function MirrorMgr:__init()
end

function MirrorMgr:setup(world_id, mirror_addr)
    self.world_id = world_id
    service.modify_id(world_id)
    --RPC 服务器
    local ip, port = saddr(mirror_addr)
    self.client = RpcClient(self, ip, port)
    log_info("[MirrorMgr][setup_agent]: {}-{}!", ip, port)

    --注册事件
    event_mgr:add_trigger(self, "on_login_success")
    event_mgr:add_trigger(self, "on_logout_success")
    -- 网络事件监听
    event_mgr:add_listener(self, "rpc_transfer_message")
    event_mgr:add_listener(self, "rpc_transfer_login")
    event_mgr:add_listener(self, "rpc_transfer_logout")
    event_mgr:add_listener(self, "rpc_transfer_reload")
    event_mgr:add_listener(self, "rpc_transfer_offline")
end

function MirrorMgr:sync_player_count()
    log_debug("[MirrorMgr][sync_player_count]")
    if self.client == nil then
        log_debug("[MirrorMgr][sync_player_count] client is nil")
        return
    end
    local player_mgr = quanta.get("player_mgr")
    local size = player_mgr:size()
    local wid = env_number("QUANTA_WID")
    local ok, cb_data = self.client:call("rpc_player_count", wid, size)
    log_debug("[MirrorMgr][sync_player_count] wid:{} size:{} ok:{}, cb_data:{}", wid, size, ok, cb_data)
end

function MirrorMgr:on_login_success(player_id, player)
    log_debug("[MirrorMgr][on_login_success] player:{}", player_id)
    self:sync_player_count()
end

function MirrorMgr:on_logout_success(player_id, player)
    log_debug("[MirrorMgr][on_logout_success] player:{}", player_id)
    self:sync_player_count()
end

function MirrorMgr:close_session(player_id)
    self.client:send("on_transfer_close", self.world_id, player_id)
end

function MirrorMgr:send(player_id, cmd_id, body)
    self.client:send("on_transfer_message", self.world_id, player_id, cmd_id, body)
end

function MirrorMgr:callback_by_id(session_id, body, player_id)
    self.client:callback(session_id, true, body)
end

function MirrorMgr:callback_errcode(session_id, code, player_id)
    self.client:callback(session_id, true, { error_code = code })
end

function MirrorMgr:rpc_transfer_login(character_id, service_type, cmd_id, body, session_id, token)
    log_info("[MirrorMgr][rpc_transfer_login]: {}-{}-{}!", cmd_id, character_id, body)
    local mirror = MirrorAgent(token)
    event_mgr:notify_listener("on_socket_cmd", mirror, service_type, cmd_id, body, session_id)
    local player_id = mirror:get_player_id()
    if player_id then
        self.mirrors[player_id] = mirror
    end
end

function MirrorMgr:rpc_transfer_logout(player_id, service_type, cmd_id, body, session_id)
    log_info("[MirrorMgr][rpc_transfer_logout]: {}-{}-{}!", cmd_id, player_id, body)
    local mirror = self.mirrors[player_id]
    if mirror then
        self.mirrors[player_id] = nil
        event_mgr:notify_listener("on_socket_cmd", mirror, service_type, cmd_id, body, session_id)
        return
    end
    self:callback_errcode(session_id, ROLE_NOT_EXIST, player_id)
end

function MirrorMgr:rpc_transfer_reload(player_id, service_type, cmd_id, body, session_id)
    log_info("[MirrorMgr][rpc_transfer_reload]: {}-{}-{}!", cmd_id, player_id, body)
    local mirror = self.mirrors[player_id]
    if mirror then
        event_mgr:notify_listener("on_socket_cmd", mirror, service_type, cmd_id, body, session_id)
        return
    end
    self:callback_errcode(session_id, ROLE_NOT_EXIST, player_id)
end

function MirrorMgr:rpc_transfer_offline(player_id, token, err)
    log_info("[MirrorMgr][rpc_transfer_offline]: {}-{}-{}!", player_id, token, err)
    local mirror = self.mirrors[player_id]
    if mirror then
        event_mgr:notify_listener("on_socket_error", mirror, token, err)
    end
end

function MirrorMgr:rpc_transfer_message(player_id, service_type, cmd_id, body, session_id)
    local mirror = self.mirrors[player_id]
    if mirror then
        event_mgr:notify_listener("on_socket_cmd", mirror, service_type, cmd_id, body, session_id)
        return
    end
    self:callback_errcode(session_id, ROLE_NOT_EXIST, player_id)
end

-- 连接关闭回调
function MirrorMgr:on_socket_error(client, token, err)
    log_info("[MirrorMgr][on_socket_error]: connect lost!")
end

-- 连接成回调
function MirrorMgr:on_socket_connect(client)
    log_info("[MirrorMgr][on_socket_connect]: connect mirror success!")
    event_mgr:fire_frame(function()
        self.client:register()
        self:sync_player_count()
    end)
end

quanta.mirror_mgr = MirrorMgr()

return MirrorMgr
