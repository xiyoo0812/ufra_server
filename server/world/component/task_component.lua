--task_component.luac
local qfailed       = quanta.failed
local log_debug     = logger.debug
local tsize         = qtable.size
local tarray        = qtable.array
local tkarray       = qtable.tkarray

local store_mgr     = quanta.get("store_mgr")
local task_mgr      = quanta.get("task_mgr")
local event_mgr     = quanta.get("event_mgr")

local TaskComponent = mixin()
local prop = property(TaskComponent)
prop:reader("accepts", {})              --可接任务

local dbprop = db_property(TaskComponent, "player_task", true)
dbprop:store_objects("tasks", {})       --tasks
dbprop:store_values("historys", {})     --historys
dbprop:store_values("loop_count", 0)  -- 完成订单任务次数

local tkprop = db_property(TaskComponent, "player_talk", true)
tkprop:store_values("talks", {})    --talks

function TaskComponent:__init()
    self:watch_event(self, "on_task_reset")
    self:watch_event(self, "on_task_finished")
end

function TaskComponent:on_task_finished(task_id, task_type)
    if task_mgr:is_loop_task(task_type) then
        local count = self.loop_count
        self:save_loop_count(count + 1)
    end
end

function TaskComponent:_relive()
    self:sync_tasks()
end

function TaskComponent:on_task_reset(task_id)
    self:del_tasks_elem(task_id)
    local task = task_mgr:create_task(self, task_id)
    if task then
        self:receive_task(task, task_id)
    end
    log_debug("[TaskComponent][on_task_reset] task_id:{}", task_id)
end

function TaskComponent:get_refresh_time()
    return task_mgr:get_player_refresh_time(self)
end

--任务数据加载
function TaskComponent:on_db_player_task_load(data)

    if not data.player_id then
        --初次登陆
        local level = self:get_level()
        task_mgr:on_level_up(self, level)
        task_mgr:update_fix_dailytask(self)
        return true
    end
    log_debug("[TaskComponent][setup] player({}) load task data: {}", self.id, data)
    --任务完成记录
    self.historys = data.historys or {}
    self.loop_count = data.loop_count or 0
    --加载任务
    for task_id, t_data in pairs(data.tasks or {}) do
        local task = task_mgr:load_task_from_data(self, task_id, t_data)
        if task then
            task:load_data(t_data)
            self:set_tasks_elem(task_id, task)
            if task_mgr:is_daily_task(t_data.type) then
                self:update_daily_task(task_id, task)
            end
        end
    end
    return true
end

-- 更新(天)
function TaskComponent:_day_update(week_flush)
    for task_id, task in pairs(self.tasks) do
        if task_mgr:is_daily_task(task:get_type()) then
            if task:is_rand_daily() then
                self:del_tasks_elem(task_id)
            else
                task:reset()
            end
        end
    end
    --更新可接日常任务
    self:save_loop_count(0)
    task_mgr:update_rand_dailytask(self, self:get_level())
end

--日常任务初始化
function TaskComponent:update_daily_task(task_id, task)
    if self:is_same_day() then
        return false
    end
    if task:is_rand_daily() then
        self:del_tasks_elem(task_id)
        return true
    end
    task:reset()
    return true
end

--闲聊数据加载
function TaskComponent:on_db_player_talk_load(data)
    self.talks = data.talks or {}
    return true
end

function TaskComponent:_unload()
    for _, task in pairs(self.tasks) do
        task:close()
    end
end

function TaskComponent:count_rand_daily()
    return tsize(self.tasks, function(task)
        return task_mgr:is_daily_task(task:get_type()) and task:is_rand_daily()
    end)
end

function TaskComponent:get_task(task_id)
    return self.tasks[task_id]
end

function TaskComponent:get_chain_task(chain_id)
    return self.historys[chain_id]
end

function TaskComponent:get_accept_task(task_id)
    return self.accepts[task_id]
end

function TaskComponent:receive_task(task, task_id)
    log_debug("[TaskComponent][receive_task] task_id:%s", task_id)

    self:save_tasks_elem(task_id, task)
    local proto_id = task:get_proto_id()
    self:notify_event("on_task_receive", task_id, proto_id)
    if task:auto_receive() then
        task:active()
    end
    self.accepts[task_id] = nil
    event_mgr:notify_trigger("on_task_receive", self, task)
end

function TaskComponent:receive_loop_task(task, task_id)
    log_debug("[TaskComponent][receive_loop_task] task_id:%s", task_id)
    self:save_tasks_elem(task_id, task)
    self:send("NID_TASK_LOOP_SYNC_NTF", { task_id = task_id, task = task:pack2client() })
    event_mgr:notify_trigger("on_task_receive", self, task)
    if task:auto_receive() then
        task:active()
    end
    self:notify_event("on_task_receive", task_id)
end

function TaskComponent:finish_task(task_id)
    local task = self.tasks[task_id]
    if task then
        local ttype = task:get_type()
        if task_mgr:is_remove_task(ttype) then
            self:del_tasks_elem(task_id)
            if task_mgr:is_history_task(ttype) then
                self:save_historys_field(task:get_chain_id(), task_id)
            end
        end
        task:close()
    end
end

function TaskComponent:update_accept(task_id, task)
    if task_id and task then
        self.accepts[task_id] = task
        self:send("NID_TASK_ACCEPT_SYNC_NTF", { tasks = { task_id } })
        return
    end
    self:send("NID_TASK_ACCEPT_SYNC_NTF", { tasks = tkarray(self.accepts) })
end

function TaskComponent:has_finish_task(task_id)
    local conf = task_mgr:find_task(task_id)
    if conf then
        local chain = conf.chain
        if chain then
            local chain_task = self.historys[chain] or 0
            return chain_task >= task_id
        end
    end
    return false
end

function TaskComponent:has_finish_target(target_id)
    local conf = task_mgr:find_target_conf(target_id)
    if conf == nil then
        return false
    end
    if not self:has_finish_task(conf.parent_id) then
        local task = self:get_task(conf.parent_id)
        if task == nil then
            return false
        end
        local target = task:get_target(target_id)
        return target == nil or target:is_complete()
    end
    return true
end

function TaskComponent:sync_task(task)
    self:send("NID_TASK_CONTENT_SYNC_NTF", task:pack_content())
end

function TaskComponent:sync_progress(task)
    self:send("NID_TASK_PROGRESS_NTF", task:pack_progress())
end

function TaskComponent:sync_tasks()
    local cur_tasks = {}
    local loop_tasks = {}
    for task_id, task in pairs(self.tasks) do
        if task:is_loop_task() then
            loop_tasks[task_id] = task:pack2client()
        else
            cur_tasks[task_id] = task:pack2client()
        end
    end
    local data = {
        tasks = cur_tasks,
        loops = loop_tasks,
        dailys = tarray(self.dailys),
        historys = tarray(self.historys)
    }
    self:send("NID_TASK_LIST_SYNC_NTF", data)
end

--加载闲聊
function TaskComponent:load_talks()
    if not self:is_player_talk_loaded() then
        return store_mgr:load(self, self.id, "player_talk")
    end
    return true
end

--更新闲聊
function TaskComponent:update_talk(talk_id, status)
    if self:load_talks() then
        local old_status = self.talks[talk_id]
        if not old_status or status > old_status then
            self:save_talks_field(talk_id, status)
            if old_status then
                task_mgr:reward_talk(self, talk_id)
            end
            return true
        end
    end
    return false
end

function TaskComponent:find_building_count(build_info)
    local ok, code, count = self:call_service("scene", "rpc_find_finish_build_count", build_info)
    return qfailed(code, ok) and 0 or count
end

return TaskComponent
