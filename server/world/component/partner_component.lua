--partner_component.lua
local log_err           = logger.err
local log_debug         = logger.debug
local guid_new          = codec.guid_new
local sformat           = string.format
local tinsert           = table.insert

local event_mgr         = quanta.get("event_mgr")
local newbee_mgr        = quanta.get("newbee_mgr")
local config_mgr        = quanta.get("config_mgr")
local recruit_mgr       = quanta.get("recruit_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")
local TARGET_BUILDING   = quanta.enum("NfSkillTarget", "BUILDING")

local npc_db            = config_mgr:init_table("npc", "id")
local nf_skill_db       = config_mgr:init_table("nf_skill", "id")

local STATE_IDLE        = protobuf_mgr:enum("partner_state", "STATE_IDLE")
local STATE_SEND        = protobuf_mgr:enum("partner_state", "STATE_SEND")
local STATE_DRIVE       = protobuf_mgr:enum("partner_state", "STATE_DRIVE")
local OBTAIN_NPC        = protobuf_mgr:enum("obtain_reason", "NID_OBTAIN_NPC")
local OBTAIN_SYSTEM     = protobuf_mgr:enum("obtain_reason", "NID_OBTAIN_SYSTEM")

local Partner           = import("world/entity/partner.lua")


local PartnerComponent = mixin()
local prop = property(PartnerComponent)
prop:reader("partner_map", {})          --伙伴原型索引(key-proto_id value-伙伴信息)
prop:accessor("sat_max", 0)               --当前满意度上限
prop:accessor("sat_atten", 0)             --当前满意度衰减值

local dbprop = db_property(PartnerComponent, "player_partner", true)
dbprop:store_objects("partners", {})     --伙伴

function PartnerComponent:__init()
    --状态改变事件
    event_mgr:add_trigger(self, "on_partner_change_state")
end

--获取派驻的个数
function PartnerComponent:get_send_partner_count()

    local send_count = 0
    for _, partner in pairs(self.partner_map or {}) do
        if partner:get_state() == STATE_SEND then
            send_count = send_count + 1
        end
    end
    return send_count
end

function PartnerComponent:get_partner_count()
    local count = 0
    for _, _ in pairs(self.partner_map or {}) do
        count = count + 1
    end
    return count
end

function PartnerComponent:get_checkin_count()
    local count = 0
    for _, partner in pairs(self.partner_map or {}) do
        if partner:check_house() then
            count = count + 1
        end
    end
    return count
end

function PartnerComponent:on_db_player_partner_load(data)
    log_debug("[PartnerComponent][on_db_player_partner_load] data:{}", data)
    if data.player_id then
        local send_count = 0
        local player_partner = data.partners
        for uuid, partner_data in pairs(player_partner) do
            log_debug("[PartnerComponent][on_db_player_partner_load] uuid:{} proto_id:{}", uuid, partner_data.proto_id)
            local star_lv, proto_id, lv, limit_lv = partner_data.star_lv,partner_data.proto_id,partner_data.lv,partner_data.limit_lv
            if proto_id and limit_lv and lv and star_lv then
                local lv_id = recruit_mgr:get_lv_id(proto_id, limit_lv,  lv)
                local partner = Partner(self, uuid, proto_id, lv_id, star_lv)
                partner:load_db(partner_data)
                partner:load_nf_skill()
                self:set_partners_elem(uuid, partner)
                self.partner_map[proto_id] = partner
                if partner:get_house_id() and partner:get_house_id() ~= 0 then
                    send_count = send_count + 1
                end
            else
                log_err("[PartnerComponent][on_db_player_partner_load] failed. uuid:{} proto_id:{}", uuid, partner_data.proto_id)
            end
        end
        -- self:get_scene():add_human(send_count)
    else
        --初始送npc
        newbee_mgr:init_partners(self)
    end
    return true
end

--检查是否跨天
function PartnerComponent:_online()
    if not self:is_same_day() then
        log_debug("[PartnerComponent][_online] player_id:{} check_same_day:false", self.id)
        self:reset_day_gift()
        self:set_day_talk_liking(0)
        return
    end
    log_debug("[PartnerComponent][_online] player_id:{} check_same_day:true", self.id)
end

--加载伙伴对建筑的影响
function PartnerComponent:load_building_effect()
    for proto_id, partner in pairs(self.partner_map or {}) do
        log_debug("[PartnerComponent][load_building_effect] partner_id:{} proto_id:{} load buff", partner:get_id(), proto_id)
        local player = partner:get_player()
        local building_id = partner:get_building_id()
        log_debug("[PartnerComponent][load_building_effect] partner_id:{} building_id:{}", partner:get_id(), building_id)
        --未派驻
        if not building_id or building_id == 0 then
            goto continue
        end
        local building = player:get_scene():get_building(building_id)
        --非产出类型建筑
        local product_type = building:get_prototype().product_type or 0
        if product_type == 0 then
            goto continue
        end
        --添加属性
        building:load_partner_attr(partner)
        --添加对建筑的buff
        local star_conf = partner:get_star_conf()
        if not star_conf or not star_conf.nf_skills then
            log_debug("[PartnerComponent][load_building_effect] player_id:{} partner_id:{} dont have skill", player:get_id(), partner:get_id())
            goto continue
        end
        self:load_nf_skill(star_conf, building, partner)
        ::continue::
    end
end

--加载生活技能
function PartnerComponent:load_nf_skill(star_conf, building, partner)
    for _, nf_skill_id in pairs(star_conf.nf_skills or {}) do
        local skill_conf = nf_skill_db:find_one(nf_skill_id)
        if not skill_conf or not skill_conf.skill_effect then
            goto continue
        end
        --判断作用目标
        if skill_conf.target ~= TARGET_BUILDING then
            goto continue
        end
        for _, buff_id in pairs(skill_conf.skill_effect) do
            building:add_partner_buff(partner:get_id(), buff_id)
        end
        ::continue::
    end
end

--初始化玩家伙伴
function PartnerComponent:init_born_partner(proto_ids)
    log_debug("[PartnerComponent][init_born_partner] proto_ids:{}", proto_ids)
    local born_partners = {}
    for proto_id, _ in pairs(proto_ids) do
        local partner = self:recv_partner(proto_id, OBTAIN_SYSTEM)
        if partner then
            tinsert(born_partners, partner)
        end
    end
end

--每日重置
function PartnerComponent:_day_update()
    self:reset_day_gift()
    self:set_day_talk_liking(0)
end

function PartnerComponent:_update(now)
    for _, partner in pairs(self.partners or {}) do
        partner:update(now)
    end
end

--重置送礼次数
function PartnerComponent:reset_day_gift()
    log_debug("[PartnerComponent][reset_day_gift] day reset")
    for _, partner in pairs(self.partners) do
        partner:reset_day_gift()
    end
    self:broadcast_partners()
end

--状态改变
function PartnerComponent:on_partner_change_state(player, id, state)
    local partner = self:get_partner(id)
    if not partner then
        return
    end
    partner:save_state(state)
    self:update_partner(partner:get_id(), partner)
end

--遣散npc
function PartnerComponent:drive_partner(partner)
    local parnter_conf = partner:get_base_conf()
    if not parnter_conf then
        return
    end
    --扣除繁荣度
    local cost_town_exp = parnter_conf.drop_town_exp
    if cost_town_exp then
        event_mgr:notify_trigger("on_town_exp_cost", self, cost_town_exp)
    end
    local town = self:get_scene()
    --失去工作
    local building_id = partner:get_building_id()
    if building_id and building_id ~= 0 then
        local building = town:get_building(building_id)
        if building then
            building:recall_partner(partner)
        end
    end
    --失去民居
    local house_id = partner:get_house_id()
    if house_id and house_id ~= 0 then
        partner:drive_away()
    end
    partner:set_state(STATE_DRIVE)

    self:del_partners_elem(partner:get_id())
    self.partner_map[parnter_conf.id] = nil
    self:update_partner(partner:get_id(), partner)
end

--派驻npc
function PartnerComponent:send_partner(partner, building, pos)
    partner:save_state(STATE_SEND)
    partner:save_building_id(building:get_id())
    partner:save_building_pos(pos)
    self:update_partner(partner:get_id(), partner)

    self:notify_event("on_send_partner", partner:get_proto_id(), building:get_group())
    event_mgr:notify_trigger("on_npc_dispatch", self, 1, partner, 1, sformat("%s", partner:get_building_id()))
end

function PartnerComponent:is_send_partner(npc_id, group_id)
    local partner = self:has_partner(npc_id)
    if partner == nil then
        return false
    end
    local town = self:get_scene()
    local uuid = partner:get_building_id()
    local building = town:get_building(uuid)
    return building ~= nil and building:get_group() == group_id
end

function PartnerComponent:get_affinity_lv(npc_id)
    local partner = self:has_partner(npc_id)
    if partner == nil then
        return 0
    end
    return partner:get_affinity_lv()
end

--召回npc
function PartnerComponent:recall_partner(partner)
    partner:save_state(STATE_IDLE)
    partner:save_building_id(0)
    partner:save_building_pos(0)
    self:update_partner(partner:get_id(), partner)
    event_mgr:notify_trigger("on_npc_dispatch", self, 2, partner, 1, sformat("%s", partner:get_building_id()))
end

--接收伙伴
function PartnerComponent:recv_partner(proto_id, from)
    log_debug("[PartnerComponent][on_partner_add] partner_id:{}", proto_id)
    --proto_id合理性判定
    local npc_conf = npc_db:find_one(proto_id)
    if not npc_conf then
        log_err("[PartnerComponent][recv_partner] cant find {} npc", proto_id)
        return
    end
    --配置判定
    local lv, star_lv = npc_conf.init_lv, npc_conf.star_lv
    if lv == nil or star_lv == nil then
        log_err("[PartnerComponent][recv_partner] npc_proto_id:{} dont have lv or star_lv", proto_id)
        return
    end
    --重复判定
    if self:has_partner(proto_id) then
        log_debug("[PartnerComponent][recv_partner] has partner_id:{}", proto_id)
        recruit_mgr:npc_repeated(self, proto_id)
        return
    end
    local uuid = guid_new()
    --获取npc初始配置
    local partner = Partner(self, uuid, proto_id, lv, star_lv, true)
    if not partner then
        log_err("[PartnerComponent][recv_partner] cant find {} npc", proto_id)
        return
    end
    partner:set_from(from)
    partner:set_state(STATE_IDLE)
    self:save_partners_elem(uuid, partner)
    self.partner_map[proto_id] = partner
    self:add_partner(uuid, partner)
    self:notify_event("on_recv_partner", proto_id)
    --掉落繁荣度
    local drop_town_exp = partner:get_base_conf().drop_town_exp
    if drop_town_exp and drop_town_exp ~= 0 then
        event_mgr:notify_trigger("on_town_exp_drop", self, drop_town_exp, OBTAIN_NPC)
    end
    partner:load_nf_skill()
    --埋点
    event_mgr:notify_trigger("on_npc_flow", self, 3, partner)
    return partner
end

--是否拥有npc
function PartnerComponent:has_partner(proto_id)
    return self.partner_map[proto_id]
end

function PartnerComponent:sync_partners()
    local packs = self:pack_partners()
    log_debug("[PartnerComponent][sync_partners] data:{}", packs)
    self:send("NID_PARTNER_LIST_NTF", packs)
end

function PartnerComponent:pack_partners()
    local partners = {}
    for id, partner in pairs(self.partners) do
        partners[id] = partner:pack2client()
    end
    return {master = self.id, partners = partners}
end

function PartnerComponent:get_partner(id)
    return self.partners[id]
end

function PartnerComponent:update_partner(id, partner)
    local data = partner:pack2client()
    local datas = {master = self.id, part_sync = true, partners = {[id] = data}}

    local town = self:get_scene()
    if town then
        town:broadcast_message("NID_PARTNER_LIST_NTF", datas)
    end
end

function PartnerComponent:broadcast_partners()
    local partners = {}
    for id, partner in pairs(self.partners) do
        partners[id] = partner:pack2client()
    end
    log_debug("[PartnerComponent][broadcast_partners] data:{}", partners)
    local datas = {master = self.id, part_sync = false, partners = partners}
    local town = self:get_scene()
    if town then
        town:broadcast_message("NID_PARTNER_LIST_NTF", datas)
    end
end

function PartnerComponent:add_partner(id, partner)
    local data = partner:pack2client()
    local datas = { part_sync = true, master = self.id, partners = { [id] = data }}
    local town = self:get_scene()
    if town then
        town:broadcast_message("NID_PARTNER_LIST_NTF", datas)
    end
end

function PartnerComponent:pack_partner2db()
    local partners = {}
    for id, partner in pairs(self.partners or {}) do
        partners[id] = partner:pack2db()
    end
    return partners
end

--失去民居
function PartnerComponent:drive_home(building_id)
    local sync_partners = {}
    for id, partner in pairs(self.partners or {}) do
        if partner:get_house_id() == building_id then
            partner:drive_away()
            sync_partners[id] = partner:pack2client()
        end
    end
    if next(sync_partners) then
        local datas = {master = self.id, part_sync = true, partners = sync_partners}
        local town = self:get_scene()
        if town then
            town:broadcast_message("NID_PARTNER_LIST_NTF", datas)
        end
    end
end

--指定npc是否在指定建筑工作
function PartnerComponent:is_building_worker(npc_proto_id, building_proto_id)
    --找到建筑
    local town = self:get_scene()
    if not town then
        return false
    end
    local buildings = town:get_building_by_proto(building_proto_id)
    if not next(buildings) then
        return false
    end

    --找到npc
    local partner = self.partner_map[npc_proto_id]
    if not partner then
        return false
    end

    --是否在建筑中工作
    local building = buildings[partner:get_building_id()]
    if not building then
        return false
    end
    return true
end

--计算满意度上限值
function PartnerComponent:calc_sat(partner, check_in)
    if check_in then
        self.sat_max = self.sat_max + (partner:get_base_conf().sat_max or 0)
        self.sat_atten = self.sat_atten + (partner:get_base_conf().sat_atten or 0)
    else
        self.sat_max = self.sat_max - (partner:get_base_conf().sat_max or 0)
        self.sat_atten = self.sat_atten - (partner:get_base_conf().sat_atten or 0)
    end
end
return PartnerComponent
