--ground_component.lua
local Ground          = import("world/entity/ground.lua")
local log_debug       = logger.debug
local log_err         = logger.err
local qfailed         = quanta.failed
local tsize           = qtable.size

local newbee_mgr      = quanta.get("newbee_mgr")
local object_fty      = quanta.get("object_fty")
local config_mgr      = quanta.get("config_mgr")
local protobuf_mgr    = quanta.get("protobuf_mgr")

local block_db        = config_mgr:init_table("block", "id")

local OBTAIN_GROUND   = protobuf_mgr:enum("obtain_reason", "NID_OBTAIN_GROUND")

local GroundComponent = mixin()
local dbprop          = db_property(GroundComponent, "home_ground", true)
dbprop:store_objects("grounds", {}) --地块解锁列表

function GroundComponent:__init()
    log_debug("[GroundComponent][__init]... id:{}", self.id)
end

function GroundComponent:on_db_home_ground_load(data)
    log_debug("[GroundComponent][on_db_home_ground_load] data:{}", data)
    if data.grounds then
        local grounds = data.grounds or {}
        for id, info in pairs(grounds) do
            local ground = Ground(id)
            ground:load_db(info)
            self:set_grounds_elem(id, ground)
        end
    else
        newbee_mgr:init_grounds(self)
    end
    return true
end

function GroundComponent:init_born_grounds(ids)
    for _, id in pairs(ids) do
        local ground = Ground(id)
        self:set_grounds_elem(id, ground)
        --解锁地皮下的默认地块
        self:unlock_default_block(nil, id)
    end
    self:flush_home_ground_db()
end

--是否建造完成
function GroundComponent:is_ground_finish(id)
    local ground = self.grounds[id]
    if not ground then
        return false
    end
    return ground:is_finish()
end

function GroundComponent:get_ground_unlock_count(type)
    if type == nil then
        return tsize(self.grounds)
    end
    local count = 0;
    for _, ground in pairs(self.grounds) do
        if ground:get_type() == type then
            count = count + 1
        end
    end
    return count
end

--是否揭幕
function GroundComponent:is_ground_unveil(id)
    local ground = self.grounds[id]
    if not ground then
        return false
    end
    return ground:is_unveil()
end

function GroundComponent:is_block_finish(ground_id, block_id)
    local ground = self.grounds[ground_id]
    if not ground then
        return false
    end
    return ground:is_block_finish(block_id)
end

function GroundComponent:is_ground_unlock(id)
    return self.grounds[id]
end

function GroundComponent:is_block_unlock(ground_id, block_id)
    if ground_id == nil then
        local block_conf = block_db:find_one(block_id)
        if block_conf == nil then
            return false
        end
        ground_id = block_conf.ground_id
    end

    local ground = self.grounds[ground_id]
    if not ground then
        return false
    end
    return ground:is_block_unlock(block_id)
end

--解锁地块
function GroundComponent:unlock_block(player, ground_id, block_id, time, init)
    local ground = self.grounds[ground_id]
    if init then
        ground:set_blocks_field(block_id, time)
    else
        ground:save_blocks_field(block_id, time)
    end
    if player then
        self:broadcast_message("NID_BUILDING_BLOCK_NTF",
            { master = player:get_id(), part_sync = true, blocks = { [block_id] = time } })
    end
end

--解锁默认地块
function GroundComponent:unlock_default_block(player, ground_id)
    local conf = object_fty:find_ground(ground_id)
    if not conf then
        log_err("[GroundComponent][unlock_default_block] ground({}) not found", ground_id)
        return false
    end
    local rewards = {}
    for _, block_id in pairs(conf.default_block or {}) do
        self:unlock_block(player, ground_id, block_id, 0, true)
        local block_conf = block_db:find_one(block_id)
        for item_id, num in pairs(block_conf.rewards) do
            rewards[item_id] = (rewards[item_id] or 0) + num
        end
    end
    if player then
        --发放奖励
        local drops = { drop = rewards, reason = OBTAIN_GROUND }
        local code = player:execute_drop(drops, true)
        if qfailed(code) then
            log_err("[GroundComponent][unlock_default_block] allot failed! player_id:{} drop:{}", player:get_id(), drops)
            return false, code
        end
    end
    return true
end

--获取地皮
function GroundComponent:get_ground(ground_id)
    return self.grounds[ground_id]
end

--是否城镇地皮
function GroundComponent:is_town_ground(ground_id)
    local ground = self.grounds[ground_id]
    if ground then
        return ground:get_prototype().type == 2
    end
end

--同步地皮
function GroundComponent:sync_ground_datas(player)
    player:send("NID_BUILDING_GROUND_NTF", {master = player.id, grounds = self:pack2client() })
    log_debug("[GroundComponent][sync_ground_datas] NID_BUILDING_GROUND_NTF:{}", self:pack2client())
end

function GroundComponent:pack2client()
    local grounds = {}
    for ground_id, ground in pairs(self.grounds) do
        grounds[ground_id] = ground:pack2client()
    end
    log_debug("[GroundComponent][pack2client] grounds:{}", grounds)
    return grounds
end
return GroundComponent
