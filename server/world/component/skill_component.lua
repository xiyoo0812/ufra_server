--skill_component.lua
local mmin              = math.min
local mfloor            = math.floor
local mmax              = qmath.max
local log_warn          = logger.warn
local tinsert           = table.insert
local judge_dis         = qmath.judge_dis

local CDLoading         = import("world/skill/cd_loading.lua")
local nav_geometry      = import('common/nav_geometry.lua')

local object_fty        = quanta.get("object_fty")
local config_mgr        = quanta.get("config_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")
local event_mgr     	= quanta.get("event_mgr")
local newbee_mgr        = quanta.get("newbee_mgr")

local utility_db        = config_mgr:get_table("utility")
local equip_db          = config_mgr:init_table("equip", "id")

local COM_CDTIME        = utility_db:find_integer("value", "common_cd_time")

local AttrID            = enum("AttrID")
local MRT_DAMAGE        = quanta.enum("MeleeResultType", "DAMAGE")
local MRT_CRITICAL      = quanta.enum("MeleeResultType", "CRITICAL")
local STATE_NORMAL      = quanta.enum("CombatState", "NORMAL")
local STATE_COMBAT      = quanta.enum("CombatState", "COMBAT")
local SKL_AVAILABLE     = quanta.enum("SkillStatus", "AVAILABLE")
local SKL_UNAVAILABLE   = quanta.enum("SkillStatus", "UNAVAILABLE")
local SKL_USE_COMPLETED = quanta.enum("SkillStatus", "USE_COMPLETED")
local STATE_DYING       = quanta.enum("CombatState", "DYING")
local HIT_NONE          = quanta.enum("BeHitAction", "NONE")
local HIT_NORMAL        = quanta.enum("BeHitAction", "NORMAL")
local HIT_REPEL         = quanta.enum("BeHitAction", "REPEL")
local HIT_FLY           = quanta.enum("BeHitAction", "FLY")
local ST_DEFAULT        = quanta.enum("SkillType", "Default")
local ST_WEAPON         = quanta.enum("SkillType", "Weapon")
local ST_COLLECTION     = quanta.enum("SkillType", "Collection")
local ST_KICK           = quanta.enum("SkillType", "Kick")
local ST_PICKUP         = quanta.enum("SkillType", "PickUp")

local FRAME_SUCCESS     = protobuf_mgr:error_code("FRAME_SUCCESS")
local FRAME_PARAMS      = protobuf_mgr:error_code("FRAME_PARAMS")
local SKILL_IS_IN_CDING = protobuf_mgr:error_code("SKILL_IS_IN_CDING")

local SkillComponent = mixin()
local prop = property(SkillComponent)
prop:reader("loading", nil)              -- loading
prop:reader("cd_common", 0)              -- 公共cd
prop:reader("cd_skills", {})             -- 技能cd
prop:reader("skill_set", {})             -- (当前可用的)技能集合
prop:reader("hit_actions", {})           -- 受击动作
prop:reader("running_skill_group", 0)    -- 当前正在释放的技能组
prop:reader("running_skill_id", 0)       -- 当前正在释放的技能id
prop:reader("running_skill_type", -1)    -- 当前正在释放的技能类型
prop:reader("hit_timing_ms", 0)          -- 受击效果结束时间
prop:reader("hit_moving_ms", 0)          -- 受击击飞结束时间
prop:reader("hit_type_key_map", {})      -- 受击类型映射
prop:reader("skill_type_map", {})        -- 技能类型映射

function SkillComponent:__init()
    self.loading = CDLoading()
    self.hit_type_key_map = {
        [HIT_NORMAL] = "normal",
        [HIT_REPEL] = "repel",
        [HIT_FLY] = "fly",
    }
    self.skill_type_map = {
        [ST_DEFAULT] = "melee",
        [ST_COLLECTION] = "collection",
        [ST_KICK] = "kick",
        [ST_PICKUP] = "pickup",
    }
end

--cd校验
function SkillComponent:check_cding(skill_id)
    local now_ms = quanta.now_ms
    if not skill_id then
        return now_ms >= self.cd_common
    end
    local skill_cd = self.cd_skills[skill_id] or 0
    return now_ms >= skill_cd
end

--检查距离是否满足
function SkillComponent:check_distance(x, z, r)
    --计算平面距离是否阈值范围内
    return judge_dis(self:get_pos_x(), self:get_pos_z() , x, z, r)
end

--cd校验
function SkillComponent:begin_cding(skill_id)
    local now_ms = quanta.now_ms
    if not skill_id then
        self.cd_common = now_ms + COM_CDTIME
        return
    end
    self.cd_skills[skill_id] = now_ms + COM_CDTIME
end

--开始技能读条
function SkillComponent:begin_loading(time, breakin, callback)
    if self.loading:get_timer_id() then
        return false
    end
    self.loading:startup(callback, time, breakin)
    return true
end

--结束技能读条
function SkillComponent:stop_loading(active)
    return self.loading:stop(active)
end

---判断技能数据是否正确
---@return Skill
function SkillComponent:check_skill(skill_data)
    if skill_data == nil then
        return nil
    end
    -- 当前装备属性
    local equip_attr = skill_data.equip_attr
    if not equip_attr or equip_attr == 0 or not self.skill_set[equip_attr] then
        -- 空手武器
        equip_attr = -1
    end
    -- 技能id
    local skill_id = skill_data.skill_id
    local skill_info = self.skill_set[equip_attr]
    local skill = (skill_info['skills'] or {})[skill_id]
    -- 普通技能判断
    if equip_attr == -1 then
        local skill_type = self.skill_type_map[skill_data.skill_type]
        skill = (skill_info['skills'][skill_type] or {})[skill_id]
    end
    if skill then
        return skill
    end
    -- 主技能判断
    if skill_info.main_skill and skill_info.main_skill.skill_id == skill_id then
        return skill_info.main_skill
    end
    -- SP技能判断
    if skill_info.sp_skill and skill_info.sp_skill.skill_id == skill_id then
        return skill_info.sp_skill
    end
end

---技能伤害
---@param skill Skill
function SkillComponent:skill_damage(skill, target_entity, param_id)
    -- 技能伤害率
    local skill_damage_ratio = skill:get_damage_ratio(param_id)
    -- 血量
    local target_hp = target_entity:get_hp()
    local min_hp = 0 -- 目标最低血量
    if target_entity:is_player() or target_entity:is_npc() then
        min_hp = 1 -- player、npc最低血量为1(进入重伤状态)
    end
    local cutdown = skill:get_toughness_cutdown(param_id)
    local be_hit_type = skill:get_be_hit_type(param_id)
    local is_fractured = target_entity:toughness_cutdown(cutdown)-- 受击者-韧性削减
    if not is_fractured then
        be_hit_type = HIT_NONE
    end
    local damage, is_critical = self:combat_damage(target_entity:get_defence(), target_entity:get_level(), skill_damage_ratio)
    -- 计算新的血量(最低为0)
    local new_hp = mmax(0, target_hp - damage)
    -- 重伤判定
    if new_hp <= min_hp and min_hp > 0 then
        new_hp = min_hp
        self:target_dying_check(target_entity)
    end
    -- 效果类型
    local melee_type = MRT_DAMAGE
    if is_critical then
        melee_type = MRT_CRITICAL
    end
    return melee_type, damage, new_hp, be_hit_type
end

---重伤判定
function SkillComponent:target_dying_check(target_entity)
    if self:is_monster() and target_entity:is_player() then
        event_mgr:notify_trigger("on_battle_end", target_entity, 2, self:get_proto_id())
    end
    -- 设置重伤状态
    if target_entity:get_combat_state() == STATE_DYING then
        return
    end
    target_entity:set_combat_state(STATE_DYING)
    local dying_time = 10
    if target_entity:is_npc() then
        dying_time = 2
    end
    target_entity:set_dying_timing(dying_time) -- 重伤倒计时(暂时为固定的10s)
    -- 仇恨移除
    local scene = self:get_scene()
    if not scene then
        return
    end
    -- 实体重伤后，将其从ai实体的仇恨列表中移除
    local target_id = target_entity:get_id()
    for ai_entity_id, _ in pairs(target_entity.ai_hated_list or {}) do
        local ai_entity = scene:get_entity(ai_entity_id)
        if ai_entity and ai_entity.enemy_leave then
            ai_entity:enemy_leave(target_id)
        end
    end
end

---添加可用技能
function SkillComponent:add_skill(equip_attr, equip_id)
    local equip_conf = equip_db:find_one(equip_id)
    if equip_conf == nil then
        return
    end
    if self.skill_set[equip_attr] == nil then
        self.skill_set[equip_attr] = {
            is_default = false,
            equip_id = equip_id,
            skill_ids = equip_conf.skill,
            skills = {}
        }
    end
    for _, skill_id in ipairs(equip_conf.skill or {}) do
        -- 普通连招技能
        local skill = object_fty:create_skill(skill_id, nil, equip_attr, equip_id, self)
        self.skill_set[equip_attr]['skills'][skill_id] = skill
    end
    if equip_conf.main_skill then
        -- 主技能
        local skill = object_fty:create_skill(equip_conf.main_skill, nil, equip_attr, equip_id, self)
        self.skill_set[equip_attr]['main_skill'] = skill
    end
    if equip_conf.sp_skill then
        -- SP技能
        local skill = object_fty:create_skill(equip_conf.sp_skill, nil, equip_attr, equip_id, self)
        self.skill_set[equip_attr]['sp_skill'] = skill
    end
end

---移除可用技能
function SkillComponent:remove_skill(equip_attr, equip_id)
    self.skill_set[equip_attr] = nil
end

---初始化加载默认(空手)技能
function SkillComponent:init_default_skills(default_skills)
    local skill_ids = default_skills['melee'] ~= nil and default_skills or {melee = default_skills}
    self.skill_set[-1] = {
        is_default = true,
        equip_id = -1,
        skill_ids = skill_ids,
        skills = {},
    }
    for skill_type, skills in pairs(skill_ids) do
        if not self.skill_set[-1]['skills'][skill_type] then
            self.skill_set[-1]['skills'][skill_type] = {}
        end
        for _, skill_id in ipairs(skills) do
            local skill = object_fty:create_skill(skill_id, nil, -1, -1, self, skill_type)
            self.skill_set[-1]['skills'][skill_type][skill_id] = skill
        end
    end
end

---初始化受击动作
function SkillComponent:init_hit_actions(hit_action)
    self.hit_actions = hit_action or {}
end

function SkillComponent:get_hit_config(hit_bype)
    local hit_type_key = self.hit_type_key_map[hit_bype]
    if hit_type_key ~= nil then
        return self.hit_actions[hit_type_key]
    end
end

---切换(装载/卸载)装备技能
---@param equip_id integer 装备属性
---@param value integer 装备对应的item.Id
function SkillComponent:toggle_equip_skills(equip_id, equip_attr)
    if equip_id > 0 then
        -- 装载装备-加载技能
        self:add_skill(equip_attr, equip_id)
    else
        -- 卸载装备-移除技能
        self:remove_skill(equip_attr, equip_id)
    end
end

---加载全部装备技能
function SkillComponent:_online()
    newbee_mgr:init_skills(self)
    newbee_mgr:init_hit_actions(self)
    self:watch_attr(self, AttrID.ATTR_SHOVEL, "toggle_equip_skills")
    self:watch_attr(self, AttrID.ATTR_WEAPON, "toggle_equip_skills")
    self:watch_attr(self, AttrID.ATTR_PICJAXE, "toggle_equip_skills")
    self:watch_attr(self, AttrID.ATTR_HATCHET, "toggle_equip_skills")
    self:toggle_equip_skills(self:get_attr(AttrID.ATTR_WEAPON), AttrID.ATTR_WEAPON)
    self:toggle_equip_skills(self:get_attr(AttrID.ATTR_SHOVEL), AttrID.ATTR_SHOVEL)
    self:toggle_equip_skills(self:get_attr(AttrID.ATTR_PICJAXE), AttrID.ATTR_PICJAXE)
    self:toggle_equip_skills(self:get_attr(AttrID.ATTR_HATCHET), AttrID.ATTR_HATCHET)
    return true
end

function SkillComponent:_dead()
    self:terminate_running_skill()
end

---开始-放技能
---@param equip_id integer 技能所属装备id
---@param skill_id integer 技能id
function SkillComponent:start_use_skill(equip_id, skill_id, skill_type)
    local skills = self.skill_set[equip_id]["skills"]
    ---@type Skill
    local skill = (self.skill_set[equip_id].is_default and skills[self.skill_type_map[skill_type]] or skills)[skill_id]
    if not skill then
        log_warn('[SkillComponent][start_use_skill] skill is nil')
        return false
    end
    if not skill:check_cd(self.is_ai_entity) then
        log_warn('[SkillComponent][start_use_skill] skill cd check failed')
        return false
    end
    skill:start_use()
    if self.is_ai_entity then
        self:change_skill_available(SKL_UNAVAILABLE)
    end
    return true
end

---打断-放技能
function SkillComponent:break_use_skill(skill_break_data)
    ---@type Skill
    local break_skill = self:check_skill(skill_break_data)
    if not break_skill then
        return false
    end
    self.running_skill_group = 0
    self.running_skill_id = 0
    self.running_skill_type = -1
    self.hit_timing_ms = 0
    self.hit_moving_ms = 0
    break_skill:break_use()
    if self.is_ai_entity then
        self:change_skill_available(SKL_USE_COMPLETED)
    end
    return true
end

---技能释放完成通知
function SkillComponent:completed_use_skill(equip_id, skill_id, skill_type)
    local skills = self.skill_set[equip_id]["skills"]
    ---@type Skill
    local skill = (self.skill_set[equip_id].is_default and skills[skill_type] or skills)[skill_id]
    if not skill then
        log_warn('[SkillComponent][completed_use_skill] skill is nil')
        return false
    end
    self.running_skill_group = 0
    self.running_skill_id = 0
    self.running_skill_type = -1
    self.hit_timing_ms = 0
    self.hit_moving_ms = 0
    if self.is_ai_entity then
        self:change_skill_available(SKL_USE_COMPLETED)
    end
    return true
end

---终止正在运行的技能
function SkillComponent:terminate_running_skill()
    ---@type Skill
    local running_skill = self:get_running_skill()
    if running_skill then
        running_skill:terminate()
    end
    self.running_skill_group = 0
    self.running_skill_id = 0
    self.running_skill_type = -1
    self.hit_timing_ms = 0
    self.hit_moving_ms = 0
end

function SkillComponent:get_running_skill()
    local equip_attr = self.running_skill_group
    local skill_id = self.running_skill_id
    local skill_type = self.running_skill_type
    if skill_id == 0 or skill_type == -1 then
        return nil
    end
    local skill_data = { equip_attr = equip_attr, skill_id = skill_id, skill_type = skill_type }
    ---@type Skill
    local skill = self:check_skill(skill_data)
    if skill and skill:get_start_ms() + 1000 * skill:get_duration() < quanta.now_ms then
        self.running_skill_group = 0
        self.running_skill_id = 0
        self.running_skill_type = -1
        self.hit_timing_ms = 0
        self.hit_moving_ms = 0
        skill = nil
    end
    return skill
end

---技能可用通知
function SkillComponent:available_use_skill(equip_id, skill_id, skill_type)
    local skills = self.skill_set[equip_id]["skills"]
    ---@type Skill
    local skill = (self.skill_set[equip_id].is_default and skills[skill_type] or skills)[skill_id]
    if not skill then
        log_warn('[SkillComponent][available_use_skill] skill is nil')
        return false
    end
    if self.is_ai_entity then
        self:change_skill_available(SKL_AVAILABLE)
    end
    return true
end

function SkillComponent:on_action_melee_target(scene, skill, target_id, param_id)
    -- 计算伤害
    ---@type AiComponent
    local target_entity = scene:get_entity(target_id)
    if target_entity == nil or self:faction_is_friendly(target_entity) or target_entity:is_release() or target_entity:get_combat_state() == STATE_DYING then
        -- 如果 目标不能存在, 或阵营关系为友好，或已死亡, 或重伤，则攻击无效
        return nil
    end
    local melee_type, damage, new_hp, be_hit_type = self:skill_damage(skill, target_entity, param_id)
    if not (melee_type ~= nil and damage ~= nil and new_hp ~= nil) then
        return nil
    end
    -- 伤害结果(初始写明全部字段，便于排错)
    local result_item = {
        ['entity_id'] = target_id,
        ['damage_type'] = melee_type,
        ['value'] = damage,
        ['hit_action'] = be_hit_type,
        ['hit_duration'] = 0,
        ['hit_move_duration'] = 0,
        ['hit_pos'] = nil,
    }
    if target_entity.hit_moving_ms > quanta.now_ms then
        -- 还处于击退效果时间内，则跳过
        result_item['hit_action'] = HIT_NONE
    end
    -- 只有包含Ai组件的Entity才有heate_list
    local target_is_ai = (self:faction_is_normal(target_entity) or self:faction_is_hostile(target_entity)) and target_entity.is_ai_entity
    self:melee_hit_effect(target_entity, result_item)
    if target_is_ai then
        -- 添加仇恨列表
        target_entity:on_assaulted(self, damage, result_item['hit_duration'], result_item['hit_pos'])
    end
    -- 更新血量
    target_entity:set_hp(new_hp)
    target_entity:check_sp_change(false)
    -- log_debug('[SkillComponent]action_melee_damage(): self_id: %s, target_id: %s, damage: %s, new_hp: %s', self:get_id(), target_id, damage, new_hp)
    self:monster_dead_check(target_entity, target_id, new_hp)
    return result_item
end

function SkillComponent:melee_hit_effect(target_entity, result_item)
    if result_item['hit_action'] == HIT_NONE then
        -- 还未破韧
        return
    end
    local hit_config = target_entity:get_hit_config(result_item['hit_action'])
    if not hit_config then
        log_warn('[SkillComponent]melee_hit_effect(): hit_config is nil, hit_type=%s', result_item['hit_action'])
        return
    end
    -- 打断技能
    ---@type Skill
    local target_running_skill = target_entity:get_running_skill()
    local is_super_armor = false -- 是否霸体
    local now_ms = quanta.now_ms
    if target_running_skill ~= nil then
        -- 韧性(霸体)配置
        local running_toughness_param = target_running_skill:get_toughness_param()
        -- 判断技能已运行的相对时间
        local skill_running_ms = (now_ms - target_running_skill:get_start_ms()) / 1000
        -- 判断霸体区间
        for _, rtp in pairs(running_toughness_param) do
            if rtp['start'] < skill_running_ms and rtp['duration'] > skill_running_ms then
                is_super_armor = true
                break
            end
        end
        if not is_super_armor then
            target_entity:break_use_skill({equip_attr=target_running_skill.equip_attr, skill_id=target_running_skill.skill_id,})
        end
    end
    if is_super_armor then
        -- 霸体期间无受击效果
        result_item['hit_action'] = HIT_NONE
        return
    end
    result_item['hit_duration'] = hit_config['duration']
    result_item['hit_move_duration'] = hit_config['move_duration']
    target_entity.hit_timing_ms = now_ms + result_item['hit_duration']
    target_entity.hit_moving_ms = now_ms + result_item['hit_move_duration']
    local target_pos = {x = target_entity:get_pos_x(), y = target_entity:get_pos_y(), z = target_entity:get_pos_z()}
    local self_pos = {x = self:get_pos_x(), y = self:get_pos_y(), z = self:get_pos_z()}
    local dir = nav_geometry.calc_dir(self_pos, target_pos)
    -- 受击移动距离
    local hit_distance = hit_config['distance']
    -- 受击移动位置
    local hit_pos = {
        x = mfloor(target_pos.x + hit_distance * dir.x),
        y = target_pos.y, -- 受击不改变y轴
        z = mfloor(target_pos.z + hit_distance * dir.z),
    }
    target_entity:set_pos_x(hit_pos.x)
    target_entity:set_pos_y(hit_pos.y)
    target_entity:set_pos_z(hit_pos.z)
    result_item['hit_pos'] = hit_pos
end

function SkillComponent:action_melee_damage(scene, skill, targets, param_id)
    local melee_result = {}
    local has_target = false
    for _, target_id in pairs(targets) do
        local result_item = self:on_action_melee_target(scene, skill, target_id, param_id)
        if result_item then
            has_target = true
            tinsert(melee_result, result_item)
        end
    end
    if has_target then
        self:check_sp_change(true)
    end
    return melee_result
end

---怪物死亡检测
function SkillComponent:monster_dead_check(target_entity, target_id, new_hp)
    if not target_entity:is_monster() then
        return
    end
    local is_dead = target_entity:get_hp() <= 0
    if is_dead then
        -- 怪物死亡移除关注
        if self.attent_ai_entities ~= nil then
            local scene = self:get_scene()
            local scene_id = scene:get_id()
            self:remove_attent(scene_id, target_id)
        end
        -- 怪物死亡
        target_entity:dead(self)
        -- 怪物死亡后修改玩家战斗状态
        if self.ai_hated_list then
            self:on_hated_change(target_id, false)
            -- 如果处于战斗状态，则需要修改为非战斗状态
            if not next(self.ai_hated_list) and self:get_combat_state() == STATE_COMBAT then
                self:set_combat_state(STATE_NORMAL)
            end
        end
        return
    elseif self:get_combat_state() == STATE_NORMAL then
        -- 如果攻击了怪物，且怪物没死，则进入战斗状态
        self:set_combat_state(STATE_COMBAT)
    end
end

---释放技能(近战)
function SkillComponent:on_action_melee(param_melee)
    local result = {
        error_code = FRAME_SUCCESS,
    }
    -- 使用技能数据
    local skill_use_data = param_melee.skill_use
    -- 打断技能数据
    local skill_break_data = param_melee.skill_break
    local scene = self:get_scene()
    if not scene then
        result.error_code = FRAME_PARAMS
        return result
    end
    -- 打断技能
    self:break_use_skill(skill_break_data)
    -- 释放技能
    if skill_use_data == nil then
        -- 技能释放过程中，可能存在没有技能额外数据的情况，仅做同步
        return result
    end
    -- 检查技能
    ---@type Skill
    local skill = self:check_skill(skill_use_data)
    if not skill then
        result.error_code = FRAME_PARAMS
        return result
    end
    -- 仅检查战斗技能(空手和武器)
    if not (skill_use_data.skill_type == ST_DEFAULT or skill_use_data.skill_type == ST_WEAPON) then
        return result
    end
    -- timeline 为0表示开始放技能
    local is_start = param_melee.timeline == 0
    if is_start then
        if not self:check_skill_cd_sp_cost(skill) then
            result.error_code = SKILL_IS_IN_CDING
            skill:break_use()
            return result
        end
        skill:set_start_ms(quanta.now_ms)
    end
    -- 伤害参数有效性判断
    local param_id = skill_use_data.target_event_id
    if not skill:check_damage_param_id(param_id) then
        result.error_code = FRAME_PARAMS
        return result
    end
    -- 技能伤害
    -- 攻击目标
    local melee_result = self:action_melee_damage(scene, skill, skill_use_data.targets, param_id)
    result['melee_result'] = melee_result
    if is_start then
        self.running_skill_group = skill_use_data.equip_attr
        self.running_skill_id = skill_use_data.skill_id
        self.running_skill_type = skill_use_data.skill_type
    end
    return result
end

---检查技能cd和sp消耗
---@param skill Skill 技能
---@return boolean 是否能释放
function SkillComponent:check_skill_cd_sp_cost(skill)
    if not skill:check_cd(self.is_ai_entity) then
        return false
    end
    -- 仅NPC和Player拥有SP
    if not (self:is_npc() or self:is_player()) then
        return true
    end
    if skill:is_need_sp() then
        if self:get_sp() < self:get_sp_max() then
            return false
        end
        -- SP技能释放将会清空SP
        self:set_sp(0)
    end
    return true
end

---检查sp变化
---@param is_attacker boolean 是否是攻击者
function SkillComponent:check_sp_change(is_attacker)
    if not (self:is_player() or self:is_npc()) then
        return
    end
    local hp = self:get_hp()
    if hp == 1 then
        -- 生命值为1表示重伤, 则清空sp
        self:set_sp(0)
        return
    end
    local sp_value = self:get_sp()
    local sp_max = self:get_sp_max()
    local sp_rate = self:get_sp_rate() / 100
    local sp_increment = 0
    if is_attacker then
        sp_increment = sp_increment + self:get_sp_attack()
    else
        sp_increment = sp_increment + self:get_sp_hit()
    end
    sp_value = sp_value + mfloor(sp_increment * sp_rate)
    sp_value = mmin(sp_value, sp_max)
    self:set_sp(sp_value)
end

return SkillComponent
