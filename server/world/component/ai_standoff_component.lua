--ai_standoff_component.lua
---@class AiStandoffComponent
local AiStandoffComponent = mixin()
local prop = property(AiStandoffComponent)
prop:reader("can_standoff", false)   -- 是否可对峙
prop:reader("can_standoff_move", false) -- 初始状态-是否可对峙移动
prop:reader("standoff_move_ctx", nil)  -- 对峙移动上下文数据
prop:reader("is_standoff_moving", false) -- 是否在对峙移动
prop:reader("check_standoff_ms", 0) -- 最近一次检测对峙移动时间
prop:reader("standoff_countdown_expire", 0) -- 对峙倒计时(结束时间, 单位: ms)
prop:accessor("standoff_available", false) -- 是否可对峙

function AiStandoffComponent:__init()
end

function AiStandoffComponent:standoff_move_break()
end

function AiStandoffComponent:check_standoff_forward()
end

function AiStandoffComponent:check_standoff_countdown()
end

return AiStandoffComponent
