--prod_storebox_component.lua
--生产储物箱
local log_warn              = logger.warn
local log_debug             = logger.debug
local tinsert               = table.insert
local tunpack               = table.unpack
local qfailed               = quanta.failed
local tsize                 = qtable.size

local object_fty            = quanta.get("object_fty")
local protobuf_mgr          = quanta.get("protobuf_mgr")
local event_mgr             = quanta.get("event_mgr")

local FRAME_SUCCESS         = protobuf_mgr:error_code("FRAME_SUCCESS")
local ACTIVE_LEVEL_ERR      = protobuf_mgr:error_code("WORKS_ACTIVE_LEVEL_ERR")
local DELV_ACTIVED          = protobuf_mgr:error_code("WORKS_PRODUCE_DELV_ACTIVED")
local ITEM_IS_FULL          = protobuf_mgr:error_code("PACKET_ITEM_IS_FULL")
local OBTAIN_PRODUCE        = protobuf_mgr:enum("obtain_reason", "NID_OBTAIN_PRODUCE")

local ProdStoreboxComponent = mixin()
local dbprop = db_property(ProdStoreboxComponent, "home_produce")
dbprop:store_objects("storeboxs", {}) -- 储物箱
function ProdStoreboxComponent:__init()
end

function ProdStoreboxComponent:_load_db(dbdata)
    log_debug("[ProdStoreboxComponent][_load_db] id:{} storeboxs:{}", self.id, dbdata.storeboxs)
    for id, item in pairs(dbdata.storeboxs or {}) do
        local storbar = object_fty:create_storebox(id)
        storbar:load_data(item)
        self:set_storeboxs_elem(id, storbar)
    end
end

-- 获取储物箱
function ProdStoreboxComponent:get_storebox(product)
    return self.storeboxs[product:get_uuid()]
end

-- 能否解锁配送检查
function ProdStoreboxComponent:check_undelive()
    if self.delive_status == true then
        return DELV_ACTIVED
    end
    if not self.prototype.undelive_lv or self.prototype.level < self.prototype.undelive_lv then
        return ACTIVE_LEVEL_ERR
    end
    return FRAME_SUCCESS
end

-- 检查配送
function ProdStoreboxComponent:check_delive()
    if not self.scene:check_bank() then
        return false
    end
    if self.delive_status == false then
        return false
    end
    if self.delive_enable == false then
        return false
    end
    return true
end

-- 自动配送主产品
function ProdStoreboxComponent:storebox_auto_delive(storebox, items, prod_end)
    -- 配送状态
    local delive = self:check_delive()
    local stash_full = not delive
    local surplus_items = {}
    -- 自动配送
    if items and next(items) then
        if delive then
            -- 先进仓库\背包,剩余的保留在储物箱
            local result = event_mgr:notify_listener("on_execute_produce_drop",
                self, items, OBTAIN_PRODUCE, { bank = true })
            local call_ok, code, last_drops = tunpack(result)
            log_warn(
                "[ProdStoreboxComponent][storebox_auto_delive] on_execute_produce_drop is fail master:{} id:{} call_ok:{} last_drops:{}",
                self.master, self.id, call_ok, last_drops)
            if not call_ok or qfailed(code) then
                if code == ITEM_IS_FULL then
                    -- 将剩余的部分重新存入储物箱
                    surplus_items = self:storebox_putin(storebox, last_drops, prod_end)
                    stash_full = true
                else
                    -- 出现异常,先存到储物箱
                    surplus_items = self:storebox_putin(storebox, items, prod_end)
                    log_warn(
                        "[ProdStoreboxComponent][storebox_auto_delive] on_execute_produce_drop is fail master:{} id:{} call_ok:{} code:{} last_drops:{}",
                        self.master, self.id, call_ok, code, last_drops)
                end
            end
        else
            surplus_items = self:storebox_putin(storebox, items, prod_end)
        end
    end
    -- 根据超出的部分,倒推存入的部分
    local putin_items = {}
    for id,count in pairs(items) do
        local surplus = surplus_items[id] or 0
        if not surplus then
            putin_items[id] = count
        else
            putin_items[id] = count - surplus
        end
    end
    return stash_full,putin_items
end

-- 自动配送额外产品
function ProdStoreboxComponent:storebox_auto_delive_extra(storebox, items)
    -- 配送状态
    local delive = self:check_delive()
    -- 自动配送
    if items and next(items) then
        if delive then
            -- 先进仓库\背包,剩余的保留在储物箱
            local result = event_mgr:notify_listener("on_execute_produce_drop",
                self, items, OBTAIN_PRODUCE, { bank = true })
            local call_ok, code, last_drops = tunpack(result)
            log_warn(
                "[ProdStoreboxComponent][storebox_auto_delive_extra] on_execute_produce_drop is fail master:{} id:{} call_ok:{} last_drops:{}",
                self.master, self.id, call_ok, last_drops)
            if not call_ok or qfailed(code) then
                if code == ITEM_IS_FULL then
                    -- 将剩余的部分重新存入储物箱
                    self:storebox_putin_extra(storebox, last_drops)
                else
                    -- 出现异常,先存到储物箱
                    self:storebox_putin_extra(storebox, items)
                    log_warn(
                        "[ProdStoreboxComponent][storebox_auto_delive_extra] on_execute_produce_drop is fail master:{} id:{} call_ok:{} code:{} last_drops:{}",
                        self.master, self.id, call_ok, code, last_drops)
                end
            end
        else
            self:storebox_putin_extra(storebox, items)
        end
    end
end

-- 分配储物箱
function ProdStoreboxComponent:storebox_allot(product)
    local storebox = self:get_storebox(product)
    if storebox then
        return storebox
    end

    -- 已分配的储物空间
    local store_count = tsize(self.storeboxs)
    local queue_count = self:get_queue_count()
    local last_count = queue_count - store_count
    if last_count <= 0 then
        return nil
    end

    local id = self:storebox_id(product)
    local storbar = object_fty:create_storebox(id)
    storbar:set_prod_id(product:get_prod_id())
    self:save_storeboxs_elem(id, storbar)
    return storbar
end

-- 检测空闲储物箱
function ProdStoreboxComponent:storebox_check_free()
    local store_count = tsize(self.storeboxs)
    local queue_count = self:get_queue_count()
    return queue_count - store_count > 0
end

-- 检测回收
function ProdStoreboxComponent:storebox_check_takeback()
    for _, storebox in pairs(self.storeboxs) do
        if not storebox:check_unbind() then
            return false
        end
    end
    return true
end

-- 检测储物箱是否有物品
function ProdStoreboxComponent:storbox_check_have()
    for _, storebox in pairs(self.storeboxs) do
        if storebox:check_have() then
            return true
        end
    end
    return false
end

-- 解除绑定
function ProdStoreboxComponent:storebox_unbind(storebox)
    if not storebox then
        return
    end
    local id = storebox:get_id()
    local product = self:get_products(id)
    if (not product and storebox:check_unbind()) then
        self:del_storeboxs_elem(id)
    end
end

-- 存入物品
function ProdStoreboxComponent:storebox_putin(storebox, items, prod_end)
    local surplus_items = storebox:putin(items)
    if prod_end then
        event_mgr:notify_listener("on_produce_storebox_putin", self.master, self:get_group(), self.id)
    end
    self:storebox_sync({ storebox })
    return surplus_items
end

-- 存储额外物品
function ProdStoreboxComponent:storebox_putin_extra(storebox, items)
    storebox:putin_extra(items)
    self:storebox_sync({ storebox })
end

-- 同步储物箱
function ProdStoreboxComponent:storebox_sync(storeboxs)
    local sync_datas = { id = self.id, store_boxs = {} }
    for _, storebox in pairs(storeboxs) do
        tinsert(sync_datas.store_boxs, storebox:pack_client())
    end
    self.scene:broadcast_message("NID_PRODUCE_STOREBOX_NTF", sync_datas)
end

-- 储物箱
function ProdStoreboxComponent:pack_storeboxs()
    local storeboxs = {}
    for _, storebox in pairs(self.storeboxs) do
        tinsert(storeboxs, storebox:pack_client())
    end
    return storeboxs
end
return ProdStoreboxComponent
