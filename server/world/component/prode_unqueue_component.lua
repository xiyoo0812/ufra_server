--prod_unqueue_component.lua
--队列解锁
local log_debug            = logger.debug
local tinsert              = table.insert
local tsort                = table.sort
local mmax                 = math.max
local tsize                = qtable.size

local object_fty           = quanta.get("object_fty")
local protobuf_mgr         = quanta.get("protobuf_mgr")

local LEVEL                = protobuf_mgr:enum("queue_untype", "PST_LEVEL")
local PAY                  = protobuf_mgr:enum("queue_untype", "PST_PAY")

local FRAME_SUCCESS        = protobuf_mgr:error_code("FRAME_SUCCESS")
local QUEUE_INVALID        = protobuf_mgr:error_code("WORKS_QUEUE_INVALID")
local QUEUE_ACTIVED        = protobuf_mgr:error_code("WORKS_QUEUE_ACTIVED")
local UNLOCK_LEVEL         = protobuf_mgr:error_code("WORKS_UNLOCK_LEVEL_ERR")

local ProdUnQueueComponent = mixin()
local prop                 = property(ProdUnQueueComponent)
prop:reader("id", 0)
prop:reader("prototype", nil)
prop:reader("unlocks", {})        --解锁列表
prop:reader("unlock_map", {})     --解锁映射
prop:accessor("sync_uqconfs", {}) --同步解锁配置

function ProdUnQueueComponent:__init()
    if self:last_unlock_count() > 0 then
        self:build_unlock_conf()
    end
end

-- 加载db数据
function ProdUnQueueComponent:_load_db()
    log_debug("ProdUnQueueComponent][_load_db] id:{} pay_queues:{}", self.id, self.pay_queues)
    if next(self.pay_queues) then
        self:build_unlock_conf()
    end
end

-- 升级事件
function ProdUnQueueComponent:_on_upgrade(conf)
    self.sync_uqconfs = {}
    for _, unconf in pairs(self.unlock_map) do
        if unconf.type == LEVEL and conf.level >= unconf.unlevel then
            unconf.unlock = true
            tinsert(self.sync_uqconfs, unconf)
        end
    end
    return self.sync_uqconfs
end

-- 剩余解锁数量
function ProdUnQueueComponent:last_unlock_count()
    local count, max_count = self:get_queue_count()
    local last_count = mmax(max_count - count, 0)
    return last_count
end

-- 队列数量
function ProdUnQueueComponent:get_queue_count()
    return self.queue_count + tsize(self.pay_queues), self.max_queue_count
end

-- key值
function ProdUnQueueComponent:get_conf_key(queue_type, queue_id)
    local key = string.format("%s_%s", queue_type, queue_id)
    return key
end

-- 解锁队列消耗
function ProdUnQueueComponent:unlock_queue_cost(queue_id)
    if not self.prototype.pay_queue_costs or not next(self.prototype.pay_queue_costs) then
        return nil
    end
    return self.prototype.pay_queue_costs[queue_id]
end

-- 构建解锁配置
function ProdUnQueueComponent:build_unlock_conf()
    local confs = object_fty:find_utensil_group(self.prototype.group)
    if not confs then
        log_debug("[ProdUnQueueComponent][build_unlock_conf] id:{} proto_id:{} group:{}", self.id, self.proto_id,
            self.prototype.group)
        return
    end
    -- 配置排序
    tsort(confs, function(a, b)
        return a.id < b.id
    end)
    local lv_qid = 0
    local py_qid = 0
    for _, conf in pairs(confs) do
        local level = conf.level
        for id = lv_qid + 1, conf.queue or 0 do
            local key = self:get_conf_key(LEVEL, id)
            local queue = {
                queue_id = id,
                type = LEVEL,
                unlevel = level,
                unlock = self.prototype.level >= level and true or false,
                proto_id = conf.id
            }
            self.unlock_map[key] = queue
            tinsert(self.unlocks, queue)
            lv_qid = id
        end
        for id = py_qid + 1, conf.pay_queue or 0 do
            local key = self:get_conf_key(PAY, id)
            local queue = {
                queue_id = id,
                type = PAY,
                unlevel = level,
                unlock = self.pay_queues[id] and true or false,
                proto_id = conf.id
            }
            self.unlock_map[key] = queue
            tinsert(self.unlocks, queue)
            py_qid = id
        end
    end
end

-- 获取解锁配置
function ProdUnQueueComponent:get_unqueue_conf(queue_type, queue_id)
    local key = self:get_conf_key(queue_type, queue_id)
    local conf = self.unlock_map[key]
    return conf
end

function ProdUnQueueComponent:check_unqueue(queue_id)
    if self:last_unlock_count() == 0 then
        return QUEUE_ACTIVED
    end

    local conf = self:get_unqueue_conf(PAY, queue_id)
    if not conf then
        return QUEUE_INVALID
    end

    if conf.unlock then
        return QUEUE_ACTIVED
    end

    if self.prototype.level < conf.unlevel then
        return UNLOCK_LEVEL
    end
    return FRAME_SUCCESS
end

function ProdUnQueueComponent:unlock_queue(queue_id)
    local conf = self:get_unqueue_conf(PAY, queue_id)
    if not conf then
        return false
    end
    conf.unlock = true
    self:save_pay_queues_field(queue_id, 1)
    self:sync_unlock_conf({ conf })
    self:sync_changed()
    return conf
end

-- 同步解散配置
function ProdUnQueueComponent:sync_unlock_conf(unconfs)
    self.scene:broadcast_message("NID_PRODUCE_UNCONF_NTF", {
        id = self.id,
        unconfs = unconfs
    })
end

-- 解锁配置
function ProdUnQueueComponent:pack_unconfs()
    log_debug("[ProdUnQueueComponent][pack_unconfs] id:{} unlocks:{}", self.id, self.unlocks)
    return self.unlocks
end

return ProdUnQueueComponent
