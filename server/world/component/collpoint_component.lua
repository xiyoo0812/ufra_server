--collpoint_component.lua
local log_debug          = logger.debug
local log_err            = logger.err
local log_info           = logger.info
local config_mgr         = quanta.get("config_mgr")
local event_mgr          = quanta.get("event_mgr")
local collpoint_db       = config_mgr:init_table("collect_points", "area_id")

local CollPointComponent = mixin()

local reprop = db_property(CollPointComponent, "home_collpoints", true)
reprop:store_values("collpoints", {}) --采集点

function CollPointComponent:__init()
end

-- 加载记录
function CollPointComponent:on_db_home_collpoints_load(data)
    log_debug("[CollPointComponent][on_db_home_collpoints_load] data:{}", data)
    if data.collpoints then
        -- 采集点
        self.collpoints = data.collpoints
    end
    -- 初始化默认采集点
    for _, conf in collpoint_db:iterator() do
        if not conf.area_id or conf.area_id == 0 then
            log_debug("[CollPointComponent][on_db_home_collpoints_load] id:{}", conf.id)
            self.collpoints[conf.id] = quanta.now
        end
    end
end

-- 玩家下线
function CollPointComponent:_offline()
    log_debug("[CollPointComponent][_offline] player:{}", self.master)
end

-- 初始化采集点事件
function CollPointComponent:init_collpoint_event()
    if not self.player then
        log_err("[CollPointComponent][init_collpoint_event] player is nil, collpoints:{}",
            self.collpoints)
        return
    end
end

-- 完成任务回调
function CollPointComponent:on_area_finish(area_id)
    log_debug("[CollPointComponent][on_area_finish] player:{} area_id:{}", self.master, area_id)
    for _, conf in collpoint_db:iterator() do
        if conf.area_id == area_id then
            self:active_collpoint(conf.id)
        end
    end
end

-- 激活采集点
function CollPointComponent:active_collpoint(proto_id)
    log_info("[CollPointComponent][active_collpoint] player:{} proto_id:{}", self.master, proto_id)
    if self.collpoints[proto_id] then
        return
    end
    local time = quanta.now
    self:save_collpoints_field(proto_id, time)
    event_mgr:notify_trigger("on_active_collpoint", self.master, self, proto_id)
    local datas = { master = self:get_master(), part_sync = true, collpoints = { [proto_id] = time } }
    self:broadcast_message("NID_COLLECT_POINTS_NTF", datas)
end

-- 检查采集点
function CollPointComponent:check_collpoint(proto_id)
    if self.collpoints[proto_id] then
        return true
    end
    return false
end

return CollPointComponent
