--ai_goap_component.lua
local log_warn           = logger.warn
local mrandom            = math.random
local tunpack            = table.unpack
local mmax               = math.max

local event_mgr          = quanta.get("event_mgr")

---@type nav_geometry
local nav_geometry       = import('common/nav_geometry.lua')
---@type GoapMgr
local goap_mgr           = quanta.get("goap_mgr")
---@type NavMgr
local nav_mgr            = quanta.get("nav_mgr")
---@type MonsterAiFsm
local MonsterAiFsm       = import("world/ai/utils/monster_ai_fsm.lua")
---@type GoapContext
local GoapContext        = import("world/ai/goap/goap_context.lua")

local LOGIC_INTERVALS    = { 50, 550} -- 逻辑计时器间隔(单位: ms)

---@class AiGoapComponent
---@field fsm MonsterAiFsm
---@field plan GoapPlan
---@field is_activated boolean
---@field behavior_duration number
---@field is_silence boolean
---@field ai_logic_update_ms number
---@field ai_config MonsterAiConfig
---@field ai_config_base table
---@field ai_config_states table
local AiGoapComponent = mixin()
local prop = property(AiGoapComponent)
prop:reader("fsm", nil) -- 顶级FSM
prop:reader("plan", nil) -- 当前计划
prop:reader("is_activated", false) -- AI功能是否激活
prop:reader("behavior_duration", 0) -- 行为持续时间
prop:reader("is_silence", false) -- 是否受击沉默中
prop:reader("ai_logic_update_ms", 0) -- ai逻辑最新更新时间(ms)
prop:reader("ai_config", nil)        -- AI配置
prop:reader("ai_config_base", nil)   -- AI基础配置
prop:reader("ai_enabled", false) -- AI启用状态
prop:reader("ai_available", false) -- AI可用状态

function AiGoapComponent:__init()
end

function AiGoapComponent:_setup()
    event_mgr:fire_frame(function()
        self:init_ai_fsm(false)
        self:setup_ai_available()
    end)
end

function AiGoapComponent:setup_ai_available()
    local map_id = self:get_map_id()
    local spawn_pos = self:get_spawn_pos()
    -- 仅可导航场景中的 ai实体 启用ai逻辑
    -- 注: 导入进 recastnavigation 中的 navmesh 左边的的x，与运行时传递的方向相反
    local can_nav_spawn_pos = nav_mgr:can_nav(map_id, {
        x = - spawn_pos.x,
        y = spawn_pos.y,
        z = spawn_pos.z,
    })
    local pos = {
        x = self:get_pos_x(),
        y = self:get_pos_y(),
        z = self:get_pos_z(),
    }
    local can_nav_pos = nav_mgr:can_nav(map_id,  {
        x = - pos.x,
        y = pos.y,
        z = pos.z,
    })
    self.ai_available = map_id ~= nil and can_nav_spawn_pos and can_nav_pos
    if not self.ai_available then
        log_warn('[AiGoapComponent][_setup] entity_id: {}, map_id: {}, can_nav_spawn_pos: {}, can_nav_pos: {}, spawn_pos: {}, nav_pos: {}',
        self:get_id(), map_id, can_nav_spawn_pos, can_nav_pos, spawn_pos, pos)
    end
end

---@param is_reinit boolean 是否重新初始化
function AiGoapComponent:init_ai_fsm(is_reinit)
    if is_reinit then
        if not self:nav_move_break(false) then
            self:standoff_move_break(false)
        end
        self.plan = nil
        self.fsm = nil
    end
    self.ai_config = self:get_ai_config()
    if not self.ai_config then
        log_warn('[AiGoapComponent]init_ai_fsm() failed: ai_config(proto_id={}) is nil', self:get_proto_id())
        return
    end
    self.ai_config_base = self.ai_config.base_config
    local state_duration = mrandom(tunpack(self.ai_config_base.state_duration))
    self.fsm = MonsterAiFsm(self.ai_config.states, state_duration)
    self:set_standoff_available(self.fsm:has_state('standoff'))
end

function AiGoapComponent:has_states()
    if not self.ai_config then
        return false
    end
    return self.ai_config.actions_count > 0
end

function AiGoapComponent:has_actions()
    if not self.ai_config then
        return false
    end
    return self.ai_config.actions_count > 0
end

---@return MonsterAiConfig
function AiGoapComponent:get_ai_config()
    return goap_mgr:get_ai_config(self:get_proto_id())
end

---@return number 速度(单位: cm/ms)
function AiGoapComponent:get_wander_speed()
    -- 1 米每秒   相当于    0.1 厘米/毫秒, 即: 1 m/s  =  0.1 cm/ms, 倍率为 10
    -- 1 厘米每秒 相当于  0.001 厘米/毫秒，即: 1 cm/s = 0.001 cm/ms, 倍率为 1000
    -- 注意: self.speed 的单位会影响需要使用的倍率，需要参见配置表
    return mmax(0.5, self:get_speed()) / 10 -- m/s 转为 cm/ms
    -- return self.speed / 1000 -- cm/s 转为 cm/ms
end

function AiGoapComponent:get_chase_speed()
    -- 1 米每秒   相当于    0.1 厘米/毫秒, 即: 1 m/s  =  0.1 cm/ms, 倍率为 10
    -- 1 厘米每秒 相当于  0.001 厘米/毫秒，即: 1 cm/s = 0.001 cm/ms, 倍率为 1000
    -- 注意: self.speed 的单位会影响需要使用的倍率，需要参见配置表
    -- return mmax(0.5, self.ai_config_base.chase_speed) / 10 -- m/s 转为 cm/ms
    return self.ai_config_base.chase_speed / 1000 -- cm/s 转为 cm/ms
end

--碰撞距离(防止AI实体与Player位置重合)
function AiGoapComponent:get_collision_distance()
    return self.ai_config_base.nearby_enemy_distance / 2
end

function AiGoapComponent:get_standoff_distance()
    return self.ai_config_base.standoff_back_distance
end

function AiGoapComponent:get_standoff_precision()
    return self.ai_config_base.standoff_back_precision
end

function AiGoapComponent:get_standoff_radius()
    return self.ai_config_base.standoff_move_radius
end

function AiGoapComponent:get_can_patrol()
    return ( -- 是否可巡逻
        self.ai_config_base.patrol_type ~= nil and ( -- 配置了巡逻类型
            self.ai_config_base.patrol_type == 1 or self.ai_config_base.patrol_type == 2
        )
    ) and ( -- 巡逻点不为空
        self.ai_config_base.patrol_path ~= nil and #self.ai_config_base.patrol_path > 0
    )
end

function AiGoapComponent:is_attaching()
    return nil ~= self.attach_token
end

---获取当前状态
---@return GoapContext
function AiGoapComponent:get_context()
    local entity_id = self:get_id()
    local debug_info = {}
    local is_standoff_available = self:is_standoff_available()
    if self:is_attaching() then
        debug_info = {
            ['hp'] = self:get_hp(),
            ['hp_max'] = self:get_hp_max(),
            ['is_skill_available'] = self.is_skill_available,
            ['is_standoff_moving'] = self.is_standoff_moving, -- 是否在对峙移动中(后退或横向移动)
            ['ai_enabled'] = self.ai_enabled,
            ['has_states'] = self:has_states(),
            ['has_actions'] = self:has_actions(),
            ['ai_available'] = self.ai_available,
            ['standoff_available'] = is_standoff_available,
            ['standoff_countdown_expire'] = tostring(self.standoff_countdown_expire),
            ['now_ms'] = tostring(quanta.now_ms),
            ['fsm_running'] = self.fsm:is_running(),
            ['is_moving'] = self.is_moving,
            ['can_patrol'] = self:get_can_patrol(),
            ['can_amuse'] = self.ai_config_base.amuse,
        }
    end
    local pos = { x = self:get_pos_x(), y = self:get_pos_y(), z = self:get_pos_z() }
    local owner_spawn = {
        ['distance'] = nav_geometry.calc_distance(pos, self.spawn_pos),
    }
    -- 我与我出生点的距离是否在追击范围(出生点安全范围内)内
    local is_in_chase_scope = owner_spawn['distance'] <= self.ai_config_base.chase_scope
    local is_nearby_spawn = owner_spawn['distance'] <= self.ai_config_base.nearby_spawn_distance
    local owner_entity = {
        ['is_silence'] = self.is_silence, -- 是否 受击被沉默中
        ['state'] = (self.fsm.current or {})['name'],
        ['is_activated'] = self.is_activated,
        ['is_hp_full'] = self:get_hp() == self:get_hp_max(),
        ['can_standoff_horizontal'] = false, -- 是否可以对峙横向移动(退到了对峙距离)
        ['is_skill_using'] = self.is_skill_using, -- 是否正在释放技能
        ['can_use_skill'] = self.is_skill_available and not (self.is_skill_using or self.can_standoff or self.is_silence),
        ['can_wander'] = self.can_wander and self.ai_config_base.wander and self.ai_config_base.wander_radius > 0,
        ['is_in_spawn_scope'] = is_in_chase_scope, -- 是否在出生点安全范围内
        ['is_nearby_spawn'] = is_nearby_spawn, -- 是否接近出生点(即在出生点)
        ['is_nearby_enemy'] = false, -- 是否接近敌人(即在仇恨目标位置, 可直接对其进行攻击的距离)
        ['can_standoff'] = self.can_standoff,
        ['can_standoff_back'] = false, -- 是否可后退移动
        ['is_in_standoff_distance'] = false,
    }
    local owner_enemy = {}
    local hate_entity = self.hate_list:first()
    if hate_entity then
        local scene = self:get_scene()
        local ai_enemy = scene:get_entity(hate_entity.entity_id)
        if ai_enemy then
            owner_enemy['entity_id'] = hate_entity.entity_id
            local self_pos = { x = self:get_pos_x(), y = self:get_pos_y(), z = self:get_pos_z() }
            local ai_pos = { x = ai_enemy:get_pos_x(), y = ai_enemy:get_pos_y(), z = ai_enemy:get_pos_z()}
            owner_enemy['distance'] = nav_geometry.calc_distance(self_pos, ai_pos)
            -- 仇恨目标与我的距离是否在追击范围内
            owner_enemy['is_in_chase_distance'] = owner_enemy['distance'] <= self.ai_config_base.chase_distance
            owner_entity['is_nearby_enemy'] = owner_enemy['distance'] <= self.ai_config_base.nearby_enemy_distance
            -- 对峙检测
            if self.can_standoff then
                local standoff_distance = self:get_standoff_distance() + self:get_collision_distance()
                local standoff_precision = self:get_standoff_precision()
                local sd_min = standoff_distance - standoff_precision
                local sd_max = standoff_distance + standoff_precision
                owner_entity['is_in_standoff_distance'] = owner_enemy['distance'] <= sd_max
                owner_entity['can_standoff_back'] = owner_enemy['distance'] < sd_min
                owner_entity['can_standoff_horizontal'] = (
                    (owner_enemy['distance'] >= sd_min and owner_enemy['distance'] <= sd_max) and
                    (self:get_standoff_radius() > 0) -- 横向移动(绕圆)半径必须大于0
                )
            end
        end
    end
    return GoapContext(entity_id, self.ai_config_base, owner_entity, owner_spawn, owner_enemy, debug_info)
end

function AiGoapComponent:get_plan()
    local result = {}
    if nil ~= self.plan then
        result['is_valid'] = self.plan:is_valid()
        result['progress'] = self.plan.progress
        result['action_flow'] = ''
        if result['is_valid'] then
            result['action_flow'] = self.plan:tostring()
        end
    end
    return result
end

function AiGoapComponent:activate(activated)
    self.is_activated = activated
end

function AiGoapComponent:ai_update()
    if self.fsm == nil then
        log_warn('self.fsm is nil')
        return
    end
    local now_ms = quanta.now_ms
    local diff_ms = now_ms - self.ai_logic_update_ms
    -- Ai逻辑更新最短间隔检查
    local interval = mrandom(LOGIC_INTERVALS[1], LOGIC_INTERVALS[2])
    if diff_ms < interval then
        return
    end
    local context = self:get_context()
    -- 顶层状态机处理
    local run_forward_ok, no_next_state = self.fsm:run_forward(context)
    if run_forward_ok then
        -- 切换状态后，重置plan, 停止移动
        self.plan = nil
        if not self:nav_move_break(true) then
            self:standoff_move_break(true)
        end
        context = self:get_context()
    elseif no_next_state then
        self:hook_attach_goap_no_next_state(context)
        return
    end
    self:hook_attach_goap_change(context)
    -- plan处理
    if self.plan == nil or not self.plan:is_valid() then
        if self.fsm.current == nil then
            log_warn('[AiGoapComponent][ai_update] entity_id={}, self.fsm.current is nil', self:get_id())
            return
        end
        -- 状态目标已满足
        if self.fsm.current:transition_match(context) then
            return
        end
        ---@type GoapPlan
        local result_plan = goap_mgr:plan(self:get_proto_id(), self.fsm.current.name, context)
        -- 判断plan是否规划成功
        if result_plan ~= nil and result_plan:is_valid() then
            self.plan = result_plan
        else
            log_warn('[AiGoapComponent][ai_update] entity_id={}, result_plan is nil or empty, state={}', self:get_id(), self.fsm.current.name)
            self:hook_attach_goap_no_plan(context)
            return
        end
    end
    self:goap_forward(context, self.fsm.current)
    self.ai_logic_update_ms = now_ms
end

---@param context GoapContext
---@param goal GoapState
function AiGoapComponent:goap_forward(context, goal)
    if self.plan:is_running() then
        -- 如果当前plan已完成，则清空
        local is_completed =  self.plan:is_completed(context, goal.enable_goal)
        if is_completed then
            self.plan = nil
            return
        end
        self.plan:forward_action(context, self)
    else
        self.plan:start()
    end
end

function AiGoapComponent:fully_hp()
    self:set_hp(self:get_hp_max())
end

return AiGoapComponent
