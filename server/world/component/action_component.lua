--action_component.lua
local log_warn          = logger.warn
local mdistance         = qmath.distance
local mmin              = qmath.min
local mmax              = qmath.max
local mfloor            = math.floor

local newbee_mgr        = quanta.get("newbee_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")

local FRAME_SUCCESS     = protobuf_mgr:error_code("FRAME_SUCCESS")
local NO_STAMINA        = protobuf_mgr:error_code("SCENE_NO_STAMINA")
local CANT_MOVE_TO      = protobuf_mgr:error_code("SCENE_CANT_MOVE_TO")
local MOVE_OVER_SPEED   = protobuf_mgr:error_code("SCENE_MOVE_OVER_SPEED")
local TIME_DISORDER     = protobuf_mgr:error_code("SCENE_TIME_DISORDER")

local AL_WALK           = quanta.enum("ActionLimit", "WALK")
local AL_RUN            = quanta.enum("ActionLimit", "RUN")
local AL_FASTRUN        = quanta.enum("ActionLimit", "FASTRUN")
local AL_ROLL_MOVE      = quanta.enum("ActionLimit", "ROLL_MOVE")
local AL_ADJUST_MOVE    = quanta.enum("ActionLimit", "ADJUST_MOVE")
local AL_FREE_CLIMB     = quanta.enum("ActionLimit", "FREE_CLIMB")
local AL_PARACHUTE      = quanta.enum("ActionLimit", "PARACHUTE")
local AL_FALLING        = quanta.enum("ActionLimit", "FALLING")

local PS_Stay           = quanta.enum("ParachuteState", "Stay")
local PCS_ClimbStay     = quanta.enum("FreeClimbState", "ClimbStay")
local PCS_ClimbMove     = quanta.enum("FreeClimbState", "ClimbMove")
local PCS_ClimbJump     = quanta.enum("FreeClimbState", "ClimbJump")

local ACTION_ROLL       = quanta.enum("ActionType", "Roll")
local ACTION_FREECLIMB  = quanta.enum("ActionType", "FreeClimb")
local ACTION_PARACHUTE  = quanta.enum("ActionType", "Parachute")

local ActionComponent = mixin()
local prop = property(ActionComponent)
prop:accessor("stamina_recover", nil)        -- 配置-每秒回复的耐力值(N/s)
prop:accessor("stamina_roll", nil)           -- 配置-每次翻滚消耗的耐力值
prop:accessor("stamina_parachute", nil)      -- 配置-滑翔每秒消耗的耐力值(N/s)
prop:accessor("stamina_freeclimb", nil)      -- 配置-自由攀爬每秒消耗的耐力值(N/s)
prop:accessor("stamina_climb_jump", nil)     -- 配置-攀爬中每跳一次消耗的耐力值
prop:reader("cache_safe_pos", nil)           -- 缓存-安全位置
prop:reader('cache_safe_dir_y', nil)         -- 缓存-安全朝向
prop:reader("stamina_can_recover", true)     -- 是否可以回复耐力
prop:accessor("sync_timestamp", 0)           -- 同步时间
prop:accessor('cache_action_type', nil)      -- 缓存-行为类型
prop:accessor('cache_free_climb_state', nil) -- 缓存-自由攀爬行为状态
prop:accessor("stamina_change_ms", 0)        -- 耐力变动(消耗/回复)时间(单位: ms)
prop:accessor("ignore_speed_limit", false) -- 是否忽略速度限制

function ActionComponent:__init()
end

function ActionComponent:apply_safe_position()
    if self.cache_safe_pos == nil then
        return false
    end
    local pos_x, pos_y, pos_z = self:get_pos_x(), self:get_pos_y(), self:get_pos_z()
    local dir_y = self:get_dir_y()
    local cpos_x, cpos_y, cpos_z = self.cache_safe_pos.x, self.cache_safe_pos.y, self.cache_safe_pos.z
    local cdir_y = self.cache_safe_dir_y
    if self.cache_action_type ~= ACTION_FREECLIMB and self.cache_action_type ~= ACTION_PARACHUTE then
        -- 非自由攀爬或滑翔过程, 不处理安全位置
        return false
    end
    -- 如果当前位置和安全位置不一样，则采用安全位置
    if pos_x ~= cpos_x or pos_y ~= cpos_y or pos_z ~= cpos_z or dir_y ~= cdir_y then
        self:set_pos_x(cpos_x)
        self:set_pos_y(cpos_y)
        self:set_pos_z(cpos_z)
        self:set_dir_y(cdir_y)
        return true
    end
    return false
end

function ActionComponent:_online()
    logger.dump('[ActionComponent:_online]')
    newbee_mgr:init_toughness_config(self)
end

function ActionComponent:_offline()
    -- Player离线线时重置同步时间戳, 防止重连后时间戳校验错误
    self.sync_timestamp = -1
end

--更新
function ActionComponent:_update()
    if self:is_npc() then
        -- NPC不处理耐力
        return
    end
    self:stamina_recover_check()
end

--判断是否是消耗耐力的行为
function ActionComponent:is_stamina_cost_action(action_type)
    return action_type == ACTION_ROLL or action_type == ACTION_FREECLIMB or action_type == ACTION_PARACHUTE
end

--耐力恢复检测
function ActionComponent:stamina_recover_check()
    --状态检查
    if not self.stamina_can_recover then
        return
    end
    -- 间隔检查
    local latest_check_ms = self.stamina_change_ms
    local now_ms = quanta.now_ms
    local diff_ms = now_ms - latest_check_ms
    if diff_ms < 1000 then
        -- 未超过1000ms(1秒)
        return
    end
    -- 行为状态检查
    local action_type = self.cache_action_type
    if self:is_stamina_cost_action(action_type) then
        -- 翻滚、自由攀爬、滑翔过程中不恢复耐力
        return
    end
    -- 值检查
    local stamina = self:get_stamina()
    local max_stamina = self:get_stamina_max()
    if stamina >= max_stamina then
        -- 耐力值已满
        return
    end
    -- 回复耐力
    local recover_value = mfloor(diff_ms / 1000) * self:get_stamina_recover()
    local new_stamina = mmin(max_stamina, stamina + recover_value)
    self:set_stamina(new_stamina)
    -- 更新时间记录
    self.stamina_change_ms = now_ms
end

--行为同步-位置限制
function ActionComponent:action_position_limit(entity_id, pos, dir, timestamp, action_limit, speed)
    local scene = self:get_scene()
    local opx = self:get_pos_x()
    local opy = self:get_pos_y()
    local opz = self:get_pos_z()
    -- local ody = self:get_dir_y()
    local error_code = FRAME_SUCCESS
    local valid_pos = nil
    -- 可移动性判断和热区传送
    local is_ok, is_transed = scene:move_entity(self, entity_id, pos.x, pos.z)
    if not is_ok then
        log_warn("[ActionComponent][action_position_limit] entity(%s) move (%s,%s,%s)=>(%s,%s,%s) out of range!", entity_id, opx, opy, opz, pos.x, pos.y, pos.z)
        error_code = CANT_MOVE_TO
        valid_pos = { x = opx, y = opy, z = opz }
        return error_code, valid_pos
    end
    if is_transed then
        -- 如果进行了传送则不在进行后续处理
        return error_code, valid_pos
    end
    -- 不限速状态处理
    --[[
    if self:get_ignore_speed_limit() then
        -- 无视速度限制
        self:change_pos_dir(pos, dir, speed, timestamp)
        return error_code, valid_pos
    end]]
    -- 速度校验
    -- 移动距离(单位: m)
    local dis = mdistance(opx, opz, pos.x, pos.z)
    -- 最大速度限制(cm/s)
    local speed_map = {
        [AL_WALK] = 600,
        [AL_RUN] = 1000,
        [AL_FASTRUN] = 1700,
        [AL_ROLL_MOVE] = 2200,
        [AL_ADJUST_MOVE] = 2700,
        [AL_FREE_CLIMB] = 2800,
        [AL_PARACHUTE] = 3100,
        [AL_FALLING] = 7000,
    }
    local max_speed = speed_map[(action_limit or AL_WALK)]
    -- 验证速度
    local verify_speed = max_speed
    if speed then
        verify_speed = mmin(speed, max_speed)
    end
    -- 非第一次上报时才校验速度(仅有上报时间和速度不持久化，故而每次场景第一次上报时为nil)
    -- 最新记录的上报时间、速度、朝向
    local ots, osp = self.sync_timestamp, self:get_speed()
    if not (ots < 0 or osp < 0) then
        -- 移动时间(单位: ms)
        local dur = timestamp - ots
        if dur <= 0 and dis > 0 then
            -- 时间错误(当前上报时间 必须 大于 上次上报时间)
            error_code = TIME_DISORDER
            return error_code, valid_pos
        end
        -- 真实速度(单位： cm/s)
        -- local real_speed = dis * 1000 / dur
        local real_speed = dis / (dur / 1000)
        -- 验证速度
        verify_speed = mmax(verify_speed, real_speed)
    end
    -- 超速校验(仅Player)
    if self:is_player() and verify_speed > max_speed then
        -- log_warn('[SceneServlet][on_actor_action_req] verify_speed: %s, max_speed: %s', verify_speed, max_speed)
        -- 超速, 需要位置拖回
        error_code = MOVE_OVER_SPEED
        valid_pos = { x = opx, y = opy, z = opz }
        return error_code, valid_pos
    end
    self:change_pos_dir(pos, dir, verify_speed, timestamp)
    return error_code, valid_pos
end

-- 耐力回复状态改变
function ActionComponent:stamina_recover_state_change(action_type)
    if action_type == self.cache_action_type then
        return
    end
    if self:is_stamina_cost_action(action_type) then
        -- 翻滚、自由攀爬、滑翔过程中不恢复耐力
        self.stamina_can_recover = false
        return
    end
    if self:is_stamina_cost_action(self.cache_action_type) then
        self.stamina_can_recover = true
        self.stamina_change_ms = quanta.now_ms
    end
end

function ActionComponent:change_pos_dir(pos, dir, verify_speed, timestamp)
    local scene = self:get_scene()
    self:set_pos_x(pos.x)
    self:set_pos_y(pos.y)
    self:set_pos_z(pos.z)
    -- 上报时间、速度、朝向更新
    self:set_dir_y(dir.y)
    if type(verify_speed) == 'number' then
        self:set_speed(verify_speed)
    end
    self.ync_timestamp = timestamp
    scene:position_changed_notify(self, pos.x, pos.y, pos.z)
end

function ActionComponent:update_cache_safe_position()
    -- 缓存安全位置, 如果滑翔或自由攀爬过程中死亡, 则回到安全位置
    self.cache_safe_pos = { x = self:get_pos_x(), y = self:get_pos_y(), z = self:get_pos_z() }
    self.cache_safe_dir_y = self:get_dir_y()
end

function ActionComponent:clean_cache_safe_position()
    self.cache_safe_pos = nil
    self.cache_safe_dir_y = nil
end

function ActionComponent:update_cache_action(action_type)
    self.cache_action_type = action_type
    -- 非自由攀爬状态下, 清除缓存的自由攀爬状态
    if self.cache_free_climb_state ~= nil and self.cache_action_type ~= ACTION_FREECLIMB then
        self.cache_free_climb_state = nil
    end
end

--行为改变-跳跃
function ActionComponent:on_action_jump(param_jump)
    local result = {
        error_code = FRAME_SUCCESS,
    }
    return result
end

--行为改变-翻滚
function ActionComponent:on_action_roll(param_roll, entity_id)
    local result = {
        error_code = FRAME_SUCCESS,
    }
    if not self:stamina_cost_check_roll(param_roll) then
        result.error_code = NO_STAMINA
    end
    return result
end

--行为改变-自由攀爬
function ActionComponent:on_action_free_climb(param_free_climb, entity_id)
    local result = {
        error_code = FRAME_SUCCESS,
    }
    local is_ok = self:stamina_cost_check_free_climb(param_free_climb)
    if not is_ok then
        result.error_code = NO_STAMINA
        --耐力已耗尽
        self:update_stamina(0)
    end
    -- 更新当前攀爬状态
    self.cache_free_climb_state = param_free_climb.state
    return result
end

--行为改变-滑翔
function ActionComponent:on_action_parachute(param_parachute, entity_id)
    local result = {
        error_code = FRAME_SUCCESS,
    }
    if not self:stamina_cost_check_parachute(param_parachute) then
        result.error_code = NO_STAMINA
        --耐力已耗尽
        self:update_stamina(0)
    end
    return result
end

--消耗耐力
---@return boolean 耐力是否足够
function ActionComponent:cost_stamina(cost_value)
    local stamina = self:get_stamina()
    local new_stamina = stamina - cost_value
    if new_stamina < 0 then
        return false
    end
    self:update_stamina(new_stamina)
    return true
end

---更新耐力属性
function ActionComponent:update_stamina(stamina_value)
    self:set_stamina(stamina_value)
    self.stamina_change_ms = quanta.now_ms
end

-- 耐力消耗检查-翻滚
function ActionComponent:stamina_cost_check_roll(param_roll)
    if not param_roll.is_start then
        return true
    end
    local stamina_roll = self:get_stamina_roll()
    if not self:cost_stamina(stamina_roll) then
        return false
    end
    return true
end

-- 耐力消耗检查-自由攀爬
function ActionComponent:stamina_cost_check_free_climb(param_free_climb)
    local action_type = ACTION_FREECLIMB
    if action_type ~= self.cache_action_type then
        -- 行为变化(即开始自由攀爬时)才检查耐力扣除
        self.stamina_change_ms = quanta.now_ms
        return true
    end
    if param_free_climb.state == PCS_ClimbMove then
        -- 自由攀爬持续期间消耗耐力
        local stamina_freeclimb = self:get_stamina_freeclimb()
        local latest_check_ms = self.stamina_change_ms
        local now_ms = quanta.now_ms
        local diff_ms = now_ms - latest_check_ms
        local rate = diff_ms / 1000
        if rate > 1 / 5 then
            -- 最小更新间隔为1/5秒
            local cost_value = mfloor(rate * stamina_freeclimb)
            if not self:cost_stamina(cost_value) then
                return false
            end
        end
    elseif param_free_climb.state ~= self.cache_free_climb_state and param_free_climb.state == PCS_ClimbJump then
        -- 自由攀爬期间跳跃, 仅当从其他状态进入跳跃状态时才消耗耐力
        local stamina_climb_jump = self:get_stamina_climb_jump()
        if not self:cost_stamina(stamina_climb_jump) then
            return false
        end
    elseif param_free_climb.state == PCS_ClimbStay then
        -- 自由攀爬期间未移动, 不消耗耐力, 仅更新计时
        self.stamina_change_ms = quanta.now_ms
    end
    return true
end

-- 耐力消耗检查-滑翔
function ActionComponent:stamina_cost_check_parachute(param_parachute)
    local action_type = ACTION_PARACHUTE
    if action_type ~= self.cache_action_type then
        -- 行为变化(即开始滑翔时), 更新时间记录
        self.stamina_change_ms = quanta.now_ms
        return true
    end
    if param_parachute.state ~= PS_Stay then
        -- 仅在滑翔持续期间才消耗耐力
        self.stamina_change_ms = quanta.now_ms
        return true
    end
    local stamina_parachute = self:get_stamina_parachute()
    local latest_check_ms = self.stamina_change_ms
    local now_ms = quanta.now_ms
    local diff_ms = now_ms - latest_check_ms
    local rate = diff_ms / 1000
    if rate > 1 / 5 then
        -- 最小更新间隔为1/5秒
        local cost_value = mfloor(rate * stamina_parachute)
        if not self:cost_stamina(cost_value) then
            return false
        end
    end
    return true
end

return ActionComponent
