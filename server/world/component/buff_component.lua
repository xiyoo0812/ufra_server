--buff_component.lua
local tinsert       = table.insert

local object_fty    = quanta.get("object_fty")

local BuffBox       = import("world/entity/buff_box.lua")

local BuffComponent = mixin(BuffBox)

local dbprop = db_property(BuffComponent, "player_buff", true)
dbprop:store_objects("buffs", {})

function BuffComponent:__init()
end

function BuffComponent:on_db_player_buff_load(data)
    if data.player_id then
        local now = quanta.now
        for uuid, buf_data in pairs(data.buffs or {}) do
            local nbuff = object_fty:create_buff(buf_data.buff_id, uuid)
            nbuff:set_expire(buf_data.expire)
            self:set_buffs_elem(uuid, nbuff)
            if nbuff:is_expire(now) then
                self:del_buffs_elem(uuid)
                goto continue
            end
            nbuff:set_layer(buf_data.layer)
            self.all_buffs[uuid] = nbuff
            :: continue ::
        end
    end
    return true
end

function BuffComponent:_update(now)
    BuffBox.update_buff(self, now)
end

function BuffComponent:on_buff_add(uuid, buff)
    self:save_buffs_elem(uuid, buff)
end

function BuffComponent:on_buff_remove(uuid)
    self:del_buffs_elem(uuid)
end

function BuffComponent:sync_buff(buff)
    self:send("NID_SKILL_BUFF_NTF", { uuid = self.id, buff = buff:pack2client()})
    if buff:is_broadcast() then
        self:sight_message("NID_SKILL_BUFF_NTF", { uuid = self.id, buffs = buff:pack2client()})
    end
end

--获取所有buff
function BuffComponent:sync_buffs()
    local sbuffs, bbuffs = {}, {}
    for _, buff in pairs(self.all_buffs) do
        tinsert(sbuffs, buff:pack2client())
        if buff:is_broadcast() then
            tinsert(bbuffs, buff:pack2client())
        end
    end
    self:send("NID_SKILL_BUFFS_NTF", { uuid = self.id, buffs = sbuffs})
    self:sight_message("NID_SKILL_BUFFS_NTF", { uuid = self.id, buffs = bbuffs})
end

return BuffComponent
