--prode_skill_component.lua
--生产buff
local log_err          = logger.err
local log_warn         = logger.warn
local log_debug        = logger.debug
local mttratio         = qmath.ttratio
local mrand            = qmath.rand
local mmax             = math.max
local mfloor           = math.floor

local object_fty       = quanta.get("object_fty")

local ItemType         = enum("ItemType")

local ProdeSkillComponent = mixin()
local prop               = property(ProdeSkillComponent)
prop:accessor("effects", {})

local dbprop = db_property(ProdeSkillComponent, "home_produce")
dbprop:store_values("sat_confs", {}) -- 满意度配置

function ProdeSkillComponent:__init()

end

function ProdeSkillComponent:_load_db(dbdata)
    log_debug("[ProdeSkillComponent][_load_db] id:{} sat_confs:{}", self.id, dbdata.sat_confs)
    -- 满意度配置
    self:set_sat_confs(dbdata.sat_confs or {})
end

-- 更新效果事件
function ProdeSkillComponent:on_update_effects()
    local sat_confs = self.effects.stage_time_per or {}
    log_debug("[ProdBase][on_update_effects] sat_confs:{}", sat_confs)
    for id, value in pairs(sat_confs) do
        if self.sat_confs[id] ~= value then
            self:save_sat_confs_field(id, value)
        end
    end
end

-- 满意度变化事件
function ProdeSkillComponent:on_sat_change()
    if self:get_product_count() == 0 then
        return
    end
    self:update_effects()
end

-- 派驻npc事件
function ProdeSkillComponent:on_send_partner()
    self:on_effects_change()
    self:start_produce()
end

-- 召回npc事件
function ProdeSkillComponent:on_recall_partner()
end

-- 刷新效果
function ProdeSkillComponent:update_effects()
    if not self.building then
        log_warn("[ProdeSkillComponent][update_effects] building is nil master:{} id:{}", self.master, self.id)
        return
    end
    self.building:execute_effect()
    self.building:print_effects()
    self.effects = self.building:pack_effects()
    log_debug("[ProdeSkillComponent][update_effects] begin effects:{}", self.effects)
    self.building:revert_effect()
    log_debug("[ProdeSkillComponent][update_effects] end effects:{}", self.effects)
    self:on_update_effects()
end

-- 获取货币减免
function ProdeSkillComponent:money_derates(cost_per)
    local derates = {}
    for _, effect in pairs(cost_per) do
        for _ = 1, effect.num or 0, 1 do
            if mttratio(effect.hit_per * 100) then
                -- 材料消耗减少的百分比
                for item_id, per in pairs(effect.items or {}) do
                    local conf = object_fty:find_item(item_id)
                    if conf and conf.type == ItemType.MONEY then
                        if not derates[item_id] then
                            derates[item_id] = per
                        else
                            derates[item_id] = derates[item_id] + per
                        end
                    end
                end
            end
        end
    end
    return derates
end

-- 获取配方消耗
function ProdeSkillComponent:formula_cost(formula, prod_count)
    log_debug("[ProdeSkillComponent][formula_cost] begin master:{} id:{} formula:{}-{} prod_count:{}",
        self.master, self.id, formula.id, formula.costs, prod_count)
    --消耗物抵扣百分比(仅针对金币消耗)
    local cost_per = self.effects.cost_per or {}
    -- 物品减免(仅对货币有效)
    local derates = self:money_derates(cost_per)
    -- 完整消耗
    local all_costs = {}
    -- 单轮消耗
    local wheel_costs = {}
    -- 消耗计算
    for id, count in pairs(formula.costs or {}) do
        wheel_costs[id] = count
        -- 货币减免
        local rate = derates[id]
        if rate then
            local derate_count = mfloor(wheel_costs[id] * (rate / 100))
            local item_count = wheel_costs[id] - derate_count
            wheel_costs[id] = mmax(item_count, 0)
        end
        all_costs[id] = wheel_costs[id] * prod_count
    end
    log_debug("[ProdeSkillComponent][formula_cost] end cost_per:{} all_costs:{} wheel_costs:{} derates:{}",
        cost_per, all_costs, wheel_costs, derates)
    return all_costs, wheel_costs
end

-- 获取配方时间
function ProdeSkillComponent:formula_time(formula_time, in_time_per)
    --生产时间缩短百分比
    local time_per = in_time_per and in_time_per or (self.effects.time_per or 0)
    --生产时间缩短(秒)
    local time_num = self.effects.time_num or 0
    --完成进度百分比
    local prog_per = self.effects.prog_per or 0
    -- 时间
    local prod_time = formula_time
    -- 完成时间
    local done_time = 0
    -- 生产时间缩短百分比B
    if time_per ~= 0 then
        prod_time = mfloor(prod_time / (1 + (time_per / 100)))
    end
    --生产时间缩短(秒)
    if time_num ~= 0 then
        prod_time = prod_time - time_num
    end
    --完成进度百分比
    if prog_per ~= 0 then
        done_time = mfloor(prod_time * (prog_per / 100))
    end
    if prod_time - done_time <= 0 then
        prod_time = 1
        done_time = 0
    end
    log_debug(
        "[ProdeSkillComponent][formula_time] master:{} formula_time:{} time_per:{} time_num:{} prog_per:{} prod_time:{} done_time:{}",
        self.master, formula_time, time_per, time_num, prog_per, prod_time, done_time)
    return prod_time, done_time
end

-- 技能效果-系数
function ProdeSkillComponent:skilleff_coeff()
    local rate = 1
    local skill_prod_coeff = object_fty:get_skill_prod_coeff()
    if not skill_prod_coeff then
        log_err("[ProdeSkillComponent][skilleff_coeff] skill_prod_coeff is nil master:{} id:{}", self.master, self.id)
        return rate
    end
    local min_rate = skill_prod_coeff[1] or 0
    local max_rate = skill_prod_coeff[2] or 0
    local num = skill_prod_coeff[3] and (skill_prod_coeff[3] > 0 and skill_prod_coeff[3] or 1) or 1
    for _ = 1, num, 1 do
        rate = rate + mrand(min_rate, max_rate)
    end
    rate = rate / num / 100
    log_debug(
        "[ProdeSkillComponent][skilleff_coeff] master:{} id:{} skill_prod_coeff:{} min_rate:{} max_rate:{} num:{} rate:{}",
        self.master, self.id, skill_prod_coeff, min_rate, max_rate, num, rate)
    return rate
end

-- 技能效果-消耗返还(单轮)
function ProdeSkillComponent:skilleff_costback_single(product, eff_id, effect, costs, items)
    log_debug("[ProdeSkillComponent][skilleff_costback_single] begin effect:{}-{} hit_per:{} costs:{} items:{}",
        eff_id, effect, effect.hit_per, costs, items)
    if mttratio(effect.hit_per * 100) then
        for id, count in pairs(costs) do
            log_debug("[ProdeSkillComponent][skilleff_costback_single] start cost_id:{} cost_count:{} skitem_count:{}",
                id, count, product:get_skitem_count(id))
            -- 公式：消耗数量*返还比例+上次未取整的余数
            local sum_count = mfloor((count * (effect.percent / 100) * 100) + product:get_skitem_count(id))
            local item_count, last_count = mfloor(sum_count // 100), mfloor(sum_count % 100)
            items[id] = (items[id] or 0) + item_count
            product:set_skitem_count(id, last_count)
            log_debug(
                "[ProdeSkillComponent][skilleff_costback_single] over sum_count:{} item_count:{} last_count:{} skitem_count:{}",
                sum_count, item_count, last_count, product:get_skitem_count(id))
        end
    else
        log_debug(
            "[ProdeSkillComponent][skilleff_costback_single] not hit hit_per:{}", effect.hit_per)
    end
    log_debug("[ProdeSkillComponent][skilleff_costback_single] end items:{}", items)
end

-- 技能效果-消耗返还(多轮)
function ProdeSkillComponent:skilleff_costback_multiple(product, eff_id, effect, costs, items, wheel_count)
    log_debug("[ProdeSkillComponent][skilleff_costback_multiple] begin effect:{}-{} costs:{} items:{} wheel_count:{}",
        eff_id, effect, costs, items, wheel_count)
    local rate = self:skilleff_coeff()
    for id, count in pairs(costs) do
        log_debug("------[ProdeSkillComponent][skilleff_costback_multiple] start id:{} count:{} skitem_count:{}", id,
            count,
            product:get_skitem_count(id))
        -- 公式：轮次*效果次数*命中概率*返还比例*消耗数量
        local sum_count = mfloor(((wheel_count * (effect.num or 1) * (effect.hit_per / 100) * ((effect.percent / 100) * count) * 100 +
            product:get_skitem_count(id))) * rate)
        local item_count, last_count = mfloor(sum_count // 100), mfloor(sum_count % 100)
        items[id] = (items[id] or 0) + item_count
        product:set_skitem_count(id, last_count)
        log_debug(
            "------[ProdeSkillComponent][skilleff_costback_multiple] over sum_count:{} item_count:{} last_count:{} skitem_count:{}",
            sum_count, item_count, last_count, product:get_skitem_count(id))
    end
    log_debug("[ProdeSkillComponent][skilleff_costback_multiple] end rate:{} num:{} items:{}",
        rate, effect.num, items)
end

-- 技能效果-消耗返还
function ProdeSkillComponent:skilleff_costback(product, wheel_count, items)
    log_debug(
        "[ProdeSkillComponent][skill_cost_back] begin master:{} id:{} product:{} wheel_count:{} items:{} cost_per:{}",
        self.master, self.id, product:get_id(), wheel_count, items, self.effects.cost_per)
    local costs = product:get_costs()
    if not costs or not next(costs) then
        log_debug("[ProdeSkillComponent][skill_cost_back] costs is nil costs:{}", costs)
        return
    end
    for eff_id, effect in pairs(self.effects.cost_per or {}) do
        log_debug("[ProdeSkillComponent][skilleff_costback] start wheel_count:{} eff_id:{} effect:{} items:{}",
            wheel_count,
            eff_id, effect, items)
        if wheel_count == 1 and effect.num == 1 then
            self:skilleff_costback_single(product, eff_id, effect, costs, items)
        else
            self:skilleff_costback_multiple(product, eff_id, effect, costs, items, wheel_count)
        end
        log_debug("[ProdeSkillComponent][skilleff_costback] over items:{}", items)
    end
    log_debug("[ProdeSkillComponent][skill_cost_back] end items:{}", items)
end

-- 技能效果-额外产出(单轮)
function ProdeSkillComponent:skilleff_extprod_single(product, eff_id, effect, items)
    local prod_id = product:get_prod_id()
    local prod_count = product:get_prod_count()
    log_debug("[ProdeSkillComponent][skilleff_extprod_single] begin effect:{}-{} hit_per:{} prod_id:{} prod_count:{}",
        eff_id, effect, effect.hit_per, prod_id, prod_count)
    if mttratio(effect.hit_per * 100) then
        -- 额外物品
        for id, count in pairs(effect.items) do
            log_debug(
                "[ProdeSkillComponent][skilleff_extprod_single] items start id:{} count:{} item:{} skitem_count:{}",
                id, count, items[id], product:get_skitem_count(id))
            local sum_count = (count * 100) + product:get_skitem_count(id)
            local item_count, last_count = mfloor(sum_count // 100), mfloor(sum_count % 100)
            items[id] = (items[id] or 0) + item_count
            product:set_skitem_count(id, last_count)
            log_debug(
                "[ProdeSkillComponent][skilleff_extprod_single] items over id:{} count:{} item:{} skitem_count:{}",
                id, count, items[id], product:get_skitem_count(id))
        end
        log_debug("[ProdeSkillComponent][skilleff_extprod_single] prod begin item:{} prod:{} skitem_count:{}",
            items, items[prod_id], product:get_skitem_count(prod_id))
        -- 额外产出
        local sum_count = mfloor((prod_count * (effect.percent / 100) * 100) + product:get_skitem_count(prod_id))
        local item_count, last_count = mfloor(sum_count // 100), mfloor(sum_count % 100)
        items[prod_id] = (items[prod_id] or 0) + item_count
        product:set_skitem_count(prod_id, last_count)
        log_debug(
            "[ProdeSkillComponent][skilleff_extprod_single] prod end sum_count:{} item_count:{} last_count:{} prod:{} skitem_count:{}",
            sum_count, item_count, last_count, items[prod_id], product:get_skitem_count(prod_id))
    end
end

-- 技能效果-额外产出(多轮)
function ProdeSkillComponent:skilleff_extprod_multiple(product, eff_id, effect, items, wheel_count)
    local prod_id = product:get_prod_id()
    local prod_count = product:get_prod_count()
    local rate = self:skilleff_coeff()
    log_debug(
        "[ProdeSkillComponent][skilleff_extprod_multiple] begin effect:{}-{} items:{} wheel_count:{} prod_id:{} prod_count:{} rate:{}",
        eff_id, effect, items, wheel_count, prod_id, prod_count, rate)
    -- 额外物品
    for id, count in pairs(effect.items) do
        log_debug("[ProdeSkillComponent][skilleff_extprod_multiple] start id:{} count:{} rate:{} skitem_count:{}", id,
            count, rate, product:get_skitem_count(id))
        local sum_count = mfloor(((wheel_count * count * (effect.num or 1) * (effect.hit_per / 100) * 100) + product:get_skitem_count(id)) *
            rate)
        local item_count, last_count = mfloor(sum_count // 100), mfloor(sum_count % 100)
        items[id] = (items[id] or 0) + item_count
        product:set_skitem_count(prod_id, last_count)
        log_debug(
            "[ProdeSkillComponent][skilleff_extprod_multiple] over sum_count:{} item_count:{} last_count:{} skitem_count:{}",
            sum_count, item_count, last_count, product:get_skitem_count(id))
    end

    -- 额外产出
    local sum_count = (wheel_count * effect.num * (effect.hit_per / 100) * (effect.percent / 100) * prod_count) * 100
    local item_count, last_count = mfloor(sum_count // 100), mfloor(sum_count % 100)
    items[prod_id] = (items[prod_id] or 0) + item_count
    product:set_skitem_count(prod_id, last_count)
    log_debug(
        "[ProdeSkillComponent][skilleff_extprod_multiple] end wheel_count:{} sum_count:{} item_count:{} last_count:{} prod_id:{} skitem_count:{}",
        wheel_count, sum_count, item_count, last_count, prod_id, product:get_skitem_count(prod_id))
end

-- 技能效果-额外产出
function ProdeSkillComponent:skilleff_extprod(product, wheel_count, items)
    log_debug(
        "[ProdeSkillComponent][skilleff_extprod] begin master:{} id:{} product:{} wheel_count:{} items:{} prod_per:{}",
        self.master, self.id, product:get_id(), wheel_count, items, self.effects.prod_per)
    for eff_id, effect in pairs(self.effects.prod_per or {}) do
        if wheel_count == 1 then
            self:skilleff_extprod_single(product, eff_id, effect, items)
        else
            self:skilleff_extprod_multiple(product, eff_id, effect, items, wheel_count)
        end
    end
    log_debug("[ProdeSkillComponent][skilleff_extprod] end master:{} id:{} product:{} items:{}",
        self.master, self.id, product:get_id(), items)
end

-- 技能效果
function ProdeSkillComponent:skill_effect(product)
    if not product then
        log_err("[ProdeSkillComponent][skill_product] product is nil master:{} id:{}",
            self.master, self.id)
        return {}
    end
    local wheel_count = product:get_wheel_count()
    log_debug("[ProdeSkillComponent][skill_product] master:{} id:{} product:{} wheel_count:{}",
        self.master, self.id, product:get_id(), wheel_count)
    if wheel_count <= 0 then
        log_err("[ProdeSkillComponent][skill_product] wheel_count=0 master:{} id:{}",
            self.master, self.id)
        return {}
    end

    local items = {}
    -- 技能产品-消耗返还
    if not object_fty:is_gather(self.prototype) then
        self:skilleff_costback(product, wheel_count, items)
    end
    -- 技能产品-额外产出
    self:skilleff_extprod(product, wheel_count, items)
    return items
end
return ProdeSkillComponent
