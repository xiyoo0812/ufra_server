--recruit_component.lua
local log_err           = logger.err
local log_debug         = logger.debug
local tinsert           = table.insert
local recruit_mgr       = quanta.get("recruit_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")
local Q_SSR             = protobuf_mgr:enum("quality", "QUALITY_ORANGE")
local Q_SR              = protobuf_mgr:enum("quality", "QUALITY_PURPLE")

local FRAME_SUCCESS     = protobuf_mgr:error_code("FRAME_SUCCESS")
local FRAME_PARAMS      = protobuf_mgr:error_code("FRAME_PARAMS")
local RECR_NO_FREE      = protobuf_mgr:error_code("RECR_NO_FREE")
local RECR_CD_FREE      = protobuf_mgr:error_code("RECR_CD_FREE")
local RECR_USED_COUNT   = protobuf_mgr:error_code("RECR_USED_COUNT")

local RecruitComponent = mixin()
local prop = property(RecruitComponent)
prop:reader("level_conf", nil)  --配置

local dbprop = db_property(RecruitComponent, "player_recruit", true)
dbprop:store_value("total_cnt", 0)          --累计招募次数
dbprop:store_values("p_per_ten", {})         --每10次招募保底轮数(key-招募池 value-产出轮数)
dbprop:store_values("q_cnt", {})            --品质保底招募次数(key-品质保底组ID value-次数)
dbprop:store_values("q_loop", {})           --up保定轮数(key-up保底组ID value-次数)
dbprop:store_values("p_free_time", {})      --招募池免费招募时间
dbprop:store_values("p_free_cnt", {})       --招募池免费累计次数
dbprop:store_values("p_ten_cnt", {})        --招募池十连抽次数
dbprop:store_values("p_used_cnt",{})        --招募池累计招募次数

function RecruitComponent:__init()
end

function RecruitComponent:on_db_player_recruit_load(data)
    if data.player_id then
        self.total_cnt = data.total_cnt or 0
        self.q_cnt = data.q_cnt or {}
        self.q_loop = data.q_loop or {}
        self.p_free_time = data.p_free_time or {}
        self.p_free_cnt = data.p_free_cnt or {}
        self.p_ten_cnt = data.p_ten_cnt or {}
        self.p_per_ten = data.p_per_ten or {}
        self.p_used_cnt = data.p_used_cnt or 0
    end
    return true
end

--同步招募信息
function RecruitComponent:sync_recr(pool_id)
    local pool_infos = {}
    --单招募池同步
    if pool_id then
        local pool = recruit_mgr:get_pool(pool_id)
        if not pool then
            log_err("[RecruitComponent][sync_recr] player_id:{} pool_id:{} not found", self.id, pool_id)
            return
        end
        pool_infos[pool_id] = {
            last_free_time = self.p_free_time[pool_id] or 0,
            free_cnt = self.p_free_cnt[pool_id] or 0,
            ten_cnt = self.p_ten_cnt[pool_id] or 0,
            used_cnt = self.p_used_cnt[pool_id] or 0,
            start_time = 0
        }
        log_debug("[RecruitComponent][sync_recr] player_id:{} pool_infos:{}", self.id, pool_infos)
        self:send("NID_RECR_POOL_NTF", {pool_infos = pool_infos})
        return
    end

    --整体同步
    for id, _ in pairs(recruit_mgr:get_pools() or {}) do
        pool_infos[id] = {
            last_free_time = self.p_free_time[id] or 0,
            free_cnt = self.p_free_cnt[id] or 0,
            ten_cnt = self.p_ten_cnt[id] or 0,
            used_cnt = self.p_used_cnt[pool_id] or 0,
            start_time = 0
        }
    end
    log_debug("[RecruitComponent][sync_recr] player_id:{} pool_infos:{}", self.id, pool_infos)
    self:send("NID_RECR_POOL_NTF", {pool_infos = pool_infos})
end

--是否可免费招募
function RecruitComponent:check_free_recr(pool_id)
    local pool = recruit_mgr:get_pool(pool_id)
    if not pool then
        log_err("[RecruitComponent][check_free_recr] player_id:{} pool_id:{} not found", self.id, pool_id)
        return FRAME_PARAMS
    end
    --是否开启免费招募
    local pool_conf = pool:get_proto()
    if not pool_conf.free_cnt then
        log_err("[RecruitComponent][check_free_recr] player_id:{}  pool_id:{} not for free", self.id, pool_id)
        return RECR_NO_FREE
    end
    --每日免费次数重置功能已被取消
    --是否CD中
    local free_time = self.p_free_time[pool_id] or 0
    if (free_time + pool_conf.free_cd) > quanta.now then
        log_err("[RecruitComponent][check_free_recr] player_id:{}, pool_id:{} free cd:{} < player free time:{}",
        self.id, pool_id, free_time + pool_conf.free_cd, quanta.now)
        return RECR_CD_FREE
    end
    if pool_conf.limit_cnt then
        --总招募次数已用完
        local used_cnt = self.p_used_cnt[pool_id] or 0
        if used_cnt > pool_conf.limit_cnt then
            log_err("[RecruitComponent][check_free_recr] player_id:{}, pool_id:{} used_cnt:{} > limit_cnt:{}",
            self.id, pool_id, used_cnt, pool_conf.limit_cnt)
            return RECR_USED_COUNT
        end
    end
    return FRAME_SUCCESS
end

--招募池是否可招募
function RecruitComponent:check_recr(pool_id, use_cnt)
    local pool = recruit_mgr:get_pool(pool_id)
    if not pool then
        log_err("[RecruitComponent][check_recr] player_id:{} pool_id:{} not found", self.id, pool_id)
        return FRAME_PARAMS
    end
    local pool_conf = pool:get_proto()
    --招募池次数限制
    if pool_conf.limit_cnt then
        local used_cnt = self.p_used_cnt[pool_id] or 0
        if used_cnt + use_cnt > pool_conf.limit_cnt then
            log_debug("[RecruitComponent][check_recr] player_id:{} used_cnt:{} + use_cnt:{} >= limit_cnt:{}",
                self.id, used_cnt, use_cnt, pool_conf.limit_cnt)
            return RECR_USED_COUNT
        end
    end
    --时间限制

    return FRAME_SUCCESS
end

--招募
function RecruitComponent:recruit(pool_id, use_cnt)
    log_debug("[RecruitComponent][recruit] player_id:{} pool_id:{} use_cnt:{}", self.id, pool_id, use_cnt)
    --开始招募
    local res_items = {}
    local pool = recruit_mgr:get_pool(pool_id)
    local q_base_id = pool:get_proto().q_base_id
    local pool_q_cnt = self.q_cnt[q_base_id] or 0
    local pool_used_cnt = (self.p_used_cnt[pool_id] or 0)
    local pool_conf = pool:get_proto()

    for i = 1, use_cnt, 1 do
        pool_q_cnt = pool_q_cnt + 1
        local item
        --是否命中特殊规则
        local special_rule = pool:get_special(pool_used_cnt + i)
        if special_rule then
            item = special_rule
            --特殊规则
            item.rule = 1
        else
            --纯随机
            item = recruit_mgr:random(pool_id, pool_q_cnt)
            item.rule = 0
        end
        --开启品质保底后是否获得sr及以上
        if item.quality >= Q_SR and q_base_id then
            --获得ssr
            if item.quality == Q_SSR then
                item = self:recv_ssr(pool,  item)
                pool_q_cnt = 0
                --强制SSR
                item.rule = 2
            end
            --获得sr及以上(记录出现的抽数)
            self:save_p_per_ten_field(pool_id, pool_used_cnt + i)
        end

        local used_cnt =  pool_used_cnt + i
        log_debug("[RecruitComponent][recruit] player_id:{}  pool_id:{} per_ten_use_cnt:{}", self.id, pool_id, used_cnt)
        --开启10次保底
        local pool_per_ten = self.p_per_ten[pool_id] or 0
        if used_cnt ==  pool_per_ten + 10 and pool_conf.base_ten then
            log_debug("[RecruitComponent][recruit] player_id:{} force SR pool_id:{}", self:get_id(), pool_id)
            --强制产出sr
            item = recruit_mgr:random_sr(pool_id)
            self:save_p_per_ten_field(pool_id, used_cnt)
            --强制产出sr
            item.rule = 3
        end
        tinsert(res_items, item)
    end
    log_debug("[RecruitComponent][recruit] player_id:{} pool_id:{} q_base_id:{} pool_q_cnt:{}",self.id, pool_id, q_base_id, pool_q_cnt)
    if q_base_id then
        self:save_q_cnt_field(q_base_id, pool_q_cnt)
    end
    self:save_total_cnt(self.total_cnt + use_cnt)

    --招募记录
    self:rec_recr(pool_id, res_items)
    return res_items
end

--处理获得ssr
function RecruitComponent:recv_ssr(pool, item)
    local ssr_item = item
    local n_base_id = pool:get_proto().n_base_id
    local q_base_id = pool:get_proto().q_base_id
    local pool_q_loop = (self.q_loop[n_base_id] or 0) + 1
    if n_base_id then
        self:save_q_loop_field(n_base_id, pool_q_loop)
    end
    local up_item = pool:get_up_item()
    --是否获得限定卡
    if up_item  then
        local up_id = up_item.item_id
        --第二轮ssr还是非up卡,则强制出up卡
        if up_id ~= ssr_item.item_id and pool_q_loop > 1 then
            log_debug("[RecruitComponent][recv_ssr] player_id:{} change 2 up", self.id)
            ssr_item = up_item
        end
        if up_id == ssr_item.item_id and n_base_id then
            log_debug("[RecruitComponent][recv_ssr] up npc got, player_id:{} up_id:{} loop:{} q_cnt:{}", self.id, up_id, pool_q_loop, self.q_cnt[q_base_id])
            self:save_q_loop_field(n_base_id,0)
        else
            log_debug("[RecruitComponent][recv_ssr] up npc miss, player_id:{} up_id:{} cur item_id:{} loop:{} q_cnt:{}",
                 self.id, up_id, ssr_item.item_id, pool_q_loop, self.q_cnt[q_base_id])
        end
    end
    if q_base_id then
        self:save_q_cnt_field(q_base_id , 0)
    end
    return ssr_item
end

--获取招募池十连次数
function RecruitComponent:get_ten_cnt(pool_id)
    return self.p_ten_cnt[pool_id] or 0
end

--获取招募池已招募次数
function RecruitComponent:get_used_cnt(pool_id)
    return self.p_used_cnt[pool_id] or 0
end

--同步招募次数
function RecruitComponent:sync_recr_cnt(pool_id, is_ten, is_free)
    if is_free then
        --免费次数修改
        self:save_p_free_cnt_field(pool_id, (self:get_p_free_cnt()[pool_id] or 0) + 1)
        --免费时间
        self:save_p_free_time_field(pool_id, quanta.now)
    end
    --次数修改
    local use_cnt = 1
    if is_ten then
        self:save_p_ten_cnt_field(pool_id, (self:get_p_ten_cnt()[pool_id] or 0) + 1)
        use_cnt = 10
    end
    self:save_p_used_cnt_field(pool_id, (self:get_p_used_cnt()[pool_id] or 0) + use_cnt)
    --同步招募池信息
    self:sync_recr(pool_id)
end
return RecruitComponent