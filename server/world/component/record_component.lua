--record_component.lua

local log_err       = logger.err
local tsort         = table.sort
local tsize         = qtable.size
local tslice        = qtable.slice
local log_debug     = logger.debug
local tarray        = qtable.array

local store_mgr     = quanta.get("store_mgr")

local Record        = import("world/recruit/recruit_record.lua")

local RecordComponent = mixin()

local dbprop = db_property(RecordComponent, "player_recruit_record", true)
dbprop:store_objects("records", {})          --招募历史

function RecordComponent:__init()
end

--加载图鉴
function RecordComponent:load_records()
    if not self:is_player_recruit_record_loaded() then
        return store_mgr:load(self, self.id, "player_recruit_record")
    end
    return true
end

function RecordComponent:on_db_player_recruit_record_load(data)
    if data.player_id then
        for seq_no, rec in pairs(data.records or {}) do
            local record = Record(rec.pool_id, rec.item_type, rec.item_id, seq_no)
            record:load_db(rec)
            self:set_records_elem(seq_no, record)
        end
    end
    return true
end

--招募记录
function RecordComponent:rec_recr(pool_id, results)
    local seq_no = 1
    local now = quanta.now_ms
    self:load_records()
    for _, item in pairs(results) do
        local item_seq_no = now*100 + seq_no
        local record = Record(pool_id, item.item_type, item.item_id, item_seq_no)
        self:save_records_elem(item_seq_no,record)
        seq_no = seq_no + 1
        log_debug("[RecordComponent][rec_recr] player_id:{} seq_no:{}", self.id, item_seq_no)
    end
end

--招募总记录数
function RecordComponent:get_total_page()
    local total =  tsize(self.records or {})
    log_debug("[RecruitComponent][get_total_page] player_id:{} total_record:{}", self.id, total)
    return math.ceil(total / 5)
end

--获取招募记录
function RecordComponent:get_record(page_num, total_num)
    log_debug("[RecruitComponent][get_record] player_id:{} page_num:{} total_num:{}", self.id, page_num, total_num)
    if page_num > total_num or page_num < 1 then
        log_err("[RecruitComponent][get_record] page_num:{}, total_num:{} not valid", page_num, total_num)
        return
    end
    local start = (page_num -1) * 5 + 1
    local endpos =  tsize(self.records or {}) + 1
    if start + 5 < endpos then
        endpos = start + 4
    end
    local record = tarray(self.records)
    --排序
    tsort(record, function(a, b) return a.seq_no > b.seq_no end)
    log_debug("offset:{} endpos:{} ", start, endpos)
    return tslice(record, start, endpos)
end
return RecordComponent