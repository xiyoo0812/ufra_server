--workshop_component.lua
local log_debug         = logger.debug
local log_err           = logger.err
local tinsert           = table.insert

local object_fty        = quanta.get("object_fty")
local newbee_mgr        = quanta.get("newbee_mgr")
local config_mgr        = quanta.get("config_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")
local event_mgr         = quanta.get("event_mgr")
local utensil_db        = config_mgr:init_table("utensil", "id")

local BUILDING_FINISH   = protobuf_mgr:enum("building_status", "BUILDING_FINISH")
local BUILDING_REMOVE   = protobuf_mgr:enum("building_status", "BUILDING_REMOVE")

local WorkshopComponent = mixin()
local prop = property(WorkshopComponent)
prop:accessor("utensil_size", 0)        --utensil_size
prop:reader("utensil_counts", {})       --建筑数量 key-建筑原型id，value建筑数量
prop:reader("fn_types", {})             --建筑功能类型(key-功能枚举值UtensilType, value-数量)
prop:accessor("bank_id", 0)             --公共仓储
prop:accessor("human", 0)              --当前人口
prop:accessor("human_limit", 0)        --人口上限
prop:accessor("sat_lv", 0)             --当前满意度等级
prop:reader("open_minute", false)      --调试使用 是否开启分钟模拟器

local bdprop = db_property(WorkshopComponent, "home_building", true)
bdprop:store_objects("buildings", {})       --建筑信息
bdprop:store_value("building_faccel", true) --建筑首次加速

local pdprop = db_property(WorkshopComponent, "home_produce", true)
pdprop:store_objects("produces", {})        --生产列表

local maprop = db_property(WorkshopComponent, "home_utensil", true)
maprop:store_objects("utensils", {})        --摆件列表

function WorkshopComponent:__init()
end

function WorkshopComponent:sync_workshop(player)
    -- 同步建筑
    player:send("NID_BUILDING_BUILDING_NTF", { master = player:get_id(), elems = self:pack_building2client() })
    -- 同步生产
    player:send("NID_PRODUCE_PRODUCE_NTF", { master = player:get_id(), elems = self:pack_produce2client() })
    -- 同步摆件
    player:send("NID_UTENSIL_LIST_NTF", { master = player:get_id(), utensils = self:pack_utensil2client() })
    log_debug("[WorkshopComponent][sync_workshop] NID_BUILDING_BUILDING_NTF:{}", self:pack_building2client())
    log_debug("[WorkshopComponent][sync_workshop] NID_PRODUCE_PRODUCE_NTF:{}", self:pack_produce2client())
    log_debug("[WorkshopComponent][sync_workshop] NID_UTENSIL_LIST_NTF:{}", self:pack_utensil2client())
    -- 同步采集点
    player:send("NID_COLLECT_POINTS_NTF", { master = player:get_id(), collpoints = self.collpoints })
end

function WorkshopComponent:add_human(num)
    self.human = self.human + num
    self:sync_scene(self.player)
end

function WorkshopComponent:add_human_limit(num)
    self.human_limit = self.human_limit + num
    self:sync_scene(self.player)
end

function WorkshopComponent:check_human_limit()
    return self.human < self.human_limit
end

function WorkshopComponent:sync_scene(player)
    if player then
        local data = {
            human = self.human,
            master = player:get_id(),
            work_size = 1000,
            human_limit = self.human_limit,
            cur_sat = 0,
        }
        player:send("NID_BUILDING_TOWN_NTF", data)
        log_debug("[WorkshopComponent][sync_scene] log end player({}) human_limit({}) data({})", self.master, self.human_limit, data)
    end
end


function WorkshopComponent:close_workshop()
    for _, building in pairs(self.buildings) do
        building:close()
    end
end

function WorkshopComponent:pack_produce2db()
    local produces = {}
    for id, produce in pairs(self.produces) do
        produces[id] = produce:pack2db()
    end
    return {produces=produces}
end


-- 建筑客户端数据包
function WorkshopComponent:pack_building2client()
    local buildings = {}
    for id, building in pairs(self.buildings) do
        buildings[id] = building:pack2client()
    end
    return buildings
end

-- 生产客户端数据包
function WorkshopComponent:pack_produce2client()
    local produces = {}
    for id, produce in pairs(self.produces) do
        produces[id] = produce:packclient()
    end
    return produces
end

function WorkshopComponent:pack_utensil2client()
    local utensils = {}
    for id, utensil in pairs(self.utensils) do
        utensils[id] = utensil:pack2db()
    end
    return utensils
end

-- 加载建筑
function WorkshopComponent:on_db_home_building_load(data)
    if not data.buildings then
        newbee_mgr:init_building(self)
        return true
    end
    self.bank_id = data.bank_id or 0
    self.building_faccel = data.building_faccel or true
    for uid, bdata in pairs(data.buildings or {}) do
        local building = object_fty:create_utensil(bdata.proto_id, uid)
        if building then
            building:set_scene(self)
            self:set_buildings_elem(uid, building)
            building:load_db(self:get_master(), bdata)
            self:add_utensil_count(building)
            self:add_building_fn(building)
        end
    end
    return true
end

-- 加载生产
function WorkshopComponent:on_db_home_produce_load(data)
    if data.produces then
        for uuid,dbdata in pairs(data.produces or {}) do
            -- 根据uuid先查找建筑
            local building = self:get_building(uuid)
            if not building then
                log_err("[WorkshopComponent][on_db_player_produce_load] building is nil master({}) uuid({})", self:get_master(), uuid)
                goto continue
            end
            local produce = object_fty:create_produce(building.proto_id, uuid)
            if not produce then
                log_err("[WorkshopComponent][on_db_player_produce_load] produce is nil master({}) proto_id({}) uuid({})",self:get_master(),data.proto_id,uuid)
                goto continue
            end
            self:set_produces_elem(uuid, produce)
            produce:load_db(dbdata)
            produce:setup(self, building)
            :: continue ::
        end
        return
    end

    -- 初始化建筑关联的生产
    local buildings = self:get_buildings()
    for uuid,building in pairs(buildings or {}) do
        local produce = object_fty:create_produce(building.proto_id, uuid)
        if not produce then
            log_debug("[WorkshopComponent][on_db_player_produce_load] produce is nil master({}) proto_id({}) uuid({})",self:get_master(),building.proto_id,uuid)
            goto continue
        end
        self:set_produces_elem(uuid, produce)
        produce:load_db({})
        produce:setup(self, building)
        :: continue ::
    end
    self:flush_home_produce_db(self:pack_produce2db())
end

-- 加载摆件
function WorkshopComponent:on_db_home_utensil_load(data)
    if not data.utensils then
        newbee_mgr:init_utensil(self)
        return true
    end
    if data.utensils then
        for uid, bdata in pairs(data.utensils or {}) do
            local utensil = object_fty:create_utensil(bdata.proto_id, uid)
            if utensil then
                utensil:set_scene(self)
                self:set_utensils_elem(uid, utensil)
                utensil:load_db(self:get_master(), bdata)
                self:add_utensil_count(utensil)
            end
        end
    end
end

--玩家建筑初始化
function WorkshopComponent:init_building(born_buildings)
    for _, born in pairs(born_buildings) do
        local proto_id = born.proto_id
        local prototype = utensil_db:find_one(proto_id)
        if not prototype then
            log_err("[WorkshopComponent][init_building] cant find proto_id:{} config", proto_id)
            goto continue
        end
        if not object_fty:is_building(prototype) then
            log_err("[WorkshopComponent][init_building] find unsupport proto_id:{}", proto_id)
            goto continue
        end
        local building = object_fty:create_utensil(proto_id)
        if building then
            building:startup(self, born.detail, 0)
            self:set_buildings_elem(building.id, building)
            self:add_utensil_count(building)
            self:add_building_fn(building)
        end
        log_debug("[WorkshopComponent][init_building] proto_id:{}", proto_id)
        ::continue::
    end
    self:flush_home_building_db()
end

--摆件初始化
function WorkshopComponent:init_utensil(born_utensis)
    for _, born in pairs(born_utensis) do
        local proto_id = born.proto_id
        local prototype = utensil_db:find_one(proto_id)
        if not prototype then
            goto continue
        end
        if not object_fty:is_utensil(prototype) then
            log_err("[WorkshopComponent][init_utensil] find unsupport proto_id:{}", proto_id)
            goto continue
        end
        local utensil = object_fty:create_utensil(proto_id)
        if utensil then
            utensil:startup(self, born.detail, 0)
            self:set_utensils_elem(utensil.id, utensil)
            self:add_utensil_count(utensil)
        end
        log_debug("[WorkshopComponent][init_utensil] proto_id:{}", proto_id)
        ::continue::
    end
    self:flush_home_utensil_db()
end

--添加建筑功能个数
function WorkshopComponent:add_building_fn(building)
    if object_fty:is_building(building) and building:get_status() ~= BUILDING_FINISH then
        return
    end
    local fn_type = building:get_prototype().fn_type
    self.fn_types[fn_type] = (self.fn_types[fn_type] or 0) + 1
end

--减少建筑功能个数
function WorkshopComponent:reduce_building_fn(building)
    if object_fty:is_building(building) and building:get_status() ~= BUILDING_REMOVE then
        return
    end
    local fn_type = building:get_prototype().fn_type
    local left = (self.fn_types[fn_type] or 0) - 1
    if left < 0 then
        left = 0
    end
    self.fn_types[fn_type] = left
end

--添加utensil关联组
function WorkshopComponent:add_utensil_count(utensil)
    local proto_id = utensil:get_group()
    if not proto_id then
        return
    end
    local num = self.utensil_counts[proto_id] or 0
    self.utensil_counts[proto_id] = num + 1
    log_debug("[WorkshopComponent][add_utensil_count] master:{} proto_id:{}, num:{}", self:get_master(), proto_id, num + 1)
end

--减少utensile关联组
function WorkshopComponent:reduce_utensil_count(utensil)
    local proto_id = utensil:get_group()
    local num = self.utensil_counts[proto_id]
    if not num or not proto_id then
        log_err("[WorkshopComponent][reduce_utensil_count] error master:{} proto_id:{}, num:{}", self:get_master(), proto_id, num)
        return
    end
    local result = num - 1
    if result < 0 then
        result = 0
    end
    self.utensil_counts[proto_id] = result
    log_debug("[WorkshopComponent][reduce_utensil_count] master:{} proto_id:{}, num:{}", self:get_master(), proto_id, result)
end

--检测utensil数量
function WorkshopComponent:check_limit(player, utensil)
    local level = player:get_scene_level()
    local conf = utensil:get_prototype()
    log_debug("[WorkshopComponent][check_limit] master:{} utensil:{} group:{}", self:get_master(), conf.id, conf.group)
    local limit = object_fty:get_utensil_limit(conf.id, level)
    if limit == -1 then --不限制
        return true
    end

    local num = self.utensil_counts[conf.group] or 0
    log_debug("[WorkshopComponent][check_limit] master:{} utensil:{} group:{} limit:{} num:{}",
        self:get_master(), conf.id, conf.group, limit, num)
    return limit >= num + 1
end

--npc召回检测
function WorkshopComponent:check_npc_recall(id)
    local produce = self:get_produces(id)
    if not produce then
        return true
    end
    return produce:check_npc_recall()
end

function WorkshopComponent:get_building(id)
    return self.buildings[id]
end

function WorkshopComponent:get_utensil(id)
    local utensil = self.utensils[id]
    if utensil then
        return utensil
    end
    return self.buildings[id]
end

--刷新
function WorkshopComponent:_update(now)
    for _, building in pairs(self.buildings) do
        building:update(now)
    end
    for _, produce in pairs(self.produces) do
        produce:update(now)
    end
end

--获取指定proto_id的建筑
function WorkshopComponent:has_building(proto_id)
    for _, building in pairs(self.buildings or {}) do
        if building:get_proto_id() == proto_id then
            return true
        end
    end
    return false
end

function WorkshopComponent:get_building_num(proto_id)
    local count = 0
    for _, building in pairs(self.buildings or {}) do
        if building:get_proto_id() == proto_id then
            count = count + 1
        end
    end
    return count
end

function WorkshopComponent:get_group_num(group_id)
    local count = 0
    for _, building in pairs(self.buildings or {}) do
        if building:get_group() == group_id then
            count = count + 1
        end
    end
    return count
end

--获取建筑原型列表
function WorkshopComponent:get_building_ids()
    local result = { }
    for _, building in pairs(self.buildings) do
        result[building:get_proto_id()] = true
    end
    return result
end

--获取生产列表
function WorkshopComponent:get_produce_list()
    local list = {}
    for _,produce in pairs(self.produces or {}) do
        if produce:is_produce() then
            tinsert(list, produce)
        end
    end
    return list
end

-- 玩家上线
function WorkshopComponent:_online()
    log_debug("[WorkshopComponent][_online] master:{}", self.master)
    --开始生产
    for _,produce in pairs(self.produces or {}) do
        if produce:is_produce() then
            produce:set_online(true)
        end
    end
end

-- 玩家重连
function WorkshopComponent:_relive(player)
    log_debug("[WorkshopComponent][_relive] master:{}", self.master)
    --继续生产
    for _,produce in pairs(self.produces or {}) do
        if produce:is_produce() then
            produce:set_online(true)
        end
    end
    -- 推送生产数据(临时解决方案)(必须延迟一秒发送,否则客户端可能接不到)
    event_mgr:fire_second(function()
        player:send("NID_BUILDING_BUILDING_NTF", { master = player:get_id(), elems = self:pack_building2client() })
        player:send("NID_PRODUCE_PRODUCE_NTF", { master = player:get_id(), elems = self:pack_produce2client() })
    end)
end

-- 玩家下线
function WorkshopComponent:_offline()
    --暂停生产
    for _,produce in pairs(self.produces or {}) do
        if produce:is_produce() then
            produce:set_online(false)
        end
    end
    -- 清理摆件数量限制
    self.utensil_counts = {}
end

--检查仓库是否开启
function WorkshopComponent:check_bank()
    if self.bank_id and self.bank_id ~= 0 then
        local building = self:get_building(self.bank_id)
        if not building then
            return false
        end
        return true
    end
    return false
end
return WorkshopComponent
