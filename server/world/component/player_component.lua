--player_component.lua
local log_debug    = logger.debug
local event_mgr     = quanta.get("event_mgr")
local config_mgr    = quanta.get("config_mgr")
local newbee_mgr    = quanta.get("newbee_mgr")
local online        = quanta.get("online")

local level_db      = config_mgr:get_table("player_level", "level")

local AttrID        = enum("AttrID")
local STATE_NORMAL  = quanta.enum("CombatState", "NORMAL")
local F_PLAYER      = quanta.enum("Faction", "PLAYER")

local PlayerComponent = mixin()

function PlayerComponent:_setup()
    self:watch_attr(self, AttrID.ATTR_EXP, "update_exp")
    return true
end

--日期版本比对
function PlayerComponent:is_same_day(version)
    if version then
        return version == self:build_version()
    end
    return self:get_version() == self:build_version()
end

--上线加载场景
function PlayerComponent:_online()
    log_debug("[PlayerComponent][_online] id:{}", self.id)
    if self:is_newbee() then
        log_debug("[PlayerComponent][init_position] id:{}", self.id)
        newbee_mgr:init_position(self)
    end
    self:update_level()
    self:init_action_data()
end

--
function PlayerComponent:update_pos()
    self:flush_attr(AttrID.ATTR_POS_X)
    self:flush_attr(AttrID.ATTR_POS_Y)
    self:flush_attr(AttrID.ATTR_POS_Z)
    self:flush_attr(AttrID.ATTR_DIR_Y)
end

function PlayerComponent:_offline()
    self:update_pos()
end

function PlayerComponent:_unload()
    self:update_pos()
    online:login_service(self.id, "world", 0)
end

--经验更新
function PlayerComponent:update_level(level, level_up, exp, exp_cur)
    if not level then
        level = self:get_level()
    end
    local lvl_attr = level_db:find_one(level)
    if lvl_attr then
        self:set_level(level)
        self:set_hp_max(lvl_attr.hp)
        self:set_hp(lvl_attr.hp)
        self:set_exp_max(lvl_attr.exp)
        self:set_attack(lvl_attr.attack)
        self:set_defence(lvl_attr.defence)
        self:set_energy_max(lvl_attr.energy)
        self:set_critical_rate(lvl_attr.critical_rate)
        self:set_critical_hurt(lvl_attr.critical_hurt)
        if level_up then
            event_mgr:fire_frame(function()
                --self:sync_mirror()
                self:save_upgrade_time(quanta.now)
                self:notify_event("on_level_up", level, level_up)
                event_mgr:notify_trigger("on_level_up", self, level, level_up, exp, exp_cur)
            end)
        end
        --更新时装/装备/属性
        self:update_equips()
    end
end

--经验更新
function PlayerComponent:update_exp(exp)
    local level_up = 0
    local exp_cur = exp
    local level = self:get_level()
    local max_exp = self:get_exp_max()
    while exp_cur >= max_exp do
        local attrs = level_db:find_one(level + level_up + 1)
        if not attrs then
            break
        end
        level_up = level_up + 1
        exp_cur = exp_cur - max_exp
        max_exp = attrs.exp
    end
    if level_up > 0 then
        --升级
        local nlevl = level + level_up
        self:update_level(nlevl, level_up, exp, exp_cur)
        self:set_exp(exp_cur)
    end
end

--行为同步数据初始化
function PlayerComponent:init_action_data()
    self:set_faction(F_PLAYER)          -- 初始化阵营
    self:set_dying_timing(-1)           -- 初始化重伤倒计时
    self:set_treat_timing(-1)           -- 初始化救治倒计时
    self:set_combat_state(STATE_NORMAL) -- 初始化战斗状态
    self:set_sync_timestamp(-1)         -- 初始化同步时间戳
end

return PlayerComponent
