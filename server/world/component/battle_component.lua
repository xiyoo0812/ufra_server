--battle_component.lua
local qclamp            = qmath.clamp
local mceil             = math.ceil
local mmax              = math.max
local mmin              = math.min
local mrandom           = math.random
local mfloor            = math.floor
local mdistance         = qmath.distance
local log_err           = logger.err

local newbee_mgr        = quanta.get("newbee_mgr")
local event_mgr         = quanta.get("event_mgr")
local config_mgr        = quanta.get("config_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")

local map_db            = config_mgr:init_table("map", "map_id")
local fac_rel_db        = config_mgr:init_table("faction_relation", "subject", "object")

local FRAME_FAILED      = protobuf_mgr:error_code("FRAME_FAILED")

local STATE_NORMAL      = quanta.enum("CombatState", "NORMAL")
local STATE_DYING       = quanta.enum("CombatState", "DYING")
local FR_FRIENDLY       = quanta.enum("FactionRelation", "FRIENDLY")
local FR_NORMAL         = quanta.enum("FactionRelation", "NORMAL")
local FR_HOSTILE        = quanta.enum("FactionRelation", "HOSTILE")
local CURE_START        = quanta.enum("DyingCureType", "CURE_START")
local CURE_ABORT        = quanta.enum("DyingCureType", "CURE_ABORT")
local CURE_TRANS        = quanta.enum("DyingCureType", "CURE_TRANS")

---@class BattleComponent
local BattleComponent = mixin()
local prop = property(BattleComponent)
prop:accessor("toughness", nil)              -- 配置-韧性值
prop:accessor("toughness_max", nil)          -- 配置-韧性值上限
prop:accessor("anti_toughness", nil)         -- 配置-消韧抵抗系数(百分值(N/%))
prop:accessor("recover_toughness", nil)      -- 配置-韧性恢复速率(N/秒)
prop:accessor("toughness_wait", nil)         -- 配置-韧性恢复等待时间(单位:毫秒)
prop:accessor("toughness_fracture", nil)     -- 配置-破韧状态持续时间(单位:毫秒)
prop:accessor("sp_hit", nil)                 -- 配置-受到攻击-怒气值回复点数
prop:accessor("sp_attack", nil)              -- 配置-攻击敌人-怒气值回复点数
prop:accessor("sp_rate", nil)                -- 配置-怒气值回复率(N%)
prop:reader("latest_assaulted_ms", 0) -- 最近一次被攻击时间(单位: ms)
prop:reader("latest_toughness_recover_ms", 0) -- 最近一次韧性恢复时间(单位: ms)
prop:accessor("latest_toughness_fracture_ms", 0) -- 最近一次破韧时间(单位: ms)

function BattleComponent:__init()
end

function BattleComponent:_online()
    logger.dump('[ActionComponent:_online]')
    newbee_mgr:init_stamina_config(self)
    newbee_mgr:init_sp_config(self)
end

--更新
function BattleComponent:_update()
    self:dying_timing_check()
    self:toughness_recover_check()
end

---(self作为攻击方)战斗伤害计算
---@param target_defence number 目标防御
---@param target_level number 目标等级
---@param skill_damage_ratio number 技能伤害倍率
---@return integer, boolean 伤害, 是否暴击
function BattleComponent:combat_damage(target_defence, target_level, skill_damage_ratio)
    local level = self:get_level() -- 等级
    local critical_rate = self:get_critical_rate() -- 会心伤害几率（万分比）
    local critical_hurt = self:get_critical_hurt() -- 会心伤害（万分比）
    -- 伤害 = 基础攻击 + 武器攻击
    local damage = self:get_attack() -- 攻击(已包含装备的武器的攻击值)
    -- 公式: 伤害=攻击*(1 - 防/(防+25*防御方Lv+50))
    damage = damage * (1 - target_defence / (target_defence + 25 * (target_level or 0) + 50))
    -- 伤害=伤害*武器伤害倍率
    damage = damage * skill_damage_ratio
    -- 计算等级差影响 - 公式: 伤害=伤害*Clamp(1+(攻等级-防等级)*0.05,0.5,1.5)
    damage = damage * qclamp(1 + (level - (target_level or 0)) * 0.05, 0.5, 1.5)
    -- 计算攻击方暴击几率属性最终值
    local real_rate = mrandom()
    local critical_value = 1
    -- 随机计算是否暴击
    if real_rate <= critical_rate / 10000 then
        critical_value = critical_hurt / 10000
    end
    -- 如果暴击-公式: 伤害=伤害*攻击方暴击伤害倍率最终值
    damage = damage * critical_value
    -- 如果防御方有伤害减免，则-公式: 伤害=Max(0,伤害-伤害减免)
    -- 伤害减免(暂时不做，后续做)
    -- [1] 伤害减免 = 数值减伤 + 伤害 * 比例减伤
    local damage_reduce = 0
    -- [2] 伤害 = 伤害 - 伤害减免
    damage = mmax(0, damage - (damage_reduce or 0))
    -- 最终计算结果向上取整
    damage = mceil(damage)

    return damage, critical_value ~= 1
end

---韧性削减
---@param cutdown number 削减值
---@return boolean 是否已破韧
function BattleComponent:toughness_cutdown(cutdown)
    local now_ms = quanta.now_ms
    self.latest_assaulted_ms = now_ms -- 记录最后一次受击时间
    local anti_toughness = self:get_anti_toughness() -- 韧性抵抗系数(百分比)
    local real_cutdown = mfloor(cutdown * (1 - anti_toughness / 100)) -- 计算真实被削减值
    local new_toughness = mmax(self:get_toughness() - real_cutdown, 0) -- 削减韧性，最小值为0
    self:set_toughness(new_toughness) -- 更新韧性值
    local is_fracture = new_toughness <= 0
    if is_fracture then
        self.latest_toughness_fracture_ms = now_ms
    end
    return is_fracture
end

---韧性恢复检测
function BattleComponent:toughness_recover_check()
    local toughness = self:get_toughness()
    local toughness_max = self:get_toughness_max()
    if toughness == toughness_max then
        -- 韧性值已满, 直接返回
        return
    end
    local toughness_wait = self:get_toughness_wait()
    local recover_wait_ms = self.latest_assaulted_ms + toughness_wait -- 可以开始计时恢复韧性的时间
    -- 是否可以恢复(过了韧性恢复等待时间、距离上次恢复达到1秒以上)
    local can_recover = self.latest_toughness_recover_ms - recover_wait_ms >= 1000 and quanta.now_ms - self.latest_toughness_recover_ms >= 1000
    local can_fracture_recover = (
        self.latest_toughness_fracture_ms > 0 and
        quanta.now_ms - self.latest_toughness_fracture_ms >= self:get_toughness_fracture()
    )
    if toughness <= 0 and can_fracture_recover then
        -- 处于破韧状态，且破韧后韧性回满倒计时已到
        self:set_toughness(toughness_max) -- 韧性值回满
        self.latest_toughness_recover_ms = quanta.now_ms -- 更新最后一次恢复韧性时间
        self.latest_toughness_fracture_ms = 0 -- 重置破韧时间
    elseif toughness > 0 and toughness < toughness_max and can_recover then
        -- 韧性值不满，且破韧后韧性恢复等待时间已到, 按秒计时, 每秒恢复韧性值
        local recover_toughness = self:get_recover_toughness()
        local new_toughness = mmin(toughness + recover_toughness, toughness_max) -- 恢复后的韧性值, 不超过最大值
        self:set_toughness(new_toughness) -- 更新韧性值
        self.latest_toughness_recover_ms = quanta.now_ms -- 更新最后一次恢复韧性时间
    end
end

---重伤&救治检测
function BattleComponent:dying_timing_check()
    if not (self:is_player() or self:is_npc()) then
        return
    end
    -- 重伤状态判断
    if self:get_combat_state() ~= STATE_DYING then
        return
    end
    -- 重伤倒计时
    local dying_timing = self:get_dying_timing()
    -- 初始值(-1)判断，表示未重伤
    if dying_timing < 0 then
        return
    end
    -- 救治倒计时
    local treat_timing = self:get_treat_timing()
    -- 救治中
    if treat_timing >= 1 then
        -- 救治倒计时更新
        treat_timing = treat_timing - 1
        if treat_timing > 0 then
            self:set_treat_timing(treat_timing)
            return
        end
    end
    if treat_timing == 0 then
        -- 救治结束
        self:set_combat_state(STATE_NORMAL)
        self:set_dying_timing(-1)
        self:set_treat_timing(-1)
        -- TODO: 发送救治完成通知
        return
    end
    -- 重伤倒计时更新
    dying_timing = dying_timing - 1
    self:set_dying_timing(dying_timing)
    -- 重伤倒计时结束判断
    if dying_timing == 0 then
        ---@type Scene
        local scene = self:get_scene()
        if scene == nil then
            return
        end
        -- 传送到当前所在场景最近的安全点
        self:reset_dying_transed()
        self:trans_nearest_safety_point(scene)
        -- 通知视野内其他player
        local notify = {
            rescuer_id = nil,
            saved_id = self.id,
            type = CURE_TRANS,
            success = true,
            dying_timing = self:get_dying_timing(),
            treat_timing = self:get_treat_timing(),
        }
        -- 广播给视野内其他玩家
        scene:broadcast_message("NID_ACTOR_TREACT_NTF", notify)
    end
end

function BattleComponent:reset_dying_transed()
    -- 重置战斗状态
    self:set_combat_state(STATE_NORMAL)
    -- 重置重伤倒计时
    self:set_dying_timing(-1)
    -- (暂时)把生命值回满
    self:set_hp(self:get_hp_max())
end

function BattleComponent:trans_nearest_safety_point(scene)
    local map_id = scene:get_map_id()
    local map_conf = map_db:find_one(map_id)

    local nearest_sp = nil
    local nearest_dis = -1
    -- 当前所在坐标的x和z
    local pos_x = self:get_pos_x()
    local pos_z = self:get_pos_z()
    -- 查找最近的安全点
    for _, safe_pos in ipairs(map_conf.safety_point or {}) do
        -- 当前坐标到安全点的距离
        local safe_pos_dis = mdistance(pos_x, pos_z, safe_pos[1], safe_pos[3])
        -- 取更近的安全点
        if nearest_dis < 0 or safe_pos_dis < nearest_dis then
            nearest_dis = safe_pos_dis
            nearest_sp = safe_pos
        end
    end
    if nearest_sp ~= nil then
        -- 传送回安全点
        scene:trans_entity(self, nearest_sp[1], nearest_sp[2], nearest_sp[3], nearest_sp[4] or self:get_dir_y())
        return
    end
    if scene:is_copy() then
        -- 副本中若没有配置安全点，则需要转给副本系统处理
        event_mgr:notify_trigger("on_copy_dying_trans", self, scene)
        return
    end
    log_err('[BattleComponent][trans_nearest_safety_point] can not find safety point! player({}), map_id={}, x={}, z={}', self.id, map_id, pos_x, pos_z)
end

---阵营关系查询
function BattleComponent:faction_relation(target)
    local self_faction = self:get_faction()
    local target_faction = target:get_faction()
    local fr_conf = fac_rel_db:find_one(self_faction, target_faction)
    if fr_conf then
        return fr_conf.relation
    end
end

---阵营关系-友好
function BattleComponent:faction_is_friendly(target)
    return self:faction_relation(target) == FR_FRIENDLY
end

---阵营关系-中立
function BattleComponent:faction_is_normal(target)
    return self:faction_relation(target) == FR_NORMAL
end

---阵营关系-敌对
function BattleComponent:faction_is_hostile(target)
    return self:faction_relation(target) == FR_HOSTILE
end

function BattleComponent:on_actor_treat_start(scene, rescuer_id, saved_id, response)
    -- TODO: 重伤救治开始
    response.type = CURE_START
end

function BattleComponent:on_actor_treat_abort(scene, rescuer_id, saved_id, response)
    -- TODO: 重伤救治取消
    response.type = CURE_ABORT
end

function BattleComponent:on_actor_treat_trans(scene, rescuer_id, saved_id, response)
    if self:get_combat_state() ~= STATE_DYING or self.id ~= saved_id then
        -- 传送需要是重伤状态、传送需要是重伤者自己操作
        response.error = FRAME_FAILED
        return
    end
    self:reset_dying_transed()
    self:trans_nearest_safety_point(scene)
    response.rescuer_id = rescuer_id
    response.saved_id = saved_id
    response.type = CURE_TRANS
    response.success = true
    local notify = {
        rescuer_id = rescuer_id,
        saved_id = saved_id,
        type = CURE_TRANS,
        success = true,
        dying_timing = self:get_dying_timing(),
        treat_timing = self:get_treat_timing(),
    }
    -- 广播给视野内其他玩家
    scene:broadcast_message("NID_ACTOR_TREACT_NTF", notify)
end

return BattleComponent
