--aoi_component.lua
local create_aoio   = aoi.create_object

local AOI_MARKER    = aoi.aoi_type.marker
local AOI_WATCHER   = aoi.aoi_type.watcher

local mdistance     = qmath.distance

local world_mgr     = quanta.get("world_mgr")
local thread_mgr    = quanta.get("thread_mgr")
local protobuf_mgr  = quanta.get("protobuf_mgr")

local ENTITY_PLAYER = protobuf_mgr:enum("entity_type", "ENTITY_PLAYER")

local SOW_THRESHOLD_MS = 1900 --(刷怪)时间间隔阈值(ms)
local SOW_THRESHOLD_DIS = 900 --(刷怪)距离变化阈值(cm)

local AoiComponent = mixin()
local prop = property(AoiComponent)
prop:accessor("aoiobj", nil)    --aoiobj
prop:accessor("watchers", {})   --观察我的人
prop:accessor("markers", {})    --我观察的人
prop:accessor("nearby_sow_area_ids", {})  --当前靠近的区域id
prop:accessor("prev_update_pos_x", nil)  --上一次更新的pos.x
prop:accessor("prev_update_pos_z", nil)  --上一次更新的pos.z
prop:accessor("prev_update_pos_ms", nil)  --上一次更新的时间
prop:accessor("trans_scene_start", 0) -- 开始场景传送的时间(单位: ms)

function AoiComponent:__init()
end

function AoiComponent:reset_nearby_sow_area()
    local scene = self:get_scene()
    for sow_area_id, _ in pairs(self.nearby_sow_area_ids) do
        scene:deactivate_sow_area(sow_area_id)
    end
    self.nearby_sow_area_ids = {}
end

---接近的撒点刷怪区域改变
function AoiComponent:nearby_sow_area_change(x, z)
    if not self:is_player() then
        return
    end
    local new_pos_x = self:get_pos_x()
    local new_pos_z = self:get_pos_z()
    if not self.prev_update_pos_x or not self.prev_update_pos_z then
        self.prev_update_pos_x = x or new_pos_x
        self.prev_update_pos_z = z or new_pos_z
        return
    end
    local now_ms = quanta.now_ms
    if not self.prev_update_pos_ms then
        self.prev_update_pos_ms = now_ms
    end
    local diff_ms = now_ms - self.prev_update_pos_ms
    local diff_dis = mdistance(self.prev_update_pos_x, self.prev_update_pos_z, new_pos_x, new_pos_z)
    if diff_ms < SOW_THRESHOLD_MS and  diff_dis < SOW_THRESHOLD_DIS then
        -- 位置没有改变
        return
    end
    -- 检测区域撒点状态(改变)
    self:check_sow_area_status(self.prev_update_pos_x, self.prev_update_pos_z)
    -- -- 更新位置记录
    self.prev_update_pos_x = new_pos_x
    self.prev_update_pos_z = new_pos_z
end

--add_watcher
function AoiComponent:add_watcher(id)
    self.watchers[id] = true
end

--remove_watcher
function AoiComponent:remove_watcher(id)
    self.watchers[id] = nil
end

--add_watcher
function AoiComponent:add_marker(id)
    self.markers[id] = true
end

--remove_marker
function AoiComponent:remove_marker(id)
    self.markers[id] = nil
end

--setup_watcher
function AoiComponent:setup_watcher()
    self.aoiobj = create_aoio(self.id, AOI_WATCHER)
end

--setup_marker
function AoiComponent:setup_marker()
    self.aoiobj = create_aoio(self.id, AOI_MARKER)
end

--send
function AoiComponent:send(cmd_id, data)
    if self.session then
        world_mgr:send_message(self.session, cmd_id, data)
    end
end

--broadcast_message
function AoiComponent:broadcast_message(cmd_id, data)
    if self:is_sight() then
        --全图视野实体
        self.scene:broadcast_message(cmd_id, data)
        return
    end
    if self:get_type() == ENTITY_PLAYER then
        --发送给自己
        self:send(cmd_id, data)
    end
    --视野广播
    self:sight_message(cmd_id, data)
end

--视野广播
function AoiComponent:sight_message(cmd_id, data)
    for entity_id in pairs(self.watchers) do
        local player = self.scene:get_entity(entity_id)
        if player then
            player:send(cmd_id, data)
        else
            self.watchers[entity_id] = nil
        end
    end
end

--更新视野
function AoiComponent:sync_sight(scene_id)
    for player_id in pairs(self.markers) do
        local player = self.scene:get_entity(player_id)
        if player then
            self:send("NID_ENTITY_ENTER_SCENE_NTF", player:pack_enter_data(scene_id))
        end
    end
end

--pack_enter_data
function AoiComponent:pack_enter_data(scene_id)
    return {
        id          = self.id,
        scene_id    = scene_id,
        type        = self:get_type(),
        pos_x       = self:get_pos_x(),
        pos_y       = self:get_pos_y(),
        pos_z       = self:get_pos_z(),
        dir_y       = self:get_dir_y(),
        map_id      = self:get_map_id(),
        attrs       = self:package_attrs(16)
    }
end

--进入视野关注更新
function AoiComponent:enter_update_attent(target)
    -- 仅关注ai实体的entity才有属性attent_ai_entities
    local target_need_attent = target.attent_ai_entities ~= nil
    local watcher_need_attent = self.attent_ai_entities ~= nil
    -- 仅连接上ai服的ai实体才需要考虑
    local target_is_ai_entity = target.is_ai_entity
    local watcher_is_ai_entity = self.is_ai_entity
    -- 添加关注
    local scene = self:get_scene()
    local scene_id = scene:get_id()
    if target_need_attent and watcher_is_ai_entity then
        target:add_attent(scene_id, self:get_id())
        self:attented_count_increase()
        self:notify_nav_move()
    elseif watcher_need_attent and target_is_ai_entity then
        self:add_attent(scene_id, target:get_id())
        target:attented_count_increase()
        target:notify_nav_move()
    end
end

--离开视野关注更新
function AoiComponent:leave_update_attent(target)
    -- 仅关注ai实体的entity才有属性attent_ai_entities
    local target_need_attent = target.attent_ai_entities ~= nil and target:has_attent()
    local watcher_need_attent = self.attent_ai_entities ~= nil and self:has_attent()
    -- 仅连接上ai服的ai实体才需要考虑
    local target_is_ai_entity = target.is_ai_entity
    local watcher_is_ai_entity = self.is_ai_entity
    -- 添加关注
    local scene = self:get_scene()
    local scene_id = nil
    if scene then
        scene_id = scene:get_id()
    end
    if target_need_attent and watcher_is_ai_entity then
        target:remove_attent(scene_id, self:get_id())
        self:attented_count_reduce()
        local target_id = target:get_id()
        if self:entity_is_hated(target_id) then
            -- 敌人离开
            self:enemy_leave(target_id)
        end
    elseif watcher_need_attent and target_is_ai_entity then
        self:remove_attent(scene_id, target:get_id())
        target:attented_count_reduce()
        local watcher_id = self:get_id()
        if target:entity_is_hated(watcher_id) then
            -- 敌人离开
            target:enemy_leave(watcher_id)
        end
    end
end

--检测撒点区域状态
function AoiComponent:check_sow_area_status(prev_x, prev_z)
    if not self:is_player() then
        return
    end
    local scene = self:get_scene()
    if not scene then
        return
    end
    thread_mgr:entry(self:address(), function()
        -- 当前位置
        local x_now = self:get_pos_x()
        local z_now = self:get_pos_z()
        -- 判断是否离开了撒点区域
        for sow_area_id, _ in pairs(self.nearby_sow_area_ids) do
            if not scene:is_nearby_sow_area(sow_area_id, prev_x, prev_z, x_now, z_now) then
                -- 如果已不在某个激活的撒点区域附近，则取消激活该撒点区域
                scene:deactivate_sow_area(sow_area_id)
                self.nearby_sow_area_ids[sow_area_id] = nil
            end
        end
        -- 当前附近的撒点区域
        local nearby_sow_areas = scene:find_nearby_sow_areas(prev_x, prev_z, x_now, z_now)
        -- 如果当前不在某个撒点区域附近, 上次检测时靠近某个撒点区域，则说明离开了该撒点区域
        for _, sow_area in ipairs(nearby_sow_areas) do
            -- 如果当前在某个撒点区域附近，上次检测时不在某个撒点区域附近(或者到了新的区域附近)，则说明靠近了该撒点区域
            if not self.nearby_sow_area_ids[sow_area.area_id] then
                -- 激活新的撒点区域
                scene:activate_sow_area(sow_area.area_id)
                self.nearby_sow_area_ids[sow_area.area_id] = true
            end
        end
    end)
end

return AoiComponent
