--ai_skill_component.lua
local log_warn           = logger.warn
local qttrandom          = qtable.random
local qtrandom_array     = qtable.random_array

local SKL_AVAILABLE      = quanta.enum("SkillStatus", "AVAILABLE")
local SKL_UNAVAILABLE    = quanta.enum("SkillStatus", "UNAVAILABLE")
local SKL_USE_COMPLETED  = quanta.enum("SkillStatus", "USE_COMPLETED")
local ST_DEFAULT         = quanta.enum("SkillType", "Default")

local SKILL_USE_THRESHOLD     = 1000 * 12 -- 技能释放时间阈值(最长时间, 单位: ms)

---@type nav_geometry
local nav_geometry       = import('common/nav_geometry.lua')

---@class AiSkillComponent
local AiSkillComponent = mixin()
local prop = property(AiSkillComponent)
prop:reader("is_skill_available", true) -- 技能是否可用
prop:accessor("is_skill_using", false) -- 技能是否是正在释放

function AiSkillComponent:__init()
    self.latest_skill_use_ms = 0
end

function AiSkillComponent:check_skill_status()
    if self.latest_skill_use_ms <= 0 then
        return
    end
    local diff_ms = quanta.now_ms - self.latest_skill_use_ms
    if diff_ms <= SKILL_USE_THRESHOLD then
        return
    end
    self.latest_skill_use_ms = 0
    self.is_skill_available = true
    self.is_skill_using = false
end

function AiSkillComponent:change_skill_available(available)
    -- 修改技能状态
    if available == SKL_AVAILABLE then
        -- 技能就绪
        self.is_skill_available = true
    elseif available == SKL_UNAVAILABLE then
        -- 技能开始释放，变成了不可用状态
        self.is_skill_available = false
        self.latest_skill_use_ms = quanta.now_ms
    elseif available == SKL_USE_COMPLETED then
        -- 技能释放完成
        self.is_skill_using = false
        if self:is_standoff_available() then
            self.can_standoff = true
            -- 设置对峙倒计时
            self.standoff_countdown_expire = self:get_standoff_countdown_expire()
        end
    end
end

function AiSkillComponent:attack_hate_entity()
    ---@type HateEntity
    local hate_entity = self.hate_list:first()
    if not hate_entity then
        return false
    end
    local scene = self:get_scene()
    local enemy = scene:get_entity(hate_entity.entity_id)
    if not enemy then
        return false
    end
    -- 打断以更新位置
    if not self:nav_move_break(true) then
        self:standoff_move_break(true)
    end
    -- 由于位置延迟更新，增加判断防止空放技能
    local entity_pos = { x = self:get_pos_x(), y = self:get_pos_y(), z = self:get_pos_z(), }
    local enemy_pos = { x = enemy:get_pos_x(), y = enemy:get_pos_y(), z = enemy:get_pos_z(), }
    local distance = nav_geometry.calc_distance(entity_pos, enemy_pos)
    if distance > 800 then -- 怪物ai配置的最大攻击距离
        -- TODO: 后续需要增加怪物ai配置, 技能最大攻击距离
        return false
    end
    -- 放技能之前朝向敌人
    self:turn_dir(enemy_pos, true)
    return self:use_skill(enemy)
end

function AiSkillComponent:use_skill(enemy)
    if self.hit_moving_ms > quanta.now_ms then
        -- 还处于击退效果时间内，则跳过
        return false
    end
    local skill_set_id = qttrandom(self.skill_set)
    if not skill_set_id then
        return false
    end
    local skill_set = self.skill_set[skill_set_id]
    local equip_id = skill_set["equip_id"]
    local skill_id = qtrandom_array(skill_set.is_default and skill_set["skill_ids"]['melee'] or skill_set["skill_ids"]) -- 随机技能
    if not skill_id then
        log_warn("[AiSkillComponent][use_skill] entity_id: {}, skill_set.equip_id: {}, skill_id is nil", self:get_id(), equip_id)
        return false
    end
    -- 由于位置延迟更新，增加判断防止空放技能
    local entity_pos = { x = self:get_pos_x(), y = self:get_pos_y(), z = self:get_pos_z(), }
    local enemy_pos = { x = enemy:get_pos_x(), y = enemy:get_pos_y(), z = enemy:get_pos_z(), }
    local distance = nav_geometry.calc_distance(entity_pos, enemy_pos)
    if distance > 800 then -- 怪物ai配置的最大攻击距离
        return false
    end
    return self:start_use_skill(equip_id, skill_id, ST_DEFAULT)
end

return AiSkillComponent
