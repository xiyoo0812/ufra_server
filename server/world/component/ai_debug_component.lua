--ai_debug_component.lua
local log_debug         = logger.debug

local event_mgr         = quanta.get("event_mgr")

---@class AiDebugComponent
local AiDebugComponent = mixin()
local prop = property(AiDebugComponent)
prop:reader("attach_token", nil) -- 附加连接Id

function AiDebugComponent:__init()
end

function AiDebugComponent:attach(token)
    log_debug('[AiDebugComponent][attach] entity_id={}, token={}', self:get_id(), token)
    self.attach_token = token
end

function AiDebugComponent:detach(is_remove)
    log_debug('[AiDebugComponent][detach] entity_id={}, attach_token={}', self:get_id(), self.attach_token)
    local scene = self:get_scene()
    if not scene then
        return
    end
    if is_remove then
        event_mgr:notify_trigger("on_attach_ai_removed", scene:get_id(),  self:get_id(), self.attach_token, self:is_attaching())
    end
    self.attach_token = nil
end

function AiDebugComponent:is_attaching()
    return nil ~= self.attach_token
end

function AiDebugComponent:attach_notify(action, data)
    if not self:is_attaching() then
        return
    end
    event_mgr:notify_trigger("on_attach_ai_notify", self.attach_token, action, data)
end

---@param context GoapContext
function AiDebugComponent:hook_attach_goap_change(context)
    local scene = self:get_scene()
    if not scene then
        return
    end
    self:attach_notify("attach_context_change", {
        scene_id = tostring(scene:get_id()),
        entity_id = tostring(self:get_id()),
        context = {
            owner_entity = context.owner_entity,
            owner_spawn = context.owner_spawn,
            owner_enemy = context.owner_enemy,
            debug_info = context.debug_info,
        },
        plan = self:get_plan(),
    })
end

---@param context GoapContext
function AiDebugComponent:hook_attach_goap_no_plan(context)
    local scene = self:get_scene()
    if not scene then
        return
    end
    self:attach_notify("attach_no_plan", {
        scene_id = tostring(scene:get_id()),
        entity_id = tostring(self:get_id()),
        hook_ms = tostring(quanta.now_ms),
        context = {
            owner_entity = context.owner_entity,
            owner_spawn = context.owner_spawn,
            owner_enemy = context.owner_enemy,
            debug_info = context.debug_info,
        },
    })
end

---@param context GoapContext
function AiDebugComponent:hook_attach_goap_no_next_state(context)
    local scene = self:get_scene()
    if not scene then
        return
    end
    self:attach_notify("attach_no_next_state", {
        scene_id = tostring(scene:get_id()),
        entity_id = tostring(self:get_id()),
        hook_ms = tostring(quanta.now_ms),
        context = {
            owner_entity = context.owner_entity,
            owner_spawn = context.owner_spawn,
            owner_enemy = context.owner_enemy,
            debug_info = context.debug_info,
        },
    })
end

return AiDebugComponent
