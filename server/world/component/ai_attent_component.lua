--ai_attent_component.lua
--ai_attent_component.lua
local tinsert       = table.insert

-- local log_debug     = logger.debug
local log_warn      = logger.warn

---@class AiAttentComponent
local AiAttentComponent = mixin()
local prop = property(AiAttentComponent)
prop:accessor("attent_ai_entities", {}) --关注的ai服列表
prop:accessor('ai_hated_list', {}) -- 被仇视的ai实体列表

---是否有关注Ai服
function AiAttentComponent:has_attent()
    return next(self.attent_ai_entities) ~= nil
end

---添加关注
---@param scene_id integer AI实体所在场景id
---@param ai_entity_id integer AI实体id
function AiAttentComponent:add_attent(scene_id, ai_entity_id)
    -- log_debug("[AiAttentComponent][add_attent]: scene_id={}, ai_entity_id={}", scene_id, ai_entity_id)
    if not self.attent_ai_entities[scene_id] then
        self.attent_ai_entities[scene_id] = {}
    end
    self.attent_ai_entities[scene_id][ai_entity_id] = true
end

---移除关注
---@param scene_id integer AI实体所在场景id
---@param ai_entity_id integer AI实体id
function AiAttentComponent:remove_attent(scene_id, ai_entity_id)
    -- log_debug("[AiAttentComponent][remove_attent]: scene_id={}, ai_entity_id={}", scene_id, ai_entity_id)
    if not scene_id or not self.attent_ai_entities[scene_id] then
        return
    end
    self.attent_ai_entities[scene_id][ai_entity_id] = nil
    if not next(self.attent_ai_entities[scene_id]) then
        self.attent_ai_entities[scene_id] = nil
    end
end

---清除关注
---@return integer[] 关注的AI实体id列表
function AiAttentComponent:clear_attent()
    -- log_debug("[AiAttentComponent][clear_attent]")
    local ai_entity_ids = {}
    for scene_id, _ in pairs(self.attent_ai_entities) do
        for ai_entity_id, _ in pairs(self.attent_ai_entities[scene_id]) do
            tinsert(ai_entity_ids, ai_entity_id)
            self.attent_ai_entities[scene_id][ai_entity_id] = nil
        end
        self.attent_ai_entities[scene_id] = nil
    end
    return ai_entity_ids
end

---被仇恨数据改变
function AiAttentComponent:on_hated_change(hater_id, is_hate_build)
    if not self.ai_hated_list then
        self.ai_hated_list = {}
    end
    if is_hate_build then
        self.ai_hated_list[hater_id] = true
    else
        self.ai_hated_list[hater_id] = nil
    end
    if self:is_npc() and self:is_follower() then
        -- 随从NPC行为通知由主人发送
        local player  = self:get_owner()
        if player then
            player:send("NID_ACTOR_HATED_NTF", { hater_id = hater_id, hated_id = self.id, is_hate_build = is_hate_build})
        else
            log_warn('[AiAttentComponent][on_hated_change] NPC(entity_id: {}) owner(player_id={}) not found', self.id, self.owner_id)
        end
        return
    end
    self:send("NID_ACTOR_HATED_NTF", { hater_id = hater_id, hated_id = self.id, is_hate_build = is_hate_build})
end

function AiAttentComponent:clean_self_hate()
    local scene = self:get_scene()
    for ai_entity_id, _ in pairs(self.ai_hated_list or {}) do
        local ai_entity = scene:get_entity(ai_entity_id)
        if ai_entity then
            ai_entity:enemy_leave(self.id)
        end
    end
end

return AiAttentComponent
