--cond_component.lua

local log_err       = logger.err
local event_mgr     = quanta.get("event_mgr")
local Condition     = enum("Condition")

local TARGET_LIST = {
    [Condition.NPC] = import("world/cond/cond_npc.lua"),
    [Condition.TASK] = import("world/cond/cond_task.lua"),
    [Condition.TOWN] = import("world/cond/cond_town_level.lua"),
    [Condition.LEVEL] = import("world/cond/cond_level.lua"),
    [Condition.ASSIGN] = import("world/cond/cond_assign.lua"),
    [Condition.BUILDING] = import("world/cond/cond_building.lua"),
    [Condition.NPC_LIKING] = import("world/cond/cond_linking.lua"),
    [Condition.NPC_COUNT] = import("world/cond/cond_npc_count.lua"),
    [Condition.FINISH_TARGET] = import("world/cond/cond_target_finish.lua"),
    [Condition.RESIDENT_NUM] = import("world/cond/cond_resident.lua"),
    [Condition.GROUP_COUNT] = import("world/cond/cond_group_count.lua")
}

local CondComponent = mixin()
local prop = property(CondComponent)
prop:reader("bind_targets", { })

function CondComponent:__init()

    for cond_id, _ in pairs(TARGET_LIST) do
        local Target = TARGET_LIST[cond_id]
        self.bind_targets[cond_id] = Target(self, cond_id)
    end
    self:watch_event(self, "on_town_level_up")
end

function CondComponent:on_town_level_up(level)
    event_mgr:notify_trigger("on_town_level_up", self, level)
end

function CondComponent:_online()
    event_mgr:notify_trigger("on_add_custom_condition", self)
    for _, target in pairs(self.bind_targets) do
        if type(target.start) ~= "function" then
            log_err("{}", target)
        end
        target:start()
    end
end

function CondComponent:add_new_condition(cond_id, cond_val, obj, func, ...)
    local bind_target = self.bind_targets[cond_id]
    if bind_target == nil then
        log_err("[CondComponent][add_new_condition] target_id=%s", cond_id)
        return false
    end
    if type(obj[func]) ~= "function" then
        log_err("[CondComponent][add_new_condition] %s is not function", func)
        return false
    end
    return bind_target:add_condition(cond_val, obj, func, ...)
end

function CondComponent:check_conditions(conditions)
    for id, value in pairs(conditions) do
        local check_target = self.bind_targets[id]
        if not check_target:check_cond(value) then
            return false
        end
    end
    return true
end

function CondComponent:check_once_condition(id, value)
    local check_target = self.bind_targets[id]
    return check_target:check_one_cond(value)
end

return CondComponent