--ai_nav_component.lua
local msqrt              = math.sqrt
local mfloor             = math.floor
local mpi                = math.pi
local mabs               = math.abs
local mmax               = math.max
local mrandom            = math.random
local tinsert            = table.insert
local log_debug          = logger.debug
local log_warn           = logger.warn

local ACTION_MOVE        = quanta.enum("ActionType", "Move")
local SB_FORWARD         = quanta.enum("StandoffBehavior", "FORWARD")

---@type NavMgr
local nav_mgr            = quanta.get("nav_mgr")

---@type nav_geometry
local nav_geometry       = import('common/nav_geometry.lua')

local MOVE_INTERVAL      = 300 -- 移动计时器间隔(单位: ms)

---AI移动类型
local AiMoveType = enum("AiMoveType", 0)
AiMoveType.WANDER     = 1 --漫游移动
AiMoveType.TO_ENEMY   = 2 --向敌人移动
AiMoveType.TO_SPAWN   = 3 --向出生点移动

---@class AiNavComponent
local AiNavComponent = mixin()
local prop = property(AiNavComponent)
prop:reader("move_ctx", nil)  -- 移动上下文数据
prop:reader("is_moving", false) -- 是否在移动
prop:reader("check_nav_ms", 0) -- 最近一次检测寻路前进时间
prop:reader("nav_failed_counter", 0) -- 寻路失败计数器

function AiNavComponent:__init()
end

---寻路-移动
---@param ai_move_type number ai移动类型(AiMoveType)
---@param points vector3[] 寻路点(仅开始移动时需要)
---@param speed number 移动速度
function AiNavComponent:nav_move(ai_move_type, points, speed)
    if self:is_release() or not self.ai_enabled then
        self.move_ctx = nil
        return
    end
    if points then
        self:nav_move_break(true)
        self:setup_move_context(ai_move_type, speed, points, MOVE_INTERVAL)
    end
    if not self.move_ctx or not self.move_ctx.points and #self.move_ctx.points >= 2 then
        self.move_ctx = nil
        return
    end
    local has_next = self:judge_position()
    if has_next == nil then
        return
    end
    self.is_moving = has_next
    local is_stoped = not has_next
    self:broadcast_pos_dir(self.move_ctx.pos, self.move_ctx.dir, is_stoped)
    if not has_next then
        self.move_ctx = nil
    end
end

function AiNavComponent:nav_move_break(not_stop)
    if self.removed then
        self.move_ctx = nil
    end
    if not self.move_ctx then
        return false
    end
    -- 被打断时计算最新移动到的位置
    local has_next = self:judge_position()
    if has_next == nil then
        return false
    end
    self.is_moving = false
    local is_stoped = true -- 打断时 is_stoped 应为 true
    if not_stop == true then
        is_stoped = false
    end
    self:broadcast_pos_dir(self.move_ctx.pos, self.move_ctx.dir, is_stoped)
    self.move_ctx = nil
    return true
end

function AiNavComponent:check_nav_forward()
    if not self.move_ctx then
        return
    end
    local now_ms = quanta.now_ms
    local diff_ms = now_ms - self.check_nav_ms
    if diff_ms < MOVE_INTERVAL then
        return
    end
    self:nav_move()
    -- 更新检测时间
    self.check_nav_ms = now_ms
end

function AiNavComponent:judge_position()
    local latest_point = self.move_ctx.points[self.move_ctx.pindex] -- 最近经过的点
    if not latest_point then
        -- 数据无效
        return nil
    end
    local now_ms = quanta.now_ms
    local time_pass = now_ms - self.move_ctx.start_ms -- 距离开始移动后流逝的总时间
    local distance_pass = time_pass * self.move_ctx.speed -- 开始移动后应该移动了的总距离
    :: check_next_segment ::
    self.move_ctx.time_ms = now_ms -- 更新时间记录
    if self.move_ctx.pindex >= #self.move_ctx.points then
        -- 已经到了终点
        self.move_ctx.pos = latest_point
        self.move_ctx.dir = self.move_ctx.dirs[self.move_ctx.pindex]
        return false
    end
    if self.move_ctx.distances[self.move_ctx.pindex + 1] > distance_pass then
        -- 当前段未走完
        self.move_ctx.dir = self.move_ctx.dirs[self.move_ctx.pindex + 1]
        local distance = distance_pass  - self.move_ctx.distances[self.move_ctx.pindex]
        self.move_ctx.pos = {
            x = latest_point.x + self.move_ctx.dir.x * distance,
            y = latest_point.y + self.move_ctx.dir.y * distance,
            z = latest_point.z + self.move_ctx.dir.z * distance,
        }
        return true
    end
    self.move_ctx.pindex = self.move_ctx.pindex + 1 -- 段记录+1
    latest_point = self.move_ctx.points[self.move_ctx.pindex]
    goto check_next_segment
end

---寻路-移动-设置
---@param ai_move_type number ai移动类型(AiMoveType)
---@param speed number 移动速度(仅开始移动时需要, 单位: cm/ms)
---@param points vector3[] 寻路点(仅开始移动时需要)
---@param interval integer 间隔(ms)(仅开始移动时需要)
function AiNavComponent:setup_move_context(ai_move_type, speed, points, interval)
    self.move_ctx = {
        ai_move_type = ai_move_type, -- ai移动类型
        speed = speed,               -- 速度(cm/ms)
        pindex = 1,                  -- 当前点索引
        interval = interval,         -- 默认间隔(ms)
        pos = points[1],             -- 最新到达的位置
        dir = {x=0, y=0, z=0},       -- 最新的朝向
        time_ms = quanta.now_ms,     -- 最新一次移动时间
        start_ms = quanta.now_ms,    -- 开始移动时间
        points = points,
        distances = {}, -- 每个点距离起始点的距离
        dirs = {}, -- 每个段的移动朝向
    }
    -- 缓存每个点距离起始点的距离，当前朝向，加速计算
    local distance = 0
    local sum_distance = distance
    local dir = {x=0,y=0,z=0}
    for idx, point in ipairs(points) do
        if idx > 1 then
            distance = nav_geometry.calc_distance(point, points[idx - 1])
            sum_distance = sum_distance + distance
            dir = nav_geometry.calc_dir(points[idx - 1], point)
        end
        tinsert(self.move_ctx.distances, sum_distance)
        tinsert(self.move_ctx.dirs, dir)
    end
    local speed_cms = mfloor(speed * 1000) -- cm/ms 转为 cm/s
    self:broadcast_nav_start(speed_cms, points, self.move_ctx.start_ms)
end

---当前ai移动类型
---@return number 当前ai移动类型(为nil时表示未在移动)
function AiNavComponent:current_move_type()
    if self.move_ctx then
        return self.move_ctx.ai_move_type
    end
    return nil
end

---当前寻路移动目标点
function AiNavComponent:current_target_point()
    if self.move_ctx and self.move_ctx.points then
        return self.move_ctx.points[#self.move_ctx.points]
    end
    return nil
end

---entity是否在视野内
---@param entity_pos vector3 entity坐标
function AiNavComponent:entity_in_sight(entity_pos)
    if not self.ai_config_base then
        log_warn('[AiNavComponent]entity_in_sight() failed: ai_config_base(proto_id={}) is nil', self.proto_id)
        return false
    end
    -- 警戒配置参数
    -- 警戒扇形半径
    local guard_sector_radius = self.ai_config_base.guard_sector_radius
    -- 警戒扇形角度
    local guard_sector_angle = self.ai_config_base.guard_sector_angle
    -- 警戒圆形半径
    local guard_circle_radius = self.ai_config_base.guard_circle_radius
    -- 当前位置
    local pos_x = self:get_pos_x()
    local pos_y = self:get_pos_y()
    local pos_z = self:get_pos_z()
    local dir_y = self:get_dir_y()
    local dx = entity_pos.x - pos_x
    local dy = entity_pos.y - pos_y
    local dz = entity_pos.z - pos_z
    local distance = msqrt(dx * dx + dy * dy + dz * dz) -- 计算玩家与怪物之间的距离
    if distance > guard_sector_radius and guard_sector_radius > 0 then
        return false -- 目标实体 在视野外围扇形范围外
    elseif distance < guard_circle_radius then
        return true -- 目标实体 在视野内围圆形范围内
    end

    local angle_to_player = nav_geometry.custom_atan2(dz, dx) -- 计算 Ai实体 到 目标实体 的方向向量与 x 轴的夹角
    local delta_angle = angle_to_player - dir_y -- 计算 Ai实体 当前朝向和 Ai实体 到 目标实体 方向的夹角
    delta_angle = (delta_angle >= -mpi) and delta_angle or (delta_angle + 2 * mpi) -- 角度转换为 -π 到 π 之间的值
    delta_angle = (delta_angle <= mpi) and delta_angle or (delta_angle - 2 * mpi) -- 角度转换为 -π 到 π 之间的值
    if mabs(delta_angle) > guard_sector_angle / 2 then
        return false -- 目标实体 在视野范围外
    end

    return true -- 目标实体 在视野范围内
end

---转向
---@param toward_pos vector3 朝向坐标
---@param is_send2client boolean 是否发给client
function AiNavComponent:turn_dir(toward_pos, is_send2client)
    local pos = { x = self:get_pos_x(), y = self:get_pos_y(), z = self:get_pos_z() }
    local dir = nav_geometry.calc_dir(pos, toward_pos)
    -- 更新朝向
    local dir_y = mfloor(nav_geometry.dir_to_euler_angle(dir) * 100)
    -- log_debug('[AiNavComponent][turn_dir] entity_id: {}', self:get_id())
    self:set_dir_y(dir_y)
    local scene = self:get_scene()
    if self.hate_list:empty() then
        scene:move_entity(self, self:get_id(), pos.x, pos.z)
    else
        -- 直接aoi移动，避免触发热区移动
        local obj = self:get_aoiobj()
        scene.aoi.move(obj, pos.x, pos.z)
    end
    local speed = self:get_speed() * 100
    local actions = {
        {
            start_time = self:live_time(),
            start_pos = pos,
            move = {
                speed = speed,
                direction = SB_FORWARD,
            },
            type = ACTION_MOVE,
            start_dir = dir_y,
        }
    }
    local results = {
        {
            type = ACTION_MOVE,
            success = true,
        }
    }
    if speed > 5000 or speed < 50 then
        log_warn('[SceneServlet:turn_dir] speed({}) out of range(50~5000)cm/s', speed)
    end
    self:action_broadcast(pos, dir_y, actions, results, scene, true)
    return true
end

---广播位置朝向
---@param pos vector3 位置
---@param dir vector3 朝向
---@param is_stoped boolean client同步需要的标记
function AiNavComponent:broadcast_pos_dir(pos, dir, is_stoped)
    local scene = self:get_scene()
    -- 更新位置
    self:set_pos_y(mfloor(pos.y))
    self:set_pos_x(mfloor(pos.x))
    self:set_pos_z(mfloor(pos.z))
    -- 更新朝向
    local dir_y = mfloor(nav_geometry.dir_to_euler_angle(dir) * 100)
    self:set_dir_y(dir_y)
    local speed = self:get_speed() * 100
    local actions = {
        {
            start_time = self:live_time(),
            start_pos = {
                x = self:get_pos_x(),
                y = self:get_pos_y(),
                z = self:get_pos_z(),
            },
            move = {
                speed = speed,
                direction = SB_FORWARD,
            },
            type = ACTION_MOVE,
            start_dir = dir_y,
        }
    }
    local results = {
        {
            type = ACTION_MOVE,
            success = true,
        }
    }
    if speed > 5000 or speed < 50 then
        log_warn('[AiNavComponent:broadcast_pos_dir] speed({}) out of range(50~5000)cm/s', speed)
    end
    self:action_broadcast(pos, dir_y, actions, results, scene, is_stoped)
    return true
end

---广播寻路开始
---@param speed number 速度(cm/s)
---@param points vector3[] 寻路点
---@param start_ms integer 开始时间(ms)
function AiNavComponent:broadcast_nav_start(speed, points, start_ms)
    local start_pos = {
        x = self:get_pos_x(),
        y = self:get_pos_y(),
        z = self:get_pos_z(),
    }
    local dir_y = self:get_dir_y()
    local actions = {
        {
            start_time = self:live_time(),
            start_pos = start_pos,
            move = {
                speed = speed,
                elapsed = mmax(quanta.now_ms - start_ms, 0), -- 防止服务期间时间偏差
                direction = SB_FORWARD,
                path = points,
            },
            type = ACTION_MOVE,
            start_dir = dir_y,
        }
    }
    local results = {
        {
            type = ACTION_MOVE,
            success = true,
        }
    }
    local scene = self:get_scene()
    self:action_broadcast(start_pos, dir_y, actions, results, scene, true)
    return true
end

function AiNavComponent:move_wander()
    if self.move_ctx ~= nil then
        -- 如果正在移动，则跳过
        return
    end
    local mesh_wpawn_pos = {
        x = - self.spawn_pos.x,
        y = self.spawn_pos.y,
        z = self.spawn_pos.z,
    }
    local random_point = nav_mgr:around_point(self:get_map_id(), mesh_wpawn_pos, self.ai_config_base.wander_radius)
    if not random_point then
        self.nav_failed_counter = self.nav_failed_counter + 1
        if self.nav_failed_counter % 50 == 0 then
            local sow_conf = self.get_sow_conf and self:get_sow_conf() or {}
            log_debug(
                '[AiNavComponent][move_wander] get around_point failed! map_id={}, entity_id={}, proto_id={}, sow(area_id={}, id={})',
                self:get_map_id(), self:get_id(), self:get_proto_id(), sow_conf.area_id, sow_conf.id
            )
        end
        return
    end
    -- 打断以更新位置
    if not self:nav_move_break(true) then
        self:standoff_move_break(true)
    end
    local start = {
        x = - self:get_pos_x(),
        y = self:get_pos_y(),
        z = self:get_pos_z(),
    }
    local stop = {
        x = mfloor(random_point.x),
        y = mfloor(random_point.y),
        z = mfloor(random_point.z),
    }
    local points = nav_mgr:find_path(self:get_map_id(), start, stop)
    for idx, point in ipairs(points or {}) do
        points[idx].x = - point.x
        points[idx].y = point.y
        points[idx].z = point.z
    end
    self:nav_move(AiMoveType.WANDER, points, self:get_wander_speed())
end

function AiNavComponent:move_to_enemy()
    ---@type HateEntity
    local hate_entity = self.hate_list:first()
    if not hate_entity then
        return
    end
    local scene = self:get_scene()
    local enemy = scene:get_entity(hate_entity.entity_id)
    if not enemy then
        return
    end
    local current_target = self:current_target_point()
    if self:current_move_type() == AiMoveType.TO_ENEMY and current_target ~= nil then
        local dis_threshold = 450 -- 距离阈值，单位: cm
        local tim_threshold = 600 -- 时间阈值，单位: ms
        -- 如果正在向敌人移动，且敌人未移动太远(<= dis_threshold)，则取消重新寻路
        local enemy_pos = {
            x = enemy:get_pos_x(),
            y = enemy:get_pos_y(),
            z = enemy:get_pos_z(),
        }
        local dis_enemy = nav_geometry.calc_distance(current_target, enemy_pos)
        local tim_enemy = quanta.now_ms - self.move_ctx.start_ms
        if dis_enemy <= dis_threshold or tim_enemy < tim_threshold then
            return
        end
    end
    -- 打断以更新位置
    if not self:nav_move_break(true) then
        self:standoff_move_break(true)
    end
    local start = {
        x = - self:get_pos_x(),
        y = self:get_pos_y(),
        z = self:get_pos_z(),
    }
    local enemy_pos = {
        x = enemy:get_pos_x(),
        y = enemy:get_pos_y(),
        z = enemy:get_pos_z(),
    }
    local self_pos = {
        x = - start.x,
        y = start.y,
        z = start.z,
    }
    local dir = nav_geometry.calc_dir(enemy_pos, self_pos)
    local angle = mrandom(0, 9)
    local is_clockwise = quanta.now_ms % 2 == 0
    dir = nav_geometry.rotate_dir(dir, angle, is_clockwise)
    local distance = self:get_collision_distance()
    local stop = {
        x = mfloor(- (enemy_pos.x + dir.x * distance)),
        y = mfloor(enemy_pos.y),
        z = mfloor(enemy_pos.z + dir.z * distance),
    }
    local points = nav_mgr:find_path(self:get_map_id(), start, stop)
    if not points then
        return
    end
    for idx, point in ipairs(points or {}) do
        points[idx].x = - point.x
        points[idx].y = point.y
        points[idx].z = point.z
    end
    self:nav_move(AiMoveType.TO_ENEMY, points, self:get_chase_speed())
end

function AiNavComponent:move_to_spawn()
    ---@type HateEntity
    local hate_entity = self.hate_list:first()
    if hate_entity and not self:hate_entity_can_chase(hate_entity.entity_id) then
        -- 先移除仇恨目标
        self:remove_hate_entity(hate_entity.entity_id)
    end
    if self:current_move_type() == AiMoveType.TO_SPAWN then
        -- 如果正在向出生点移动，则跳过重新寻路
        return
    end
    local start = {
        x = - self:get_pos_x(),
        y = self:get_pos_y(),
        z = self:get_pos_z(),
    }
    local stop = {
        x = mfloor(- self.spawn_pos.x),
        y = mfloor(self.spawn_pos.y),
        z = mfloor(self.spawn_pos.z),
    }
    local points = nav_mgr:find_path(self:get_map_id(), start, stop)
    if not points then
        return
    end
    for idx, point in ipairs(points or {}) do
        points[idx].x = - point.x
        points[idx].y = point.y
        points[idx].z = point.z
    end
    -- logger.dump("[AiNavComponent:move_to_spawn()] start: {}, stop: {}, points: {}", start, stop, points)
    self:nav_move(AiMoveType.TO_SPAWN, points, self:get_chase_speed())
end

return AiNavComponent
