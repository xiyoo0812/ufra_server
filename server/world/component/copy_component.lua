--copy_component.lua
local timer_mgr         = quanta.get("timer_mgr")
local event_mgr         = quanta.get("event_mgr")

-- 副本组件
local CopyComponent = mixin()
local prop = property(CopyComponent)
prop:accessor("current_copy_id", nil) -- 当前副本ID
prop:accessor("current_copy_level", nil) -- 当前副本关卡
prop:accessor("current_copy_new", true) -- 当前副本是否是新副本
prop:accessor("current_copy_finish", false) -- 当前副本是否已通关
prop:reader("timer_id", 0) -- 定时器Id

local dbprop = db_property(CopyComponent, "player_copy", true)
dbprop:store_objects("copies", {})  -- (进行中的)副本

function CopyComponent:__init()
end

function CopyComponent:_online()
    if self:is_current_copy_finish() then
        -- 副本退出倒计时完成
        event_mgr:notify_trigger("on_copy_finish_timeout", self)
        return
    end
    local copy_id = self:get_current_copy_id()
    local copy_level = self:get_current_copy_level()
    if copy_id and copy_level then
        event_mgr:notify_trigger("on_copy_online_check", self, copy_id, copy_level)
    end
end

function CopyComponent:_offline()
    if self:is_current_copy_finish() then
        -- 副本退出倒计时完成
        event_mgr:notify_trigger("on_copy_finish_timeout", self)
        return
    end
    -- 离线时存储随从NPC的位置和朝向
    local copy = self:current_copy()
    local scene = self:get_scene()
    if copy and scene then
        -- 离线时清除自身仇恨
        self:clean_self_hate()
        for _, follower in pairs(scene:find_followers(self:get_id()) or {}) do
            local pos = {
                x = follower:get_pos_x(),
                y = follower:get_pos_y(),
                z = follower:get_pos_z(),
            }
            copy:upsert_follower(follower:get_proto_id(), pos, follower:get_dir_y(), true)
            -- 随从仇恨清除
            follower:clean_self_hate()
            -- 随从NPC重置同步时间戳, 防止Player重连后随从行为同步报错(SCENE_TIME_DISORDER=1355)
            follower:set_sync_timestamp(-1)
        end
    end
end

function CopyComponent:_unload()
    self:countdown_stop()
end

function CopyComponent:current_copy()
    local copy_id = self:get_current_copy_id()
    if not copy_id then
        return
    end
    return self:get_copies(copy_id)
end

function CopyComponent:enter_copy(config_id, copy_id, level_id, map_id, is_temporary, config_followers)
    local copy = self:get_copies(copy_id)
    local is_exists = (copy and next(copy)) or false
    if not is_exists then
        -- 新进入副本
        local PlayerCopy = import("world/copy/player_copy.lua")
        copy = PlayerCopy()
    end
    self:set_current_copy_new(not is_exists)
    copy:set_temporary(is_temporary)
    self:set_current_copy_id(copy_id)
    self:set_current_copy_level(level_id)
    self:set_current_copy_finish(false)
    if not is_exists then
        local followers = {}
        for proto_id, cf in pairs(config_followers) do
            followers[proto_id] = {
                proto_id = proto_id,
                x = cf.pos[1],
                y = cf.pos[2],
                z = cf.pos[3],
                d = cf.dir[2],
            }
        end
        local copy_data = {
            copy_id = copy_id,
            copy_config = config_id,
            copy_level = level_id or 1,
            copy_map = map_id,
            followers = followers,
        }
        if not is_temporary then
            copy:init_data(copy_data)
            self:save_copies_elem(copy_id, copy)
        else
            copy:load_data(copy_data)
            self:set_copies_elem(copy_id, copy)
        end
        return
    end
    -- 重入副本
    copy:set_copy_level(level_id)
    self:set_copies_elem(copy_id, copy)
end

function CopyComponent:exit_copy(copy_id)
    self:set_current_copy_id(nil)
    self:set_current_copy_level(nil)
    if copy_id then
        self:del_copies_elem(copy_id)
    end
end

function CopyComponent:get_copy_level(copy_id)
    local copy = self:get_copies(copy_id)
    if copy then
        return copy:get_copy_level()
    end
    -- 未参加指定 copy_id 的副本时，默认得到关卡 1
    return 1
end

function CopyComponent:update_copy_level(copy_id, level_id)
    local copy = self:get_copies(copy_id)
    if copy then
        copy:set_copy_level(level_id)
        if copy:is_temporary() then
            self:set_copies_elem(copy_id, copy)
        else
            self:save_copies_elem(copy_id, copy)
        end
        return true
    end
    return false
end

function CopyComponent:on_db_player_copy_load(data)
    if data.player_id then
        local copies = data.copies or {}
        local PlayerCopy = import("world/copy/player_copy.lua")
        for id, copy_data in pairs(copies) do
            local copy = PlayerCopy()
            copy:load_data(copy_data)
            self:set_copies_elem(id, copy)
        end
    end
    return true
end

function CopyComponent:find_copy(map_id)
    for id, copy in pairs(self:get_copies()) do
        if copy:get_copy_map() == map_id then
            return copy
        end
    end
end

function CopyComponent:countdown_start(over_time, is_force)
    if self.timer_id > 0 then
        if is_force then
            self:countdown_stop()
        else
            return
        end
    end
    self.timer_id = timer_mgr:once(over_time, function()
        self.timer_id = 0
        if self:is_current_copy_finish() then
            -- 副本退出倒计时完成
            event_mgr:notify_trigger("on_copy_finish_timeout", self)
        else
            -- 副本剧情关卡倒计时完成
            event_mgr:notify_trigger("on_plot_level_timeout", self)
        end
    end)
end

function CopyComponent:countdown_stop()
    if self.timer_id > 0 then
        timer_mgr:unregister(self.timer_id)
        self.timer_id = 0
    end
end

function CopyComponent:get_copy_followers()
    local copy = self:current_copy()
    if copy then
        local t = copy:get_followers()
        return t
    end
end

return CopyComponent
