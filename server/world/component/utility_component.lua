--utility_component.lua
local tarray        = qtable.array
local log_debug     = logger.debug

local store_mgr     = quanta.get("store_mgr")
local config_mgr    = quanta.get("config_mgr")
local protobuf_mgr  = quanta.get("protobuf_mgr")

local PACKET_ITEM   = protobuf_mgr:enum("packet_type", "PACKET_ITEM")

local item_db       = config_mgr:get_table("item")
-- 功能开放配置
local openfunc_db   = config_mgr:init_table("openfunc", "id")

local UtilityComponent = mixin()
-- 登录加载
local new_dbprop = db_property(UtilityComponent, "player_newbees", true)
new_dbprop:store_values("newbees", {})             --新手引导

local crs_prop = db_property(UtilityComponent, "player_clirewards", true)
crs_prop:store_values("clirewards", {})           --客户端奖励

-- 懒加载
local set_dbprop = db_property(UtilityComponent, "player_settings", true)
set_dbprop:store_values("settings", "")             --设置数据
local pic_dbprop = db_property(UtilityComponent, "player_pictorials", true)
pic_dbprop:store_values("pictorials", {})           --图鉴数据
local cpo_dbprop = db_property(UtilityComponent, "player_cpositions", true)
cpo_dbprop:store_values("cpositions", {})           --收藏位置

local saving_dbprop = db_property(UtilityComponent, "player_savings", true)
saving_dbprop:store_values("saving", {})             --存档数据

local prop = property(UtilityComponent)
prop:accessor("bait_item_id", 0) --当前钓鱼使用的鱼饵

-- 新手引导加载
function UtilityComponent:on_db_player_newbees_load(data)
    self.newbees = data.newbees or {}
    return true
end

-- 客户端奖励加载
function UtilityComponent:on_db_player_clirewards_load(data)
    self.clirewards = data.clirewards or {}
    -- 初始化客户端奖励红点
    self:init_clireward_reddot()
    return true
end

-- 客户端存档数据加载
function UtilityComponent:on_db_player_savings_load(data)
    self.saving = data.saving or {}
    return true
end

function UtilityComponent:set_saving(key, val)
    self:save_saving_field(key, val)
end

function UtilityComponent:del_saving(key)
    self:del_saving_field(key)
end


--加载设置
function UtilityComponent:load_settings()
    if not self:is_player_settings_loaded() then
        return store_mgr:load(self, self.id, "player_settings")
    end
    return true
end

-- 设置加载
function UtilityComponent:on_db_player_settings_load(data)
    self.settings = data.settings or ""
    return true
end

--加载图鉴
function UtilityComponent:load_pictorials()
    if not self:is_player_pictorials_loaded() then
        return store_mgr:load(self, self.id, "player_pictorials")
    end
    return true
end

-- 图鉴加载
function UtilityComponent:on_db_player_pictorials_load(data)
    self.pictorials = data.pictorials or {}
    return true
end

--加载图鉴
function UtilityComponent:load_cpositions()
    if not self:is_player_cpositions_loaded() then
        return store_mgr:load(self, self.id, "player_cpositions")
    end
    return true
end

-- 收藏位置加载
function UtilityComponent:on_db_player_cpositions_load(data)
    self.cpositions = data.cpositions or {}
    return true
end

--新手引导---------------------------------------------------------------------------------------------------------------
-- 插入新手引导数据
function UtilityComponent:insert_newbee(newbee_id)
    --与客户端约定 4位如101代表第1个引导的第1步 1010代表第10个引导的第10步
    local stage_id = math.floor(newbee_id / 100)
    self:save_newbees_field(stage_id, newbee_id)
end

--更新新手引导
function UtilityComponent:update_newbee(newbee_id)
    self:insert_newbee(newbee_id)
end

--设置---------------------------------------------------------------------------------------------------------------
-- 获取设置信息
function UtilityComponent:get_save_setting()
    return self.settings
end

--更新设置
function UtilityComponent:update_setting(setting_data)
    self:save_settings(setting_data)
end

-- 分钟定时器
function UtilityComponent:on_minute()
    self:refresh_reddots()
end

--图鉴物品---------------------------------------------------------------------------------------------------------------
-- 添加图鉴物品
function UtilityComponent:add_pictorial(item_id, count, packet_id)
    local has_count = self.pictorials[item_id]
    local item_count
    if has_count then
        item_count = has_count + count
    -- 首次获得
    else
        item_count = count
        if packet_id == PACKET_ITEM then
            self:add_item_reddot(item_id)
        end
    end
    self:save_pictorials_field(item_id, item_count)
end

-- 组装图鉴客户端数据格式
function UtilityComponent:pack_pictorials()
    return self.pictorials
end

-- 添加道具红点
function UtilityComponent:add_item_reddot(item_id)
    local item_conf = item_db:find_one(item_id)
    if not item_conf or not item_conf.first_reddots then
        return
    end
    -- 临时代码
    -- for _, rid in pairs(item_conf.first_reddots) do
    --     if rid and rid ~= 0 then
    --         --TODO_FIX self:add_new_redot(rid, 0, item_id)
    --     end
    -- end
end

-- 读取道具红点
function UtilityComponent:read_item_reddot(item_id)
    local item_conf = item_db:find_one(item_id)
    if not item_conf or not item_conf.first_reddots then
        return
    end
    for _, rid in pairs(item_conf.first_reddots) do
        if rid and rid ~= 0 then
            self:read_reddot_in_server(rid, item_id)
        end
    end
end

--位置收藏---------------------------------------------------------------------------------------------------------------
-- 添加位置
function UtilityComponent:add_collect_position(position)
    -- 生成标识(最大存储100条)
    local max_id = 100
    local new_id = max_id
    -- 分配空闲标识
    for id=1, max_id do
        local positions = self.cpositions[id]
        if not positions then
            new_id = id
            break
        end
    end
    -- 标识
    position.id = new_id
    -- 时间戳
    position.timestamp = os.time()
    -- 设置数据
    self:save_cpositions_field(new_id, position)
    return position
end

-- 删除位置
function UtilityComponent:del_collect_position(id)
    self:del_cpositions_field(id)
end

-- 编辑位置
function UtilityComponent:edit_collect_position(id, name, type)
    local position = self.cpositions[id]
    if not position then
        return false
    end
    -- 设置属性
    position.name = name
    position.type = type
    -- 保存数据
    self:save_cpositions_field(id, position)
    return true
end

-- 验证收藏位置是否合法
function UtilityComponent:is_collect_position_verify(position)
    if not position.map_id then
        return false
    end
    if not position.map_id then
        return false
    end
    if not position.pos_x then
        return false
    end
    if not position.pos_y then
        return false
    end
    if not position.pos_z then
        return false
    end
    return true
end

-- 位置收藏客户端数据
function UtilityComponent:pack_cpositions()
    local reslut = tarray(self.cpositions)
    return reslut
end

-- 获取功能开放条件
function UtilityComponent:get_openfunc_condition(c_sys_id)
    local openfunc_conf = openfunc_db:find_one(c_sys_id)
    if openfunc_conf then
        return openfunc_conf.condition
    end
    return {}
end

-- 获取剩余次数
function UtilityComponent:check_cliereward_count(conf)
    local clireward = self.clirewards[conf.id]
    if clireward then
        return conf.limit_count > clireward.done_count
    end
    return true
end

-- 领取客户端奖励
function UtilityComponent:finish_clireward(id, conf)
    local clireward = self.clirewards[id]
    if clireward then
        clireward.done_count = clireward.done_count + 1
    else
        clireward = { id = id, done_count = 1 }
    end

    self:send_template_mail(conf.email_template_id)
    self:save_clirewards_field(id, clireward)

    -- 红点处理(没有奖励时,清理红点)
    if not self:check_cliereward_count(conf) then
        local opts = {
            reddot_type = conf.reddot_type,
            reddot_rid = conf.reddot_rid
        }
        self:read_reddot_server(opts)
    end
    return clireward
end

-- 客户端奖励数据包
function UtilityComponent:pack_clireward(id)
    return self.clirewards[id]
end

-- 已读红点
function UtilityComponent:read_reddot(player_id, opts)
    log_debug("[UtilityComponent][read_reddot] player_id:{}, opts:{}", self.id, opts)
    self:on_read_reddot(opts)
end

-- 删除红点
function UtilityComponent:del_reddot(opts)
    log_debug("[UtilityComponent][del_reddot] player_id:{}, opts:{}", self.id, opts)
    local rid = opts.reddot_rid
    if opts.customs then
        for i, v in ipairs(opts.customs) do
            self:read_reddot_in_server(rid, v)
        end
    else
        self:del_reddot_by_rid(rid)
    end
end

return UtilityComponent
