--ai_component.lua
local mmax                  = math.max
local tinsert               = table.insert
local mfloor                = math.floor

local ai_mgr                = quanta.get("ai_mgr")
local event_mgr             = quanta.get("event_mgr")

local AL_FASTRUN            = quanta.enum("ActionLimit", "FASTRUN")
local ACTION_MOVE           = quanta.enum("ActionType", "Move")
local STATE_NORMAL          = quanta.enum("CombatState", "NORMAL")
local STATE_COMBAT          = quanta.enum("CombatState", "COMBAT")

---@class AiComponent
local AiComponent = mixin()
local prop = property(AiComponent)
prop:accessor("is_ai_entity", true)
prop:accessor("spawn_pos", {})      -- 出生点坐标
prop:accessor("can_wander", false)  -- 待机状态是否可漫游
prop:accessor("attented_count", 0)  -- 被关注的次数
prop:accessor("ai_nav_cache", nil)  -- 寻路缓存

function AiComponent:__init()
end

function AiComponent:_setup()
    self:assign_ai_data()
    return true
end

---增加被关注(大于0时启用哎)
function AiComponent:attented_count_increase(increase)
    self.attented_count = self.attented_count + (increase or 1)
    if self.attented_count > 0 then
        self.ai_enabled = true
    end
end

---减少被关注(等于0时禁用ai)
function AiComponent:attented_count_reduce(reduce)
    self.attented_count = mmax(self.attented_count - (reduce or 1), 0)
    if self.attented_count == 0 then
        self.ai_enabled = false
    end
end

---受到攻击
---@param attacker Player 攻击者
---@param damage number 伤害
---@param hit_duration number 受击持续时间
---@param hit_pos vector3 受击移动到的位置
function AiComponent:on_assaulted(attacker, damage, hit_duration, hit_pos)
    local attacker_id = attacker:get_id()
    local ai_entity_id = self.id
    local is_hated = self:has_hate_entity(attacker_id)
    if (not self.hate_list:has_entity(attacker_id)) or attacker.ai_hated_list[ai_entity_id] == nil then
        -- 添加仇恨列表 或 增加仇恨值
        self:add_hate_entity(attacker, damage, is_hated)
    end
    -- 受击后立即停止移动
    if not self:nav_move_break(false) then
        self:standoff_move_break(false)
    end
    self:assaulted_silence(hit_duration, hit_pos)
end

---敌人离开
function AiComponent:enemy_leave(enemy_id)
    local scene = self:get_scene()
    self.hate_list:remove_entity(enemy_id, false)
    if self.hate_list:empty() then
        self:set_combat_state(STATE_NORMAL) -- 设置为非战斗
    end
    local target = scene:get_entity(enemy_id)
    if target and target.ai_hated_list then
        target:on_hated_change(self.id, false)
        -- 如果没有被仇视了，且处于战斗状态，则恢复为非战斗状态
        if not next(target.ai_hated_list) and target:get_combat_state() == STATE_COMBAT then
            target:set_combat_state(STATE_NORMAL)
        end
    end
end

---enemy是否在仇恨列表中
function AiComponent:entity_is_hated(enemy_id)
    return self.hate_list:has_entity(enemy_id)
end

---行为广播
function AiComponent:action_broadcast(pos, dir_y, actions, results, scene, is_full_sync)
    ---@type Scene
    local sc = scene or self:get_scene()
    local notify = {
        entity_id = self.id,
        pos = {
            x = mfloor(pos.x),
            y = mfloor(pos.y),
            z = mfloor(pos.z),
        },
        dir = {
            x = 0,
            y = dir_y,
            z = 0,
        },
        timestamp = self:live_time(),
        full_sync = is_full_sync == true,
        action_limit = AL_FASTRUN,
        actions = {},
        results = {},
    }
    for _, action in ipairs(actions) do
        tinsert(notify.actions, action)
    end
    for _, result in ipairs(results) do
        tinsert(notify.results, result)
    end
    sc:broadcast_message("NID_ACTOR_ACTION_NTF", notify)
end

function AiComponent:assign_ai_data()
    -- 由于entity先调用setup后附加scene，所以需要等待下一帧再设置值
    self:set_spawn_pos({
        x = self:get_pos_x(),
        y = self:get_pos_y(),
        z = self:get_pos_z(),
    })
    self:set_release(false)
    event_mgr:fire_frame(function()
        -- 分配关联的ai服地址
        ai_mgr:assign_aisvr(self)
    end)
end

function AiComponent:update_nav_cache(speed, points, start_ms)
    if not (speed and points and start_ms) then
        self.ai_nav_cache = nil
        return
    end
    self.ai_nav_cache = {
        speed = speed,
        points = points,
        start_ms = start_ms,
    }
end

--导航移动通知
function AiComponent:notify_nav_move()
    if not (self.ai_nav_cache and self.ai_nav_cache.points and #self.ai_nav_cache.points >= 2) then
        return
    end
    local scene = self:get_scene()
    if not scene then
        return
    end
    --通知client
    local start_pos = {
        x = self:get_pos_x(),
        y = self:get_pos_y(),
        z = self:get_pos_z(),
    }
    local dir_y = self:get_dir_y()
    local actions = {
        {
            start_time = self:live_time(),
            start_pos = start_pos,
            -- elapsed_time = 0,
            -- stop_time = 0,
            move = {
                speed = self.ai_nav_cache.speed,
                elapsed = mmax(quanta.now_ms - self.ai_nav_cache.start_ms, 0), -- 防止服务期间时间偏差
                path = self.ai_nav_cache.points,
            },
            type = ACTION_MOVE,
            start_dir = dir_y,
        }
    }
    local results = {
        {
            type = ACTION_MOVE,
            success = true,
        }
    }
    self:action_broadcast(start_pos, dir_y, actions, results, scene, true)
end

return AiComponent
