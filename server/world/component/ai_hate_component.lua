--ai_hate_component.lua
local log_warn           = logger.warn
local tinsert            = table.insert

local timer_mgr          = quanta.get("timer_mgr")

---@type nav_geometry
local nav_geometry       = import('common/nav_geometry.lua')

---@type HatePriorityQueue
local HatePriorityQueue  = import('world/ai/object/hate_priority_queue.lua')

---@type HateEntity
local HateEntity         = import('world/ai/object/hate_entity.lua')

local STATE_NORMAL       = quanta.enum("CombatState", "NORMAL")
local STATE_COMBAT       = quanta.enum("CombatState", "COMBAT")

local HATE_INTERVAL      = 1000 -- 仇恨衰减计时器间隔(单位: ms)

---@class AiHateComponent
local AiHateComponent = mixin()
local prop = property(AiHateComponent)
prop:accessor("hate_list", {})        -- 仇恨列表
prop:reader("check_hate_ms", 0) -- 最近一次检测仇恨(衰减/移除)时间
prop:reader("silence_id", nil) -- 沉默倒计时计时器id

function AiHateComponent:__init()
    self.hate_list = HatePriorityQueue()
end

---受击沉默
---@param hit_duration number 受击持续时间
function AiHateComponent:assaulted_silence(hit_duration, hit_pos)
    if not hit_duration or hit_duration <= 5 then
        -- 判空，判断间隔太小(间隔太小无意义)
        return
    end
    if self.silence_id then
        timer_mgr:unregister(self.silence_id)
        self.is_silence = false
        self.silence_id = nil
    end
    self.is_silence = true
    self.silence_id = timer_mgr:once(hit_duration, function()
        self.is_silence = false
        self.silence_id = nil
        if hit_pos then
            self:set_pos_x(hit_pos.x)
            self:set_pos_y(hit_pos.y)
            self:set_pos_z(hit_pos.z)
        end
    end)
end

---仇恨列表-添加
function AiHateComponent:add_hate_entity(enemy, damage, is_hated)
    -- log_debug('[AiHateComponent][add_hate_entity] scene_id={}, entity_id: {}, enemy_id: {}, damage={}', self.scene_id, self.id, enemy:get_id(), damage)
    local entity_id = enemy:get_id()
    local idx, hate_entity = self.hate_list:find_entity(entity_id)
    if not idx then
        hate_entity = HateEntity(entity_id)
    end
    -- 增加仇恨值
    hate_entity:add_hate_value(damage)
    self.hate_list:insert_or_update(hate_entity, false)
    self:set_combat_state(STATE_COMBAT) -- 设置为战斗状态
    -- 敌人被仇恨和战斗状态改变
    enemy.ai_hated_list[self.id] = true
    enemy:on_hated_change(self.id, true)
    -- 如果处于非战斗状态，则需要修改为战斗状态
    if enemy:get_combat_state() == STATE_NORMAL then
        enemy:set_combat_state(STATE_COMBAT)
    end
end

---仇恨列表-移除
function AiHateComponent:remove_hate_entity(entity_id)
    -- log_debug('[AiHateComponent][remove_hate_entity] scene_id={}, entity_id: {}, enemy_id: {}', self.scene_id, self.id, entity_id)
    self.hate_list:remove_entity(entity_id, false)
    local scene = self:get_scene()
    if not scene then
        return false
    end
    if self.hate_list:empty() then
        self:set_combat_state(STATE_NORMAL) -- 设置为非战斗
    end
    local enemy = scene:get_entity(entity_id)
    if not enemy then
        return false
    end
    enemy.ai_hated_list[self.id] = nil
    if enemy.ai_hated_list then
        enemy:on_hated_change(self.id, false)
        -- 如果没有被仇视了，且处于战斗状态，则恢复为非战斗状态
        if not next(enemy.ai_hated_list) and enemy:get_combat_state() == STATE_COMBAT then
            enemy:set_combat_state(STATE_NORMAL)
        end
    end
end

---仇恨列表-是否存在
function AiHateComponent:has_hate_entity(entity_id)
    return self.hate_list:has_entity(entity_id)
end

---仇恨目标是否可追击
function AiHateComponent:hate_entity_can_chase(entity_id)
    local scene = self:get_scene()
    if not scene then
        return false
    end
    local enemy = scene:get_entity(entity_id)
    if not enemy then
        return false
    end
    -- 判断仇恨对象是否在可追击范围内
    local self_pos = { x = self:get_pos_x(), y = self:get_pos_y(), z = self:get_pos_z() }
    local spawn_distance = nav_geometry.calc_distance(self_pos, self.spawn_pos)
    local is_in_chase_scope = spawn_distance < self.ai_config_base.chase_scope
    local enemy_pos = { x = enemy:get_pos_x(), y = enemy:get_pos_y(), z = enemy:get_pos_z() }
    local hate_distance = nav_geometry.calc_distance(self_pos, enemy_pos)
    local is_in_chase_distance = hate_distance < self.ai_config_base.chase_distance
    return is_in_chase_scope and is_in_chase_distance
end

---检测仇恨衰减
function AiHateComponent:check_hate_attenuate()
    local now_ms = quanta.now_ms
    local diff_ms = now_ms - self.check_hate_ms
    -- 更新最短间隔为 HATE_INTERVAL ms
    if diff_ms < HATE_INTERVAL then
        return
    end
    if not self.ai_config_base then
        log_warn('[AiHateComponent]check_hate_attenuate() failed: ai_config_base(proto_id={}) is nil', self:get_proto_id())
        return
    end
    local reduce_value = self.ai_config_base.hate_attenuate
    if reduce_value < 1 then
        return
    end
    local remove_hate_ids = {}
    local need_resort = false
    for idx, hate_entity in self.hate_list:iterator() do
        -- 尝试减小仇恨值
        if hate_entity:try_reduce_hate_value(reduce_value) then
            -- 记录需要移除的仇恨目标
            tinsert(remove_hate_ids, hate_entity.entity_id)
        else
            -- 更新
            self.hate_list:force_set(idx, hate_entity, true)
            need_resort = true
        end
    end
    if need_resort then
        -- 仇恨值重排序
        self.hate_list:resort()
    end
    -- 移除仇恨对象
    for _, hate_entity_id in ipairs(remove_hate_ids) do
        self:remove_hate_entity(hate_entity_id)
    end
    -- 更新检测时间
    self.check_hate_ms = now_ms
end

---清空仇恨列表
function AiHateComponent:clear_hate_list()
    local hate_id_list = {}
    for _, hate_entity in self.hate_list:iterator() do
        tinsert(hate_id_list, hate_entity.entity_id)
    end
    for _, hate_id in pairs(hate_id_list) do
        self:remove_hate_entity(hate_id)
    end
end

return AiHateComponent
