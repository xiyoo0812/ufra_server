--prode_offline_component.lua
--离线生产
local log_warn              = logger.warn
local log_debug             = logger.debug
local log_err               = logger.err
local tinsert               = table.insert
local mfloor                = math.floor

local protobuf_mgr          = quanta.get("protobuf_mgr")
local event_mgr             = quanta.get("event_mgr")

local FULL                  = protobuf_mgr:enum("product_status", "PS_FULL")

local ProdeOffLineComponent = mixin()
function ProdeOffLineComponent:__init()
end

-- 离线更新
function ProdeOffLineComponent:off_update()
    if #self.product_list == 0 then
        return
    end
    if self:check_serial() then
        local base_time = quanta.now
        if not base_time then
            log_warn("[ProdeOffLineComponent][off_update] base_time is err master:{} id:{} base_time:{}",
                self.master, self.id, base_time)
            return
        end
        local elapse_time = self:max_off_time(quanta.now - base_time)
        local off_time = 0
        log_debug("[ProdeOffLineComponent][off_update] begin master:{} id:{} base_time:{} elapse_time:{} off_time:{}",
            self.master, self.id, base_time, elapse_time, off_time)
        for _, product in ipairs(self.product_list) do
            local storebox = self:storebox_allot(product)
            if storebox then
                local prod_time, done_time = self:formula_time(product.prototype.time, 0)
                log_debug(
                    "[ProdeOffLineComponent][off_update] serial log master:{} id:{} formula_time:{} prod_time:{} done_time:{} expt_count:{}",
                    self.master, self.id, product.prototype.time, prod_time, done_time,
                    mfloor(off_time / (prod_time - done_time)))
                local result, prod_end, rem_time = product:off_update(quanta.now, off_time, prod_time)
                if result then
                    self:sync_product(product)
                end
                if not result or not prod_end then
                    break
                end
                off_time = rem_time
            end
        end
    elseif self:check_parallel() then
        -- local login_time = 0
        for _, product in ipairs(self.product_list) do
            -- local diff_time = quanta.now - login_time
            -- local elapse_time = self:max_off_time(diff_time)
            local off_time = 0
            local storebox = self:storebox_allot(product)
            if storebox then
                local prod_time, _ = self:formula_time(product.prototype.time, 0)
                log_debug(
                    "[ProdeOffLineComponent][off_update] parallel log master:{} id:{} formula_time:{} prod_time:{} expt_count:{}",
                    self.master, self.id, product.prototype.time, prod_time, mfloor(off_time / prod_time))
                local result, _, _ = product:off_update(quanta.now, off_time, prod_time)
                if result then
                    self:sync_product(product)
                end
            end
        end
    end
    self:off_drop()
end

-- 离线掉落
function ProdeOffLineComponent:off_drop()
    local removes = {}
    for _, product in ipairs(self.product_list) do
        local fin_count = product:get_fin_count()
        if fin_count > 0 then
            local conf = product:get_prototype()
            event_mgr:notify_listener("on_product_done", self.master, product.prototype.produce_id, fin_count, conf.group)
            local goods = product:goods()
            local storebox = self:get_storebox(product)
            if not storebox then
                log_err("[ProdeOffLineComponent][off_drop] storebox is nil {} product({}-{}) not found", self.master, self.id,
                    product:get_uuid())
                goto continue
            end
            -- 自动配送(产出物品)
            local stash_full,putin_items = self:storebox_auto_delive(storebox, goods, product:is_end())
            -- 离线的轮次需要根据存入的物品进行计算(因为储物箱和仓库可能存在满载的情况)
            local item_id = putin_items and next(putin_items) or nil
            if item_id then
                -- 计算轮次
                local wheel_count = mfloor((putin_items[item_id] or 0) / product:get_prod_count())
                product:set_wheel_count(wheel_count)
            end
            -- 计算技能产出物品
            local skill_goods = self:skill_effect(product)
            product:receive()
            self:storebox_auto_delive_extra(storebox, skill_goods)
            if product:is_end() then
                tinsert(removes, product)
            else
                -- 仓库和储物箱都满了
                if self:check_product_full(storebox, stash_full) then
                    product:set_full()
                    self:sync_product(product)
                    -- 串行模式
                    if self:check_serial() then
                        self:change_status(FULL)
                    end
                end
            end
        end
        :: continue ::
    end
    -- 清理产品
    for _, product in pairs(removes) do
        self:remove_product(product, product:get_uuid())
    end

    if self:check_produce_full() then
        self:change_status(FULL)
    end
end
return ProdeOffLineComponent
