-- packet_component.lua

local log_info      = logger.info
local log_err       = logger.err
local log_debug     = logger.debug
local log_warn      = logger.warn
local mmin          = math.min

local Packet        = import("world/packet/packet.lua")
local Costor        = import("world/packet/costor.lua")
local Swaper        = import("world/packet/swaper.lua")
local Alloter       = import("world/packet/alloter.lua")
local Item          = import("world/packet/item.lua")

local config_mgr    = quanta.get("config_mgr")
local newbee_mgr    = quanta.get("newbee_mgr")
local protobuf_mgr  = quanta.get("protobuf_mgr")
local REISSUE       = quanta.enum("EmailTemplateId", "REISSUE")

local utility_db    = config_mgr:get_table("utility")

local BAG_CAPACITY  = utility_db:find_number("value", "bag_capacity")
local SHU_CAPACITY  = utility_db:find_number("value", "shut_capacity")
local EQU_CAPACITY  = utility_db:find_number("value", "equip_capacity")
local BAK_CAPACITY  = utility_db:find_number("value", "bank_capacity")

local FRAME_SUCCESS = protobuf_mgr:error_code("FRAME_SUCCESS")
local PACKET_ITEM   = protobuf_mgr:enum("packet_type", "PACKET_ITEM")
local PACKET_SHUT   = protobuf_mgr:enum("packet_type", "PACKET_SHUT")
local PACKET_EQUIP  = protobuf_mgr:enum("packet_type", "PACKET_EQUIP")
local PACKET_BANK   = protobuf_mgr:enum("packet_type", "PACKET_BANK")

local ITEM_NOT_EXIST    = protobuf_mgr:error_code("PACKET_ITEM_NOT_EXIST")
local COST_NOT_ENOUGH   = protobuf_mgr:error_code("PACKET_COST_NOT_ENOUGH")
local ITEM_IS_FULL      = protobuf_mgr:error_code("PACKET_ITEM_IS_FULL")
local PACKET_NOT_EMPTY  = protobuf_mgr:error_code("PACKET_PACKET_NOT_EMPTY")
local PACKET_NOT_EXIST  = protobuf_mgr:error_code("PACKET_PACKET_NOT_EXIST")

local PacketComponent = mixin()
local dbprop = db_property(PacketComponent, "player_item", true)
dbprop:store_objects("packets", {}) --背包列表

function PacketComponent:__init()
end

function PacketComponent:on_db_player_item_load(data)
    log_info("[PacketComponent][on_db_player_item_load]....")
    if not data.player_id then
        --初次登陆创建基础包裹
        self:add_packet(PACKET_ITEM, BAG_CAPACITY)
        self:add_packet(PACKET_SHUT, SHU_CAPACITY)
        self:add_packet(PACKET_EQUIP, EQU_CAPACITY)
        self:add_packet(PACKET_BANK, BAK_CAPACITY)
        --初始化背包/装备
        newbee_mgr:init_packet(self)
        return true
    end
    for packet_id, pdata in pairs(data.packets or {}) do
        local packet = self:load_packet(packet_id, pdata.capacity)
        packet:load_items(pdata.items or {})
    end
    return true
end

-- 加载包裹
function PacketComponent:load_packet(id, capacity)
    local packet = self.packets[id]
    if not packet then
        packet = Packet(self, id, capacity)
        self:set_packets_elem(id, packet)
    end
    packet:set_equip_bag(id == PACKET_EQUIP)
    log_info("[PacketComponent][load_packet] bag: {} load, capacity: {}!", id, capacity)
    return packet
end

-- 加载包裹
function PacketComponent:add_packet(id, capacity)
    local packet = Packet(self, id, 0)
    self:set_packets_elem(id, packet)
    packet:save_capacity(capacity)
    log_info("[PacketComponent][add_packet] add packet: {} load, capacity: {}!", id, capacity)
end

-- 移除包裹
function PacketComponent:remove_packet(id)
    self:del_packets_elem(id)
    log_info("[PacketComponent][remove_packet] packet: {} removed!", id)
end

--获取包裹
function PacketComponent:get_packet(id)
    return self.packets[id]
end

-- 获取道具
function PacketComponent:get_item(item_id, packet_id)
    log_info("[PacketComponent][get_item] player_id:{} item_id:{} packet_id:{}", self:get_id(), item_id, packet_id)
    local packet = self:get_packet(packet_id)
    if not packet then
        log_err("[PacketComponent][get_item] player_id({}) packet_id({}) not found", self:get_id(), packet_id)
        return
    end
    return packet:get_item_num(item_id)
end

--获取背包数量
function PacketComponent:check_item_num(item_id, num)
    local packet = self.packets[PACKET_ITEM]
    return packet:check_item_num(item_id, num)
end

--获取背包中道具数量
function PacketComponent:get_item_num(item_id)
    local packet = self.packets[PACKET_ITEM]
    if not packet then
        log_err("[PacketComponent][get_item_num] player_id({}) packet_id({}) not found", self:get_id(), PACKET_ITEM)
    end
    return packet:get_item_num(item_id)
end

-- 获取物品空间
function PacketComponent:get_item_space(item_id, packet_id)
    local alloter = Alloter(self, self:get_packet(packet_id or PACKET_ITEM))
    return alloter:get_item_space(item_id)
end

-- 分配物品
-- item_id [number] 道具ID
-- num [number] 数量
-- reason [obtain_reason] 原因
function PacketComponent:allot_item(item_id, num, reason, target, packet_id)
    return self:allot_items({[item_id] = num }, reason, target, packet_id)
end

-- 分配检查
function PacketComponent:allot_check(items, target, reason, packet_id)
    local alloter = Alloter(self, self:get_packet(packet_id or PACKET_ITEM))
    if not alloter:check_items(items, reason) then
        return
    end
    alloter:set_target(target)
    return alloter
end

-- 分配多种物品
-- items = { [item_id] = num, ... }
function PacketComponent:allot_items(items, reason, target, packet_id)
    local alloter = self:allot_check(items, target, reason, packet_id)
    if not alloter then
        return false
    end
    alloter:allot(reason)
    return true
end

-- 消耗item
function PacketComponent:cost_item(item_id, num, reason, packet_id)
    return self:cost_items({[item_id] = num}, reason, packet_id)
end

-- 消耗检查
function PacketComponent:cost_check(items, packet_id)
    local costor = Costor(self, self:get_packet(packet_id and packet_id or PACKET_ITEM))
    if not costor:check_items(items) then
        return
    end
    return costor
end

-- 消耗多个item
-- items = { [item_id] = num, ... }
-- reason [cost_reason] 原因
function PacketComponent:cost_items(items, reason, packet_id)
    local costor = self:cost_check(items, packet_id)
    if not costor then
        return false
    end
    costor:cost(reason)
    return true
end

--丢弃道具
function PacketComponent:drop_item(item_id, num, reason, packet_id)
    return self:drop_items({[item_id] = num}, reason, packet_id)
end

--丢弃道具
function PacketComponent:drop_items(items, reason, packet_id)
    local packet = self:get_packet(packet_id)
    if not packet then
        log_err("[PacketComponent][drop_items] player_id:{} packet_id:{} not found", self:get_id(), packet_id)
        return
    end
    local costor = Costor(self, packet)
    for item_id, num in pairs(items) do
        costor:add_item(item_id, num)
    end
    costor:cost(reason)
end

--道具排序
function PacketComponent:sort_items(packet_id)
    local packet = self.packets[packet_id]
    if not packet then
        return false
    end
    local swaper = Swaper(self)
    swaper:sort(packet)
    return true
end

--交换道具
function PacketComponent:swap_item(item, tar_packet, src_packet, count)
    local swaper = Swaper(self)
    if not swaper:check_swap(item, tar_packet) then
        return false
    end
    local item_id = item:get_item_id()
    swaper:swap_item(item, tar_packet, src_packet, count)
    if src_packet.id == PACKET_ITEM and src_packet:get_item_num(item_id) == 0 then
        self:read_item_reddot(item_id)
    end
    self:notify_event("on_item_changed", item_id, count)
    return true
end

--目标装备位是否已有装备
function PacketComponent:has_old_equip(equip)
    --目标装备位是否已有装备
    local pos = equip:get_pos()
    local equip_bag = self:get_packet(PACKET_EQUIP) or {}
    for item_id, _ in pairs(equip_bag:get_items()) do
        local old_equip = Item(item_id, 1)
        if old_equip:get_pos() == pos then
            return old_equip
        end
    end
end

--穿戴装备
function PacketComponent:install(equip)
    local swaper = Swaper(self)
    local ok = swaper:install(equip, self:get_packet(PACKET_EQUIP),self:get_packet(PACKET_ITEM))
    if not ok then
        return ok
    end
    --上装
    equip:install_effect(self)
    local item_id = equip:get_item_id()
    self:notify_event("on_item_changed", item_id, 1)
    return true
end

--卸载装备
function PacketComponent:uninstall(equip)
    local swaper = Swaper(self)
    local ok = swaper:uninstall(equip, self:get_packet(PACKET_ITEM), self:get_packet(PACKET_EQUIP))
    if not ok then
        return ok
    end
    --下装
    equip:uninstall_effect(self)
    local item_id = equip:get_item_id()
    self:notify_event("on_item_changed", item_id, 1)
    return true
end

--修改外观显示
function PacketComponent:update_equips()
    local suitable = self:get_suitable()
    local equip_packet = self:get_packet(PACKET_EQUIP)
    if equip_packet then
        equip_packet:update_equip_attrs()
        equip_packet:update_avatars(suitable == 0)
    end
end

--更新容量
function PacketComponent:update_capacity(packet, id, capacity)
    packet:save_capacity(capacity)
    self:send("NID_PACKET_ITEM_SYNC_NTF", { packet_id = id, capacity = capacity })
end

-- 奖励
function PacketComponent:reward_items(items, reason, is_email)
    self:execute_drop({ drop = items, reason = reason }, is_email)
end

--同步道具
function PacketComponent:sync_items(packet_id, items)
    self:send("NID_PACKET_ITEM_SYNC_NTF", { packet_id = packet_id, items = items })
end

--同步获得道具
function PacketComponent:sync_obtains(obtains)
    self:send("NID_PACKET_ITEM_OBTAIN_NTF", obtains)
end

--同步包裹
function PacketComponent:sync_packet(id)
    local packet = self:get_packet(id)
    self:send("NID_PACKET_ITEM_SYNC_NTF", packet:pack2client())
end

-- 同步包裹
function PacketComponent:sync_packets()
    for _, packet in pairs(self.packets) do
        self:send("NID_PACKET_ITEM_SYNC_NTF", packet:pack2client())
    end
end

--自动填充储物箱
function PacketComponent:fill_packet(src_packet, tar_packet)
    local swaper = Swaper(self)
    swaper:fill_packet(src_packet, tar_packet)
end


---------------------------rpc改写--------------------------
-- 获取生产掉落
function PacketComponent:get_produce_drop(drops, extend)
    local drop = drops.drop
    -- 仓库存入
    local bank_drop = {drop={},reason=drops.reason, packet_id=PACKET_BANK}
    -- 背包存入
    local item_drop = {drop={},reason=drops.reason, packet_id=PACKET_ITEM}
    for item_id,_ in pairs(drop) do
        if extend.bank then
            local count = mmin(drop[item_id], self:get_item_space(item_id, PACKET_BANK))
            drop[item_id] = drop[item_id] - count
            if count > 0 then
                bank_drop.drop[item_id] = count
            end
        end
        if extend.pack and drop[item_id] > 0 then
            local count = mmin(drop[item_id], self:get_item_space(item_id, PACKET_ITEM))
            drop[item_id] = drop[item_id] - count
            if count > 0 then
                item_drop.drop[item_id] = count
            end
        end
        if drop[item_id] == 0 then
            drop[item_id] = nil
        end
    end
    -- 空间不足时,阻断
    if extend.block then
        if next(drop) then
            return ITEM_IS_FULL,bank_drop,item_drop,drop
        end
    end
    return FRAME_SUCCESS, bank_drop, item_drop,drop
end

-- 获取生产消耗
function PacketComponent:get_produce_cost(costs, bank)
    local cost = costs.cost
    -- 仓库消耗
    local bank_costs = {cost={},reason=costs.reason, packet_id=PACKET_BANK}
    -- 背包消耗
    local item_costs = {cost={},reason=costs.reason, packet_id=PACKET_ITEM}
    -- 物品是否足够验证
    for item_id,_ in pairs(cost) do
        if bank then
            local count = mmin(cost[item_id], self:get_item(item_id, PACKET_BANK))
            if count > 0 then
                cost[item_id] = cost[item_id] - count
                bank_costs.cost[item_id] = count
            end
        end
        if cost[item_id] > 0 then
            local costor = self:cost_check({[item_id]=cost[item_id]}, PACKET_ITEM)
            if costor then
                item_costs.cost[item_id] = cost[item_id]
                cost[item_id] = 0
            end
        end
        if cost[item_id] > 0 then
            return COST_NOT_ENOUGH, bank_costs, item_costs
        end
    end
    return FRAME_SUCCESS, bank_costs, item_costs
end
--格式化花费
function PacketComponent:format_costs(costs, packet_id)
    if not costs then
        return
    end
    --是否指定背包
    if not packet_id then
        packet_id = PACKET_ITEM
    end
    if costs.guid then
        local items = {}
        for item_id, count in pairs(costs.guid) do
            local has_count = self:get_item(item_id, packet_id)
            if has_count < count then
                log_err("[PacketComponent][format_costs] get_item ({}) failed!", item_id)
                return nil, ITEM_NOT_EXIST
            end
            items[item_id] = count
        end
        self:drop_items(items, costs.reason, packet_id)
    end
    if costs.cost then
        local costor = self:cost_check(costs.cost, packet_id)
        if not costor then
            log_warn("[PacketComponent][format_costs] cost_check ({}) failed!", costs.cost)
            return nil, COST_NOT_ENOUGH
        end
        return costor
    end
end

--格式化掉落
function PacketComponent:format_drops(drops, packet_id)
    if not drops or not drops.drop then
        return
    end
    local drop_items = drops.drop
    local alloter = self:allot_check(drop_items, nil, drops.reason, packet_id)
    if not alloter then
        log_err("[PacketComponent][format_drops] allot_check ({}) failed!", drop_items)
        return nil, ITEM_IS_FULL
    end
    return alloter
end

--扣除花费
function PacketComponent:execute_cost(costs)
    log_debug("[PacketComponent][execute_cost] player({}) costs({})!", self.id, costs)
    local costor, cost_code = self:format_costs(costs, costs.packet_id)
    if cost_code then
        return cost_code
    end
    if costor then
        costor:cost(costs.reason)
    end
    return FRAME_SUCCESS
end

--掉落物品
function PacketComponent:execute_drop(drops, email)
    log_debug("[PacketComponent][execute_drop] player({}) drops({})!", self.id, drops)
    local alloter, allot_code = self:format_drops(drops, drops.packet_id)
    if allot_code then
        if email then
            self:send_template_mail(REISSUE, drops.drop)
            return FRAME_SUCCESS
        end
        return allot_code
    end
    if alloter then
        alloter:allot(drops.reason)
    end
    return FRAME_SUCCESS
end

-- 奖励掉落&&消耗
function PacketComponent:execute_cost_drop(drops, costs, packet_id, email)
    log_debug("[PacketComponent][execute_cost_drop] player({}) drops({}), costs({}) packet_id({}) email({})!",
             self.id, drops, costs, packet_id, email)
    local costor, cost_code = self:format_costs(costs, packet_id)
    if cost_code then
        log_err("[PacketComponent][execute_cost_drop] cost item falied player_id:{}, costs:{}",
            self.id, costs)
        return cost_code
    end
    local alloter, allot_code = self:format_drops(drops, packet_id)
    if allot_code then
        if email then
            if costor then
                costor:cost(costs.reason)
            end
            self:send_template_mail(REISSUE, drops.drop)
            return FRAME_SUCCESS
        end
        return allot_code
    end
    if alloter then
        alloter:allot(drops.reason)
    end
    if costor then
        costor:cost(costs.reason)
    end
    return FRAME_SUCCESS
end

-- 生产消耗
function PacketComponent:execute_produce_cost(costs, bank)
    if not costs then
        return ITEM_NOT_EXIST
    end

    local ret_code, bank_costs, item_costs = self:get_produce_cost(costs, bank)
    if ret_code ~= FRAME_SUCCESS then
        return ret_code
    end

    -- 扣除物品
    if next(bank_costs.cost) then
        local code = self:execute_cost(bank_costs)
        if code ~= FRAME_SUCCESS then
            return code
        end
    end

    if next(item_costs.cost) then
        local code = self:execute_cost(item_costs)
        if code ~= FRAME_SUCCESS then
            return code
        end
    end
    return FRAME_SUCCESS,bank_costs.cost
end

-- 生产掉落
function PacketComponent:execute_produce_drop(drops, extend)
    if not drops then
        return ITEM_NOT_EXIST
    end

    local ret_code, bank_drops, item_drops, last_drop = self:get_produce_drop(drops, extend)
    if ret_code ~= FRAME_SUCCESS then
        return ret_code
    end

    -- 仓库添加物品
    if next(bank_drops.drop) then
        local code = self:execute_drop(bank_drops)
        if code ~= FRAME_SUCCESS then
            return code
        end
    end

    -- 背包添加物品
    if next(item_drops.drop) then
        local code = self:execute_drop(item_drops)
        if code ~= FRAME_SUCCESS then
            return code
        end
    end
    return FRAME_SUCCESS,bank_drops.drop,last_drop
end

--添加储物箱
function PacketComponent:execute_add_packet(packet_id, capacity)
    log_debug("[PacketComponent][execute_add_packet] player({}) packet({}-{})!", self.id, packet_id, capacity)
    self:add_packet(packet_id, capacity)
    self:sync_packet(packet_id)
    return FRAME_SUCCESS
end

function PacketComponent:execute_check_packet(packet_id)
    log_debug("[PacketComponent][execute_check_packet] player({}) packet_id({})!", self.id, packet_id)
    local packet = self:get_packet(packet_id)
    if not packet then
        return FRAME_SUCCESS
    end
    if not packet:empty() then
        log_err("[PacketComponent][execute_check_packet] {} remove packet {} not empty!", self.id, packet_id)
        return PACKET_NOT_EMPTY
    end
    return FRAME_SUCCESS
end

function PacketComponent:execute_remove_packet(packet_id)
    log_debug("[PacketComponent][execute_remove_packet] player({}) packet_id({})!", self.id, packet_id)
    local packet = self:get_packet(packet_id)
    if not packet then
        return FRAME_SUCCESS
    end
    if not packet:empty() then
        log_err("[PacketComponent][execute_remove_packet] {} remove packet {} not empty!", self.id, packet_id)
        return PACKET_NOT_EMPTY
    end
    self:remove_packet(packet_id)
    log_err("[PacketServlet][execute_remove_packet] {} remove packet failed!", self.id)
    return FRAME_SUCCESS
end

function PacketComponent:execute_update_packet(packet_id, capacity)
    log_debug("[PacketComponent][execute_update_packet] player({}) packet({}-{})!", self.id, packet_id, capacity)
    local packet = self:get_packet(packet_id)
    if not packet then
        log_err("[PacketServlet][execute_update_packet] {} remove packet {} not exits!", self.id, packet_id)
        return PACKET_NOT_EXIST
    end
    self:update_capacity(packet, packet_id, capacity)
    return FRAME_SUCCESS
end

--结构件摆放
function PacketComponent:place_utensil(add_utensil, drops, costs, email)
    log_debug("[PacketComponent][place_utensil] player_id:{} add_utensil:{}", self.id, add_utensil)
    for proto_id, info in pairs(add_utensil or {}) do
        local type = info.type
        self:notify_event("on_place_utensil", proto_id, type)
    end
    return self:execute_cost_drop(drops, costs, email)
end

return PacketComponent
