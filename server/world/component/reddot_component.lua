
local otime                 = os.time
local log_debug             = logger.debug
local log_error             = logger.err
local log_warn             = logger.warn
local new_guid              = codec.guid_new
local config_mgr            = quanta.get("config_mgr")
local rodot_db              = config_mgr:init_table("ui_node_list", "id")
local protobuf_mgr          = quanta.get("protobuf_mgr")

local FRAME_SUCCESS         = protobuf_mgr:error_code("FRAME_SUCCESS")
local UTILITY_REDOT_PARAMS  = protobuf_mgr:error_code("UTILITY_REDOT_PARAMS")

-- 客户端奖励配置
local client_reward_db      = config_mgr:init_table("client_reward", "id")

local ReddotComponent   = mixin()

local red_dbprop = db_property(ReddotComponent, "player_reddots", true)
red_dbprop:store_values("reddots", {})             --红点数据

-- 红点加载
function ReddotComponent:on_db_player_reddots_load(data)
    self.reddots = data.reddots or {}
    return true
end

--红点功能---------------------------------------------------------------------------------------------------------------
-- 更新红点
function ReddotComponent:update_reddot(opts)
    local reddot_type = opts.reddot_type or 0
    local reddot_rid = opts.reddot_rid
    local amount = opts.amount or 0
    local count = opts.count or 0
    local overtime = opts.overtime or 0
    local customs = opts.customs or {}
    local reddot = self:get_reddot_by_rid(reddot_type, reddot_rid)
    if reddot then
        -- 支持增量覆盖
        if count == 0 then
            reddot.count = reddot.count + amount
            -- 支持数量覆盖
        else
            reddot.count = count
        end
        if customs then
            for _,v in pairs(customs) do
                reddot.customs[v] = 1
            end
        end
        if overtime > 0 then
            if reddot.overtime >= otime() then
                reddot.overtime = reddot.overtime + overtime
            else
                reddot.overtime = otime() + overtime
            end
        end
        -- 设置红点
        self:save_reddots_field(reddot.reddot_id, reddot)
        self:sync_reddot(reddot)
        -- 刷新红点
        self:refresh_reddot(reddot)
    end
end

-- 添加红点
function ReddotComponent:insert_reddot(opts)
    local oreddot = self:get_reddot_by_rid(opts.reddot_type, opts.reddot_rid)
    if not oreddot then
        local reddot_type = opts.reddot_type or 0
        local reddot_rid = opts.reddot_rid
        local amount = opts.amount or 0
        local count = opts.count or 0
        local overtime = opts.overtime or 0
        local customs = opts.customs or {}
        -- 创建uid
        local reddot_id = new_guid()
        local reddot = {}
        -- 使用增量值
        if count == 0 then
            reddot.count = amount
            -- 使用覆盖值
        else
            reddot.count = count
        end
        reddot.reddot_id = reddot_id
        reddot.reddot_rid = reddot_rid
        reddot.reddot_type = reddot_type
        reddot.customs = {}
        -- 设置数据
        if customs then
            for _,v in pairs(customs) do
                reddot.customs[v] = 1
            end
        end
        if overtime > 0 then
            reddot.overtime = otime() + overtime
        end

        -- 设置红点
        self:save_reddots_field(reddot.reddot_id, reddot)
        -- 同步红点
        self:sync_reddot(reddot)
        -- 刷新红点
        self:refresh_reddot(reddot)
    end
end

-- 添加红点
function ReddotComponent:add_reddot(opts)
    -- id验证
    if opts.reddot_rid <= 0 or not opts.reddot_rid then
        return UTILITY_REDOT_PARAMS
    end

    local oreddot = self:get_reddot_by_rid(opts.reddot_type, opts.reddot_rid)
    if oreddot then
        self:update_reddot(opts)
        return FRAME_SUCCESS
    else
        self:insert_reddot(opts)
        return FRAME_SUCCESS
    end
end

---@param rid number
---@param count number
---@param custom_id any
function ReddotComponent:add_new_redot(rid, count, custom_id)
    local conf = rodot_db:find_one(rid)
    if conf == nil or conf.type == nil then
        log_warn("[ReddotComponent][add_new_redot] config error : {}", rid)
        return
    end
    local opts = {
        reddot_type = conf.type,
        reddot_rid = rid,
        count = count,
        amount = conf.amount,
        customs = { custom_id }
    }
    if conf.time ~= nil and conf.time > 0 then
        opts.overtime = quanta.now + conf.time
    end

    self:add_reddot(opts)
end

---@param rid number
---@param child_id any
function ReddotComponent:del_reddot_by_rid(rid)
    local conf = rodot_db:find_one(rid)
    if conf == nil or conf.type == nil then
        log_error("[ReddotComponent][del_reddot_by_rid] not find config : {}", rid)
        return
    end
    local rotdot = self:get_reddot_by_rid(conf.type, rid)
    if rotdot ~= nil then
        return self:del_reddot(rotdot)
    end
end


-- 删除红点
function ReddotComponent:del_reddot(reddot)
    self:del_reddots_field(reddot.reddot_id)
    -- 下发删除红点
    self:send("NID_UTILITY_DEL_REDDOT_NTF", {reddot_id=reddot.reddot_id, reddot_rid=reddot.reddot_rid})
    log_debug("[ReddotComponent][del_reddot] del_reddot :{}", reddot)
end

-- 获取红点
function ReddotComponent:get_reddot_by_rid(reddot_type, reddot_rid)
    for _,v in pairs(self.reddots) do
        if v.reddot_type == reddot_type and v.reddot_rid == reddot_rid then
            return v
        end
    end
end

-- 刷新红点数据
function ReddotComponent:refresh_reddots()
    -- 清理数据
    for _, reddot in pairs(self.reddots or {}) do
        self:refresh_reddot(reddot)
    end
end

-- 刷新红点
function ReddotComponent:refresh_reddot(reddot)
    if ((reddot.count and reddot.count <= 0) and (not reddot.customs or not next(reddot.customs)))
            or (reddot.overtime and reddot.overtime ~= 0 and reddot.overtime >= quanta.now) then
        self:del_reddot(reddot)
    end
end

---@param rid number
---@param child_id any
function ReddotComponent:read_reddot_in_server(rid, child_id)
    local conf = rodot_db:find_one(rid)
    if conf == nil or conf.type == nil then
        log_debug("[ReddotComponent][read_reddot_in_server] not conf player:{} rid:{} child_id:{}", self.id, rid, child_id)
        return
    end
    self:read_reddot_server({
        reddot_type = conf.type,
        reddot_rid = rid,
        customs = { child_id }
    })
end

-- 已读红点(服务端调用)
function ReddotComponent:read_reddot_server(opts)
    local reddot_type = opts.reddot_type
    local reddot_rid = opts.reddot_rid
    local customs = opts.customs
    -- 获取对象
    local reddot = self:get_reddot_by_rid(reddot_type, reddot_rid)
    if not reddot then
        log_debug("[ReddotComponent][read_reddot_server] not reddot player:{} opts:{}", self.id, opts)
        return false
    end
    self:read_reddot(reddot.reddot_id, customs)
end

function ReddotComponent:transfer_reddot_by_rid(src_id, tar_id, customs)
    local src_conf = rodot_db:find_one(src_id)
    if src_conf == nil then
        log_error("[ReddotComponent][transfer_reddot_by_rid] not rodot conf : {}", src_id)
        return
    end
    local tar_conf = rodot_db:find_one(tar_id)
    if tar_conf == nil then
        log_error("[ReddotComponent][transfer_reddot_by_rid] not rodot conf : {}", tar_id)
        return
    end
    self:transfer_reddot({
        source_rid = src_conf.id,
        source_type = src_conf.type,

        target_rid = tar_conf.id,
        target_type = tar_conf.type,
        customs = customs
    })
end

-- 转移红点
function ReddotComponent:transfer_reddot(opts)
    local source_type = opts.source_type
    local source_rid = opts.source_rid
    local target_type = opts.target_type
    local target_rid = opts.target_rid
    local customs = opts.customs

    -- 清理原有红点数据
    self:read_reddot_server({
        reddot_type = source_type,
        reddot_rid = source_rid,
        customs = customs
    })
    -- 增加新的红点数据
    self:add_reddot({
        reddot_type = target_type,
        reddot_rid = target_rid,
        customs = customs
    })
end

-- 已读红点
function ReddotComponent:read_reddot(reddot_id, customs)
    local reddot = self.reddots[reddot_id]
    if reddot then
        -- 清理自定义数据
        local count = 0
        -- 清理指定数据
        if customs and next(customs) then
            for _,v in pairs(customs or {}) do
                if reddot.customs[v] then
                    count = count + 1
                    reddot.customs[v] = nil
                    if reddot.count > 0 then
                        reddot.count = reddot.count - 1
                    end
                end
            end
            -- 清理所有
        else
            for k,v in pairs(reddot.customs or {}) do
                count = count + 1
                reddot.customs[k] = nil
            end
        end
        -- 验证是否清理
        if not next(reddot.customs) then
            self:del_reddot(reddot)
        elseif count > 0 then
            -- 保存数据
            self:save_reddots_field(reddot.reddot_id, reddot)
            -- 同步红点
            self:sync_reddot(reddot)
        end
    end
end

-- 同步红点
function ReddotComponent:sync_reddot(reddot)
    local result = {}
    if reddot then
        result[reddot.reddot_id] = reddot
    else
        result = self:pack_reddots()
    end
    self:send("NID_UTILITY_SYNC_REDDOT_NTF", {reddots = result})
end

function ReddotComponent:pack_reddots()
    local reddots = {}
    for _, reddot in pairs(self.reddots) do
        reddots[reddot.reddot_id] = reddot
    end
    return reddots
end

-- 初始化客户端奖励红点
function ReddotComponent:init_clireward_reddot()
    for id, conf in client_reward_db:iterator() do
        self:refresh_clireward_reddot(conf)
    end
end

-- 刷新客户端奖励红点
function ReddotComponent:refresh_clireward_reddot(conf)
    if conf.enable and conf.reddot_type ~= 0 and conf.reddot_rid ~= 0 then
        -- 功能开放验证
        -- local c_sys_id = conf.c_sys_id
        -- if c_sys_id ~= 0 then
        --     local condition = self:get_openfunc_condition(c_sys_id)
        --     -- 条件验证
        --     if not condition_mgr:check_condition(self, condition) then
        --         return false
        --     end
        -- end
        if self:check_cliereward_count(conf) then
            local oreddot = self:get_reddot_by_rid(conf.reddot_type, conf.reddot_rid)
            if not oreddot or oreddot.count == 0 then
                local opts = {
                    reddot_type = conf.reddot_type,
                    reddot_rid = conf.reddot_rid,
                    count = 1
                }
                self:add_reddot(opts)
            end
        end
    end
    return true
end

return ReddotComponent