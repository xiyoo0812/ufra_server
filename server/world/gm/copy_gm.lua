--copy_gm.lua
local mfloor            = math.floor
local log_warn          = logger.warn
local log_debug         = logger.debug
local guid_new          = codec.guid_new

---@type nav_geometry
local nav_geometry      = import('common/nav_geometry.lua')

local gm_mgr            = quanta.get("gm_mgr")
local player_mgr        = quanta.get("player_mgr")
local copy_mgr          = quanta.get("copy_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")

local FRAME_SUCCESS     = protobuf_mgr:error_code("FRAME_SUCCESS")
local FRAME_FAILED      = protobuf_mgr:error_code("FRAME_FAILED")
local COPY_ALREADY_IN   = protobuf_mgr:error_code("COPY_ALREADY_IN")
local COPY_NOT_IN       = protobuf_mgr:error_code("COPY_NOT_IN")
local COPY_NOT_FOUND    = protobuf_mgr:error_code("COPY_NOT_FOUND")
local COPY_SCENE_ERROR  = protobuf_mgr:error_code("COPY_SCENE_ERROR")
local LEVEL_NOT_FOUND   = protobuf_mgr:error_code("COPY_NOT_FOUND_LEVEL")
local LEVEL_NO_NEXT     = protobuf_mgr:error_code("COPY_NO_NEXT_LEVEL")
local LEVEL_ERROR       = protobuf_mgr:error_code("COPY_LEVEL_ERROR")
local LEVEL_NO_CHANGE   = protobuf_mgr:error_code("COPY_LEVEL_NO_CHANGE")

local ENT_NPC           = protobuf_mgr:enum("entity_type", "ENTITY_NPC")

local GM_SERVICE        = quanta.enum("GMType", "SERVICE")
local F_FOLLOWER        = quanta.enum("Faction", "FOLLOWER")

local CopyGM = singleton()
function CopyGM:__init()
    local cmd_list = {
        {
            name = "enter_copy",
            gm_type = GM_SERVICE,
            group = "副本",
            desc = "进入副本",
            args = "player_id|integer copy_id|integer",
            example= "enter_copy 10008888 1",
            tip=""
        },
        {
            name = "exit_copy",
            gm_type = GM_SERVICE,
            group = "副本",
            desc = "退出(放弃)副本",
            args = "player_id|integer",
            example= "exit_copy 10008888",
            tip=""
        },
        {
            name = "create_follower",
            gm_type = GM_SERVICE,
            group = "副本",
            desc = "创建随从(NPC)",
            args = "player_id|integer proto_id|integer",
            example= "create_follower 10008888 30002",
            tip=""
        },
        {
            name = "enter_level",
            gm_type = GM_SERVICE,
            group = "副本",
            desc = "进入关卡",
            args = "player_id|integer level_id|integer",
            example= "enter_level 10008888 3",
            tip=""
        },
        {
            name = "forward_level",
            gm_type = GM_SERVICE,
            group = "副本",
            desc = "关卡推进(到下一关)",
            args = "player_id|integer",
            example= "forward_level 10008888",
            tip=""
        }
    }
    --注册GM
    gm_mgr:register_command(cmd_list)
    --注册GM回调
    for _, v in ipairs(cmd_list) do
        gm_mgr:add_listener(self, v.name)
    end
end

-- 进入副本
function CopyGM:enter_copy(player_id, copy_id)
    log_debug("[CopyGM][enter_copy]->player_id:{}, copy_id:{}", player_id, copy_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    local result = copy_mgr:enter_copy(player, player_id, copy_id)
    if result == COPY_NOT_FOUND then
        return "copy not found"
    end
    if result == COPY_SCENE_ERROR then
        return "player have no scene"
    end
    if result == COPY_ALREADY_IN then
        return "player is already in a copy scene"
    end
    if result == FRAME_SUCCESS then
        return "success"
    end
    return "failed, unkonwn error"
end

-- 退出副本
function CopyGM:exit_copy(player_id)
    log_debug("[CopyGM][exit_copy]->player_id: {}", player_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    local result = copy_mgr:exit_copy(player, player_id)
    if result == COPY_SCENE_ERROR then
        return "player have no scene"
    end
    if result == COPY_NOT_IN then
        return "player is not in a copy scene"
    end
    if result == FRAME_SUCCESS then
        return "success"
    end
    return "failed, unkonwn error"
end

-- 创建随从(NPC)
function CopyGM:create_follower(player_id, proto_id)
    log_debug("[CopyGM][create_follower]->player_id:{}, proto_id:{}", player_id, proto_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    local scene = player:get_scene()
    if not scene then
        return "player have no scene"
    end
    -- if not scene:is_copy() then
    --     log_warn('player[{}] is not in copy scene', player_id)
    --     return "player is not in a copy scene"
    -- end
    local conf = {
        copy_id = 1, -- TODO: copy_id from scene
        type = ENT_NPC,
        dynamic = true,
        refresh_time = 0,
        id = guid_new(),
        proto_id = proto_id,
        name = "test_follower",
        faction = F_FOLLOWER,
    }
    --计算玩家面前4米的位置
    local offset = 400
    local dir = nav_geometry.euler_angle_to_dir(player:get_dir_y() / 100)
    conf.pos = {
        mfloor(player:get_pos_x() + dir.x * offset),
        mfloor(player:get_pos_y() + dir.y),
        mfloor(player:get_pos_z() + dir.z * offset)
    }
    --面向玩家
    dir.x = -dir.x
    dir.z = -dir.z
    local euler_angle = mfloor(nav_geometry.dir_to_euler_angle(dir) * 100)
    conf.dir = { 0, euler_angle, 0 }
    local entity = scene:create_entity(conf)
    if not entity then
        log_warn("[CopyGM][create_follower] failed id:{} entityId:{} entityId:{} pos:{} dir:{}", conf.id, proto_id, nil, conf.pos, conf.dir)
        return "failed"
    end
    entity:set_owner_id(player_id)
    log_warn("[CopyGM][create_follower] success id:{} protoId:{} entityId:{} pos:{} dir:{}", conf.id, proto_id, entity:get_id(), conf.pos, conf.dir)
    return "success"
end

-- 进入关卡
function CopyGM:enter_level(player_id, level_id)
    log_debug("[CopyGM][enter_level]->player_id:{}, level_id:{}", player_id, level_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    local result = copy_mgr:switch_level(player, player_id, level_id)
    if result == COPY_SCENE_ERROR then
        return "player have no scene"
    end
    if result == COPY_NOT_IN then
        return "player is not in a copy scene"
    end
    if result == LEVEL_ERROR then
        return "level data error"
    end
    if result == LEVEL_NO_CHANGE then
        return "target level is same as current level"
    end
    if result == LEVEL_NOT_FOUND then
        return "level not found"
    end
    if result == FRAME_FAILED then
        return "player enter copy level failed"
    end
    if result == FRAME_SUCCESS then
        return "success"
    end
    return "failed, unkonwn error"
end

-- 推进关卡(进入到下一关卡)
function CopyGM:forward_level(player_id)
    log_debug("[CopyGM][forward_level]->player_id:{}", player_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    local result = copy_mgr:forward_copy_level(player, player_id)
    if result == COPY_SCENE_ERROR then
        return "player have no scene"
    end
    if result == COPY_NOT_IN then
        return "player is not in a copy scene"
    end

    if result == LEVEL_ERROR then
        return "level data error"
    end
    if result == LEVEL_NO_NEXT then
        return "no next level"
    end
    if result == FRAME_FAILED then
        return "player enter copy level failed"
    end
    if result == FRAME_SUCCESS then
        return "success"
    end
    return "failed, unkonwn error"
end

quanta.copy_gm = CopyGM()

return CopyGM
