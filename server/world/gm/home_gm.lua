--home_gm.lua
local log_debug     = logger.debug
local log_warn      = logger.warn
local log_err       = logger.err
local qfailed       = quanta.failed
local gm_mgr        = quanta.get("gm_mgr")
local player_mgr    = quanta.get("player_mgr")
local config_mgr    = quanta.get("config_mgr")
local object_fty    = quanta.get("object_fty")
local protobuf_mgr  = quanta.get("protobuf_mgr")


local ground_db    = config_mgr:init_table("ground", "id")
local formula_db   = config_mgr:init_table("formula", "id")
local blueprint_db = config_mgr:init_table("blueprint", "id")
local utensil_db   = config_mgr:init_table("utensil", "id")
local home_db      = config_mgr:init_table("home", "id")
local buff_db      = config_mgr:init_table("buff", "id")

local COST_GM       = protobuf_mgr:enum("cost_reason", "NID_COST_GM")
local OBTAIN_GM     = protobuf_mgr:enum("obtain_reason", "NID_OBTAIN_GM")
local BUILDING_PROCESS  = protobuf_mgr:enum("building_status", "BUILDING_PROCESS")
local BUILDING_RECAST   = protobuf_mgr:enum("building_status", "BUILDING_RECAST")

local GM_SERVICE   = quanta.enum("GMType", "SERVICE")

local HomeGM       = singleton()
function HomeGM:__init()
    local cmd_list = {
        {
            name = "unlock_drawing",
            gm_type = GM_SERVICE,
            group = "家园",
            desc = "解锁建筑",
            args =
            "player_id|integer value|integer",
            example =
            "unlock_drawing 1001014152 0",
            tip =
            "value=0 解锁所有建筑 非0解锁指定建筑(配置参考[建筑物家具表] utensil|building])"
        },
        {
            name = "unlock_ground",
            gm_type = GM_SERVICE,
            group = "家园",
            desc = "解锁地皮",
            args =
            "player_id|integer value|integer",
            example =
            "unlock_ground 1001014152 100",
            tip =
            "value=0 解锁所有地皮 非0解锁指定地皮(配置参考[地图信息表] ground)"
        },
        {
            name = "unlock_formula",
            gm_type = GM_SERVICE,
            group = "家园",
            desc = "解锁配方",
            args =
            "player_id|integer value|integer",
            example =
            "unlock_formula 1001014152 1010",
            tip =
            "value=0 解锁所有配方 非0解锁指定配方(配置参考[建筑物家具表 formula])"
        },
        {
            name = "unlock_blueprint",
            gm_type = GM_SERVICE,
            group = "家园",
            desc = "解锁蓝图",
            args =
            "player_id|integer value|integer",
            example =
            "unlock_blueprint 1001014152 5001",
            tip =
            "value=0 解锁所有所有蓝图 非0时解锁指定蓝图(配置参考[建筑物家具表] blueprint)"
        },
        {
            name = "building_complete",
            gm_type = GM_SERVICE,
            group = "家园",
            desc = "完成建筑",
            args = "player_id|integer",
            example= "building_complete 1001014152",
            tip="立即完成正在建造/升级的建造"
        },
        {
            name = "town_exp",
            gm_type = GM_SERVICE,
            group = "家园",
            desc = "繁荣度",
            args = "player_id|integer change_value|integer",
            example= "town_exp 1001014152 100",
            tip="正数添加 负数减少"
        },
        {
            name = "flush_entity",
            gm_type = GM_SERVICE,
            group = "家园",
            desc = "创建实体",
            args = "player_id|integer world_id|integer",
            example= "flush_entity 1001014152 91017",
            tip="world_id在world表查看"
        },
        {
            name = "block_complete",
            gm_type = GM_SERVICE,
            group = "家园",
            desc = "解锁地块完成",
            args = "player_id|integer",
            example= "block_complete 1001014152",
            tip="立即完成所有正在清理的地块"
        },
        {
            name = "fill_building_item",
            gm_type = GM_SERVICE,
            group = "家园",
            desc = "获得建筑所需材料",
            args = "player_id|integer proto_id|integer",
            example= "fill_building_item 1001014152 31005",
            tip="获得建筑所需材料"
        },
        {
            name = "fill_blueprint_item",
            gm_type = GM_SERVICE,
            group = "家园",
            desc = "获得组装台所需材料",
            args = "player_id|integer proto_id|integer",
            example= "fill_blueprint_item 1001014152 5013",
            tip="获得组装台所需材料"
        },
        {
            name = "building_add_buff",
            gm_type = GM_SERVICE,
            group = "家园",
            desc = "建筑添加buff",
            args = "player_id|integer buff_id|integer",
            example= "building_add_buff 1001014152 10110",
            tip="建筑添加buff"
        },
        {
            name = "active_collpoints",
            gm_type = GM_SERVICE,
            group = "家园",
            desc = "激活采集点",
            args = "player_id|integer collect_id|integer",
            example= "active_collpoints 1001014152 10110",
            tip="激活采集点"
        },
        {
            name = "add_sat",
            gm_type = GM_SERVICE,
            group = "家园",
            desc = "添加满意度",
            args = "player_id|integer add_value|integer",
            example= "add_sat 1001014152 1000",
            tip="添加满意度数值"
        }
    }
    --注册GM
    gm_mgr:register_command(cmd_list)
    --注册GM回调
    for _, v in ipairs(cmd_list) do
        gm_mgr:add_listener(self, v.name)
    end
end

--添加满意值
function HomeGM:add_sat(player_id, add_value)
    log_debug("[HomeGM][add_sat] player({}) add_value({})", player_id, add_value)
    local player = player_mgr:get_entity(player_id)
    if not player then
        log_err("[HomeGM][add_sat] player_id:{} not found add_value:{}", player_id, add_value)
        return "player not exist!"
    end
    local town = player:get_town()
    if not town then
        log_err("[HomeGM][add_sat] player_id:{} town not found add_value:{}", player_id, add_value)
        return "town not found!"
    end
    if not player:get_sat_open() then
        return "system not open"
    end

    town:add_sat(add_value)
    return "success"
end

--建筑添加buff
function HomeGM:building_add_buff(player_id, buff_id)
    log_debug("[HomeGM][building_add_buff] player({}) buff_id({})", player_id, buff_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    --检查buff合理性
    local conf = buff_db:find_one(buff_id)
    if not conf then
        log_err("[PartnerGM][partner_add_buff] conf {} not exist!", buff_id)
        return
    end

    local town = player:get_town()
    --遍历建筑
    local buildings = town:get_buildings()
    for _, building in pairs(buildings or {}) do
        building:add_buff(buff_id)
    end
    return "success"
end

--获得建筑所需材料
function HomeGM:fill_building_item(player_id, proto_id)
    log_debug("[HomeGM][fill_building_item] player({}) proto_id({})!", player_id, proto_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    local utensil_cfg = utensil_db:find_one(proto_id)
    local drop_items
    if utensil_cfg and object_fty:is_building(utensil_cfg) then
        drop_items = utensil_cfg.costs
    else
        return "proto_id is not building"
    end
    local drops = { drop = drop_items, reason = OBTAIN_GM }
    local ok, code = player:call_lobby("rpc_execute_drop", drops, true)
    if qfailed(code, ok) then
        log_debug("[HomeGM][fill_building_item] player({}) drop cost failed: {}!", player_id, code)
        return "failed"
    end
    return "success"
end

--获得组装所需材料
function HomeGM:fill_blueprint_item(player_id, proto_id)
    log_debug("[HomeGM][fill_blueprint_item] player({}) proto_id({})!", player_id, proto_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    local config = object_fty:find_blueprint(proto_id)
    local drop_items
    if config then
        drop_items = config.makings
    else
        return "proto_id is not blueprint"
    end
    local drops = { drop = drop_items, reason = OBTAIN_GM }
    local ok, code = player:call_lobby("rpc_execute_drop", drops, true)
    if qfailed(code, ok) then
        log_debug("[HomeGM][fill_blueprint_item] player({}) drop cost failed: {}!", player_id, code)
        return "failed"
    end
    return "success"
end

--解锁地块
function HomeGM:block_complete(player_id)
    log_debug("[HomeGM][block_complete] player({})!", player_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    local town = player:get_town()
    --遍历正在清理的地皮
    local grounds = town:get_grounds()
    local now_time = quanta.now
    for ground_id, ground in pairs(grounds or {}) do
        local blocks = ground:get_blocks()
        for block_id, time in pairs(blocks or {}) do
            if time == 0 then
                goto continue
            end
            town:unlock_block(player, ground_id, block_id, now_time)
            ::continue::
        end
    end
    return "success"
end

--刷新实体
function HomeGM:flush_entity(player_id, world_id)
    log_debug("[HomeGM][flush_entity] player({}), world_id({})!", player_id, world_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not online!"
    end
    local town = player:get_town()
    if not town then
        return "town not found"
    end
    local conf = home_db:find_one(world_id)
    if not conf then
        return "world_id invalid"
    end
    local entity = town:create_snap_entity(conf, true)
    if not entity then
        return "create failed"
    end
end

--增加繁荣度
function HomeGM:town_exp(player_id, change_value)
    log_debug("[HomeGM][town_exp] player({}), change_value({})!", player_id, change_value)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not online!"
    end
    local town = player:get_town()
    if not town then
        return "town not found"
    end
    if change_value > 0 then
        town:add_exp(player, change_value, OBTAIN_GM)
    else
        town:reduce_exp(player, -change_value, COST_GM)
    end
    return "success"
end


--完成建筑建造
function HomeGM:building_complete(player_id)
    log_debug("[HomeGM][building_complete] player({})!", player_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    local town = player:get_town()
    --遍历正在建造或升级的建筑
    local buildings = town:get_buildings()
    for _, building in pairs(buildings or {}) do
        if building:get_status() == BUILDING_RECAST or building:get_status() == BUILDING_PROCESS then
            building:save_timestamp(quanta.now)
            building:sync_changed()
        end
    end
    return "success"
end

--解锁建筑
function HomeGM:unlock_drawing(player_id, value)
    log_debug("[HomeGM][unlock_drawing] player({}), value({})!", player_id, value)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    -- 解锁指定建筑
    if value and value > 0 then
        if not utensil_db:find_one(value) then
            return 'ground not exist'
        end
        player:active_drawing(value)
        return "success"
    end
    -- 解锁所有建筑
    for id, conf in utensil_db:iterator() do
        player:active_drawing(id)
    end
    return "success"
end

--解锁地皮
function HomeGM:unlock_ground(player_id, value)
    log_debug("[HomeGM][unlock_ground] player({}), value({})!", player_id, value)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    local town = player:get_town()
    -- 解锁指定地皮
    if value and value > 0 then
        if not ground_db:find_one(value) then
            return 'ground not exist'
        end
        town:unlock_ground(player, value, 0)
        return "success"
    end
    -- 解锁所有地皮
    for id, conf in ground_db:iterator() do
        town:unlock_ground(player, id, 0)
    end
    return "success"
end

--解锁配方
function HomeGM:unlock_formula(player_id, value)
    log_debug("[HomeGM][unlock_formula] player({}), value({})!", player_id, value)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    local town = player:get_town()
    if not town then
        log_warn("[HomeGM][unlock_formula] {} formula({}) scene is null", player_id, value)
        return "scene not exist!"
    end
    -- 解锁指定配方
    if value and value > 0 then
        local formula = object_fty:find_formula(value)
        if not formula then
            log_warn("[HomeGM][unlock_formula] {} formula({}) not found", player_id, value)
            return "formula not exist!"
        end
        town:active_formula(value)
        return "success"
    end
    -- 解锁所有配方
    for id, conf in formula_db:iterator() do
        town:active_formula(id)
    end
    return "success"
end

--解锁蓝图
function HomeGM:unlock_blueprint(player_id, value)
    log_debug("[HomeGM][unlock_blueprint] player({}), value({})!", player_id, value)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    -- 解锁指定蓝图
    if value and value > 0 then
        local blueprint = object_fty:find_blueprint(value)
        if not blueprint then
            log_warn("[HomeGM][unlock_blueprint] {} blueprint({}) not found", player_id, value)
            return "blueprint not exist!"
        end
        player:active_blueprint(blueprint)
        return "success"
    end
    -- 解锁所有蓝图
    for id, blueprint in blueprint_db:iterator() do
        player:active_blueprint(blueprint)
    end
    return "success"
end

--激活采集点
function HomeGM:active_collpoints(player_id, value)
    log_debug("[HomeGM][active_collpoints] player({}), value({})!", player_id, value)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end

    local collect_points = object_fty:find_collpoint_id(value)
    if not collect_points then
        log_warn("[HomeGM][active_collpoints] {} collect_points({}) not found", player_id, value)
        return "collect_points not exist!"
    end

    local town = player:get_town()
    town:active_collpoint(value)
    return "success"
end

quanta.home_gm = HomeGM()
return HomeGM
