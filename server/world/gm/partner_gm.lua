--partner_gm.lua
local log_debug     = logger.debug
local log_err       = logger.err
local gm_mgr        = quanta.get("gm_mgr")
local player_mgr    = quanta.get("player_mgr")
local protobuf_mgr  = quanta.get("protobuf_mgr")
local config_mgr    = quanta.get("config_mgr")
local buff_db       = config_mgr:init_table("buff", "id")

local GM_SERVICE    = quanta.enum("GMType", "SERVICE")

local STATE_IDLE    = protobuf_mgr:enum("partner_state", "STATE_IDLE")
local STATE_SEND    = protobuf_mgr:enum("partner_state", "STATE_SEND")
local STATE_WORK    = protobuf_mgr:enum("partner_state", "STATE_WORK")
local OBTAIN_GM     = protobuf_mgr:enum("obtain_reason", "NID_OBTAIN_GM")

local PartnerGM = singleton()
function PartnerGM:__init()
    local cmd_list = {
        {
            name = "add_partner",
            gm_type = GM_SERVICE,
            group = "伙伴",
            desc = "添加伙伴",
            args = "player_id|integer npc_id|integer",
            example= "add_partner 1001014152 20004",
            tip=""
        },
        {
            name = "send_partner",
            gm_type = GM_SERVICE,
            group = "伙伴",
            desc = "派驻伙伴",
            args = "player_id|integer npc_id|integer building_id|integer pos|integer",
            example= "send_partner 1001014152 1 1 1",
            tip=""
        },
        {
            name = "recall_partner",
            gm_type = GM_SERVICE,
            group = "伙伴",
            desc = "召回伙伴",
            args = "player_id|integer npc_id|integer",
            example= "recall_partner 1001014152 117342329047614466",
            tip=""
        },

        {
            name = "reset_day_gift",
            gm_type = GM_SERVICE,
            group = "伙伴",
            desc = "重置送礼次数",
            args = "player_id|integer",
            example= "reset_day_gift 1001014152",
            tip="重置玩家所有伙伴今日送礼次数"
        },
        {
            name = "partner_add_buff",
            gm_type = GM_SERVICE,
            group = "伙伴",
            desc = "伙伴添加buff",
            args = "player_id|integer buff_id|integer part_proto_id|integer",
            example= "partner_add_buff 1001014152 10107 20001",
            tip="伙伴添加buff, part_proto_id=0 代表全体伙伴"
        },
        {
            name = "del_partner",
            gm_type = GM_SERVICE,
            group = "伙伴",
            desc = "遣散伙伴",
            args = "player_id|integer part_proto_id|integer",
            example= "del_partner 1001014152 20001",
            tip="遣散伙伴"
        },
        {
            name = "add_partner_affinity",
            gm_type = GM_SERVICE,
            group = "伙伴",
            desc = "添加伙伴好感度",
            args = "player_id|integer proto_id|integer add_num|integer",
            example= "add_partner_affinity 1001014152 20051 100",
            tip="添加伙伴好感度 player_id 伙伴原型ID 添加好感度数值"
        },
    }
    --注册GM
    gm_mgr:register_command(cmd_list)
    --注册GM回调
    for _, v in ipairs(cmd_list) do
        gm_mgr:add_listener(self, v.name)
    end
end

--添加伙伴好感度
function PartnerGM:add_partner_affinity(player_id, proto_id, num)
    log_debug("[PartnerGM][add_partner_affinity] player_id:{} partner:{}, num:{}", player_id, proto_id, num)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    local partner = player:get_partner_map()[proto_id]
    if partner == nil then
        return "partner not found"
    end
    partner:add_affinity(num)
    player:update_partner(partner:get_id(), partner)
    return "success"
end

--遣散伙伴
function PartnerGM:del_partner(player_id, proto_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    local partner = player:get_partner_map()[proto_id]
    if partner == nil then
        return "partner not found"
    end
    --通知遣散
    player:drive_partner(partner)
    return "success"
end

--伙伴添加buff
function PartnerGM:partner_add_buff(player_id, buff_id, part_proto_id)
    log_debug("[PartnerGM][partner_add_buff] player({}) buff_id({}) part_proto_id({})", player_id, buff_id, part_proto_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    --检查buff合理性
    local conf = buff_db:find_one(buff_id)
    if not conf then
        log_err("[PartnerGM][partner_add_buff] conf {} not exist!", buff_id)
        return
    end

    if part_proto_id == 0 then
        for _, partner in pairs(player:get_partner_map()) do
            partner:add_buff(buff_id)
        end
        return "success"
    end
    local partner = player:get_partner_map()[part_proto_id]
    if partner == nil then
        return "partner not found"
    end
    partner:add_buff(buff_id)
    return "success"
end

--重置玩家所有伙伴今日送礼次数
function PartnerGM:reset_day_gift(player_id)
    log_debug("[PartnerGM][recruit_gift_count] player({}) ", player_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    player:reset_day_gift()
    return "success"
end

--召回伙伴
function PartnerGM:recall_partner(player_id, npc_id)
    log_debug("[PartnerGM][recall_partner] player({}), npc_id({})", player_id, npc_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    --npc信息
    local town = player:get_town()
    local npc = player:get_partner(npc_id)
    if not npc then
        return "partner not found"
    end
    --建筑
    local building_id = npc:get_building_id()
    if building_id == 0 or npc:get_state() == STATE_WORK then
        return "busy"
    end
    local building = town:get_building(building_id)
    --通知建筑
    building:recall_partner(npc)
    --召回
    player:recall_partner(npc)
end

--派驻npc
function PartnerGM:send_partner(player_id, npc_id, building_id, pos)
    log_debug("[PartnerGM][send_partner] player({}), npc_id({}), building_id:{} pos:{}!", player_id, npc_id, building_id, pos)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end

    local town = player:get_town()
    --建筑信息
    local building = town:get_building(building_id)
    if not building then
        return "no building"
    end
    --位置校验
    local valid_pos, onpc_id = building:is_valid_pos(pos)
    if not valid_pos then
        return "pos invalid"
    end
    --旧npc校验
    local onpc = player:get_partner(onpc_id)
    if onpc then
        if onpc:get_state() ~= STATE_SEND then
            return "old npc busy"
        end
    end
    --新npc信息
    local nnpc = player:get_partner(npc_id)
    if not nnpc then
        return "npc not found"
    end
    --npc状态
    if nnpc:get_building_id() ~= 0 and nnpc:get_state() ~= STATE_IDLE then
        return "npc state error"
    end
    if onpc then
        building:recall_partner(onpc)
        player:recall_partner(onpc)
    end
    --派遣npc
    building:send_partner(pos, nnpc)
    player:send_partner(nnpc, building, pos)
end

--添加npc
function PartnerGM:add_partner(player_id, npc_id)
    log_debug("[PartnerGM][add_partner] player({}), npc_id({})!", player_id, npc_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end

    local has = player:has_partner(npc_id)
    if has then
        return "has npc"
    end
    local partner = player:recv_partner(npc_id, OBTAIN_GM)
    if not partner then
        return "failed!"
    end
    return "success"
end

quanta.partner_gm = PartnerGM()
return PartnerGM
