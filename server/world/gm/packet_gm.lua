--packet_gm.lua
local log_debug     = logger.debug

local gm_mgr        = quanta.get("gm_mgr")
local config_mgr    = quanta.get("config_mgr")
local player_mgr    = quanta.get("player_mgr")
local protobuf_mgr  = quanta.get("protobuf_mgr")

local COST_GM       = protobuf_mgr:enum("cost_reason", "NID_COST_GM")
local OBTAIN_GM     = protobuf_mgr:enum("obtain_reason", "NID_OBTAIN_GM")
local PACKET_ITEM   = protobuf_mgr:enum("packet_type", "PACKET_ITEM")
local PACKET_BANK   = protobuf_mgr:enum("packet_type", "PACKET_BANK")


local utility_db    = config_mgr:get_table("utility")
local BAG_CAPACITY  = utility_db:find_number("value", "bag_capacity")
local BAK_CAPACITY  = utility_db:find_number("value", "bank_capacity")
local item_db      = config_mgr:init_table("item", "id")
local MAX_CAPACITY  = utility_db:find_number("value", "max_capacity")

local PacketGM = singleton()
function PacketGM:__init()
    local cmd_list = {
        {
            name = "add_item",
            group = "道具",
            desc = "添加物品",
            args = "player_id|integer item_id|integer count|integer",
            example= "add_item 10201221 100101 100",
            tip=""
        },
        {
            name = "del_item",
            group = "道具",
            desc = "删除物品",
            args = "player_id|integer item_id|integer count|integer",
            example= "del_item 10201221 100101 100",
            tip=""
        },
        {
            name = "add_bank",
            group = "道具",
            desc = "添加物品(仓库)",
            args = "player_id|integer item_id|integer count|integer",
            example= "add_bank 10201221 100101 100",
            tip=""
        },
        {
            name = "del_bank",
            group = "道具",
            desc = "删除物品(仓库)",
            args = "player_id|integer item_id|integer count|integer",
            example= "del_bank 10201221 100101 100",
            tip=""
        },
        {
            name = "unlock_backpack",
            group = "道具",
            desc = "解锁背包",
            args = "player_id|integer value|integer",
            example = "unlock_backpack 1001014152 30",
            tip = "value=0 解锁所有格子 非0解锁指定容量格子(配置参考[通用常量表] utility](初始容量 30 最大容量 80))"
        },
        {
            name = "full_backpack",
            group = "道具",
            desc = "背包搞满",
            args = "player_id|integer",
            example = "full_backpack 1001014152",
            tip = ""
        },
        {
            name = "full_bank",
            group = "道具",
            desc = "仓库搞满",
            args = "player_id|integer",
            example = "full_bank 1001014152",
            tip = ""
        },
        {
            name = "clear_backpack",
            group = "道具",
            desc = "清理背包",
            args = "player_id|integer",
            example = "clear_backpack 1001014152",
            tip = ""
        },
        {
            name = "clear_bank",
            group = "道具",
            desc = "清理仓库",
            args = "player_id|integer",
            example = "clear_bank 1001014152",
            tip = ""
        },
    }
    --注册GM
    gm_mgr:register_command(cmd_list, quanta.service)
    --注册GM回调
    for _, v in ipairs(cmd_list) do
        gm_mgr:add_listener(self, v.name)
    end
end

-- 添加物品
function PacketGM:add_item(player_id, item_id, count)
    log_debug("[PacketGM][add_item]->player_id:{}, item_id:{}, count:{}", player_id, item_id, count)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    if not player:allot_item(item_id, count, OBTAIN_GM, 0, PACKET_ITEM) then
        return "add_item failed!"
    end
    return "success"
end

-- 删除物品
function PacketGM:del_item(player_id, item_id, count)
    log_debug("[PacketGM][del_item]->player_id:{}, item_id:{}, count:{}", player_id, item_id, count)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    if not player:cost_item(item_id, count, COST_GM, PACKET_ITEM) then
        return "del_item failed!"
    end
    return "success"
end

-- 添加物品(仓库)
function PacketGM:add_bank(player_id, item_id, count)
    log_debug("[PacketGM][add_bank]->player_id:{}, item_id:{}, count:{}", player_id, item_id, count)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    if not player:allot_item(item_id, count, OBTAIN_GM, 0, PACKET_BANK) then
        return "add_item failed!"
    end
    return "success"
end

-- 删除物品(仓库)
function PacketGM:del_bank(player_id, item_id, count)
    log_debug("[PacketGM][del_bank]->player_id:{}, item_id:{}, count:{}", player_id, item_id, count)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    if not player:cost_item(item_id, count, COST_GM, PACKET_BANK) then
        return "del_bank failed!"
    end
    return "success"
end

--unlock_backpack
function PacketGM:unlock_backpack(player_id, value)
    log_debug("[PlayerGM][unlock_backpack] player({}), value({})!", player_id, value)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    -- 获取道具背包
    local packet = player:get_packet(PACKET_ITEM)
    if not packet then
        return "packet not exist!"
    end
    if value <= 0 then
        value = MAX_CAPACITY
    end
    local capacity = packet:get_capacity() + value
    if capacity >= MAX_CAPACITY then
        capacity = MAX_CAPACITY
    end
    -- 更新容量
    player:update_capacity(packet, PACKET_ITEM, capacity)
    return "success"
end

-- 背包搞满
function PacketGM:full_backpack(player_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    local add_count = 0
    for _, conf in item_db:iterator() do
        local has_count = player:get_item_num(conf.id, PACKET_ITEM)
        if has_count > 0 then
            goto continue
        end
        if not player:allot_item(conf.id, 1, OBTAIN_GM, 0, PACKET_ITEM) then
            return "falied"
        end
        --单次不能添加太多
        add_count = add_count + 1
        if add_count > 500 then
            return "success"
        end
        ::continue::
    end
    return "success"
end


-- 仓库搞满
function PacketGM:full_bank(player_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    local add_count = 0
    for _, conf in item_db:iterator() do
        local has_count = player:get_item_num(conf.id, PACKET_BANK)
        if has_count > 0 then
            goto continue
        end
        if not player:allot_item(conf.id, 1, OBTAIN_GM, 0, PACKET_BANK) then
            return "falied"
        end
        --单次不能添加太多
        add_count = add_count + 1
        if add_count > 500 then
            return "success"
        end
        ::continue::
    end
    return "success"
end

-- 清理背包
function PacketGM:clear_backpack(player_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    player:remove_packet(PACKET_ITEM)
    player:add_packet(PACKET_ITEM, BAG_CAPACITY)
    return "success"
end

-- 清理仓库
function PacketGM:clear_bank(player_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    player:remove_packet(PACKET_BANK)
    player:add_packet(PACKET_BANK, BAK_CAPACITY)
    return "success"
end

quanta.packet_gm = PacketGM()

return PacketGM
