--gm_mgr.lua
import("basic/cmdline.lua")
import("agent/online_agent.lua")

local Listener          = import("basic/listener.lua")
local HttpServer        = import("network/http_server.lua")

local log_debug         = logger.debug
local tunpack           = table.unpack

local cmdline           = quanta.get("cmdline")
local update_mgr        = quanta.get("update_mgr")

local LOCAL             = quanta.enum("GMType", "LOCAL")
local SUCCESS           = quanta.enum("KernCode", "SUCCESS")

local GM_Mgr = singleton(Listener)
local prop = property(GM_Mgr)
prop:reader("http_server", nil)
prop:reader("services", {})
prop:reader("gm_page", "")
prop:reader("gm_status", false)

function GM_Mgr:__init()
    --创建HTTP服务器
    local server = HttpServer(environ.get("QUANTA_GM_HTTP"))
    service.modify_host(server:get_port())
    self.http_server = server
    --是否开启GM功能
    if environ.status("QUANTA_GM_SERVER") then
        self.gm_status = true
        self:register_webgm()
    end
    --定时更新
    update_mgr:attach_second5(self)
    self:on_second5()
end

--外部注册post请求
function GM_Mgr:register_post(url, handler, target)
    self.http_server:register_post(url, handler, target)
end

--外部注册get请求
function GM_Mgr:register_get(url, handler, target)
    self.http_server:register_get(url, handler, target)
end

--取消注册请求
function GM_Mgr:unregister_url(url)
    self.http_server:unregister(url)
end

--定时更新
function GM_Mgr:on_second5()
    self.gm_page = import("world/gm/gm_page.lua")
end

-- 事件请求
function GM_Mgr:on_register_command(command_list, service_id)
    self:rpc_register_command(command_list, service_id)
    return SUCCESS
end

function GM_Mgr:register_webgm()
    self:register_get("/", "on_gm_page", self)
    self:register_get("/gmlist", "on_gmlist", self)
    self:register_post("/command", "on_command", self)
    self:register_post("/message", "on_message", self)
end

function GM_Mgr:unregister_webgm()
    self:unregister_url("/")
    self:unregister_url("/gmlist")
    self:unregister_url("/command")
    self:unregister_url("/message")
end

--切换gm状态
function GM_Mgr:gm_switch(status)
    self.gm_status = status
    if self.gm_status then
        self:register_webgm()
    else
        self:unregister_webgm()
    end
    return status
end

--rpc请求
---------------------------------------------------------------------
--注册GM
function GM_Mgr:register_command(command_list, service_id)
    --同服务只执行一次
    if service_id and self.services[service_id] then
        return
    end
    for _, cmd in pairs(command_list) do
        local gm_type = LOCAL
        cmdline:register_command(cmd.name, cmd.args, cmd.desc, gm_type, cmd.group, cmd.tip, cmd.example, service_id)
    end
    if service_id then
        self.services[service_id] = true
    end
    return SUCCESS
end

--执行gm, command：string
function GM_Mgr:execute_command(command)
    log_debug("[GM_Mgr][execute_command] command: {} gm_status:{}", command, self.gm_status)
    local res = self:exec_command(command)
    return SUCCESS, res
end

--执行gm, message: table
function GM_Mgr:execute_message(message)
    log_debug("[GM_Mgr][execute_message] message: {} gm_status:{}", message, self.gm_status)
    local res = self:exec_message(message)
    return SUCCESS, res
end

--http 回调
----------------------------------------------------------------------
--gm_page
function GM_Mgr:on_gm_page(url, body, params)
    return self.gm_page, {["Access-Control-Allow-Origin"] = "*", ["X-Frame-Options"]= "ALLOW_FROM"}
end

--gm列表
function GM_Mgr:on_gmlist(url, body, params)
    return { text = "GM指令(World)", nodes = cmdline:get_displays() }
end

--后台GM调用，字符串格式
function GM_Mgr:on_command(url, body)
    log_debug("[GM_Mgr][on_command] body: {}", body)
    return self:exec_command(body.data)
end

--后台GM调用，table格式
function GM_Mgr:on_message(url, body)
    log_debug("[GM_Mgr][on_message] body: {}", body)
    return self:exec_message(body.data)
end

-------------------------------------------------------------------------
--参数分发预处理
function GM_Mgr:dispatch_pre_command(fmtargs)
    if not self.gm_status then
        return {code = 1, msg = 'gm_status is false'}
    end
    return self:dispatch_command(fmtargs.args, fmtargs.type, fmtargs.service)
end

--后台GM执行，字符串格式
function GM_Mgr:exec_command(command)
    local fmtargs, err = cmdline:parser_command(command)
    if not fmtargs then
        return { code = 1, msg = err }
    end
    return self:dispatch_pre_command(fmtargs)
end

--后台GM执行，table格式
--message必须有name字段，作为cmd_name
function GM_Mgr:exec_message(message)
    local fmtargs, err = cmdline:parser_data(message)
    if not fmtargs then
        return { code = 1, msg = err }
    end
    return self:dispatch_pre_command(fmtargs)
end

--分发command
function GM_Mgr:dispatch_command(cmd_args, gm_type, service_id)
    local callback = {
        [LOCAL]     = GM_Mgr.exec_local_cmd,
    }
    return callback[gm_type](self, service_id, tunpack(cmd_args))
end

--local command
function GM_Mgr:exec_local_cmd(service_id, cmd_name, ...)
    local ok, code, res = tunpack(self:notify_listener(cmd_name, ...))
    if not ok then
        return { code = code, msg = res }
    end
    return { code = 0, msg = "success" }
end

quanta.gm_mgr = GM_Mgr()

return GM_Mgr