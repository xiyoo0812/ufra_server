--player_gm.lua
local log_debug     = logger.debug
local tinsert       = table.insert

local gm_mgr        = quanta.get("gm_mgr")
local player_mgr    = quanta.get("player_mgr")
local config_mgr    = quanta.get("config_mgr")
--TODO_FIXlocal task_mgr      = quanta.get("task_mgr")
local event_mgr     = quanta.get("event_mgr")

-- 新手引导配置
local tutorial_db   = config_mgr:init_table("tutorial", "id")
local level_db      = config_mgr:init_table("player_level", "level")

local GM_SERVICE    = quanta.enum("GMType", "SERVICE")

local PlayerGM      = singleton()
local prop          = property(PlayerGM)
prop:reader("newbee_max_id_arr", {}) --所有引导id
function PlayerGM:__init()
    local cmd_list = {
        {
            name = "set_hp",
            group = "属性",
            desc = "设置HP",
            args = "player_id|integer value|integer",
            example = "",
            tip = ""
        },
        {
            name = "set_exp",
            group = "属性",
            desc = "设置经验",
            args = "player_id|integer value|integer",
            example = "",
            tip = ""
        },
        {
            name = "set_lv",
            group = "属性",
            desc = "设置等级",
            args = "player_id|integer level|integer",
            example = "",
            tip = ""
        },
        {
            name = "set_coin",
            group = "属性",
            desc = "设置金币",
            args = "player_id|integer value|integer",
            example = "",
            tip = ""
        },
        {
            name = "set_diamond",
            group = "属性",
            desc = "设置钻石",
            args = "player_id|integer value|integer",
            example = "",
            tip = ""
        },
        {
            name = "get_attr",
            group = "属性",
            desc = "获取属性",
            args = "player_id|integer attr_id|integer",
            example = "",
            tip = ""
        },
        {
            name = "set_attr",
            group = "属性",
            desc = "设置属性",
            args = "player_id|integer attr_id|integer value|integer",
            example = "",
            tip = ""
        },
        {
            name = "skip_boot",
            group = "新手",
            desc = "跳过引导",
            args = "player_id|integer value|integer",
            example = "unlock_backpack 1001014152 100",
            tip = "value=0 跳过所有引导 非0跳过指定引导(配置参考[新手引导] tutorial])"
        },
        {
            name = "next_day",
            group = "时间",
            desc = "在线现实时间跨天",
            args = "player_id|integer week|integer",
            example = "",
            tip = "week=1 触发周刷新 现实时间跨天"
        },
        {
            name = "next_game_day",
            group = "时间",
            desc = "在线游戏时间跨天",
            args = "player_id|integer",
            example = "",
            tip = "注意：这是游戏时间跨天"
        },
        {
            name = "add_shop",
            group = "商店",
            desc = "添加一个新商店",
            args = "player_id|integer shop_id|integer shop_lv|integer",
            example = "玩家id 商店id 商店等级",
        },
        {
            name = "kick_all",
            gm_type = GM_SERVICE,
            group = "运维",
            desc = "踢全部玩家下线",
            args = "",
            example = "",
            tip = ""
        },
        {
            name = "kick_out",
            gm_type = GM_SERVICE,
            group = "运维",
            desc = "踢玩家下线",
            args = "player_id|integer",
            example = "",
            tip = ""
        },
        {
            name = "friend_recommend",
            gm_type = GM_SERVICE,
            group = "运维",
            desc = "推荐好友",
            args = "player_id|integer",
            example = "",
            tip = ""
        }
    }
    --注册GM
    gm_mgr:register_command(cmd_list)
    --注册GM回调
    for _, v in ipairs(cmd_list) do
        gm_mgr:add_listener(self, v.name)
    end
    -- 初始新手引导配置
    self:init_newbee_id_conf()
end

-- 初始化新手引导配置
function PlayerGM:init_newbee_id_conf()
    -- 整理出所有引导id
    local newbee_id_arr = {}
    for id, conf in tutorial_db:iterator() do
        tinsert(newbee_id_arr, id)
        if conf.next_id then
            tinsert(newbee_id_arr, conf.next_id)
        end
    end
    -- 计算每个引导项最大值
    for _, v in pairs(newbee_id_arr) do
        local n1 = v // 100
        local item_number = self.newbee_max_id_arr[n1]
        if item_number then
            if v > item_number then
                self.newbee_max_id_arr[n1] = v
            end
        else
            self.newbee_max_id_arr[n1] = v
        end
    end
end

--推荐好友
function PlayerGM:friend_recommend(player_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    player:sync_mirror()
    return "success"
end

--kick_all
function PlayerGM:kick_all()
    log_debug("[PlayerGM][kick_all] all player kickout!")
    player_mgr:kick_all()
    return "success"
end

--kick_out
function PlayerGM:kick_out(player_id)
    log_debug("[PlayerGM][kick_out] player({}) kickout!", player_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    player_mgr:kick_out(player, player_id)
    return "success"
end

--get_attr
function PlayerGM:get_attr(player_id, attr_id)
    log_debug("[PlayerGM][get_attr] player({}), attr_id({})!", player_id, attr_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    local value = player:get_attr(attr_id)
    if not value then
        return "attr_id not exist!"
    end
    return value
end

--set_attr
function PlayerGM:set_attr(player_id, attr_id, value)
    log_debug("[PlayerGM][set_attr] player({}), attr_id({}), value({})!", player_id, attr_id, value)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    if not player:set_attr(attr_id, value) then
        return "attr_id not exist!"
    end
    return "success"
end

--set_exp
function PlayerGM:set_exp(player_id, value)
    log_debug("[PlayerGM][set_exp] player({}), value({})!", player_id, value)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    player:set_exp(value)
    return "success"
end

--set_hp
function PlayerGM:set_hp(player_id, value)
    log_debug("[PlayerGM][set_hp] player({}), value({})!", player_id, value)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    player:set_hp(value)
    return "success"
end

--set_coin
function PlayerGM:set_coin(player_id, value)
    log_debug("[PlayerGM][set_coin] player({}), value({})!", player_id, value)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    player:set_coin(value)
    return "success"
end

--set_diamond
function PlayerGM:set_diamond(player_id, value)
    log_debug("[PlayerGM][set_diamond] player({}), value({})!", player_id, value)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    player:set_diamond(value)
    return "success"
end

--skip_boot
function PlayerGM:skip_boot(player_id, value)
    log_debug("[PlayerGM][skip_boot] player({}), value({})!", player_id, value)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    -- 更新指定引导
    if value >= 100 and value < 10000 then
        -- 更新新手数据
        player:update_newbee(value)
        return "success"
    end
    -- 设置所有引导
    for _, v in pairs(self.newbee_max_id_arr) do
        -- 更新新手数据
        player:update_newbee(v)
    end
    return "success"
end

function PlayerGM:set_lv(player_id, level)
    log_debug("[PlayerGM][set_lv] player({}), level({})!", player_id, level)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    local conf = level_db:find_one(level)
    if not conf then
        return "level config not find : " .. level
    end
    player:set_exp(0)
    player:set_level(level)
    return "success"
end

function PlayerGM:next_day(player_id, week)
    local week_flush = (week == 1)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    player:day_update(week_flush)
    event_mgr:notify_trigger("on_day_update", week_flush)
    return "success"
end

function PlayerGM:next_game_day(player_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    event_mgr:notify_trigger("on_game_day_update")
    return "success"
end

function PlayerGM:add_shop(player_id, shop_id, shop_lv)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    player:on_shop_finish({
        shop_id = shop_id,
        shop_level = shop_lv
    })
    return "success"
end

quanta.player_gm = PlayerGM()

return PlayerGM
