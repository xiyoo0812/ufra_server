--battle_gm.lua
local mfloor        = math.floor
local log_warn      = logger.warn
local log_debug     = logger.debug
local guid_new      = codec.guid_new

---@type nav_geometry
local nav_geometry  = import('common/nav_geometry.lua')

local gm_mgr        = quanta.get("gm_mgr")
local player_mgr    = quanta.get("player_mgr")

local GM_SERVICE   = quanta.enum("GMType", "SERVICE")

local BattleGM = singleton()
function BattleGM:__init()
    local cmd_list = {
        {
            name = "create_monster",
            gm_type = GM_SERVICE,
            group = "战斗",
            desc = "创建怪物",
            args = "player_id|integer proto_id|integer",
            example= "create_monster 10008888 30002",
            tip=""
        },
        {
            name = "ignore_speed",
            gm_type = GM_SERVICE,
            group = "战斗",
            desc = "忽略速度(限制)",
            args = "player_id|integer ignore|integer",
            example= "ignore_speed 10008888 1",
            tip=""
        },
    }
    --注册GM
    gm_mgr:register_command(cmd_list)
    --注册GM回调
    for _, v in ipairs(cmd_list) do
        gm_mgr:add_listener(self, v.name)
    end
end

-- 创建怪物
function BattleGM:create_monster(player_id, proto_id)
    log_debug("[BattleGM][create_monster]->player_id:{}, proto_id:{}", player_id, proto_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    local scene = player:get_scene()
    if not scene then
        return "player have no scene"
    end
    local conf = {
        type = 3,
        dynamic = 1,
        refresh_time = 200,
        id = guid_new(),
        can_wander = false,
        proto_id = proto_id,
        name = "test_battle_monster"
    }
    --计算玩家面前4米的位置
    local offset = 400
    local dir = nav_geometry.euler_angle_to_dir(player:get_dir_y() / 100)
    conf.pos = {
        mfloor(player:get_pos_x() + dir.x * offset),
        mfloor(player:get_pos_y() + dir.y),
        mfloor(player:get_pos_z() + dir.z * offset)
    }
    --面向玩家
    dir.x = -dir.x
    dir.z = -dir.z
    local euler_angle = mfloor(nav_geometry.dir_to_euler_angle(dir) * 100)
    conf.dir = { 0, euler_angle, 0 }
    local entity = scene:create_entity(conf)
    if not entity then
        log_warn("[BattleGM][create_monster] failed id:{} entityId:{} entityId:{} pos:{} dir:{}", conf.id, proto_id, nil, conf.pos, conf.dir)
        return "failed"
    end
    log_warn("[BattleGM][create_monster] success id:{} protoId:{} entityId:{} pos:{} dir:{}", conf.id, proto_id, entity:get_id(), conf.pos, conf.dir)
    return "success"
end

function BattleGM:ignore_speed(player_id, ignore)
    log_debug("[BattleGM][ignore_speed]->player_id:%s, ignore:%s", player_id, ignore)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    local scene = player:get_scene()
    if not scene then
        return "player have no scene"
    end
    player:set_ignore_speed_limit(ignore == 1)
    return "success"
end

quanta.battle_gm = BattleGM()

return BattleGM
