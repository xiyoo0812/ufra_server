
local log_debug     = logger.debug
local gm_mgr        = quanta.get("gm_mgr")
local player_mgr    = quanta.get("player_mgr")
local task_mgr      = quanta.get("task_mgr")


local TaskGM = singleton()
function TaskGM:__init()
    local cmd_list = {
        {
            name = "finish_task",
            group = "任务",
            desc = "完成任务",
            args = "player_id|integer task_id|integer",
            example = "",
            tip = ""
        },
        {
            name = "accept_task",
            group = "任务",
            desc = "接取任务",
            args = "player_id|integer task_id|integer",
            example = "",
            tip = ""
        }
    }
    gm_mgr:register_command(cmd_list)
    for _, v in ipairs(cmd_list) do
        gm_mgr:add_listener(self, v.name)
    end
end

function TaskGM:finish_task(player_id, task_id)
    log_debug("[TaskGM][finish_task] player({}), task_id({})!", player_id, task_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    local task = player:get_task(task_id)
    if not task then
        return "task not exist!"
    end
    task:complete()
    return "success"
end

function TaskGM:accept_task(player_id, task_id)
    log_debug("[TaskGM][accept_task] player({}), task_id({})!", player_id, task_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return "player not exist!"
    end
    local task = player:get_task(task_id)
    if task then
        return "success"
    end
    local story = task_mgr:create_task(player, task_id)
    player:receive_task(story, task_id)
    player:sync_task(story)
    return "success"
end

quanta.task_gm = TaskGM()

return TaskGM