--scene.lua

local log_err            = logger.err
local log_warn           = logger.warn
local log_info           = logger.info
local log_debug          = logger.debug
local tunpack            = table.unpack
local qxpcall            = quanta.xpcall
local mdistance          = qmath.distance
local tinsert            = table.insert
local msqrt              = math.sqrt
local env_number         = environ.number

local scene_mgr          = quanta.get("scene_mgr")
local event_mgr          = quanta.get("event_mgr")
local object_fty         = quanta.get("object_fty")
local config_mgr         = quanta.get("config_mgr")
local thread_mgr         = quanta.get("thread_mgr")
local protobuf_mgr       = quanta.get("protobuf_mgr")
local store_mgr          = quanta.get("store_mgr")

local sow_area_db        = config_mgr:init_table("sow_area", "map_id", "id")
local sow_entity_db      = config_mgr:init_table("sow_area_entity", "map_id", "id")
local newbee_db          = config_mgr:init_table("newbee", "key")

local BORN_MAP           = newbee_db:find_integer("value", "born_map_id")
local FRAME_SUCCESS      = protobuf_mgr:error_code("FRAME_SUCCESS")
local ACTION_EMPTY       = protobuf_mgr:error_code("SCENE_ACTION_EMPTY")
local DYING_CANT_SKILL   = protobuf_mgr:error_code("SCENE_DYING_CANT_SKILL")
local NO_STAMINA         = protobuf_mgr:error_code("SCENE_NO_STAMINA")

local COPY               = quanta.enum("MapType", "COPY")
local PUBLIC             = quanta.enum("MapType", "PUBLIC")
local ACTION_MOVE        = quanta.enum("ActionType", "Move")
local ACTION_JUMP        = quanta.enum("ActionType", "Jump")
local ACTION_ROLL        = quanta.enum("ActionType", "Roll")
local ACTION_MELEE       = quanta.enum("ActionType", "Melee")
local ACTION_FREECLIMB   = quanta.enum("ActionType", "FreeClimb")
local ACTION_PARACHUTE   = quanta.enum("ActionType", "Parachute")
local STATE_DYING        = quanta.enum("CombatState", "DYING")

local GroundComponent    = import("world/component/ground_component.lua")
local WorkshopComponent  = import("world/component/workshop_component.lua")
local CollpointComponent = import("world/component/collpoint_component.lua")

local Scene              = class(nil, CollpointComponent, GroundComponent, WorkshopComponent)
local prop               = property(Scene)
prop:reader("id", nil)
prop:reader("aoi", nil)
prop:reader("type", nil)
prop:reader("name", nil)
prop:reader("map_id", nil)
prop:reader("prototype", nil)
prop:reader("hotareas", {})
prop:reader("entitys", {})
prop:reader("players", {})
prop:reader("srelives", {})
prop:reader("crelives", {})
prop:reader("sow_areas", {}) --怪物撒点区域
prop:reader("sights", {})    --全图视野实体

function Scene:__init(id, conf)
    self.id = id
    self.prototype = conf
    self.name = conf.nick
    self.type = conf.type
    self.map_id = conf.map_id
end

function Scene:load()
    --已经加载过了
    if self.aoi then
        return
    end
    --创建aoi对象
    local conf = self.prototype
    local w, h = tunpack(conf.size)
    local aoi_obj = aoi.create_aoi(w, h, conf.grid, conf.aoi, conf.center, true)
    aoi_obj.on_enter = function(obj_pairs)
        qxpcall(self.on_enter, "on_enter: {}", self, obj_pairs)
    end
    aoi_obj.on_leave = function(obj_pairs)
        qxpcall(self.on_leave, "on_leave: {}", self, obj_pairs)
    end
    aoi_obj.on_log = function(fmt, ...)
        log_info(fmt, ...)
    end
    self.aoi = aoi_obj
    --加载实体
    self:load_entitys()
    self:load_hotareas()
    self:load_sow_areas(conf.grid * msqrt(conf.aoi * conf.aoi * 2))
    if self.map_id == BORN_MAP then
        store_mgr:load_group(self, env_number("QUANTA_WID"), "world")
    end
    if self:is_public() then
        for area_id, _ in pairs(self.sow_areas) do
            self:activate_sow_area(area_id)
        end
    end
end

function Scene:inactive()
    return false
end

function Scene:close()
    self.entitys = {}
    self.hotareas = {}
end

function Scene:is_owner(player_id)
    return false
end

function Scene:is_copy()
    return self.type == COPY
end

function Scene:is_public()
    return self.type == PUBLIC
end

function Scene:refresh(now)
    local relives = {}
    for eid, entity in pairs(self.srelives) do
        if entity:refresh(now) then
            entity:set_scene(self)
            self.srelives[eid] = nil
            if entity:can_relive() then
                self.entitys[eid] = entity
                self:attach_scene(entity)
                --去除区域待复活记录
                for sow_area_id, sow_area in pairs(self.sow_areas) do
                    if sow_area.entity_id_map[eid] then
                        local conf_id = sow_area.entity_id_map[eid]
                        sow_area.dead_conf_id_map[conf_id] = nil
                        log_warn("[Scene][refresh] remove from dead_conf_id_map sow_area_id:{} entityId:{} config_id:{}",
                            sow_area_id, eid, conf_id)
                        break
                    end
                end
            end
        end
    end
    for eid, refresh_time in pairs(self.crelives) do
        if now >= refresh_time then
            self.crelives[eid] = nil
            relives[eid] = 1
        end
    end
    if next(relives) then
        self:broadcast_message("NID_ENTITY_ENTITY_STATUS_NTF", { scene_id = self.id, entitys = relives })
    end
end

function Scene:get_master()
    return self.id
end

function Scene:hour_update()
end

function Scene:update(now)
    local dead_entitys = {}
    for eid, entity in pairs(self.entitys) do
        entity:update(now)
        if entity:is_release() then
            dead_entitys[eid] = entity
        end
    end
    for eid, entity in pairs(dead_entitys) do
        --是否可复活
        if self:can_relive(entity) then
            if entity:is_dynamic() then
                self.srelives[eid] = entity
            else
                self.crelives[eid] = now + entity:get_refresh_time()
            end
        else
            self.srelives[eid] = nil
        end
        self:detach_scene(entity)
        self.entitys[eid] = nil
        self.sights[eid] = nil
    end
    self:invoke("_update", now)
end

--是否可复活
function Scene:can_relive(entity)
    return true
end

--查询实体
function Scene:find_entity(eid)
    local entity = self.entitys[eid]
    if entity then
        return entity
    end
    if self.srelives[eid] or self.crelives[eid] then
        return
    end
    local entity_no = eid & 0xFFFFF
    local map_db = config_mgr:get_table(self.name, "id")
    local conf = map_db:find_one(entity_no)
    if not conf or conf.dynamic then
        return
    end
    return self:create_entity(conf)
end

--获取实体
function Scene:get_entity(eid)
    local entity = self.entitys[eid]
    if entity then
        return entity
    end
    return self.players[eid]
end

--加载热区
function Scene:load_hotareas()
    local hotarea_db = config_mgr:init_table("hotarea", "id")
    local confs = hotarea_db:select({ map_id = self.map_id })
    for _, conf in pairs(confs) do
        local x, _, z = tunpack(conf.pos)
        local tx, ty, tz = tunpack(conf.tar_pos)
        self.aoi.add_hotarea(conf.id, conf.radius, x, z)
        self.hotareas[conf.id] = { map_id = conf.tar_map_id, x = tx, y = ty, z = tz }
    end
end

function Scene:create_entity(conf)
    local entity = object_fty:create_object(self.map_id, conf)
    if not entity then
        log_err("[Scene][create_entity] failed. map_id:{} conf.id:{}", self.map_id, conf.id)
        return
    end
    local x, y, z = tunpack(conf.pos)
    entity:set_pos_x(x)
    entity:set_pos_y(y)
    entity:set_pos_z(z)
    entity:set_map_id(self.map_id)
    entity:set_load_success(true)
    if conf.dir then
        entity:set_dir_y(conf.dir[2])
    end
    self.entitys[entity:get_id()] = entity
    self:attach_scene(entity)
    return entity
end

function Scene:destory_entity(entity_id, sow_area, conf_id)
    local entity = self.entitys[entity_id]
    if not entity then
        entity = self.srelives[entity_id]
    end
    if not entity then
        return
    end
    if entity:is_release() and self:can_relive(entity) and entity:is_dynamic() then
        --记录区域中死去待复活的id
        if sow_area ~= nil and conf_id ~= nil then
            sow_area.dead_conf_id_map[conf_id] = true
        end
    elseif entity:is_monster() then
        entity:dead(nil, self.id)
    else
        entity:set_release(true)
    end
    self:detach_scene(entity)
    self.srelives[entity_id] = nil
    self.entitys[entity_id] = nil
    self.sights[entity_id] = nil
end

--加载实体
function Scene:load_entitys()
    local map_db = config_mgr:init_table(self.name, "id")
    for _, conf in map_db:iterator() do
        if conf.dynamic then
            self:create_entity(conf)
        end
    end
end

--玩家进入
function Scene:reenter(player, player_id)
    --同步自己
    player:send("NID_ENTITY_ENTER_SCENE_NTF", player:pack_enter_data(self.id))
    --同步场景
    self:on_player_enter(player, player_id)
    --同步死亡的静态实体
    self:sync_relives(player)
    --同步全图视野实体
    self:sync_sights(player)
    --同步视野
    player:sync_sight(self.id)
    player:sync_recr();
end

--同步死亡的静态实体
function Scene:sync_relives(player)
    local relives = {}
    for eid in pairs(self.crelives) do
        relives[eid] = 0
    end
    if next(relives) then
        player:send("NID_ENTITY_ENTITY_STATUS_NTF", { scene_id = self.id, entitys = relives })
    end
end

--同步全图视野实体
function Scene:sync_sights(player)
    for _, obj in pairs(self.sights) do
        player:send("NID_ENTITY_ENTER_SCENE_NTF", obj:pack_enter_data(self.id))
    end
end

--玩家进入
function Scene:enter(player, player_id)
    --同步自己
    player:send("NID_ENTITY_ENTER_SCENE_NTF", player:pack_enter_data(self.id))
    --同步场景
    self:on_player_enter(player, player_id)
    self.players[player_id] = player
    --添加场景
    self:attach_scene(player)
    --同步全图视野实体
    self:sync_sights(player)
    --同步死亡的静态实体
    self:sync_relives(player)
    log_info("[Scene][enter] player({}) attach scene({})", player_id, self.map_id)
end

function Scene:on_player_enter(player, player_id)
    if not next(self.sow_areas) then
        return
    end
    event_mgr:fire_frame(function()
        -- 进入场景点在刷怪区内时, 防止需要走动后才能激活刷怪区
        player:nearby_sow_area_change(player:get_pos_x(), player:get_pos_z())
        self:sync_scene(player)
        self:sync_ground_datas(player)
        self:sync_workshop(player)
        player:sync_recr();
        player:sync_partners();
    end)
end

--玩家离开
function Scene:leave(player, player_id)
    self:on_player_leave(player, player_id)
    self:detach_scene(player)
    self.players[player_id] = nil
    log_info("[Scene][leave] player: {} detach scene {}!", player_id, self.map_id)
end

function Scene:on_player_leave(player, player_id)
end

function Scene:attach_scene(entity)
    entity:set_scene(self)
    local obj = entity:get_aoiobj()
    local pos_x = entity:get_pos_x()
    local pos_z = entity:get_pos_z()
    local entity_id = entity:get_id()
    if entity:is_sight() then
        --全图视野实体，不加入AOI
        self.sights[entity_id] = entity
        self:broadcast_message("NID_ENTITY_ENTER_SCENE_NTF", entity:pack_enter_data(self.id))
        return
    end
    if not self.aoi.attach(obj, pos_x, pos_z) then
        log_warn("[Scene][attach_scene] attach failed! map_id: {}, entity_id: {}, pos(x={},z={})", self.map_id, entity_id,
            pos_x, pos_z)
    end
end

---移除实体
function Scene:detach_scene(entity)
    -- 通知ai(仅关注实体的entity才有属性attent_ai_entities)
    local entity_id = entity:get_id()
    if entity.attent_ai_entities ~= nil and entity:has_attent() then
        local ai_entity_ids = entity:clear_attent()
        for _, e_id in pairs(ai_entity_ids) do
            local e_item = self:get_entity(e_id)
            if e_item ~= nil and e_item.is_ai_entity and e_item:entity_is_hated(entity_id) then
                -- 移除仇恨列表
                e_item:enemy_leave(entity_id)
            end
        end
    end
    if entity:is_sight() then
        --全图视野实体
        self.sights[entity_id] = nil
        self:broadcast_message("NID_ENTITY_LEAVE_SCENE_NTF", { id = entity_id, scene_id = self.id })
    else
        self.aoi.detach(entity:get_aoiobj())
    end
    entity:set_scene(nil)
end

function Scene:trans_entity(entity, x, y, z, d)
    local obj = entity:get_aoiobj()
    self.aoi.detach(obj)
    entity:set_pos_x(x)
    entity:set_pos_y(y)
    entity:set_pos_z(z)
    entity:set_dir_y(d)
    if entity:is_player() then
        -- player同场景内传送，通知client位置改变
        entity:send("NID_ENTITY_ENTER_SCENE_NTF", entity:pack_enter_data(self.id))
    end
    self.aoi.attach(obj, x, z)
    log_debug("[Scene][trans_entity] entity({}) trans to ({}-{})!", entity:get_id(), obj.x, obj.z)
end

--移动
function Scene:move_entity(entity, entity_id, pos_x, pos_z)
    local is_trans = false -- 是否传送
    local obj = entity:get_aoiobj()
    local hotarea_id = self.aoi.move(obj, pos_x, pos_z)
    if hotarea_id < 0 then
        log_err("[Scene][move_entity] entity: {} move failed!", entity_id)
        return false, is_trans
    end
    --log_debug("[Scene][move_entity] entity({}) move to ({}-{})! hotarea_id: {}", entity_id, obj.x, obj.z, hotarea_id)
    local hotarea = self.hotareas[hotarea_id]
    if hotarea then
        log_info("[Scene][move_entity] entity: {} will be trans from {} to {}!", entity_id, self.map_id, hotarea.map_id)
        scene_mgr:trans_scene(entity, hotarea.map_id, hotarea.x, hotarea.y, hotarea.z)
        is_trans = true
    end
    return true, is_trans
end

-- 广播消息
function Scene:broadcast_message(cmd_id, data)
    for _, player in pairs(self.players) do
        -- 临时代码
        if data.master then
            data.master = player:get_id()
        end
        player:send(cmd_id, data)
    end
end

--AOI回调
-----------------------------------------------------------------------
--进入视野
function Scene:on_enter(obj_pairs)
    log_debug("[Scene][on_enter] enters:{}!", obj_pairs)
    for i = 1, #obj_pairs, 2 do
        local watcher_id = obj_pairs[i]
        local target_id = obj_pairs[i + 1]
        thread_mgr:fork(function()
            local watcher = self:get_entity(watcher_id)
            local target = self:get_entity(target_id)
            if watcher and target then
                target:add_watcher(watcher_id)
                watcher:add_marker(target_id)
                watcher:send("NID_ENTITY_ENTER_SCENE_NTF", target:pack_enter_data(self.id))
                log_info("[Scene][on_enter] watcher_id:{} target_id:{}!", watcher_id, target_id)
                event_mgr:fire_frame(function()
                    watcher:enter_update_attent(target)
                end)
            end
        end)
    end
end

--离开视野
function Scene:on_leave(obj_pairs)
    log_debug("[Scene][on_leave] leaves:{}!", obj_pairs)
    for i = 1, #obj_pairs, 2 do
        local watcher_id = obj_pairs[i]
        local target_id = obj_pairs[i + 1]
        thread_mgr:fork(function()
            local watcher = self:get_entity(watcher_id)
            local target = self:get_entity(target_id)
            if watcher and target then
                watcher:remove_marker(target_id)
                target:remove_watcher(watcher_id)
                watcher:send("NID_ENTITY_LEAVE_SCENE_NTF", { id = target_id, scene_id = self.id })
                log_info("[Scene][on_leave] watcher_id:{} target_id:{}!", watcher_id, target_id)
                event_mgr:fire_frame(function()
                    watcher:leave_update_attent(target)
                end)
            end
        end)
    end
end

--加载撒点区域
---@param aoi_radius number aoi半径(单位: cm)
function Scene:load_sow_areas(aoi_radius)
    for _, area_conf in sow_area_db:iterator() do
        if area_conf.map_id == self.map_id then
            local x, y, z = tunpack(area_conf.area_center)
            local sow_area = {
                area_id = area_conf.id,
                map_id = area_conf.map_id,
                area_center = { x = x, y = y, z = z },
                area_radius = area_conf.area_radius,
                aoi_radius = aoi_radius, -- aoi半径
                is_activate = false,     -- 区域是否激活
                entity_conf_list = {},   -- 区域内刷怪配置列表
                entity_id_map = {},      -- 区域内创建的实体id集合
                player_id_map = {},      -- 激活该区域的玩家id集合
                dead_conf_id_map = {} --已死掉待复活的id列表
            }
            for _, entity_conf in sow_entity_db:iterator() do
                if entity_conf.map_id == self.map_id and entity_conf.area_id == area_conf.id then
                    tinsert(sow_area.entity_conf_list, entity_conf)
                end
            end
            self.sow_areas[area_conf.id] = sow_area
        end
    end
end

--激活撒点区域
---@param sow_area_id integer 撒点区域id
function Scene:activate_sow_area(sow_area_id)
    log_warn("[Scene][activate_sow_area] sow_area_id:{}", sow_area_id)
    local sow_area = self.sow_areas[sow_area_id]
    if not sow_area then
        return
    end
    sow_area.is_activate = true
    -- 将区域内的所有实体创建
    for conf_id, entity_conf in ipairs(sow_area.entity_conf_list) do
        --没在待复活记录的
        if sow_area.dead_conf_id_map[conf_id] == nil then
            -- 创建撒点区域内实体
            local entity = self:create_entity(entity_conf)
            if entity then
                sow_area.entity_id_map[entity:get_id()] = conf_id
            end
        end
    end
end

--释放撒点区域(使区域非激活)
function Scene:deactivate_sow_area(sow_area_id)
    log_warn("[Scene][deactivate_sow_area] sow_area_id:{}", sow_area_id)
    local sow_area = self.sow_areas[sow_area_id]
    if not sow_area then
        return
    end
    -- 将区域内所有实体清除
    for entity_id, conf_id in pairs(sow_area.entity_id_map) do
        -- 清除没在待复活记录的
        if sow_area.dead_conf_id_map[conf_id] == nil then
            self:destory_entity(entity_id, sow_area, conf_id)
        end
    end
    sow_area.entity_id_map = {}
    sow_area.is_activate = false
end

--判断player是否在某个撒点区域附近
---@param sow_area_id integer 撒点区域id
---@param x_old number 上一次坐标点x
---@param z_old number 上一次坐标点z
---@param x_now number 当前坐标点x
---@param z_now number 当前坐标点z
function Scene:is_nearby_sow_area(sow_area_id, x_old, z_old, x_now, z_now)
    local sow_area = self.sow_areas[sow_area_id]
    if not sow_area then
        return
    end
    local pos = sow_area.area_center
    -- 撒点区域检测阈值: 区域半径 + aoi半径
    local dis_threshold = sow_area.area_radius + sow_area.aoi_radius
    local dis_old = mdistance(x_old, z_old, pos.x, pos.z)
    local dis_now = mdistance(x_now, z_now, pos.x, pos.z)
    -- 如果两次移动检测都在区域内则判定该区域为当前靠近的区域
    return dis_old < dis_threshold and dis_now < dis_threshold
end

--寻找坐标点附近的撒点区域
---@param x_old number 上一次坐标点x
---@param z_old number 上一次坐标点z
---@param x_now number 当前坐标点x
---@param z_now number 当前坐标点z
function Scene:find_nearby_sow_areas(x_old, z_old, x_now, z_now)
    local nearby_sow_areas = {}
    for _, sow_area in pairs(self.sow_areas) do
        local pos = sow_area.area_center
        -- 撒点区域检测阈值: 区域半径 + aoi半径
        local dis_threshold = sow_area.area_radius + sow_area.aoi_radius
        local dis_old = mdistance(x_old, z_old, pos.x, pos.z)
        local dis_now = mdistance(x_now, z_now, pos.x, pos.z)
        -- 如果两次移动检测都在区域内则判定该区域为当前靠近的区域
        if dis_old < dis_threshold and dis_now < dis_threshold then
            tinsert(nearby_sow_areas, sow_area)
        end
    end
    return nearby_sow_areas
end

function Scene:position_changed_notify(entity, pos_x, pos_y, pos_z)
    if entity:is_player() then
        entity:notify_event("on_position_changed", self.map_id, pos_x, pos_z)
    end
    if entity.attent_ai_entities ~= nil and entity:has_attent() then
        event_mgr:notify_trigger("on_position_changed", entity, self.id, pos_x, pos_y, pos_z)
    end
end

function Scene:handle_action_move(action_move, entity, entity_id, notify, response, action_result)
    if not action_move then
        action_result.success = false
        response.error_code = ACTION_EMPTY
        log_warn("[Scene][handle_action_move] action_move is nil!")
        return
    end
    entity:update_cache_safe_position()
    action_result.success = true
end

function Scene:handle_action_jump(action_jump, entity, entity_id, notify, response, action_result)
    if not action_jump then
        action_result.success = false
        response.error_code = ACTION_EMPTY
        log_warn("[Scene][handle_action_jump] action_jump is nil!")
        return
    end
    local result = entity:on_action_jump(action_jump)
    action_result.success = result.error_code == FRAME_SUCCESS
    if not action_result.success then
        response.error_code = result.error_code
    end
end

function Scene:handle_action_roll(action_roll, entity, entity_id, notify, response, action_result)
    if not action_roll then
        action_result.success = false
        response.error_code = ACTION_EMPTY
        log_warn("[Scene][handle_action_roll] action_roll is nil!")
        return
    end
    local result = entity:on_action_roll(action_roll, entity_id)
    action_result.success = result.error_code == FRAME_SUCCESS
    if not action_result.success then
        response.error_code = result.error_code
    end
    if result.roll_result ~= nil then
        action_result.roll_result = result.roll_result
    end
end

---@param entity SkillComponent|BattleComponent
function Scene:handle_action_melee(action_melee, entity, entity_id, notify, response, action_result)
    if not action_melee then
        action_result.success = false
        response.error_code = ACTION_EMPTY
        log_warn("[Scene][handle_action_melee] action_melee is nil!")
        return
    end
    local result = entity:on_action_melee(action_melee)
    action_result.success = result.error_code == FRAME_SUCCESS
    if not action_result.success then
        response.error_code = result.error_code
    end
    if result.melee_result ~= nil then
        action_result.melee_result = result.melee_result
    end
end

function Scene:handle_action_free_climb(action_free_climb, entity, entity_id, notify, response, action_result)
    if not action_free_climb then
        action_result.success = false
        response.error_code = ACTION_EMPTY
        log_warn("[Scene][handle_action_free_climb] action_free_climb is nil!")
        return
    end
    local result = entity:on_action_free_climb(action_free_climb, entity_id)
    action_result.success = result.error_code == FRAME_SUCCESS or result.error_code == NO_STAMINA
    if not action_result.success then
        response.error_code = result.error_code
    end
end

function Scene:handle_action_parachute(action_parachute, entity, entity_id, notify, response, action_result)
    if not action_parachute then
        action_result.success = false
        response.error_code = ACTION_EMPTY
        log_warn("[Scene][handle_action_parachute] action_parachute is nil!")
        return
    end
    local result = entity:on_action_parachute(action_parachute, entity_id)
    action_result.success = result.error_code == FRAME_SUCCESS
    if not action_result.success then
        response.error_code = result.error_code
    end
end

---单个action处理
function Scene:single_action_handle(action, entity, entity_id, notify, response, results, idx)
    results[idx] = {
        type = action.type,
        success = false,
    }
    if entity:get_combat_state() == STATE_DYING then
        -- 重伤状态不允许使用技能
        results[idx].success = false
        response.error_code = DYING_CANT_SKILL
        return
    end
    if action.type == ACTION_MOVE then
        -- 移动
        self:handle_action_move(action.move, entity, entity_id, notify, response, results[idx])
    elseif action.type == ACTION_JUMP then
        -- 跳跃
        self:handle_action_jump(action.jump, entity, entity_id, notify, response, results[idx])
    elseif action.type == ACTION_ROLL then
        -- 翻滚
        self:handle_action_roll(action.roll, entity, entity_id, notify, response, results[idx])
    elseif action.type == ACTION_MELEE then
        -- 近战
        self:handle_action_melee(action.melee, entity, entity_id, notify, response, results[idx])
    elseif action.type == ACTION_FREECLIMB then
        -- 自由攀爬
        self:handle_action_free_climb(action.freeclimb, entity, entity_id, notify, response, results[idx])
    elseif action.type == ACTION_PARACHUTE then
        -- 滑翔
        self:handle_action_parachute(action.parachute, entity, entity_id, notify, response, results[idx])
    end
    if results[idx].success then
        entity:stamina_recover_state_change(action.type)
        entity:update_cache_action(action.type)
    end
end

--创建持久化实体
function Scene:create_snap_entity(conf, force)
    --非town不支持创建计次数的采集物
    return
end

return Scene
