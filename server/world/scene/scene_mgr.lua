--scene_mgr.lua
local log_info      = logger.info
local log_warn      = logger.warn

local event_mgr     = quanta.get("event_mgr")
local update_mgr    = quanta.get("update_mgr")
local config_mgr    = quanta.get("config_mgr")

local map_db        = config_mgr:init_table("map", "map_id")

local COPY          = quanta.enum("MapType", "COPY")
local PUBLIC        = quanta.enum("MapType", "PUBLIC")

local SceneMgr = singleton()
local prop = property(SceneMgr)
prop:reader("scenes", {})

function SceneMgr:__init()
    update_mgr:attach_second(self)
    update_mgr:attach_second30(self)
    update_mgr:attach_hour(self)
end

function SceneMgr:on_second30()
    local inactives = {}
    local now = quanta.now
    for id, scene in pairs(self.scenes) do
        scene:refresh(now)
        if scene:inactive() then
            inactives[id] = scene
        end
    end
    for _, scene in pairs(inactives) do
        self:close_scene(scene)
    end
end

function SceneMgr:on_second()
    local now = quanta.now
    for _, scene in pairs(self.scenes) do
        scene:update(now)
    end
end

function SceneMgr:on_hour()
    for _, scene in pairs(self.scenes) do
        scene:hour_update()
    end
end

function SceneMgr:get_scene(id)
    return self.scenes[id]
end

function SceneMgr:get_scene_by_map(map_id)
    for _,scene in pairs(self.scenes) do
        if scene.map_id == map_id then
            return scene
        end
    end
    return nil
end

function SceneMgr:create_copy(player_id, map_id)
    local conf = self:find_conf(map_id)
    if not conf then
        return
    end
    local scene_id = self:new_scene_id(COPY, player_id, map_id)
    local scene = self.scenes[scene_id]
    if scene then
        return scene
    end
    local Copy = import("world/copy/copy.lua")
    local new_scene = Copy(scene_id, conf)
    self.scenes[scene_id] = new_scene
    new_scene:set_master(player_id)
    new_scene:load()
    log_info("[SceneMgr][create_copy] create copy {} success!", scene_id)
    return new_scene
end

function SceneMgr:find_conf(map_id)
    return map_db:find_one(map_id)
end

function SceneMgr:load_scene(map_conf, player_id)
    local map_id = map_conf.map_id
    if map_conf.type == PUBLIC then
        local scene_id = self:new_scene_id(PUBLIC, nil, map_id)
        local scene = self.scenes[scene_id]
        if not scene then
            log_info("[SceneMgr][load_scene] load scene {} success!", map_id)
            local Scene = import("world/scene/scene.lua")
            local new_scene = Scene(scene_id, map_conf)
            self.scenes[scene_id] = new_scene
            new_scene:load()
            return new_scene
        end
        return scene
    end
    local scene_id = self:new_scene_id(map_conf.type, player_id, map_id)
    return self.scenes[scene_id]
end

function SceneMgr:enter_scene(player, map_id, player_id)
    log_info("[SceneMgr][enter_scene] player({}) will enter scene({})", player_id, map_id)
    local conf = self:find_conf(map_id)
    -- 其它场景进入
    local scene = self:load_scene(conf, player_id)
    if scene then
        scene:enter(player, player_id, self.max_line)
    else
        log_warn("[SceneMgr][enter_scene] player(id={}) enter scene(map_id={}) not found", player_id, map_id)
    end
end

function SceneMgr:goto_scene(player, scene_id, player_id)
    log_info("[SceneMgr][goto_scene] player({}) will goto scene({})", player_id, scene_id)
    local scene = self:get_scene(scene_id)
    if not scene then
        local cscene_id = self:new_scene_id(PUBLIC, nil, 1003)
        scene =  self:get_scene(cscene_id)
    end
    scene:enter(player, player_id, self.max_line)
end

function SceneMgr:close_scene(scene)
    local id = scene:get_id()
    local map_id = scene:get_map_id()
    log_info("[SceneMgr][close_scene] close scene: ({}-{})", id, map_id)
    event_mgr:fire_frame("on_scene_closed", id, map_id)
    self.scenes[id] = nil
    scene:close()
end

function SceneMgr:trans_scene(player, map_id, x, y, z, d)
    local player_id = player:get_id()
    local scene = player:get_scene()
    if scene then
        player:reset_nearby_sow_area()
        scene:leave(player, player_id)
        player:set_trans_scene_start(quanta.now_ms)
    end
    player:set_pos_x(x)
    player:set_pos_y(y)
    player:set_pos_z(z)
    player:set_dir_y(d or 0)
    player:set_map_id(map_id)
    --进入新场景
    self:enter_scene(player, map_id, player_id)
    event_mgr:notify_second(player, "on_scene_changed", map_id, x, z)
    log_info("[SceneMgr][trans_scene] player({}) enter {}", player_id, map_id)
end

--场景ID生成
function SceneMgr:new_scene_id(type, player_id, map_id)
    if type == COPY then
        return type << 56 | map_id << 32 | (player_id & 0xffffffff)
    end
    return type << 56 | map_id << 32
end

function SceneMgr:get_scene_type(scene_id)
    return scene_id >> 56 & 0xff
end

function SceneMgr:get_scenemap_id(scene_id)
    return scene_id >> 32 & 0xffffff
end

-- export
quanta.scene_mgr = SceneMgr()

return SceneMgr
