--world_mgr.lua
local log_err           = logger.err
local log_warn          = logger.warn
local log_debug         = logger.debug
local qdefer            = quanta.defer
local qfailed           = quanta.failed
local tunpack           = table.unpack
local sformat           = string.format

local event_mgr         = quanta.get("event_mgr")
local mirror_mgr        = quanta.get("mirror_mgr")
local client_mgr        = quanta.get("client_mgr")
local thread_mgr        = quanta.get("thread_mgr")
local player_mgr        = quanta.get("player_mgr")
local config_mgr        = quanta.get("config_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")

local filter            = config_mgr:init_table("filter", "name")

local FRAME_UPHOLD      = protobuf_mgr:error_code("FRAME_UPHOLD")
local FRAME_TOOFAST     = protobuf_mgr:error_code("FRAME_TOOFAST")
local ROLE_NOT_EXIST    = protobuf_mgr:error_code("LOGIN_ROLE_NOT_EXIST")

local WorldMgr = singleton()
local prop = property(WorldMgr)
prop:reader("ignore_cmds", {})      --日志过滤
prop:reader("reenter_cmds", {})     --重入过滤

function WorldMgr:__init()
    -- 协议过滤
    self:init_filter()
    -- 网络事件监听
    event_mgr:add_listener(self, "on_socket_cmd")
    event_mgr:add_listener(self, "on_socket_error")
    event_mgr:add_listener(self, "on_socket_accept")
end

---是否输出CMD消息的内容
function WorldMgr:is_print_cmd(cmd_id)
    if self.ignore_cmds[cmd_id] then
        return false
    end
    return true
end

---是否输出CMD消息的内容
function WorldMgr:init_filter()
    self:on_cfg_filter_changed()
    event_mgr:add_trigger(self, "on_cfg_filter_changed")
end


---日志忽略网络消息通知名
function WorldMgr:on_cfg_filter_changed()
    for cmd_name, conf in filter:iterator() do
        local cmd_id = protobuf_mgr:msg_id(cmd_name)
        if conf.log then
            self.ignore_cmds[cmd_id] = true
            self.ignore_cmds[cmd_name] = true
        end
        if conf.proto then
            self.reenter_cmds[cmd_id] = true
        end
    end
end

function WorldMgr:close_session(session)
    if session.mirror then
        mirror_mgr:close_session(session)
        return
    end
    client_mgr:close_session(session)
end

function WorldMgr:send_message(session, cmd_id, data)
    local player_id = session.player_id
    if self:is_print_cmd(cmd_id) then
        log_debug("[WorldMgr][send_message] player({}) send message({}-{}) !", player_id, cmd_id, data)
    end
    if session.mirror then
        mirror_mgr:send(player_id, cmd_id, data)
        return
    end
    client_mgr:send(session, cmd_id, data)
end

function WorldMgr:callback_by_id(session, cmd_id, body, session_id)
    if session.mirror then
        mirror_mgr:callback_by_id(session_id, body, session.player_id)
        return
    end
    client_mgr:callback_by_id(session, cmd_id, body, session_id)
end

function WorldMgr:callback_errcode(session, cmd_id, code, session_id)
    if session.mirror then
        mirror_mgr:callback_errcode(session_id, code, session.player_id)
        return
    end
    client_mgr:callback_errcode(session, cmd_id, code, session_id)
end

--客户端连上
function WorldMgr:on_socket_accept(session)
    log_debug("[WorldMgr][on_socket_accept] {} connected!", session.token)
end

--客户端连接断开
function WorldMgr:on_socket_error(session, token, err)
    local player_id = session.player_id
    log_debug("[WorldMgr][on_socket_error] (t:{}-u:{}) lost, because: {}!", token, player_id, err)
    local player = player_mgr:get_entity(player_id)
    if player then
        log_warn("[WorldMgr][on_player_disconnect] player({}) offline", player_id)
        player:set_session(nil)
        player:offline()
    end
end

--客户端消息分发
function WorldMgr:on_socket_cmd(session, service_type, cmd_id, body, session_id)
    local result = event_mgr:notify_listener("on_proto_filter", cmd_id, service_type)
    if result[1] and result[2] then
        log_warn("[WorldMgr][on_socket_cmd] on_proto_filter false, cmd_id={}", cmd_id)
        self:callback_errcode(session, cmd_id, FRAME_UPHOLD, session_id)
        return
    end
    -- 协议锁
    local hook<close> = qdefer()
    if self.reenter_cmds[cmd_id] then
        local lock_key = sformat("%s_%s", session.token, cmd_id)
        hook:register(function()
            thread_mgr:unlock(lock_key)
        end)
        if not thread_mgr:lock(lock_key, false) then
            log_warn("[WorldMgr][on_socket_cmd] check lock failed, cmd_id={}, {}", cmd_id, lock_key)
            self:callback_errcode(session, cmd_id, FRAME_TOOFAST, session_id)
            return
        end
    end
    if service_type == 0 then
        event_mgr:notify_command(cmd_id, session, cmd_id, body, session_id)
        return
    end
    local player_id = session.player_id
    local player = player_mgr:get_entity(player_id)
    if not player then
        log_err("[WorldMgr][on_socket_cmd] player: {} not exist, cmd_id: {}", player_id, cmd_id)
        self:callback_errcode(session, cmd_id, ROLE_NOT_EXIST, session_id)
        return
    end
    local ok, code, res = tunpack(event_mgr:notify_command(cmd_id, player, player_id, body))
    if qfailed(code, ok) then
        log_err("[WorldMgr][on_socket_cmd] player: {} call cmd_id: {} error: {} ", player_id, cmd_id, res)
        self:callback_errcode(session, cmd_id, ok and code or FRAME_UPHOLD, session_id)
        return
    end
    self:callback_by_id(session, cmd_id, res, session_id)
end

quanta.world_mgr = WorldMgr()

return WorldMgr
