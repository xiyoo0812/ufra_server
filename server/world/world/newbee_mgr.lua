--newbee_mgr.lua
local log_debug     = logger.debug
local unserialize   = luakit.unserialize
local tinsert       = table.insert

local event_mgr     = quanta.get("event_mgr")
local config_mgr    = quanta.get("config_mgr")
local protobuf_mgr  = quanta.get("protobuf_mgr")

local newbee_db     = config_mgr:init_table("newbee", "key")
local level_db      = config_mgr:init_table("player_level", "level")

local BORN_MAP      = newbee_db:find_integer("value", "born_map_id")
local BORN_X        = newbee_db:find_integer("value", "born_pox_x")
local BORN_Y        = newbee_db:find_integer("value", "born_pox_y")
local BORN_Z        = newbee_db:find_integer("value", "born_pox_z")
local BORN_D        = newbee_db:find_integer("value", "born_dir_y")

local BORN_ITEMS    = newbee_db:find_value("value", "born_items")
local BORN_EQUIPS   = newbee_db:find_value("value", "born_equip")
local BORN_COIN     = newbee_db:find_integer("value", "born_coin")
local BORN_HP       = newbee_db:find_integer("value", "born_hp")
local BORN_SP_MAX   = newbee_db:find_integer("value", "born_sp_max")
local BORN_STAMINA  = newbee_db:find_integer("value", "born_stamina")
local BK_MELEE      = newbee_db:find_value("value", "born_melee_skills")
local BK_COLLECTION = newbee_db:find_value("value", "born_collection_skills")
local BK_PICKUP     = newbee_db:find_value("value", "born_pickup_skills")
local BK_KICK       = newbee_db:find_value("value", "born_kick_skills")
local BORN_HIT      = newbee_db:find_value("value", "born_hit_action")
local STAMINA_MAX   = newbee_db:find_integer("value", "born_stamina_max")
local BORN_GROUND   = newbee_db:find_value("value", "born_ground")
local BORN_BUILD    = newbee_db:find_value("value", "born_building")
local BORN_UTENSIL  = newbee_db:find_value("value", "born_utensil")
local BORN_PARTNER  = newbee_db:find_value("value", "born_partner")

local BORN_MAX_TOUG = newbee_db:find_integer("value", "born_max_toughness")
local BORN_ANT_TOUG = newbee_db:find_integer("value", "born_anti_toughness")
local BORN_REC_TOUG = newbee_db:find_integer("value", "born_recover_toughness")
local BORN_TOU_WAIT = newbee_db:find_integer("value", "born_toughness_wait")
local BORN_TOU_FRAC = newbee_db:find_integer("value", "born_toughness_fracture")

local BORN_SP_HIT   = newbee_db:find_integer("value", "born_sp_hit")
local BORN_SP_ATTACK= newbee_db:find_integer("value", "born_sp_attack")
local BORN_SP_RATE  = newbee_db:find_integer("value", "born_sp_rate")

local BST_RECOVER   = newbee_db:find_integer("value", "born_stamina_recover")
local BST_ROLL      = newbee_db:find_integer("value", "born_stamina_roll")
local BST_PARACHUTE = newbee_db:find_integer("value", "born_stamina_parachute")
local BST_FREECLIMB = newbee_db:find_integer("value", "born_stamina_freeclimb")
local BST_CLIMB_JUMP= newbee_db:find_integer("value", "born_stamina_climb_jump")

local PACKET_ITEM   = protobuf_mgr:enum("packet_type", "PACKET_ITEM")
local PACKET_EQUIP  = protobuf_mgr:enum("packet_type", "PACKET_EQUIP")

local NewbeeMgr = singleton()
local prop = property(NewbeeMgr)
prop:reader("born_items", {})
prop:reader("born_equips", {})
prop:reader("born_skills", {})
prop:reader("born_hit_action", {})
prop:reader("lv_conf", nil)
prop:reader("born_grounds", {})
prop:reader("born_buildings", {})
prop:reader("born_utensils", {})
prop:reader("born_partners", {})

function NewbeeMgr:__init()
    --初始道具
    local items = unserialize(BORN_ITEMS or "{}")
    for item_id, count in pairs(items or {}) do
        self.born_items[item_id] = count
    end
    --初始装备
    local equips = unserialize(BORN_EQUIPS or "{}")
    for item_id, count in pairs(equips or {}) do
        self.born_equips[item_id] = count
    end
    --初始等级
    local lvl_attr = level_db:find_one(1)
    if not lvl_attr then
        return
    end
    --初始npc
    local born_partner_conf = unserialize(BORN_PARTNER or "{}")
    for partner_id, _ in pairs(born_partner_conf) do
        self.born_partners[partner_id] = 1
    end
    self.lv_conf = lvl_attr
    self:init_workshop_conf()
    --初始技能
    self:init_born_skills()
    -- 初始受击动作
    self.born_hit_action = unserialize(BORN_HIT or "{}")
    --监听事件
    event_mgr:add_trigger(self, "on_player_attr_init")
end

function NewbeeMgr:init_workshop_conf()
     --初始地皮
     self.born_grounds = unserialize(BORN_GROUND or "{}")
     log_debug("[NewbeeMgr][__init] self.born_grounds:{}", self.born_grounds)
    --初始建筑
    local born_building = unserialize(BORN_BUILD or "{}")
    for _, building in pairs(born_building) do
        for proto_id, detail in pairs( building) do
            local bytes = protobuf_mgr:encode_byname("ncmd_cs.build_detail", detail)
            local born = {
                proto_id = proto_id,
                detail = bytes
            }
            tinsert(self.born_buildings, born)
        end
    end
    --初始摆件
    local born_utensils = unserialize(BORN_UTENSIL or "{}")
    for _, utensil in pairs(born_utensils) do
        for proto_id, detail in pairs( utensil) do
            local bytes = protobuf_mgr:encode_byname("ncmd_cs.build_detail", detail)
            local born = {
                proto_id = proto_id,
                detail = bytes
            }
            tinsert(self.born_utensils, born)
        end
    end
end

--初始化位置
function NewbeeMgr:init_position(player)
    player:set_pos_x(BORN_X)
    player:set_pos_y(BORN_Y)
    player:set_pos_z(BORN_Z)
    player:set_dir_y(BORN_D)
    player:set_map_id(BORN_MAP)
    player:update_pos()
    log_debug("[NewbeeMgr][init_position] player:{} map_id:{}-{}", player.id, BORN_MAP, player:get_map_id())
end

--初始化背包道具
function NewbeeMgr:init_packet(player)
    if next(self.born_items) then
        local ipacket = player:get_packet(PACKET_ITEM)
        ipacket:load_items(self.born_items, true)
    end
    if next(self.born_equips) then
        local epacket = player:get_packet(PACKET_EQUIP)
        epacket:load_items(self.born_equips, true)
    end
end

--初始化玩家地皮
function NewbeeMgr:init_grounds(scene)
    if not next(self.born_grounds) then
        return
    end
    scene:init_born_grounds(self.born_grounds)
end

--初始化玩家伙伴
function NewbeeMgr:init_partners(partner)
    if not next(self.born_partners) then
        return
    end
    partner:init_born_partner(self.born_partners)
end

--初始化建筑
function NewbeeMgr:init_building(scene)
    if not next(self.born_buildings) then
        return
    end
    scene:init_building(self.born_buildings)
end

--初始化用具
function NewbeeMgr:init_utensil(scene)
    if not next(self.born_utensils) then
        return
    end
    scene:init_utensil(self.born_utensils)
end

--初始化玩家数据
function NewbeeMgr:on_player_attr_init(player)
    player:update_level(1)
    --创建角色基础属性
    player:set_sp(0)
    player:set_exp(0)
    player:set_lucky(0)
    player:set_diamond(0)
    player:set_hp(BORN_HP)
    player:set_coin(BORN_COIN)
    player:set_town_exp(0)
    player:set_town_level(1)
    player:set_sp_max(BORN_SP_MAX)
    player:set_stamina(BORN_STAMINA)
    player:set_stamina_max(STAMINA_MAX)
end

--初始技能
function NewbeeMgr:init_born_skills()
    self.born_skills = {
        melee = unserialize(BK_MELEE or "{}"),           -- 空手攻击
        collection = unserialize(BK_COLLECTION or "{}"), -- 采集
        pickup = unserialize(BK_PICKUP or "{}"),         -- 捡东西
        kick = unserialize(BK_KICK or "{}"),             -- 踢(树)
    }
end

---初始化默认(空手)技能
function NewbeeMgr:init_skills(player)
    if not self.born_skills or not next(self.born_skills) then
        return
    end
    player:init_default_skills(self.born_skills)
end

---初始化受击动作和韧性配置
function NewbeeMgr:init_hit_actions(player)
    if self.born_hit_action and next(self.born_hit_action) then
        player:init_hit_actions(self.born_hit_action)
    end
end

function NewbeeMgr:init_toughness_config(player)
    --韧性基础属性
    player:set_toughness_max(BORN_MAX_TOUG)
    player:set_toughness(BORN_MAX_TOUG)
    player:set_anti_toughness(BORN_ANT_TOUG)
    player:set_recover_toughness(BORN_REC_TOUG)
    player:set_toughness_wait(BORN_TOU_WAIT)
    player:set_toughness_fracture(BORN_TOU_FRAC)
end

--初始耐力配置
function NewbeeMgr:init_stamina_config(player)
    player:set_stamina_recover(BST_RECOVER)
    player:set_stamina_roll(BST_ROLL)
    player:set_stamina_parachute(BST_PARACHUTE)
    player:set_stamina_freeclimb(BST_FREECLIMB)
    player:set_stamina_climb_jump(BST_CLIMB_JUMP)
end

--初始sp技能配置
function NewbeeMgr:init_sp_config(player)
    player:set_sp_hit(BORN_SP_HIT)
    player:set_sp_attack(BORN_SP_ATTACK)
    player:set_sp_rate(BORN_SP_RATE)
end

-- export
quanta.newbee_mgr = NewbeeMgr()

return NewbeeMgr
