--ai_mgr.lua
local tinsert            = table.insert
local log_warn           = logger.warn

local AI_INTERVAL        = 500 -- AI循环计时器间隔(单位: ms)

local EntityMgr          = import("business/entity/entity_mgr.lua")

local timer_mgr          = quanta.get("timer_mgr")
local thread_mgr         = quanta.get("thread_mgr")
local event_mgr          = quanta.get("event_mgr")
local update_mgr         = quanta.get("update_mgr")
local scene_mgr          = quanta.get("scene_mgr")

---@class AiMgr
local AiMgr = singleton(EntityMgr)

local prop = property(AiMgr)
prop:reader("scene_ai_data_map", nil) -- 场景ai实体列表
prop:reader("wheel_cur", 1)
prop:reader("ai_proto_bloack_list", {}) -- ai配置缺失的 proto_id 黑名单

function AiMgr:__init()
    --初始化数据
    self.scene_ai_data_map = {}
    --定时处理
    update_mgr:attach_frame(self)
    --事件监听
    event_mgr:add_trigger(self, "on_position_changed")
    -- ai循环
    self:setup_ai_loop()
end

function AiMgr:setup_ai_loop()
    self:teardown_ai_loop()
    -- ai循环(精度 AI_INTERVAL ms)
    self.ai_loop_tid = timer_mgr:loop(AI_INTERVAL, function()
        self:ai_loop_update()
    end)
end

---ai循环-update
function AiMgr:ai_loop_update()
    local function error_handler(err)
        -- 注意: debug.traceback(), 第二个参数控制从调用栈的哪一层开始显示，通常设为2忽略xpcall自身
        log_warn('[AiMgr][ai_loop_update] error occurred: {}', debug.traceback(err, 2))
    end
    thread_mgr:entry(self:address(), function()
        local success, elapsed_ms, iter_count = xpcall(self.ai_loop_call, error_handler, self)
        if not success or elapsed_ms / iter_count > 50 then
            log_warn("[AiMgr][ai_loop_update] success={}, elapsed_ms={}ms, iter_count={}", success, elapsed_ms, iter_count)
        end
    end)
end

---ai循环-主逻辑
function AiMgr:ai_loop_call()
    local start_ms = quanta.now_ms
    local iter_count = 0
    for scene_id, scene_data in pairs(self.scene_ai_data_map) do
        for entity_id, entity in scene_data.entity_map:iterator() do
            if not entity.ai_config_base then
                -- 跳过没有配置ai配置的实体
                local proto_id = entity:get_proto_id()
                if not self.ai_proto_bloack_list[proto_id] then
                    self.ai_proto_bloack_list[proto_id] = true
                    log_warn("[AiMgr][ai_loop_call] scene_id: {}, entity_id: {}, proto_id: {} has no ai_config_base", scene_id, entity_id, proto_id)
                end
                goto continue_scene
            end
            if entity:is_release() then
                goto continue_scene
            end
            iter_count = iter_count + 1
            entity:check_hate_attenuate() -- 仇恨衰减检测
            local has_states = entity:has_states()
            local has_actions = entity:has_actions()
            if entity.ai_available and entity.ai_enabled and has_states and has_actions then
                entity:check_skill_status() -- 技能状态检测
                entity:check_nav_forward() -- 寻路前进检测
                if entity:is_standoff_available() then
                    entity:check_standoff_forward() -- 对峙移动检测
                    entity:check_standoff_countdown() -- 对峙倒计时检测
                end
                entity:ai_update() -- ai逻辑更新
            end
            :: continue_scene ::
        end
    end
    return quanta.now_ms - start_ms, iter_count
end

---停止ai循环
function AiMgr:teardown_ai_loop()
    if self.ai_loop_tid then
        timer_mgr:unregister(self.ai_loop_tid)
    end
end

function AiMgr:on_position_changed(enemy, scene_id, x, y, z)
    local scene = enemy:get_scene()
    if not scene then
        return
    end
    local scene_ai_entities = (enemy.attent_ai_entities or {})[scene_id]
    if not scene_ai_entities then
        return
    end
    for ai_entity_id, _ in pairs(scene_ai_entities) do
        local ai_entity = scene:get_entity(ai_entity_id)
        if ai_entity == nil then
            goto continue
        end
        local enemy_id = enemy:get_id()
        -- 仅敌对关系和有仇恨关系时ai服才需要关注
        local is_hated = ai_entity.hate_list ~= nil and ai_entity:entity_is_hated(enemy_id)
        if enemy:faction_is_hostile(ai_entity) or is_hated then
            -- 主动怪警戒范围判断
            if not ai_entity:has_hate_entity(enemy_id) and ai_entity:entity_in_sight({x=x, y=y, z=z}) then
                -- 添加仇恨列表 或 增加仇恨值
                ai_entity:add_hate_entity(enemy, 1, is_hated)
            end
            if ai_entity:has_hate_entity(enemy_id) and not ai_entity:hate_entity_can_chase(enemy_id) then
                -- 如果仇恨目标已不可追击，则移除仇恨关系
                ai_entity:remove_hate_entity(enemy_id)
            end
        end
        :: continue ::
    end
end

function AiMgr:get_scene_entities(proto_id, need_tostring)
    local result = {}
    for scene_id, scene_data in pairs(self.scene_ai_data_map) do
        for entity_id, entity in scene_data.entity_map:iterator() do
            if entity:get_proto_id() == proto_id then
                if result[scene_id] == nil then
                    result[scene_id] = {
                        scene_holder = scene_data.scene_holder,
                        scene_map_id = scene_data.map_id,
                        entities = {}
                    }
                end
                if need_tostring then
                    entity_id = tostring(entity_id)
                end
                tinsert(result[scene_id].entities, entity_id)
            end
        end
    end
    return result
end

function AiMgr:on_frame(now)
    local del_entitys = {}
    for scene_id, scene_data in pairs(self.scene_ai_data_map) do
        local fiter, wheel = scene_data.entity_map:wheel_iterator(self.wheel_cur)
        for entity_id, entity in fiter do
            entity:update(now)
            if entity:is_release() then
                if not del_entitys[scene_id] then
                    del_entitys[scene_id] = {}
                end
                del_entitys[scene_id][entity_id] = entity
            end
        end
        self.wheel_cur = wheel
    end
    for scene_id, entities in pairs(del_entitys) do
        for entity_id, _ in pairs(entities) do
            self:remove_ai_entity(scene_id, entity_id)
        end
    end
end

function AiMgr:ensure_scene(scene_id, map_id, scene_holder)
    if not self.scene_ai_data_map[scene_id] then
        ---@type WheelMap
        local WheelMap = import("container/wheel_map.lua")
        self.scene_ai_data_map[scene_id] = {
            entity_map = WheelMap(10), -- ai实体列表
            map_id = map_id,
            scene_holder = scene_holder,
        }
    end
end

--实体被消耗
function AiMgr:on_ai_destory(entity)
end

function AiMgr:add_ai_entity(scene_id, entity_id, entity, map_id, scene_holder)
    --log_info("[AiMgr][add_ai_entity] scene_id={}, entity_id={}", scene_id, entity_id)
    self:ensure_scene(scene_id, map_id, scene_holder)
    self.scene_ai_data_map[scene_id].entity_map:set(entity_id, entity)
end

function AiMgr:remove_ai_entity(scene_id, entity_id)
    --log_info("[AiMgr][remove_ai_entity] scene_id={}, entity_id={}", scene_id, entity_id)
    if not self.scene_ai_data_map[scene_id] or not entity_id then
        return
    end
    local entity = self:get_ai_entity(scene_id, entity_id)
    if not entity then
        return
    end
    self:on_ai_destory(entity)
    self.scene_ai_data_map[scene_id].entity_map:set(entity_id, nil)
    entity:destory()
end

function AiMgr:get_ai_entity(scene_id, entity_id)
    if not self.scene_ai_data_map[scene_id] or not entity_id then
        return nil
    end
    return self.scene_ai_data_map[scene_id].entity_map:get(entity_id)
end

---分配一个 AI服
---@return integer AI服Id
function AiMgr:assign_aisvr(entity)
    local entity_id = entity.id
    local map_id = entity:get_map_id()
    local scene = entity:get_scene()
    if not scene and map_id ~= nil then
        scene = scene_mgr:get_scene(map_id)
    end
    local scene_entity = scene:find_entity(entity_id)
    if scene_entity and scene_entity:get_scene() ~= nil then
        entity = scene_entity
        -- 更新出生点位置
        entity:set_spawn_pos({
            x = entity:get_pos_x(),
            y = entity:get_pos_y(),
            z = entity:get_pos_z(),
        })
    end
    local scene_id = scene:get_id()
    map_id = scene:get_map_id()
    local scene_holder
    if scene.get_holder ~= nil then
        scene_holder = scene:get_holder()
    end
    self:ensure_scene(scene_id, map_id, scene_holder)
    self:on_associated_to_aisvr(entity, scene_id, map_id, scene_holder)
end

---@param entity AiComponent
function AiMgr:on_associated_to_aisvr(entity, scene_id, map_id, scene_holder)
    if not entity then
        return
    end
    local entity_id = entity.id
    if entity:is_release() then
        return
    end
    self:add_ai_entity(scene_id, entity_id, entity, map_id, scene_holder)
end

---@param entity AiComponent
function AiMgr:on_disassociated_to_aisvr(entity, scene_id)
    if not entity or not scene_id then
        return
    end
    local entity_id = entity.id
    self:remove_ai_entity(scene_id, entity_id, entity)
    -- 清空仇恨列表
    entity.hate_list:clear()
end

function AiMgr:on_scene_closed(scene_id, map_id)
    -- log_debug('[AiMgr]on_scene_closed(): scene_id={}, map_id={}', scene_id, map_id)
    local scene_data = self.scene_ai_data_map[scene_id]
    if not scene_data then
        return
    end
    for _, entity in scene_data.entity_map:iterator() do
        self:on_disassociated_to_aisvr(entity, scene_id)
    end
    scene_data.entity_map = nil
    self.scene_ai_data_map[scene_id] = nil
end

-- export
quanta.ai_mgr = AiMgr()

return AiMgr
