--utility_servlet.lua
local log_debug             = logger.debug
local log_warn              = logger.warn
local sformat               = string.format
local event_mgr             = quanta.get("event_mgr")
local gm_mgr                = quanta.get("gm_mgr")
local protobuf_mgr          = quanta.get("protobuf_mgr")
local config_mgr            = quanta.get("config_mgr")
local client_reward_db      = config_mgr:init_table("client_reward", "id")
local area_db               = config_mgr:init_table("area", "id")
local blueprint_db          = config_mgr:init_table("blueprint", "id")

local PACKET_ITEM           = protobuf_mgr:enum("packet_type", "PACKET_ITEM")

local FRAME_SUCCESS         = protobuf_mgr:error_code("FRAME_SUCCESS")
local FRAME_PARAMS          = protobuf_mgr:error_code("FRAME_PARAMS")
local FRAME_UPHOLD          = protobuf_mgr:error_code("FRAME_UPHOLD")
local NOT_CLIREWARD         = protobuf_mgr:error_code("UTILITY_NOT_CLIREWARD")
local CLIREWARD_RECEIVED    = protobuf_mgr:error_code("UTILITY_CLIREWARD_RECEIVED")
local SETTING_FAILED        = protobuf_mgr:error_code("UTILITY_SETTING_FAILED")
local PICTORIALS_FAILED     = protobuf_mgr:error_code("UTILITY_PICTORIALS_FAILED")
local CPOSITIONS_FAILED     = protobuf_mgr:error_code("UTILITY_CPOSITIONS_FAILED")
local DEL_DATA_NOT_EXIST    = protobuf_mgr:error_code("SAVING_DEL_DATA_NOT_EXIST")
local QUERY_DATA_NOT_EXIST  = protobuf_mgr:error_code("SAVING_QUERY_DATA_NOT_EXIST")
local UTILITY_AREA_NOT      = protobuf_mgr:error_code("UTILITY_AREA_NOT")

-- 客户端奖励配置
local UtilityServlet        = singleton()
local prop = property(UtilityServlet)
prop:reader("gm_status", false)

function UtilityServlet:__init()
    -- GM初始状态
    if environ.status("QUANTA_GM_CLIENT") then
        self.gm_status = true
    end
    blueprint_db:add_group("level")

    -- 事件监听
    event_mgr:add_trigger(self, "on_login_success")
    event_mgr:add_trigger(self, "on_transfer_reddot")
    event_mgr:add_trigger(self, "on_item_add")
    event_mgr:add_trigger(self, "on_item_cost")
    event_mgr:add_trigger(self, "on_level_up")

    -- RPC调用
    -- 切换GM客户端开关
    event_mgr:add_listener(self, "rpc_bgip_gm_client")

    -- CS协议
    protobuf_mgr:register(self, "NID_UTILITY_NEWBEE_COMPLETE_REQ", "on_newbee_complete_req")
    protobuf_mgr:register(self, "NID_UTILITY_SETTING_SAVE_REQ", "on_setting_save_req")
    protobuf_mgr:register(self, "NID_UTILITY_SETTING_REQ", "on_setting_req")
    protobuf_mgr:register(self, "NID_UTILITY_REDDOT_ADD_REQ", "on_reddot_add_req")
    protobuf_mgr:register(self, "NID_UTILITY_REDDOT_READ_REQ", "on_reddot_read_req")
    protobuf_mgr:register(self, "NID_UTILITY_PICTORIAL_REQ", "on_pictorial_req")
    protobuf_mgr:register(self, "NID_UTILITY_GET_POSITIONS_REQ", "on_get_positions_req")
    protobuf_mgr:register(self, "NID_UTILITY_ADD_POSITION_REQ", "on_add_position_req")
    protobuf_mgr:register(self, "NID_UTILITY_DEL_POSITION_REQ", "on_del_position_req")
    protobuf_mgr:register(self, "NID_UTILITY_EDIT_POSITION_REQ", "on_edit_position_req")
    protobuf_mgr:register(self, "NID_UTILITY_GM_COMMAND_REQ", "on_gm_command_req")

    protobuf_mgr:register(self, "NID_UTILITY_GET_CLIREWARD_REQ", "on_get_clireward_req")
    protobuf_mgr:register(self, "NID_UTILITY_FINISH_CLIREWARD_REQ", "on_finish_clireward_req")

    protobuf_mgr:register(self, "NID_SAVING_ADD_REQ", "on_add_saving_req")
    protobuf_mgr:register(self, "NID_SAVING_DEL_REQ", "on_del_saving_req")
    protobuf_mgr:register(self, "NID_SAVING_QUERY_REQ", "on_query_saving_req")

    protobuf_mgr:register(self, "NID_UTILITY_AREA_UNLOCK_REQ", "on_area_unlock_req")
end

--事件监听
-------------------------------------------------------------------------
-- 登陆成功
function UtilityServlet:on_login_success(player_id, player)
    --同步红点
    --TODO_FIX player:sync_reddot()
    --发送bilog数据(客户端埋点使用)
    player:send("NID_UTILITY_BILOG_NTF", { create_role_time = player:get_create_time(), total_cost = 0 })
    --同步区域
    --TODO_FIX player:send("NID_UTILITY_AREA_NTF", { areas = player:pack_areas()})
end

-- 转移红点
function UtilityServlet:on_transfer_reddot(player, opts)
    log_debug("[UtilityServlet][on_transfer_reddot] player_id:{}, opts={}", player:get_id(), opts)
    player:transfer_reddot(opts)
end

--玩家道具增加
function UtilityServlet:on_item_add(player, item_id, num, reason, packet_id)
    log_debug("[UtilityServlet][on_item_add] player_id:{}, item:{}, num:{}, reason:{} packet_id:{}", player:get_id(),
    item_id, num, reason, packet_id)
    player:add_pictorial(item_id, num, packet_id)
end


--玩家道具减少
function UtilityServlet:on_item_cost(player, item_id, num, reason, after_count, packet_id)
    log_debug("[UtilityServlet][on_item_cost] player_id:{}, item:{}, num:{}, reason:{} after_count:{} packet_id:{}", player:get_id(),
    item_id, num, reason, after_count, packet_id)
    if packet_id == PACKET_ITEM and after_count == 0 then
        player:read_item_reddot(item_id, packet_id)
    end
end

--玩家家园升级
function UtilityServlet:on_level_up(player, level)
    log_debug("[UtilityServlet][on_level_up] player_id:{}, level:{}", player:get_id(), level)
    -- 获取解锁的配置
    local blueprints = blueprint_db:find_group(level)
    if blueprints and next(blueprints) then
        for _, blueprint in pairs(blueprints) do
            -- 非道具激活
            if not blueprint.item_id or blueprint.item_id == 0 then
                if blueprint.reddot_type and blueprint.reddot_rid then
                    local opts = {
                        reddot_type = blueprint.reddot_type,
                        reddot_rid = blueprint.reddot_rid,
                        amount = 1,
                        customs = { blueprint.id }
                    }
                    player:add_reddot(opts)
                    log_debug("[UtilityServlet][on_level_up] reddot_log add_reddot player_id:{}, level:{} blueprint:{}", player:get_id(), level, blueprint.id)
                else
                    log_warn("[UtilityServlet][on_level_up] reddot is err player_id:{}, level:{} blueprint:{}", player:get_id(), level, blueprint)
                end
            end
        end
    end
end

--rpc协议
-------------------------------------------------------------------------

--切换GM客户端开关
function UtilityServlet:rpc_bgip_gm_client(status)
    self.gm_status = status
    return FRAME_SUCCESS, {msg = sformat("gm_status:%s", status)}
end

--CS协议
-------------------------------------------------------------------------
--已读红点
function UtilityServlet:on_reddot_read_req(player, player_id, message)
    local reddot_id = message.reddot_id
    local customs = message.customs or {}
    log_debug("[UtilityServlet][on_reddot_read_req] reddot_log player_id:{}, reddot_id:{} customs:{}", player_id, reddot_id, customs)
    player:read_reddot(reddot_id, customs)
    return FRAME_SUCCESS, { error_code = 0 }
end

-- 新增红点
function UtilityServlet:on_reddot_add_req(player, player_id, message)
    log_debug("[UtilityServlet][on_reddot_add_req] player:{}, message:{}", player_id, message)
    local reddot = message.reddot_data;
    local ret_code = player:add_reddot(reddot)
    return FRAME_SUCCESS, { error_code = ret_code }
end

--完成新手引导
function UtilityServlet:on_newbee_complete_req(player, player_id, message)
    log_debug("[UtilityServlet][on_newbee_complete_req] player:{}, message:{}", player_id, message)
    player:update_newbee(message.newbee_id)
    return FRAME_SUCCESS, { error_code = 0 }
end

--保存客户端设置
function UtilityServlet:on_setting_save_req(player, player_id, message)
    log_debug("[UtilityServlet][on_setting_save_req] player:{}, message:{}", player_id, message)
    player:update_setting(message.setting_data)
    return FRAME_SUCCESS, { error_code = 0 }
end

--获取客户端设置
function UtilityServlet:on_setting_req(player, player_id, message)
    log_debug("[UtilityServlet][on_setting_req] player_id:{}", player_id)
    if player:load_settings() then
        local setting_data = player:get_settings()
        return FRAME_SUCCESS, { error_code = 0, setting_data = setting_data }
    end
    return FRAME_SUCCESS, { error_code = SETTING_FAILED }
end

--获取图鉴信息
function UtilityServlet:on_pictorial_req(player, player_id, message)
    log_debug("[UtilityServlet][on_pictorial_req] player_id:{}", player_id)
    if player:load_pictorials() then
        local pictorials = player:get_pictorials()
        return FRAME_SUCCESS, { error_code = 0, pictorials = pictorials }
    end
    return FRAME_SUCCESS, { error_code = PICTORIALS_FAILED }
end

-- 获取收藏位置
function UtilityServlet:on_get_positions_req(player, player_id, message)
    log_debug("[UtilityServlet][on_get_positions_req] player_id:{}", player_id)
    if player:load_cpositions() then
        local cpositions = player:get_cpositions()
        return FRAME_SUCCESS, { error_code = 0, positions = cpositions }
    end
    return FRAME_SUCCESS, { error_code = CPOSITIONS_FAILED }
end

-- 添加收藏位置
function UtilityServlet:on_add_position_req(player, player_id, message)
    log_debug("[UtilityServlet][on_add_position_req] player_id:{}", player_id)
    -- 位置数据
    local position = message.position
    -- 位置数据验证
    if not position then
        log_warn("[UtilityServlet][on_add_position_req] is fail(position) player_id:{} message={}", player_id, message)
        return FRAME_SUCCESS, { error_code = FRAME_PARAMS }
    end

    -- 位置验证
    if not player:is_collect_position_verify(position) then
        log_warn("[UtilityServlet][on_add_position_req] is fail(is_collect_position_verify) player_id:{} message={}",
        player_id, message)
        return FRAME_SUCCESS, { error_code = FRAME_PARAMS }
    end

    local new_position = player:add_collect_position(position)
    return FRAME_SUCCESS, { error_code = 0, position = new_position }
end

-- 删除收藏位置
function UtilityServlet:on_del_position_req(player, player_id, message)
    log_debug("[UtilityServlet][on_del_position_req] player_id:{}", player_id)

    -- 位置id
    local id = message.id
    -- 位置数据验证
    if not id then
        log_warn("[UtilityServlet][on_del_position_req] is fail player_id:{} message={}", player_id, message)
        return FRAME_SUCCESS, { error_code = FRAME_PARAMS }
    end

    player:del_collect_position(id)
    return FRAME_SUCCESS, { error_code = 0, id = id }
end

-- 编辑收藏位置
function UtilityServlet:on_edit_position_req(player, player_id, message)
    log_debug("[UtilityServlet][on_edit_position_req] player_id:{}", player_id)
    -- 标识
    local id = message.id
    -- 名称
    local name = message.name
    -- 位置
    local type = message.type
    -- 数据验证
    if not name then
        log_warn("[UtilityServlet][on_edit_position_req] is fail(name) player_id:{} message={}", player_id, message)
        return FRAME_SUCCESS, { error_code = FRAME_PARAMS }
    end

    local result = player:edit_collect_position(id, name, type)
    if not result then
        log_warn("[UtilityServlet][on_edit_position_req] is fail(result) player_id:{} message={}", player_id, message)
        return FRAME_SUCCESS, { error_code = FRAME_PARAMS }
    end
    return FRAME_SUCCESS, { error_code = 0, id = id, name = name, type = type }
end

-- gm命令
function UtilityServlet:on_gm_command_req(player, player_id, message)
    log_debug("[UtilityServlet][on_gm_command_req] player_id:{}", player_id)
    if not self.gm_status then
        log_warn("[UtilityServlet][on_gm_command_req] player_id:{} use gm", player_id)
        return FRAME_SUCCESS, { error_code = FRAME_UPHOLD }
    end
    -- 指令
    local command = message.command
    if not command then
        log_warn("[UtilityServlet][on_gm_command_req] is fail(command is null) player_id:{} message={}", player_id, message)
        return FRAME_SUCCESS, { error_code = FRAME_PARAMS }
    end

    -- 返回结果
    local result = {
        error_code = FRAME_SUCCESS,
        error_msg = ""
    }
    -- 通知执行命令
    local ok, res = gm_mgr:execute_command(command)
    if not ok or (not res or res.code ~= 0) then
        result.code = FRAME_PARAMS
        if res and res.msg then
            result.error_msg = res.msg
        else
            result.error_msg = "Unknown error, contact technical personnel"
        end
    end
    log_debug("result:{}", result)
    return FRAME_SUCCESS, {error_code = result.code, error_msg = result.error_msg}
end

-- 获取客户端奖励信息
function UtilityServlet:on_get_clireward_req(player, player_id, message)
    log_debug("[UtilityServlet][on_get_clireward_req] player_id:{}", player_id)
    local id = message.id
    local conf = client_reward_db:find_one(id)
    if not conf or conf.enable == 0 or conf.limit_count == 0 then
        log_warn("[UtilityServlet][on_get_clireward_req] is fail(conf)(player={} id={})", self:get_id(), id)
        return FRAME_SUCCESS, { error_code = NOT_CLIREWARD }
    end
    -- 刷新红点
    --TODO_FIX player:refresh_clireward_reddot(conf)
    local clirewards = player:pack_clireward(id)
    return FRAME_SUCCESS, { error_code = 0, data = clirewards }
end

-- 完成客户端奖励
function UtilityServlet:on_finish_clireward_req(player, player_id, message)
    log_debug("[UtilityServlet][on_finish_clireward_req] player_id:{}", player_id)
    local id = message.id
    local conf = client_reward_db:find_one(id)
    if not conf or conf.enable == 0 or conf.limit_count == 0 then
        log_warn("[UtilityServlet][on_finish_clireward_req] is fail(conf)(player={} id={})", player:get_id(), id)
        return FRAME_SUCCESS, { error_code = NOT_CLIREWARD }
    end
    local has_count = player:check_cliereward_count(conf)
    if not has_count then
        return FRAME_SUCCESS, { error_code = CLIREWARD_RECEIVED }
    end
    local clireward = player:finish_clireward(id, conf)
    return FRAME_SUCCESS, { error_code = 0, data = clireward }
end

function UtilityServlet:on_add_saving_req(player, player_id, message)
    local key, value = message.data.key, message.data.value
    log_debug("[UtilityServlet][on_add_saving_req] player_id:{} {}={}", player_id, key, value)
    player:save_saving_field(key, value)
    return FRAME_SUCCESS, { error_code = 0 }
end

function UtilityServlet:on_del_saving_req(player, player_id, message)
    local key = message.key
    if player:get_saving(key) == nil then
        return FRAME_SUCCESS, { error_code = DEL_DATA_NOT_EXIST }
    end
    player:del_saving_field(key)
    return FRAME_SUCCESS, { error_code = 0 }
end

function UtilityServlet:on_query_saving_req(player, player_id, message)
    local datas = player:get_saving()
    local data = datas[message.key]
    if data == nil then
        return FRAME_SUCCESS, { error_code = QUERY_DATA_NOT_EXIST; }
    end
    return FRAME_SUCCESS, { error_code = 0, value = data }
end

-- 区域解锁
function UtilityServlet:on_area_unlock_req(player, player_id, message)
    local id = message.id
    log_debug("[UtilityServlet][on_area_unlock_req] player_id:{} {}", player_id, id)

    local conf = area_db:find_one(id)
    if not conf then
        log_warn("[UtilityServlet][on_area_unlock_req] is fail(conf)(player={} id={})", player:get_id(), id)
        return FRAME_SUCCESS, { error_code = UTILITY_AREA_NOT }
    end

    local ch_code = player:check_area(id)
    if ch_code ~= FRAME_SUCCESS then
        log_warn("[UtilityServlet][on_area_unlock_req] check_area(conf)(player={} id={} ch_code={})", player:get_id(), id, ch_code)
        return FRAME_SUCCESS, { error_code = ch_code }
    end

    local un_code = player:area_unlock(conf)
    if un_code ~= FRAME_SUCCESS then
        log_warn("[UtilityServlet][on_area_unlock_req] area_unlock(conf)(player={} id={} un_code={})", player:get_id(), id, un_code)
        return FRAME_SUCCESS, { error_code = un_code }
    end
    return FRAME_SUCCESS, { error_code = 0}
end

quanta.utility_servlet = UtilityServlet()

return UtilityServlet
