--scene_servlet.lua
local ipairs            = ipairs
local mmax              = math.max
local mfloor            = math.floor
local log_debug         = logger.debug

local scene_mgr         = quanta.get("scene_mgr")
local player_mgr        = quanta.get("player_mgr")
local config_mgr        = quanta.get("config_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")

local FRAME_FAILED      = protobuf_mgr:error_code("FRAME_FAILED")
local FRAME_SUCCESS     = protobuf_mgr:error_code("FRAME_SUCCESS")
local FRAME_PARAMS      = protobuf_mgr:error_code("FRAME_PARAMS")
local CANT_TRANSFOR     = protobuf_mgr:error_code("SCENE_CANT_TRANSFOR")
local ACTION_EMPTY      = protobuf_mgr:error_code("SCENE_ACTION_EMPTY")

local utility_db        = config_mgr:init_table("utility", "key")

-- 最小坠落伤害高度
local MIN_FALLLING_HEIGHT = utility_db:find_integer("value", "min_fallling_height") or 500
-- 坠落伤害百分比: 2%/m, 对应0.02%/cm
local FALLING_PRECENT     = (utility_db:find_integer("value", "falling_precent") or 2) / 100 / 100

local SceneServlet = singleton()
function SceneServlet:__init()
    -- CS协议
    protobuf_mgr:register(self, "NID_ACTOR_ACTION_REQ", "on_actor_action_req")
    protobuf_mgr:register(self, "NID_ACTOR_FALLING_REQ", "on_actor_falling_req")
    protobuf_mgr:register(self, "NID_ENTITY_SOC_ACTION_REQ", "on_soc_action_req")
    protobuf_mgr:register(self, "NID_ENTITY_TRANS_SCENE_REQ", "on_trans_scene_req")
    protobuf_mgr:register(self, "NID_ENTITY_LEAVE_STUCK_REQ", "on_leave_stuck_req")
end

--CS协议
-------------------------------------------------------------------------
function SceneServlet:on_trans_scene_req(player, player_id, message)
    local pos_x, pos_y, pos_z, dir_y, map_id = message.pos_x, message.pos_y, message.pos_z, message.dir_y, message.map_id
    log_debug("[SceneServlet][on_trans_scene_req] player({}-{})!", player_id, map_id)
    local cur_scene = player:get_scene()
    if cur_scene:get_map_id() == map_id then
        cur_scene:trans_entity(player, pos_x, pos_y, pos_z, dir_y)
        return FRAME_SUCCESS, { error_code =  0}
    end
    scene_mgr:trans_scene(player, map_id, pos_x, pos_y, pos_z, dir_y)
    return FRAME_SUCCESS, { error_code =  0}
end

function SceneServlet:on_homenpc_move_req(player, player_id, message)
    local npc_id = message.npc_id
    log_debug("[SceneServlet][on_homenpc_move_req] player({}) npc({})!", player_id, npc_id)
    local scene = player:get_scene()
    if not scene or (not scene:is_home()) or (not scene:is_owner(player_id)) then
        return CANT_TRANSFOR
    end
    local npc_entity = player_mgr:get_entity(npc_id)
    if not npc_entity then
        return CANT_TRANSFOR
    end
    scene:trans_entity(npc_entity, message.pos_x, message.pos_y, message.pos_z, message.dir_y)
    return FRAME_SUCCESS, { error_code =  0}
end

-- 行为上报限定(player自己上报、player上报其随从)
function SceneServlet:action_req_limit(entity, entity_id, player_id)
    return entity_id == player_id or (entity:is_npc() and entity:is_owned_by(player_id))
end

function SceneServlet:on_actor_action_req(player, player_id, message)
    local scene = player:get_scene()
    if scene == nil then
        return FRAME_SUCCESS, { error_code = FRAME_FAILED }
    end
    local entity_id, actions = message.entity_id, message.actions
    --log_debug("[SceneServlet][on_actor_action_req] player({}), entity_id({})", player_id, entity_id)
    -- 判断上报者
    local entity = scene:get_entity(entity_id)
    entity = entity or player
    if not self:action_req_limit(entity, entity_id, player_id) then
        return FRAME_SUCCESS, { error_code = FRAME_PARAMS }
    end
    -- 没有动作数据
    if #actions < 1 then
        return FRAME_SUCCESS, { error_code = ACTION_EMPTY }
    end
    -- 判断是否在传送
    local trans_threshold = 1200 -- 传送等待时间阈值(单位: ms, 该期间内，忽略行为同步请求)
    local trans_scene_start = entity:get_trans_scene_start()
    if trans_scene_start > 0 and quanta.now_ms - trans_scene_start < trans_threshold then
        -- 在传送时间阈值内, 直接返回
        return FRAME_SUCCESS, { error_code = FRAME_SUCCESS }
    end
    entity:set_trans_scene_start(0) -- 重置传送时间开始记录
    local results = {}
    local response = { entity_id = entity_id, full_sync = message.full_sync }
    local has_success = false
    local error_code, valid_pos = entity:action_position_limit(entity_id, message.pos, message.dir, message.timestamp, message.action_limit)
    if error_code ~= FRAME_SUCCESS then
        response.error_code = error_code
        response.valid_pos = valid_pos
        return FRAME_SUCCESS, response
    end
    for idx, action in ipairs(actions) do
        scene:single_action_handle(action, entity, entity_id, message, response, results, idx)
        if results[idx].success then
            has_success = true
        end
    end
    message.results = results
    -- 仅操作成功时才notify给其他人
    if has_success then
        -- 给视野内其他人的通知
        entity:sight_message("NID_ACTOR_ACTION_NTF", message)
    end
    response.results = results
    return FRAME_SUCCESS, response
end

-- 社交动作
function SceneServlet:on_soc_action_req(player, player_id, message)
    log_debug("[SceneServlet][on_soc_action_req] player({}), entity_id({})", player_id, message.type)
    local scene = player:get_scene()
    if not scene then
        return FRAME_SUCCESS, { error_code = FRAME_FAILED }
    end
    -- 广播客户
    scene:broadcast_message("NID_ENTITY_SOC_ACTION_NTF", { type = message.type, id = player_id })
    return FRAME_SUCCESS, { error_code = 0 }
end

function SceneServlet:on_actor_falling_req(player, player_id, message)
    log_debug("[SceneServlet][on_actor_falling_req] player({}), entity_id({})", player_id, message.entity_id)
    local scene = player:get_scene()
    if scene == nil then
        return FRAME_SUCCESS, { error_code = FRAME_FAILED }
    end
    local response = { error_code = FRAME_SUCCESS }
    local hp = player:get_hp()
    local hp_max = player:get_hp_max()
    local damage = mmax(0, message.height - MIN_FALLLING_HEIGHT) * FALLING_PRECENT * hp_max
    local new_hp = mmax(1, hp - damage)
    if new_hp == 1 then
        player:clean_cache_safe_position()
        player:set_sp(0) -- 清空SP
        player:target_dying_check(player)
    end
    new_hp = mfloor(new_hp)
    player:set_hp(new_hp)
    response.damage = mfloor(damage)
    response.hp = new_hp
    return FRAME_SUCCESS, response
end

function SceneServlet:on_leave_stuck_req(player, player_id, message)
    log_debug("[SceneServlet][on_leave_stuck_req] player({})", player_id)
    local scene = player:get_scene()
    if scene == nil then
        return FRAME_SUCCESS, { error_code = FRAME_FAILED }
    end
    player:trans_nearest_safety_point(scene)
    return FRAME_SUCCESS, { error_code = FRAME_SUCCESS }
end

quanta.scene_servlet = SceneServlet()

return SceneServlet
