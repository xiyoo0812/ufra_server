--world_servlet.lua
local Character         = import("world/entity/character.lua")

local log_err           = logger.err
local log_info          = logger.info
local log_debug         = logger.debug
local mrandom           = qmath.random
local env_get           = environ.get
local env_number        = environ.number

local event_mgr         = quanta.get("event_mgr")
local world_mgr         = quanta.get("world_mgr")
local scene_mgr         = quanta.get("scene_mgr")
local player_mgr        = quanta.get("player_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")
local online            = quanta.get("online")

local FRAME_SUCCESS     = protobuf_mgr:error_code("FRAME_SUCCESS")
local ROLE_NOT_EXIST    = protobuf_mgr:error_code("LOGIN_ROLE_NOT_EXIST")
local ROLE_TOKEN_ERR    = protobuf_mgr:error_code("LOGIN_ROLE_TOKEN_ERR")
local FRAME_FAILED      = protobuf_mgr:error_code("FRAME_FAILED")
local LOGIN_PW_ERR      = protobuf_mgr:error_code("LOGIN_PW_ERR")


local WorldServlet = singleton()

function WorldServlet:__init()
    --CS协议
    protobuf_mgr:register(self, "NID_HEARTBEAT_REQ", "on_heartbeat_req")
    protobuf_mgr:register(self, "NID_LOGIN_ROLE_LOGIN_REQ", "on_role_login_req")
    protobuf_mgr:register(self, "NID_LOGIN_ROLE_LOGOUT_REQ", "on_role_logout_req")
    protobuf_mgr:register(self, "NID_LOGIN_ROLE_RELOAD_REQ", "on_role_reload_req")
end

--心跳
function WorldServlet:on_heartbeat_req(session, cmd_id, body, session_id)
    -- log_debug("[WorldServlet][on_heartbeat_req] player_id({})!", session.player_id)
    local player = player_mgr:get_entity(session.player_id)
    if player then
        player:heartbeat()
    end
    world_mgr:callback_by_id(session, cmd_id, { time = quanta.now_ms, error_code = 0 }, session_id)
end

function WorldServlet:on_role_login_req(session, cmd_id, body, session_id)
    local open_id, world_id, character, password, user_id = body.open_id, body.world_id, body.role, body.password, body.user_id
    --local open_id, token, world_id, setting, character = body.open_id, body.token, body.world_id, body.setting, body.role
    log_debug("[WorldServlet][on_role_login_req] open_id({}) body({})  login req!", open_id, body)
    --验证token
    --[[
    local ok, login_token = self:check_login_token(open_id, token)
    if not ok then
        log_err("[WorldServlet][on_role_login_req] token verify failed! player:{}, token: {}-{}", player_id, token, login_token)
        world_mgr:callback_errcode(session, cmd_id, ROLE_TOKEN_ERR, session_id)
        return
    end]]

    local character_id = character.character_id
    --当前character在当前世界是否已有角色
    local player_id
    local char_world = Character(character_id, world_id)
    if not char_world:load() then
        log_err("[WorldServlet][on_role_login_req] load player_id failed! character_id:{} world_id:{}",
                character_id, world_id)
    end
    if char_world:is_newbee() then
        --创建char在当前世界存档
        local ok, role_id = char_world:get_autoinc_id(character_id, world_id)
        if not ok then
            log_err("[WorldServlet][on_role_login_req] create player_id failed! character_id:{} world_id:{}", character_id, world_id)
            world_mgr:callback_errcode(session, cmd_id, ROLE_NOT_EXIST, session_id)
            return
        end
        player_id = role_id
        char_world:create(character_id, world_id, player_id)
    else
        player_id = char_world.role_id
    end
    local player = player_mgr:load_player(open_id, player_id)
    if not player then
        return world_mgr:callback_errcode(session, cmd_id, FRAME_FAILED, session_id)
    end

    local master = env_number("QUANTA_USER")
    -- 非房主用户需要验证密码
    if user_id ~= master then
        local token = env_get("QUANTA_WTOKEN")
        if token and token ~= "" and token ~= password then
            log_debug("[WorldServlet][on_role_login_req] token is fail world_id:{} token:{} password:{}", world_id, token, password)
            return world_mgr:callback_errcode(session, cmd_id, LOGIN_PW_ERR, session_id)
        end
    end

    --更新character
    player:update_char(character)
    local new_token = mrandom()
    session.player_id = player_id
    player:set_session(session)
    player:set_reload_token(new_token)
    online:login_service(player_id, "world", quanta.id)
    --通知登陆成功
    event_mgr:fire_frame(function()
        --玩家上线
        player:online()
        --进入场景
        scene_mgr:enter_scene(player, player:get_map_id(), player_id)
        --通知登陆成功
        event_mgr:notify_trigger("on_login_success", player_id, player)
    end)
    log_info("[WorldServlet][on_role_login_req] player({}) login success!", player_id)
    world_mgr:callback_by_id(session, cmd_id, { token = new_token, role_id = player_id, error_code = FRAME_SUCCESS } , session_id)
end

function WorldServlet:on_role_logout_req(session, cmd_id, body, session_id)
    local player_id = body.role_id
    log_debug("[WorldServlet][on_role_logout_req] player({}) logout req!", player_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        world_mgr:callback_errcode(session, cmd_id, ROLE_NOT_EXIST, session_id)
        world_mgr:close_session(session)
        return
    end
    session.player_id = nil
    player_mgr:remove_entity(player, player_id)
    log_info("[WorldServlet][on_role_logout_req] player({}) logout success!", player_id)
    world_mgr:callback_errcode(session, cmd_id, FRAME_SUCCESS, session_id)
    world_mgr:close_session(session)
end

function WorldServlet:on_role_reload_req(session, cmd_id, body, session_id)
    local player_id, token = body.role_id, body.token
    log_debug("[WorldServlet][on_role_reload_req] player({}) reload req!", player_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        world_mgr:callback_errcode(session, cmd_id, ROLE_NOT_EXIST, session_id)
        world_mgr:close_session(session)
        return
    end
    --验证token
    local old_token = player:get_reload_token()
    if token ~= old_token then
        log_err("[WorldServlet][on_role_reload_req] token verify failed! player:{}, token: {}-{}", player_id, token, old_token)
        world_mgr:callback_errcode(session, cmd_id, ROLE_TOKEN_ERR, session_id)
        world_mgr:close_session(session)
        return
    end
    local new_token = mrandom()
    session.player_id = player_id
    player:set_session(session)
    player:set_reload_token(new_token)
    online:login_service(player_id, "world", quanta.id)
    event_mgr:fire_frame(function()
        player:relive()
        event_mgr:notify_trigger("on_reload_success", player_id, player)
    end)
    log_debug("[WorldServlet][on_role_reload_req] player({}) reload success!", player_id)
    world_mgr:callback_by_id(session, cmd_id,  { token = new_token, error_code = FRAME_SUCCESS } , session_id)
end

quanta.world_servlet = WorldServlet()

return WorldServlet
