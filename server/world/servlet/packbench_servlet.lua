--packbench_servlet.lua
--组装台
local log_err               = logger.err
local log_debug             = logger.debug
local qfailed               = quanta.failed

local event_mgr             = quanta.get("event_mgr")
local protobuf_mgr          = quanta.get("protobuf_mgr")
local object_fty            = quanta.get("object_fty")

local FRAME_FAILED          = protobuf_mgr:error_code("FRAME_FAILED")
local FRAME_SUCCESS         = protobuf_mgr:error_code("FRAME_SUCCESS")
local BLUEP_NOT_EXIST       = protobuf_mgr:error_code("WORKS_BLUEP_NOT_EXIST")
-- local BLUEP_NOT_ACTIVE      = protobuf_mgr:error_code("WORKS_BLUEP_NOT_ACTIVE")
local BUING_NOT_EXIST       = protobuf_mgr:error_code("WORKS_BUING_NOT_EXIST")
local QUEUE_FULL         = protobuf_mgr:error_code("WORKS_QUEUE_FULL")
local PRODUCT_NOT_EXIST     = protobuf_mgr:error_code("WORKS_PRODUCT_NOT_EXIST")
local MAKINGS_INVALID       = protobuf_mgr:error_code("WORKS_MAKINGS_INVALID")
local PRODUCT_IS_EMPTY      = protobuf_mgr:error_code("WORKS_PRODUCT_IS_EMPTY")
local PRODUCT_NOT_RECV      = protobuf_mgr:error_code("WORKS_PRODUCT_NOT_RECV")

local COST_MAKINGS          = protobuf_mgr:enum("cost_reason", "NID_COST_MAKINGS")
local OBTAIN_MACHINE        = protobuf_mgr:enum("obtain_reason", "NID_OBTAIN_MACHINE")
local OBTAIN_MACHBACK       = protobuf_mgr:enum("obtain_reason", "NID_OBTAIN_MACHBACK")

local PackbenchServlet        = singleton()
function PackbenchServlet:__init()
    protobuf_mgr:register(self, "NID_PRODUCE_PKBSET_REQ", "on_pkbset_req")   -- 选择蓝图
    protobuf_mgr:register(self, "NID_PRODUCE_PKBCMT_REQ", "on_pkbcmt_req")   -- 提交材料
    protobuf_mgr:register(self, "NID_PRODUCE_PKBCAN_REQ", "on_pkbcan_req")   -- 取消蓝图
    protobuf_mgr:register(self, "NID_PRODUCE_PKBGET_REQ", "on_pkbget_req")   -- 领取请求
end

--CS协议
-------------------------------------------------------------------------
-- 选择蓝图
function PackbenchServlet:on_pkbset_req(player, player_id, message)
    local town = player:get_scene()
    local id, blue_id = message.id, message.blue_id
    log_debug("[PackbenchServlet][on_pkbset_req] player{} make ({}-{})!", player_id, id, blue_id)
    local blueprint = object_fty:find_blueprint(blue_id)
    if not blueprint then
        log_err("[PackbenchServlet][on_pkbset_req] {} blueprint{} not found", player_id, blue_id)
        return FRAME_SUCCESS, { error_code = BLUEP_NOT_EXIST }
    end
    -- 测试代码
    -- if not player:check_blue_active(player, blueprint) then
    --     log_err("[PackbenchServlet][on_pkbset_req] {} blueprint{} not active", player_id, blue_id)
    --     return FRAME_SUCCESS, { error_code = BLUEP_NOT_ACTIVE }
    -- end
    local produce = town:get_produces(id)
    if not produce then
        log_err("[PackbenchServlet][on_pkbset_req] {} produce{} not found", player_id, id)
        return FRAME_SUCCESS, { error_code = BUING_NOT_EXIST }
    end
    if not produce:check_queue_num(blue_id, player_id) then
        log_err("[PackbenchServlet][on_pkbset_req] {} blueprint{} queue overflow", player_id, id)
        return FRAME_SUCCESS, { error_code = QUEUE_FULL }
    end
    local result = produce:add_product(blue_id)
    if not result then
        log_err("[PackbenchServlet][on_pkbset_req] add_product false player{} id{} blue_id{}", player_id, id, blue_id)
        return FRAME_SUCCESS, { error_code = FRAME_FAILED }
    end
    event_mgr:notify_trigger("on_product_blueprint", player, produce, blueprint, 1)
    return FRAME_SUCCESS, { error_code = 0 }
end

-- 提交材料
function PackbenchServlet:on_pkbcmt_req(player, player_id, message)
    local town = player:get_scene()
    local id, uuid, makings = message.id, message.uuid, message.makings
    log_debug("[PackbenchServlet][on_pkbcmt_req] player{} combine({}-{})!", player_id, id, makings)
    local produce = town:get_produces(id)
    if not produce then
        log_err("[PackbenchServlet][on_pkbcmt_req] {} produce{} not found", player_id, id)
        return FRAME_SUCCESS, { error_code = BUING_NOT_EXIST }
    end
    local combine = produce:get_products(uuid)
    if not combine then
        log_debug("[PackbenchServlet][on_pkbcmt_req] player{} combine({}-{}) not found!", player_id, uuid, makings)
        return FRAME_SUCCESS, { error_code = PRODUCT_NOT_EXIST }
    end
    local cok, fills = combine:check_fill(makings)
    if not cok then
        log_debug("[PackbenchServlet][on_pkbcmt_req] player{} makings({}-{}) invalid!", player_id, uuid, makings)
        return FRAME_SUCCESS, { error_code = MAKINGS_INVALID }
    end
    local costs = { cost = fills, reason = COST_MAKINGS }
    local code = player:execute_cost(costs)
    if qfailed(code) then
        log_err("[PackbenchServlet][on_pkbcmt_req] player{} drop cost failed: {}!", player_id, code)
        return FRAME_SUCCESS, { error_code = code }
    end
    produce:fillin(combine, makings)
    return FRAME_SUCCESS, { error_code = 0 }
end

-- 取消蓝图
function PackbenchServlet:on_pkbcan_req(player, player_id, message)
    local town = player:get_scene()
    local id, uuid = message.id, message.uuid
    log_debug("[PackbenchServlet][on_cancel_req] player{} cancel ({}-{})!", player_id, id, uuid)
    local produce = town:get_produces(id)
    if not produce then
        log_err("[PackbenchServlet][on_cancel_req] {} produce{} not found", player_id, id)
        return FRAME_SUCCESS, { error_code = BUING_NOT_EXIST }
    end
    local product = produce:get_products(uuid, player_id)
    if not product then
        log_err("[PackbenchServlet][on_cancel_req] {} product({}-{}) not found", player_id, id, uuid)
        return FRAME_SUCCESS, { error_code = PRODUCT_NOT_EXIST }
    end
    if not product:check_cancel() then
        log_err("[PackbenchServlet][on_cancel_req] {} product({}-{}) can't receive", player_id, id, uuid)
        return FRAME_SUCCESS, { error_code = PRODUCT_NOT_RECV }
    end
    local item_back = product:cancel()
    if item_back and next(item_back) then
        local drops = { drop = item_back, reason = OBTAIN_MACHBACK }
        local code = player:execute_drop(drops)
        if qfailed(code) then
            log_err("[PackbenchServlet][on_cancel_req] player{} drop cost failed: {}!", player_id, code)
            return FRAME_SUCCESS, { error_code = code }
        end
    end
    produce:remove_product(product, uuid)
    event_mgr:notify_trigger("on_product_cancel", player, produce, product)
    return FRAME_SUCCESS, { error_code = 0 }
end

-- 领取请求
function PackbenchServlet:on_pkbget_req(player, player_id, message)
    local town = player:get_scene()
    local id, uuid = message.id, message.uuid
    log_debug("[PackbenchServlet][on_pkbget_req] player{} recv ({}-{})!", player_id, id, uuid)
    local produce = town:get_produces(id)
    if not produce then
        log_err("[PackbenchServlet][on_pkbget_req] {} produce{} not found", player_id, id)
        return FRAME_SUCCESS, { error_code = BUING_NOT_EXIST }
    end
    local product = produce:get_products(uuid, player_id)
    if not product then
        log_err("[PackbenchServlet][on_pkbget_req] {} product({}-{}) not found", player_id, id, uuid)
        return FRAME_SUCCESS, { error_code = PRODUCT_NOT_EXIST }
    end
    local goods = product:goods()
    if not goods then
        log_err("[PackbenchServlet][on_pkbget_req] {} formula({}-{}) product not found", player_id, id, uuid)
        return FRAME_SUCCESS, { error_code = PRODUCT_IS_EMPTY }
    end
    local drops = { drop = goods, reason = OBTAIN_MACHINE }
    local code = player:execute_drop(drops)
    if qfailed(code) then
        log_err("[PackbenchServlet][on_pkbget_req] player{} drop cost failed: {}!", player_id, code)
        return FRAME_SUCCESS, { error_code = code }
    end
    event_mgr:notify_trigger("on_product_receive", player, produce, product)
    produce:recv_product(product, uuid)
    player:notify_event("on_product_make", product:get_id())
    return FRAME_SUCCESS, { error_code = 0 }
end
quanta.packbench_servlet = PackbenchServlet()
return PackbenchServlet