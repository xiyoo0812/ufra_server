--produce_servlet.lua
local log_err                 = logger.err
local log_debug               = logger.debug
local log_warn                = logger.warn
local log_info                = logger.info
local qfailed                 = quanta.failed
local unserialize             = luakit.unserialize
local tkarray                 = qtable.tkarray

local mfloor                  = math.floor
local mmin                    = math.min

local event_mgr               = quanta.get("event_mgr")
local player_mgr              = quanta.get("player_mgr")
local protobuf_mgr            = quanta.get("protobuf_mgr")
local object_fty              = quanta.get("object_fty")
local config_mgr              = quanta.get("config_mgr")

local item_db                 = config_mgr:get_table("item")
local utility_db              = config_mgr:init_table("utility", "key")

local FRAME_PARAMS            = protobuf_mgr:error_code("FRAME_PARAMS")
local FRAME_FAILED            = protobuf_mgr:error_code("FRAME_FAILED")
local FRAME_SUCCESS           = protobuf_mgr:error_code("FRAME_SUCCESS")
local ROLE_NOT_EXIST          = protobuf_mgr:error_code("LOGIN_ROLE_NOT_EXIST")
local FORMULA_NOT_EXIST       = protobuf_mgr:error_code("WORKS_FORMULA_NOT_EXIST")
local ACTIVE_LEVEL_ERR        = protobuf_mgr:error_code("WORKS_ACTIVE_LEVEL_ERR")
local FORMULA_ACTIVED         = protobuf_mgr:error_code("WORKS_FORMULA_ACTIVED")
local BLUEP_NOT_EXIST         = protobuf_mgr:error_code("WORKS_BLUEP_NOT_EXIST")
local BLUEP_ACTIVED           = protobuf_mgr:error_code("WORKS_BLUEP_ACTIVED")
local BUING_NOT_EXIST         = protobuf_mgr:error_code("WORKS_BUING_NOT_EXIST")
local PRODUCT_NOT_EXIST       = protobuf_mgr:error_code("WORKS_PRODUCT_NOT_EXIST")
local PRODUCT_IS_EMPTY        = protobuf_mgr:error_code("WORKS_PRODUCT_IS_EMPTY")
local FORMULA_NOT_ACTIVE      = protobuf_mgr:error_code("WORKS_FORMULA_NOT_ACTIVE")
local QUEUE_FULL              = protobuf_mgr:error_code("WORKS_QUEUE_FULL")
local ITEM_IS_FULL            = protobuf_mgr:error_code("PACKET_ITEM_IS_FULL")
local QUEUE_INVALID           = protobuf_mgr:error_code("WORKS_QUEUE_INVALID")
local STOREBOX_NOT_EXIST      = protobuf_mgr:error_code("WORKS_STOREBOX_NOT_EXIST")
local PRODUCT_SP_INVALID      = protobuf_mgr:error_code("WORKS_PRODUCT_SP_INVALID")
local PRODUCT_NOT_SP          = protobuf_mgr:error_code("WORKS_PRODUCT_NOT_SP")
local PRODUCE_DELV_NOT_ACTIVE = protobuf_mgr:error_code("WORKS_PRODUCE_DELV_NOT_ACTIVE")
local FORMULA_COUNT_LIMIT     = protobuf_mgr:error_code("WORKS_FORMULA_COUNT_LIMIT")
local PRODUCT_NOT_RECV        = protobuf_mgr:error_code("WORKS_PRODUCT_NOT_RECV")
local BUILD_BANK_NOT_EXIST    = protobuf_mgr:error_code("WORKS_BUILD_BANK_NOT_EXIST")
local PRODUCT_NUM_ERR         = protobuf_mgr:error_code("WORKS_PRODUCT_NUM_ERR")
local OBTAIN_PRODRECV          = protobuf_mgr:enum("obtain_reason", "NID_OBTAIN_PRODRECV")

local COST_PRODUCE            = protobuf_mgr:enum("cost_reason", "NID_COST_PRODUCE")
local COST_QUEUE              = protobuf_mgr:enum("cost_reason", "NID_COST_QUEUE")
local COST_SPEEDUP            = protobuf_mgr:enum("cost_reason", "NID_COST_SPEEDUP")
local COST_DELV               = protobuf_mgr:enum("cost_reason", "NID_COST_DELV")

local PACKET_BANK             = protobuf_mgr:enum("packet_type", "PACKET_BANK")

local BOT_SAVE                = protobuf_mgr:enum("build_operate_type", "BOT_SAVE")
local BOT_TAKE                = protobuf_mgr:enum("build_operate_type", "BOT_TAKE")
local BOE_BUILD               = protobuf_mgr:enum("build_operate_entity", "BOE_BUILD")

-- 货币加速开关
local CRCY_SP_ENABLE          = utility_db:find_integer("value", "crcy_sp_enable") or 0
-- 道具加速开关
local ITEM_SP_ENABLE          = utility_db:find_integer("value", "item_sp_enable") or 0
-- 货币加速道具id
local CRCY_SP_ITEM_ID         = utility_db:find_integer("value", "crcy_sp_item_id") or 0
-- 货币加速配置
local CRCY_SP_CONF            = utility_db:find_value("value", "crcy_sp_conf") or ""
-- 货币加速配置
local DELIVERY_COSTS          = utility_db:find_value("value", "delivery_costs") or ""

-- 加载货币加速配置
local load_crcy_spconf        = function()
    local data = unserialize(CRCY_SP_CONF)
    return data[1], data[2], data[3], data[4], data[5]
end

-- 加载配送消耗配置
local load_delivery_costs     = function()
    local data = unserialize(DELIVERY_COSTS)
    return data
end

local ProduceServlet          = singleton()
function ProduceServlet:__init()
    event_mgr:add_listener(self, "rpc_active_formula")
    event_mgr:add_listener(self, "rpc_active_blueprint")
    event_mgr:add_listener(self, "on_execute_cost")
    event_mgr:add_listener(self, "on_execute_drop")
    event_mgr:add_listener(self, "on_product_done")
    event_mgr:add_listener(self, "on_produce_storebox_putin")
    event_mgr:add_listener(self, "on_execute_produce_cost")
    event_mgr:add_listener(self, "on_execute_produce_drop")

    -- 事件监听
    event_mgr:add_trigger(self, "on_building_add")         -- 添加建筑
    event_mgr:add_trigger(self, "on_building_del")         -- 删除建筑
    event_mgr:add_trigger(self, "on_building_upgrade")     -- 升级建筑
    event_mgr:add_trigger(self, "on_send_partner")         -- 派驻npc
    event_mgr:add_trigger(self, "on_recall_partner")       -- 召回npc
    event_mgr:add_trigger(self, "on_active_collpoint")     -- 激活采集点
    event_mgr:add_trigger(self, "on_partner_attr_done")    -- npc属性变化
    event_mgr:add_trigger(self, "on_satlv_chg_done")       -- 满意度变化完成

    -- CS协议
    protobuf_mgr:register(self, "NID_PRODUCE_MAKE_REQ", "on_make_req")         -- 添加生产
    protobuf_mgr:register(self, "NID_PRODUCE_CANCEL_REQ", "on_cancel_req")     -- 删除生产
    protobuf_mgr:register(self, "NID_PRODUCE_RECEIVE_REQ", "on_receive_req")   -- 领取物品
    protobuf_mgr:register(self, "NID_PRODUCE_UNQUEUE_REQ", "on_unqueue_req")   -- 解锁队列
    protobuf_mgr:register(self, "NID_PRODUCE_SORT_REQ", "on_sort_req")         -- 生产排序
    protobuf_mgr:register(self, "NID_PRODUCE_SPEEDUP_REQ", "on_speedup_req")   -- 生产加速
    protobuf_mgr:register(self, "NID_PRODUCE_UNDELIVE_REQ", "on_undelive_req") -- 激活配送
    protobuf_mgr:register(self, "NID_PRODUCE_SWDELIVE_REQ", "on_swdelive_req") -- 切换开关
    protobuf_mgr:register(self, "NID_PRODUCE_GATHER_REQ", "on_gather_req")     -- 采集请求
end

-- 添加仓库记录
function ProduceServlet:add_bank_history(town, packet_id, items, type, entity, proto_id)
    if packet_id ~= PACKET_BANK then
        return
    end
    local bank_id = town:get_bank_id()
    local bank_build = town:get_building(bank_id)
    if bank_build then
        bank_build:add_history(type, entity, proto_id, items)
    end
end

-- 消耗
function ProduceServlet:on_execute_cost(player_id, cost, reason, town, packet_id, proto_id)
    log_debug("[ProduceServlet][on_execute_cost] player:{} proto_id:{}", player_id, proto_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        log_err("[ProduceServlet][on_execute_cost] need login, player_id:{}", player_id)
        return ROLE_NOT_EXIST
    end
    local costs = { cost = cost, reason = reason, packet_id = packet_id }
    local code = player:execute_cost(costs)
    if qfailed(code) then
        log_warn("[ProduceServlet][on_execute_cost] player:{} args:{} cost failed:{}!", player:get_id(), reason, code)
        return code
    end
    -- 添加仓库记录
    self:add_bank_history(town, packet_id, cost, BOT_TAKE, BOE_BUILD, proto_id)
    return FRAME_SUCCESS
end

-- 掉落
function ProduceServlet:on_execute_drop(player_id, drop, reason, town, packet_id, proto_id)
    log_debug("[ProduceServlet][on_execute_drop] player:{} proto_id:{}", player_id, proto_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        log_err("[ProduceServlet][on_execute_drop] need login, player_id:{}", player_id)
        return ROLE_NOT_EXIST
    end
    local drops = { drop = drop, reason = reason, packet_id = packet_id }
    local code = player:execute_drop(drops)
    if qfailed(code) then
        log_warn("[ProduceServlet][on_execute_drop] player:{} args:{} drop failed:{}!", player:get_id(), reason, code)
        return code
    end
    -- 添加仓库记录
    self:add_bank_history(town, packet_id, drop, BOT_SAVE, BOE_BUILD, proto_id)
    return FRAME_SUCCESS
end

-- 生产消耗
function ProduceServlet:on_execute_produce_cost(player_id, cost, reason, town, proto_id)
    log_debug("[ProduceServlet][on_execute_produce_cost] player:{} proto_id:{}", player_id, proto_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        log_err("[ProduceServlet][on_execute_produce_cost] need login, player_id:{}", player_id)
        return ROLE_NOT_EXIST
    end
    local costs = { cost = cost, reason = reason }
    local code, bank_costs = player:execute_produce_cost(costs, town:check_bank())
    if qfailed(code) then
        log_warn("[ProduceServlet][on_execute_produce_cost] player:{} args:{} cost failed:{}!", player:get_id(),
            reason, code)
        return code
    end
    -- 添加仓库记录
    if next(bank_costs) then
        self:add_bank_history(town, PACKET_BANK, bank_costs, BOT_TAKE, BOE_BUILD, proto_id)
    end
    return FRAME_SUCCESS
end

-- 生产掉落
-- extend：block=空间不足阻断|bank=存入仓库|pack=存入背包
function ProduceServlet:on_execute_produce_drop(produce, drop, reason, extend)
    log_debug("[ProduceServlet][on_execute_produce_drop] player:{} proto_id:{}", produce:get_master(),
        produce:get_proto_id())
    local player = player_mgr:get_entity(produce:get_master())
    if not player then
        log_err("[ProduceServlet][on_execute_produce_drop] need login, player_id:{}", produce:get_master())
        return ROLE_NOT_EXIST
    end
    local drops = { drop = drop, reason = reason }
    local code, bank_drop, last_drop = player:execute_produce_drop(drops, extend)
    if qfailed(code) then
        log_warn("[ProduceServlet][on_execute_produce_drop] player:{} reason:{} drop failed:{}!", player:get_id(),
            reason, code)
        return code
    end
    -- 添加仓库记录
    if bank_drop and next(bank_drop) then
        self:add_bank_history(player:get_scene(), PACKET_BANK, bank_drop, BOT_SAVE, BOE_BUILD, produce:get_proto_id())
    end
    -- 背包满了
    if last_drop and next(last_drop) then
        log_warn("[ProduceServlet][on_execute_produce_drop] player:{} reason:{} drop failed:{}!", player:get_id(),
            reason, last_drop)
        return ITEM_IS_FULL, last_drop
    end
    return FRAME_SUCCESS
end

-- 完成生产
function ProduceServlet:on_product_done(player_id, item_id, item_count)
    log_debug("[ProduceServlet][on_product_done] player:{} item_id:{} item_count:{}", player_id, item_id, item_count)
    -- local player = player_mgr:get_entity(player_id)
    -- if not player then
    --     log_err("[ProduceServlet][on_product_done] need login, player_id:{}", player_id)
    --     return ROLE_NOT_EXIST
    -- end
    -- player:notify_event("on_product_done", item_id, item_count)
    return FRAME_SUCCESS
end

-- 储物箱存入
function ProduceServlet:on_produce_storebox_putin(player_id, build_id, uuid)
    log_debug("[ProduceServlet][on_produce_storebox_putin] player:{} group_id:{} uuid:{}", player_id, build_id, uuid)
    local player = player_mgr:get_entity(player_id)
    if not player then
        log_err("[ProduceServlet][on_produce_storebox_putin] need login, player_id:{}", player_id)
        return ROLE_NOT_EXIST
    end
    player:notify_event("on_produce_storebox_putin", build_id, uuid)
    log_debug("[ProduceServlet][on_produce_storebox_putin] player:{} build:{} produce:{}!", player_id, build_id, uuid)
    return FRAME_SUCCESS
end

--rpc协议
-------------------------------------------------------------------------
function ProduceServlet:rpc_active_formula(player_id, formula_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        log_err("[ProduceServlet][rpc_active_drawing] need login, player_id:{}", player_id)
        return ROLE_NOT_EXIST
    end
    local formula = object_fty:find_formula(formula_id)
    if not formula then
        log_err("[ProduceServlet][rpc_active_formula] {} formula:{} not found", player_id, formula_id)
        return FORMULA_NOT_EXIST
    end
    if player:check_formula_level(formula, player) then
        log_err("[ProduceServlet][rpc_active_formula] {} formula:{} active level err", player_id, formula_id)
        return ACTIVE_LEVEL_ERR
    end
    if player:check_formula_active(formula, player) then
        log_err("[ProduceServlet][rpc_active_formula] {} formula:{} aready active", player_id, formula_id)
        return FORMULA_ACTIVED
    end
    player:active_formula(formula_id)
    return FRAME_SUCCESS
end

function ProduceServlet:rpc_active_blueprint(player_id, blue_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        log_err("[ProduceServlet][rpc_active_blueprint] need login, player_id:{}", player_id)
        return ROLE_NOT_EXIST
    end
    local blueprint = object_fty:find_blueprint(blue_id)
    if not blueprint then
        log_err("[ProduceServlet][rpc_active_blueprint] {} blueprint:{} not found", player_id, blue_id)
        return BLUEP_NOT_EXIST
    end
    if player:check_level(blueprint) then
        log_err("[ProduceServlet][rpc_active_blueprint] {} blueprint:{} active level err", player_id, blue_id)
        return ACTIVE_LEVEL_ERR
    end
    if player:check_blue_active(player, blueprint) then
        log_err("[ProduceServlet][rpc_active_blueprint] {} blueprint:{} aready active", player_id, blue_id)
        return BLUEP_ACTIVED
    end
    player:active_blueprint(blueprint)
    return FRAME_SUCCESS
end

-- 事件监听
-------------------------------------------------------------------------
-- 添加建筑
function ProduceServlet:on_building_add(town, player_id, building)
    local uuid = building.id
    local proto_id = building:get_proto_id()
    log_debug("[ProduceServlet][on_building_add] player:{} uuid:{}", player_id, uuid)
    local player = player_mgr:get_entity(player_id)
    if not player then
        log_err("[ProduceServlet][on_building_add] need login, player:{}", player_id)
        return
    end

    local produce = town:get_produces(uuid)
    if produce then
        log_warn("[ProduceServlet][on_building_add] repeat {} produce:{}", player_id, uuid)
        return
    end

    produce = object_fty:create_produce(proto_id, uuid)
    if produce then
        town:save_produces_elem(uuid, produce)
        produce:load_db({})
        produce:setup(town, building)
        produce:sync_changed()
        log_info("[ProduceServlet][on_building_add] is success player:{} proto_id:{} uuid:{}", player_id, proto_id,
            uuid)
        return
    end
    log_warn("[ProduceServlet][on_building_add] is fail player:{} proto_id:{} uuid:{}", player_id, proto_id, uuid)
end

-- 升级建筑
function ProduceServlet:on_building_upgrade(town, player_id, building)
    local uuid = building.id
    local proto_id = building:get_proto_id()
    log_debug("[ProduceServlet][on_building_upgrade] player:{} uuid:{} proto_id:{}", player_id, uuid, proto_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        log_err("[ProduceServlet][on_building_upgrade] need login, player:{} uuid:{} proto_id:{}", player_id, uuid, proto_id)
        return
    end

    local produce = town:get_produces(uuid)
    if not produce then
        log_warn("[ProduceServlet][on_building_upgrade] repeat {} produce:{} proto_id:{}", player_id, uuid, proto_id)
        return
    end

    produce:on_upgrade(building.prototype)
    produce:sync_changed()
    log_info("[ProduceServlet][on_building_upgrade] success player:{} proto_id:{} uuid:{}", player_id, proto_id, uuid)
end

-- 删除建筑
function ProduceServlet:on_building_del(town, player_id, utensil)
    local uuid = utensil.id
    log_debug("[ProduceServlet][on_building_del] player:{} uuid:{}", player_id, uuid)
    local player = player_mgr:get_entity(player_id)
    if not player then
        log_err("[ProduceServlet][on_building_del] need login, player_id:{}", player_id)
        return ROLE_NOT_EXIST
    end

    -- 仓库收回时,所有配送开关需要关闭
    if object_fty:is_bank(utensil.prototype) then
        local produces = town:get_produces()
        for _, produce in pairs(produces) do
            if produce:is_produce() then
                if produce:switch_delive_enable(false) then
                    produce:sync_changed()
                end
            end
        end
    end
    -- -- 激活配送消耗(配送功能正常开放激活之后,需将此段代码解开)
    -- local produce = town:get_produces(uuid)
    -- if not produce then
    --     log_warn("[ProduceServlet][on_building_del] not produce player:{} produce:{}", player_id, uuid)
    --     return
    -- end

    -- local cost = load_delivery_costs()
    -- if cost and next(cost) then
    --   -- 返回生产物品
    --     local drops = produce:tackback_obtain(cost, OBTAIN_BUILDING)
    --     if drops then
    --         local code = player:execute_drop(drops)
    --         if qfailed(code) then
    --             log_err("[ProduceServlet][on_building_del] player:{} produce drop cost failed: {}!", player_id, code)
    --             return FRAME_SUCCESS, { error_code = code }
    --         end
    --     end
    -- end
    town:del_produces_elem(uuid)
end

-- 派驻npc
function ProduceServlet:on_send_partner(player_id, partner, building)
    local uuid = building.id
    log_debug("[ProduceServlet][on_send_partner] player:{} uuid:{} partner:{}", player_id, uuid, partner:get_id())
    local player = player_mgr:get_entity(player_id)
    if not player then
        log_err("[ProduceServlet][on_send_partner] need login, player_id:{}", player_id)
        return ROLE_NOT_EXIST
    end
    local town = player:get_scene()
    local produce = town:get_produces(uuid)
    if not produce then
        log_warn("[ProduceServlet][on_send_partner] not produce player:{} produce:{}", player_id, uuid)
        return
    end
    produce:on_send_partner(partner)
end

-- 召回npc
function ProduceServlet:on_recall_partner(player_id, partner, building)
    local uuid = building.id
    local partner_id = partner:get_id()
    log_debug("[ProduceServlet][on_recall_partner] player:{} uuid:{} partner:{}", player_id, uuid, partner_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        log_err("[ProduceServlet][on_recall_partner] need login, player_id:{}", player_id)
        return ROLE_NOT_EXIST
    end

    local town = player:get_scene()
    local produce = town:get_produces(uuid)
    if not produce then
        log_warn("[ProduceServlet][on_recall_partner] not produce player:{} produce:{}", player_id, uuid)
        return
    end
    produce:on_recall_partner(partner)
end

-- 激活采集点
function ProduceServlet:on_active_collpoint(player_id, town, proto_id)
    log_debug("[ProduceServlet][on_active_collpoint] player:{} proto_id:{}", player_id, proto_id)
    -- 生产列表
    local produces = town:get_produces()
    -- 通知所有生产同步更新
    for _, produce in pairs(produces) do
        if object_fty:is_gather(produce.prototype) then
            produce:on_active_collpoint(proto_id)
        end
    end
end

-- npc属性变化
function ProduceServlet:on_partner_attr_done(player, build_id)
    local player_id = player:get_id()
    local town = player:get_scene()
    log_debug("[ProduceServlet][on_partner_attr_done] player:{} uuid:{} ", player_id, build_id)
    local produce = town:get_produces(build_id)
    if not produce then
        log_err("[ProduceServlet][on_partner_attr_done] {} produce:{} not found", player_id, build_id)
        return FRAME_SUCCESS, { error_code = BUING_NOT_EXIST }
    end
    produce:on_effects_change()
end

-- 满意度变化
function ProduceServlet:on_satlv_chg_done(player)
    local player_id = player:get_id()
    local town = player:get_scene()
    log_debug("[ProduceServlet][on_satlv_chg_done] player:{}", player_id)
    local produces = town:get_produces()
    if not produces then
        log_err("[ProduceServlet][on_satlv_chg_done] {} produces not found", player_id)
        return
    end

    -- 遍历生产
    for _, produce in pairs(produces) do
        produce:on_effects_change()
    end
end

--CS协议
-------------------------------------------------------------------------
function ProduceServlet:check_make(player_id, produce, formula_id, count)
    -- 数量验证
    if not count or count <= 0 then
        log_err("[ProduceServlet][check_make] {} produce:{} count:{} count err", player_id, produce.id, count)
        return PRODUCT_NUM_ERR
    end

    local formula = object_fty:find_formula(formula_id)
    if not formula then
        log_err("[ProduceServlet][check_make] {} produce:{} not formula:{}", player_id, produce.id, formula_id)
        return FORMULA_NOT_EXIST
    end

    -- 上限验证
    if count > formula.max then
        log_err("[ProduceServlet][check_make] {} produce:{} formula:{} max count:{}", player_id, produce.id, formula_id, count)
        return FORMULA_COUNT_LIMIT
    end

    -- 配方验证
    if not produce:check_formula({ formula_id }) then
        log_err("[ProduceServlet][check_make] {} produce:{} unlock formula:{}", player_id, produce.id, formula_id)
        return FORMULA_NOT_ACTIVE
    end

    if not produce:check_queue() then
        log_err("[ProduceServlet][check_make] {} produce:{} not queue", player_id, produce.id)
        return QUEUE_FULL
    end
    return FRAME_SUCCESS, formula
end

-- 添加生产
function ProduceServlet:on_make_req(player, player_id, message)
    local town = player:get_scene()
    local id, formula_id, count = message.id, message.formula_id, message.count
    log_debug("[ProduceServlet][on_make_req] player:{} produce:{} formula_id:{} count:{}!", player_id, id, formula_id,
        count)
    local produce = town:get_produces(id)
    if not produce then
        log_err("[ProduceServlet][on_make_req] {} produce:{} not found", player_id, id)
        return FRAME_SUCCESS, { error_code = BUING_NOT_EXIST }
    end

    -- 检查生产
    local ch_code, formula= self:check_make(player_id, produce, formula_id, count)
    if ch_code ~= FRAME_SUCCESS then
        log_err("[ProduceServlet][on_make_req] check_make is fail {} produce:{} ch_code:{}", player_id, id, ch_code)
        return FRAME_SUCCESS, { error_code = ch_code }
    end

    -- 刷新效果
    produce:update_effects()
    local all_costs, wheel_costs
    if not object_fty:is_gather(produce.prototype) then
        -- 获取消耗(所有消耗,一轮消耗)
        all_costs, wheel_costs = produce:formula_cost(formula, count)
        -- 扣除消耗
        if next(all_costs) then
            local cost_code = self:on_execute_produce_cost(player_id, all_costs, COST_PRODUCE, town, produce:get_proto_id())
            if cost_code ~= FRAME_SUCCESS then
                log_err("[ProduceServlet][on_make_req] {} produce:{} produce_cost is fail(code:{})", player_id, id,
                    cost_code)
                return FRAME_SUCCESS, { error_code = cost_code }
            end
        end
        -- 采集类建筑不走此协议
    else
        log_err("[ProduceServlet][on_make_req] gather {} produce:{} formula:{}", player_id, id, formula_id)
        return FRAME_SUCCESS, { error_code = FRAME_FAILED }
    end

    -- 获取产出
    local prod_time, done_time = produce:formula_time(formula.time or 0)
    local opts = {
        master = player_id,
        prod_time = prod_time,
        done_time = done_time,
        costs = wheel_costs or {},
        prod_count = 1,
        rem_count = count,
        tgt_count = count,
        sort = produce:allot_queue()
    }
    local product = produce:add_product(formula_id, opts)
    if not product then
        log_err("[ProduceServlet][on_make_req] add_product false player:{} id:{} formula:{}", player_id, id,
            formula_id)
        return FRAME_SUCCESS, { error_code = FRAME_FAILED }
    end
    return FRAME_SUCCESS, { error_code = 0 }
end

-- 取消生产
function ProduceServlet:on_cancel_req(player, player_id, message)
    local town = player:get_scene()
    local id, uuids = message.id, message.uuids
    log_debug("[ProduceServlet][on_cancel_req] player:{} produce:{} args:{}!", player_id, id, uuids)

    local produce = town:get_produces(id)
    if not produce then
        log_err("[ProduceServlet][on_cancel_req] {} produce:{} not found", player_id, id)
        return FRAME_SUCCESS, { error_code = BUING_NOT_EXIST }
    end


    local error_code = FRAME_SUCCESS
    for _, uuid in pairs(uuids) do
        local product = produce:get_products(uuid)
        if not product then
            log_warn("[ProduceServlet][on_cancel_req] {} product({}-{}) not found", player_id, id, uuid)
            goto continue
        end

        -- 取消检查
        if not product:check_cancel() then
            log_err("[ProduceServlet][on_cancel_req] {} product({}-{}) not found", player_id, id, uuid)
            error_code = PRODUCT_NOT_RECV
            goto continue
        end

        local items = product:cancel()
        -- if items and next(items) then
        --     local dcode = self:on_execute_produce_drop(produce, items, OBTAIN_PRODBACK, {
        --         block = true,
        --         bank = town:check_bank(),
        --         pack = true
        --     })
        --     if dcode ~= FRAME_SUCCESS then
        --         log_err("[PackbenchServlet][on_cancel_req] player:{} drop cost failed:{}!", player_id, dcode)
        --         error_code = dcode
        --         goto continue
        --     end
        -- end
        local dcode = self:on_execute_drop(player_id, items, OBTAIN_PRODRECV, town)
        if dcode ~= FRAME_SUCCESS then
            return FRAME_SUCCESS, { error_code = dcode }
        end

        produce:remove_product(product, uuid)
        event_mgr:notify_trigger("on_product_cancel", player, produce, product)
        :: continue ::
    end
    produce:start_produce()
    return FRAME_SUCCESS, { error_code = error_code }
end

-- 领取物品
function ProduceServlet:on_receive_req(player, player_id, message)
    local id, store_box_id = message.id, message.store_box_id
    log_debug("[ProduceServlet][on_receive_req] player:{} produce:{} store_box_id:{}!", player_id, id, store_box_id)
    local town = player:get_scene()
    local produce = town:get_produces(id)
    if not produce then
        log_err("[ProduceServlet][on_receive_req] {} produce:{} not found", player_id, id)
        return FRAME_SUCCESS, { error_code = BUING_NOT_EXIST }
    end

    local storebox = produce:get_storeboxs(store_box_id)
    if not storebox then
        log_err("[PackbenchServlet][on_receive_req] storeboxs {} product({}-{}) not found", player_id, id, store_box_id)
        return FRAME_SUCCESS, { error_code = STOREBOX_NOT_EXIST }
    end

    -- 检查物品
    local items,good_count = storebox:goods()
    if not next(items) or good_count<= 0 then
        log_err("[PackbenchServlet][on_receive_req] {} formula({}-{}) product not found", player_id, id, store_box_id)
        return FRAME_SUCCESS, { error_code = PRODUCT_IS_EMPTY }
    end

    -- 领取可能返还物品
    local dcode = self:on_execute_drop(player_id, items, OBTAIN_PRODRECV, town)
    if dcode ~= FRAME_SUCCESS then
        return FRAME_SUCCESS, { error_code = dcode }
    end

    -- 清空储物箱
    storebox:takeout()
    -- 获取对应的产品
    local product = produce:get_products(storebox:get_id())
    if not product then
        -- 解除绑定
        produce:storebox_unbind(storebox)
    elseif product:is_full() then
        -- 重新开始工作
        product:start_work()
        produce:sync_product(product)
    end

    -- 检查储物箱物品
    if not produce:storbox_check_have() then
        player:notify_event("on_produce_storebox_empty", produce:get_group(), id)
        log_debug("[ProduceServlet][on_receive_req] on_produce_storebox_empty player:{} build:{} produce:{}!", player_id, produce:get_group(), id)
    end

    -- 开始生产
    produce:start_produce()
    -- 同步数据
    produce:storebox_sync({ storebox })
    -- 这部分数据需要重新接
    -- event_mgr:notify_trigger("on_product_receive", player, produce, product)
    -- player:notify_event("on_product_make", product:get_id())
    return FRAME_SUCCESS, { error_code = 0 }
end

-- 解锁队列
function ProduceServlet:on_unqueue_req(player, player_id, message)
    local id, queue_id = message.id, message.queue_id
    log_debug("[ProduceServlet][on_unqueue_req] player:{} produce:{} queue_id:{}!", player_id, id, queue_id)

    local town = player:get_scene()
    local produce = town:get_produces(id)
    if not produce then
        log_err("[ProduceServlet][on_unqueue_req] {} produce:{} not found", player_id, id)
        return FRAME_SUCCESS, { error_code = BUING_NOT_EXIST }
    end

    -- 队列解锁验证
    local ul_code = produce:check_unqueue(queue_id)
    if ul_code ~= FRAME_SUCCESS then
        log_err("[ProduceServlet][on_unqueue_req] {} produce:{} queue_id:{} code:{}", player_id, id, queue_id, ul_code)
        return FRAME_SUCCESS, { error_code = ul_code }
    end

    local cost = produce:unlock_queue_cost(queue_id)
    if not cost then
        log_err("[ProduceServlet][on_unqueue_req] {} produce:{} queue_id:{} cost not found", player_id, id, queue_id)
        return FRAME_SUCCESS, { error_code = QUEUE_INVALID }
    end

    -- 扣除物品(走背包)
    local code = self:on_execute_cost(player_id, cost, COST_QUEUE, town)
    if qfailed(code) then
        log_warn("[ProduceServlet][on_unqueue_req] rpc_execute_cost player:{} cost failed:{}!", player:get_id(), code)
        return code
    end
    produce:unlock_queue(queue_id)
    return FRAME_SUCCESS, { error_code = FRAME_SUCCESS }
end

-- 生产排序
function ProduceServlet:on_sort_req(player, player_id, message)
    local id, source_uuid, target_uuid = message.id, message.source_uuid, message.target_uuid
    log_debug("[ProduceServlet][on_sort_req] player:{} produce:{} uuid({}-{})!", player_id, id, source_uuid, target_uuid)

    local town = player:get_scene()
    local produce = town:get_produces(id)
    if not produce then
        log_err("[ProduceServlet][on_sort_req] {} produce:{} not found", player_id, id)
        return FRAME_SUCCESS, { error_code = BUING_NOT_EXIST }
    end

    if #produce.product_list <= 1 then
        log_err("[ProduceServlet][on_sort_req] {} produce:{} product_list err", player_id, id)
        return FRAME_SUCCESS, { error_code = FRAME_PARAMS }
    end

    produce:product_swap(source_uuid, target_uuid)
    return FRAME_SUCCESS, { error_code = FRAME_SUCCESS }
end

-- 物品加速
function ProduceServlet:item_speedup(rem_time, items)
    local cost = {}
    local speedup_time = 0
    if ITEM_SP_ENABLE == 0 then
        return PRODUCT_NOT_SP, speedup_time, cost
    end
    for item_id, item_count in pairs(items) do
        local item_conf = item_db:find_one(item_id)
        if not item_conf then
            goto continue
        end
        cost[item_id] = item_count
        speedup_time = speedup_time + (item_count * (item_conf.speedup_time or 0))
        -- 消耗溢出处理
        if speedup_time > rem_time then
            cost[item_id] = cost[item_id] - mfloor((speedup_time - rem_time) / item_conf.speedup_time)
            break
        end
        :: continue ::
    end
    if not next(cost) then
        return PRODUCT_SP_INVALID, speedup_time, cost
    end
    return FRAME_SUCCESS, speedup_time, cost
end

-- 货币加速
function ProduceServlet:crcy_speedup(rem_time, crcy_count)
    local speedup_time = 0
    local cost = {}
    if CRCY_SP_ENABLE == 0 or CRCY_SP_ITEM_ID == 0 then
        return PRODUCT_NOT_SP, speedup_time, cost
    end

    -- 基础时间,基础消耗,时间单位,消耗单位
    local base_time, base_cost, _, time_unit, cost_unit = load_crcy_spconf()
    -- 货币合法性验证
    if crcy_count < base_cost or (crcy_count - base_cost) % cost_unit ~= 0 then
        return PRODUCT_SP_INVALID, speedup_time, cost
    end

    -- 根据剩余时间计算需要消耗的货币
    local rem_crcy_count = base_cost + (math.ceil((rem_time - base_time) / time_unit)) * cost_unit
    if rem_crcy_count ~= crcy_count then
        if rem_crcy_count > crcy_count then
            return PRODUCT_SP_INVALID, speedup_time, cost
        end
    end
    speedup_time = rem_time
    cost[CRCY_SP_ITEM_ID] = rem_crcy_count
    return FRAME_SUCCESS, speedup_time, cost
end

-- 获取加速信息
function ProduceServlet:get_speedup_info(player_id, produce, product, items, crcy_count)
    log_debug(
        "[ProduceServlet][get_speedup_info] begin player:{} produce:{} product:{} items:{} crcy_count",
        player_id, produce:get_id(), product:get_uuid(), items, crcy_count)
    local cost, speedup_time, code
    local rem_time = product:get_rem_time()
    -- 道具消耗
    if items and next(items) then
        code, speedup_time, cost = self:item_speedup(rem_time, items)
        if code ~= FRAME_SUCCESS then
            return code
        end
        -- 货币消耗
    else
        code, speedup_time, cost = self:crcy_speedup(rem_time, crcy_count)
        if code ~= FRAME_SUCCESS then
            return code
        end
    end
    if speedup_time <= 0 then
        return PRODUCT_SP_INVALID
    end
    speedup_time = mmin(rem_time, speedup_time)
    log_debug(
        "[ProduceServlet][get_speedup_info] end player:{} cost:{} rem_time:{} speedup_time:{} code:{}",
        player_id, cost, rem_time, speedup_time, code)
    return FRAME_SUCCESS, cost, speedup_time
end

-- 生产加速
function ProduceServlet:on_speedup_req(player, player_id, message)
    local id, uuid, crcy_count, items = message.id, message.uuid, message.crcy_count, message.items
    log_debug("[ProduceServlet][on_speedup_req] player:{} produce:{} crcy_count:{} items:{}!", player_id, id, crcy_count,
        items)
    local town = player:get_scene()
    local produce = town:get_produces(id)
    if not produce then
        log_err("[ProduceServlet][on_speedup_req] {} produce:{} not found", player_id, id)
        return FRAME_SUCCESS, { error_code = BUING_NOT_EXIST }
    end

    -- 获取产品
    local product = produce:get_products(uuid)
    if not product then
        log_err("[ProduceServlet][on_speedup_req] {} produce:{} not found", player_id, id)
        return FRAME_SUCCESS, { error_code = PRODUCT_NOT_EXIST }
    end

    -- 工作状态验证
    if not product:is_work() then
        log_err("[ProduceServlet][on_speedup_req] {} produce:{} not work", player_id, id)
        return FRAME_SUCCESS, { error_code = PRODUCT_SP_INVALID }
    end

    local ret_code, cost, sp_time = self:get_speedup_info(player_id, produce, product, items, crcy_count)
    if ret_code ~= FRAME_SUCCESS then
        log_err("[ProduceServlet][on_speedup_req] {} produce:{} speedup_info fail code:{}", player_id, ret_code)
        return FRAME_SUCCESS, { error_code = PRODUCT_SP_INVALID }
    end

    -- 扣除物品(走背包)
    local code = self:on_execute_cost(player_id, cost, COST_SPEEDUP, town)
    if qfailed(code) then
        log_warn("[ProduceServlet][on_speedup_req] player:{} cost failed:{}!", player:get_id(), code)
        return FRAME_SUCCESS, { error_code = code }
    end

    produce:product_speedup(product, sp_time)
    return FRAME_SUCCESS, { error_code = FRAME_SUCCESS }
end

-- 解锁配送
function ProduceServlet:on_undelive_req(player, player_id, message)
    local id = message.id
    log_debug("[ProduceServlet][on_undelive_req] player:{} produce:{}!", player_id, id)

    local town = player:get_scene()
    local produce = town:get_produces(id)
    if not produce then
        log_err("[ProduceServlet][on_undelive_req] {} produce:{} not found", player_id, id)
        return FRAME_SUCCESS, { error_code = BUING_NOT_EXIST }
    end

    local building = town:get_building(id)
    if not building then
        log_err("[ProduceServlet][on_undelive_req] {} building:{} not found", player_id, id)
        return FRAME_SUCCESS, { error_code = BUING_NOT_EXIST }
    end

    -- 检查解锁
    local un_code = produce:check_undelive()
    if un_code ~= FRAME_SUCCESS then
        log_err("[ProduceServlet][on_undelive_req] {} produce:{} un_code:{}", player_id, id, un_code)
        return FRAME_SUCCESS, { error_code = un_code }
    end

    -- 加载消耗的配置
    local cost = load_delivery_costs()
    if not cost or not next(cost) then
        log_err("[ProduceServlet][on_undelive_req] {} produce:{} cost not found", player_id, id)
        return FRAME_SUCCESS, { error_code = FRAME_FAILED }
    end

    -- 扣除物品
    local code = self:on_execute_cost(player_id, cost, COST_DELV, town)
    if qfailed(code) then
        log_warn("[ProduceServlet][on_undelive_req] player:{} cost failed:{}!", player:get_id(), code)
        return FRAME_SUCCESS, { error_code = code }
    end

    produce:unlock_delive()
    produce:sync_changed()
    return FRAME_SUCCESS, { error_code = FRAME_SUCCESS }
end

-- 切换开关
function ProduceServlet:on_swdelive_req(player, player_id, message)
    local id, enable = message.id, message.enable
    log_debug("[ProduceServlet][on_swdelive_req] player:{} produce:{} enable:{}!", player_id, id, enable)

    local town = player:get_scene()
    local produce = town:get_produces(id)
    if not produce then
        log_err("[ProduceServlet][on_swdelive_req] {} produce:{} not found", player_id, id)
        return FRAME_SUCCESS, { error_code = BUING_NOT_EXIST }
    end

    local building = town:get_building(id)
    if not building then
        log_err("[ProduceServlet][on_swdelive_req] {} building:{} not found", player_id, id)
        return FRAME_SUCCESS, { error_code = BUING_NOT_EXIST }
    end

    -- 没有仓库建筑,无法开启
    if enable and not town:check_bank() or 1==1 then
        log_err("[ProduceServlet][on_swdelive_req] {} building:{} not bank", player_id, id)
        return FRAME_SUCCESS, { error_code = BUILD_BANK_NOT_EXIST }
    end

    -- 配送状态验证
    if produce:get_delive_status() == false then
        log_err("[ProduceServlet][on_swdelive_req] {} produce:{} not active", player_id, id)
        return FRAME_SUCCESS, { error_code = PRODUCE_DELV_NOT_ACTIVE }
    end
    produce:switch_delive_enable(enable)
    -- 开启配送时,触发开始生产
    if enable then
        produce:start_produce()
    end
    produce:sync_changed()
    return FRAME_SUCCESS, { error_code = FRAME_SUCCESS }
end

-- 检查采集配方
function ProduceServlet:check_gather_formulas(prod_cap, formulas)
    for id, _ in pairs(formulas) do
        local conf = object_fty:find_formula(id)
        if not conf then
            return FORMULA_NOT_EXIST
        end
    end
    return FRAME_SUCCESS
end

-- 采集
function ProduceServlet:on_gather_req(player, player_id, message)
    local id, formulas = message.id, message.formulas
    local town = player:get_scene()
    log_debug("[ProduceServlet][on_gather_req] player:{} produce:{} formulas:{}!", player_id, id, formulas)
    local produce = town:get_produces(id)
    if not produce then
        log_err("[ProduceServlet][on_gather_req] not produce {} produce:{} not found", player_id, id)
        return FRAME_SUCCESS, { error_code = BUING_NOT_EXIST }
    end

    -- 数据检查
    if not formulas or not next(formulas) then
        produce:clear_product()
        return FRAME_SUCCESS, { error_code = 0 }
    end

    -- 队列检查
    if not produce:check_queue(tkarray(formulas)) then
        log_err("[ProduceServlet][on_gather_req] not check_queue {} produce:{} not found", player_id, id)
        return FRAME_SUCCESS, { error_code = PRODUCT_NOT_RECV }
    end

    -- 配方检查
    if not produce:check_formula(tkarray(formulas)) then
        log_err("[ProduceServlet][on_gather_req] not check_formula {} produce:{} not found", player_id, id)
        return FRAME_SUCCESS, { error_code = FORMULA_NOT_ACTIVE }
    end

    -- 取消生产检查
    if produce:check_gather_clear(formulas) then
        -- 清理生产
        produce:clear_product()
        return FRAME_SUCCESS, { error_code = 0 }
    end

    produce:update_effects()
    local code, confs, cost_sum_cap = produce:build_procap_conf(formulas)
    if code ~= FRAME_SUCCESS then
        log_err("[ProduceServlet][on_gather_req] {} produce:{} code:{}", player_id, id, code)
        return FRAME_SUCCESS, { error_code = code }
    end

    -- 产品完成比
    local done_ratios = produce:pack_done_ratios()
    -- 清理生产
    if produce:get_product_count() > 0 then
        produce:clear_product()
    end

    -- 添加生产
    local ok = produce:add_product(confs, cost_sum_cap, done_ratios)
    if not ok then
        log_err("[ProduceServlet][on_gather_req] add_product false player:{} id:{} formula:{}", player_id, id, formulas)
        return FRAME_SUCCESS, { error_code = FRAME_FAILED }
    end
    return FRAME_SUCCESS, { error_code = 0 }
end

quanta.produce_servlet = ProduceServlet()
return ProduceServlet
