--packet_servlet.lua
local Item              = import("world/packet/item.lua")
local log_err           = logger.err
local log_debug         = logger.debug
local qfailed           = quanta.failed

local dropor            = quanta.get("dropor")
local event_mgr         = quanta.get("event_mgr")
local config_mgr        = quanta.get("config_mgr")
local player_mgr        = quanta.get("player_mgr")
local effect_mgr        = quanta.get("effect_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")


local utility_db        = config_mgr:get_table("utility")
local unlock_db         = config_mgr:init_table("unlock", "id")
local item_db           = config_mgr:get_table("item")

local BAG_CAPACITY      = utility_db:find_number("value", "bag_capacity")
local MAX_CAPACITY      = utility_db:find_number("value", "max_capacity")

local FRAME_SUCCESS     = protobuf_mgr:error_code("FRAME_SUCCESS")
local FRAME_PARAMS      = protobuf_mgr:error_code("FRAME_PARAMS")
local ITEM_NOT_EQUIP    = protobuf_mgr:error_code("PACKET_ITEM_NOT_EQUIP")
local ITEM_NOT_EXIST    = protobuf_mgr:error_code("PACKET_ITEM_NOT_EXIST")
local ITEM_CNOT_DROP    = protobuf_mgr:error_code("PACKET_ITEM_CNOT_DROP")
local ITEM_CNOT_DRAG    = protobuf_mgr:error_code("PACKET_ITEM_CNOT_DRAG")
local CAPACITY_LIMIT    = protobuf_mgr:error_code("PACKET_CAPACITY_LIMIT")
local ITEM_IS_FULL      = protobuf_mgr:error_code("PACKET_ITEM_IS_FULL")
local COST_NOT_ENOUGH   = protobuf_mgr:error_code("PACKET_COST_NOT_ENOUGH")
local TARGET_IS_NULL    = protobuf_mgr:error_code("PACKET_TARGET_IS_NULL")
local PACKET_NOT_EXIST  = protobuf_mgr:error_code("PACKET_PACKET_NOT_EXIST")
local ITEM_CNOT_USE     = protobuf_mgr:error_code("PACKET_ITEM_CNOT_USE")
local IS_IN_CDING       = protobuf_mgr:error_code("PACKET_IS_IN_CDING")
local BANK_IS_FULL      = protobuf_mgr:error_code("PACKET_BANK_IS_FULL")
local COST_USE          = protobuf_mgr:enum("cost_reason", "NID_COST_USE")
local COST_DROP         = protobuf_mgr:enum("cost_reason", "NID_COST_DROP")
local COST_UNLOCK       = protobuf_mgr:enum("cost_reason", "NID_COST_UNLOCK")
local OBTAIN_MONSTER    = protobuf_mgr:enum("obtain_reason", "NID_OBTAIN_MONSTER")
local PACKET_ITEM       = protobuf_mgr:enum("packet_type", "PACKET_ITEM")
local PACKET_EQUIP      = protobuf_mgr:enum("packet_type", "PACKET_EQUIP")

-- 背包服务
local PacketServlet    = singleton()
function PacketServlet:__init()
    self:setup()
end

function PacketServlet:setup()
    --注册消息
    event_mgr:add_trigger(self, "on_item_add")
    event_mgr:add_trigger(self, "on_item_cost")
    event_mgr:add_trigger(self, "on_login_success")
    -- CS协议
    protobuf_mgr:register(self, "NID_PACKET_ITEM_USE_REQ", "on_item_use_req")
    protobuf_mgr:register(self, "NID_PACKET_ITEM_DROP_REQ", "on_item_drop_req")
    protobuf_mgr:register(self, "NID_PACKET_ITEM_MOVE_REQ", "on_item_move_req")
    protobuf_mgr:register(self, "NID_PACKET_EQUIP_SHOW_REQ", "on_equip_show_req")
    protobuf_mgr:register(self, "NID_PACKET_GRID_UNLOCK_REQ", "on_grid_unlock_req")
    protobuf_mgr:register(self, "NID_PACKET_EQUIP_INSTALL_REQ", "on_equip_install_req")
    protobuf_mgr:register(self, "NID_PACKET_EQUIP_UNINSTALL_REQ", "on_equip_uninstall_req")
    protobuf_mgr:register(self, "NID_PACKET_ITEM_FILL_REQ", "on_item_fill_req")
end

-- 登陆成功
function PacketServlet:on_login_success(player_id, player)
    player:sync_packets()
    --监听离线消息
    player:watch_event(self, "on_monster_killed")
end

--道具增加
function PacketServlet:on_item_add(player, item_id, num, reason)
    player:notify_event("on_item_changed", item_id, num)
end

--道具扣除
function PacketServlet:on_item_cost(player, item_id, num, reason)
    player:notify_event("on_item_changed", item_id, -num)
end

function PacketServlet:on_monster_killed(attacker_id, monster_id, drop_id)
    -- 掉落
    local player = player_mgr:get_entity(attacker_id)
    if player then
        player:notify_event("on_kill_monster", monster_id)
    end

    local drop_items = {}
    if not dropor:execute(drop_id, drop_items) then
        log_err("[PacketServlet][on_monster_killed] player({}) kill monster({}) drop failed, drop_id={}", attacker_id, monster_id, drop_id)
        return
    end
    local drops = { drop = drop_items, reason = OBTAIN_MONSTER }
    if player then
        local alloter, allot_code = player:format_drops(player, drops)
        if allot_code then
            log_err("[PacketServlet][on_monster_killed] falied: player_id: {}, allot_code: {}", attacker_id, allot_code)
            player:send("NID_POPUP_NTF", {error_code = allot_code})
            return
        end
        if alloter then
            alloter:allot(drops.reason)
        end
    end
end

--CS协议
-----------------------------------------------------
-- 使用物品
function PacketServlet:on_item_use_req(player, player_id, message)
    local item_id, count, target_id, packet_id = message.item_id, message.count, message.target_id, message.packet_id
    log_debug("[PacketServlet][on_item_use_req] player_id:{}, item_id:{}, count:{}", player_id, item_id, count)
    local has_count = player:get_item(item_id, packet_id)
    if has_count < count then
        log_err("[PacketServlet][on_item_use_req] {} packet_id {} use item {} not exits!", player_id, packet_id, item_id)
        return ITEM_NOT_EXIST
    end
    --道具参数是否合法
    if not self:check_item_conf(item_id) then
        return FRAME_PARAMS
    end
    --查询道具使用效果ID
    local item = Item(item_id, has_count)
    local effect_id = item:get_effect_id()
    if not effect_id then
        log_err("[PacketServlet][on_item_use_req] {} use item {} not effect!", player_id, item_id)
        return ITEM_CNOT_USE
    end
    local cd_group, cd_time = item:get_cdtime()
    if not player:check_cding(cd_group) then
        log_err("[PacketServlet][on_item_use_req] {} use item {} iscding!", player_id, item_id)
        return IS_IN_CDING
    end
    local rcount = item:get_usecount(count)
    if rcount == 0 then
        log_err("[PacketServlet][on_item_use_req] {} use item {} not enough!", player_id, item_id)
        return COST_NOT_ENOUGH
    end
    local target = (target_id == 0) and player or player_mgr:get_entity(target_id)
    if not target then
        log_err("[PacketServlet][on_item_use_req] {} use item target {} is nil!", player_id, target_id)
        return TARGET_IS_NULL
    end
    local code = effect_mgr:execute(player, effect_id, rcount, target)
    if qfailed(code) then
        log_err("[PacketServlet][on_item_use_req] {} use item {} effect {} execute failed!", player_id, item_id, effect_id)
        return code
    end
    player:begin_cding(cd_group, cd_time)
    player:drop_item(item_id, rcount, COST_USE, packet_id)
    return FRAME_SUCCESS, { error_code = 0 }
end

-- 道具丢弃
function PacketServlet:on_item_drop_req(player, player_id, message)
    local item_ids, packet_id = message.item_ids, message.packet_id
    log_debug("[PacketServlet][on_item_drop_req] player_id:{}, item_ids:{} packet_id:{}", player_id, item_ids, packet_id)
    local drop_items = {}
    for _, item_id in pairs(item_ids) do
        local has_count = player:get_item(item_id, packet_id)
        if has_count <= 0 then
            log_err("[PacketServlet][on_item_drop_req] {} drop item {} not exits!", player_id, item_id)
            return ITEM_NOT_EXIST
        end
        local item = Item(item_id, has_count)
        if not item:is_dropable() then
            log_err("[PacketServlet][on_item_drop_req] {} drop item {} cont drop!", player_id, item_id)
            return ITEM_CNOT_DROP
        end
        drop_items[item_id] = has_count
    end
    player:drop_items(drop_items, COST_DROP, packet_id)
    return FRAME_SUCCESS, { error_code = 0 }
end

-- 道具移动
function PacketServlet:on_item_move_req(player, player_id, message)
    local item_id, tar_packet_id,src_packet_id, count = message.item_id, message.tar_packet_id, message.src_packet_id, message.count
    log_debug("[PacketServlet][on_item_move_req] player_id:{}, item_id:{}, src_packet:{}, tar_packet:{}, count:{}",
                player_id, item_id, src_packet_id, tar_packet_id, count)

    if src_packet_id == tar_packet_id then
        return FRAME_PARAMS
    end

    local tar_packet, src_packet = player:get_packet(tar_packet_id), player:get_packet(src_packet_id)
    if not tar_packet or not src_packet then
        return FRAME_PARAMS
    end
    if tar_packet:is_equip_bag() then
        log_err("[PacketServlet][on_item_move_req] {} move item {} target packet is equip!", player_id, item_id)
        return FRAME_PARAMS
    end
    local has_count = player:get_item(item_id, src_packet_id)
    if has_count <= 0 then
        log_err("[PacketServlet][on_item_move_req] {} move item {} not exits!", player_id, item_id)
        return ITEM_NOT_EXIST
    end

    local tar_item = Item(item_id, tar_packet:get_item_num(item_id))
    if not tar_item:is_overlie() then
        log_err("[PacketServlet][on_item_move_req] {} move item {} not exits!", player_id, item_id)
        return tar_packet_id == PACKET_ITEM and ITEM_IS_FULL or BANK_IS_FULL
    end

    local src_item = Item(item_id, has_count)
    if not player:swap_item(src_item, tar_packet, src_packet, count) then
        log_err("[PacketServlet][on_item_move_req] {} switch item {} failed!", player_id, item_id)
        return ITEM_CNOT_DRAG
    end
    log_debug("[PacketServlet][on_item_move_req] player({}) success!", player_id)
    return FRAME_SUCCESS, { error_code = 0 }
end

function PacketServlet:on_grid_unlock_req(player, player_id, message)
    local packet_id, count = message.packet_id, message.count
    log_debug("[PacketServlet][on_grid_unlock_req] player_id:{}, packet_id:{}, count:{}", player_id, packet_id, count)
    local packet = player:get_packet(packet_id)
    if not packet then
        return PACKET_NOT_EXIST
    end
    local capacity = packet:get_capacity()
    local now_capacity = capacity + count
    if count < 1 or now_capacity > MAX_CAPACITY then
        return CAPACITY_LIMIT
    end
    local cost_items = {}
    for i = 1, count do
        local costs = unlock_db:find_value("costs", i + capacity - BAG_CAPACITY)
        for item_id, num in pairs(costs or {}) do
            local old_num = cost_items[item_id] or 0
            cost_items[item_id] = old_num + num
        end
    end
    if not player:cost_items(cost_items, COST_UNLOCK) then
        return COST_NOT_ENOUGH
    end
    player:update_capacity(packet, packet_id, now_capacity)
    event_mgr:notify_trigger("on_unlock_package", player, count, now_capacity)
    return FRAME_SUCCESS, { error_code = 0 }
end

--装备下装
function PacketServlet:on_equip_uninstall_req(player, player_id, message)
    local item_id = message.item_id
    log_debug("[PacketServlet][on_equip_uninstall_req] player_id:{}, item_id:{}", player_id, item_id)
    local has_count = player:get_item(item_id, PACKET_EQUIP)
    if has_count <= 0 then
        return ITEM_NOT_EXIST
    end
    local equip = Item(item_id, has_count)
    if not equip:is_equip() then
        return ITEM_NOT_EQUIP
    end
    if not player:uninstall(equip) then
        return ITEM_IS_FULL
    end
    return FRAME_SUCCESS, { error_code = 0 }
end

--装备上装
function PacketServlet:on_equip_install_req(player, player_id, message)
    local item_id = message.item_id
    log_debug("[PacketServlet][on_equip_install_req] player_id:{}, item_id:{}", player_id, item_id)
    local packet = player:get_packet(PACKET_ITEM)
    local has_count = packet:get_item_num(item_id)
    if has_count <= 0 then
        return ITEM_NOT_EXIST
    end
    local equip = Item(item_id, has_count)
    if not equip:is_equip() then
        return ITEM_NOT_EQUIP
    end
    --目标位置上是否已有装备
    local old_equip = player:has_old_equip(equip)
    if old_equip then
        if not player:uninstall(old_equip) then
            return ITEM_IS_FULL
        end
    end
    if not player:install(equip) then
        return ITEM_IS_FULL
    end
    -- 清理红点
    local rids = equip:get_rid()
    for _,value in pairs(rids or {}) do
        --TODO_FIX 红点系统接入后开启player:read_reddot_in_server(value, equip.item_id)
    end
    player:notify_event("on_install_equip", item_id)
    return FRAME_SUCCESS, { error_code = 0 }
end

--装备显示类别
function PacketServlet:on_equip_show_req(player, player_id, message)
    local suitable = message.suitable
    log_debug("[PacketServlet][on_equip_show_req] player_id:{}, suitable:{}", player_id, suitable)
    player:set_suitable(suitable and 1 or 0)
    player:update_suitable()
    return FRAME_SUCCESS, { error_code = 0 }
end

-- 道具填充
function PacketServlet:on_item_fill_req(player, player_id, message)
    local src_packet_id, tar_packet_id = message.src_packet_id, message.tar_packet_id
    log_debug("[PacketServlet][on_item_fill_req] player_id:{}, src_packet_id:{}  tar_packet_id:{}", player_id,
        src_packet_id, tar_packet_id)
    local tar_packet = player:get_packet(tar_packet_id)
    if not tar_packet then
        return FRAME_PARAMS
    end
    local src_packet = player:get_packet(src_packet_id)
    if not src_packet then
        return FRAME_PARAMS
    end
    --自动填充储物箱
    player:fill_packet(src_packet, tar_packet)
    return FRAME_SUCCESS, { error_code = 0 }
end

--检查道具是否合法
function PacketServlet:check_item_conf(item_id)
    local item_conf = item_db:find_one(item_id)
    if not item_conf then
        log_err("[PacketServlet][check_item_conf] item_id {} not found conf", item_id)
        return false
    end
    return true
end

quanta.packet_servlet = PacketServlet()

return PacketServlet
