--skill_servlet.lua
import("business/item/dropor.lua")

local log_err           = logger.err
local log_debug         = logger.debug
local qfailed           = quanta.failed
local tinsert           = table.insert
local mfloor            = math.floor

local dropor            = quanta.get("dropor")
local event_mgr         = quanta.get("event_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")
local config_mgr        = quanta.get("config_mgr")
local home_db           = config_mgr:init_table("home", "id")
local scene_eff_db      = config_mgr:init_table("scene_effect", "id")

local ACTION_MELEE      = quanta.enum("ActionType", "Melee")
local CURE_START        = quanta.enum("DyingCureType", "CURE_START")
local CURE_ABORT        = quanta.enum("DyingCureType", "CURE_ABORT")
local CURE_TRANS        = quanta.enum("DyingCureType", "CURE_TRANS")
local ST_DEFAULT        = quanta.enum("SkillType", "Default")

local ENT_EFFECT        = protobuf_mgr:enum("entity_type", "ENTITY_EFFECT")
local FRAME_FAILED      = protobuf_mgr:error_code("FRAME_FAILED")
local FRAME_PARAMS      = protobuf_mgr:error_code("FRAME_PARAMS")
local FRAME_SUCCESS     = protobuf_mgr:error_code("FRAME_SUCCESS")
local GATHER_EMPTY      = protobuf_mgr:error_code("SKILL_GATHER_EMPTY")
local SKILL_IN_CDING    = protobuf_mgr:error_code("SKILL_IS_IN_CDING")
local SKILL_IN_LOADING  = protobuf_mgr:error_code("SKILL_IS_IN_LOADING")
local CANT_BREAK_IN     = protobuf_mgr:error_code("SKILL_CANT_BREAK_IN")
local ITEM_DROP_ERR     = protobuf_mgr:error_code("PACKET_ITEM_DROP_ERR")
local DISTANCE_LIMIT    = protobuf_mgr:error_code("SKILL_DISTANCE_LIMIT")

local SkillServlet = singleton()
function SkillServlet:__init()
    -- 消息监听
    event_mgr:add_trigger(self, "on_skill_start_use")
    event_mgr:add_trigger(self, "on_skill_target_check")
    event_mgr:add_trigger(self, "on_skill_completed")
    event_mgr:add_trigger(self, "on_skill_available")
    -- CS协议
    protobuf_mgr:register(self, "NID_SKILL_GATHER_REQ", "on_gather_req")
    protobuf_mgr:register(self, "NID_SKILL_STOP_LOADING_REQ", "on_stop_loading_req")
    protobuf_mgr:register(self, "NID_ACTOR_TREACT_REQ", "on_actor_treact_req")
    protobuf_mgr:register(self, "NID_SKILL_CREATE_GATHER_REQ", "on_create_gather_req")
    protobuf_mgr:register(self, "NID_SKILL_ENTER_EFFECT_REQ", "on_enter_effect_req")
    protobuf_mgr:register(self, "NID_SKILL_EXIT_EFFECT_REQ", "on_exit_effect_req")
end

--事件监听
-------------------------------------------------------------------------
function SkillServlet:on_gather_loading(player, resource, args, time)
    local uuid = resource:get_id()
    local player_id = player:get_id()
    local gather = resource:check_loading(args)
    if not gather then
        log_err("[SkillServlet][on_gather_loading] player({}) gather args({}) loading failed!", player_id, args)
        player:send("NID_SKILL_STOP_LOADING_NTF", { uuid = uuid, error_code = GATHER_EMPTY })
        return
    end
    local code_res, result = resource:check_gather(gather)
    if qfailed(code_res) then
        log_err("[SkillServlet][on_gather_loading] player({}) gather res({}) failed!", player_id, uuid)
        player:send("NID_SKILL_STOP_LOADING_NTF", { uuid = uuid, error_code = code_res })
        return code_res
    end
    local code = self:execute_cost_drop(player_id, resource, result)
    if qfailed(code) then
        player:send("NID_SKILL_STOP_LOADING_NTF", { uuid = uuid, error_code = code })
        return
    end
    if resource:is_multi(gather) then
        resource:multi_drop_result(gather, result)
    else
        local drop_code = resource:drop_result(player, result, gather)
        if drop_code then
            player:send("NID_POPUP_NTF", {error_code = drop_code})
            player:send("NID_SKILL_STOP_LOADING_NTF", { uuid = uuid, error_code = drop_code })
            return drop_code
        end
    end
    player:send("NID_SKILL_STOP_LOADING_NTF", { uuid = uuid, error_code = FRAME_SUCCESS })
    event_mgr:notify_trigger("on_resource_gather", player, resource, result.drop, gather, time)
    log_debug("[SkillServlet][on_gather_loading] player({}) gather {} success, result : {}!", player_id, uuid, result)
end

--消息监听
-------------------------------------------------------------------------
function SkillServlet:on_skill_start_use(skill, check_index)
    local entity = skill.owner_entity
    log_debug('[SkillServlet][on_skill_start_use] entity_id: {}, skill_proto_id: {}, skill_id: {}', entity:get_id(), skill.skill_id, skill.id)
    local target_event_id = skill:get_check_target_param(check_index, "data", "target_event_id")
    if target_event_id == nil then
        return
    end
    -- melee 数据填充
    local action_melee = {
        timeline = 0,
        skill_use = {
            skill_id = skill.skill_id,
            targets = {},
            equip_attr = 0,
            target_event_id = target_event_id,
            skill_type = ST_DEFAULT,
        },
    }
    local result = entity:on_action_melee(action_melee)
    if result.error_code ~= FRAME_SUCCESS then
        return
    end
    local pos = {
        x = entity:get_pos_x(),
        y = entity:get_pos_y(),
        z = entity:get_pos_z(),
    }
    local dir_y = entity:get_dir_y()
    local actions = {
        {
            start_time = entity:live_time(),
            start_pos = pos,
            -- elapsed_time = 0,
            -- stop_time = 0,
            melee = action_melee,
            type = ACTION_MELEE,
            start_dir = dir_y,
        }
    }
    local results = {
        {
            type = ACTION_MELEE,
            success = true,
            melee_result = result.melee_result,
        }
    }
    entity:action_broadcast(pos, dir_y, actions, results, nil, false)
end

---技能释放目标检测
---@param skill Skill 技能
function SkillServlet:on_skill_target_check(skill, check_index)
    ---@type BattleComponent|SkillComponent
    local entity = skill.owner_entity
    log_debug('[SkillServlet][on_skill_target_check] entity_id: {}, skill_proto_id: {}, skill_id: {}', entity:get_id(), skill.skill_id, skill.id)
    -- 判断技能释放区域内是否有攻击目标
    ---@type Scene
    local scene = entity:get_scene()
    if not scene then
        skill:terminate()
        return false
    end
    local over_time = skill:get_check_target_param(check_index, "time")
    if not over_time then
        return false
    end
    -- melee 数据填充
    local action_melee = {
        timeline = mfloor(over_time * 1000),
        skill_use = {
            skill_id = skill.skill_id,
            targets = {},
            equip_attr = 0,
            target_event_id = skill:get_check_target_param(check_index, "data", "target_event_id"),
            skill_type = ST_DEFAULT,
        },
    }
    -- 攻击目标筛选
    for watcher_id, _ in pairs(entity.watchers) do
        local watcher = scene:get_entity(watcher_id)
        -- 判断阵营、判断是否在技能释放区域内
        if watcher and not entity:faction_is_friendly(watcher) and skill:check_target(watcher, check_index) then
            tinsert(action_melee.skill_use.targets, watcher_id)
        end
    end
    -- 攻击伤害计算
    local result = entity:on_action_melee(action_melee)
    if result.error_code ~= FRAME_SUCCESS then
        skill:check_next_target_param(check_index + 1)
        return false
    end
    -- 消息下发
    local pos = {
        x = entity:get_pos_x(),
        y = entity:get_pos_y(),
        z = entity:get_pos_z(),
    }
    local dir_y = entity:get_dir_y()
    local actions = {
        {
            start_time = entity:live_time(),
            start_pos = pos,
            -- elapsed_time = 0, -- melee 不需要
            -- stop_time = 0, -- melee 不需要
            melee = action_melee,
            type = ACTION_MELEE,
            start_dir = dir_y,
        }
    }
    local results = {
        {
            type = ACTION_MELEE,
            success = true,
            melee_result = result.melee_result,
        }
    }
    entity:action_broadcast(pos, dir_y, actions, results, scene, false)
    skill:check_next_target_param(check_index + 1)
    return true
end

---技能释放完成
---@param skill Skill 技能
function SkillServlet:on_skill_completed(skill)
    local entity = skill.owner_entity
    log_debug('[SkillServlet][on_skill_completed] entity_id: {}, skill_proto_id: {}, skill_id: {}', entity:get_id(), skill.skill_id, skill.id)
    entity:completed_use_skill(skill.equip_attr, skill.skill_id, skill.skill_type)
end

function SkillServlet:on_skill_available(skill)
    local entity = skill.owner_entity
    log_debug('[SkillServlet][on_skill_available] entity_id: {}, skill_proto_id: {}, skill_id: {}', entity:get_id(), skill.skill_id, skill.id)
    entity:available_use_skill(skill.equip_attr, skill.skill_id, skill.skill_type)
end
--CS协议
-------------------------------------------------------------------------
function SkillServlet:on_gather_req(player, player_id, message)
    local uuid, step = message.uuid, message.step
    log_debug("[SkillServlet][on_gather_req] player({}) gather resource({}-{})!", player_id, uuid, step)
    local scene = player:get_scene()
    local resource = scene:find_entity(uuid)
    if not resource then
        log_err("[SkillServlet][on_gather_req] player({}) gather {} not exist!", player_id, uuid)
        return FRAME_PARAMS
    end

    if not player:check_cding() then
        return SKILL_IN_CDING
    end
    local gather = resource:find_gather(step)
    if not gather then
        log_err("[SkillServlet][on_gather_req] player({}) gather {} step {} not exist!", player_id, uuid, step)
        return FRAME_PARAMS
    end
    local code_equip = resource:check_equip(player, gather)
    if qfailed(code_equip) then
        log_err("[SkillServlet][on_gather_req] player({}) gather {} equip error!", player_id, uuid)
        return code_equip
    end
    --检查距离
    if not player:check_distance(resource:get_pos_x(), resource:get_pos_z(), resource:get_radius()) then
        log_err("[SkillServlet][on_gather_req] player({}) gather {} distance error!", player_id, uuid)
        return FRAME_SUCCESS, { error_code = DISTANCE_LIMIT}
    end
    local loading_time, breakin, args = resource:load_loading(gather, step)
    if loading_time then
        local function callback()
            self:on_gather_loading(player, resource, args, loading_time)
        end
        if not player:begin_loading(loading_time, breakin, callback) then
            return SKILL_IN_LOADING
        end
        player:begin_cding()
        player:broadcast_message("NID_SKILL_GATHER_NTF", { id = player_id, uuid = uuid, step = step })
        return FRAME_SUCCESS, { error_code = 0}
    end
    local code_res, result = resource:check_gather(gather)
    if qfailed(code_res) then
        log_err("[SkillServlet][on_gather_req] player({}) gather {} failed!", player_id, uuid)
        return code_res
    end
    local code = self:execute_cost_drop(player_id, resource, result)
    if qfailed(code) then
        return code
    end
    if resource:is_multi(gather) then
        resource:multi_drop_result(gather, result)
    else
        local drop_code = resource:drop_result(player, result, gather)
        if drop_code then
            return drop_code
        end
    end
    player:begin_cding()
    player:broadcast_message("NID_SKILL_GATHER_NTF", { id = player_id, uuid = uuid, step = step })
    event_mgr:notify_trigger("on_resource_gather", player, resource, result.drop, gather, 0)
    log_debug("[SkillServlet][on_gather_req] player({}) gather {} success, result : {}!", player_id, uuid, result)
    return FRAME_SUCCESS, { error_code = 0}
end

--结束技能CD
function SkillServlet:on_stop_loading_req(player, player_id, message)
    log_debug("[SkillServlet][on_stop_loading_req] player({}) stop skillcd!", player_id)
    if not player:stop_loading(true) then
        return CANT_BREAK_IN
    end
    return FRAME_SUCCESS, { error_code = 0}
end

--内部方法
----------------------------------------------
--生成掉落
function SkillServlet:check_drop(drop)
    local drop_items = {}
    for _, drop_id in pairs(drop.drop) do
        if not dropor:execute(drop_id, drop_items) then
            log_err("[SkillServlet][check_drop] drop_id ({}) failed!", drop_id)
            return ITEM_DROP_ERR
        end
    end
    drop.drop = drop_items
end

--检查掉落和消耗
function SkillServlet:execute_cost_drop(player_id, resource, result)
    local drop, cost = result.drop, result.cost
    if drop or cost then
        if drop then
           local code_drop = self:check_drop(drop)
           if code_drop then
            return code_drop
           end
        end
    end
    resource:execute_gather(result, player_id)
    return FRAME_SUCCESS
end

function SkillServlet:on_actor_treact_req(player, player_id, message)
    log_debug(
        "[SkillServlet][on_actor_treact_req] player({}): rescuer_id={}, saved_id={}, type={}",
        player_id, message.rescuer_id, message.saved_id, message.type
    )
    local scene = player:get_scene()
    if scene == nil then
        return FRAME_SUCCESS, { error_code = FRAME_SUCCESS }
    end
    local response = { error_code = FRAME_SUCCESS }
    if message.type == CURE_START then
        -- TODO: 1.重伤救治请求-返回结果-推送状态
        player:on_actor_treat_start(scene, message.rescuer_id, message.saved_id, response)
    elseif message.type == CURE_ABORT then
        -- TODO: 2.重伤救治中止-返回结果-推送状态
        player:on_actor_treat_abort(scene, message.rescuer_id, message.saved_id, response)
    elseif message.type == CURE_TRANS then
        -- 重伤救治-立即传送
        player:on_actor_treat_trans(scene, message.rescuer_id, message.saved_id, response)
    else
        response.error_code = FRAME_FAILED
    end
    return FRAME_SUCCESS, response
end

--创建次数限制的采集物
function SkillServlet:on_create_gather_req(player, player_id, message)
    log_debug("[SkillServlet][on_create_gather_req] player({}): message={}", player_id, message)
    local town = player:get_scene()
    if town == nil then
        return FRAME_SUCCESS, { error_code = FRAME_SUCCESS }
    end
    local proto_id = message.proto_id
    local conf = home_db:find_one(proto_id)
    if not conf then
        return FRAME_SUCCESS, { error_code = FRAME_PARAMS }
    end
    local entity = town:create_snap_entity(conf, true)
    if not entity then
        return FRAME_SUCCESS, { error_code = FRAME_PARAMS }
    end
    return FRAME_SUCCESS, {error_code = 0}
end

--进入/交互场景效果物体
function SkillServlet:on_enter_effect_req(player, player_id, message)
    log_debug("[SkillServlet][on_enter_effect_req] player({}): message={}", player_id, message)
    local scene = player:get_scene()
    if scene == nil then
        return FRAME_SUCCESS, { error_code = FRAME_SUCCESS }
    end
    local uuid = message.id
    local error_code = self:check_effect_entity(player, scene:get_name(), uuid)
    if error_code ~= FRAME_SUCCESS then
        return error_code
    end
    --查找或创建实体
    local effect_entity = scene:find_entity(uuid)
    if not effect_entity then
        log_err("[SkillServlet][on_enter_effect_req] player({}) entity_id {} not exist!", player_id, uuid)
        return FRAME_PARAMS
    end
    --进入
    effect_entity:enter(player)
    return FRAME_SUCCESS, { error_code = 0 }
end

--退出场景效果物体
function SkillServlet:on_exit_effect_req(player, player_id, message)
    log_debug("[SkillServlet][on_exit_effect_req] player({}): message={}", player_id, message)
    local scene = player:get_scene()
    if scene == nil then
        return FRAME_FAILED
    end
    local uuid = message.id
    local error_code = self:check_effect_entity(player, scene:get_name(), uuid)
    if error_code ~= FRAME_SUCCESS then
        return error_code
    end
    --查找或创建实体
    local scene_effect = scene:find_entity(uuid)
    if not scene_effect then
        log_err("[SkillServlet][on_exit_effect_req] player({}) scene_effect.id {} not exist!", player_id, uuid)
        return FRAME_PARAMS
    end
    --退出
    scene_effect:exit(player)
    return FRAME_SUCCESS, {error_code = 0}
end

--buff物体检查
function SkillServlet:check_effect_entity(player, scene_name, uuid)
    local player_id = player:get_id()
    --判定type
    local entity_no = uuid & 0xFFFFF
    local map_db = config_mgr:get_table(scene_name, "id")
    local conf = map_db:find_one(entity_no)
    if not conf then
        log_err("[SkillServlet][check_effect_entity] player({}) uuid {} not found!", player_id, uuid)
        return FRAME_PARAMS
    end
    --非buf实体
    if ENT_EFFECT ~= conf.type then
        log_err("[SkillServlet][check_effect_entity] player({}) uuid {} not buff_entity!", player_id, uuid)
        return FRAME_PARAMS
    end

    --获取触发buff配置
    local scene_eff_conf = scene_eff_db:find_one(conf.proto_id)
    if not scene_eff_conf then
        log_err("[SkillServlet][check_effect_entity] player({}) uuid {} not found scene_eff_conf:{}!",
            player_id, uuid, conf.proto_id)
        return FRAME_PARAMS
    end
    return FRAME_SUCCESS
end

quanta.skill_servlet = SkillServlet()

return SkillServlet

