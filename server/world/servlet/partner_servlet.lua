--npc_servlet.lua
local log_debug           = logger.debug
local qfailed             = quanta.failed
local log_err             = logger.err

local protobuf_mgr        = quanta.get("protobuf_mgr")
local player_mgr          = quanta.get("player_mgr")
local event_mgr           = quanta.get("event_mgr")
local recruit_mgr         = quanta.get("recruit_mgr")
local config_mgr          = quanta.get("config_mgr")
local item_mgr            = quanta.get("item_mgr")
local item_db             = config_mgr:init_table("item", "id")
local nf_skill_db         = config_mgr:init_table("nf_skill", "id")
local sat_db              = config_mgr:init_table("town_sat", "sat_lv")
local utility_db          = config_mgr:get_table("utility")
local NPC_TALK_MAX        = utility_db:find_integer("value", "npc_talk_max")

local STATE_IDLE          = protobuf_mgr:enum("partner_state", "STATE_IDLE")
local STATE_SEND          = protobuf_mgr:enum("partner_state", "STATE_SEND")
local STATE_WORK          = protobuf_mgr:enum("partner_state", "STATE_WORK")
local NID_COST_GIFT       = protobuf_mgr:enum("cost_reason", "NID_COST_GIFT")
local NID_COST_STAR       = protobuf_mgr:enum("cost_reason", "NID_COST_STAR")
local NID_COST_UNLV       = protobuf_mgr:enum("cost_reason", "NID_COST_UNLV")
local COST_NPC_EXP        = protobuf_mgr:enum("cost_reason", "NID_COST_NPC_EXP")
local OBTAIN_NPC          = protobuf_mgr:enum("obtain_reason", "NID_OBTAIN_NPC")
local TARGET_BUILDING     = quanta.enum("NfSkillTarget", "BUILDING")

local FRAME_SUCCESS       = protobuf_mgr:error_code("FRAME_SUCCESS")
local FRAME_PARAMS        = protobuf_mgr:error_code("FRAME_PARAMS")
local BUING_NOT_EXIST     = protobuf_mgr:error_code("WORKS_BUING_NOT_EXIST")
local PARTNER_NOT_FOUND   = protobuf_mgr:error_code("PARTNER_NOT_FOUND")
local PARTNER_STATE_ERR   = protobuf_mgr:error_code("PARTNER_STATE_ERR")
local PARTNER_STATE_BUSY  = protobuf_mgr:error_code("PARTNER_STATE_BUSY")
local ROLE_NOT_EXIST      = protobuf_mgr:error_code("LOGIN_ROLE_NOT_EXIST")
local PARTNER_GIFT_LIMIT  = protobuf_mgr:error_code("PARTNER_GIFT_LIMIT")
local CANT_DRIVE          = protobuf_mgr:error_code("PARTNER_CANT_DRIVE")
local ITEM_NOT_GIFT       = protobuf_mgr:error_code("PACKET_ITEM_NOT_GIFT")
local PARTNER_WORK_RECALL = protobuf_mgr:error_code("PARTNER_WORK_RECALL")
local PARTNER_WORK_RPLC   = protobuf_mgr:error_code("PARTNER_WORK_RPLC")
local COST_NOT_ENOUGH     = protobuf_mgr:error_code("PACKET_COST_NOT_ENOUGH")
local PARTNER_UNLIMIT     = protobuf_mgr:error_code("PARTNER_UNLIMIT")
local PARTNER_TOWN_LV_ERR = protobuf_mgr:error_code("PARTNER_TOWN_LV_ERR")

local PartnerServlet      = singleton()
function PartnerServlet:__init()
    event_mgr:add_listener(self, "rpc_affinity_change")
    event_mgr:add_listener(self, "rpc_obtain_npc")

    --消息监听
    event_mgr:add_trigger(self, "on_house_takeback")      --民居销毁
    event_mgr:add_trigger(self, "on_send_partner")        --派驻
    event_mgr:add_trigger(self, "on_recall_partner")      --召回
    event_mgr:add_trigger(self, "on_partner_attr_before") --伙伴属性变化前
    event_mgr:add_trigger(self, "on_partner_attr_after")  --伙伴属性变化后
    event_mgr:add_trigger(self, "on_satlv_chg_after")     --满意度变化后
    event_mgr:add_trigger(self, "on_satlv_chg_before")    --满意度变化前
    -- CS协议
    protobuf_mgr:register(self, "NID_PARTNER_SEND_REQ", "on_send_req")
    protobuf_mgr:register(self, "NID_PARTNER_RECALL_REQ", "on_recall_req")
    protobuf_mgr:register(self, "NID_PARTNER_GIFT_REQ", "on_gift_req")
    protobuf_mgr:register(self, "NID_PARTNER_DRIVE_REQ", "on_drive_req")
    protobuf_mgr:register(self, "NID_PARTNER_UP_STAR_REQ", "on_up_star_req")
    protobuf_mgr:register(self, "NID_PARTNER_UP_LV_REQ", "on_up_lv_req")
    protobuf_mgr:register(self, "NID_PARTNER_UN_LV_REQ", "on_unlv_req")
    protobuf_mgr:register(self, "NID_PARTNER_REDDOT_REQ", "on_reddot_req")
    protobuf_mgr:register(self, "NID_PARTNER_DIALOG_REQ", "on_dialog_req")
end

--满意度等级变化前
function PartnerServlet:on_satlv_chg_before(player, lv)
    log_debug("[PartnerServlet][on_satlv_chg_before] player_id:{} lv:{}", player:get_id(), lv)
    --获取效果列表
    for _, partner in pairs(player:get_partners() or {}) do
        --卸载npc属性
        event_mgr:notify_trigger("on_partner_attr_before", partner)
        --卸载效果
        partner:unload_sat_effect()
    end
end

--满意度等级变化后
function PartnerServlet:on_satlv_chg_after(player, lv)
    log_debug("[PartnerServlet][on_satlv_chg_after] player_id:{} lv:{}", player:get_id(), lv)
    --获取效果列表
    local effects = sat_db:find_one(lv).effect_ids
    for _, partner in pairs(player:get_partners() or {}) do
        --加载效果
        partner:load_sat_effect(effects)
        --重载npc数据
        event_mgr:notify_trigger("on_partner_attr_after", partner)
    end
    event_mgr:notify_trigger("on_satlv_chg_done", player)
end

--伙伴属性变化前
function PartnerServlet:on_partner_attr_before(partner)
    log_debug("[PartnerServlet][on_partner_attr_before] partner_id:{}", partner:get_id())

    local player = partner:get_player()
    --派驻中
    local building_id = partner:get_building_id()
    if building_id and building_id ~= 0 then
        local building = player:get_scene():get_building(building_id)
        --非产出类型建筑
        local product_type = building:get_prototype().product_type or 0
        if product_type == 0 then
            return
        end
        partner:unload_nf_skill()
        --npc属性效果
        building:unload_partner_attr(partner)
        --技能效果
        building:remove_partner_buff(partner:get_id())
    end
end

--伙伴属性变化后
function PartnerServlet:on_partner_attr_after(partner)
    log_debug("[PartnerServlet][on_partner_attr_after] partner_id:{}", partner:get_id())

    local player = partner:get_player()
    --派驻中
    local building_id = partner:get_building_id()
    if building_id and building_id ~= 0 then
        local building = player:get_scene():get_building(building_id)
        --非产出类型建筑
        local product_type = building:get_prototype().product_type or 0
        if product_type == 0 then
            return
        end

        partner:load_nf_skill()
        --npc属性效果
        building:load_partner_attr(partner)
        --npc技能效果
        local star_conf = partner:get_star_conf()
        if not star_conf or not star_conf.nf_skills then
            log_debug("[PartnerServlet][on_partner_attr_after] player_id:{} partner_id:{} dont have skill",
                player:get_id(), partner:get_id())
            return
        end
        for _, nf_skill_id in pairs(star_conf.nf_skills or {}) do
            local skill_conf = nf_skill_db:find_one(nf_skill_id)
            if not skill_conf or not skill_conf.skill_effect then
                goto continue
            end
            --判断作用目标
            if skill_conf.target ~= TARGET_BUILDING then
                goto continue
            end
            for _, buff_id in pairs(skill_conf.skill_effect) do
                building:add_partner_buff(partner:get_id(), buff_id)
            end
            ::continue::
        end
    end
end

--npc派驻
function PartnerServlet:on_send_partner(player_id, partner, building)
    log_debug("[PartnerServlet][on_send_partner] player_id:{} partner_id:{}, building_id:{}", player_id, partner:get_id(),
        building:get_id())
    local player = player_mgr:get_entity(player_id)
    if not player then
        log_debug("[PartnerServlet][on_send_partner] player_id:{} not online", player_id)
        return
    end
    --非产出类型建筑
    local product_type = building:get_prototype().product_type or 0
    if product_type == 0 then
        return
    end
    log_debug("[PartnerServlet][on_send_partner] add buff to building_id({})", building:get_id())

    if player:get_sat_open() then
        local town = player:get_scene()
        local effects = sat_db:find_one(town:get_sat_lv()).effect_ids
        partner:load_sat_effect(effects)
    end
    --npc属性效果
    building:load_partner_attr(partner)
    --npc技能效果
    local star_conf = partner:get_star_conf()
    if not star_conf or not star_conf.nf_skills then
        log_debug("[PartnerServlet][on_send_partner] player_id:{} partner_id:{} dont have skill", player_id,
            partner:get_id())
        return
    end
    for _, nf_skill_id in pairs(star_conf.nf_skills or {}) do
        local skill_conf = nf_skill_db:find_one(nf_skill_id)
        if not skill_conf or not skill_conf.skill_effect then
            goto continue
        end
        --判断作用目标
        if skill_conf.target ~= TARGET_BUILDING then
            goto continue
        end
        for _, buff_id in pairs(skill_conf.skill_effect) do
            building:add_partner_buff(partner:get_id(), buff_id)
        end
        ::continue::
    end
end

--npc撤回
function PartnerServlet:on_recall_partner(player_id, partner, building)
    local partner_id = partner:get_id()
    log_debug("[PartnerServlet][on_recall_partner] player_id:{} partner_id:{}, building_id:{}", player_id, partner_id,
        building:get_id())
    --非产出类型建筑
    local product_type = building:get_prototype().product_type or 0
    if product_type == 0 then
        return
    end
    local player = player_mgr:get_entity(player_id)
    if not player then
        log_debug("[PartnerServlet][on_recall_partner] player_id:{} not online", player_id)
        return
    end
    --npc属性效果
    building:unload_partner_attr(partner)
    if player:get_sat_open() then
        partner:unload_sat_effect()
    end
    --技能效果
    building:remove_partner_buff(partner_id)
end

--民居销毁
function PartnerServlet:on_house_takeback(player, building_id)
    player:drive_home(building_id)
end

--npc好感度变化
function PartnerServlet:rpc_affinity_change(player_id, npc_id, num)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return ROLE_NOT_EXIST
    end
    local partner = player:has_partner(npc_id)
    if not partner then
        return PARTNER_NOT_FOUND
    end
    if num < 0 then
        partner:reduce_affinity(num)
    else
        local liking = player:get_day_talk_liking()
        if liking >= NPC_TALK_MAX then -- 好感度达到最大值
            log_debug("[PartnerServlet][rpc_affinity_change] liking value limit player:({}) liking:{}", player_id, liking)
            return
        end
        if liking + num >= NPC_TALK_MAX then
            num = NPC_TALK_MAX - liking
        end
        partner:add_affinity(num)
        player:add_day_talk_liking(num)
    end
    player:update_partner(partner:get_id(), partner)
    return FRAME_SUCCESS
end

--获得npc
function PartnerServlet:rpc_obtain_npc(player_id, npc_id, from)
    log_debug("[PartnerServlet][rpc_obtain_npc] npc_id:{}", npc_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        return ROLE_NOT_EXIST
    end

    local has = player:has_partner(npc_id)
    if has then
        --转为碎片
        recruit_mgr:npc_repeated(player, npc_id)
    else
        player:recv_partner(npc_id, from)
    end
    return FRAME_SUCCESS
end

--CS协议
-------------------------------------------------------------------------
--派驻
function PartnerServlet:on_send_req(player, player_id, message)
    local town = player:get_scene()
    local id, building_pos, building_id = message.id, message.building_pos, message.building_id
    log_debug("[PartnerServlet][on_send_req] player({}) id({}), pos({}) building_id({})!", player_id, id, building_pos,
        building_id)
    local building = town:get_building(building_id)
    if not building then
        return FRAME_SUCCESS, { error_code = BUING_NOT_EXIST }
    end

    --位置校验
    local pos, onpc_id = building:is_valid_pos(building_pos)
    if not pos then
        return FRAME_SUCCESS, { error_code = FRAME_PARAMS }
    end
    --旧npc校验
    local onpc = player:get_partner(onpc_id)
    if onpc then
        if onpc:get_state() ~= STATE_SEND then
            return FRAME_SUCCESS, { error_code = PARTNER_STATE_ERR }
        end
        -- npc召回验证
        if not town:check_npc_recall(building_id) then
            log_err("[PartnerServlet][on_send_req] {} produce:{} check_npc_recall", player_id, id)
            return FRAME_SUCCESS, { error_code = PARTNER_WORK_RPLC }
        end
    end
    --新npc信息
    local nnpc = player:get_partner(id)
    if not nnpc then
        return FRAME_SUCCESS, { error_code = PARTNER_NOT_FOUND }
    end
    --npc民居
    if not nnpc:check_house() then
        return FRAME_SUCCESS, { error_code = PARTNER_STATE_ERR }
    end
    --npc状态
    if nnpc:get_building_id() ~= 0 and nnpc:get_state() ~= STATE_IDLE then
        return FRAME_SUCCESS, { error_code = PARTNER_STATE_ERR }
    end
    if onpc then
        building:recall_partner(onpc)
        player:recall_partner(onpc)
    end
    --派遣npc
    building:send_partner(pos, nnpc)
    player:send_partner(nnpc, building, pos)
    return FRAME_SUCCESS, { error_code = 0 }
end

--召回
function PartnerServlet:on_recall_req(player, player_id, message)
    local town = player:get_scene()
    local id = message.id
    log_debug("[PartnerServlet][on_recall_req] player({}) id({})!", player_id, id)
    --npc信息
    local npc = player:get_partner(id)
    if not npc then
        return FRAME_SUCCESS, { error_code = PARTNER_NOT_FOUND }
    end
    --建筑
    local building_id = npc:get_building_id()
    if building_id == 0 or npc:get_state() == STATE_WORK then
        return FRAME_SUCCESS, { error_code = PARTNER_STATE_BUSY }
    end

    local building = town:get_building(building_id)
    if not building then
        log_err("[PartnerServlet][on_recall_req] {} building({}) not building", player_id, id)
        return FRAME_SUCCESS, { error_code = BUING_NOT_EXIST }
    end

    -- npc召回验证
    if not town:check_npc_recall(building_id) then
        log_err("[PartnerServlet][on_recall_req] {} produce:{} check_npc_recall", player_id, id)
        return FRAME_SUCCESS, { error_code = PARTNER_WORK_RECALL }
    end

    --通知建筑
    building:recall_partner(npc)
    --召回
    player:recall_partner(npc)
    return FRAME_SUCCESS, { error_code = 0 }
end

--送礼
function PartnerServlet:on_gift_req(player, player_id, message)
    local partner_id, item_id, num = message.id, message.item_id, message.num
    log_debug("[PartnerServlet][on_gift_req] player({}) partner_id({}), item_id({}) num({})!", player_id, partner_id,
        item_id, num)
    --npc信息
    local partner = player:get_partner(partner_id)
    if not partner then
        return FRAME_SUCCESS, { error_code = PARTNER_NOT_FOUND }
    end
    --是否可收礼
    if not partner:check_recv_gift(num) then
        return FRAME_SUCCESS, { error_code = PARTNER_GIFT_LIMIT }
    end
    --是否可送礼物
    local item_conf = item_db:find_one(item_id)
    if not item_conf or not item_conf.base_affinity or item_conf.base_affinity == 0 then
        log_err("[PartnerServlet][on_gift_req] item({}) not found or not gift", item_id)
        return FRAME_SUCCESS, { error_code = ITEM_NOT_GIFT }
    end

    --扣除
    local costs = { cost = { [item_id] = num }, reason = NID_COST_GIFT }
    local code = player:execute_cost(costs)
    if qfailed(code) then
        log_err("[PartnerServlet][on_gift_req] player({}) drop cost failed: {}!", player_id)
        return FRAME_SUCCESS, { error_code = code }
    end
    partner:recv_gift(player, item_id, num)
    player:update_partner(partner:get_id(), partner)
    return FRAME_SUCCESS, { error_code = 0 }
end

--遣散
function PartnerServlet:on_drive_req(player, player_id, message)
    log_debug("[PartnerServlet][on_drive_req] player({}) message:{}!", player_id, message)
    local partner_id = message.id
    --npc信息
    local partner = player:get_partner(partner_id)
    if not partner then
        return FRAME_SUCCESS, { error_code = PARTNER_NOT_FOUND }
    end
    --是否可遣散
    if not partner:can_drive() then
        return FRAME_SUCCESS, { error_code = CANT_DRIVE }
    end
    --通知遣散
    player:drive_partner(partner)
    return FRAME_SUCCESS, { error_code = 0 }
end

--升星
function PartnerServlet:on_up_star_req(player, player_id, message)
    log_debug("[PartnerServlet][on_up_star_req] player({}) message:{}!", player_id, message)
    local partner_id = message.partner_id
    local partner = player:get_partner(partner_id)
    if not partner then
        log_err("[PartnerServlet][on_up_star_req] player:{} partner_id:{} not found", player_id, partner_id)
        return FRAME_PARAMS
    end
    --是否可升星
    local ok, check_code = partner:check_up_star()
    if not ok then
        return FRAME_SUCCESS, { error_code = check_code }
    end
    --材料是否足够
    local costs = { cost = partner:get_star_conf().up_costs, reason = NID_COST_STAR }
    local code = player:execute_cost(costs)
    if qfailed(code) then
        log_err("[PartnerServlet][on_up_star_req] player({}) cost failed: {}!", player_id, code)
        return FRAME_SUCCESS, { error_code = COST_NOT_ENOUGH }
    end
    local old_star_lv = partner:get_star_conf().star_lv
    partner:up_star()
    --通知客户端
    player:update_partner(partner_id, partner)
    return FRAME_SUCCESS, { error_code = 0, old_star_lv = old_star_lv }
end

--升级
function PartnerServlet:on_up_lv_req(player, player_id, message)
    log_debug("[PartnerServlet][on_up_lv_req] player({}) message:{}!", player_id, message)
    local partner_id, items = message.partner_id, message.items
    local check_code, add_exp = self:check_up_lv(partner_id, player, items)
    if FRAME_SUCCESS ~= check_code then
        return FRAME_SUCCESS, { error_code = check_code }
    end

    local exp_cost_items = {}
    --使用经验道具的消耗
    for item_id, num in pairs(items) do
        --道具本身
        exp_cost_items[item_id] = (exp_cost_items[item_id] or 0) + num
        local item_cfg = item_db:find_one(item_id)
        local exp_costs = item_cfg.exp_costs
        --使用消耗
        for cost_id, cost_num in pairs(exp_costs or {}) do
            exp_cost_items[cost_id] = (exp_cost_items[cost_id] or 0) + cost_num * num
        end
    end

    local cost_code = self:cost_items(exp_cost_items, player)
    if cost_code ~= FRAME_SUCCESS then
        return FRAME_SUCCESS, { error_code = cost_code }
    end

    local partner = player:get_partner(partner_id)
    local old_lv = partner:get_lv_conf().level
    local left_exp = partner:add_exp(add_exp)
    local return_items = {}
    if left_exp then
        return_items = item_mgr:exp_to_item(left_exp)
        --发放道具
        if return_items and next(return_items) then
            log_debug("[PartnerServlet][on_up_lv_req] player_id:{} return_items:{}", player_id, return_items)
            local drop_code = self:drop_items(return_items, player)
            if FRAME_SUCCESS ~= drop_code then
                return FRAME_SUCCESS, { error_code = drop_code }
            end
        end
    end
    --更新信息
    player:update_partner(partner_id, partner)

    log_debug("[PartnerServlet][on_up_lv_req] player_id:{} old_lv:{}, new_level:{}", player_id, old_lv, partner:get_lv())
    return FRAME_SUCCESS, { error_code = 0, items = return_items, old_lv = old_lv }
end

--升级参数检查
function PartnerServlet:check_up_lv(partner_id, player, items)
    local partner = player:get_partner(partner_id)
    if not partner then
        log_err("[PartnerServlet][on_up_lv_req] check_up_lv:{} partner_id:{} not found", player:get_id(), partner_id)
        return FRAME_PARAMS
    end
    --是否已达到待突破状态
    if partner:check_limit_lv() then
        return PARTNER_UNLIMIT
    end

    local item_ok, add_exp = item_mgr:get_items_exp(items)
    if not item_ok then
        log_err("[PartnerServlet][on_up_lv_req] check_up_lv:{} items:{} cant add npc exp", player:get_id(), items)
        return FRAME_PARAMS
    end
    return FRAME_SUCCESS, add_exp
end

--增加物品
function PartnerServlet:drop_items(return_items, player)
    local drops = { drop = return_items, reason = OBTAIN_NPC }
    local code = player:execute_drop(drops, true)
    if qfailed(code) then
        log_err("[PartnerServlet][drop_items] player({}) drop cost failed: {}!", player:get_id(), code)
        return code
    end
    return FRAME_SUCCESS
end

--扣除物品
function PartnerServlet:cost_items(exp_cost_items, player)
    --扣除道具
    local costs = { cost = exp_cost_items, reason = COST_NPC_EXP }
    local code = player:execute_cost(costs)
    log_debug("[PartnerServlet][cost_items] player_id:{} costs:{} code:{}", player:get_id(),
        costs, code)
    if qfailed(code) then
        log_err("[PartnerServlet][cost_items] player({}) drop cost failed: {}!", player:get_id(), code)
        return code
    end
    return FRAME_SUCCESS
end

--记录NPC红点
function PartnerServlet:on_reddot_req(player, player_id, message)
    log_debug("[PartnerServlet][on_reddot_req] player({}) message:{}!", player_id, message)
    local partner_id, red_story, red_voice = message.partner_id, message.reddot_story, message.reddot_voice
    local partner = player:get_partner(partner_id)
    if not partner then
        log_err("[PartnerServlet][on_reddot_req] player_id:{} partner_id:{} not found", player_id, partner_id)
        return FRAME_PARAMS
    end
    if red_story ~= 0 then
        partner:save_reddot_story(red_story)
    end
    if red_voice ~= 0 then
        partner:save_reddot_voice(red_voice)
    end
    return FRAME_SUCCESS, { error_code = 0 }
end

--突破
function PartnerServlet:on_unlv_req(player, player_id, message)
    log_debug("[PartnerServlet][on_unlv_req] player({}) message:{}!", player_id, message)
    local partner_id = message.partner_id
    local partner = player:get_partner(partner_id)
    if not partner then
        log_err("[PartnerServlet][on_unlv_req] player_id:{} partner_id:{} not found", player_id, partner_id)
        return FRAME_PARAMS
    end
    --是否达到突破条件
    if not partner:check_limit_lv() then
        log_err("[PartnerServlet][on_unlv_req] player_id:{} partner_id:{} cant unlv", player_id, partner_id)
        return FRAME_PARAMS
    end

    --突破消耗是否足够
    local lv_conf = partner:get_lv_conf()
    local cost_items = lv_conf.limit_costs
    if not cost_items then
        log_err("[PartnerServlet][on_unlv_req] partner_id:{} cant find limit costs", partner_id)
        return FRAME_PARAMS
    end
    --城镇等级限制
    if player:get_scene_level() < lv_conf.town_lv then
        return FRAME_SUCCESS, { error_code = PARTNER_TOWN_LV_ERR }
    end
    --扣除道具
    if cost_items then
        local costs = { cost = cost_items, reason = NID_COST_UNLV }
        local code = player:execute_cost(costs)
        if qfailed(code) then
            log_err("[PartnerServlet][on_up_lv_req] player({}) cost failed: {}!", player_id, code)
            return FRAME_SUCCESS, { error_code = code }
        end
    end
    local old_limit_lv = partner:get_lv_conf().limit_lv
    --突破
    partner:unlv()
    local new_limit_lv = partner:get_lv_conf().limit_lv
    --更新信息
    player:update_partner(partner_id, partner)
    log_debug("[PartnerServlet][on_unlv_req] player_id:{} old_limit_lv:{}, new_limit_lv:{}", player_id, old_limit_lv,
        new_limit_lv)
    return FRAME_SUCCESS, { error_code = 0, old_limit_lv = old_limit_lv }
end

function PartnerServlet:on_dialog_req(player, player_id, message)
    log_debug("[PartnerServlet][on_dialog_req] player({}) message:{}!", player_id, message)
    local partner_id = message.partner_id
    local partner = player:get_partner(partner_id)
    if not partner then
        log_err("[PartnerServlet][on_dialog_req] player_id:{} partner_id:{} not found", player_id, partner_id)
        return FRAME_PARAMS
    end
    if partner:get_is_dialog() then
        return FRAME_SUCCESS, { error_code = 1 }
    end
    partner:save_is_dialog(true)
    return FRAME_SUCCESS, { error_code = 0 }
end

quanta.partner_servlet = PartnerServlet()

return PartnerServlet
