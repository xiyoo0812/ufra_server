--building_servlet.lua
local log_err             = logger.err
local log_debug           = logger.debug
local log_warn            = logger.warn

local u8len               = utf8.len
local qfailed             = quanta.failed
local event_mgr           = quanta.get("event_mgr")
local object_fty          = quanta.get("object_fty")
local player_mgr          = quanta.get("player_mgr")
local protobuf_mgr        = quanta.get("protobuf_mgr")
local config_mgr          = quanta.get("config_mgr")
local block_db            = config_mgr:init_table("block", "id")
local item_db             = config_mgr:get_table("item")
local utility_db          = config_mgr:get_table("utility")
local RECYCLE_CD          = utility_db:find_number("value", "recovery_time")
local RECYCLE_NUM         = utility_db:find_number("value", "recovery_num")

local BUILDING_UPGRADE    = quanta.enum("BuildingOpt", "UPGRADE")
local BUILDING_FINISH     = quanta.enum("BuildingOpt", "FINISH")

local FRAME_FAILED        = protobuf_mgr:error_code("FRAME_FAILED")
local FRAME_SUCCESS       = protobuf_mgr:error_code("FRAME_SUCCESS")
local FRAME_PARAMS        = protobuf_mgr:error_code("FRAME_PARAMS")
local ROLE_NOT_EXIST      = protobuf_mgr:error_code("LOGIN_ROLE_NOT_EXIST")
local BUING_NOT_EXIST     = protobuf_mgr:error_code("WORKS_BUING_NOT_EXIST")
local ACTIVE_LEVEL_ERR    = protobuf_mgr:error_code("WORKS_ACTIVE_LEVEL_ERR")
local DRAWING_ACTIVED     = protobuf_mgr:error_code("WORKS_DRAWING_ACTIVED")
local DRAWING_NOT_EXIST   = protobuf_mgr:error_code("WORKS_DRAWING_NOT_EXIST")
local GROUND_ACTIVED      = protobuf_mgr:error_code("WORKS_GROUND_ACTIVED")
local BLOCK_ACTIVED       = protobuf_mgr:error_code("WORKS_BLOCK_ACTIVED")
local GROUND_NOT_FINISH   = protobuf_mgr:error_code("WORKS_GROUND_NOT_FINISH")
local BLOCK_NOT_FINISH    = protobuf_mgr:error_code("WORKS_BLOCK_NOT_FINISH")
local UNLOCK_LEVEL_ERR    = protobuf_mgr:error_code("WORKS_UNLOCK_LEVEL_ERR")
local BUILD_NOT_FINISH    = protobuf_mgr:error_code("WORKS_BUILD_NOT_FINISH")
local WORK_FULL           = protobuf_mgr:error_code("TOWN_WORK_FULL")
local UTENSIL_LIMIT       = protobuf_mgr:error_code("WORKS_UTENSIL_LIMIT")
local BUILD_NOT_SP        = protobuf_mgr:error_code("WORKS_BUILD_NOT_SP")
local RECYCLE_ITEM_CD     = protobuf_mgr:error_code("WORKS_RECYCLE_ITEM_CD")
local RECYCLE_ITEM_ERR    = protobuf_mgr:error_code("WORKS_RECYCLE_ITEM_ERR")
local RECYCLE_NUM_ERR     = protobuf_mgr:error_code("WORKS_RECYCLE_NUM_ERR")
local HOUSE_NAME_ERR      = protobuf_mgr:error_code("HOUSE_NAME_ERR")
local HOUSE_POS_NOT_EMPTY = protobuf_mgr:error_code("HOUSE_POS_NOT_EMPTY")
local HOUSE_NO            = protobuf_mgr:error_code("HOUSE_NO")
local PARTNER_BUSY        = protobuf_mgr:error_code("PARTNER_STATE_BUSY")
local PARTNER_NOT_FOUND   = protobuf_mgr:error_code("PARTNER_NOT_FOUND")
local BUILD_NODE_ACTIVED  = protobuf_mgr:error_code("WORKS_BUILD_NODE_ACTIVED")
local BUILD_CANT_UP       = protobuf_mgr:error_code("WORKS_BUILD_CANT_UP")

local COST_BUILDING       = protobuf_mgr:enum("cost_reason", "NID_COST_BUILDING")
local COST_SPEEDUP        = protobuf_mgr:enum("cost_reason", "NID_COST_SPEEDUP")
local OBTAIN_GROUND       = protobuf_mgr:enum("obtain_reason", "NID_OBTAIN_GROUND")
local NID_COST_ACCEL      = protobuf_mgr:enum("cost_reason", "NID_COST_ACCEL")
local COST_RECYCLE        = protobuf_mgr:enum("cost_reason", "NID_COST_RECYCLE")
local OBTAIN_RECYCLE      = protobuf_mgr:enum("obtain_reason", "NID_OBTAIN_RECYCLE")

local BuildingServlet     = singleton()
function BuildingServlet:__init()
    event_mgr:add_listener(self, "rpc_active_drawing")
    event_mgr:add_listener(self, "rpc_ground_unlock_count")
    event_mgr:add_listener(self, "rpc_utensil_update")
    event_mgr:add_listener(self, "rpc_block_unlock")
    -- CS协议
    protobuf_mgr:register(self, "NID_BUILDING_MOVE_REQ", "on_move_req")
    protobuf_mgr:register(self, "NID_BUILDING_PLACE_REQ", "on_place_req")
    protobuf_mgr:register(self, "NID_BUILDING_FINISH_REQ", "on_finish_req")
    protobuf_mgr:register(self, "NID_BUILDING_UNLOCK_REQ", "on_unlock_req")
    protobuf_mgr:register(self, "NID_BLOCK_ACCEL_REQ", "on_block_accel_req")
    protobuf_mgr:register(self, "NID_BUILDING_UPGRADE_REQ", "on_upgrade_req")
    protobuf_mgr:register(self, "NID_BUILDING_SPEEDUP_REQ", "on_speedup_req")
    protobuf_mgr:register(self, "NID_BLOCK_UNLOCK_REQ", "on_block_unlock_req")
    protobuf_mgr:register(self, "NID_BLOCK_FINISH_REQ", "on_block_finish_req")
    protobuf_mgr:register(self, "NID_BUILDING_TAKEBACK_REQ", "on_takeback_req")
    protobuf_mgr:register(self, "NID_BUILDING_ITEM_RECYCLE_REQ", "on_item_recycle_req")
    protobuf_mgr:register(self, "NID_BUILDING_CHECKIN_REQ", "on_checkin_req")
    protobuf_mgr:register(self, "NID_BUILDING_HOUSENAME_REQ", "on_housename_req")
    protobuf_mgr:register(self, "NID_BUILDING_CHECKOUT_REQ", "on_checkout_req")
    protobuf_mgr:register(self, "NID_BUILDING_CHANGEHOUSE_REQ", "on_changehouse_req")
end

--rpc协议
-------------------------------------------------------------------------
--data通知scene观察者拉取opid
function BuildingServlet:rpc_utensil_update(player_id, op_id)
    log_debug("[BuildingServlet][rpc_utensil_update] player({}), op_id({})", player_id, op_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        log_err("[BuildingServlet][rpc_utensil_update] not found player_id={}", player_id)
        return ROLE_NOT_EXIST
    end
    local town = player:get_scene()
    if not town then
        return ROLE_NOT_EXIST
    end
    for id, scene_player in pairs(town:get_players()) do
        --自己不发
        if player_id ~= id then
            scene_player:send("NID_BUILDING_UTENSIL_NTF", { player_id = player_id, opid = op_id })
        end
    end
end

function BuildingServlet:rpc_active_drawing(player_id, drawing_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        log_err("[BuildingServlet][rpc_active_drawing] need login, player_id={}", player_id)
        return ROLE_NOT_EXIST
    end
    local drawing = object_fty:find_utensil(drawing_id)
    if not drawing then
        log_err("[BuildingServlet][rpc_active_drawing] {} drawing({}) not found", player_id, drawing_id)
        return DRAWING_NOT_EXIST
    end
    if player:check_level(drawing) then
        log_err("[BuildingServlet][rpc_active_drawing] {} drawing({}) active level err", player_id, drawing_id)
        return ACTIVE_LEVEL_ERR
    end
    if player:check_drawing_active(drawing) then
        log_err("[BuildingServlet][rpc_active_drawing] {} drawing({}) aready active", player_id, drawing_id)
        return DRAWING_ACTIVED
    end
    player:active_drawing(drawing_id)
    return FRAME_SUCCESS
end

function BuildingServlet:rpc_ground_unlock_count(player_id, type)
    local player = player_mgr:get_entity(player_id)
    if not player then
        log_err("[BuildingServlet][rpc_is_lock_full] need login, player_id={}", player_id)
        return ROLE_NOT_EXIST, 0
    end
    local town = player:get_scene()
    return FRAME_SUCCESS, town:get_ground_unlock_count(type)
end

function BuildingServlet:rpc_block_unlock(player_id, block_id)
    local player = player_mgr:get_entity(player_id)
    if not player then
        log_err("[BuildingServlet][rpc_block_unlock] need login, player_id={}", player_id)
        return ROLE_NOT_EXIST
    end

    local conf = block_db:find_one(block_id)
    if not conf then
        log_err("[BuildingServlet][rpc_block_unlock] {} block({}) not found", player_id, block_id)
        return FRAME_PARAMS
    end

    local town = player:get_scene()
    if not town then
        log_err("[BuildingServlet][rpc_block_unlock] {} town not found", player_id)
        return FRAME_FAILED
    end

    local code_state = self:check_block(player, town, conf, block_id)
    if code_state then
        log_err("[BuildingServlet][rpc_block_unlock] {} town not found code_state:{}", player_id, code_state)
        return code_state
    end

    local ground_id = conf.ground_id
    if town:is_block_unlock(ground_id, block_id) then
        return BLOCK_ACTIVED
    end

    town:unlock_block(player, ground_id, block_id, quanta.now + conf.time)
    log_debug("[BuildingServlet][rpc_block_unlock] unlock block ok player{} ground_id={} block_id={}", player_id,
    ground_id, block_id)
    return FRAME_SUCCESS
end

--CS协议
-------------------------------------------------------------------------
--地块加速
function BuildingServlet:on_block_accel_req(player, player_id, message)
    log_debug("[BuildingServlet][on_block_accel_req] player({}) message:{}!", player_id, message)
    local block_id, newbee = message.block_id, message.newbee or false
    local conf = block_db:find_one(block_id)
    if not conf then
        log_err("[BuildingServlet][on_block_accel_req] {} block({}) not found", player_id, block_id)
        return FRAME_SUCCESS, { error_code = FRAME_PARAMS }
    end
    local town = player:get_scene()
    local ground_id = conf.ground_id
    if not town:is_ground_unveil(ground_id) then
        log_err("[BuildingServlet][on_block_accel_req] {} ground({}) is unlock", player_id, ground_id)
        return FRAME_SUCCESS, { error_code = GROUND_NOT_FINISH }
    end
    --新手引导
    if newbee and town:get_block_faccel() then
        --立即完成
        town:unlock_block(player, ground_id, block_id, quanta.now)
        town:save_block_faccel(false)
        return FRAME_SUCCESS, { error_code = 0 }
    end
    local ground = town:get_ground(ground_id)
    if not ground then
        log_warn("[BuildingServlet][on_block_accel_req] ground_id({}) not found", ground_id)
        return FRAME_SUCCESS, { error_code = FRAME_PARAMS }
    end
    local can_accel, sp_costs = ground:get_sp_block_costs(block_id)
    if not can_accel then
        return FRAME_SUCCESS, { error_code = FRAME_PARAMS }
    end
    local costs = { cost = sp_costs, reason = NID_COST_ACCEL }
    local code = player:execute_cost(costs)
    if qfailed(code) then
        log_err("[BuildingServlet][on_block_accel_req] player({}) drop cost failed: {}!", player_id, code)
        return FRAME_SUCCESS, { error_code = code }
    end
    log_debug("[BuildingServlet][on_block_accel_req] cost:{}", costs)
    --设置为当前值
    town:unlock_block(player, ground_id, block_id, quanta.now)
    return FRAME_SUCCESS, { error_code = 0 }
end

function BuildingServlet:on_place_req(player, player_id, message)
    local scene = player:get_scene()
    local item_id, proto_id, opid, detail = message.item_id, message.proto_id, message.opid, message.detail
    log_debug("[BuildingServlet][on_place_req] player({}) place({}), proto_id({})!", player_id, item_id, proto_id)
    local utensil = object_fty:create_utensil(proto_id)
    if not utensil then
        log_err("[BuildingServlet][on_place_req] {} prototype {} not exist", player_id, proto_id)
        return FRAME_SUCCESS, { error_code = FRAME_PARAMS, opid = opid }
    end
    --个数限制
    local limit = scene:check_limit(player, utensil)
    if not limit then
        log_err("[BuildingServlet][on_place_req] proto_id:{} limit max", proto_id)
        return FRAME_SUCCESS, { error_code = UTENSIL_LIMIT, opid = opid }
    end

    -- --建筑图纸是否激活
    -- local drawing = utensil:get_prototype()
    -- if object_fty:is_building(drawing) then
    --     if player:check_level(drawing) then
    --         log_err("[BuildingServlet][on_place_req] {} proto_id({}) level err", player_id, proto_id)
    --         return FRAME_SUCCESS, {error_code = ACTIVE_LEVEL_ERR, opid = opid}
    --     end
    -- end

    local costs = utensil:place_costs()
    if costs then
        local code = player:execute_cost(costs)
        if qfailed(code) then
            log_err("[BuildingServlet][on_place_req] player({}) drop cost failed: {}-{}!", player_id, costs, code)
            return FRAME_SUCCESS, { error_code = code, opid = opid }
        end
    end
    utensil:startup(scene, detail)
    if not utensil:display_detail(scene, player, opid) then
        log_err("[BuildingServlet][on_place_req] player({}) display utensil failed!", player_id)
        return FRAME_SUCCESS, { error_code = FRAME_FAILED, opid = opid }
    end
    if object_fty:is_building(utensil.prototype) then
        utensil:fillin()
    end
    event_mgr:notify_trigger("on_building_add", scene, player_id, utensil)
    player:notify_event("on_place_utensil", proto_id, utensil:get_type())
    player:notify_event("on_home_place", {proto_id})
    scene:add_utensil_count(utensil)
    local group_id = utensil:get_group()
    local count = scene:get_group_num(group_id)
    player:notify_event("on_group_count", group_id, count)
    return FRAME_SUCCESS, { error_code = 0, opid = opid, id = utensil:get_id() }
end

function BuildingServlet:on_move_req(player, player_id, message)
    local scene = player:get_scene()
    local id, detail = message.id, message.detail
    log_debug("[BuildingServlet][on_move_req] player({}) hand({})!", player_id, message.proto_id)
    local utensi = scene:get_utensil(id)
    if not utensi then
        log_err("[BuildingServlet][on_move_req] {} utensi({}) not found", player_id, id)
        return FRAME_SUCCESS, { error_code = BUING_NOT_EXIST, id = id  }
    end
    utensi:update_detail(detail)
    return FRAME_SUCCESS, { error_code = 0, id = id }
end

function BuildingServlet:on_item_recycle_req(player, player_id, message)
    log_debug("[BuildingServlet][on_item_recycle_req] player({}) message({})!", player_id, message)
    local items = message.items
    --是否CD中
    local now = quanta.now
    local cd_end_time = player:get_recycle_end_time()
    if cd_end_time > now then
        log_err("[BuildingServlet][on_item_recycle_req] player_id:{} cd_end_time:{} < now:{}",
            player_id, cd_end_time, now)
        return FRAME_SUCCESS, { error_code = RECYCLE_ITEM_CD }
    end
    --出售道具
    local return_items = {}
    local total_size = 0
    for item_id, num in pairs(items) do
        local item_conf = item_db:find_one(item_id)
        if not item_conf or not item_conf.recycle_price then
            return FRAME_SUCCESS, { error_code = RECYCLE_ITEM_ERR }
        end
        for recy_item, recy_price in pairs(item_conf.recycle_price) do
            return_items[recy_item] = (return_items[recy_item] or 0) + recy_price * num
        end
        total_size = total_size + 1
    end
    --数量是否超出
    if total_size > RECYCLE_NUM then
        return FRAME_SUCCESS, { error_code = RECYCLE_NUM_ERR }
    end
    --扣除并掉落
    local cost = { cost = items, reason = COST_RECYCLE }
    local drop = { drop = return_items, reason = OBTAIN_RECYCLE }
    local code = player:execute_cost_drop(drop, cost, nil, true)
    if qfailed(code) then
        return FRAME_SUCCESS, { error_code = code }
    end
    --设置CD时间
    player:set_recycle_end_time(now + RECYCLE_CD)
    --抛出回收事件
    for item_id, num in pairs(items) do
        player:notify_event("on_item_recycle", item_id, num)
    end
    return FRAME_SUCCESS, { error_code = 0 }
end

function BuildingServlet:on_takeback_req(player, player_id, message)
    local uid = message.id
    local scene = player:get_scene()
    log_debug("[BuildingServlet][on_takeback_req] player({}) takeback({})!", player_id, uid)

    -- 获取建筑对象
    local utensil = scene:get_utensil(uid)
    if not utensil then
        log_err("[BuildingServlet][on_takeback_req] {} utensil({}) not found", player_id, uid)
        return FRAME_SUCCESS, { error_code = BUING_NOT_EXIST, id = uid  }
    end

    -- 派驻检查
    local error_code = utensil:check_takeback(player)
    if qfailed(error_code) then
        log_err("[BuildingServlet][on_takeback_req] {} utensil({}) working error_code:{}", player_id, uid, error_code)
        return FRAME_SUCCESS, { error_code = error_code, id = uid  }
    end

    -- 获取生产
    local produce = scene:get_produces(uid)
    if produce then
        error_code = produce:check_takeback()
        if qfailed(error_code) then
            log_err("[BuildingServlet][on_takeback_req] {} produce({}) working error_code:{}", player_id, uid, error_code)
            return FRAME_SUCCESS, { error_code = error_code, id = uid  }
        end
    end

    -- 返回物品
    local drops = utensil:tackback_obtain()
    if drops then
        local code = player:execute_drop(drops)
        if qfailed(code) then
            log_err("[BuildingServlet][on_takeback_req] player({}) drop cost failed: {}!", player_id, code)
            return FRAME_SUCCESS, { error_code = code, id = uid  }
        end
    end

    -- 拆除建筑
    if not utensil:takeback_detail(scene, player) then
        log_err("[BuildingServlet][on_takeback_req] player({}) takeback utensil failed!", player_id)
        return FRAME_SUCCESS, { error_code = FRAME_FAILED, id = uid }
    end

    event_mgr:notify_trigger("on_building_del", scene, player_id, utensil)
    scene:reduce_utensil_count(utensil)
    scene:reduce_building_fn(utensil)
    return FRAME_SUCCESS, { error_code = 0, id = uid }
end

function BuildingServlet:on_unlock_req(player, player_id, message)
    local id = message.id
    log_debug("[BuildingServlet][on_unlock_req] player({}) ground({})!", player_id, id)
    local conf = object_fty:find_ground(id)
    if not conf then
        log_err("[BuildingServlet][on_unlock_req] {} ground({}) not found", player_id, id)
        return FRAME_SUCCESS, { error_code = FRAME_PARAMS }
    end
    local town = player:get_scene()
    if not town then
        log_err("[BuildingServlet][on_unlock_req] {} town not found", player_id)
        return FRAME_FAILED
    end
    if town:is_ground_unlock(id) then
        log_err("[BuildingServlet][on_unlock_req] {} ground({}) is unlock", player_id, id)
        return FRAME_SUCCESS, { error_code = GROUND_ACTIVED }
    end
    if player:get_scene_level() < conf.level then
        log_err("[BuildingServlet][on_unlock_req] {} ground({}) level err", player_id, id)
        return FRAME_SUCCESS, { error_code = UNLOCK_LEVEL_ERR }
    end
    local costs = { cost = conf.costs, reason = COST_BUILDING }
    local code = player:execute_cost(costs)
    if qfailed(code) then
        log_err("[BuildingServlet][on_unlock_req] player({}) cost failed: {}!", player_id, code)
        return FRAME_SUCCESS, { error_code = code }
    end
    town:unlock_ground(player, id, quanta.now + conf.time)
    event_mgr:notify_trigger("on_unlock_land", player, conf)
    --改为直接解锁
    self:on_finish_req(player, player_id, { id = id, type = 2 })
    return FRAME_SUCCESS, { error_code = 0 }
end

--地块解锁
function BuildingServlet:on_block_unlock_req(player, player_id, message)
    local block_id = message.block_id
    log_debug("[BuildingServlet][on_block_unlock_req] player({}) block({})!", player_id, block_id)
    local conf = block_db:find_one(block_id)
    if not conf then
        log_err("[BuildingServlet][on_block_unlock_req] {} block({}) not found", player_id, block_id)
        return FRAME_SUCCESS, { error_code = FRAME_PARAMS }
    end
    local town = player:get_scene()
    if not town then
        log_err("[BuildingServlet][on_block_unlock_req] {} town not found", player_id)
        return FRAME_SUCCESS, { error_code = FRAME_FAILED }
    end
    --条件检查
    local code_state = self:check_block(player, town, conf, block_id)
    if code_state then
        return FRAME_SUCCESS, { error_code = code_state }
    end
    local ground_id = conf.ground_id
    if town:is_block_unlock(ground_id, block_id) then
        return FRAME_SUCCESS, { error_code = BLOCK_ACTIVED }
    end

    --工作队列
    if not town:is_town_ground(ground_id) then
        return FRAME_SUCCESS, { error_code = WORK_FULL }
    end

    --消耗是否足够
    local cost_code = self:check_block_unlock_cost(player, town, ground_id, block_id, conf)
    if cost_code ~= FRAME_SUCCESS then
        return FRAME_SUCCESS, {error_code = cost_code}
    end

    town:unlock_block(player, ground_id, block_id, quanta.now + conf.time)
    log_debug("[BuildingServlet][on_block_unlock_req] unlock block ok player{} ground_id={} block_id={}", player_id,
        ground_id, block_id)
    return FRAME_SUCCESS, { error_code = 0 }
end

--检查地块解锁消耗
function BuildingServlet:check_block_unlock_cost(player, town, ground_id, block_id, conf)
    local ground = town:get_ground(ground_id)
    local block_time = ground:get_block_time(block_id)
    if not block_time and conf.costs then
        --消耗
        local costs = { cost = conf.costs, reason = COST_BUILDING }
        local code = player:execute_cost(costs)
        if not code or qfailed(code) then
            log_err("[BuildingServlet][check_block_unlock_cost] player({}) cost failed: {}!", player:get_id(), code)
            return code
        end
    end
    return FRAME_SUCCESS
end

function BuildingServlet:check_utensil_upgrade(player, scene, utensil_id, proto_id)
    --工作队列
    local town = player:get_scene()
    local utensil = scene:get_utensil(utensil_id)
    if not utensil then
        log_err("[BuildingServlet][check_utensil_upgrade] {} utensil({}) not utensil", player:get_id(), utensil_id)
        return BUING_NOT_EXIST
    end

    if utensil:get_proto_id() == proto_id then
        log_err("[BuildingServlet][check_utensil_upgrade] {} utensilconf({}) node activied", player:get_id(), utensil_id)
        return BUILD_NODE_ACTIVED
    end

    -- 获取节点配置
    local conf = object_fty:find_utensil(proto_id)
    if not conf then
        log_err("[BuildingServlet][check_utensil_upgrade] {} utensil({}-{}) not found", player:get_id(), utensil_id, proto_id)
        return BUILD_CANT_UP
    end

    local ch_code = utensil:check_upgrade(player, scene, conf)
    if ch_code ~= FRAME_SUCCESS then
        log_err("[BuildingServlet][check_utensil_upgrade] {} utensil({}-{}-{}) not found", player:get_id(), utensil_id, proto_id, ch_code)
        return ch_code
    end
    return FRAME_SUCCESS, town, utensil, conf, utensil:is_worn()
end

function BuildingServlet:on_upgrade_req(player, player_id, message)
    local id, detail, proto_id = message.id, message.detail, message.proto_id
    local scene = player:get_scene()
    log_debug("[BuildingServlet][on_upgrade_req] player({}) utensil({}) proto_id:{}!", player_id, id, proto_id)

    local ch_code,_,utensil,conf,worn = self:check_utensil_upgrade(player, scene, id, proto_id)
    if ch_code ~= FRAME_SUCCESS then
        log_err("[BuildingServlet][on_upgrade_req] {} utensil({}-{}) ch_code:{}", player_id, id, proto_id, ch_code)
        return FRAME_SUCCESS, { error_code = ch_code }
    end

    -- 修复
    if worn then
        local worn_code = self:building_worn(player, player_id, utensil, conf, detail)
        return FRAME_SUCCESS, { error_code = worn_code }
    end

    -- 获取升级材料
    local cost, type = utensil:get_upcost(conf)
    if cost and next(cost) then
        local costs = { cost = cost, reason = COST_BUILDING }
        local code = player:execute_cost(costs)
        if qfailed(code) then
            log_err("[BuildingServlet][on_upgrade_req] player({}) drop cost failed: {}!", player_id, code)
            return FRAME_SUCCESS, { error_code = code }
        end
    end

    utensil:upgrade(player, proto_id, detail)
    local prototype = utensil:get_prototype()
    if object_fty:is_building(prototype) then
        event_mgr:notify_trigger("on_building_stage", player, BUILDING_UPGRADE, id, prototype)
    end
    log_debug("[BuildingServlet][on_upgrade_req] end player({}) utensil({}) proto_id:{} worn:{} costs:{}!", player_id, id, proto_id, worn, cost)
    return FRAME_SUCCESS, { error_code = 0, type = type }
end

-- 修复建筑
function BuildingServlet:building_worn(player, player_id, utensil, conf, detail)
    log_debug("[BuildingServlet][building_worn] player_id:{}, utensil:{} conf:{}", player_id, utensil.id, conf)
    local scene = player:get_scene()
    local cost = conf.costs
    if cost and next(cost) then
        local costs = { cost = cost, reason = COST_BUILDING }
        local code = player:execute_cost(costs)
        if qfailed(code) then
            log_err("[BuildingServlet][building_worn] player({}) drop cost failed: {}!", player_id, code)
            return code
        end
    end

    local utensil_id = utensil:get_proto_id()
    utensil:repair(player, conf.id, detail)
    event_mgr:notify_trigger("on_building_upgrade", scene, player_id, utensil)
    local utensil_type = utensil:get_struct_type()
    -- 用具修复
    player:notify_event("on_utensil_repair", utensil_type, utensil_id)
    local prototype = utensil:get_prototype()
    if object_fty:is_building(prototype) then
        event_mgr:notify_trigger("on_building_stage", player, BUILDING_UPGRADE, utensil.id, prototype)
    end
    return FRAME_SUCCESS
end

--- 建筑揭幕
function BuildingServlet:building_finish(player, player_id, scene, id)
    log_debug("[BuildingServlet][building_finish] player_id:{}, building_id:{}", player_id, id)
    local building = scene:get_building(id)
    if not building then
        log_err("[BuildingServlet][building_finish] player:{} building:{} not found", player_id, id)
        return FRAME_SUCCESS, { error_code = BUING_NOT_EXIST }
    end
    if not building:check_complete() then
        log_debug("[BuildingServlet][building_finish] check_complete player:{} building({}) not complete!", player_id, id)
        return FRAME_SUCCESS, { error_code = BUILD_NOT_FINISH }
    end
    local prototype = building:get_prototype()
    local code, upgrade = building:finish(player)
    if qfailed(code) then
        log_debug("[BuildingServlet][building_finish] finish player:{} building:{} code:{} not finish!", player_id, id, code)
        return FRAME_SUCCESS, { error_code = code }
    end

    -- 首次创建
    local town = player:get_scene()
    if not upgrade then
        local proto_id = building:get_proto_id()
        local count = town:get_building_num(proto_id)
        event_mgr:notify_trigger("on_building_add", scene, player_id, building)
        player:notify_event("on_building_finish", proto_id, prototype.tab)
        player:notify_event("on_building_count", proto_id, count)
        -- 升级建筑
    else
        event_mgr:notify_trigger("on_building_upgrade", scene, player_id, building)
    end

    local new_prototype = building:get_prototype()
    if object_fty:is_building(new_prototype) then
        town:add_building_fn(building)
        local level = building:get_level()
        local group_id = building:get_group()
        player:notify_event("on_building_level_up", group_id, level)
        event_mgr:notify_trigger("on_building_stage", player, BUILDING_FINISH, id, new_prototype,
            new_prototype.build_time)
    end
    return FRAME_SUCCESS, { error_code = 0 }
end

function BuildingServlet:on_finish_req(player, player_id, message)
    local scene = player:get_scene()
    local id, type = message.id, message.type
    log_debug("[BuildingServlet][on_finish_req] player({}) ground({})!", player_id, id)
    if type == 1 then
        return self:building_finish(player, player_id, scene, id)
    end
    local conf = object_fty:find_ground(id)
    if not conf then
        log_err("[BuildingServlet][on_finish_req] {} ground({}) not found", player_id, id)
        return FRAME_SUCCESS, { error_code = FRAME_PARAMS }
    end
    local town = player:get_scene()
    if not town then
        log_err("[BuildingServlet][on_finish_req] {} town not found", player_id)
        return FRAME_SUCCESS, { error_code = FRAME_FAILED }
    end
    if not town:is_ground_finish(id) then
        log_err("[BuildingServlet][on_finish_req] {} ground({}) is not finish", player_id, id)
        return FRAME_SUCCESS, { error_code = GROUND_NOT_FINISH }
    end
    local drops = { drop = conf.rewards, reason = OBTAIN_GROUND }
    local code = player:execute_drop(drops)
    if qfailed(code) then
        log_err("[BuildingServlet][on_finish_req] player({}) drop failed: {}!", player_id, code)
        return FRAME_SUCCESS, { error_code = code }
    end
    town:unlock_ground(player, id, 0)
    player:notify_event("on_ground_unlock", id, conf.type)
    return FRAME_SUCCESS, { error_code = 0 }
end

function BuildingServlet:on_block_finish_req(player, player_id, message)
    local block_id = message.block_id
    log_debug("[BuildingServlet][on_block_finish_req] player({}) block_id({})!", player_id, block_id)
    local conf = block_db:find_one(block_id)
    if not conf then
        log_err("[BuildingServlet][on_block_finish_req] {} block({}) not found", player_id, block_id)
        return FRAME_SUCCESS, { error_code = FRAME_PARAMS }
    end
    local town = player:get_scene()
    if not town then
        log_err("[BuildingServlet][on_block_finish_req] {} town not found", player_id)
        return FRAME_SUCCESS, { error_code = FRAME_FAILED }
    end
    local ground_id = conf.ground_id
    if not town:is_ground_unveil(ground_id) then
        log_err("[BuildingServlet][on_block_finish_req] {} ground({}) is unlock", player_id, ground_id)
        return FRAME_SUCCESS, { error_code = GROUND_NOT_FINISH }
    end

    if not town:is_block_finish(ground_id, block_id) then
        log_err("[BuildingServlet][on_block_finish_req] {} block({}) is unlock", player_id, block_id)
        return FRAME_SUCCESS, { error_code = BLOCK_NOT_FINISH }
    end
    if town:is_block_unlock(ground_id, block_id) then
        log_err("[BuildingServlet][on_block_finish_req] {} block_id({}) is locked", player_id, block_id)
        return BLOCK_ACTIVED
    end
    --发放奖励
    local drops = { drop = conf.rewards, reason = OBTAIN_GROUND }
    local code = player:execute_drop(drops, true)
    if qfailed(code) then
        log_err("[BuildingServlet][on_block_finish_req] allot failed! player_id:{} drop:{}", player_id, drops)
        return FRAME_SUCCESS, { error_code = code }
    end
    town:unlock_block(player, ground_id, block_id, 0)
    player:notify_event("on_block_finish", ground_id, block_id)
    return FRAME_SUCCESS, { error_code = 0 }
end

function BuildingServlet:on_speedup_req(player, player_id, message)
    local id, is_newbee = message.id, message.is_newbee or false
    local scene = player:get_scene()
    log_debug("[BuildingServlet][on_speedup_req] player({}) building({}) is_newbee({})!", player_id, id, is_newbee)

    local building = scene:get_building(id)
    if not building then
        log_err("[BuildingServlet][on_speedup_req] {} building({}) not found", player_id, id)
        return FRAME_SUCCESS, { error_code = BUING_NOT_EXIST }
    end

    -- 状态验证
    if not building:check_producing() then
        log_debug("[BuildingServlet][on_speedup_req] player({}) building({}) not producing!", player_id, id)
        return FRAME_SUCCESS, { error_code = BUILD_NOT_SP }
    end

    -- 新手引导
    if is_newbee and scene:get_building_faccel() then
        building:save_timestamp(quanta.now)
        building:sync_changed()
        scene:save_building_faccel(false)
        return FRAME_SUCCESS, { id = id, error_code = 0 }
    end

    -- 加速消耗的道具
    local can_speedup, costs = building:get_sp_costs()
    if not can_speedup then
        log_err("[BuildingwServlet][on_speedup_req] {} product({}-{}) costs nil", player_id, id)
        return FRAME_SUCCESS, { error_code = BUILD_NOT_SP }
    end

    -- 扣除道具
    local code = player:execute_drop({ drop = costs, reason = COST_SPEEDUP })
    if qfailed(code) then
        log_err("[BuildingServlet][on_speedup_req] player({}) drop cost failed: {}!", player_id, code)
        return FRAME_SUCCESS, { error_code = code }
    end

    -- 再次验证
    if not building:check_producing() then
        log_debug("[BuildingServlet][on_speedup_req] player({}) building({}) not producing!", player_id, id)
        return FRAME_SUCCESS, { id = id, error_code = 0 }
    end

    building:save_timestamp(quanta.now)
    building:sync_changed()
    return FRAME_SUCCESS, { id = id, error_code = 0 }
end

--民居操作检查
function BuildingServlet:check_house(player, house_ids)
    local town = player:get_scene()
    for _, house_id in pairs(house_ids) do
        local house = town:get_building(house_id)
        if not house then
            return BUING_NOT_EXIST
        end
        if not object_fty:is_house(house:get_prototype()) then
            return FRAME_PARAMS
        end
    end
    return FRAME_SUCCESS
end

--入住民居
function BuildingServlet:on_checkin_req(player, player_id, message)
    log_debug("[BuildingServlet][on_checkin_req] player_id:{} message:{}", player_id, message)
    local house_pos, partner_id, house_id = message.house_pos, message.partner_id, message.house_id
    --参数检查
    local check_code = self:check_house(player, { house_id })
    if check_code ~= FRAME_SUCCESS then
        return FRAME_SUCCESS, { error_code = check_code }
    end
    local town = player:get_scene()
    local house = town:get_building(house_id)
    --房间号检查
    if not house:check_house_pos(house_pos) then
        return FRAME_SUCCESS, { error_code = HOUSE_POS_NOT_EMPTY }
    end
    --目标位置上的npc
    local tar_npc_id = house:get_partner_id(house_pos)
    if tar_npc_id then
        log_debug("[BuildingServlet][on_checkin_req] house_pos:{} has npc:{}", house_id, tar_npc_id)
        local tar_npc = player:get_partner(tar_npc_id)
        --是否派驻中
        if tar_npc:get_building_id() and tar_npc:get_building_id() ~= 0 then
            return FRAME_SUCCESS, { error_code = PARTNER_BUSY }
        end
        house:check_out(tar_npc)
        player:calc_sat(tar_npc)
        player:update_partner(tar_npc_id, tar_npc)
    end
    --伙伴检查
    local check_partner = player:get_partner(partner_id)
    local need_hunman = true
    --是否已入住
    local ohouse_id = check_partner:get_house_id()
    if ohouse_id and ohouse_id ~= 0 then
        local src_house = town:get_building(ohouse_id)
        src_house:check_out(check_partner)
        need_hunman = false
    end
    --入住民居
    house:check_in(check_partner, house_pos)
    player:update_partner(check_partner:get_id(), check_partner)
    if need_hunman then
        town:add_human(1)
        player:calc_sat(check_partner, true)
        -- town:add_check_sat(player, check_partner)
    end
    --入住事件
    player:notify_event("on_npc_checkin", check_partner:get_proto_id(), house:get_proto_id())
    event_mgr:notify_trigger("on_sync_sat", player, true)
    return FRAME_SUCCESS, { error_code = 0 }
end

--修改民居名称
function BuildingServlet:on_housename_req(player, player_id, message)
    log_debug("[BuildingServlet][on_housename_req] player_id:{} message:{}", player_id, message)
    local house_id, new_name = message.house_id, message.house_name
    --参数检查
    local check_code = self:check_house(player, { house_id })
    if check_code ~= FRAME_SUCCESS then
        return FRAME_SUCCESS, { error_code = check_code }
    end
    local town = player:get_scene()
    local house = town:get_building(house_id)
    --是否一致
    if house:get_house().house_name == new_name then
        return FRAME_SUCCESS, { error_code = FRAME_PARAMS }
    end
    --长度限制
    local name_length = u8len(new_name)
    if name_length == 0 or name_length > 16 then
        return FRAME_SUCCESS, { error_code = HOUSE_NAME_ERR }
    end
    --TODO 敏感字检测
    house:change_name(new_name)
    house:sync_changed()
    return FRAME_SUCCESS, { error_code = 0 }
end

--取消入住
function BuildingServlet:on_checkout_req(player, player_id, message)
    log_debug("[BuildingServlet][on_checkout_req] player_id:{} message:{}", player_id, message)
    local partner_id = message.partner_id
    local partner = player:get_partner(partner_id)
    if not partner:get_house_id() or partner:get_house_id() == 0 then
        return FRAME_SUCCESS, { error_code = HOUSE_NO }
    end
    --是否派驻中
    if partner:get_building_id() and partner:get_building_id() ~= 0 then
        return FRAME_SUCCESS, { error_code = PARTNER_BUSY }
    end

    local town = player:get_scene()
    local house = town:get_building(partner:get_house_id())
    house:check_out(partner)
    player:update_partner(partner_id, partner)
    town:add_human(-1)
    player:calc_sat(partner)
    event_mgr:notify_trigger("on_sync_sat", player, true)
    return FRAME_SUCCESS, { error_code = 0 }
end

--交换房间
function BuildingServlet:on_changehouse_req(player, player_id, message)
    log_debug("[BuildingServlet][on_changehouse_req] player_id:{} message:{}", player_id, message)
    local src_partner_id, tar_partner_id = message.src_partner_id, message.tar_partner_id
    --检查参数
    local src_partner = player:get_partner(src_partner_id)
    local dst_partner = player:get_partner(tar_partner_id)
    if not src_partner or not dst_partner then
        return FRAME_SUCCESS, { error_code = PARTNER_NOT_FOUND }
    end

    local src_house_id = src_partner:get_house_id()
    local dst_house_id = dst_partner:get_house_id()

    if src_house_id == nil or src_house_id == 0
        or
        dst_house_id == nil or dst_house_id == 0
    then
        return FRAME_SUCCESS, { error_code = HOUSE_NO }
    end
    local town = player:get_scene()
    --交换房间
    local dst_house_pos = dst_partner:get_house_pos()
    local src_house_pos = src_partner:get_house_pos()
    local dst_house = town:get_building(dst_house_id)
    local src_house = town:get_building(src_house_id)
    dst_house:check_out(dst_partner)
    src_house:check_out(src_partner)
    dst_house:check_in(src_partner, dst_house_pos)
    src_house:check_in(dst_partner, src_house_pos)

    player:update_partner(src_partner_id, src_partner)
    player:update_partner(tar_partner_id, dst_partner)
    return FRAME_SUCCESS, { error_code = 0 }
end

--内部方法
-------------------------------------------------------------------------
function BuildingServlet:check_block(player, town, conf, block_id)
    local player_id = player:get_id()
    local ground_id = conf.ground_id
    if not town:is_ground_unveil(ground_id) then
        log_err("[BuildingServlet][on_block_unlock_req] {} ground({}) is unlock", player_id, ground_id)
        return GROUND_NOT_FINISH
    end
    if town:is_block_unlock(ground_id, block_id) then
        log_err("[BuildingServlet][on_block_unlock_req] {} block_id({}) is unlock", player_id, block_id)
        return BLOCK_ACTIVED
    end
    --等级
    if conf.level and player:get_level() < conf.level then
        return UNLOCK_LEVEL_ERR
    end
end

quanta.building_servlet = BuildingServlet()

return BuildingServlet
