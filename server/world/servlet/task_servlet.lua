--task_servlet.lua
local log_err           = logger.err
local log_info          = logger.info
local log_debug         = logger.debug
local tunpack           = table.unpack
local tarray            = qtable.array
local task_mgr          = quanta.get("task_mgr")
local event_mgr         = quanta.get("event_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")
local config_mgr        = quanta.get("config_mgr")

local item_db           = config_mgr:get_table("item")
local dialog_db         = config_mgr:init_table("dialog", "id")


local FRAME_SUCCESS     = protobuf_mgr:error_code("FRAME_SUCCESS")
local TASK_NOT_EXIST    = protobuf_mgr:error_code("TASK_IS_NOT_EXIST")
local TASK_NOT_ORDER    = protobuf_mgr:error_code("TASK_IS_NOT_ORDER")
local TASK_CANT_RECV    = protobuf_mgr:error_code("TASK_CANT_RECVIVE")
local TASK_CANT_SPEED   = protobuf_mgr:error_code("TASK_CANT_SPEED")
local TASK_CANT_REFRESH = protobuf_mgr:error_code("TASK_CANT_REFRESH")
local TASK_NOT_DONE     = protobuf_mgr:error_code("TASK_HAS_NOT_DONE")
local TASK_CANT_DELETE  = protobuf_mgr:error_code("TASK_CANT_DELETE")
local TASK_FINISHED     = protobuf_mgr:error_code("TASK_HAS_FINISHED")
local OCHAT_FAILED      = protobuf_mgr:error_code("TASK_OCHAT_FAILED")
local OCHAT_EXIST       = protobuf_mgr:error_code("TASK_OCHAT_EXIST")
local OCHAT_FINISH      = protobuf_mgr:error_code("TASK_OCHAT_FINISH")
local COST_NOT_ENOUGH   = protobuf_mgr:error_code("PACKET_COST_NOT_ENOUGH")

local TASK_TALK         = protobuf_mgr:enum("task_action", "TALK")
local TASK_TARGET       = protobuf_mgr:enum("task_action", "TARGET")
local TASK_LETTER       = protobuf_mgr:enum("task_action", "LETTER")
local TASK_CHEND        = protobuf_mgr:enum("task_action", "CHAT_END")

local COST_TASK         = protobuf_mgr:enum("cost_reason", "NID_COST_TASK")

local TALK_CONFIG_NOT_EXIST = protobuf_mgr:error_code("TALK_CONFIG_NOT_EXIST")
local NID_OBTAIN_NPCTALK   = protobuf_mgr:enum("obtain_reason", "NID_OBTAIN_NPCTALK")

local TASK_ACTION_LIST = {
    [TASK_TALK]     = "on_talkwith_npc",
    [TASK_LETTER]   = "on_letter_read",
    [TASK_TARGET]   = "on_virtual_target",
}

local TaskServlet = singleton()
function TaskServlet:__init()
    -- 事件监听
    event_mgr:add_trigger(self, "on_task_stop")
    event_mgr:add_trigger(self, "on_task_finished")
    event_mgr:add_trigger(self, "on_task_upgraded")
    event_mgr:add_trigger(self, "on_login_success")
    -- CS协议
    protobuf_mgr:register(self, "NID_TASK_TALKS_REQ", "on_talks_req")
    protobuf_mgr:register(self, "NID_TASK_SPEED_REQ", "on_speed_req")
    protobuf_mgr:register(self, "NID_TASK_DELETE_REQ", "on_delete_req")
    protobuf_mgr:register(self, "NID_TASK_FINISH_REQ", "on_finish_req")
    protobuf_mgr:register(self, "NID_TASK_ACTION_REQ", "on_action_req")
    protobuf_mgr:register(self, "NID_TASK_COMMIT_REQ", "on_commit_req")
    protobuf_mgr:register(self, "NID_TASK_RECEIVE_REQ", "on_receive_req")
    protobuf_mgr:register(self, "NID_TASK_REFRESH_REQ", "on_refresh_req")

    protobuf_mgr:register(self, "NID_TASK_NPC_TALK_REQ", "on_npc_talk")
end

--事件监听
-------------------------------------------------------------------------
--登陆成功
function TaskServlet:on_login_success(player_id, player)
    --发送新手引导服务器保存数据
    local newbee_data = player:get_newbees()
    player:send("NID_UTILITY_NEWBEE_SYNC_NTF", { newbees = tarray(newbee_data) })
    --更新可接日常任务
    task_mgr:update_rand_dailytask(player, player:get_level())
    --同步任务
    player:sync_tasks()
end

--子任务更新
function TaskServlet:on_task_upgraded(player, task, next_id)
    local task_id = task:get_id()
    log_debug("[TaskServlet][on_task_upgraded] player({}) upgrade task({}-{})!", player:get_id(), task_id, next_id)
    if task_mgr:build_child_task(task, next_id) then
        task:update_target()
        player:sync_task(task)
    end
end

--任务完成
function TaskServlet:on_task_finished(player, task)
    player:notify_event("on_task_finished", task:get_id(), task:get_type())
end

--任务结束刷新
function TaskServlet:on_task_stop(player, task)
    local task_id = task:get_id()
    if task:is_loop_task() then
        --刷新循环任务
        task_mgr:refresh_loop_task(player, task_id, task:get_type())
        return
    end
end

--CS协议
-------------------------------------------------------------------------
---接收任务
function TaskServlet:on_receive_req(player, player_id, message)
    local task_id, task_type = message.task_id, message.task_type
    log_debug("[TaskServlet][on_receive_req] player({}) req receive task({}-{})!", player_id, task_id, task_type)
    --订单任务
    if task_mgr:is_order_task(task_type) then
        local task = player:get_task(task_id)
        if not task or not task:is_init() then
            log_err("[TaskServlet][on_receive_req] player({}) task({}) can't receive!", player_id, task_id)
            return TASK_CANT_RECV
        end
        task:active()
        return FRAME_SUCCESS, { error_code = 0}
    end
    --其他任务
    local task = player:get_accept_task(task_id)
    if not task then
        log_err("[TaskServlet][on_receive_req] player({}) task({}) can't receive!", player_id, task_id)
        return TASK_CANT_RECV
    end
    player:receive_task(task, task_id)
    log_info("[TaskServlet][on_receive_req] player({}) receive task({})!", player_id, task_id)
    return FRAME_SUCCESS, { error_code = 0}
end

--完成任务
function TaskServlet:on_finish_req(player, player_id, message)
    local task_id, npc_id = message.task_id, message.npc_id
    log_debug("[TaskServlet][on_finish_req] player({}) req finish task({}-{})!", player_id, task_id, npc_id)
    local task = player:get_task(task_id)
    if not task then
        log_err("[TaskServlet][on_finish_req] player({}) task({}) not exist!", player_id, task_id)
        return TASK_NOT_EXIST
    end
    if task:is_finished() then
        log_err("[TaskServlet][on_finish_req] player({}) task({}) is finished!", player_id, task_id)
        return TASK_FINISHED
    end
    if not task:is_reward() then
        log_err("[TaskServlet][on_finish_req] player({}) task({}) not done!", player_id, task_id)
        return TASK_NOT_DONE
    end
    if not task:cost_check() then
        log_err("[TaskServlet][on_finish_req] player({}) task({}) cost fail!", player_id, task_id)
        return TASK_NOT_DONE
    end
    task:finish()
    log_info("[TaskServlet][on_finish_req] player({}) finish task({})!", player_id, task_id)
    return FRAME_SUCCESS, { error_code = 0}
end

--删除任务
function TaskServlet:on_delete_req(player, player_id, message)
    local task_id = message.task_id
    log_debug("[TaskServlet][on_delete_req] player({}) req delete task({})!", player_id, task_id)
    local task = player:get_task(task_id)
    if not task then
        log_err("[TaskServlet][on_delete_req] player({}) task({}) not exist!", player_id, task_id)
        return TASK_NOT_EXIST
    end
    if not task:is_loop_task() then
        log_err("[TaskServlet][on_delete_req] player({}) task({}) cont delete!", player_id, task_id)
        return TASK_CANT_DELETE
    end
    task:giveup()
    log_info("[TaskServlet][on_delete_req] player({}) delete task({})!", player_id, task_id)
    return FRAME_SUCCESS, { error_code = 0}
end

--对话框操作
function TaskServlet:on_action_req(player, player_id, message)
    local action, args, params = message.action, message.args, message.params
    log_info("[TaskServlet][on_action_req] player({}) action({}) args({})!", player_id, action, args)
    local event = TASK_ACTION_LIST[action]
    if event then
        player:notify_event(event, args, action, params)
        return FRAME_SUCCESS, { error_code = 0}
    end
    if action <= TASK_CHEND then
        if not player:update_talk(args, action) then
            return FRAME_SUCCESS, { error_code = (action == TASK_CHEND) and OCHAT_FINISH or OCHAT_EXIST }
        end
    end
    return FRAME_SUCCESS, { error_code = 0}
end

--道具提交
function TaskServlet:on_commit_req(player, player_id, message)
    local items, task_id = message.items, message.task_id
    log_debug("[TaskServlet][on_commit_req] player({}) commit task({}) items({}) req!", player_id, task_id, items)
    local task = player:get_task(task_id)
    if not task then
        log_err("[TaskServlet][on_commit_req] player({}) task({}) not exist!", player_id, task_id)
        return TASK_NOT_EXIST
    end
    if not player:cost_items(items, COST_TASK) then
        return COST_NOT_ENOUGH
    end
    for item_id, count in pairs(items) do
        player:notify_event("on_item_commit", item_id, count)
    end
    log_debug("[TaskServlet][on_commit_req] player({}) commit task({}) items({}) success!", player_id, task_id, items)
    return FRAME_SUCCESS, { error_code = 0}
end

---加速订单任务
function TaskServlet:on_speed_req(player, player_id, message)
    local task_id = message.task_id
    log_debug("[TaskServlet][on_speed_req] player({}) req speed task({})!", player_id, task_id)
    local task = player:get_task(task_id)
    if not task or not task:is_loop_task() then
        log_err("[TaskServlet][on_speed_req] player({}) task({}) not exist or not order!", player_id, task_id)
        return TASK_NOT_ORDER
    end
    local costs = task_mgr:calc_speed_cost(player, task)
    if not costs then
        return TASK_CANT_SPEED
    end
    if not player:cost_items(costs, COST_TASK) then
        return COST_NOT_ENOUGH
    end
    task_mgr:refresh_loop_task(player, task:get_id(), task:get_type())
    log_debug("[TaskServlet][on_speed_req] player({}) speed task({}) success!", player_id, task_id)
    return FRAME_SUCCESS, { error_code = 0}
end

---刷新订单任务
function TaskServlet:on_refresh_req(player, player_id, message)
    local task_id = message.task_id
    log_debug("[TaskServlet][on_refresh_req] player({}) req refresh task({})!", player_id, task_id)
    local task = player:get_task(task_id)
    if not task or not task:is_loop_task() then
        log_err("[TaskServlet][on_refresh_req] player({}) task({}) not exist or not order!", player_id, task_id)
        return TASK_NOT_ORDER
    end
    local costs = task_mgr:calc_refresh_cost(player, task)
    if not costs then
        return TASK_CANT_REFRESH
    end
    if not player:cost_items(costs, COST_TASK) then
        return COST_NOT_ENOUGH
    end
    task:refresh()
    log_debug("[TaskServlet][on_refresh_req] player({}) refresh task({}) success!", player_id, task_id)
    return FRAME_SUCCESS, { error_code = 0}
end

function TaskServlet:on_talks_req(player, player_id, message)
    log_debug("[TaskServlet][on_talks_req] player({}) req all talks!", player_id)
    if player:load_talks() then
        return FRAME_SUCCESS, { error_code = 0, talks = player:get_talks() }
    end
    return FRAME_SUCCESS, { error_code = OCHAT_FAILED }
end

function TaskServlet:on_npc_talk(player, player_id, message)
    local talk_id = message.talk_id
    local conf = dialog_db:find_one(talk_id)
    if conf == nil then
        return FRAME_SUCCESS, { error_code = TALK_CONFIG_NOT_EXIST; }
    end
    if conf.args == nil then
        return FRAME_SUCCESS, { error_code = TALK_CONFIG_NOT_EXIST; }
    end

    for _, info in pairs(conf.args) do
        local npc_id, item_id, num = tunpack(info)
        local item_conf = item_db:find_one(item_id)
        if item_conf == nil or item_conf.base_affinity == nil then
            return FRAME_SUCCESS, { error_code = TALK_CONFIG_NOT_EXIST; }
        end
        player:allot_item(item_id, num, NID_OBTAIN_NPCTALK, npc_id)
    end
    return FRAME_SUCCESS, { error_code = 0}
end

quanta.task_servlet = TaskServlet()

return TaskServlet

