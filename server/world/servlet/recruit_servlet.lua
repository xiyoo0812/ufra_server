--recruit_servlet.lua
local log_err                   = logger.err
local log_debug                 = logger.debug
local qfailed                   = quanta.failed
local tinsert                   = table.insert

local protobuf_mgr              = quanta.get("protobuf_mgr")
local recruit_mgr               = quanta.get("recruit_mgr")
local event_mgr                 = quanta.get("event_mgr")

local FRAME_SUCCESS             = protobuf_mgr:error_code("FRAME_SUCCESS")
local FRAME_PARAMS              = protobuf_mgr:error_code("FRAME_PARAMS")
local COST_RECR                 = protobuf_mgr:enum("cost_reason", "NID_COST_RECR")
local OBTAIN_RECRUIT            = protobuf_mgr:enum("obtain_reason", "NID_OBTAIN_RECRUIT")

-- npc招募服务
local RecruitServlet = singleton()
function RecruitServlet:__init()
    self:setup()
end

function RecruitServlet:setup()
    -- CS协议
    protobuf_mgr:register(self, "NID_RECR_START_REQ",       "on_start_req")
    protobuf_mgr:register(self, "NID_RECR_REC_REQ",         "on_rec_req")
end

--CS协议
-----------------------------------------------------

--请求招募历史记录
function RecruitServlet:on_rec_req(player, player_id, message)
    log_debug("[RecruitServlet][on_rec_req] player_id:{}, message:{}", player_id, message)
    local page_num = message.page_num
    player:load_records()
    local total_page = player:get_total_page()
    local result = player:get_record(page_num, total_page)
    if not result then
        return FRAME_SUCCESS, {error_code = FRAME_PARAMS}
    end
    local records = {}
    for _, r in pairs(result or {}) do
        tinsert(records, r:pack2client())
    end
    log_debug("[RecruitServlet][on_rec_req] player_id:{}, records:{}", player_id, records)
    return FRAME_SUCCESS, {error_code = 0, page_num = page_num, total_num = total_page, records = records}
end

-- 开始招募
function RecruitServlet:on_start_req(player, player_id, message)
    log_debug("[RecruitServlet][on_start_req] player_id:{}, message:{} ", player_id, message)
    local pool_id, is_ten, is_free = message.pool_id, message.ten, message.free
    --是否免费招募
    if is_free then
        return self:free_recr(player, pool_id)
    end
    --付费招募
    return self:cost_recr(player, pool_id, is_ten, is_free)
end

--免费招募
function RecruitServlet:free_recr(player, pool_id)
    local check_code = player:check_free_recr(pool_id)
    if FRAME_SUCCESS ~= check_code then
        return FRAME_SUCCESS, {error_code = check_code}
    end
    --记录免费次数
    player:save_p_free_time_field(pool_id, quanta.now)
    player:save_p_free_cnt_field(pool_id, (player:get_p_free_cnt()[pool_id] or 0) +1 )
    local results = self:recr(player, pool_id, 1)

    --同步招募池信息
    player:sync_recr_cnt(pool_id, false, true)
    event_mgr:notify_trigger("on_recr_result", player, pool_id, 0, 1, player:get_p_used_cnt()[pool_id] or 0,
    {}, results)
    return FRAME_SUCCESS, {error_code = 0, items = results}
end

--付费招募
function RecruitServlet:cost_recr(player, pool_id, is_ten, is_free)
    --招募次数
    local use_cnt = 1
    if is_ten then
        use_cnt = 10
    end

    --招募条件是否满足
    local check_code = player:check_recr(pool_id, use_cnt)
    if FRAME_SUCCESS ~= check_code then
        return FRAME_SUCCESS, {error_code = check_code}
    end

    local pool = recruit_mgr:get_pool(pool_id)
    local pool_conf = pool:get_proto()
    --消耗是否足够
    local cost_item = pool_conf.cost_item
    local cost_num = 1
    if is_ten then
        cost_num = 10
        local p_ten_cnt = player:get_ten_cnt(pool_id)
        --是否在十连优惠范围内
        if p_ten_cnt < pool_conf.cnt_ten then
            cost_num = pool_conf.dis_ten
        end
    end
    --扣除消耗
    local costs = { cost = {[cost_item] = cost_num}, reason = COST_RECR }
    local code = player:execute_cost(costs)
    if qfailed(code) then
        log_err("[RecruitServlet][cost_recr] player({}) drop cost failed: {}!", player:get_id(), code)
        return FRAME_SUCCESS, { error_code = code }
    end
    --开始招募
    local results = self:recr(player, pool_id, use_cnt)
    player:sync_recr_cnt(pool_id, is_ten, is_free)
    log_debug("[RecruitServlet][cost_recr] player({}) results: {}!", player:get_id(), results)

    event_mgr:notify_trigger("on_recr_result", player, pool_id, 1, use_cnt, player:get_p_used_cnt()[pool_id] or 0,
                costs, results)

    return FRAME_SUCCESS, { error_code = 0, items = results }
end

--招募逻辑
function RecruitServlet:recr(player, pool_id, use_cnt)
    local res_items = player:recruit(pool_id, use_cnt)
    local results = {}
    local pack_items = {}
    for _, item in pairs(res_items) do
        local recr_item = {}
        recr_item.proto_type = item.item_type
        recr_item.proto_id = item.item_id
        recr_item.rule = item.rule
        --npc
        if item.item_type == 1 then
            --是否重复获得
            local rep_npc = true
            local npc_proto_id = item.item_id
            local partner = player:recv_partner(npc_proto_id, OBTAIN_RECRUIT)
            if partner then
                rep_npc = false
                recr_item.uuid =  partner:get_id()
                player:notify_event("on_npc_recruit", npc_proto_id)
            end
            recr_item.rep_npc = rep_npc
        else
            --道具
            pack_items[item.item_id] = (pack_items[item.item_id] or 0) + 1
            recr_item.rep_npc = false
        end

        tinsert(results, recr_item)
    end

    --道具类物品发放
    if next(pack_items) then
        local drops = { drop = pack_items, reason = OBTAIN_RECRUIT }
        local code = player:execute_drop(drops, true)
        if qfailed(code) then
            log_err("[RecruitServlet][recr] player_id:{} drop item:{} failed! code:{}", player:get_id(), pack_items, code)
            return results
        end
    end

    return results
end

quanta.recruit_servlet = RecruitServlet()

return RecruitServlet