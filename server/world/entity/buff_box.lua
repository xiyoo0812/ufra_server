--buff_box.lua
local log_err       = logger.err
local log_debug     = logger.debug
local tinsert       = table.insert

local object_fty    = quanta.get("object_fty")

local COVER         = quanta.enum("OverlapType", "COVER")
local SHARE         = quanta.enum("OverlapType", "SHARE")
local TIME          = quanta.enum("OverlapType", "TIME")

local BuffBox = mixin()
local prop = property(BuffBox)
prop:reader("all_buffs", {})    --buffs

function BuffBox:__init()
end

-- 获取buff
function BuffBox:get_buff(uuid)
    return self.all_buffs[uuid]
end

-- 添加buff
function BuffBox:add_buff(buff_id)
    log_debug("[BuffBox][add_buff]->player_id:{}, buff_id:{}", self.id, buff_id)
    --添加列表
    local buff, uuid = object_fty:create_buff(buff_id)
    if not buff then
        log_err("[BuffBox][add_buff]->player_id:{}, buff_id:{} create failed", self.id, buff_id)
        return false
    end
    --检查互斥
    local conf = buff:get_prototype()
    if not self:check_mutex_buff(conf) then
        log_err("[BuffBox][add_buff] mutex check failed, player_id:{}, buff_id:{}", self.id, buff_id)
        return false
    end
    --检查叠加
    if not self:check_overlap_buff(conf) then
        log_err("[BuffBox][add_buff] overlap check failed, player_id:{}, buff_id:{}", self.id, buff_id)
        return false
    end
    buff:active(self)
    if buff:is_store() then
        self:on_buff_add(uuid, buff)
    end
    self.all_buffs[uuid] = buff
    --同步数据
    self:sync_buff(buff)
    return true, uuid
end

-- 覆盖
function BuffBox:overlap_buff(buff, conf)
    buff:overlap_time(conf)
    self:sync_buff(buff)
end

-- 移除buff
function BuffBox:remove_buff(uuid)
    local buff = self.all_buffs[uuid]
    if buff then
        buff:stop(self)
        if buff:is_store() then
            self:on_buff_remove(uuid)
        end
        self.all_buffs[uuid] = nil
        --通知客户端
        self:sync_buff(buff)
        log_debug("[BuffBox][remove_buff]->player_id:{}, buff_id:{}", self.id, buff:get_prototype().id)
    end
end


-- 检查互斥buff
function BuffBox:check_mutex_buff(conf)
    if conf.mutex_group == 0 then
        return true
    end
    for id, buff in pairs(self.all_buffs) do
        if buff:get_mutex_group() == conf.mutex_group then
            if conf.mutex_priority < buff:get_mutex_priority() then
                --新buff不是最高优先级
                return false
            else
                self:remove_buff(id)
                return true
            end
        end
    end
    return true
end

-- 检查叠加buff
function BuffBox:check_overlap_buff(conf)
    local overlap_type  = conf.overlap_type
    for id, buff in pairs(self.all_buffs) do
        if buff:get_overlap_group() == conf.overlap_group then
            if overlap_type == COVER then
                self:remove_buff(id)
                return true
            elseif overlap_type == SHARE then
                return true
            elseif overlap_type == TIME then
                if buff:get_layer() >= conf.overlap_layer then
                    return false
                end
                self:overlap_buff(buff, conf)
                return false
            else
                return false
            end
        end
    end
    return true
end

function BuffBox:_update(now)
    self:update_buff(now)
end

--更新
function BuffBox:update_buff(now)
    local dead_buffs = {}
    local now_ms = quanta.now_ms
    for buff_id, buff in pairs(self.all_buffs) do
        buff:run(self, now_ms)
        if buff:is_expire(now) then
            tinsert(dead_buffs, buff_id)
        end
    end
    for _, buff_id in pairs(dead_buffs) do
        self:remove_buff(buff_id)
    end
end

function BuffBox:on_buff_add(uuid, buff)
end

function BuffBox:on_buff_remove(uuid)
end

function BuffBox:sync_buff(buff)
end

return BuffBox
