--skill.lua
local slower            = string.lower
local mfloor            = math.floor
local mmax              = math.max
-- local log_debug         = logger.debug

local MELEE_SAFE_TIME   = mfloor(1 / 6 * 1000) -- 安全时间-近战默认最小CD(单位: ms)

---@type geometry
local geometry          = import('world/skill/geometry.lua')

local timer_mgr         = quanta.get("timer_mgr")
local event_mgr         = quanta.get("event_mgr")

---@class Skill
local Skill = class()
local prop = property(Skill)
prop:reader("id", 0)          -- id
prop:reader("skill_id", 0)    -- 技能id(配置)
prop:reader("prototype", nil) -- prototype
prop:reader("equip_attr", 0) -- 所属装备Attr
prop:reader("equip_id", 0) -- 所属装备Id
prop:reader("timer_id", 0) -- 技能定时器Id
prop:reader("owner_entity", nil) -- 所属Entity
prop:reader("skill_type", nil) -- (空手状态下区分)技能(组)类型
prop:accessor("start_ms", 0)  -- 开始释放时间(ms)

function Skill:__init(uid, conf, equip_attr, equip_id, owner_entity, skill_type)
    self.id = uid
    self.skill_id = conf.id
    self.prototype = conf
    self.equip_attr = equip_attr
    self.equip_id = equip_id
    self.owner_entity = owner_entity
    self.skill_type = skill_type
end

---技能CD
---@return number
function Skill:get_cd()
    return self.prototype.cd
end

---检测技能CD
---@return boolean 是否可以释放技能
function Skill:check_cd(is_ai)
    -- return true -- 暂时禁用cd限制
    local diff_ms = quanta.now_ms - self:get_start_ms()
    -- 安全时间校验
    if diff_ms < MELEE_SAFE_TIME then
        return false
    end
    -- cd校验
    local cd = self:get_cd()
    if is_ai then
        cd = mmax(cd, self:get_duration())
    end
    return diff_ms > cd * 1000
end

---技能是否可被打断
---@return boolean
function Skill:is_can_break()
    return self.prototype.can_break
end

---技能时长
---@return number
function Skill:get_duration()
    return self.prototype.duration
end

function Skill:get_check_target_param_by_index(check_index)
    return (self.prototype["check_target_param"] or {})[check_index]
end

---技能目标检测参数
function Skill:get_check_target_param(check_index, ...)
    local value = self:get_check_target_param_by_index(check_index)
    if not value then
        return nil
    end
    local args = {...}
    if value ~= nil and #args > 0 then
        for i = 1, #args, 1 do
            value = value[args[i]]
        end
    end
    return value
end

function Skill:get_damage_param_by_id(param_id)
    for _, param in ipairs(self.prototype["damage_param"] or {}) do
        if param.data.target_event_id == param_id then
            return param
        end
    end
    return nil
end

---技能伤害参数
function Skill:get_damage_param(param_id, ...)
    local value = self:get_damage_param_by_id(param_id)
    if not value then
        return nil
    end
    local args = {...}
    if value ~= nil and #args > 0 then
        for _, argi in ipairs(args) do
            value = value[argi]
        end
    end
    return value
end

---技能伤害倍率
function Skill:get_damage_ratio(param_id)
    return self:get_damage_param(param_id, "data", "dmg_ratio")
end

---技能削韧值
function Skill:get_toughness_cutdown(param_id)
    return self:get_damage_param(param_id, "data", "toughness_cutdown")
end

---技能受击类型
function Skill:get_be_hit_type(param_id)
    return self:get_damage_param(param_id, "data", "be_hit_type")
end

---技能韧性(霸体)信息
function Skill:get_toughness_param()
    return self.prototype["toughness_param"] or {}
end

---技能伤害id参数检查
function Skill:check_damage_param_id(param_id)
    local param = self:get_damage_param_by_id(param_id)
    return param ~= nil
end

---是否需要(消耗)SP
function Skill:is_need_sp()
    return self.prototype.cost_sp == 1
end

--检测技能SP
function Skill:check_sp(sp_max, sp)
    if self:is_need_sp() then
        return sp >= sp_max
    end
    return true
end

function Skill:check_complete_use()
    -- 完成检测的计时器
    local diff_ms = quanta.now_ms - self:get_start_ms()
    local end_time = 1000 * self:get_duration() - diff_ms
    if end_time > 0 then
        self.timer_id = timer_mgr:once(end_time, function()
            self:notify_complete_use()
        end)
    else
        self:notify_complete_use()
    end
end

function Skill:check_next_target_param(check_index)
    -- 目标检测的计时器
    local check_time = self:get_check_target_param(check_index, "time")
    if not check_time then
        self:check_complete_use()
        return
    end
    local start_ms = self:get_start_ms()
    local diff_ms = 0
    if start_ms > 0 then
        diff_ms = quanta.now_ms - start_ms
    end
    local end_time = 1000 * check_time - diff_ms
    if end_time > 0 then
        self.timer_id = timer_mgr:once(end_time, function()
            event_mgr:notify_trigger("on_skill_target_check", self, check_index)
        end)
    else
        event_mgr:notify_trigger("on_skill_target_check", self, check_index)
    end
end

---开始
function Skill:start_use()
    local check_index = 1
    self:set_start_ms(0)
    self:check_next_target_param(check_index)
    event_mgr:notify_trigger("on_skill_start_use", self, check_index)
end

function Skill:notify_complete_use()
    -- log_debug('[Skill]notify_complete_use() skill_id={}', self.skill_id)
    --通知skill模块
    self.timer_id = nil
    event_mgr:notify_trigger("on_skill_completed", self)
    -- 启动技能可用的计时器
    local diff_ms = quanta.now_ms - self:get_start_ms()
    local cd_ms = 1000 * mmax(self:get_cd(), self:get_duration())
    local end_time = cd_ms - diff_ms
    if end_time <= 0 then
        end_time = MELEE_SAFE_TIME
    end
    self.timer_id = timer_mgr:once(end_time, function()
        self:notify_available_use()
    end)
end

---打断
function Skill:break_use()
    -- log_debug('[Skill]break_use() skill_id={}', self.skill_id)
    -- 打断技能
    if self.timer_id then
        timer_mgr:unregister(self.timer_id)
        self.timer_id = nil
        -- 启动技能可用的计时器
        local diff_ms = quanta.now_ms - self:get_start_ms()
        local cd_ms = 1000 * mmax(self:get_cd(), self:get_duration())
        local end_time = cd_ms - diff_ms
        if end_time > 0 then
            self.timer_id = timer_mgr:once(end_time + MELEE_SAFE_TIME, function()
                self:notify_available_use()
            end)
        end
    end
end

function Skill:notify_available_use()
    -- log_debug('[Skill]notify_available_use() skill_id={}', self.skill_id)
    --通知skill模块
    self.timer_id = nil
    self:set_start_ms(0)
    event_mgr:notify_trigger("on_skill_available", self)
end

function Skill:terminate()
    if self.timer_id then
        timer_mgr:unregister(self.timer_id)
        self.timer_id = nil
    end
end

---技能检测目标
function Skill:check_target(target, check_index)
    local region_type = self:get_check_target_param(check_index, "data", "region_type")
    region_type = slower(region_type)
    local region_param = self:get_check_target_param(check_index, "data", "region_param")

    local target_world_pos = {
        x = target:get_pos_x(),
        y = target:get_pos_y() + 84, -- 身高的一半(假设身高为1.68m=168cm)
        z = target:get_pos_z(),
    }
    local entity = self.owner_entity
    local my_world_pos = {
        x = entity:get_pos_x(),
        y = entity:get_pos_y(),
        z = entity:get_pos_z(),
    }
    local my_world_rot_y = (entity:get_dir_y() / 100) * -1 -- client上报的朝向欧拉角是乘了100后的结果，计算时需要除以100，并转换左右说坐标系(乘以-1)
    if region_type == "sphere" then
        return geometry.is_point_inside_sphere(
            target_world_pos, my_world_pos, my_world_rot_y,
            geometry.m2cm(region_param["center"]), geometry.m2cm(region_param["radius"])
        )
    elseif region_type == "cube" then
        return geometry.is_point_inside_cube(
            target_world_pos, my_world_pos, my_world_rot_y,
            geometry.m2cm(region_param["center"]), geometry.m2cm(region_param["size"])
        )
    elseif region_type == "capsule" then
        return geometry.is_point_inside_capsule(
            target_world_pos, my_world_pos, my_world_rot_y,
            geometry.m2cm(region_param["center1"]), geometry.m2cm(region_param["center2"]), geometry.m2cm(region_param["radius"])
        )
    end
    return false
end

return Skill
