--character.lua
import("agent/mongo_agent.lua")
local log_debug     = logger.debug
local sformat       = string.format
local store_mgr     = quanta.get("store_mgr")
local Character = class()
local prop = property(Character)
prop:reader("uni_id", "")       --联合

local dprop = db_property(Character, "character_world", true)
dprop:store_value("world_id", 0)        --世界ID
dprop:store_value("character_id", 0)    --选角ID
dprop:store_value("role_id", 0)         --角色ID

function Character:__init(character_id, world_id)
    self.uni_id = sformat("%s_%s", character_id, world_id)
end

function Character:load()
    return store_mgr:load(self, self.uni_id, "character_world")
end

function Character:is_newbee()
    log_debug("[Character][is_newbee] role_id:{}", self.role_id)
    return self.role_id == 0
end

function Character:on_db_character_world_load(data)
    if data.uni_id then
        self.world_id = data.world_id
        self.character_id = data.character_id
        self.role_id = data.role_id
    end
end

function Character:create(character_id, world_id, role_id)
    self.character_id = character_id
    self.world_id = world_id
    self.role_id = role_id
    self.create_time = quanta.now
    self:flush_character_world_db(true)
    return true
end

function Character:get_autoinc_id(character_id, world_id)
    return store_mgr:get_autoinc_id()
end

return Character
