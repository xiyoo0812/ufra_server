--npc.lua
local log_err          = logger.err
local mfloor           = math.floor

local config_mgr       = quanta.get("config_mgr")
local protobuf_mgr     = quanta.get("protobuf_mgr")
local player_mgr       = quanta.get("player_mgr")
local recruit_mgr      = quanta.get("recruit_mgr")

local attr_db          = config_mgr:init_table("npc_attr", "key")
local npc_lv_db        = config_mgr:init_table("npc_lv", "id")
local npc_star_db      = config_mgr:init_table("npc_star", "id")

local ENTITY_NPC       = protobuf_mgr:enum("entity_type", "ENTITY_NPC")
local STATE_NORMAL     = quanta.enum("CombatState", "NORMAL")
local F_NPC            = quanta.enum("Faction", "NPC")
local F_FOLLOWER       = quanta.enum("Faction", "FOLLOWER")

local Entity           = import("business/entity/entity.lua")
local AoiComponent     = import("world/component/aoi_component.lua")
local BuffComponent    = import("world/component/buff_component.lua")
local SkillComponent   = import("world/component/skill_component.lua")
local BattleComponent  = import("world/component/battle_component.lua")
local AiAttentComponent= import("world/component/ai_attent_component.lua")
local ActionComponent  = import("world/component/action_component.lua")

local NPC = class(Entity, AoiComponent, BuffComponent, SkillComponent, BattleComponent, AiAttentComponent, ActionComponent)
local prop = property(NPC)
prop:reader("conf", nil)            --配置
prop:reader("type", ENTITY_NPC)     --type
prop:accessor("scene", nil)         --scene
prop:accessor("refresh_time", 0)    --刷新时间
prop:accessor("default_skills", {}) --默认技能
prop:accessor("refresh_limit", nil) --刷新次数上限
prop:accessor("star_level", 0)      --星级
prop:accessor("follower_conf", nil) --随从配置

function NPC:__init(id)
end

function NPC:is_npc()
    return true
end

function NPC:is_follower()
    return self:get_faction() == F_FOLLOWER
end

function NPC:is_owned_by(player_id)
    return self:is_follower() and self:get_owner_id() == player_id
end

function NPC:get_owner()
    local owner_id = self.owner_id
    if not (self:is_follower() and owner_id) then
        return nil
    end
    return player_mgr:get_entity(owner_id)
end

-- 改变NPC的星级或等级
-- 为 “将Partner转为随从NPC” 功能预留
function NPC:change_star_level(level, star)
    -- 星级或等级改变后，重新加载数据
    self:load_lv_conf(level or self:get_set_level(), star or self:get_star_level())
end

--load
function NPC:load(conf)
    self.conf = conf
    self:init_attrset(attr_db)
    self:set_proto_id(conf.id)
    self:set_speed(conf.speed)
    self:load_lv_conf(conf.init_lv, conf.star_lv)
    self:set_default_skills(conf.default_skills)
    local faction = (self.follower_conf or {}).faction or conf.faction or F_NPC
    self:set_faction(faction)
    self:set_combat_state(STATE_NORMAL)
    --韧性相关属性
    self:load_toughness_conf(conf)
    if conf.hit_action and next(conf.hit_action) then
        self:init_hit_actions(conf.hit_action)
    end
    self:load_sp_conf(conf)
    return true
end

function NPC:get_star_config(npc_group_id, star_lv)
    local star_id = recruit_mgr:get_star_id(npc_group_id, star_lv)
    local conf = npc_star_db:find_one(star_id)
    if not conf then
        log_err("[NPC][get_star_config] conf(npc_group_id={}, star_lv={}) with star level not exist!", npc_group_id, star_lv)
    end
    return conf
end

function NPC:load_lv_conf(init_lv, star_lv)
    if not init_lv then
        return
    end
    local lv_conf = npc_lv_db:find_one(init_lv)
    if not lv_conf then
        return
    end
    self:set_level(lv_conf.level)
    self:set_name(lv_conf.name)
    local star_conf = self:get_star_config(lv_conf.npc_group_id, star_lv)
    if not star_conf then
        return
    end
    self:set_star_level(star_lv)
    local hp_max = mfloor(star_conf.hp * lv_conf.hp)
    self:set_hp_max(hp_max)
    self:set_hp(hp_max)
    self:load_fight_conf(lv_conf, star_conf)
end

function NPC:load_fight_conf(lv_conf, star_conf)
    if self.conf.fight and lv_conf and star_conf then
        local attack_max = mfloor(lv_conf.attack * star_conf.attack)
        self:set_attack_max(attack_max)
        self:set_attack(attack_max)
        self:set_defence(mfloor(lv_conf.defence * star_conf.defence))
        self:set_critical_rate(mfloor(lv_conf.critical_rate * star_conf.critical_rate))
        self:set_critical_hurt(mfloor(lv_conf.critical_hurt * star_conf.critical_hurt))
    end
end

function NPC:load_toughness_conf(conf)
    if conf.max_toughness and conf.anti_toughness and conf.recover_toughness and conf.toughness_wait and conf.toughness_fracture then
        self:set_toughness(conf.max_toughness)
        self:set_toughness_max(conf.max_toughness)
        self:set_anti_toughness(conf.anti_toughness)
        self:set_recover_toughness(conf.recover_toughness)
        self:set_toughness_wait(conf.toughness_wait)
        self:set_toughness_fracture(conf.toughness_fracture)
    end
end

function NPC:load_sp_conf(conf)
    if conf.sp_max and conf.sp_hit and conf.sp_attack and conf.sp_rate then
        self:set_sp_max(conf.sp_max)
        self:set_sp_hit(conf.sp_hit)
        self:set_sp_attack(conf.sp_attack)
        self:set_sp_rate(conf.sp_rate)
        self:set_sp(0)
    end
end

--refresh
function NPC:refresh(now)
    return false
end

return NPC
