--monster.lua
local log_warn      = logger.warn

local ai_mgr        = quanta.get("ai_mgr")
local object_fty    = quanta.get("object_fty")
local config_mgr    = quanta.get("config_mgr")
local protobuf_mgr  = quanta.get("protobuf_mgr")
local event_mgr     = quanta.get("event_mgr")

local attr_db       = config_mgr:init_table("monster_attr", "key")

local ENT_MONSTER   = protobuf_mgr:enum("entity_type", "ENTITY_MONSTER")
local STATE_NORMAL  = quanta.enum("CombatState", "NORMAL")
local FP_MONSTER    = quanta.enum("Faction", "PASSIVE_MONSTER")

local Entity        = import("business/entity/entity.lua")
local AoiComponent  = import("world/component/aoi_component.lua")
local BuffComponent = import("world/component/buff_component.lua")
local SkillComponent    = import("world/component/skill_component.lua")
local BattleComponent   = import("world/component/battle_component.lua")
local AiComponent   = import("world/component/ai_component.lua")
local AiGoapComponent   = import("world/component/ai_goap_component.lua")
local AiHateComponent   = import("world/component/ai_hate_component.lua")
local AiNavComponent    = import("world/component/ai_nav_component.lua")
local AiSkillComponent  = import("world/component/ai_skill_component.lua")
local AiStandoffComponent = import("world/component/ai_standoff_component.lua")
local AiDebugComponent  = import("world/component/ai_debug_component.lua")

local Monster = class(Entity,
    AoiComponent, BuffComponent, SkillComponent, BattleComponent, AiComponent,
    AiGoapComponent, AiHateComponent, AiNavComponent, AiSkillComponent, AiStandoffComponent,
    AiDebugComponent
)
local prop = property(Monster)
prop:reader("conf", nil)            --配置
prop:reader("type", ENT_MONSTER)    --type
prop:accessor("scene", nil)         --scene
prop:accessor("refresh_time", 0)    --刷新时间
prop:accessor("default_skills", {}) --默认技能
prop:accessor("birth_time", 0)      --怪物实例化时间(单位:ms)
prop:accessor("refresh_limit", nil) --刷新次数上限
prop:accessor("monster_type", nil)  --怪物类型
prop:accessor("sow_conf", nil)      --刷怪配置

function Monster:is_monster()
    return true
end

--load
function Monster:load(conf)
    self:init_attrset(attr_db)
    self.conf = conf
    self:set_proto_id(conf.id)
    self:set_name(conf.name)
    self:set_level(conf.level)
    self:set_default_skills(conf.default_skills)
    self:set_birth_time(quanta.now_ms)
    self:set_speed(conf.speed)
    self:update_growth_curve()
    local faction = (self.sow_conf or {}).faction or conf.faction or FP_MONSTER
    self:set_faction(faction)
    self:set_faction(conf.faction or FP_MONSTER)
    self:set_combat_state(STATE_NORMAL)
    self:set_monster_type(conf.type)
    --韧性相关属性
    if conf.max_toughness and conf.anti_toughness and conf.recover_toughness and conf.toughness_wait and conf.toughness_fracture then
        self:set_toughness(conf.max_toughness)
        self:set_toughness_max(conf.max_toughness)
        self:set_anti_toughness(conf.anti_toughness)
        self:set_recover_toughness(conf.recover_toughness)
        self:set_toughness_wait(conf.toughness_wait)
        self:set_toughness_fracture(conf.toughness_fracture)
    end
    if conf.hit_action and next(conf.hit_action) then
       self:init_hit_actions(conf.hit_action)
    end
    return true
end

---更新成长曲线
function Monster:update_growth_curve()
    local conf = self.conf
    local level = self:get_level()

    local hp = object_fty:growth_curve_value(conf.hp, level)
    self:set_hp(hp)
    self:set_hp_max(hp)

    local attack = object_fty:growth_curve_value(conf.attack, level)
    self:set_attack(attack)
    self:set_attack_max(attack)

    local defence = object_fty:growth_curve_value(conf.defence, level)
    self:set_defence(defence)

    local critical_rate = object_fty:growth_curve_value(conf.critical_rate, level)
    self:set_critical_rate(critical_rate)

    local critical_hurt = object_fty:growth_curve_value(conf.critical_hurt, level)
    self:set_critical_hurt(critical_hurt)
end

--refresh
function Monster:refresh(now)
    local diff_time = now - self.active_time
    if diff_time > self.refresh_time then
        self:set_release(false)
        self:set_birth_time(quanta.now_ms)
        self:update_growth_curve()
        -- 刷新位置重置
        local spawn_pos = self:get_spawn_pos()
        self:set_pos_x(spawn_pos.x)
        self:set_pos_y(spawn_pos.y)
        self:set_pos_z(spawn_pos.z)
        -- 战斗状态重置
        self:set_combat_state(STATE_NORMAL)
        -- ai服重新分配
        event_mgr:fire_frame(function()
            ai_mgr:assign_aisvr(self)
        end)
        return true
    end
    return false
end

---@param attacker Player|SkillComponent 攻击者
function Monster:dead(attacker, scene_id)
    self.active_time = quanta.now
    self:set_release(true)
    self:invoke("_dead")
    log_warn('[Monster][dead]: id=%s', self:get_id())
    local scene = self:get_scene()
    -- 怪物死亡通知(掉落)
    if attacker then
        local attacker_id = attacker:get_id()
        local collaborator_ids = self.hate_list:keys()
        event_mgr:fire_frame(function()
            local send_id = nil
            if attacker:is_npc() and attacker:is_follower() then
                -- 随从NPC行为通知由主人发送
                local player  = attacker:get_owner()
                if player then
                    send_id = player:get_id()
                    self:notify_killed(scene, player, attacker_id)
                else
                    log_warn('[Monster][dead] NPC(attacker_id: {}) owner(player_id={}) not found', attacker_id, attacker:get_owner_id())
                end
            elseif attacker:is_player() then
                send_id = attacker_id
                self:notify_killed(scene, attacker, attacker_id)
            end
            -- 协同杀怪的player发掉落奖励
            for _, collaborator_id in ipairs(collaborator_ids) do
                if scene and collaborator_id ~= attacker_id and collaborator_id ~= send_id then
                    local collaborator = scene:get_entity(collaborator_id)
                    self:notify_killed(scene, collaborator, collaborator_id)
                end
            end
        end)
        if attacker:is_player() then
            event_mgr:notify_trigger("on_battle_end", attacker, 1, self:get_proto_id())
        end
    end
    if scene then
        scene_id = scene:get_id()
    end
    -- 通知AI服
    ai_mgr:on_disassociated_to_aisvr(self, scene_id)
end

function Monster:notify_killed(scene, entity, attacker_id)
    if not (entity and entity:is_player()) then
        return
    end
    local copy_config_id = nil
    local copy_id = nil
    if scene and scene:is_copy() then
        copy_config_id = scene:get_config_id()
        copy_id = scene:get_copy_id()
    end
    local copy_level = nil
    if copy_config_id ~= nil and copy_id ~= nil then
        copy_level = entity:get_copy_level(copy_id)
    end
    local proto_id = self:get_proto_id()
    local drop = self.conf.drop
    entity:notify_event("on_monster_killed", entity:get_id(), proto_id, drop, copy_config_id, copy_level)
    if copy_level then
        local sow_conf = self:get_sow_conf()
        -- 通知副本关卡怪物死亡
        scene:remove_level_monster(entity, copy_level, sow_conf.id)
    end
end

function Monster:live_time()
    return quanta.now_ms - self:get_birth_time()
end

return Monster
