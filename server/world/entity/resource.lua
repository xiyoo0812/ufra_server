--resource.lua
local mmax          = qmath.max
local mmin          = qmath.min
local in_range      = qmath.in_range
local tinsert       = table.insert
local log_debug     = logger.debug
local qfailed       = quanta.failed

local config_mgr    = quanta.get("config_mgr")
local protobuf_mgr  = quanta.get("protobuf_mgr")
local player_mgr    = quanta.get("player_mgr")

local equip_db      = config_mgr:init_table("equip", "id")
local resource_db   = config_mgr:init_table("resource", "id")
local attr_db       = config_mgr:init_table("resource_attr", "key")

local ENT_RESOURCE  = protobuf_mgr:enum("entity_type", "ENTITY_RESOURCE")
local COST_GATHER   = protobuf_mgr:enum("cost_reason", "NID_COST_GATHER")
local OBTAIN_GATHER = protobuf_mgr:enum("obtain_reason", "NID_OBTAIN_GATHER")

local FRAME_SUCCESS = protobuf_mgr:error_code("FRAME_SUCCESS")
local GATH_TOOL_ERR = protobuf_mgr:error_code("SKILL_GATHER_TOOL_ERR")
local GATH_TOOL_LVL = protobuf_mgr:error_code("SKILL_GATHER_TOOL_LVL")
local GATHER_EMPTY  = protobuf_mgr:error_code("SKILL_GATHER_EMPTY")

local Entity        = import("business/entity/entity.lua")
local AoiComponent  = import("world/component/aoi_component.lua")

local RESTO_HP      = quanta.enum("ResEftType", "HP")
local RESTO_COUNT   = quanta.enum("ResEftType", "COUNT")
local ATTR_POS      = quanta.enum("AttrID", "ATTR_HEAD") - 1
local RADIUS        = 500                     --默认采集半径

local Resource = class(Entity, AoiComponent)
local prop = property(Resource)
prop:reader("conf", nil)            --配置
prop:reader("heap", nil)            --heap
prop:reader("order", 1)             --order
prop:reader("gathers", {})          --交互信息
prop:reader("type", ENT_RESOURCE)   --type
prop:reader("players", {})          --采集玩家
prop:reader("multi_drop_time",nil)  --多人掉落时长
prop:reader("radius", RADIUS)       --采集半径
prop:accessor("scene", nil)         --scene
prop:accessor("block_id", 0)        --block_id
prop:accessor("refresh_time", 0)    --刷新时间
prop:accessor("refresh_limit", nil) --刷新次数上限
prop:accessor("visit_gather", true) --拜访可采集

function Resource:__init(id)
end

function Resource:is_resource()
    return true
end

function Resource:is_sight()
    return self.conf.sight
end

function Resource:can_relive()
    if self.block_id > 0 then
        if self.scene:is_block_unlock(nil, self.block_id) then
            return false
        end
    end
    return true
end

--load
function Resource:load(conf)
    --初始化属性
    self:init_attrset(attr_db)
    --初始化配置
    self:set_name(conf.name)
    self:reset(conf)
    self.conf = conf
    return true
end

--refresh
function Resource:refresh(now)
    local diff_time = now - self.active_time
    if diff_time > self.refresh_time then
        self:set_release(false)
        self:reset(self.conf)
        self.players = {}
        return true
    end
    return false
end

function Resource:get_step()
    local progress = self:get_gprogress()
    return progress & 0xffff
end

function Resource:set_step(step)
    local progress = self:get_gprogress()
    self:set_gprogress((progress & 0xffff0000) | step)
end

function Resource:get_progress()
    local progress = self:get_gprogress()
    return progress >> 16
end

function Resource:set_progress(prog)
    self:set_gprogress(prog << 16)
end

--reset
function Resource:reset(conf)
    self:set_gcount(0)
    self:set_gprogress(0)
    self:set_proto_id(conf.id)
    self.radius = (conf.radius or RADIUS) * 2
    if conf.multi_drop_time then
        self.multi_drop_time = conf.multi_drop_time
    end
    local heap = conf.heap
    if heap and next(heap) then
        local hconf = resource_db:find_one(heap[1])
        if hconf then
            conf = hconf
            self.heap = heap
            self:set_progress(1)
        end
    end
    self:load_gather(conf)
end

--收集交互信息
function Resource:load_gather(conf)
    self.order = conf.order
    self.gathers = conf.gather
    if self.order == 1 then
        self:update_step()
        return
    end
    for _, gather in ipairs(self.gathers) do
        if gather.effect == RESTO_HP then
            self:set_hp_max(gather.args)
            self:set_hp(gather.args)
        else
            self:set_gcount(gather.args)
        end
    end
end

--查询交互信息
function Resource:find_gather(step)
    if self.order == 1 then
        --串行交互
        if self:get_step() == step then
            return self.gathers[step]
        end
        return
    end
    return self.gathers[step]
end

function Resource:find_equip(player, gather)
    local pos = gather.tool
    if pos and pos > 0 then
        local attr_id = ATTR_POS + gather.tool
        return player:get_attr(attr_id)
    end
    return 0
end

--检查工具
function Resource:check_equip(player, gather)
    local pos, lvl = gather.tool, gather.lvl
    if pos and pos > 0 then
        local attr_id = ATTR_POS + pos
        local equip_id = player:get_attr(attr_id)
        if not equip_id then
            return GATH_TOOL_ERR
        end
        local conf = equip_db:find_one(equip_id)
        if not conf or conf.level < lvl then
            return GATH_TOOL_LVL
        end
        gather.tool_damage = conf.tool_damage or 0
    end
    return FRAME_SUCCESS
end

function Resource:check_loading(args)
    if self:get_gprogress() == args.progress then
        return self.gathers[args.cur]
    end
end

function Resource:load_loading(gather, step)
    if gather.effect ~= RESTO_HP then
        local loading = gather.loading
        if loading and loading > 0 then
            local args = { cur = step, progress = self:get_gprogress() }
            return loading, gather.breakin, args
        end
    end
end

--交互检查
function Resource:check_gather(gather)
    local result = {}
    if gather.effect ~= RESTO_HP then
        if self:get_gcount() == 0 then
            return GATHER_EMPTY
        end
        result.count = 1
        if gather.cost then
            result.cost = { cost = gather.cost, reason = COST_GATHER }
        end
        local drop_id = gather.drop[100]
        if drop_id then
            result.drop = { drop = { drop_id }, reason = OBTAIN_GATHER }
        end
        return FRAME_SUCCESS, result
    end
    local hp = self:get_hp()
    if hp <= 0 then
        return GATHER_EMPTY
    end
    local attack = gather.tool_damage
    local drop_ids = {}
    local hp_max = self:get_hp_max()
    local now_hp = mmax(hp - attack, 0)
    local promin = 100 * (hp_max - hp) // hp_max
    local promax = 100 * (hp_max - now_hp) // hp_max
    for progress, drop_id in pairs(gather.drop) do
        if in_range(progress, promin, promax) then
            tinsert(drop_ids, drop_id)
        end
    end
    result.hp = mmin(hp, attack)
    if next(drop_ids) then
        result.drop = { drop = drop_ids, reason = OBTAIN_GATHER }
    end
    return FRAME_SUCCESS, result
end

--执行效果
function Resource:execute_gather(effect, player_id)
    local effect_type
    if effect.count then
        self:cost_gcount(effect.count)
        self:update_count()
        effect_type = RESTO_COUNT
    end
    if effect.hp then
        self:cost_hp(effect.hp)
        self:update_hp()
        effect_type = RESTO_HP
    end
    self:record_hurt(player_id, effect_type)
end

--阶段变更
function Resource:update_step()
    local step = self:get_step()
    local nxt_step = step + 1
    local gather = self.gathers[nxt_step]
    if gather and gather.type then
        self:set_step(nxt_step)
        if gather.effect == RESTO_HP then
            self:set_hp_max(gather.args)
            self:set_hp(gather.args)
            if self.multi_drop_time then
                self.players = {}
            end
        else
            self:set_gcount(gather.args)
        end
        return true
    end
    return false
end

--进度变更
function Resource:update_progress()
    local progress = self:get_progress()
    if progress > 0 then
        local nxt_progress = progress + 1
        local res_id = self.heap[nxt_progress]
        if res_id then
            self:set_progress(nxt_progress)
            local conf = resource_db:find_one(res_id)
            if conf then
                self:load_gather(conf)
                return true
            end
        end
    end
    return false
end

--update_hp
function Resource:update_hp()
    if self:get_hp() > 0 then
        return
    end
    if self:update_progress() then
        return
    end
    self:dead()
end

--update_count
function Resource:update_count()
    if self:get_gcount() > 0 then
        return
    end
    if self.order == 1 and self:update_step() then
        return
    end
    if self:get_hp_max() > 0 then
        return
    end
    if self:update_progress() then
        return
    end
    self:dead()
end

function Resource:dead()
    log_debug("[Resource][dead] id:{} proto_id:{} name:{}", self:get_id(), self:get_proto_id(), self:get_name())
    self.active_time = quanta.now
    self:set_release(true)
end

--记录伤害
function Resource:record_hurt(player_id, effect)
    if not player_id or not self.multi_drop_time or not effect then
        return
    end
    local hurt = self.players[player_id]
    if not hurt then
        hurt = {}
        self.players[player_id] = hurt
    end
    hurt[effect] = quanta.now
end

--是否多人掉落
function Resource:is_multi()
    return self.multi_drop_time ~= nil
end

--单人掉落
function Resource:drop_result(player, result, gather)
    local drop = result.drop
    local cost = result.cost
    if drop or cost then
        --特殊采集物(需要进邮件)
        local ok, code = player:execute_cost_drop(drop, cost, nil, not self.visit_gather)
        if qfailed(code, ok) then
            return code
        end
        local type = gather.tool or 0
        player:notify_event("on_resource_gather", self:get_proto_id(), type)
    end
end

--多人掉落
function Resource:multi_drop_result(gather, result)
    --HP类型未死亡
    if not self:get_release() and gather.effect == RESTO_HP then
        return
    end
    local effect_type = gather.effect
    local now = quanta.now
    local drop = result.drop
    local cost = result.cost
    for player_id, hurt in pairs(self.players) do
        local time = hurt[effect_type]
        if not time then
            goto continue
        end
        --10s以内产生过伤害的玩家
        if time + self.multi_drop_time >= now then
            local player = player_mgr:get_entity(player_id);
            if player then
                local ok, code = player:execute_cost_drop(drop, cost, nil, not self.visit_gather)
                if qfailed(code, ok) then
                    --提示背包已满
                    player:send("NID_POPUP_NTF", {error_code = code})
                    goto continue
                end
                local type = gather.tool or 0
                player:notify_event("on_resource_gather", self:get_proto_id(), type)
            end
        end
        ::continue::
    end
    return true
end

return Resource
