--object_factory.lua
local log_err          = logger.err
local log_debug        = logger.debug
local new_guid         = codec.guid_new
local tinsert          = table.insert
local mmax             = math.max
local unserialize      = luakit.unserialize

local event_mgr        = quanta.get("event_mgr")
local config_mgr       = quanta.get("config_mgr")
local protobuf_mgr     = quanta.get("protobuf_mgr")

local buff_db          = config_mgr:init_table("buff", "id")
local ground_db        = config_mgr:init_table("ground", "id")
local formula_db       = config_mgr:init_table("formula", "id")
local utensil_db       = config_mgr:init_table("utensil", "id")
local resource_db      = config_mgr:init_table("resource", "id")
local scene_eft_db     = config_mgr:init_table("scene_effect", "id")
local npc_db           = config_mgr:init_table("npc", "id")
local monster_db       = config_mgr:init_table("monster", "id")
local blueprint_db     = config_mgr:init_table("blueprint", "id")
local fuel_db          = config_mgr:init_table("fuel", "item_id")
local grow_curve_db    = config_mgr:init_table("growth_curve", "id", "lv")
local skillset_db      = config_mgr:init_table("skillset", "id")
local item_db          = config_mgr:get_table("item")
local collpoint_db     = config_mgr:init_table("collect_points", "id")
local utility_db       = config_mgr:init_table("utility", "key")

local ENT_NPC          = protobuf_mgr:enum("entity_type", "ENTITY_NPC")
local ENT_MONSTER      = protobuf_mgr:enum("entity_type", "ENTITY_MONSTER")
local ENT_RESOURCE     = protobuf_mgr:enum("entity_type", "ENTITY_RESOURCE")
local ENT_EFFECT       = protobuf_mgr:enum("entity_type", "ENTITY_EFFECT")

local SKILL_PROD_COEFF = utility_db:find_value("value", "skill_prod_coeff")

local StrtctType       = enum("StrtctType")
local UtensilType      = enum("UtensilType")

local ObjectFactory    = singleton()
local prop             = property(ObjectFactory)
prop:reader("growth_curve_map", {})
prop:reader("snap_confs", {})       --需要快照实体配置
prop:accessor("skill_prod_coeff", {}) --技能生产系数

function ObjectFactory:__init()
    --配置初始化
    self:on_cfg_growth_curve_changed()
    --事件监听
    event_mgr:add_trigger(self, "on_cfg_growth_curve_changed")
    --地图表
    --[[
    for _, conf in map_db:iterator() do
        if conf.type == HOME then
            self:init_map_snap(conf.nick)
            event_mgr:add_trigger(self, sformat("on_cfg_%s_changed", conf.nick))
        end
    end]]
    self.skill_prod_coeff = unserialize(SKILL_PROD_COEFF or "{ 0, 0, 0 }")
    self:init_limit_config()
    -- 配置添加分组
    utensil_db:add_group("group")
end

function ObjectFactory:init_limit_config()
    self.level_limit_config = {}
    for _, conf in utensil_db:iterator() do
        if conf.level_limit ~= nil then
            local tmp = {}
            for lv, limit in pairs(conf.level_limit) do
                table.insert(tmp, { lv, limit })
            end
            table.sort(tmp, function(element1, element2)
                return element1[1] < element2[1]
            end)
            self.level_limit_config[conf.id] = tmp
        end
    end
end

function ObjectFactory:get_utensil_limit(id, level)
    local limit_conf = self.level_limit_config[id]
    if limit_conf == nil then --不限制
        return -1
    end
    local count = 0
    for _, info in ipairs(limit_conf) do
        if level < info[1] then
            return count
        end
        count = info[2]
    end
    return count
end

--home地图更新
function ObjectFactory:on_cfg_home_changed()
    self:init_map_snap("home")
end

---成长曲线数据预处理(读取/更新配置表)
function ObjectFactory:on_cfg_growth_curve_changed()
    self.growth_curve_map = {}
    for _, conf in grow_curve_db:iterator() do
        if self.growth_curve_map[conf.id] == nil then
            self.growth_curve_map[conf.id] = {}
        end
        self.growth_curve_map[conf.id][conf.lv] = conf.value
    end
end

---获取成长曲线值
---@param curve_id integer 成长曲线Id
---@param level integer 成长等级
---@return number 成长曲线值
function ObjectFactory:growth_curve_value(curve_id, level)
    return (self.growth_curve_map[curve_id] or {})[level]
end

--创建场景效果物体
function ObjectFactory:create_scene_effect(id, proto_id)
    local conf = scene_eft_db:find_one(proto_id)
    if not conf then
        log_err("[ObjectFactory][create_scene_effect] conf {} not exist!", proto_id)
        return
    end
    local SceneEffect = import("world/entity/scene_effect.lua")
    local sceneEff = SceneEffect(id)
    if sceneEff:setup(conf) then
        return sceneEff
    end
    log_err("[ObjectFactory][create_scene_effect] failed. id:{} proto_id:{}", id, proto_id)
end

--创建资源
function ObjectFactory:create_resource(id, proto_id)
    local conf = resource_db:find_one(proto_id)
    if not conf then
        log_err("[ObjectFactory][create_resource] conf {} not exist!", proto_id)
        return
    end
    local Resource = import("world/entity/resource.lua")
    local resource = Resource(id)
    if resource:setup(conf) then
        return resource
    end
end

--创建NPC
function ObjectFactory:create_npc(id, proto_id, follower_conf)
    local conf = npc_db:find_one(proto_id)
    if not conf then
        log_err("[ObjectFactory][create_npc] conf {} not exist!", proto_id)
        return
    end
    local NPC = import("world/entity/npc.lua")
    local npc = NPC(id)
    if follower_conf then
        npc:set_follower_conf(follower_conf)
    end
    if npc:setup(conf) then
        log_debug("[ObjectFactory][create_npc] id: {}, no: {}, name: {}", id, conf.id, npc:get_name())
        return npc
    end
end

--创建怪物
function ObjectFactory:create_monster(id, proto_id, sow_conf)
    local conf = monster_db:find_one(proto_id)
    if not conf then
        log_err("[ObjectFactory][create_monster] conf {} not exist!", proto_id)
        return
    end
    local Monster = import("world/entity/monster.lua")
    local monster = Monster(id)
    monster:set_sow_conf(sow_conf)
    monster:set_can_wander(sow_conf.can_wander)
    if monster:setup(conf) then
        return monster
    end
end

--获取实体ID
function ObjectFactory:get_entity_id(mapid, conf)
    return mapid << 20 | conf.id
end

--创建实体
function ObjectFactory:create_object(mapid, conf)
    local entity
    --id规则使用 map1d + entity_no
    local id = mapid << 20 | conf.id
    if conf.type == ENT_NPC then
        entity = self:create_npc(id, conf.proto_id, conf)
    elseif conf.type == ENT_MONSTER then
        entity = self:create_monster(id, conf.proto_id, conf)
    elseif conf.type == ENT_RESOURCE then
        entity = self:create_resource(id, conf.proto_id)
        --是否允许拜访采集
        entity:set_block_id(conf.block_id or 0)
        if conf.visit_gather ~= nil and conf.visit_gather == false then
            entity:set_visit_gather(false)
        end
    elseif conf.type == ENT_EFFECT then
        entity = self:create_scene_effect(id, conf.proto_id)
    end
    if entity then
        entity:setup_marker()
        entity:set_dynamic(conf.dynamic)
        entity:set_refresh_time(conf.refresh_time)
        --刷新次数限制
        if conf.refresh_limit then
            entity:set_refresh_limit(conf.refresh_limit)
        end
        if entity.default_skills ~= nil and next(entity.default_skills) then
            entity:init_default_skills(entity.default_skills)
        end
        return entity
    end
end

--创建生产产品
function ObjectFactory:create_product(fn_type, proto_id, uuid)
    local conf = formula_db:find_one(proto_id)
    if not conf then
        log_err("[ObjectFactory][create_product] conf {} not exist!", proto_id)
        return
    end

    local item_conf = item_db:find_one(conf.produce_id)
    if not item_conf then
        log_err("[ObjectFactory][create_product] item_conf {}-{} not exist!", proto_id, conf.produce_id)
        return
    end

    if fn_type == UtensilType.PROD or fn_type == UtensilType.COMPANY then
        local Product = import("world/produce/product.lua")
        return Product(uuid or new_guid(), conf, item_conf.overlie)
    elseif fn_type == UtensilType.GATHER then
        local Product = import("world/produce/prodt_gather.lua")
        return Product(uuid or new_guid(), conf, item_conf.overlie)
    end
    return
end

--创建组装台产品
function ObjectFactory:create_combine(proto_id, uuid)
    local conf = blueprint_db:find_one(proto_id)
    if not conf then
        log_err("[ObjectFactory][create_combine] conf {} not exist!", proto_id)
        return
    end
    local Combine = import("world/produce/combine.lua")
    return Combine(uuid or new_guid(), conf)
end

function ObjectFactory:create_buff(buff_id, uuid)
    local conf = buff_db:find_one(buff_id)
    if not conf then
        log_err("[ObjectFactory][create_buff] conf {} not exist!", buff_id)
        return
    end
    local uid = uuid or new_guid()
    local Buff = import("world/entity/buff.lua")
    return Buff(uid, conf), uid
end

function ObjectFactory:create_skill(skill_id, uuid, equip_attr, equip_id, owner_entity, skill_type)
    local conf = skillset_db:find_one(skill_id)
    if not conf then
        log_err("[ObjectFactory][create_skill] conf {} not exist!", skill_id)
        return
    end
    local uid = uuid or new_guid()
    local Skill = import("world/entity/skill.lua")
    return Skill(uid, conf, equip_attr, equip_id, owner_entity, skill_type), uid
end

function ObjectFactory:find_utensil(proto_id)
    return utensil_db:find_one(proto_id)
end

function ObjectFactory:find_utensil_group(group_id)
    return utensil_db:find_group(group_id)
end

function ObjectFactory:find_utensil_maxqueue(group_id)
    local confs = self:find_utensil_group(group_id)
    local level_count = 0
    local pay_count = 0
    if confs then
        for _, item in pairs(confs) do
            level_count = mmax(level_count, item.queue or 0)
            pay_count = mmax(pay_count, item.pay_queue or 0)
        end
    end
    return level_count + pay_count
end

--创建建筑物家具
function ObjectFactory:create_utensil(proto_id, uuid)
    local conf = utensil_db:find_one(proto_id)
    if not conf then
        log_err("[ObjectFactory][create_utensil] conf {} not exist!", proto_id)
        return
    end
    local uid = uuid or new_guid()
    local fn_type = conf.fn_type
    local struct_type = conf.struct_type
    -- 建筑
    if struct_type == StrtctType.BUILDING then
        --帐篷
        if fn_type == UtensilType.HOUSE then
            local House = import("world/building/house.lua")
            return House(uid, conf)
            --仓库
        elseif fn_type == UtensilType.BANK then
            local Storage = import("world/building/bank.lua")
            return Storage(uid, conf)
        end
        local Building = import("world/building/building.lua")
        return Building(uid, conf)
        -- 用具
    elseif struct_type == StrtctType.UTENSIL then
        local Utensil = import("world/utensil/utensil.lua")
        return Utensil(uid, conf)
    end
    log_err("[ObjectFactory][create_utensil] is fail, proto_id:{} uuid:{}", proto_id, uuid)
    return nil
end

-- 创建生产
function ObjectFactory:create_produce(proto_id, uuid)
    local conf = utensil_db:find_one(proto_id)
    if not conf then
        log_err("[ObjectFactory][create_produce] conf {} not exist!", proto_id)
        return
    end
    local uid = uuid or new_guid()
    local fn_type = conf.fn_type
    if fn_type == UtensilType.PROD or fn_type == UtensilType.COMPANY then
        local ProduceRaw = import("world/produce/prode_raw.lua")
        return ProduceRaw(uid, conf)
    elseif fn_type == UtensilType.GATHER then
        local Growable = import("world/produce/prode_gather.lua")
        return Growable(uid, conf)
    elseif fn_type == UtensilType.PACK then
        local PackBench = import("world/produce/packbench.lua")
        return PackBench(uid, conf)
    end
    log_debug("[ObjectFactory][create_produce] fn_type {} not exist proto_id({})", fn_type, proto_id)
    return nil
end

-- 创建储物箱
function ObjectFactory:create_storebox(id)
    local StoreBox = import("world/produce/storebox.lua")
    return StoreBox(id)
end

--收集需要持久化的实体
function ObjectFactory:init_map_snap(nick)
    local map_nick_db = config_mgr:init_table(nick, "id")
    local limits = {}
    for _, conf in map_nick_db:iterator() do
        if conf.dynamic and conf.refresh_limit then
            tinsert(limits, conf)
        end
    end
    self.snap_confs[nick] = limits
end

--获取需要持久化的实体
function ObjectFactory:load_map_store(nick)
    return self.snap_confs[nick]
end

function ObjectFactory:find_formula(proto_id)
    local formula = formula_db:find_one(proto_id)
    if formula then
        return formula
    end
end

function ObjectFactory:find_blueprint(proto_id)
    local blueprint = blueprint_db:find_one(proto_id)
    if blueprint then
        return blueprint
    end
end

function ObjectFactory:find_fuel(proto_id)
    return fuel_db:find_integer("value", proto_id)
end

function ObjectFactory:find_ground(proto_id)
    return ground_db:find_one(proto_id)
end

function ObjectFactory:find_parent_build(conf)
    local configs = utensil_db:find_group(conf.group)
    for _, item in pairs(configs) do
        if item.id == conf.parent_id then
            return item
        end
    end
    return nil
end

function ObjectFactory:find_collpoint_id(proto_id)
    return collpoint_db:find_one(proto_id)
end

function ObjectFactory:find_item(item_id)
    return item_db:find_one(item_id)
end

function ObjectFactory:is_utensil(prototype)
    return prototype.struct_type == StrtctType.UTENSIL
end

function ObjectFactory:is_storebox(prototype)
    return prototype.fn_type == UtensilType.STORE
end

function ObjectFactory:is_recruit(prototype)
    return prototype.fn_type == UtensilType.RECR
end

function ObjectFactory:is_shop(prototype)
    return prototype.fn_type == UtensilType.SHOP
end

function ObjectFactory:is_company(prototype)
    return prototype.fn_type == UtensilType.COMPANY
end

function ObjectFactory:is_hall(prototype)
    return prototype.fn_type == UtensilType.HALL
end

function ObjectFactory:is_building(prototype)
    return prototype.struct_type == StrtctType.BUILDING
end

function ObjectFactory:is_structure(prototype)
    return prototype.struct_type == StrtctType.STRUCT
end

function ObjectFactory:is_house(prototype)
    return prototype.fn_type == UtensilType.HOUSE
end

function ObjectFactory:is_bank(prototype)
    return prototype.fn_type == UtensilType.BANK
end

function ObjectFactory:is_gather(prototype)
    return prototype.fn_type == UtensilType.GATHER
end

-- export
quanta.object_fty = ObjectFactory()

return ObjectFactory
