--scene_effect.lua
local log_debug     = logger.debug
local log_err       = logger.err

local config_mgr    = quanta.get("config_mgr")
local protobuf_mgr  = quanta.get("protobuf_mgr")
local effect_mgr    = quanta.get("effect_mgr")
local RES_AWALYS    = quanta.enum("SceneEffectType", "AWALYS")

local scene_efct_db = config_mgr:init_table("scene_effect", "id")
local attr_db       = config_mgr:init_table("scene_effect_attr", "key")

local ENT_EFFECT    = protobuf_mgr:enum("entity_type", "ENTITY_EFFECT")
local Entity        = import("business/entity/entity.lua")
local AoiComponent  = import("world/component/aoi_component.lua")

local RADIUS        = 500                     --默认采集半径

local SceneEffect = class(Entity, AoiComponent)
local prop = property(SceneEffect)
prop:reader("type", ENT_EFFECT)       --type
prop:reader("radius", RADIUS)       --采集半径
prop:accessor("scene", nil)         --scene
prop:accessor("refresh_time", 0)    --刷新时间
prop:accessor("refresh_limit", nil) --刷新次数上限
prop:reader("entities", {})         --区域内实体(key-实体ID value-实体对象)
prop:reader("scene_eff_conf", nil)  --场景buf配置
prop:reader("last_update_time", 0)  --上次调度时间(针对区域)

function SceneEffect:__init(id)

end

function SceneEffect:is_resource()
    return false
end

--load
function SceneEffect:load(conf)
    log_debug("[SceneEffect][load] conf.id:{}", conf.id)
    --初始化属性
    self:init_attrset(attr_db)
    --初始化配置
    self:set_name(conf.name)
    self:reset(conf)
    --获取触发buff配置
    local effect_conf = scene_efct_db:find_one(conf.id)
    if not effect_conf then
        log_err("[SceneEffect][load] conf.proto_id:{} not found ", conf.id)
    end
    self.scene_eff_conf = effect_conf
    return true
end


--refresh
function SceneEffect:refresh(now)
    local diff_time = now - self.active_time
    if diff_time > self.refresh_time then
        local res_id = self:get_proto_id()
        local conf = scene_efct_db:find_one(res_id)
        self:set_release(false)
        self:reset(conf)
        return true
    end
    return false
end

--reset
function SceneEffect:reset(conf)
    self:set_proto_id(conf.id)
    self.radius = conf.radius or RADIUS
end

--交互
function SceneEffect:enter(target)
    log_debug("[SceneEffect][enter] scene_effect.id:{} target_id:{}", self.id, target:get_id())
    --添加到列表
    self.entities[target:get_id()] = target
    --是否为持续性物体
    if self:is_awalys() then
        return
    end
    --非持续触发效果
    self:execute_effect()
    self:dead()
end

--退出
function SceneEffect:exit(target)
    log_debug("[SceneEffect][exit] target_id:{}", target:get_id())
    self.entities[target:get_id()] = nil
end

--死亡
function SceneEffect:dead()
    log_debug("[SceneEffect][dead] id:{} proto_id:{} name:{}", self:get_id(), self:get_proto_id(), self:get_name())
    self.active_time = quanta.now
    self:set_release(true)
end

--坐标是否在覆盖范围内
function SceneEffect:in_area(x, y, z)
    local pos_x, pos_y, pos_z = self:get_pos_x(), self:get_pos_y(), self:get_pos_z()
    return math.sqrt((pos_x - x)^2 + (pos_y - y)^2 + (pos_z -z) ^2) <= self.radius
end

--是否为持续性物体
function SceneEffect:is_awalys()
    return self.scene_eff_conf.type == RES_AWALYS
end

-- 刷新
function SceneEffect:update(now)
    --是否区域触发
    if not self:is_awalys() or self:get_release() then
        return
    end
    if now - self.last_update_time > self.scene_eff_conf.interval then
        self:execute_effect()
        self.last_update_time = now
    end
end

--触发
function SceneEffect:execute_effect()
    local valid_entities = {}
    local effects = self.scene_eff_conf.effect_ids or {}
    for entity_id, entity in pairs(self.entities or {}) do
        --是否范围内
        if self:in_area(entity:get_pos_x(), entity:get_pos_y(), entity:get_pos_z()) then
            valid_entities[entity_id] = entity
            --执行
            for _, effect_id in pairs(effects) do
                effect_mgr:execute(entity, effect_id, 1, entity)
            end
        end

    end
    self.entities = valid_entities
end

return SceneEffect
