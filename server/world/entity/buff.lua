-- buff.lua
-- buff的实体类
local tinsert       = table.insert

local START         = quanta.enum("TriggerType", "START")
local PERIOD        = quanta.enum("TriggerType", "PERIOD")
local STOP          = quanta.enum("TriggerType", "STOP")
local TEMP          = quanta.enum("TriggerType", "TEMP")

local effect_mgr    = quanta.get("effect_mgr")

local Buff = class()
local prop = property(Buff)
prop:reader("id", 0)                    -- id
prop:reader("buff_id", 0)               -- buffid(配置)
prop:reader("prototype", 0)             -- prototype
prop:reader("stop_eid", nil)            -- 结束效果id
prop:reader("stop_effects", {})         -- 结束效果列表
prop:reader("period_effects", {})       -- 周期效果列表

local dbprop = db_property(Buff, "player_buff")
dbprop:store_value("layer", 1)
dbprop:store_value("expire", 0)

function Buff:__init(id, conf)
    self.id = id
    self.buff_id = conf.id
    self.prototype = conf
end

--激活
function Buff:active(player)
    local conf = self.prototype
    local now, now_ms = quanta.now, quanta.now_ms
    for _, effect in pairs(conf.effects) do
        if effect.trigger_type == TEMP then
            effect_mgr:execute(player, effect.id, 1, player)
            self.stop_effects[effect.id] = 0
            goto continue
        end
        --开始触发
        if self.expire == 0 and (effect.trigger_type & START == START) then
            effect_mgr:execute(player, effect.id, 1, player)
        end
        --周期触发
        if effect.trigger_type & PERIOD == PERIOD then
            tinsert(self.period_effects, { id = effect.id, period = effect.period, time = now_ms + effect.period })
        end
        --结束触发
        if effect.trigger_type & STOP == STOP then
            self.stop_effects[effect.id] = 1
        end
        :: continue ::
    end
    if self.expire == 0 then
        self.expire = (conf.time > 0) and (conf.time + now) or conf.time
    end
end

--结束
function Buff:stop(player)
    self.buff_id = 0
    for effect_id, op in pairs(self.stop_effects) do
        if op == 0 then
            effect_mgr:revert(player, effect_id, 1, player)
        else
            effect_mgr:execute(player, effect_id, 1, player)
        end
    end
end

--是否过期
function Buff:is_expire(now)
    if self.expire > 0 then
        return now > self.expire
    end
    return false
end

--叠加时间
function Buff:overlap_time(conf)
    self:save_layer(self.layer + 1)
    if conf > 0 and self.expire > 0 then
        self:save_expire(self.expire + conf.time)
    end
end

--检查周期效果
function Buff:run(player, now_ms)
    for _, effect in pairs(self.period_effects) do
        if now_ms >= effect.time then
            effect.time = effect.time + effect.period
            effect_mgr:execute(player, effect.id, 1, player)
        end
    end
end

function Buff:get_mutex_group()
    return self.prototype.mutex_group
end

function Buff:get_mutex_priority()
    return self.prototype.mutex_priority
end

function Buff:get_overlap_group()
    return self.prototype.overlap_group
end

function Buff:is_broadcast()
    return self.prototype.broadcast
end

function Buff:is_store()
    return self.prototype.store
end

function Buff:pack2db()
    return { buff_id = self.buff_id, layer = self.layer, expire = self.expire }
end

function Buff:pack2client()
    return { uuid = self.id, buff_id = self.buff_id, expire = self.expire }
end

return Buff
