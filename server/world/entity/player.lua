--player.lua
local log_warn         = logger.warn
local log_debug        = logger.debug
local log_info         = logger.info
local qedition         = quanta.edition
local makechan         = quanta.make_channel

local store_mgr        = quanta.get("store_mgr")
local config_mgr       = quanta.get("config_mgr")
local protobuf_mgr     = quanta.get("protobuf_mgr")

local attr_db          = config_mgr:init_table("player_attr", "key")
local utility_db       = config_mgr:init_table("utility", "key")

local OFFTIMEOUT       = quanta.enum("NetwkTime", "OFFLINE_TIMEOUT")
local SECOND_5_MS      = quanta.enum("PeriodTime", "SECOND_5_MS")

local ONL_LOADING      = quanta.enum("OnlineStatus", "LOADING")
local ONL_INLINE       = quanta.enum("OnlineStatus", "INLINE")
local ONL_OFFLINE      = quanta.enum("OnlineStatus", "OFFLINE")
local ONL_CLOSE        = quanta.enum("OnlineStatus", "CLOSE")

local ENTITY_PLAYER    = protobuf_mgr:enum("entity_type", "ENTITY_PLAYER")

local DAY_FLUSH_S      = utility_db:find_integer("value", "flush_day_hour") * 3600

local Entity           = import("business/entity/entity.lua")
local AoiComponent     = import("world/component/aoi_component.lua")
local PlayerComponent  = import("world/component/player_component.lua")
local PacketComponent  = import("world/component/packet_component.lua")
local UtilityComponent = import("world/component/utility_component.lua")
local ReddotComponent  = import("world/component/reddot_component.lua")
local ActionComponent  = import("world/component/action_component.lua")
local SkillComponent   = import("world/component/skill_component.lua")
local RecruitComponent = import("world/component/recruit_component.lua")
local RecordComponent  = import("world/component/record_component.lua")
local PartnerComponent = import("world/component/partner_component.lua")
local TaskComponent    = import("world/component/task_component.lua")
local BattleComponent  = import("world/component/battle_component.lua")
local AiAttentComponent= import("world/component/ai_attent_component.lua")
local CopyComponent    = import("world/component/copy_component.lua")
local CondComponent    = import("world/component/cond_component.lua")

local Player = class(Entity,
    AoiComponent,
    PlayerComponent,
    PacketComponent,
    ActionComponent,
    UtilityComponent,
    SkillComponent,
    ReddotComponent,
    RecruitComponent,
    RecordComponent,
    PartnerComponent,
    TaskComponent,
    BattleComponent,
    AiAttentComponent,
    CopyComponent,
    CondComponent
)

local prop             = property(Player)
prop:reader("type", ENTITY_PLAYER)   --type
prop:reader("status", 0)             --status
prop:reader("create_time", 0)        --create_time
prop:accessor("scene", nil)          --scene
prop:accessor("session", nil)        --session
prop:accessor("open_id", nil)        --open_id
prop:accessor("reload_token", nil)   --reload_token
prop:accessor("offtime", OFFTIMEOUT) --offtime

local dprop = db_property(Player, "player", true)
dprop:store_value("character_id", 0)
dprop:store_value("upgrade_time", 0) --upgrade_time
dprop:store_value("create_time", 0) --create_time

function Player:__init(id)
end

function Player:is_player()
    return true
end

function Player:on_db_player_load(data)
    log_debug("[Player][on_db_player_load] data:{}", data)
    if data.player_id then
        return false
    end
    self.create_time = data.create_time
    self.upgrade_time = data.upgrade_time or 0
    return true
end

--load
function Player:load(conf)
    self.status = ONL_LOADING
    self.active_time = quanta.now_ms
    self:setup_watcher()
    self:init_attrset(attr_db)
    local channel = makechan("load_player")
    channel:push(function()
        return store_mgr:load_group(self, self.id, "player")
    end)
    channel:push(function()
        return store_mgr:load_group(self, self.id, "player_world")
    end)
    self:invoke("_load", channel, self.id)
    local ok, code = channel:execute()
    if not ok then
        log_warn("[Player][load] player({}) failed: {}!", self.id, code)
    end
    return ok
end

function Player:get_scene()
    return self.scene
end

function Player:get_scene_level()
    return 1000;
end

--修改玩家名字
function Player:update_name(name)
    self:set_name(name)
    self:save_nick(name)
    self.account:update_nick(self.id, name)
end

--修改玩家外观
function Player:update_custom(custom)
    self:save_facade(custom)
    self.account:update_custom(self.id, custom)
end

--是否新玩家
function Player:is_newbee()
    log_debug("[Player][is_newbee] id:{} login_time:{}", self.id, self:get_login_time())
    return self:get_login_time() == 0
end

--day_update
function Player:day_update(week_flush)
    self:invoke("_day_update", week_flush)
    self:set_version(self:build_version())
end

function Player:build_version()
    return qedition("day", quanta.now, DAY_FLUSH_S)
end

--update
function Player:check(now)
    if self.status == ONL_CLOSE then
        return false
    end
    local now_ms = quanta.now_ms
    if self.status == ONL_LOADING then
        --加载失败
        if now_ms - self.active_time > SECOND_5_MS then
            log_warn("[Player][check] player({}) load too long, will be destory!", self.id)
            self:set_release(true)
            self.status = ONL_CLOSE
        end
        return false
    end
    if self.status == ONL_OFFLINE then
        --掉线清理
        if now_ms - self.active_time > self.offtime then
            log_warn("[Player][check] player({}) offline too long, will be destory!", self.id)
            self:set_release(true)
            self.status = ONL_CLOSE
        end
        return false
    end
    return true
end

--数据同步
function Player:sync_data()
    self:invoke("_sync_data")
end

--online
function Player:online()
    self.release = false
    self.status = ONL_INLINE
    self.active_time = quanta.now_ms
    --invoke
    self:invoke("_online")
    --load success
    self:set_login_time(quanta.now)
    self:set_version(self:build_version())
    self.load_success = true
    log_info("[Player][online] player({}) is online!", self.id)
end

--掉线
function Player:offline()
    self.status = ONL_OFFLINE
    self.active_time = quanta.now_ms
    --invoke
    self:invoke("_offline")
    log_warn("[Player][offline] player({}) is offline!", self.id)
end

function Player:relive()
    self.release = false
    self.status = ONL_INLINE
    self.active_time = quanta.now_ms
    --invoke
    self:invoke("_relive")
    log_warn("[Player][relive] player({}) is relive!", self.id)
end

--unload
function Player:unload()
    if self.scene then
        self.scene:leave(self, self.id)
    end
    --计算在线时间
    self:invoke("_unload")
    self:add_online_time(quanta.now - self:get_login_time())
    self:set_login_time(quanta.now)
    return true
end

--heartbeat
function Player:heartbeat()
    self.active_time = quanta.now_ms
    --invoke
    self:invoke("_heartbeat", quanta.now)
end

--create
function Player:update_char(char_info)
    self:set_name(char_info.nick)
    self:set_gender(char_info.gender)
    self:set_custom( char_info.facade)
    if self:is_newbee() then
        self:save_create_time(quanta.now)
        self:save_character_id(char_info.character_id)
    end
end

return Player
