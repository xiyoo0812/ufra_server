--ground.lua
local object_fty    = quanta.get("object_fty")
local config_mgr    = quanta.get("config_mgr")
local block_db      = config_mgr:init_table("block","id")

local Ground = class()
local prop = property(Ground)
prop:reader("id", 0)
prop:reader("prototype", nil)

local dbprop = db_property(Ground, "home_ground")
dbprop:store_values("blocks", {})
dbprop:store_value("time", 0)

function Ground:__init(id)
    self.id = id
    self.prototype = object_fty:find_ground(id)
end

function Ground:get_type()
    return self.prototype.type
end

function Ground:load_db(data)
    self.blocks = data.blocks or {}
    self.time = data.time
end

--地皮解锁是否完成
function Ground:is_finish()
    if self.time then
        return self.time <= quanta.now
    end
    return false
end

--地皮是否揭幕
function Ground:is_unveil()
    if self.time then
        return self.time == 0
    end
    return false
end

--地块解锁是否完成
function Ground:is_block_finish(block_id)
    if not self:is_finish() then
        return false
    end
    local block_time = self.blocks[block_id]
    if block_time then
        return block_time <= quanta.now
    end
    return false
end

--地块是否解锁
function Ground:is_block_unlock(block_id)
    if not self:is_finish() then
        return false
    end
    local block_time = self.blocks[block_id]
    if not block_time then
        return false
    end
    return block_time == 0
end

--获取地块解锁时间
function Ground:get_block_time(block_id)
    return self.blocks[block_id]
end

-- 获取加速消耗
function Ground:get_sp_block_costs(block_id)
    local block_conf = block_db:find_one(block_id)
    if not block_conf then
        return false
    end
    if not next(block_conf.sp_costs) then
        return false
    end
    if not block_conf.sp_unit or block_conf.sp_unit <= 0 then
        return false
    end
    local block_time = self:get_block_time(block_id)
    if not block_time then
        return false
    end
    local costs = {}
    -- 剩余的时间
    local last_time = quanta.now - block_time
    -- 消耗的加速单位
    local cost_unit = math.max(math.ceil(last_time / block_conf.sp_unit), 1)
    for item_id, count in pairs(block_conf.sp_costs or {}) do
        costs[item_id] = count * cost_unit
    end
    return true, costs
end

function Ground:pack2client()
    return {
        id          = self.id,
        blocks      = self.blocks,
        time        = self.time
    }
end

return Ground
