--partner.lua
local BuffBox           = import("world/entity/buff_box.lua")
local log_debug         = logger.debug
local log_err           = logger.err
local log_warn          = logger.warn
local sformat           = string.format
local mfloor            = math.floor
local config_mgr        = quanta.get("config_mgr")
local event_mgr         = quanta.get("event_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")
local recruit_mgr       = quanta.get("recruit_mgr")
local effect_mgr        = quanta.get("effect_mgr")
local OBTAIN_GIFT       = protobuf_mgr:enum("obtain_reason", "NID_OBTAIN_GIFT")
local TARGET_NPC        = quanta.enum("NfSkillTarget", "NPC")

local STAR_MAX          = protobuf_mgr:error_code("PARTNER_STAR_MAX")

local npc_db            = config_mgr:init_table("npc", "id")
local npc_star_db       = config_mgr:init_table("npc_star", "id")
local npc_lv_db         = config_mgr:init_table("npc_lv", "id")
local affinity_lv_db    = config_mgr:init_table("npc_affinity_level", "level")
local item_db           = config_mgr:get_table("item")
local nf_skill_db       = config_mgr:init_table("nf_skill", "id")

local Partner = class(nil, BuffBox)
local prop = property(Partner)
prop:reader("id", nil)
prop:reader("affinity_lv_conf", nil)        --当前好感等级配置
prop:reader("base_conf", nil)               --当前npc配置
prop:reader("lv_conf", nil)                 --当前npc等级配置
prop:reader("star_conf", nil)               --当前npc星级配置
prop:reader("player", nil)
prop:accessor("hung_buff_uid", 0)           --npc饥饿值衰减UUID
prop:accessor("buff_uids", {})              --技能buff uuid
prop:reader("sat_effects", {})              --满意度效果
prop:reader("nfskill_per", 0)               --生活技能效果百分比
prop:reader("affinity_per", 0)              --好感度效果百分比

local dbprop = db_property(Partner, "player_partner")
dbprop:store_value("state", 0)          --状态
dbprop:store_value("proto_id", 0)       --npc原型ID
dbprop:store_value("building_id", 0)    --工作建筑ID
dbprop:store_value("building_pos", 0)   --工作位置
dbprop:store_value("lv", 0)             --等级
dbprop:store_value("star_lv", 0)        --星级
dbprop:store_value("affinity", 0)       --好感度
dbprop:store_value("affinity_lv", 1)    --好感度等级
dbprop:store_value("day_gift", 0)       --今日收到礼物数量
dbprop:store_value("house_id", 0)       --民居ID
dbprop:store_value("house_pos", 0)      --床位
dbprop:store_value("from", 0)           --npc来源(任务奖励,招募等)
dbprop:store_value("exp", 0)            --经验值
dbprop:store_value("limit_lv", 0)       --突破等级
dbprop:store_value("reddot_story", 0)   --剧情红点
dbprop:store_value("reddot_voice", 0)   --语音红点
dbprop:store_value("is_dialog", false)  --是否对话
dbprop:store_value("check_in_time", 0)  --首次入住时间

function Partner:__init(player, id, proto_id, lv_id, star_lv, is_new)
    self.player = player
    self.id = id
    self.star_lv = star_lv
    self.proto_id = proto_id
    --基础配置
    self.base_conf = npc_db:find_one(proto_id)
    if not self.base_conf then
        log_err("[Partner][__init] proto_id:{} not found", proto_id)
        return
    end
    --等级配置
    self.lv_conf = npc_lv_db:find_one(lv_id)
    if not self.lv_conf then
        log_err("[Partner][__init] lv_id:{} not found", lv_id)
        return
    end
    --星级配置
    local star_id = self:get_star_id()
    self.star_conf = npc_star_db:find_one(star_id)
    if not self.star_conf then
        log_err("[Partner][__init] star_id:{} not found", star_id)
        return
    end
    --初始判定
    if is_new then
        self.affinity_lv_conf = affinity_lv_db:find_one(1)
        if self.base_conf then
            self.affinity = self.base_conf.default_affinity or 0
            --是否初始升级
            self:add_affinity(0)
        end
        event_mgr:fire_frame(function()
            self.player:notify_event("on_affinity_change", self.proto_id, self:get_affinity_lv(), self:get_affinity())
        end)

        local lv = mfloor(lv_id % 100)
        self:save_lv(lv)
        self:save_limit_lv(self.lv_conf.limit_lv)
    end
end

function Partner:load_db(data)
    self.state = data.state
    self.building_id = data.building_id
    self.building_pos = data.building_pos
    self.lv = data.lv
    self.star_lv = data.star_lv
    self.affinity = data.affinity
    self.affinity_lv = data.affinity_lv
    self.day_gift = data.day_gift
    self.house_id = data.house_id
    self.house_pos = data.house_pos
    self.from = data.from
    self.exp = data.exp
    self.limit_lv = data.limit_lv
    self.is_dialog = data.is_dialog
    self.reddot_story = data.reddot_story
    self.reddot_voice = data.reddot_voice
    self.affinity_lv_conf = affinity_lv_db:find_one(self.affinity_lv)
end

--是否可接受礼物
function Partner:check_recv_gift(num)
    local result = self.day_gift + num
    return self.base_conf.day_gift_limit >= result
end

--是否可遣散
function Partner:can_drive()
    return self.base_conf.can_drive or false
end

--每日重置
function Partner:reset_day_gift()
    self:save_day_gift(0)
end

--收礼
function Partner:recv_gift(player, item_id, num)
    --基础好感度
    local item_conf = item_db:find_one(item_id)
    if not item_conf or not item_conf.base_affinity then
        log_warn("[Partner][recv_gift] item({}) not found or not gift", item_id)
        return
    end
    local affinity = item_conf.base_affinity
    --礼物喜好度
    local like_items = self.base_conf.like_items
    if like_items and like_items[item_id] then
        local like_scale = like_items[item_id]
        affinity = mfloor(affinity * like_scale)
        log_debug("[Partner][recv_gift] base_conf.id:{} like_items:{}", self.base_conf.id, like_items)
    end
    affinity = affinity * num
    log_debug("[Partner][recv_gift] base_conf.id:{} affinity:{}", self.base_conf.id, affinity)
    local before_lv = self.affinity_lv
    self:add_affinity(affinity)
    self:save_day_gift(self.day_gift + num)
    local after_lv = self.affinity_lv
    self.player:notify_event("on_recv_gift", self.proto_id, item_id, num)
    event_mgr:notify_trigger("on_npc_flow", player, 2, self, affinity, after_lv - before_lv, sformat("%s:%s*%s", OBTAIN_GIFT, item_id, num))
end

--卸载效果
function Partner:unload_sat_effect()
    --卸载之前的效果
    for _, effect_id in pairs(self.sat_effects or {}) do
        log_debug("[Partner][unload_sat_effect] partner_id:{} effect_id:{} unload", self:get_id(), effect_id)
        effect_mgr:revert(self, effect_id, 1)
    end
    self.sat_effects = {}
end

--添加效果
function Partner:load_sat_effect(effects)
    self:unload_sat_effect()
    --加载新的效果
    for _, effect_id in pairs(effects or {}) do
        log_debug("[Partner][load_sat_effect] partner_id:{} effect_id:{} load", self:get_id(), effect_id)
        effect_mgr:execute(self, effect_id, 1)
    end
    self.sat_effects = effects or {}
end

--添加生活属性加层
function Partner:add_nfskill_per(add_value)
    self.nfskill_per = self.nfskill_per + add_value
    log_debug("[Partner][add_nfskill_per] partner_id:{} nfskill_per:{} add_value:{}", self:get_id(), self.nfskill_per, add_value)
end
--添加好感度加层
function Partner:add_affinity_per(add_value)
    log_debug("[Partner][add_affinity_per] partner_id:{} before affinity_per:{} add_value:{}", self:get_id(), self.affinity_per, add_value)
     self.affinity_per = self.affinity_per + add_value
     log_debug("[Partner][add_affinity_per] partner_id:{} after affinity_per:{} add_value:{}", self:get_id(), self.affinity_per, add_value)
end

--提高好感度
function Partner:add_affinity(num)
    log_debug("[Partner][add_affinity] orgin num:{}", num)
    --效果加成
    num = math.floor(num * (1 + self.affinity_per / 100))
    log_debug("[Partner][add_affinity] result self.affinity_per:{} num:{}", self.affinity_per, num)

    local result = self:get_affinity() + num
    local level_affinity = self.affinity_lv_conf.affinity
    --已经最高级
    if not level_affinity then
        return
    end
    if result < level_affinity then
        self:save_affinity(result)
        return
    end
    --是否升级
    local level_up = false
    --连续升级
    while level_affinity and result >= level_affinity do
        --升级
        local next_level_conf = affinity_lv_db:find_one(self.affinity_lv + 1)
        --最后一级
        if not next_level_conf then
            self:save_affinity(level_affinity)
            return level_up
        end
        self:save_affinity(result - level_affinity)
        self:save_affinity_lv(self.affinity_lv + 1)
        self.affinity_lv_conf = next_level_conf
        result = result - level_affinity
        level_affinity = next_level_conf.affinity
        level_up = true
        self.player:notify_event("on_affinity_change", self.proto_id, self:get_affinity_lv(), self:get_affinity())
        log_debug("[Partner][add_affinity] id:{} level:{} affinity:{} max:{}", self.proto_id, self.affinity_lv, result, level_affinity)
    end
    return level_up
end

--减少好感度
function Partner:reduce_affinity(num)
    log_debug("[Partner][reduce_affinity] num:{}", num)
    if num >= 0 then
        log_warn("[Partner][reduce_affinity] failed. num >= 0")
        return
    end
    local before = self:get_affinity()
    local cur = before + num
    if cur < 0 then
        cur = 0
    end
    self:save_affinity(cur)
end

--驱离民居
function Partner:drive_away()
    self:save_house_id(0)
    self:save_house_pos(0)
end

--是否已入住民居
function Partner:check_house()
    if not self.house_id then
        return false
    end
    return self.house_id ~= 0
end

--重载技能
function Partner:unload_nf_skill()
    for uid, _ in pairs(self.buff_uids or {}) do
        self:remove_buff(uid)
    end
end

--加载npc作用于自己的技能
function Partner:load_nf_skill()
    for _, skill_id in pairs(self.star_conf.nf_skills) do
        local skill_cfg = nf_skill_db:find_one(skill_id)
        if not skill_cfg then
            log_err("[Partner][load_nf_skill] star_id:{} skill_id:{} not found", self.star_conf.id, skill_id)
            goto continue
        end
        --作用目标是npc
        if skill_cfg.target ~= TARGET_NPC then
            goto continue
        end
        log_debug("[Partner][load_nf_skill] star_id:{} skill_id:{} ", self.star_conf.id, skill_id)
        --加载buff
        for _, buff_id in pairs(skill_cfg.skill_effect) do
            local ok, uid = self:add_buff(buff_id)
            if ok then
                self.buff_uids[uid] = buff_id
            end
        end
        ::continue::
    end
end

--获取等级ID
function Partner:get_lv_id(lv)
    if lv then
        return recruit_mgr:get_lv_id(self.proto_id, self.limit_lv, lv)
    end
    return recruit_mgr:get_lv_id(self.proto_id, self.limit_lv, self.lv)
end
--获取星级ID
function Partner:get_star_id()
    return recruit_mgr:get_star_id(self.proto_id, self.star_lv)
end

function Partner:update(now)
    BuffBox.update_buff(self, now)
end

--获取生产属性
function Partner:get_produce()
    local base =  mfloor(self.lv_conf.produce * self.star_conf.produce)
    --满意度影响
    local result = mfloor(base * (1 + self.nfskill_per / 100))
    log_debug("[Partner][get_produce] partner_id:{}, base:{} result:{} nfskill_per:{}", self.id, base, result, self.nfskill_per)
    return result, base
end
--获取采集属性
function Partner:get_collect()
    local base = mfloor(self.lv_conf.collect * self.star_conf.collect)
    --满意度影响
    local result = mfloor(base * (1 + self.nfskill_per / 100))
    log_debug("[Partner][get_collect] partner_id:{}, base:{} result:{} nfskill_per:{}", self.id, base, result, self.nfskill_per)
    return result, base
end
--获取研究属性
function Partner:get_research()
    local base =  mfloor(self.lv_conf.research * self.star_conf.research)
    --满意度影响
    local result = mfloor(base * (1 + self.nfskill_per / 100))
    log_debug("[Partner][get_research] partner_id:{}, base:{} result:{} nfskill_per:{}", self.id, base, result, self.nfskill_per)
    return result, base
end
--获取经营属性
function Partner:get_buss()
    local base =  mfloor(self.lv_conf.buss * self.star_conf.buss)
    --满意度影响
    local result = mfloor(base * (1 + self.nfskill_per / 100))
    log_debug("[Partner][get_buss] partner_id:{}, base:{} result:{} nfskill_per:{}", self.id, base, result, self.nfskill_per)
    return result, base
end
--获取作战属性
function Partner:get_fight()
    local base =  mfloor(self.lv_conf.fight * self.star_conf.fight)
    --满意度影响
    local result = mfloor(base * (1 + self.nfskill_per / 100))
    log_debug("[Partner][get_fight] partner_id:{}, base:{} result:{} nfskill_per:{}", self.id, base, result, self.nfskill_per)
    return result, base
end

--是否可升星
function Partner:check_up_star()
    local costs = self.star_conf.up_costs
    --是否可升级
    if not costs then
        return false, STAR_MAX
    end
    return true
end

--升星
function Partner:up_star()
    --升级前事件
    event_mgr:notify_trigger("on_partner_attr_before", self)
    self:save_star_lv(self.star_lv + 1)
    self.star_conf = npc_star_db:find_one(self:get_star_id())
    --升级后事件
    event_mgr:notify_trigger("on_partner_attr_after", self)

    event_mgr:notify_trigger("on_partner_attr_done", self.player, self.building_id)
end

--是否需要突破
function Partner:check_limit_lv()
    --达到突破等级的满经验值状态
    if self.lv_conf.limit_costs then
        return true
    end
    return false
end

--增加经验
function Partner:add_exp(add_exp)
    log_debug("[Partner][add_exp]: partner_id:{} add_exp:{}", self:get_id(), add_exp)
    local result = self.exp + add_exp
    --经验不触发升级
    if result < self.lv_conf.exp_max then
        log_debug("[Partner][add_exp] result({}) < self.lv_conf.exp_max({})", result, self.lv_conf.exp_max)
        self:save_exp(result)
        return
    end
    --升级前事件
    event_mgr:notify_trigger("on_partner_attr_before", self)
    --触发升级
    local level_up = 0
    --经验超过当前上限且未超过突破等级
    while result >= self.lv_conf.exp_max and (self.lv + level_up) < self.lv_conf.limit_lv do
        log_debug("[Partner][add_exp] result:{} exp_max:{}", result, self.lv_conf.exp_max)
        level_up = level_up + 1
        log_debug("[Partner][add_exp] level_up({})", level_up)
        local next_lv_conf = npc_lv_db:find_one(self:get_lv_id(self.lv + level_up))
        if next_lv_conf then
            log_debug("[Partner][add_exp] next_lv_conf:{}", self.lv + level_up)
            self.lv_conf = next_lv_conf
        else
            log_debug("[Partner][add_exp] next_lv_conf not found. {}", self.lv + level_up)
            break
        end
    end

    self:save_exp(result)
    self:save_lv(self.lv + level_up)
    log_debug("[Partner][add_exp] partner_id:{} exp({}) lv:{}", self:get_id(), self.exp, self.lv)
    --升级后事件
    event_mgr:notify_trigger("on_partner_attr_after", self)
    event_mgr:notify_trigger("on_partner_attr_done", self.player, self.building_id)
    --已达到突破等级
    if result >= self.lv_conf.exp_max and self.lv >= self.lv_conf.limit_lv then
        --剩余经验
        local left_exp = result - self.lv_conf.exp_max
        log_debug("[Partner][add_exp] partner_id:{} left_exp:{}", self:get_id(), left_exp)
        return left_exp
    end
end

--突破
function Partner:unlv()
    local next_lv_id = self.lv_conf.next_id
    log_debug("[Partner][unlv] partner_id:{} self.lv_conf.id:{}", self:get_id(),self.lv_conf.id)
    local next_lv_conf = npc_lv_db:find_one(next_lv_id)
    if not next_lv_conf then
        log_err("[Partner][unlv] partner_id:{} cant find {} next lv id",self:get_id(), self.lv_conf.id)
        return
    end
    --突破前事件
    event_mgr:notify_trigger("on_partner_attr_before", self)
    self.lv_conf = next_lv_conf
    self:save_lv(self.lv_conf.level)
    self:save_limit_lv(self.lv_conf.limit_lv)
    self:save_exp(0)
    --突破后事件
    event_mgr:notify_trigger("on_partner_attr_after", self)
    event_mgr:notify_trigger("on_partner_attr_done", self.player, self.building_id)
end

--序列化
function Partner:pack2db()
    local partner = {
        state       = self.state,
        building_id = self.building_id,
        building_pos= self.building_pos,
        lv          = self.lv,
        star_lv     = self.star_lv,
        affinity    = self.affinity,
        affinity_lv = self.affinity_lv,
        day_gift    = self.day_gift,
        house_id    = self.house_id,
        house_pos   = self.house_pos,
        from        = self.from,
        exp         = self.exp,
        limit_lv    = self.limit_lv,
        proto_id    = self.proto_id,
        reddot_story= self.reddot_story,
        reddot_voice= self.reddot_voice,
        is_dialog= self.is_dialog
    }
    return partner
end
--pb
function Partner:pack2client()
    local partner = {
        state       = self.state,
        building_id = self.building_id,
        building_pos= self.building_pos,
        lv          = self.lv,
        star_lv     = self.star_lv,
        affinity    = self.affinity,
        affinity_lv = self.affinity_lv,
        day_gift    = self.day_gift,
        house_id    = self.house_id,
        house_pos   = self.house_pos,
        from        = self.from,
        exp         = self.exp,
        limit_lv    = self.limit_lv,
        proto_id    = self.proto_id,
        reddot_story= self.reddot_story,
        reddot_voice= self.reddot_voice,
        is_dialog=self.is_dialog
    }
    return partner
end

return Partner
