--target.lua


local action_mgr    = quanta.get("action_mgr")

local mmin          = qmath.min
local mtointeger    = math.tointeger
local unserialize   = luakit.unserialize

local Target = class()
local prop = property(Target)
prop:reader("no", 1)
prop:reader("task", nil)
prop:reader("count", 1)
prop:reader("player", nil)
prop:reader("running", false)
prop:reader("prototype", nil)
prop:accessor("progress", 0)    --目标进度

function Target:__init(task, conf)
    self.task = task
    self.prototype = conf
    self.no = conf.target_no
    self.player = task:get_player()
end

-- 获取实体
function Target:get_entity()
    return self.task
end

--GM完成
function Target:complete()
    if self.running then
        self:forward(self.count)
    end
end

function Target:update_progress(num)
    local task = self.task
    self.progress = mmin(self.count, num)
    task:save_progress_field(self.no, self.progress)
    if self:is_complete() then
        self:on_done()
        local target_id = self.prototype.target_id
        if target_id then
            self.player:notify_event("on_target_finish", target_id)
        end
    end
    self.player:sync_progress(task)
    task:update_target()
end

function Target:forward(num)
    self:update_progress(self.progress + (num or 1))
end

function Target:is_complete()
    return self.progress == self.count
end

function Target:active()
    if not self.running then
        self.running = true
        self:on_active()
    end
end

function Target:deactive()
    if self.running then
        self.running = false
        self:on_deactive(true)
    end
end

function Target:finish()
    local action = self.prototype.action
    if action then
        action_mgr:execute(action, self)
    end
    self:on_deactive(false)
    self:on_finish()
end

function Target:on_cost()
    return true
end

--子类实现
------------------------------------------------
function Target:on_active()
end

function Target:on_deactive(close)
end

function Target:on_done()
end

function Target:on_finish()
end

--辅助接口
------------------------------------------------
function Target:get_arg_i(i, def)
    return mtointeger(self.prototype.args[i]) or def
end

function Target:get_arg_t(i)
    return unserialize(self.prototype.args[i])
end

function Target:get_arg(i, def)
    return self.prototype.args[i] or def
end

function Target:get_param_i(i, def)
    return mtointeger(self:get_param(i)) or def
end

function Target:get_param_t(i)
    local params = self:get_param(i)
    if params == nil then
        return nil
    end
    return unserialize(params)
end

function Target:get_param(i)
    local params = self.prototype.params
    if params then
        return params[i]
    end
end

return Target
