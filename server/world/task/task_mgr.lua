--task_mgr.lua
local mceil         = math.ceil
local mround        = qmath.round
local mrandom       = math.random
local tinsert       = table.insert
local log_warn      = logger.warn
local log_err       = logger.err
local trandarray    = qtable.random_array

local event_mgr     = quanta.get("event_mgr")
local config_mgr    = quanta.get("config_mgr")
local protobuf_mgr  = quanta.get("protobuf_mgr")
local condition_mgr = quanta.get("condition_mgr")

local task_db       = config_mgr:init_table("task", "id")
local talk_db       = config_mgr:init_table("talk", "id")
local text_db       = config_mgr:init_table("loop_text", "id")
local reward_db     = config_mgr:init_table("loop_reward", "id")
local ltask_db      = config_mgr:init_table("loop_task", "id")
local ltarget_db    = config_mgr:init_table("loop_target", "id")
local nearby_town_db= config_mgr:init_table("nearby_town", "id")
local target_db     = config_mgr:init_table("target", "id", "target_no")
local loop_daily_db = config_mgr:init_table("loop_dailycount", "finish_count")

local MAIN_TASK     = protobuf_mgr:enum("task_type", "MAIN")
local ORDER_TASK    = protobuf_mgr:enum("task_type", "ORDER")

local STORY_TASK    = protobuf_mgr:enum("task_type", "STORY")
local BRANCH_TASK   = protobuf_mgr:enum("task_type", "BRANCH")
local DAILY_TASK    = protobuf_mgr:enum("task_type", "DAILY")
local ORDER_BUILD   = protobuf_mgr:enum("task_type", "BUILDING")
local BIG_TOWN_ORDER   = protobuf_mgr:enum("task_type", "BIG_TOWN_ORDER")
local SHORT_TOWN_ORDER   = protobuf_mgr:enum("task_type", "SHORT_TOWN_ORDER")

local OBTAIN_OCHAT  = protobuf_mgr:enum("obtain_reason", "NID_OBTAIN_OCHAT")

local Random        = import("basic/random.lua")
local Task          = import("world/task/task.lua")
local TaskLoop      = import("world/task/task_loop.lua")

local Behavior      = enum("Behavior")
local TARGET_LIST   = {
    [Behavior.TALK]             = import("world/task/target/ttalk.lua"),
    [Behavior.MAKE]             = import("world/task/target/tmake.lua"),
    [Behavior.AREA]             = import("world/task/target/tarea.lua"),
    [Behavior.COST]             = import("world/task/target/tcost.lua"),
    [Behavior.TASK]             = import("world/task/target/ttask.lua"),
    [Behavior.PLACE]            = import("world/task/target/tplace.lua"),
    [Behavior.TIMER]            = import("world/task/target/ttimer.lua"),
    [Behavior.ACCEPT]           = import("world/task/target/taccept.lua"),
    [Behavior.COMMIT]           = import("world/task/target/tcommit.lua"),
    [Behavior.GATHER]           = import("world/task/target/tgather.lua"),
    [Behavior.GROUND]           = import("world/task/target/tground.lua"),
    [Behavior.LETTER]           = import("world/task/target/tletter.lua"),
    [Behavior.ASSIGN]           = import("world/task/target/tassign.lua"),
    [Behavior.COLLECT]          = import("world/task/target/tcollect.lua"),
    [Behavior.VIRTUAL]          = import("world/task/target/tvirtual.lua"),
    [Behavior.BUILDING]         = import("world/task/target/tbuilding.lua"),
    [Behavior.LOGIN]            = import("world/task/target/tlogin.lua"),
    [Behavior.ENERGY]           = import("world/task/target/tenergy.lua"),
    [Behavior.RECRUIT]          = import("world/task/target/trecruit.lua"),
    [Behavior.BUYITEM]          = import("world/task/target/tbuy.lua"),
    [Behavior.PARTNER]          = import("world/task/target/tpartner.lua"),
    [Behavior.UP_LEVEL]         = import("world/task/target/tlevel.lua"),
    [Behavior.READ_EMAIL]       = import("world/task/target/temail.lua"),
    [Behavior.ENTER]            = import("world/task/target/tenter.lua"),
    [Behavior.OS_DAY]           = import("world/task/target/tosday.lua"),
    [Behavior.GAME_DAY]         = import("world/task/target/tgameday.lua"),
    [Behavior.BLOCK]            = import("world/task/target/tblock.lua"),
    [Behavior.COMMITS]          = import("world/task/target/tcommits.lua"),
    [Behavior.TASKS]            = import("world/task/target/ttasks.lua"),
    [Behavior.FIND_BUILDING]    = import("world/task/target/tfind_building.lua"),
    [Behavior.PRODUCTION]       = import("world/task/target/tproduct.lua"),

    [Behavior.TOWN_LEVEL]       = import("world/task/target/ttown_level.lua"),
    [Behavior.WEAR_EQUIP]       = import("world/task/target/twear_equip.lua"),
    [Behavior.SALE_RESOURCE]    = import("world/task/target/tsale_resource.lua"),
    [Behavior.NPC_CHECK_IN]     = import("world/task/target/tnpc_checkin.lua"),
    [Behavior.FINISH_DUNGEON]   = import("world/task/target/tdungeon.lua"),
    [Behavior.KILL_MONSTER]     = import("world/task/target/tkill.lua"),

    [Behavior.DONE_FISH]        = import("world/task/target/tfish.lua"),
    [Behavior.RESTORE]          = import("world/task/target/trestore.lua"),
    [Behavior.NPC_GIFT]         = import("world/task/target/tnpc_gift.lua"),
    [Behavior.BUILDING_LEVEL]   = import("world/task/target/tbuild_level.lua"),
    [Behavior.RESIDENT_NUM]   = import("world/task/target/tresident.lua"),
}
local LOOP_TASK_TYPE_LIST = { DAILY_TASK, ORDER_BUILD, BIG_TOWN_ORDER, SHORT_TOWN_ORDER }

local TaskMgr = singleton()
local prop = property(TaskMgr)
prop:reader("dailys", {})
prop:reader("daily_rands", {})
prop:reader("loop_tasks", {})
prop:reader("loop_rewards", {})
prop:reader("loop_targets", {})
prop:reader("task_conf_conds", { })
prop:reader("task_conf_level_conds", { }) --等级关联任务
prop:reader("task_conf_chain_tasks", {}) --任务链
prop:reader("target_conf", { }) --子目标配置
prop:reader("max_loop_count", 0)

function TaskMgr:__init()

    --配置初始化
    self:on_cfg_task_changed()
    self:on_cfg_loop_task_changed()
    self:on_cfg_loop_target_changed()
    self:on_cfg_loop_reward_changed()
    --配置分组
    target_db:add_group("id")
    --事件监听
    event_mgr:add_trigger(self, "on_task_stop")
    event_mgr:add_trigger(self, "on_level_up")
    event_mgr:add_trigger(self, "on_cfg_task_changed")
    event_mgr:add_trigger(self, "on_cfg_loop_task_changed")
    event_mgr:add_trigger(self, "on_cfg_loop_target_changed")
    event_mgr:add_trigger(self, "on_cfg_loop_reward_changed")
    event_mgr:add_trigger(self, "on_cfg_loop_text_changed")
    self:check_task_target_conf()
    self:on_cfg_loop_dailycount_changed()
end

function TaskMgr:on_cfg_loop_text_changed()
    self.loop_texts = {}
    for _, conf in text_db:iterator() do
        local type_texts = self.loop_texts[conf.type]
        if not type_texts then
            type_texts = {}
            self.loop_texts[conf.type] = type_texts
        end
        local label_texts = type_texts[conf.label]
        if not label_texts then
            label_texts = {}
            type_texts[conf.label] = label_texts
        end
        label_texts[conf.npcid] = conf
    end
end

function TaskMgr:range_task_text_id(player, type, item_type)
    local loop_texts = self.loop_texts[type][item_type]
    if not loop_texts then
        return nil
    end
    local type_ids = {}
    local range_npcs = {}
    if type == ORDER_TASK then
        type_ids = player:get_partner_map()
    elseif type == ORDER_BUILD then
        local town = player:get_scene()
        type_ids = town:get_building_ids()
    elseif type == BIG_TOWN_ORDER or type == SHORT_TOWN_ORDER then
        for _, conf in nearby_town_db:iterator() do
            type_ids[conf.id] = true
        end
    end
    for id in pairs(type_ids) do
        if loop_texts[id] then
            tinsert(range_npcs, id)
        end
    end
    local npc_id = trandarray(range_npcs)
    if not npc_id then
        return nil
    end
    return npc_id, loop_texts[npc_id].id
end

function TaskMgr:on_cfg_loop_dailycount_changed()
    self.max_loop_count = 0
    for _, conf in loop_daily_db:iterator() do
        if conf.finish_count > self.max_loop_count then
            self.max_loop_count = conf.finish_count
        end
    end
end

function TaskMgr:check_task_target_conf()
    self.target_conf = { }
    for _, conf in target_db:iterator() do
        if conf.next_id then
            if conf.id == conf.next_id then
                log_err("[TaskMgr][check_task_target_conf] config error task_id==next_id ({})", conf.id)
                return false
            end
            local group = target_db:find_group(conf.next_id)
            if group == nil or (not next(group)) then
                log_err("[TaskMgr][check_task_target_conf] next config error next_id:{}", conf.next_id)
                return false
            end
        end
        self.target_conf[conf.target_id] = conf
    end
    return true
end

function TaskMgr:find_target_conf(id)
    return self.target_conf[id]
end

function TaskMgr:on_level_up(player, level)
    local tasks = self.task_conf_level_conds[level]
    for _, conf in ipairs(tasks or {}) do
        self:register_task_conds(player, conf)
    end
    self:update_accept_looptask(player, level)
end

function TaskMgr:on_task_stop(player, task)
    local tasks = self.task_conf_conds[task.id] --拿到以task_id 作为前置条件的任务列表
    for _, conf in ipairs(tasks or {}) do
        self:register_task_conds(player, conf)
    end
end

function TaskMgr:register_task_conds(player, conf)

    if player:has_finish_task(conf.id) then --完成任务
        return false
    end

    if player:get_accept_task(conf.id) then --接到任务
        return false
    end

    if self:try_acceept_task(player, conf) then --接取了
        return false
    end
    return true
end

function TaskMgr:check_level(player, conf)
    local level = player:get_level()
    if level < conf.level then --等级条件不够
        return false
    end
    return true
end

--是否订单任务
function TaskMgr:is_order_task(ttype)
    if self:is_town_order_task(ttype) then
        return true
    end
    return ttype == ORDER_TASK or ttype == STORY_TASK or ttype == ORDER_BUILD
end

--是否是城镇订单
function TaskMgr:is_town_order_task(ttype)
    return ttype == BIG_TOWN_ORDER or ttype == SHORT_TOWN_ORDER
end

function TaskMgr:is_main_task(ttype)
    return ttype == MAIN_TASK
end

function TaskMgr:is_branch_task(ttype)
    return ttype == BRANCH_TASK
end

function TaskMgr:is_daily_task(ttype)
    return ttype == DAILY_TASK
end

--是否剧情订单
function TaskMgr:is_story_task(ttype)
    return ttype == STORY_TASK
end

--是否历史任务
function TaskMgr:is_history_task(ttype)
    return ttype <= BRANCH_TASK or ttype == STORY_TASK
end

--是否循环任务
function TaskMgr:is_loop_task(ttype)
    if self:is_town_order_task(ttype) then
        return true
    end
    return ttype >= ORDER_TASK and ttype <= ORDER_BUILD
end

--是否需要移除任务
function TaskMgr:is_remove_task(ttype)
    return ttype < ORDER_TASK or ttype > DAILY_TASK
end

function TaskMgr:on_task_install(conf)
    --上一个任务解锁条件
    if conf.chain and self.task_conf_chain_tasks[conf.chain] == nil then
        local confs = { }
        local next_conf = task_db:find_one(conf.chain)
        while next_conf ~= nil do
            tinsert(confs, next_conf)
            if next_conf.next_task == nil then
                break
            end
            if next_conf.next_task and next_conf.next_task <= next_conf.id then
                log_err("[TaskMgr][on_task_install] conf:{}", next_conf)
                break
            end
            next_conf = task_db:find_one(next_conf.next_task)
        end
        self.task_conf_chain_tasks[conf.chain] = confs
    end

    if conf.pre_task then
        if self.task_conf_conds[conf.pre_task] == nil then
            self.task_conf_conds[conf.pre_task] = { }
        end
        tinsert(self.task_conf_conds[conf.pre_task], conf)
    else
        if self.task_conf_level_conds[conf.level] == nil then
            self.task_conf_level_conds[conf.level] = { }
        end
        tinsert(self.task_conf_level_conds[conf.level], conf)
    end
end

function TaskMgr:on_daily_install(level, conf, task_list)
    local level_rand = task_list[level]
    if not level_rand then
        level_rand = Random()
        task_list[level] = level_rand
    end
    if not conf.pre_task then
        level_rand:add_wheel(conf.id, 10, true)
    end
end

--任务改变
function TaskMgr:on_cfg_task_changed()
    self.dailys = {}
    self.daily_rands = {}
    self.task_conf_level_conds = { }
    self.task_conf_chain_tasks = { }
    for task_id, conf in task_db:iterator() do
        if self:is_story_task(conf.type) then --跳过剧情订单的注册
            goto continue
        elseif self:is_history_task(conf.type) then
            self:on_task_install(conf)
        elseif conf.type == DAILY_TASK then
            if conf.level_max == 0 then
                tinsert(self.dailys, task_id)
            else
                for lvl = conf.level, conf.level_max do
                    self:on_daily_install(lvl, conf, self.daily_rands)
                end
            end
        end
        ::continue::
    end
end

--循环任务改变
function TaskMgr:on_cfg_loop_task_changed()
    self.loop_tasks = {}
    for _, conf in ltask_db:iterator() do
        for lvl = conf.lvlmin, conf.lvlmax do
            local index = conf.type * 1000 + lvl
            self.loop_tasks[index] = conf
        end
    end
end

--循环任务随机目标改变
function TaskMgr:on_cfg_loop_target_changed()
    local lop_targets = {}
    for _, conf in ltarget_db:iterator() do
        local type, item_type = conf.type, conf.item_type
        local type_targets = lop_targets[type]
        if not type_targets then
            type_targets = {}
            lop_targets[type] = type_targets
        end
        for lvl = conf.lvlmin, conf.lvlmax do
            local lvl_targets = type_targets[lvl]
            if not lvl_targets then
                lvl_targets = {}
                type_targets[lvl] = lvl_targets
            end
            local itye_targets = lvl_targets[item_type]
            if not itye_targets then
                local tar_rand = Random()
                tar_rand.item_type = item_type
                itye_targets = { weight = 0, rand = tar_rand }
                lvl_targets[item_type] = itye_targets
            end
            itye_targets.weight = itye_targets.weight + conf.weight
            itye_targets.rand:add_wheel(conf, conf.weight)
        end
    end
    self.loop_targets = {}
    for type, type_targets in pairs(lop_targets) do
        self.loop_targets[type] = {}
        for level, lvl_targets in pairs(type_targets) do
            local itye_rand = Random()
            for _, item_targets in pairs(lvl_targets) do
                itye_rand:add_wheel(item_targets.rand, item_targets.weight)
            end
            self.loop_targets[type][level] = itye_rand
        end
    end
end

--循环任务随机奖励改变
function TaskMgr:on_cfg_loop_reward_changed()
    self.loop_rewards = {}
    for _, conf in reward_db:iterator() do
        local index = conf.type * 1000 + conf.lvlmin
        local rand = self.loop_rewards[index]
        if not rand then
            rand = Random()
            for lvl = conf.lvlmin, conf.lvlmax do
                local nindex = conf.type * 1000 + lvl
                self.loop_rewards[nindex] = rand
            end
        end
        rand:add_alone(conf, conf.rate)
    end
end

function TaskMgr:get_loop_task(lvl, type)
    return self.loop_tasks[type * 1000 + lvl]
end

function TaskMgr:get_loop_target(lvl, type)
    local itye_rand = self.loop_targets[type][lvl]
    if not itye_rand then
        return
    end
    return itye_rand:rand_wheel()
end

function TaskMgr:get_loop_reward(lvl, type)
    return self.loop_rewards[type * 1000 + lvl]
end

function TaskMgr:build_loop_target(prototype, targetc, taskc)
    local ratio, confs = 0, {}
    local count = mrandom(taskc.cntmin, taskc.cntmax)
    for i = 1, count do
        local tconf = targetc:rand_wheel()
        if not tconf then
            break
        end
        ratio = ratio + tconf.ratio
        tinsert(confs, tconf)
    end
    local items = {}
    taskc.sum_count = 0
    for _, conf in pairs(confs) do
        local rcount = items[conf.item_id] or 0
        local num = taskc.value * conf.ratio / ratio / conf.value
        if self:is_town_order_task(prototype.type) then
            num = mceil(mround(num))
        end
        items[conf.item_id] = rcount + (num <= 0 and 1 or num)
        taskc.sum_count = taskc.sum_count + (items[conf.item_id] * conf.value)
    end

    for item_id, rcount in pairs(items) do
        tinsert(prototype.targets, { item_id = item_id, count = rcount })
    end
end

function TaskMgr:build_loop_reward(prototype, rewardc, taskc)
    local ratio, ratios = 0, {}
    local items = rewardc:execute()
    for _, item in pairs(items) do
        if item.weight == 0 then --权重为0的物品不分走其他物品的价值
            local num = mceil(mround(taskc.sum_count / item.value))
            prototype.rewards[item.item_id] = num <= 0 and 1 or num
        else
            local cratio = item.weight + mrandom(0, item.drift * 2) - item.drift
            ratios[item.item_id] = { ratio = cratio, value = item.value }
            ratio = ratio + cratio
        end
    end

    for item_id, item in pairs(ratios) do
        local num = taskc.sum_count * item.ratio / ratio / item.value
        if self:is_town_order_task(prototype.type) then --城镇订单四舍五入
            local number = mceil(mround(num))
            prototype.rewards[item_id] = number <= 0 and 1 or number
        else
            prototype.rewards[item_id] = mceil(num)
        end
    end
end

--find_loop_text
function TaskMgr:find_loop_text(player, type, item_type)
    return self:range_task_text_id(player, type, item_type)
end

function TaskMgr:init_loop_prototype(player, type)
    local lvl = player:get_level()
    local taskc = self:get_loop_task(lvl, type)
    local rewardc = self:get_loop_reward(lvl, type)
    if not taskc or not rewardc then
        log_warn("[TaskMgr][init_loop_prototype] reward failed! player_id:%s, level:%s, type:%s", player:get_id(), lvl, type)
        return
    end
    local targetc = self:get_loop_target(lvl, type)
    if not targetc then
        log_warn("[TaskMgr][init_loop_prototype] target failed! player_id:%s, level:%s, type:%s", player:get_id(), lvl, type)
        return
    end
    local npc_id, text_id = self:find_loop_text(player, type, targetc.item_type)
    if not npc_id or not text_id then
        log_warn("[TaskMgr][init_loop_prototype] text failed! player_id:%s, npc_id:%s, text_id:%s", player:get_id(), npc_id, text_id)
        return
    end
    local prototype = {
        type = type,
        level = lvl,
        id = taskc.id,
        end_npc = npc_id,
        text_id = text_id,
        start_npc = npc_id,
        auto_finish = false,
        cd_time = taskc.cd_time,
        refresh_time = taskc.refresh_time,
        rewards = {},
        targets = {},
    }
    self:build_loop_target(prototype, targetc, taskc)
    self:build_loop_reward(prototype, rewardc, taskc)
    return prototype
end

function TaskMgr:try_acceept_task(player, conf)

    local task_id = conf.id
    if self:is_daily_task(conf.type) then
        self:update_fix_dailytask(player)
        local level = player:get_level()
        self:update_rand_dailytask(player, level)
    end

    if self:can_receive(player, conf) then
        local task = self:create_task(player, task_id)
        if task:auto_receive() then
            player:receive_task(task, task_id)
        end
        player:update_accept()
        return true
    end
    return false
end

function TaskMgr:try_acceept_tasks(player, tconfs)
    for _, conf in pairs(tconfs) do
        self:try_acceept_task(player, conf)
    end
end

--刷新可接循环任务
function TaskMgr:refresh_loop_task(player, task_id, type)
    local prototype = self:init_loop_prototype(player, type)
    if not prototype then
        log_warn("[TaskMgr][refresh_loop_task] build failed! player_id:%s, task_id:%s, type:%s", player:get_id(), task_id, type)
        return
    end
    local old_task = player:get_task(task_id)
    if old_task then
        old_task:close()
    end
    local task = self:create_loop_task(player, task_id, prototype)
    player:receive_loop_task(task, task_id)
end

--更新可接循环任务
function TaskMgr:update_accept_looptask(player, level)

    for _, type in ipairs(LOOP_TASK_TYPE_LIST) do
        local taskc = self:get_loop_task(level, type)
        if taskc then
            for index = 1, taskc.order_num do
                local task_id = taskc.task_id * 100 + index
                local task = player:get_task(task_id)
                if task and (not task:is_stop()) then
                    goto continue
                end
                self:refresh_loop_task(player, task_id, type)
                :: continue ::
            end
        end
    end
end

--更新固定日常任务
function TaskMgr:update_fix_dailytask(player)

    for _, task_id in pairs(self.dailys) do
        if not player:get_task(task_id) then
            local conf = task_db:find_one(task_id)
            if condition_mgr:check_condition(player, conf.condition) then
                local task = self:create_task(player, task_id)
                player:receive_task(task, task_id)
            end
        end
    end
end

--更新随机日常任务
function TaskMgr:update_rand_dailytask(player, level)
    local taskc = self:get_loop_task(level, DAILY_TASK)
    if taskc then
        local diff_num = taskc.order_num - player:count_rand_daily()
        if diff_num > 0 then
            local trand = self.daily_rands[level]
            if trand then
                local daily_rand = trand:clone()
                while diff_num > 0 do
                    if not self:build_daily_task(player, daily_rand) then
                        break
                    end
                    diff_num = diff_num - 1
                end
            end
        end
    end
end

--判定是否可接
function TaskMgr:can_receive(player, conf)
    local task_id, child_id = conf.id, conf.chain

    if player:get_level() < conf.level  then
        return false
    end
    --已经领取
    if player:get_task(task_id) then
        return false
    end
    --已经完成
    if self:is_finished(player, child_id, task_id) then
        return false
    end
    --查看前置任务
    if not self:is_finished(player, child_id, conf.pre_task) then
        return false
    end
    --任务条件检查
    if conf.condition == nil then
        return true
    end

    -- TODO 验证前置条件
    --if not player:check_conditions(conf.condition) then
    --    return false
    --end
    --if not condition_mgr:check_condition(player, conf.condition) then
    --    return false
    --end
    return true
end

--查找任务
function TaskMgr:find_task(task_id)
    return task_db:find_one(task_id)
end

--创建任务
function TaskMgr:create_task(player, task_id, child_id)
    local conf = task_db:find_one(task_id)
    if conf then
        local task = Task(player, task_id, conf)
        self:build_child_task(task, child_id or conf.child_id)
        return task
    else
        log_warn("[TaskMgr][create_task] is fail, player:{} task_id:{} child_id:{}", player:get_id(), task_id, child_id)
    end
    return nil
end

-- 分配任务
function TaskMgr:allot_task(player, task_id, child_id)
    local task = self:create_task(player, task_id, child_id)
    if not task then
        log_warn("[TaskMgr][allot_task] is fail, player:{} task_id:{} child_id:{}", player:get_id(), task_id, child_id)
        return nil
    end
    player:receive_task(task, task_id)
    return task
end

--创建任务
function TaskMgr:create_loop_task(player, task_id, prototype)
    local task = TaskLoop(player, task_id, prototype)
    self:build_loop_task(task, prototype)
    return task
end

--构建原型
function TaskMgr:build_loop_prototype(task_data)
    local text = text_db:find_one(task_data.text_id)
    local task = ltask_db:find_one(task_data.child_id)
    return {
        text_id = text.id,
        auto_finish = false,
        end_npc = text.npcid,
        type = task_data.type,
        cd_time = task.cd_time,
        id = task_data.child_id,
        rewards = task_data.rewards,
        targets = task_data.targets,
        refresh_time = task.refresh_time
    }
end

--加载任务
function TaskMgr:load_task_from_data(player, task_id, task_data)
    local type = task_data.type
    if not type then
        return
    end
    if self:is_loop_task(type) then
        local prototype = self:build_loop_prototype(task_data)
        return self:create_loop_task(player, task_id, prototype)
    end
    return self:create_task(player, task_id, task_data.child_id)
end

--构建日常任务
function TaskMgr:build_daily_task(player, daily_rand)
    local task_id = daily_rand:rand_wheel()
    if task_id then
        if not player:get_task(task_id) then
            local conf = task_db:find_one(task_id)
            if not condition_mgr:check_condition(player, conf.condition) then
                return false
            end
            local task = self:create_task(player, task_id)
            player:receive_task(task, task_id)
            return true
        end
    end
    return false
end

--构建循环任务
function TaskMgr:build_loop_task(task, prototype)
    local targets = {}
    for i, ctarget in ipairs(prototype.targets) do
        --设置并行
        task:set_relation(1)
        local conf = { target_no = i, type = Behavior.COST, args = { ctarget.item_id, ctarget.count } }
        local target = self:build_target(task, conf)
        targets[i] = target
    end
    task:save_child_id(prototype.id)
    task:reset_target(targets)
end

--构建子任务
function TaskMgr:build_child_task(task, child_id)
    local targets = {}
    local tconfs = target_db:find_group(child_id)
    if tconfs == nil or next(tconfs) == nil then
        log_err("[TaskMgr][build_child_task] config error next_id=%s", child_id)
        return false
    elseif task:get_proto_id() == child_id then
        log_err("[TaskMgr][build_child_task] config error task_proto_id == next_id:%s", child_id)
        return false
    end
    for _, conf in pairs(tconfs) do
        task:set_next_id(conf.next_id)
        task:set_relation(conf.relation)
        local target = self:build_target(task, conf)
        targets[conf.target_no] = target
    end
    task:save_child_id(child_id)
    task:reset_target(targets)
    task:save_create_time(quanta.now)
    return true
end

--构建任务
function TaskMgr:build_target(task, conf)
    local TTaregt = TARGET_LIST[conf.type]
    if TTaregt == nil then
        log_err("[TaskMgr][build_target] type:{}", conf.type)
    end
    return TTaregt(task, conf)
end

--是否完成
function TaskMgr:is_finished(player, chain_id, task_id)
    if not task_id then
        return true
    end
    local chain_task = player:get_chain_task(chain_id)
    if not chain_task then
        return false
    end
    return chain_task >= task_id
end

--计算加速cost
function TaskMgr:calc_speed_cost(player, task)
    if task:is_finished() then
        local lvl = player:get_level()
        local timestamp = task:get_timestamp()
        local diff_time = timestamp - quanta.now
        local taskc = self:get_loop_task(lvl, task:get_type())
        if taskc and timestamp > 0 and diff_time > 0 then
            local time = mceil(diff_time / 60)
            for item_id, ratio in pairs(taskc.ratio or {}) do
                return { [item_id] = ratio * time }
            end
        end
    end
end

--计算刷新cost
function TaskMgr:calc_refresh_cost(player, task)
    if task:is_finished() then
        return
    end
    local lvl = player:get_level()
    local taskc = self:get_loop_task(lvl, task:get_type())
    if taskc then
        return taskc.price
    end
end

function TaskMgr:reward_talk(player, talk_id)
    local conf = talk_db:find_one(talk_id)
    if conf and conf.rewards then
        player:reward_items(conf.rewards, OBTAIN_OCHAT)
    end
end

function TaskMgr:get_player_refresh_time(player)
    local count = player:get_loop_count() + 1
    if count > self.max_loop_count then
        count = self.max_loop_count
    end
    local conf = loop_daily_db:find_one(count)
    return conf.refresh_time
end


function TaskMgr:get_task_cond_list(player)
    local task_info = {}
    local lv = player:get_level()
    for _, conf in task_db:iterator() do
        if self:is_history_task(conf.type) and conf.level <= lv then
            if not self:try_acceept_task(player, conf) then
                local conds = { }
                local info = {
                    task_id = conf.id,
                    type = conf.type,
                    desc = conf.desc,
                    cond = { }
                }

                self:pack_task_cond(conf, conds)
                for cond_id, cond_vals in pairs(conds) do
                    local cond_info = {
                        cond_id = cond_id,
                        value = { }
                    }
                    for _, cond_item in ipairs(cond_vals) do
                        if cond_item ~= nil then
                            local val = player:check_once_condition(cond_id, cond_item)
                            for i, v in pairs(val) do
                                cond_info.value[i] = v
                            end
                        end
                    end
                    table.insert(info.cond, cond_info)
                end
                table.insert(task_info, info)
            end
        end
    end
    return task_info
end

-- export
quanta.task_mgr = TaskMgr()

return TaskMgr
