--action_mgr.lua
local log_err   = logger.err
local log_debug = logger.debug
local qsuccess  = quanta.success

local dropor    = quanta.get("dropor")

local Behavior  = enum("Behavior")

local ActionMgr = singleton()
local prop      = property(ActionMgr)
prop:reader("actions", {}) --actions

function ActionMgr:__init()
    self.actions = {
        [Behavior.ITEM]   = ActionMgr.exec_item_action,
        [Behavior.DROP]   = ActionMgr.exec_drop_action,
        [Behavior.STORY]  = ActionMgr.exec_story_action,
        [Behavior.GROUND] = ActionMgr.exec_ground_action,
        --[Behavior.EMAIL]  = ActionMgr.exec_email_action, TODO 暂时不要了
    }
end

function ActionMgr:execute(action, target)
    local executor = self.actions[action]
    if executor then
        executor(self, target:get_entity(), target)
    end
end

function ActionMgr:exec_ground_action(player, target)
    local block_id = 0
    local ok, code = player:call_service("scene", "rpc_block_unlock", target:get_param_i(2))
    if not qsuccess(code, ok) then
        log_err("[ActionMgr][exec_ground_action] rpc_block_unlock is fail player:{} block_id:{}", player:get_id(), block_id)
        return
    end
    log_debug("[ActionMgr][exec_ground_action] rpc_block_unlock success player:{} block_id:{}", player:get_id(), block_id)
end

function ActionMgr:exec_email_action(task, target)
    local player = task:get_player()
    local template_id = target:get_param_i(1)
    player:send_template_mail(template_id)
    log_debug("[ActionMgr][exec_email_action] receive email template_id (%s)!", template_id)
end

function ActionMgr:exec_story_action(task, target)
    local player   = task:get_player()
    local task_ids = target:get_param_t(1)
    local task_mgr = quanta.get("task_mgr")
    for _, task_id in pairs(task_ids or {}) do
        local story = task_mgr:create_task(player, task_id)
        player:receive_task(story, task_id)
        player:sync_task(story)
    end
    log_debug("[ActionMgr][exec_story_action] receive story (%s)!", task_ids)
end

function ActionMgr:exec_drop_action(task, target)
    local drop_items = {}
    local drop_id = target:get_param_i(1)
    if not dropor:execute(drop_id, drop_items) then
        log_err("[ActionMgr][exec_drop_action] execute drop (%s) failed!", drop_id)
        return
    end
    log_debug("[ActionMgr][exec_drop_action] add drop_id (%s)!", drop_id)
    task:reward(drop_items)
end

function ActionMgr:exec_item_action(task, target)
    local items = target:get_param_t(1)
    if items == nil then
        log_err("[ActionMgr][exec_item_action] items:nil")
        return
    end
    log_debug("[ActionMgr][exec_item_action] add items (%s)!", items)
    task:reward(items)
end

quanta.action_mgr = ActionMgr()

return ActionMgr
