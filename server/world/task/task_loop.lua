--task_loop.lua

local update_mgr    = quanta.get("update_mgr")

local Task  = import("world/task/task.lua")

local TaskLoop = class(Task)
local dbprop = db_property(TaskLoop, "player_task")
dbprop:store_value("timestamp", 0)      --任务时间戳

function TaskLoop:__init(player, id, conf)
end

function TaskLoop:close()
    Task.close(self)
    update_mgr:detach_second5(self)
end

function TaskLoop:is_loop_task()
    return true
end

function TaskLoop:load_data(data)
    local timestamp = data.timestamp or 0
    if timestamp > 0 then
        self:wait_time(timestamp)
    end
    Task.load_data(self, data)
end

function TaskLoop:on_second5()
    if quanta.now > self.timestamp then
        update_mgr:detach_second5(self)
        self:time_out()
    end
end

function TaskLoop:wait_time(time)
    self:save_timestamp(time)
    update_mgr:attach_second5(self)
end

function TaskLoop:pack2db()
    return {
        type = self.type,
        status = self.status,
        child_id = self.child_id,
        progress = self.progress,
        timestamp = self.timestamp,
        targets = self.prototype.targets,
        rewards = self.prototype.rewards,
        text_id = self.prototype.text_id,
        create_time = self.create_time,
    }
end

function TaskLoop:pack2client()
    return {
        type = self.type,
        status = self.status,
        child_id = self.child_id,
        progress = self.progress,
        timestamp = self.timestamp,
        targets = self.prototype.targets,
        rewards = self.prototype.rewards,
        text_id = self.prototype.text_id,
        create_time = self.create_time,
    }
end

function TaskLoop:pack_content()
    return {
        task_id = self.id,
        status = self.status,
        progress = self.progress,
        child_id = self.child_id,
        timestamp = self.timestamp,
        create_time = self.create_time,
    }
end

return TaskLoop
