--task.lua
local log_info          = logger.info
local event_mgr         = quanta.get("event_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")

local SACTIVE           = protobuf_mgr:enum("task_status", "ACTIVE")
local SDONE             = protobuf_mgr:enum("task_status", "DONE")
local SFINISH           = protobuf_mgr:enum("task_status", "FINISH")
local SINIT             = protobuf_mgr:enum("task_status", "INIT")
local SREAWRD           = protobuf_mgr:enum("task_status", "REAWRD")
local SFAIL             = protobuf_mgr:enum("task_status", "FAIL")
local SSTOP             = protobuf_mgr:enum("task_status", "STOP")

local COST_TASK         = protobuf_mgr:enum("cost_reason", "NID_COST_TASK")
local OBTAIN_TASK       = protobuf_mgr:enum("obtain_reason", "NID_OBTAIN_TASK")

local ORDER_TASK        = protobuf_mgr:enum("task_type", "ORDER")
local STORY_TASK        = protobuf_mgr:enum("task_type", "STORY")
local ORDER_BUILD       = protobuf_mgr:enum("task_type", "BUILDING")
local BIG_TOWN_ORDER    = protobuf_mgr:enum("task_type", "BIG_TOWN_ORDER")
local SHORT_TOWN_ORDER  = protobuf_mgr:enum("task_type", "SHORT_TOWN_ORDER")

local Task = class()
local prop = property(Task)
prop:reader("id", 0)
prop:reader("player", nil)
prop:reader("player_id", nil)
prop:reader("prototype", nil)       --任务原型
prop:accessor("next_id", nil)       --下一个子任务
prop:accessor("relation", nil)      --目标关系
prop:accessor("targets", {})        --目标列表

local dbprop = db_property(Task, "player_task")
dbprop:store_value("type", 0)           --任务类型
dbprop:store_value("status", 0)         --任务状态
dbprop:store_value("child_id", nil)     --子任务ID
dbprop:store_value("proto_id", nil)     --proto_id
dbprop:store_value("create_time", 0)    --任务创建时间
dbprop:store_values("progress", {})     --任务目标进度

function Task:__init(player, id, conf)
    self.id = id
    self.type = conf.type
    self.player = player
    self.prototype = conf
    self.proto_id = conf.id
    self.player_id = player:get_id()
end

function Task:close()
    for _, target in ipairs(self.targets) do
        target:deactive()
    end
end

function Task:reset_target(targets)
    for _, target in ipairs(self.targets) do
        target:deactive()
    end
    self:save_progress({})
    self.targets = targets
end

function Task:load_data(data)
    for id, progress in pairs(data.progress) do
        local target = self.targets[id]
        if target then
            target:set_progress(progress)
            self.progress[id] = progress
        end
    end
    self.status = data.status
    self.create_time = data.create_time
    --下一帧更新
    event_mgr:fire_frame(function()
        self:update_target()
    end)
end

function Task:is_init()
    return self.status == SINIT
end

function Task:is_reward()
    return self.status == SREAWRD
end

function Task:is_finished()
    return self.status >= SFINISH
end

function Task:is_stop()
    return self.status >= SSTOP
end

function Task:is_loop_task()
    return false
end

function Task:is_order_task()
    local ttype = self.type
    if self:is_town_order_task(ttype) then
        return true
    end
    return ttype == ORDER_TASK or ttype == STORY_TASK or ttype == ORDER_BUILD
end

function Task:is_town_order_task(ttype)
    return ttype == BIG_TOWN_ORDER or ttype == SHORT_TOWN_ORDER
end

function Task:is_town_short_order_task()
    return self.type == SHORT_TOWN_ORDER
end

function Task:is_rand_daily()
    local level_max = self.prototype.level_max
    return level_max and level_max > 0
end

function Task:get_chain_id()
    return self.prototype.chain or self.proto_id
end

function Task:get_proto_id()
    return self.prototype.id
end

function Task:get_next_task_id()
    return self.prototype.next_task
end

function Task:update_target()
    local old_state = self.status
    if old_state == SINIT or old_state >= SFINISH then
        return
    end
    local count = 0
    for _, target in ipairs(self.targets) do
        --relation == 0串行1并行2任意
        if target:is_complete() then
            if self.relation == 2 then
                count = 0
                break
            end
        else
            if self.relation == 0 and count > 0 then
                --串行目标前面目标失败需要回退后面的目标
                target:deactive()
                goto continue
            end
            target:active()
            count = count + 1
        end
        :: continue ::
    end
    if old_state ~= self.status then
        return
    end
    if count > 0 then
        self:status_changed(SACTIVE)
        return
    end
    log_info("[Task][update_target] task: %s-%s count %s state=%s", self.id, self.child_id, count, self.status)
    if not self:is_order_task() then
        self:finish_target()
    end
    if self.next_id then
        event_mgr:fire_frame("on_task_upgraded", self.player, self, self.next_id)
        return
    end
    self:status_changed(SDONE)
end

function Task:auto_receive()
    if self:is_order_task() then
        return true
    end
    return self.prototype.start_npc == nil
end

function Task:reward(items)
    self.player:reward_items(items, OBTAIN_TASK, true)
end

function Task:cost(item_id, count)
    return self.player:cost_item(item_id, count, COST_TASK)
end

function Task:reset()
    self:save_progress({})
    self:status_changed(SACTIVE)
end

function Task:active()
    self:status_changed(SACTIVE)
    self:update_target()
    local proto_id = self:get_proto_id()
    self.player:notify_event("on_task_accept", self.id, self.prototype.type, proto_id)
end

function Task:finish()
    if self:is_order_task() then
        self:finish_target()
    end
    self:reward(self.prototype.rewards)
    self:status_changed(SFINISH)
end

function Task:finish_target()
    for _, target in ipairs(self.targets) do
        target:finish()
    end
end

-- 客户端手动完成的时候 做最后判断
function Task:cost_check()
    for _, target in ipairs(self.targets) do
        if not target:on_cost() then
            return false
        end
    end
    return true
end

function Task:get_target(target_id)
    for _, target in ipairs(self.targets) do
        local conf = target:get_prototype()
        if conf.target_id == target_id then
            return target
        end
    end
    return nil
end

--GM完成
function Task:complete()
    if self.status == SACTIVE then
        for _, target in ipairs(self.targets) do
            target:complete()
        end
        self:finish()
    end
end

--放弃订单
function Task:giveup()
    self:status_changed(SFAIL)
end

--强制刷新订单
function Task:refresh()
    self:status_changed(SFINISH)
end

function Task:time_out()
    if self.status == SDONE then
        self:status_changed(SREAWRD)
    elseif self.status == SFINISH then
        self:status_changed(SSTOP)
    elseif self.status == SFAIL then
        self:status_changed(SSTOP)
    end
end

function Task:wait_time(time)
end

function Task:start_wait(cd)
    if not cd or cd <= 0 then
        return false
    end
    self:wait_time(quanta.now + cd)
    return true
end

--状态改变
function Task:status_changed(status)
    if self.status == status then
        return
    end
    log_info("[Task][status_changed] task: (%s,%s) status %s-=>%s", self.id, self.child_id, self.status, status)
    self:save_status(status)
    local proto = self.prototype
    if status == SDONE then
        local cd = proto.cd_time
        if not self:start_wait(cd) then
            self:status_changed(SREAWRD)
            return
        end
    elseif status == SREAWRD then
        if proto.auto_finish then
            self:finish()
            return
        end
    elseif status == SFINISH then
        event_mgr:fire_frame("on_task_finished", self.player, self)
        local refresh_time = proto.refresh_time
        if self:is_town_short_order_task() then
            refresh_time = self.player:get_refresh_time()
        end
        if not self:start_wait(refresh_time) then
            self:status_changed(SSTOP)
            return
        end
    elseif status == SFAIL then
        local refresh_time = proto.refresh_time
        if not self:start_wait(refresh_time) then
            self:status_changed(SSTOP)
            return
        end
    elseif status == SSTOP then
        self.player:finish_task(self.id)
        event_mgr:fire_frame("on_task_stop", self.player, self)
    end
    self.player:sync_task(self)
end

function Task:pack2client()
    return {
        status = self.status,
        proto_id = self.proto_id,
        child_id = self.child_id,
        progress = self.progress,
        create_time = self.create_time
    }
end

function Task:pack_content()
    return {
        task_id = self.id,
        status = self.status,
        proto_id = self.proto_id,
        child_id = self.child_id,
        progress = self.progress,
        create_time = self.create_time
    }
end

function Task:pack_progress()
    return {
        task_id = self.id,
        progress = self.progress,
    }
end

return Task
