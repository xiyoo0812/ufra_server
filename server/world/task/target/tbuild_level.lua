--tbuild_level.lua
local log_debug = logger.debug
local new_guid  = codec.guid_new
local Target    = import("world/task/target.lua")

local TBuildLevel = class(Target)
local prop = property(TBuildLevel)
prop:reader("group_id", 0)    --目标建筑
prop:reader("level", 1) --建筑类型，对应配置表中tab字段

function TBuildLevel:__init(task, conf)
    self.group_id = self:get_arg_i(1, 0)
    self.count = self:get_arg_i(2, 1)
    self.level = self:get_arg_i(3, 1)
end

function TBuildLevel:on_building_level_up(group_id, level)
    log_debug("[TBuildLevel][on_building_level_up] group_id:{} level:{}", group_id, level)
    if self.group_id == 0 or self.group_id == group_id then
        if level >= self.level then
            self:forward()
        end
    end
end

--子类实现
------------------------------------------------
function TBuildLevel:on_active()
    self.guid = new_guid()
    self.player:watch_event(self, "on_building_level_up")
end

function TBuildLevel:on_deactive()
    self.player:unwatch_event(self, "on_building_level_up")
end

function TBuildLevel:on_done()
    self.player:unwatch_event(self, "on_building_level_up")
end

return TBuildLevel
