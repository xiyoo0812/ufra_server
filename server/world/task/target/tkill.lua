--tkill.lua

local Target    = import("world/task/target.lua")

local TKill = class(Target)
local prop = property(TKill)
prop:reader("monster_id", nil)      --npcid

function TKill:__init(task, conf)
    self.monster_id = self:get_arg_i(1)
    self.count = self:get_arg_i(2, 1)
end

function TKill:on_kill_monster(monster_id)
    if self.monster_id == 0 or self.monster_id == monster_id then
        self:forward()
    end
end

--子类实现
------------------------------------------------
function TKill:on_active()
    self.player:watch_event(self, "on_kill_monster")
end

function TKill:on_deactive()
    self.player:unwatch_event(self, "on_kill_monster")
end

return TKill
