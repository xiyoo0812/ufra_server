-- tgroup.lua
local Target    = import("world/task/target.lua")

local TCommits    = class(Target)
local prop = property(TCommits)
prop:reader("items", {})  --物品信息
prop:reader("commits",{}) --提交的物品信息
function TCommits:__init(task, conf)
    self.items = self:get_arg_t(1)
end

function TCommits:on_active()
    self.player:watch_event(self, "on_item_commit")
end

function TCommits:on_deactive()
    self.player:unwatch_event(self, "on_item_commit")
end

function TCommits:on_item_commit(item_id, num)
    if self.items[item_id] == nil then
        return
    end
    self.commits[item_id] = num
    for id, count in pairs(self.commits) do
        if count < self.items[id] then
            return
        end
    end
    self:forward()
end

return TCommits