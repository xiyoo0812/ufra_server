--tblock.lua
local Target    = import("world/task/target.lua")
local TBlock = class(Target)
local prop = property(TBlock)
prop:reader("ground_id", 0)

function TBlock:__init(task, conf)
    self.count = self:get_arg_i(1, 1)
    self.ground_id = self:get_arg_i(2, 0)
end

function TBlock:on_active()
    self.player:watch_event(self, "on_block_finish")
end

function TBlock:on_block_finish(ground_id, block_id)
    if self.ground_id == 0 then
        self:forward()
    elseif self.ground_id == ground_id then
        self:forward()
    end
end

function TBlock:on_deactive()
    self.player:unwatch_event(self, "on_block_finish")
end

return TBlock