--tplace.lua


local Target = import("world/task/target.lua")

local TProduct = class(Target)
local prop = property(TProduct)
prop:reader("item_id", 0)

function TProduct:__init(task, conf)
    self.item_id = self:get_arg_i(1, 0)
    self.count = self:get_arg_i(2, 1)
end

function TProduct:on_product_done(item_id, num)

    if self.item_id == 0 or self.item_id == item_id then
        self:forward(num)
    end
end

--子类实现
------------------------------------------------
function TProduct:on_active()
    self.player:watch_event(self, "on_product_done")
end

function TProduct:on_deactive()
    self.player:unwatch_event(self, "on_product_done")
end

function TProduct:on_done()
    self.player:unwatch_event(self, "on_product_done")
end

return TProduct
