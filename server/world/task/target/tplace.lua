--tplace.lua
local log_debug = logger.debug

local Target = import("world/task/target.lua")

local TPlace = class(Target)
local prop = property(TPlace)
prop:reader("build_id", nil)    --目标建筑
prop:reader("type", nil) -- 建筑类型，对应配置表中的tab字段

function TPlace:__init(task, conf)
    self.build_id = self:get_arg_i(1, 0)
    self.count = self:get_arg_i(2, 1)
    self.type = self:get_arg_i(3, nil)
end

function TPlace:on_place_utensil(build_id, type)
    log_debug("[TPlace][on_place_utensil] build_id: %s", build_id)

    if self.type ~= nil then
        if self.type == 0 or self.type == type then
            self:forward()
        end
        return
    end
    if self.build_id == 0 or self.build_id == build_id then
        self:forward()
    end
end

--子类实现
------------------------------------------------
function TPlace:on_active()
    self.player:watch_event(self, "on_place_utensil")
end

function TPlace:on_deactive()
    self.player:unwatch_event(self, "on_place_utensil")
end

function TPlace:on_done()
    self.player:unwatch_event(self, "on_place_utensil")
end

return TPlace
