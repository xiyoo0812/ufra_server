--tenter.lua
local new_guid  = codec.guid_new
local Target    = import("world/task/target.lua")

local TEnter    = class(Target)
local prop      = property(TEnter)
prop:reader("scene_id", 0)  --场景id

function TEnter:__init(task, conf)
    self.scene_id = self:get_arg_t(1, 0)
end

function TEnter:on_scene_changed(map_id)
    if self.scene_id == 0 then
        self:forward()
    elseif self.scene_id == map_id then
        self:forward()
    end
end

function TEnter:on_active()
    self.guid = new_guid()
    self.player:watch_event(self, "on_scene_changed")
end

function TEnter:on_deactive()
    self.player:unwatch_event(self, "on_scene_changed")
end

return TEnter
