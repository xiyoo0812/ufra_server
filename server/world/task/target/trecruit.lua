
--trecruit.lua

local Target = import("world/task/target.lua")

local TRecruit = class(Target)
local prop = property(TRecruit)
prop:reader("npc_id", nil)

function TRecruit:__init(task, conf)
    self.npc_id = self:get_arg_i(1, 0)
    self.count = self:get_arg_i(2, 1)
end

function TRecruit:on_active()
    self.player:watch_event(self, "on_npc_recruit")
end

function TRecruit:on_deactive()
    self.player:unwatch_event(self, "on_npc_recruit")
end

function TRecruit:on_npc_recruit(proto_id)
    if self.npc_id == 0 or self.npc_id == proto_id then
        self:forward()
    end
end

return TRecruit
