-- twear_equip.lua

local Target = import("world/task/target.lua")

local TWearEquip = class(Target)
local prop = property(TWearEquip)
prop:reader("equip_id", 0)

function TWearEquip:__init(task, conf)
    self.equip_id = self:get_arg_i(1, 0)
end

function TWearEquip:on_install_equip(item_id)
    if self.equip_id == 0 or self.equip_id == item_id then
        self:forward()
    end
end

function TWearEquip:on_active()
    self.player:watch_event(self, "on_install_equip")
end

function TWearEquip:on_deactive()
    self.player:unwatch_event(self, "on_install_equip")
end

return TWearEquip