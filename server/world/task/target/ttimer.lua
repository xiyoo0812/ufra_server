--ttimer.lua

local timer_mgr     = quanta.get("timer_mgr")

local Target        = import("world/task/target.lua")

local SECOND_10_MS  = quanta.enum("PeriodTime", "SECOND_10_MS")

local TTimer = class(Target)
local prop = property(TTimer)
prop:reader("timer_id", nil)    --等待时间

function TTimer:__init(task, conf)
end

--子类实现
------------------------------------------------
function TTimer:on_active()
    local time = self:get_arg_i(1, SECOND_10_MS)
    self.timer_id = timer_mgr:once(time, function()
        self:forward()
    end)
end

function TTimer:on_deactive()
    if self.timer_id then
        timer_mgr:unregister(self.timer_id)
    end
end

return TTimer
