--tcost.lua

local log_err = logger.err
local Target = import("world/task/target.lua")

local TCost = class(Target)
local prop = property(TCost)
prop:reader("item_id", nil)  --item_id

function TCost:__init(task, conf)
    self.item_id = self:get_arg_i(1)
    self.count = self:get_arg_i(2, 1)
end

-- 都是在接到任务计算进度，不和背包关联
function TCost:on_item_changed(item_id, num)
    if self.item_id == item_id then
        local cur_num = self.player:check_item_num(self.item_id, self.count)
        self:update_progress(cur_num)
    end
end

--子类实现
------------------------------------------------
function TCost:on_active()
    local cur_num = self.player:check_item_num(self.item_id, self.count)
    if cur_num > 0 then
        self:update_progress(cur_num)
    end
    self.player:watch_event(self, "on_item_changed")
end

function TCost:on_deactive(close)
    if close then
        self.player:unwatch_event(self, "on_item_changed")
    end
end

function TCost:on_cost()

    if self.task:cost(self.item_id, self.count) then
        self.player:unwatch_event(self, "on_item_changed")
        return true
    end
    self:on_item_changed(self.item_id, 0)
    log_err("[TCost][on_cost] cost fail {}:{}", self.item_id, self.count)
    return false
end

return TCost
