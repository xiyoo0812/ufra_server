--tcollect.lua

local Target    = import("world/task/target.lua")

local TCollect = class(Target)
local prop = property(TCollect)
prop:reader("item_id", nil)  --item_id

function TCollect:__init(task, conf)
    self.item_id = self:get_arg_i(1, 0)
    self.count = self:get_arg_i(2, 1)
end

function TCollect:on_item_changed(item_id, num)
    if self.item_id == 0 then
        if num > 0 then
            self:forward(num)
        end
        return
    end
    if self.item_id == item_id then
        if num > 0 then
            self:forward(num)
            return
        end
        local cur_num = self.player:check_item_num(self.item_id, self.count)
        self:update_progress(cur_num)
    end
end

--子类实现
------------------------------------------------
function TCollect:on_active()
    local cur_num = self.player:check_item_num(self.item_id, self.count)
    if cur_num > 0 then
        self:update_progress(cur_num)
    end
    self.player:watch_event(self, "on_item_changed")
end

function TCollect:on_deactive()
    self.player:unwatch_event(self, "on_item_changed")
end

return TCollect
