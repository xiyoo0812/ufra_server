--tenergy.lua

local AttrID_ENERGY = quanta.enum("AttrID", "ATTR_ENERGY")

local Target = import("world/task/target.lua")

local TEnergy = class(Target)

function TEnergy:__init(task, conf)
    self.count = self:get_arg_i(2, 1)
end

function TEnergy:on_active()
    self.player:watch_event(self, "on_attritem_cost")
end

function TEnergy:on_deactive()
    self.player:unwatch_event(self, "on_attritem_cost")
end

function TEnergy:on_attritem_cost(item_id, attr_id, num, reason)
    if attr_id == AttrID_ENERGY then
        self:forward(num)
    end
end

return TEnergy
