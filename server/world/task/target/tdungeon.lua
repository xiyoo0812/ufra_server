--tdungeon.lua

local log_debug = logger.debug
local new_guid  = codec.guid_new

local Target    = import("world/task/target.lua")

local TDungeon = class(Target)
local prop = property(TDungeon)
prop:reader("dungeon_id", 0) --副本id

function TDungeon:__init(task, conf)
    self.dungeon_id = self:get_arg_i(1, 0)
end

function TDungeon:on_active()
    self.guid = new_guid()
    self.fail_guid = new_guid()
    self.player:watch_event(self, "on_dungeon_fail")
    self.player:watch_event(self, "on_dungeon_finish")
end

function TDungeon:on_dungeon_finish(entity_id, dungeon_id)
    log_debug("[TDungeon][on_dungeon_finish] self.id:{} dungeon_id({})", self.dungeon_id, dungeon_id)
    if self.dungeon_id == 0 or self.dungeon_id == dungeon_id then
        self:forward()
    end
end

function TDungeon:on_dungeon_fail(player_id, dungeon_id)
    if self.dungeon_id == dungeon_id then
        local task_id = self.task:get_id()
        self.player:notify_event("on_task_reset", task_id)
    end
end

function TDungeon:on_deactive()
    self.player:unwatch_event(self, "on_dungeon_fail")
    self.player:unwatch_event(self, "on_dungeon_finish")
end

return TDungeon