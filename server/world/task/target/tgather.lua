--tgather.lua
local log_debug = logger.debug
local new_guid  = codec.guid_new
local Target    = import("world/task/target.lua")

local TGather = class(Target)
local prop = property(TGather)
prop:reader("res_id", nil)  --资源id
prop:reader("type", nil) --采集工具类型

function TGather:__init(task, conf)
    self.res_id = self:get_arg_i(1, 0)
    self.count = self:get_arg_i(2, 1)
end

function TGather:on_resource_gather(res_id)
    log_debug("[TGather][on_resource_gather] res_id: %s", res_id)
    if self.res_id == 0 or self.res_id == res_id then
        self:forward()
    end
end

--子类实现
------------------------------------------------
function TGather:on_active()
    self.guid = new_guid()
    self.player:subscriber("scene", self.guid, "on_resource_gather", self.res_id)
    self.player:watch_event(self, "on_resource_gather")
end

function TGather:on_deactive()
    if self.guid ~= nil then
        self.player:unsubscriber("scene", "on_resource_gather", self.guid)
    end
    self.player:unwatch_event(self, "on_resource_gather")
end

function TGather:on_done()
    self.player:unwatch_event(self, "on_resource_gather")
end

return TGather
