--tresident.lua

local log_debug = logger.debug
local Target    = import("world/task/target.lua")

local TResidentCount = class(Target)

function TResidentCount:__init(task, conf)
    self.count = self:get_arg_i(2, 1)
end

function TResidentCount:on_checkin_count(count)
    log_debug("[TResidentCount][on_checkin_count] count:{}", count)
    self:update_progress(count)
end

--子类实现
------------------------------------------------
function TResidentCount:on_active()
    self.player:watch_event(self, "on_checkin_count")
end

function TResidentCount:on_deactive()
    self.player:unwatch_event(self, "on_checkin_count")
end

function TResidentCount:on_done()
    self.player:unwatch_event(self, "on_checkin_count")
end

return TResidentCount
