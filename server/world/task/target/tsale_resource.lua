-- tsale_resource.lua

local Target = import("world/task/target.lua")

local TSaleResource = class(Target)
local prop = property(TSaleResource)
prop:reader("item_id", 0)

function TSaleResource:__init(task, conf)
    self.item_id = self:get_arg_i(1, 0)
end

function TSaleResource:on_item_recycle(item_id)
    if self.item_id == 0 or self.item_id == item_id then
        self:forward()
    end
end

function TSaleResource:on_active()
    self.player:watch_event(self, "on_item_recycle")
end

function TSaleResource:on_deactive()
    self.player:unwatch_event(self, "on_item_recycle")
end

return TSaleResource