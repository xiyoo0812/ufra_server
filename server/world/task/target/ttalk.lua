--ttalk.lua
local log_debug = logger.debug

local Target    = import("world/task/target.lua")

local TTalk = class(Target)
local prop = property(TTalk)
prop:reader("npc_id", nil)  --目标npc
prop:reader("talk_id", 0)

function TTalk:__init(task, conf)
    self.npc_id = self:get_arg_i(1, 0)
    self.talk_id = self:get_arg_i(3, 0)
end

function TTalk:on_talkwith_npc(npc_id, _, args)
    local talk_id = tonumber(args[1])
    log_debug("[TTalk][on_talkwith_npc] npc_id: %s-%s talk_id: %s-%s", self.npc_id, npc_id, self.talk_id, talk_id)
    if self.npc_id == 0 or self.npc_id == npc_id then
        if self.talk_id == 0 then
            self:forward()
        elseif self.talk_id == talk_id then
            self:forward()
        end
    end
end

--子类实现
------------------------------------------------
function TTalk:on_active()
    self.player:watch_event(self, "on_talkwith_npc")
end

function TTalk:on_deactive()
    self.player:unwatch_event(self, "on_talkwith_npc")
end

function TTalk:on_done()
    self.player:unwatch_event(self, "on_talkwith_npc")
end

return TTalk
