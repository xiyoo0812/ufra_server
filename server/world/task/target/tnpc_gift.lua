--npc_gift.lua

local log_debug = logger.debug
local Target    = import("world/task/target.lua")

local TNpcGift = class(Target)
local prop = property(TNpcGift)
prop:reader("npc_id", 0)      --npcid
prop:reader("item_id", 0)    --建筑id


function TNpcGift:__init(task, conf)
    self.npc_id = self:get_arg_i(1, 0)
    self.count = self:get_arg_i(2, 1)
    self.item_id = self:get_arg_i(3, 0)
end

function TNpcGift:on_recv_gift(npc_id, item_id, num)
    log_debug("[TNpcGift][on_recv_gift] npc_id:{} item_id:{} num:{}", npc_id, item_id, num)
    if self.npc_id == 0 then
        if self.item_id == 0 or self.item_id == item_id then
            self:forward(num)
        end
    elseif self.item_id == 0 then
        if self.npc_id == 0 or self.npc_id == npc_id then
            self:forward(num)
        end
    elseif self.npc_id == npc_id and self.item_id == item_id then
        self:forward(num)
    end
end

--子类实现
------------------------------------------------
function TNpcGift:on_active()
    self.player:watch_event(self, "on_recv_gift")
end

function TNpcGift:on_deactive()
    self.player:unwatch_event(self, "on_recv_gift")
end

return TNpcGift
