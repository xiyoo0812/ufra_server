--tlogin.lua
local log_debug = logger.debug

local Target    = import("world/task/target.lua")

local TLogin = class(Target)
function TLogin:__init(task, conf)

end

function TLogin:on_active()
    self:forward()
    log_debug("[TLogin][on_active] login task")
end

return TLogin
