--tgameday.lua
local event_mgr     = quanta.get("event_mgr")
local Target        = import("world/task/target.lua")

local TGameDay = class(Target)

function TGameDay:__init(task, conf)
    self.count = self:get_arg_i(1, 1)
end

function TGameDay:on_active()
    event_mgr:add_trigger(self, "on_game_day_update")
end

function TGameDay:on_game_day_update()
    self:forward()
end

function TGameDay:on_deactive()
    event_mgr:remove_trigger(self, "on_game_day_update")
end

return TGameDay