--temail.lua

local Target = import("world/task/target.lua")

local TEmail = class(Target)
local prop = property(TEmail)
prop:reader("email_id", 0)

function TEmail:__init(task, conf)
    self.email_id = self:get_arg_i(1, 0)
end

function TEmail:on_read_email(email_id)
    if self.email_id == 0 then
        self:forward()
    elseif self.email_id == email_id then
        self:forward()
    end
end

function TEmail:on_active()
    self.player:watch_event(self, "on_read_email")
end

function TEmail:on_deactive()
    self.player:unwatch_event(self, "on_read_email")
end

return TEmail
