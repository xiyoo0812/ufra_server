--tvirtual.lua
local log_debug = logger.debug

local Target    = import("world/task/target.lua")

local TVirtual = class(Target)
local prop = property(TVirtual)
prop:reader("virtual_id", nil)  --虚拟目标

function TVirtual:__init(task, conf)
    self.virtual_id = self:get_arg_i(1)
    self.count = self:get_arg_i(2, 1)
    self.param_id = self:get_arg(3, 0)
end

function TVirtual:on_virtual_target(virtual_id, _, args)
    local id = args[1]
    if self.virtual_id == virtual_id and self.param_id == id then
        log_debug("[TVirtual][on_virtual_target] virtual_id: %s", virtual_id)
        self:forward()
    end
end

--子类实现
------------------------------------------------
function TVirtual:on_active()
    self.player:watch_event(self, "on_virtual_target")
end

function TVirtual:on_deactive()
    self.player:unwatch_event(self, "on_virtual_target")
end

function TVirtual:on_done()
    self.player:unwatch_event(self, "on_virtual_target")
end

return TVirtual
