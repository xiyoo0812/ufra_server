--tfish
-- tlevel.lua
local Target = import("world/task/target.lua")
local log_debug = logger.debug
local TFish = class(Target)
local prop = property(TFish)
prop:reader("level", nil)
prop:reader("type", 0)

function TFish:__init(task, conf)
    self.item_id = self:get_arg_i(1, 0)
end

function TFish:on_fish_done(item_id)
    log_debug("[TFish][on_fish_done] item_id:{}", item_id)
    if self.item_id == 0 or self.item_id == item_id then
        self:forward()
    end
end

function TFish:on_active()
    self.player:watch_event(self, "on_fish_done")
end

function TFish:on_deactive()
    self.player:unwatch_event(self, "on_fish_done")
end

return TFish
