--tbuilding.lua
local log_debug = logger.debug
local new_guid  = codec.guid_new
local Target    = import("world/task/target.lua")

local TBuilding = class(Target)
local prop = property(TBuilding)
prop:reader("build_id", nil)    --目标建筑
prop:reader("type", nil) --建筑类型，对应配置表中tab字段

function TBuilding:__init(task, conf)
    self.build_id = self:get_arg_i(1, 0)
    self.count = self:get_arg_i(2, 1)
    self.type = self:get_arg_i(3)
end

function TBuilding:on_building_finish(build_id, type)
    log_debug("[TBuilding][on_building_finish] build_id: %s", build_id)
    if self.type ~= nil then
        if self.type == 0 or self.type == type then
            self:forward()
        end
        return
    end

    if self.build_id == build_id or self.build_id == 0 then
        self:forward()
    end
end

--子类实现
------------------------------------------------
function TBuilding:on_active()
    self.guid = new_guid()
    self.player:watch_event(self, "on_building_finish")
end

function TBuilding:on_deactive()
    self.player:unwatch_event(self, "on_building_finish")
end

function TBuilding:on_done()
    self.player:unwatch_event(self, "on_building_finish")
end

return TBuilding
