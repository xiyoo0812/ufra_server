--tpartner.lua
local new_guid  = codec.guid_new
local Target    = import("world/task/target.lua")

local TPartner  = class(Target)
local prop = property(TPartner)
prop:reader("npc_id", nil)

function TPartner:__init(task, conf)
    self.npc_id = self:get_arg_i(1, 0)
    self.count = self:get_arg_i(2, 1)
end

function TPartner:on_active()
    self.guid = new_guid()
    self.player:watch_event(self, "on_recv_partner")
end

function TPartner:on_deactive()
    self.player:unwatch_event(self, "on_recv_partner")
end

function TPartner:on_recv_partner(proto_id)
    if self.npc_id == 0 or self.npc_id == proto_id then
        self:forward()
        return
    end
end
return TPartner
