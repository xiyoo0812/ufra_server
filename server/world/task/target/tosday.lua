--tosday.lua

local event_mgr = quanta.get("event_mgr")

local Target = import("world/task/target.lua")
local TOsDay = class(Target)

function TOsDay:__init(task, conf)
    self.count = self:get_arg_i(1, 1)
end

function TOsDay:on_active()
    if not self.player:is_same_day() then
        self:forward()
    end
    if not self:is_complete() then
        event_mgr:add_trigger(self, "on_day_update")
    end
end

function TOsDay:on_day_update(week)
    self:forward()
end

function TOsDay:on_deactive()
    event_mgr:remove_trigger(self, "on_day_update")
end

return TOsDay
