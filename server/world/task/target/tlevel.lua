-- tlevel.lua
local Target = import("world/task/target.lua")

local TLevel = class(Target)
local prop = property(TLevel)
prop:reader("level", nil)
prop:reader("type", 0)

function TLevel:__init(task, conf)
    self.level = self:get_arg_i(1, 0)
    self.type = self:get_arg_i(2, 0)
end

function TLevel:on_level_up(level, up)
    if self.type == 0 then
        if level >= self.level then
            self:forward()
        end
    else
        self:forward(up)
    end

end

function TLevel:on_active()
    if self.type == 0 then
        if self.player:get_level() >= self.level then
            self:forward()
            return
        end
    end
    self.player:watch_event(self, "on_level_up")
end

function TLevel:on_deactive()
    self.player:unwatch_event(self, "on_level_up")
end

return TLevel
