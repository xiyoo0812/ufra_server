--tarea.lua
local judge_dis = qmath.judge_dis
local new_guid  = codec.guid_new
local Target    = import("world/task/target.lua")

local TArea = class(Target)
local prop = property(TArea)
prop:reader("area", nil)  --目标区域

function TArea:__init(task, conf)
    self.area = self:get_arg_t(1)
    self.radius = self:get_arg_i(2)
end

function TArea:on_position_changed(map_id, x, z)
    if self.area.m == map_id then
        if judge_dis(self.area.x, self.area.z, x, z, self.radius) then
            self:forward()
        end
    end
end

--子类实现
------------------------------------------------
function TArea:on_active()
    self.guid = new_guid()
    self.player:watch_event(self, "on_position_changed")
end

function TArea:on_deactive()
    self.player:unwatch_event(self, "on_position_changed")
end

return TArea
