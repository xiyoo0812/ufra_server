--tmake.lua
local log_debug = logger.debug
local new_guid  = codec.guid_new
local Target    = import("world/task/target.lua")

local TMake = class(Target)
local prop = property(TMake)
prop:reader("formula_id", nil)  --蓝图互转配方

function TMake:__init(task, conf)
    self.formula_id = self:get_arg_i(1, 0)
    self.count = self:get_arg_i(2, 1)
end

function TMake:on_product_make(formula_id)
    log_debug("[TMake][on_product_make] formula_id: %s", formula_id)
    if self.formula_id == 0 or self.formula_id == formula_id then
        self:forward()
    end
end

--子类实现
------------------------------------------------
function TMake:on_active()
    self.guid = new_guid()
    self.player:watch_event(self, "on_product_make")
end

function TMake:on_deactive()
    self.player:unwatch_event(self, "on_product_make")
end

function TMake:on_done()
    self.player:unwatch_event(self, "on_product_make")
end

return TMake
