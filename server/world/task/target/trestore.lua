-- trestore.lua

local log_debug = logger.debug
local Target    = import("world/task/target.lua")

local TRestore = class(Target)
local prop = property(TRestore)
prop:reader("build_id", nil)    --目标建筑

function TRestore:__init(task, conf)
    self.build_id = self:get_arg_i(1, 0)
    self.count = self:get_arg_i(2, 1)
end

function TRestore:on_utensil_repair(type, build_id)
    log_debug("[TRestore][on_utensil_repair] build_id: %s", build_id)
    if self.build_id == build_id or self.build_id == 0 then
        self:forward()
    end
end

--子类实现
------------------------------------------------
function TRestore:on_active()
    self.player:watch_event(self, "on_utensil_repair")
end

function TRestore:on_deactive()
    self.player:unwatch_event(self, "on_utensil_repair")
end

function TRestore:on_done()
    self.player:unwatch_event(self, "on_utensil_repair")
end

return TRestore
