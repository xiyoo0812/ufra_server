--ttown_level.lua

local Target = import("world/task/target.lua")

local TTownLevel = class(Target)

local prop = property(TTownLevel)
prop:reader("level", nil)

function TTownLevel:__init(task, conf)
    self.level = self:get_arg_i(1, 1)
end

function TTownLevel:on_active()
    local level = self.player:get_level()
    if level >= self.level then
        self:forward()
        return
    end
    self.player:watch_event(self, "on_town_level_up")
end

function TTownLevel:on_town_level_up(lv)
    if lv >= self.level then
        self:forward()
    end
end

function TTownLevel:on_deactive()
    self.player:unwatch_event(self, "on_town_level_up")
end

return TTownLevel
