--tletter.lua

local Target    = import("world/task/target.lua")

local TLetter = class(Target)
local prop = property(TLetter)
prop:reader("letter_id", nil)   --信件id

function TLetter:__init(task, conf)
    self.letter_id = self:get_arg_i(1)
    self.count = self:get_arg_i(2, 1)
end

function TLetter:on_letter_read(letter_id)
    if self.letter_id == letter_id then
        self:forward()
    end
end

--子类实现
------------------------------------------------
function TLetter:on_active()
    self.player:watch_event(self, "on_letter_read")
end

function TLetter:on_deactive()
    self.player:unwatch_event(self, "on_letter_read")
end

function TLetter:on_done()
    self.player:unwatch_event(self, "on_letter_read")
end


return TLetter
