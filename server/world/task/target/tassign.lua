--tarea.lua
local new_guid  = codec.guid_new
local Target    = import("world/task/target.lua")

local TAssign = class(Target)
local prop = property(TAssign)
prop:reader("npc_id", nil)      --npcid
prop:reader("building", nil)    --建筑id
prop:reader("type", 0) --0 监听新的 1查询旧的

function TAssign:__init(task, conf)
    self.npc_id = self:get_arg_i(1)
    self.building = self:get_arg_i(2)
    self.type = self:get_arg_i(3, 0)
end

function TAssign:on_send_partner(npc_id, group_id)
    if self.npc_id == 0 and self.building == group_id then
        self:forward()
        return
    end
    if self.npc_id == npc_id and self.building == group_id then
        self:forward()
    end
end

--子类实现
------------------------------------------------
function TAssign:on_active()
    self.guid = new_guid()
    self.player:watch_event(self, "on_send_partner")
end

function TAssign:on_deactive()
    self.player:unwatch_event(self, "on_send_partner")
end

return TAssign
