--tbuy.lua
local Target = import("world/task/target.lua")

local TBuy = class(Target)
local prop = property(TBuy)
prop:reader("item_id", nil)

function TBuy:__init(task, conf)
    self.item_id = self:get_arg_i(1, 0)
    self.count = self:get_arg_i(2, 1)
end

function TBuy:on_active()
    self.player:watch_event(self, "on_shop_buy")
end

function TBuy:on_deactive()
    self.player:unwatch_event(self, "on_shop_buy")
end

function TBuy:on_shop_buy(shop_id, goods, num)
    if goods.item_id == self.item_id or self.item_id == 0 then
        self:forward(num)
    end
end
return TBuy
