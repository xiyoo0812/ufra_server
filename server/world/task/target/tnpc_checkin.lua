--tnpc_checkin.lua

local Target = import("world/task/target.lua")

local TNpcCheckin = class(Target)
local prop = property(TNpcCheckin)
prop:reader("npc_id", 0)

function TNpcCheckin:__init(task, conf)
    self.npc_id = self:get_arg_i(1, 0)
end

function TNpcCheckin:on_npc_checkin(npc_id)
    if self.npc_id == 0 or self.npc_id == npc_id then
        self:forward()
    end
end

function TNpcCheckin:on_active()
    self.player:watch_event(self, "on_npc_checkin")
end

function TNpcCheckin:on_deactive()
    self.player:unwatch_event(self, "on_npc_checkin")
end

return TNpcCheckin