--ttasks.lua

local Target = import("world/task/target.lua")

local TTasks = class(Target)
local prop = property(TTasks)
prop:reader("tasks", {})     --任务类型数组

function TTasks:__init(task, conf)
    local types = self:get_arg_t(1)
    for _, type in ipairs(types) do
        self.tasks[type] = true
    end
end

function TTasks:on_task_finished(id, type)
    if self.types[type] == nil then
        return
    end
    self:forward()
end

--子类实现
------------------------------------------------

function TTasks:on_active()
    self.player:watch_event(self, "on_task_finished")
end

function TTasks:on_deactive()
    self.player:unwatch_event(self, "on_task_finished")
end

function TTasks:on_done()
    self.player:unwatch_event(self, "on_task_finished")
end

return TTasks
