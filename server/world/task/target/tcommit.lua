--tcommit.lua

local Target    = import("world/task/target.lua")

local TCommit = class(Target)
local prop = property(TCommit)
prop:reader("item_id", nil)  --item_id

function TCommit:__init(task, conf)
    self.item_id = self:get_arg_i(1)
    self.count = self:get_arg_i(2, 1)
end

function TCommit:on_item_commit(item_id, num)
    if self.item_id == item_id then
        self:forward(num)
    end
end

--子类实现
------------------------------------------------
function TCommit:on_active()
    self.player:watch_event(self, "on_item_commit")
end

function TCommit:on_deactive()
    self.player:unwatch_event(self, "on_item_commit")
end

return TCommit
