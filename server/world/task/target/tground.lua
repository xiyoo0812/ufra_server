--tground.lua

local Target    = import("world/task/target.lua")

local TGround = class(Target)
local prop = property(TGround)
prop:reader("type", nil)        --类型

function TGround:__init(task, conf)
    self.type = self:get_arg_i(1)
    self.count = self:get_arg_i(2, 1)
    self.history = self:get_arg_i(3, 0)
end

function TGround:on_ground_unlock(id, type)
    if type == self.type then
        self:forward()
    end
end

--子类实现
------------------------------------------------
function TGround:on_active()
    if self.history == 1 then
        -- 第一个参数是表示远程调用是否成功
        -- TODO 判断是否解锁成功
        return
    end
    if self:is_complete() then
        return
    end
    self.player:watch_event(self, "on_ground_unlock")
end

function TGround:on_deactive()
    self.player:unwatch_event(self, "on_ground_unlock")
end

function TGround:on_done()
    self.player:unwatch_event(self, "on_ground_unlock")
end

return TGround
