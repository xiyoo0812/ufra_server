--ttask.lua

local Target = import("world/task/target.lua")

local TTask = class(Target)
local prop = property(TTask)
prop:reader("type", nil)        --任务类型
prop:reader("task_id", nil)     --任务ID

function TTask:__init(task, conf)
    self.type = self:get_arg_i(1)
    self.count = self:get_arg_i(2, 1)
    self.task_id = self:get_arg_i(3)
end

function TTask:on_task_finished(id, type)
    if self.task_id and self.task_id == id then
        self:forward()
        return
    end
    if self.type == 0 or type == self.type then
        self:forward()
    end
end

--子类实现
------------------------------------------------

function TTask:on_active()
    self.player:watch_event(self, "on_task_finished")
end

function TTask:on_deactive()
    self.player:unwatch_event(self, "on_task_finished")
end

function TTask:on_done()
    self.player:unwatch_event(self, "on_task_finished")
end

return TTask
