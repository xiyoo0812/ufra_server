--tbuilding.lua
local log_debug = logger.debug
local new_guid  = codec.guid_new
local Target    = import("world/task/target.lua")

local TFindBuilding = class(Target)
local prop = property(TFindBuilding)
prop:reader("build_id", nil)    --目标建筑

function TFindBuilding:__init(task, conf)
    self.build_id = self:get_arg_i(1, 0)
    self.count = self:get_arg_i(2, 1)
end

function TFindBuilding:on_building_count(build_id, count)

    log_debug("[TBuilding][on_building_count] build_id:{} count:{}", build_id, count)

    if self.build_id == build_id or self.build_id == 0 then
        self:update_progress(count)
    end
end

--子类实现
------------------------------------------------
function TFindBuilding:on_active()
    self.guid = new_guid()
    self.player:watch_event(self, "on_building_count")
end

function TFindBuilding:on_deactive()
    self.player:unwatch_event(self, "on_building_count")
end

function TFindBuilding:on_done()
    self.player:unwatch_event(self, "on_building_count")
end

return TFindBuilding
