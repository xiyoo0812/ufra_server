--alloter.lua
local mhuge         = math.huge
local log_debug     = logger.debug

local EFFECT_NPC    = quanta.enum("EffectType", "NPC")

local event_mgr     = quanta.get("event_mgr")
local config_mgr    = quanta.get("config_mgr")
local effect_mgr    = quanta.get("effect_mgr")

local item_db       = config_mgr:get_table("item")
local utility_db    = config_mgr:get_table("utility")

local EXP_ID        = utility_db:find_number("value", "exp_id")
local COIN_ID       = utility_db:find_number("value", "coin_id")
local ENERGY_ID     = utility_db:find_number("value", "energy_id")
local DIAMOND_ID    = utility_db:find_number("value", "diamond_id")
local TOWN_EXP_ID   = utility_db:find_number("value", "town_exp_id")
local EFF_NPC_ID    = utility_db:find_number("value", "npc_start_id")
local AFFINITY_ID   = utility_db:find_number("value", "affinity_id")
local ACTY_ID       = utility_db:find_number("value", "acty_id")
local SAT_ID        = utility_db:find_number("value", "sat_id")
local ItemType      = enum("ItemType")

local attr_indexs = {
    [EXP_ID]        = quanta.enum("AttrID", "ATTR_EXP"),
    [COIN_ID]       = quanta.enum("AttrID", "ATTR_COIN"),
    [ENERGY_ID]     = quanta.enum("AttrID", "ATTR_ENERGY"),
    [DIAMOND_ID]    = quanta.enum("AttrID", "ATTR_DIAMOND"),
    [ACTY_ID]       = quanta.enum("AttrID", "ATTR_ACTY")
}

local effect_indexs = {
    [TOWN_EXP_ID]   = quanta.enum("EffectType", "TOWN_EXP"),
    [AFFINITY_ID]   = quanta.enum("EffectType", "AFFINITY"),
    [SAT_ID]        = quanta.enum("EffectType", "SAT"),
}

local attr_per_indexs = {
    [EXP_ID]        = quanta.enum("AttrID", "ATTR_EXP_PER"),
    [COIN_ID]       = quanta.enum("AttrID", "ATTR_COIN_PER"),
}

-- 一对多
local attr_otm_indexs = {
    [ACTY_ID] = {
        quanta.enum("AttrID", "ATTR_DAY_ACTY"),
        quanta.enum("AttrID", "ATTR_WEEK_ACTY")
    }
}

local Alloter = class()
local prop = property(Alloter)
prop:reader("bag", nil)
prop:reader("role", nil)
prop:accessor("target", nil)
prop:reader("items", {})
prop:reader("attr_items", {})
prop:reader("effect_items", {})

function Alloter:__init(role, bag)
    self.bag = bag
    self.role = role
end

--添加效果
function Alloter:add_effect_item(item_id, num)
    local oldn = self.effect_items[item_id] or 0
    self.effect_items[item_id] = oldn + num
end

-- 添加分配货币
function Alloter:add_attr_item(item_id, num)
    local oldn = self.attr_items[item_id] or 0
    self.attr_items[item_id] = oldn + num
end

-- 添加分配道具
function Alloter:add_allot(item_id, num)
    local oldn = 0
    local allot = self.items[item_id]
    if allot then
        oldn = allot
    end
    self.items[item_id] = oldn + num
end

-- 分配道具
function Alloter:allot(reason)
    local allot_items = {}
    local obtain_items = {}
    local obtain_attr_items = {}
    local player_id = self.role.id
    --属性
    for item_id, num in pairs(self.attr_items) do
        obtain_attr_items[item_id] = num
        local attr_id = attr_indexs[item_id]
        local attr_per_id = attr_per_indexs[item_id]
        if attr_per_id then
            local attr_per = self.role:get_attr(attr_per_id)
            if attr_per > 0 then
                num = num + num * attr_per / 100
            end
        end

        -- 添加属性
        self.role:add_attr(attr_id, num)
        event_mgr:notify_trigger("on_attritem_add", self.role, item_id, attr_id, num, reason)

        -- 一对多属性
        local attr_ids = attr_otm_indexs[item_id] or {}
        for _,id in pairs(attr_ids) do
            self.role:add_attr(id, num)
            event_mgr:notify_trigger("on_attritem_add", self.role, item_id, id, num, reason)
        end
        log_debug("[Alloter][allot] player {} attot attr item {} num: {}", player_id, item_id, num)
    end
    --道具
    for item_id, num in pairs(self.items or {}) do
        self.bag:allot_item(item_id, num, reason)
        allot_items[item_id] = self.bag:get_item_num(item_id)
        obtain_items[item_id] = num
        log_debug("[Alloter][allot] player {} attot item {} num: {}", player_id, item_id, num)
    end
    --效果
    for item_id, num in pairs(self.effect_items) do
        local config = item_db:find_one(item_id)
        if not config then
            goto continue
        end
        obtain_attr_items[item_id] = num
        if config.type == ItemType.EFFECT then
            local effect_id = config.effect_id
            effect_mgr:execute(self.role, effect_id, num, self.target)
        else
            local effect_type = effect_indexs[item_id]
            effect_mgr:execute_doer(self.role, effect_type, self.target, item_id, num, reason)
        end
        ::continue::
    end
    local obtains = { items = obtain_items, moneys = obtain_attr_items, reason = reason }
    self:sync(allot_items, obtains)
end

-- 获取物品空间
function Alloter:get_item_space(item_id)
    -- 属性验证
    local attr_id = attr_indexs[item_id]
    if attr_id then
        return mhuge
    end

    local config = item_db:find_one(item_id)
    if not config then
        return 0
    end
    local overlie = config.overlie
    local item_count = self.bag:get_item_num(item_id)
    local last_space = 0
    if item_count > 0 then
        last_space = overlie - item_count
        return last_space < 0 and 0 or last_space
    else
        if self.bag:get_space_size() > 1 then
            return overlie
        end
    end
    return last_space
end

--检查是否足够
function Alloter:check_items(items)
    for item_id, num in pairs(items) do
        if not self:check_space(item_id, num) then
            return false
        end
    end
    return true
end

-- 检查item是否足够
function Alloter:check_space(item_id, num)
    --属性
    if attr_indexs[item_id] then
        self:add_attr_item(item_id, num)
        return true
    end
    --效果
    if effect_indexs[item_id] then
        self:add_effect_item(item_id, num)
        return true
    end
    --虚拟npc
    if item_id < EFF_NPC_ID then
        log_debug("[Alloter][check_space] alloter npc:{}", item_id)
        effect_indexs[item_id] = EFFECT_NPC
        self:add_effect_item(item_id, num)
        return true
    end
    local config = item_db:find_one(item_id)
    if not config then
        return false
    end
    --虚拟物品
    if config.type == ItemType.EFFECT then
        log_debug("[Alloter][check_space] alloter effect item:{}", item_id)
        self:add_effect_item(item_id, num)
        return true
    end
    --道具
    return self:check_bag_space(config, item_id, num)
end

--检查空间是否足够
function Alloter:check_bag_space(config, item_id, num)
    local overlie = config.overlie
    local item_count = self.bag:get_item_num(item_id)
    --单次添加是否超过上限
    if item_count + num > overlie then
        return false
    end

    local result
    if item_count > 0 then
        --已有道具
        result = item_count + num <= overlie
    else
        --空位
        result= self.bag:get_space_size() > 1
    end
    if result then
        self:add_allot(item_id, num)
    end
    return result
end

--同步消耗的物品
function Alloter:sync(allot_items, obtains)
    --同步客户端
    self.role:sync_items(self.bag.id, allot_items)
    --同步客户端
    self.role:sync_obtains(obtains)
end

return Alloter
