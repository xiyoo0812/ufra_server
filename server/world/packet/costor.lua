--costor.lua
local log_debug     = logger.debug

local event_mgr     = quanta.get("event_mgr")
local config_mgr    = quanta.get("config_mgr")

local utility_db    = config_mgr:get_table("utility")

local COIN_ID       = utility_db:find_number("value", "coin_id")
local ENERGY_ID     = utility_db:find_number("value", "energy_id")
local DIAMOND_ID    = utility_db:find_number("value", "diamond_id")
local ACTY_ID       = utility_db:find_number("value", "acty_id")

local attr_indexs = {
    [COIN_ID]       = quanta.enum("AttrID", "ATTR_COIN"),
    [ENERGY_ID]     = quanta.enum("AttrID", "ATTR_ENERGY"),
    [DIAMOND_ID]    = quanta.enum("AttrID", "ATTR_DIAMOND"),
    [ACTY_ID]       = quanta.enum("AttrID", "ATTR_ACTY")
}

-- 一对多
local attr_otm_indexs = {
    [ACTY_ID] = {
        quanta.enum("AttrID", "ATTR_DAY_ACTY"),
        quanta.enum("AttrID", "ATTR_WEEK_ACTY")
    }
}

local Costor = class()
local prop = property(Costor)
prop:reader("bag", nil)
prop:reader("role", nil)
prop:reader("items", {})
prop:reader("attr_items", {})

function Costor:__init(role, bag)
    self.bag = bag
    self.role = role
end

-- 添加消耗道具
function Costor:add_item(item_id, num)
    self.items[item_id] = num + (self.items[item_id] or 0)
end

-- 添加消耗货币
function Costor:add_attr_item(item_id, num)
    local oldn = self.attr_items[item_id] or 0
    self.attr_items[item_id] = oldn + num
end

-- 消耗道具
function Costor:cost(reason)
    for item_id, num in pairs(self.attr_items) do
        -- 扣除属性
        local attr_id = attr_indexs[item_id]
        self.role:cost_attr(attr_id, num)
        self.role:notify_event("on_attritem_cost", item_id, attr_id, num, reason)
        event_mgr:notify_trigger("on_attritem_cost", self.role, item_id, attr_id, num, reason)

        -- 一对多属性
        local attr_ids = attr_otm_indexs[item_id] or {}
        for _,id in pairs(attr_ids) do
            self.role:cost_attr(id, num)
            self.role:notify_event("on_attritem_cost", id, item_id, id, num, reason)
            event_mgr:notify_trigger("on_attritem_cost", self.role, item_id, id, num, reason)
       end

        log_debug("[Costor][cost] player {} cost attr item {} num: {}", self.role.id, item_id, num)
    end
    for item_id, num in pairs(self.items) do
        self.bag:cost_item(item_id, num, reason)
        log_debug("[Costor][cost] player {} cost item {} num: {}", self.role.id, item_id, num)
    end
    if next(self.items) then
        self:sync(self.items)
    end
end

-- 检查是否足够
function Costor:check_items(items)
    for item_id, num in pairs(items) do
        if not self:check_item(item_id, num) then
            return false
        end
    end
    return true
end

-- 检查item是否足够
function Costor:check_item(item_id, num)
    if num == 0 then
        return true
    end
    local attr_id = attr_indexs[item_id]
    if not attr_id then
        return self:check_bag_item(item_id, num)
    end
    if not self.role:check_attr(attr_id, num) then
        return false
    end
    self:add_attr_item(item_id, num)
    return true
end

--检查物品是否足够
function Costor:check_bag_item(item_id, num)
    local item_count = self.bag:get_item_num(item_id) or 0
    local enough = (item_count >= num)
    if enough then
        self:add_item(item_id, num)
    end
    return enough
end

--同步消耗的物品
function Costor:sync(cost_items)
    local items = {}
    for item_id, _ in pairs(cost_items) do
        local item_count = self.bag:get_item_num(item_id)
        items[item_id] = item_count
    end
    --同步客户端
    self.role:sync_items(self.bag.id, items)
end

return Costor
