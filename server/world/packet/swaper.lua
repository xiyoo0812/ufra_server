--swaper.lua
local Item          = import("world/packet/item.lua")
local protobuf_mgr  = quanta.get("protobuf_mgr")

local PACKET_SHUT   = protobuf_mgr:enum("packet_type", "PACKET_SHUT")
local PACKET_BANK   = protobuf_mgr:enum("packet_type", "PACKET_BANK")

local Swaper = class()
local prop = property(Swaper)
prop:reader("role", nil)

function Swaper:__init(role)
    self.role = role
end

--装备上装
function Swaper:install(equip, equip_bag, item_bag)
    --目标装备位是否已有装备
    local pos = equip:get_pos()
    local oEquip
    for item_id, _ in pairs(equip_bag:get_items() or {}) do
        local old_equip = Item(item_id, 1)
        if old_equip:get_pos() == pos then
            oEquip = old_equip
            break;
        end
    end
    if oEquip then
        self:swap_item(oEquip, item_bag, equip_bag, 1)
    end

    self:swap_item(equip, equip_bag, item_bag, 1)
    equip_bag:update_avatar(equip:get_item_id())
    return true
end

--装备卸装
function Swaper:uninstall(equip, item_bag, equip_bag)
    self:swap_item(equip, item_bag, equip_bag, 1)
    equip_bag:update_avatar(equip:get_item_id(), true)
    return true
end

--检查能否交换
function Swaper:check_swap(src_item, tar_bag)
    if tar_bag.id == PACKET_SHUT and not src_item:is_shutable() then
        return false
    end
    if tar_bag.id == PACKET_BANK and not src_item:is_bankable() then
        return false
    end
    return true
end


--覆盖道具
function Swaper:cover_item(src_bag, src_item, tar_bag, tar_item, count)
    --目标道具堆叠上限
    local item_id = src_item:get_item_id()
    local overlie = src_item:overlie()
    local tar_count = tar_item:get_count()
    local move_count = count
    if overlie < count + tar_count then
        --超过堆叠上限
        move_count = overlie - tar_count
    end
    src_bag:swap_item(item_id, -move_count)
    tar_bag:swap_item(item_id, move_count)

    self:sync(src_bag.id, {[item_id] = src_bag:get_item_num(item_id)})
    self:sync(tar_bag.id, {[item_id] = tar_bag:get_item_num(item_id)
})
end

--交换道具位置
function Swaper:swap_item(src_item, tar_bag, src_bag, count)
    if not count or count == 0 then
        count = src_item.count
    end
    local item_id = src_item:get_item_id()
    --目标背包是否已有该道具
    local tar_count = tar_bag:get_item_num(item_id)
    if tar_count > 0 then
        local tar_item = Item(item_id, tar_count)
        if tar_item:is_combine(src_item) then
            self:cover_item(src_bag, src_item, tar_bag, tar_item, count)
            return
        end
    else
        --全部转移
        src_bag:swap_item(item_id, -count)
        tar_bag:swap_item(item_id, count)

        local src_count = src_bag:get_item_num(item_id)
        tar_count = tar_bag:get_item_num(item_id)

        self:sync(src_bag.id, {[item_id] = src_count})
        self:sync(tar_bag.id, {[item_id] = tar_count})
    end
end

--同步交换的物品
function Swaper:sync(bag_id, item)
    if item then
        --同步客户端
        self.role:sync_items(bag_id, item)
        return
    end
    --同步客户端
    self.role:sync_items(bag_id, item)
end

--自动填充
function Swaper:fill_packet(src_packet, tar_packet)
    for item_id, has_count in pairs(tar_packet:get_items()) do
        --源背包是否已有该道具
        local src_count = src_packet:get_item_num(item_id)
        if src_count <= 0 then
            goto continue
        end
        local src_item = Item(item_id, src_count)
        --是否可交换
        local can_swap = self:check_swap(src_item, tar_packet)
        if not can_swap then
            goto continue
        end
        --是否可堆叠
        local tar_item = Item(item_id, has_count)
        if not tar_item:is_overlie() then
            goto continue
        end
        self:cover_item(src_packet, src_item, tar_packet, tar_item, src_count)
        ::continue::
    end
end


return Swaper
