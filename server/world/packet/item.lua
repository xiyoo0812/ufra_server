--item.lua
--仅提供item配置类工具
local log_err       = logger.err

local config_mgr    = quanta.get("config_mgr")
local item_db       = config_mgr:get_table("item")
local ItemType      = enum("ItemType")

local ATTACK_ID         = quanta.enum("AttrID", "ATTR_ATTACK")
local ATTACK_MAX_ID     = quanta.enum("AttrID", "ATTR_ATTACK_MAX")

--属性映射表
local attr_indexs = {
    ["defense"]         = quanta.enum("AttrID", "ATTR_DEFENCE"),
    ["cr_rate"]         = quanta.enum("AttrID", "ATTR_CRITICAL_RATE"),
    ["cr_damage"]       = quanta.enum("AttrID", "ATTR_CRITICAL_HURT"),
}


local Item = class()
local prop = property(Item)
prop:reader("item_id", nil)
prop:reader("prototype", nil)
prop:accessor("count", 0)

function Item:__init(item_id, count)
    self.item_id = item_id
    self.count = count
    self.prototype = item_db:find_one(item_id)
    if not self.prototype then
        log_err("[Item][__init] item_id {} not found conf", item_id)
        return
    end
end

--是否装备
function Item:is_equip()
    local item_type = self.prototype.type
    return item_type ==  ItemType.EQUIP or item_type ==  ItemType.SUIT
end

-- 获取红点id
function Item:get_rid()
    return self.prototype.first_reddots or nil
end

--获取主位置
function Item:get_pos()
    local equip_pos = self.prototype.equip_pos
    if equip_pos then
        return equip_pos[1]
    end
end

function Item:is_dropable()
    return self.prototype.is_drop
end

function Item:is_shutable()
    return self.prototype.is_shortcut
end

function Item:is_bankable()
    return self.prototype.is_bank
end

function Item:get_cdtime()
    return self.prototype.cd_group, self.prototype.cd_time
end

function Item:get_effect_id()
    return self.prototype.effect_id
end

function Item:get_usecount(count)
    if self.prototype.batch then
        if count > self.count then
            return self.count
        end
        return count
    end
    if self.count > 0 then
        return 1
    end
    return 0
end

--是否可以组合
function Item:is_combine(item)
    if self.item_id == item.item_id then
        return self:is_proto() and item:is_proto()
    end
    return false
end

function Item:alike(item_id)
    return self.item_id == item_id
end

--是否可堆叠
function Item:is_overlie()
    return self:overlie() > self.count
end

--堆叠上限
function Item:overlie()
    return self.prototype.overlie
end

--是否原型
function Item:is_proto()
    return true
end

--获取品质
function Item:get_quality()
    return self.prototype.quality or 0
end

--穿戴装备效果
function Item:install_effect(entity)
    --是否有攻击属性
    local attack_value = self.prototype["attack"]
    if attack_value then
        entity:add_attr(ATTACK_ID, attack_value)
        entity:add_attr(ATTACK_MAX_ID, attack_value)
    end
    --其他属性
    for attr, attr_id in pairs(attr_indexs) do
        local attr_value = self.prototype[attr]
        if attr_value then
            entity:add_attr(attr_id, attr_value)
        end
    end
end

--卸载装备效果
function Item:uninstall_effect(entity)
    --是否有攻击属性
    local attack_value = self.prototype["attack"]
    if attack_value then
        entity:cost_attr(ATTACK_ID, attack_value)
        entity:cost_attr(ATTACK_MAX_ID, attack_value)
    end
    --其他属性
    for attr, attr_id in pairs(attr_indexs) do
        local attr_value = self.prototype[attr]
        if attr_value then
            entity:cost_attr(attr_id, attr_value)
        end
    end
end


return Item
