--packet.lua
local Item          = import("world/packet/item.lua")

local log_err       = logger.err
local log_debug     = logger.debug
local tsize         = qtable.size

local event_mgr     = quanta.get("event_mgr")
local config_mgr    = quanta.get("config_mgr")

local item_db       = config_mgr:get_table("item")

local ATTR_POS      = quanta.enum("AttrID", "ATTR_HEAD") - 1

local Packet = class()
local prop = property(Packet)
prop:reader("id", nil)
prop:reader("role", nil)
prop:accessor("avatar", false)
prop:accessor("equip_bag", false)

local dbprop = db_property(Packet, "player_item")
dbprop:store_values("items", {})        --key-道具原型ID value-数量
dbprop:store_value("capacity", 0)

function Packet:__init(role, id, capacity)
    self.id = id
    self.role = role
    self.capacity = capacity
end

function Packet:empty()
    if next(self.items) then
        return false
    end
    return true
end

--清空包裹
function Packet:clear()
    self.items = {}
end

--检查物品数量
function Packet:check_item_num(item_id, num)
    local count =  self.items[item_id] or 0
    if count >= num then
        return num
    end
    return count
end

--获取物品数量
function Packet:get_item_num(item_id)
    return self.items[item_id] or 0
end

--加载packet
function Packet:load_items(items, binit)
    local item_size = tsize(items)
    log_debug("[Packet][load_items] item_size:{} items: {}!", item_size, items)
    if binit then
        for item_id, count in pairs(items or {}) do
            self:save_items_field(item_id, count)
        end
    else
        self:set_items(items)
    end
end

--分配道具，用于奖励等新增物品
function Packet:allot_item(item_id, num, reason)
    self:add_item(item_id, num)
    event_mgr:fire_frame("on_item_add", self.role, item_id, num, reason, self.id)
    return true
end

--添加道具
function Packet:add_item(item_id, count)
    local result = self:get_item_num(item_id) + count
    self:save_items_field(item_id, result)
end

function Packet:delete_item(item_id)
    self:del_items_field(item_id)
end

--内部交换 count> 0 加 反之减
function Packet:swap_item(item_id, count)
    local result = self:get_item_num(item_id) + count
    if result <= 0 then
        self:delete_item(item_id)
    else
        self:save_items_field(item_id, result)
    end
end

--消耗/丢弃/出售道具，会销毁道具
function Packet:cost_item(item_id, num, reason)
    local count = self.items[item_id] or 0
    if count < num then
        log_err("[Packet][cost_item] failed! player_id({}) item_id({}) count({}) < num({})", self.role:get_id(), item_id, count, num)
        return
    end
    local result = count - num
    if result <= 0 then
        self:del_items_field(item_id)
    else
        self:save_items_field(item_id, result)
    end
    event_mgr:fire_frame("on_item_cost", self.role, item_id, num, reason, result, self.id)
end

--剩余空间大小
function Packet:get_space_size()
    return self.capacity - tsize(self.items or {})
end

--外观展示
function Packet:update_avatars(enable)
    self.avatar = enable
    if enable then
        local player = self.role
        --装备栏可遍历
        for item_id, _ in pairs(self.items or {}) do
            --获取配置上的装备位
            local item_conf = item_db:find_one(item_id)
            local grid = item_conf.equip_pos[1]
            if grid  then
                local attr_id = ATTR_POS + grid
                player:set_attr(attr_id, item_id)
                log_debug("[Packet][update_avatars] player_id:{} attr_id:{} item_id:{}", player:get_id(), attr_id, item_id)
            end
        end
    end
end

--更新外观
function Packet:update_avatar(item_id, uninstall)
    if self.avatar then
         --获取配置上的装备位
         local item_conf = item_db:find_one(item_id)
         local grid = item_conf.equip_pos[1]
         if grid  then
             local attr_id = ATTR_POS + grid
             if uninstall then
                item_id = 0
             end
             self.role:set_attr(attr_id, item_id)
             log_debug("[Packet][update_avatars] player_id:{} attr_id:{} item_id:{}", self.role:get_id(), attr_id, item_id)
         end
    end
end

--序列化
function Packet:pack2db()
    return { items = self.items, capacity = self.capacity }
end

--序列化
function Packet:pack2client()
    return {
        items = self.items,
        full_sync = true,
        packet_id = self.id,
        capacity = self.capacity
    }
end

--更新装备属性
function Packet:update_equip_attrs()
    local player = self.role
    --装备栏可遍历
    for item_id, _ in pairs(self.items or {}) do
        local equip = Item(item_id, 1)
        equip:install_effect(player)
    end
end

--获取当前背包已使用大小
function Packet:get_size()
    return tsize(self.items or {})
end

return Packet
