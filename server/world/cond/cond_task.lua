-- open_task.lua
local Target = import("world/cond/cond_target.lua")

local CondTask = class(Target)

function CondTask:on_init(task_id)
    return { task_id }
end

function CondTask:on_add_new_condition(task_id)
    if self.player:has_finish_task(task_id) then
        self:forward(task_id)
    end
end

function CondTask:on_start()
    self.player:watch_event(self, "on_task_finished")
end

function CondTask:on_task_finished(task_id)
    self:forward(task_id)
end

function CondTask:on_stop()
    self.player:unwatch_event(self, "on_task_finished")
end

function CondTask:check_cond(task_id)
    return self.player:has_finish_task(task_id)
end

return CondTask