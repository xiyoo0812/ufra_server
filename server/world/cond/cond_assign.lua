
local table_unpack = table.unpack
local Target = import("world/cond/cond_target.lua")

local CondAssign = class(Target)

function CondAssign:on_init(conditions)
    local npc_id, group_id = table_unpack(conditions)
    local key = (npc_id << 32) | group_id
    return { key }
end

function CondAssign:on_start()
    self.player:watch_event(self, "on_send_partner")
end

function CondAssign:on_add_new_condition(key)
    local npc_id = (key >> 32)
    local group_id = (key & 0xFFFFFFFF)
    if self.player:is_send_partner(npc_id, group_id) then
        self:forward(key)
    end
end

function CondAssign:on_send_partner(npc_id, group_id)
    local key = (npc_id << 32) | group_id
    self:forward(key)
end

function CondAssign:check_cond(value)
    local npc_id, group_id = table_unpack(value)
    return self.player:is_send_partner(npc_id, group_id)
end

function CondAssign:on_stop()
    self.player:unwatch_event(self, "on_send_partner")
end

function CondAssign:check_one_cond(value)
end

return CondAssign