-- open_level.lua
local Target = import("world/cond/cond_target.lua")

local CondTownLevel = class(Target)

function CondTownLevel:on_init(level)
    return { level }
end

function CondTownLevel:is_number()
    return true
end

function CondTownLevel:on_add_new_condition(level)
    local town_lv = self.player:get_town_level()
    if town_lv >= level then
        self:forward(town_lv)
    end
end

function CondTownLevel:on_start()
    self:forward(self.player:get_town_level())
    self.player:watch_event(self, "on_town_level_up")
end

function CondTownLevel:on_town_level_up(level)
    self:forward(level)
end

function CondTownLevel:on_stop()
    self.player:unwatch_event(self, "on_town_level_up")
end

function CondTownLevel:check_cond(level)
    return self.player:get_town_level() >= level
end

function CondTownLevel:check_one_cond(value)
    local res = self:check_cond(value)
    return { [tostring(value)] = res }
end

return CondTownLevel

