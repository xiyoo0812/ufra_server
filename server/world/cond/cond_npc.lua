-- open_npc.lua
local Target = import("world/cond/cond_target.lua")

local CondNpc = class(Target)
local prop = property(Target)
prop:reader("sync_guid", nil)

function CondNpc:on_init(conditions)
    if type(conditions) == "number" then
        return { conditions }
    end
    return conditions
end

function CondNpc:on_start()
    self.player:watch_event(self, "on_recv_partner")
end

function CondNpc:on_add_new_condition(npc_id)

end

function CondNpc:on_recv_partner(npc_id)

    self:forward(npc_id)
end

function CondNpc:on_stop()
    self.player:unwatch_event(self, "on_recv_partner")
end

function CondNpc:check_cond(value)
    for _, npc_id in ipairs(value) do
        if self.player:has_partner(npc_id) == nil then
            return false
        end
    end
    return true
end

function CondNpc:check_one_cond(value)
    local res = { }
    for _, npc_id in ipairs(value) do

    end
    return res
end

function CondNpc:check_cond(value)
    for _, npc_id in ipairs(value) do
        if self.player:has_partner(npc_id) == nil then
            return false
        end
    end
    return true
end

return CondNpc