--cond_linking.lua

local Target = import("world/cond/cond_target.lua")

local tinsert = table.insert
local CondLinking = class(Target)

function CondLinking:on_init(conditions)
    local conds = { }
    for npc_id, lv in pairs(conditions) do
        local key = (npc_id << 32) | lv
        tinsert(conds, key)
    end
    return conds
end

function CondLinking:on_start()
    self.player:watch_event(self, "on_affinity_change")
end

function CondLinking:on_add_new_condition(key)

    local npc_id = (key >> 32)
    local lv = (key & 0xFFFFFFFF)
    local level = self.player:get_affinity_lv(npc_id)
    if level >= lv then
        self:forward(key)
    end
end

function CondLinking:on_affinity_change(npc_id, lv, value)
    local key = (npc_id << 32) | lv
    self:forward(key)
end

function CondLinking:on_stop()
    self.player:unwatch_event(self, "on_affinity_change")
end

function CondLinking:check_cond(value)
    for npc_id, lv in pairs(value) do
        local level = self.player:get_affinity_lv(npc_id)
        if level < lv then
            return false
        end
    end
    return true
end

function CondLinking:check_one_cond(value)
    local res = { }
    for npc_id, lv in pairs(value) do

    end
    return res
end

function CondLinking:check_cond(value)
    for npc_id, lv in pairs(value) do
        local level = self.player:get_affinity_lv(npc_id)
        if level < lv then
            return false
        end
    end
    return true
end

return CondLinking