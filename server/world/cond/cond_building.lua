-- open_building.lua

local Target = import("world/cond/cond_target.lua")

local CondBuilding = class(Target)

function CondBuilding:on_init(conditions)
    if type(conditions) == 'number' then
        return { conditions }
    end
   return conditions
end

function CondBuilding:on_start()
    self.player:watch_event(self, "on_building_finish")
end

function CondBuilding:on_add_new_condition(build_id)
    local scene = self.player:get_scene()
    if scene:has_building(build_id) then
        self:forward(build_id)
    end
end

function CondBuilding:on_building_finish(build_id)
    self:forward(build_id)
end

function CondBuilding:check_cond(value)
    local scene = self.player:get_scene()
    for _, build_id in ipairs(value) do
        if scene:has_building(build_id) == nil then
            return false
        end
    end
    return true
end

function CondBuilding:on_stop()
    self.player:unwatch_event(self, "on_building_finish")
end

return CondBuilding