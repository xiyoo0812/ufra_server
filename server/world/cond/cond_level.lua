-- open_level.lua
local Target = import("world/cond/cond_target.lua")

local CondLevel = class(Target)

function CondLevel:on_init(level)
    return { level }
end

function CondLevel:is_number()
    return true
end

function CondLevel:on_add_new_condition(level)
    local player_lv = self.player:get_level()
    if player_lv >= level then
        self:forward(player_lv)
    end
end

function CondLevel:on_start()
    self:forward(self.player:get_level())
    self.player:watch_event(self, "on_level_up")
end

function CondLevel:on_level_up(level, up)
    self:forward(level)
end

function CondLevel:on_stop()
    self.player:unwatch_event(self, "on_level_up")
end

function CondLevel:check_cond(level)
    return self.player:get_level() >= level
end

return CondLevel

