-- open_target.lua
local xpcall        = xpcall
local dtraceback    = debug.traceback
local table_unpack  = table.unpack
local table_insert  = table.insert

local CondTarget = class()
local prop = property(CondTarget)
prop:reader("player", nil)
prop:reader("condition_id", nil)
prop:reader("watch_condition", { })
prop:reader("is_start", false)
prop:reader("change_values", { }) --变化的值

function CondTarget:__init(player, condition_id)
    self.player = player
    self.condition_id = condition_id
    self.watch_condition = { }
end

function CondTarget:start()
    self:on_start()
    self.is_start = true
end

function CondTarget:is_number()
    return false
end

function CondTarget:add_condition(conditions, obj, func, ...)

    local condition = self:on_init(conditions)

    for _, key in ipairs(condition) do
            if self.watch_condition[key] == nil then
                self.watch_condition[key] = { }
            end
            table_insert(self.watch_condition[key], {
                obj, func, { ... }
            })
            self:on_add_new_condition(key)
    end
end

function CondTarget:on_add_new_condition(key)

end

function CondTarget:on_start()
    --订阅事件
end

function CondTarget:on_stop()
    --移除监听事件
end

function CondTarget:forward(value)

    local infos = self.watch_condition[value]
    if infos == nil then
        return
    end

    self.watch_condition[value] = nil -- // 移除关注
    for _, info in ipairs(infos) do
        local trigger, func_name, args = table_unpack(info)
        xpcall(trigger[func_name], dtraceback, trigger, self.player, self.condition_id, value, table_unpack(args))
    end
end

function CondTarget:is_complete()
    local count = 0
    for _, _ in pairs(self.watch_condition) do
        count = count + 1
    end
    return count == 0
end

function CondTarget:check_cond(value)

end

function CondTarget:check_one_cond(value)

end


return CondTarget