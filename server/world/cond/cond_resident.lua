--cond_resident.lua

local Target    = import("world/cond/cond_target.lua")

local CondResidentCount = class(Target)

function CondResidentCount:on_init(count)
    return { count }
end

function CondResidentCount:on_start()
    self.player:watch_event(self, "on_npc_checkin")
end

function CondResidentCount:on_add_new_condition(count)
    local number = self.player:get_checkin_count()
    if number >= count then
        self:on_npc_checkin(0, 0)
    end
end

function CondResidentCount:on_npc_checkin(npc_id, build_id)
    local count = self.player:get_checkin_count()
    for i = 1, count do
        self:forward(i)
    end
end

function CondResidentCount:on_stop()
    self.player:unwatch_event(self, "on_npc_checkin")
end

function CondResidentCount:check_cond(count)
    return self.player:get_checkin_count() >= count
end

return CondResidentCount