--cond_group_count.lua

local tab_insert    = table.insert
local sformat       = string.format

local Target        = import("world/cond/cond_target.lua")

local CondBuildingCount = class(Target)

function CondBuildingCount:on_init(conditions)
    local items = { }
    for group_id, count in pairs(conditions) do
        local uuid = group_id << 32 | count
        tab_insert(items, uuid)
    end
    return items
end

function CondBuildingCount:on_start()
    self.player:watch_event(self, "on_group_count")
end

function CondBuildingCount:on_add_new_condition(key)
    local group_id = (key >> 32)
    local scene = self.player:get_scene()
    local count = scene:get_group_num(group_id)
    if count > 0 then
        self:on_group_count(group_id, count)
    end
end

function CondBuildingCount:on_group_count(group_id, count)
    for i = 1, count do
        local uuid = group_id << 32 | i
        self:forward(uuid)
    end
end

function CondBuildingCount:on_stop()
    self.player:unwatch_event(self, "on_group_count")
end

function CondBuildingCount:check_cond(value)
    local scene = self.player:get_scene()
    for group_id, count in pairs(value) do
        local num = scene:get_group_num(group_id)
        if num >= count then
            local uuid = group_id << 32 | count
            self:forward(uuid)
        end
    end
    return true
end

function CondBuildingCount:check_one_cond(value)
    local res = { }
    for group_id, count in pairs(value) do
        local key = (group_id << 32) | count
        local has = self.progress[key] ~= nil
        res[sformat("%s:%s", group_id, count)] = has
    end
    return res
end

function CondBuildingCount:check_cond(value)
    local scene = self.player:get_scene()
    for group_id, count in pairs(value) do
        local num = scene:get_group_num(group_id)
        if num < count then
            return false
        end
    end
    return true
end

return CondBuildingCount