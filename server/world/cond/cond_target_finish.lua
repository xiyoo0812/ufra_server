--cond_target_finish.lua

local Target = import("world/cond/cond_target.lua")

local CondTargetFinish = class(Target)

function CondTargetFinish:on_init(conditions)
    if type(conditions) == "number" then
        return { conditions }
    end
    return conditions
end

function CondTargetFinish:on_start()
    self.player:watch_event(self, "on_target_finish")
end

function CondTargetFinish:on_target_finish(target_id)
    self:forward(target_id)
end

function CondTargetFinish:on_stop()
    self.player:unwatch_event(self, "on_target_finish")
end

function CondTargetFinish:on_add_new_condition(target_id)
    if self.player:has_finish_target(target_id) then
        self:forward(target_id)
    end
end

function CondTargetFinish:check_cond(value)
    for _, target_id in ipairs(value) do
        if not self.player:has_finish_target(target_id) then
            return false
        end
    end
    return true
end

return CondTargetFinish