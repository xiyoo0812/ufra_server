-- cond_npc_count.lua

-- open_npc.lua

local Target = import("world/cond/cond_target.lua")

local CondNpcCount = class(Target)

function CondNpcCount:on_init(count)
    return { count }
end

function CondNpcCount:on_start()
    self.player:watch_event(self, "on_recv_partner")
end

function CondNpcCount:on_add_new_condition(count)
    local number = self.player:get_partner_count()
    if number >= count then
        self:on_recv_partner(0)
    end
end

function CondNpcCount:on_recv_partner(npc_id)
    local number = self.player:get_partner_count()
    for i = 1, number do
        self:forward(i)
    end
end

function CondNpcCount:on_stop()
    self.player:unwatch_event(self, "on_recv_partner")
end

function CondNpcCount:check_cond(count)
    return self.player:get_partner_count() >= count
end

return CondNpcCount