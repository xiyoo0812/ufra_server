--item_mgr.lua
--用于处理道具内存数据
local log_debug     = logger.debug
local tsort         = table.sort
local tinsert       = table.insert

local config_mgr    = quanta.get("config_mgr")

local item_db           = config_mgr:init_table("item", "id")

local ItemMgr = singleton()
local prop = property(ItemMgr)
prop:reader("npc_exp_items", {})    --npc经验道具(key-item_id value-exp)
prop:reader("exp_sort_items", {})   --npc经验道具排序(exp从大到小)

function ItemMgr:__init()
    self:on_cfg_item_changed()
end

function ItemMgr:on_cfg_item_changed()
    for _, conf in item_db:iterator() do
        --加载npc经验道具
        if conf.npc_exp and conf.npc_exp ~= 0 then
            self.npc_exp_items[conf.id] = conf
            tinsert(self.exp_sort_items, conf)
            log_debug("[ItemMgr][on_cfg_item_changed] npc exp items({}) exp:{}", conf.id, conf.npc_exp)
        end
    end
    --经验值大小排序
    tsort(self.exp_sort_items, function(a, b)
        return a.npc_exp > b.npc_exp
    end)
end

--获取道具的经验值
function ItemMgr:get_items_exp(items)
    local exp = 0
    for item_id, num in pairs(items or {}) do
        local item_conf = self.npc_exp_items[item_id]
        if not item_conf then
            return false
        end
        exp = exp + item_conf.npc_exp * num
    end
    return true, exp
end

--经验转为道具
function ItemMgr:exp_to_item(exp)
    local items = {}
    for _, item_conf in pairs(self.exp_sort_items) do
        local cnt = math.floor(exp / item_conf.npc_exp)
        if cnt > 0 then
            items[item_conf.id] = cnt
        end
        exp = exp - item_conf.npc_exp * cnt
    end
    return items
end

-- export
quanta.item_mgr = ItemMgr()

return ItemMgr
