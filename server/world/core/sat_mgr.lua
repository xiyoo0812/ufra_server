--sat_manager.lua
--用于处理满意度
local log_debug     = logger.debug

local config_mgr    = quanta.get("config_mgr")

local sat_db           = config_mgr:init_table("town_sat", "sat_lv")
local effect_db         = config_mgr:init_table("effect", "id")
local EffectType        = enum("EffectType")

local SatMgr = singleton()
local prop = property(SatMgr)
prop:reader("sat_nf_per", {})    --满意度生活技能效果(key-满意度等级 value-改变百分比)

function SatMgr:__init()
    for _, conf in sat_db:iterator() do
        local sat_lv = conf.sat_lv
        local nfskill_per = 0
        for _, effect_id in pairs(conf.effect_ids or {}) do
            local effect = effect_db:find_one(effect_id)
            if effect.sub_type == EffectType.PARTNER_ATTR and effect.args[3] == "nfskill_per" then
                nfskill_per = nfskill_per + effect.args[2]
            end
        end
        self.sat_nf_per[sat_lv] = nfskill_per
        log_debug("[SatMgr][__init] sat_lv:{} nfskill_per:{}", sat_lv, nfskill_per)
    end
end


--获取属性各阶段加成效果
function SatMgr:get_attr_stage(base_value, conver_point, conver_per)
   local result = {}
   for sat_lv, nfskill_per in pairs(self.sat_nf_per or {}) do
        local result_attr = math.floor(base_value * (1 + nfskill_per / 100))
        log_debug("[SatMgr][get_attr_stage] base_value:{} result_attr:{} nfksill_per:{}", base_value, result_attr, nfskill_per)
        local capacity_per = math.floor((result_attr / conver_point)) * conver_per
        result[sat_lv] = capacity_per
   end
   log_debug("[SatMgr][get_attr_stage] base_value:{} result:{}", base_value, result)
   return result
end

-- export
quanta.sat_mgr = SatMgr()

return SatMgr
