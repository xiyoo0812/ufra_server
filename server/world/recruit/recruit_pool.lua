--recruit_pool.lua
local Random            = import("basic/random.lua")
local log_debug         = logger.debug
local log_err           = logger.err

local config_mgr        = quanta.get("config_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")
local recruit_mgr       = quanta.get("recruit_mgr")
local item_db           = config_mgr:init_table("item", "id")
local npc_db            = config_mgr:init_table("npc", "id")
local npc_star_db       = config_mgr:init_table("npc_star", "id")
local npc_lv_db         = config_mgr:init_table("npc_lv", "id")
local detail_db         = config_mgr:init_table("recruit_pool_detail", "id")
local Q_SSR             = protobuf_mgr:enum("quality", "QUALITY_ORANGE")
local Q_SR              = protobuf_mgr:enum("quality", "QUALITY_PURPLE")

local RecruitPool = class()
local prop = property(RecruitPool)

prop:reader("pool_id", 0)               --招募池ID
prop:reader("items", {})                --物品(key-道具ID value-道具概率配置)
prop:reader("proto", nil)               --配置
prop:reader("ssr_items", {})            --ssr道具(key-道具ID value-道具概率配置)
prop:reader("ssr_per", 0)               --ssr道具概率
prop:reader("ssr_rand", {})             --ssr内部随机
prop:reader("rand", nil)                --非ssr内部随机
prop:reader("sr_items", {})             --sr道具(key-道具ID value-道具概率配置)
prop:reader("sr_rand",{})               --sr随机
prop:reader("special_rules", {})        --特殊规则,自定次数产出特定npc或道具

function RecruitPool:__init(proto)
    self.proto = proto
    self.pool_id = proto.id
    self:load_pool(proto)
end

--加载招募池配置
function RecruitPool:load_pool(proto)
    log_debug("[RecruitPool][load_pool] pool_id:{} load", self.pool_id)
    local ssr_weight = 0
    local total_weight = 0
    for _, conf in detail_db:iterator() do
        if conf.pool_id ~= self.pool_id then
            goto continue
        end
        --查询道具或npc品质
        local item_conf
        if conf.item_type == 1 then
            item_conf = npc_db:find_one(conf.item_id)
            --等级是否配置
            if not npc_lv_db:find_one(item_conf.init_lv) then
                log_err("[RecruitPool][load_pool] pool_id:{} item_id:{} not found init_lv:{}", self.pool_id, conf.item_id, item_conf.init_lv)
                goto continue
            end
            --星级是否配置
            if not npc_star_db:find_one(recruit_mgr:get_star_id(item_conf.id, item_conf.star_lv)) then
                log_err("[RecruitPool][load_pool] pool_id:{} item_id:{} not found star_lv:{}", self.pool_id, conf.item_id, item_conf.star_lv)
                goto continue
            end
        else
            item_conf = item_db:find_one(conf.item_id)
        end
        --品质是否配置
        if not item_conf.quality then
            log_err("[RecruitPool][load_pool] pool_id:{} item_id:{} not found quality", self.pool_id, conf.item_id)
            goto continue
        end
        conf.quality = item_conf.quality
        if conf.quality == Q_SSR then
            self.ssr_items[conf.item_id] = conf
            ssr_weight = ssr_weight + conf.weight
        else
            if conf.quality == Q_SR then
                self.sr_items[conf.item_id] = conf
            end
            self.items[conf.item_id] = conf
        end
        total_weight  = total_weight + conf.weight
        ::continue::
    end
    --命中ssr概率
    self.ssr_per = ssr_weight * 100 / total_weight
    --ssr内部权重随机
    self.ssr_rand = Random()
    for _, ssr_item in pairs(self.ssr_items) do
        self.ssr_rand:add_wheel(ssr_item, ssr_item.weight)
    end
    --非ssr内部权重随机
    self.rand = Random()
    for _, item in pairs(self.items) do
        self.rand:add_wheel(item, item.weight)
    end
    --sr内部权重随机
    self.sr_rand = Random()
    for _, item in pairs(self.sr_items) do
        self.sr_rand:add_wheel(item, item.weight)
    end
    self:load_special(proto)
    self:check_conf(proto)
end

--加载特殊规则
function RecruitPool:load_special(proto)
    --特殊规则
    for count, detail_id in pairs(proto.special_rules or {}) do
        local detail = detail_db:find_one(detail_id)
        if not detail then
            log_err("[RecruitPool][load_pool] pool_id:{} special_rules count:{} detail_id:{} not found in detail.",
            self.pool_id, count, detail_id)
            goto continue
        end
        self.special_rules[count] = detail
        ::continue::
    end
end

--检查配置
function RecruitPool:check_conf(proto)
    --校验up权重
    if proto.up_id then
        if not (self.items[proto.up_id] or self.ssr_items[proto.up_id]) then
            log_err("[RecruitPool][load_pool] pool_id:{} up_id:{} not found weight in detail", self.pool_id, proto.up_id)
            return
        end
    end

    --如果开启品质保底则进行sr道具校验
    if proto.q_base_id and not next(self.sr_items) then
        log_err("[RecruitPool][load_pool] pool_id:{} open q_base_id({}) has not sr items  check excel pls.", self.pool_id, proto.q_base_id)
        return
    end

    --如果开启限定保底则应填写up卡
    if proto.n_base_id and not proto.up_id then
        log_err("[RecruitPool][load_pool] pool_id:{} open n_base_id({}) has not up_id  check excel pls.", self.pool_id, proto.n_base_id)
        return
    end

    --道具日志
    log_debug("[RecruitPool][load_pool] pool_id:{} ssr_per:{}", self.pool_id, self.ssr_per)
end

--随机
function RecruitPool:random(up)
    local ssr_p = self.ssr_per + up
    local rand = Random()
    --命中ssr
    if rand:ttratio(ssr_p * 100) then
        log_debug("[RecruitPool][random] pool_id:{} hit ssr ssr_p:{}", self.pool_id, ssr_p)
        return self.ssr_rand:rand_wheel()
    else
        return self.rand:rand_wheel()
    end
end

--sr随机
function RecruitPool:random_sr()
    return self.sr_rand:rand_wheel()
end

--获取up卡ID
function RecruitPool:get_up_id()
    return self.proto.up_id
end

--获取限定卡
function RecruitPool:get_up_item()
    if not self.proto.up_id then
        return
    end
    return self.ssr_items[self.proto.up_id] or self.items[self.proto.up_id]
end

--特殊规则产出
function RecruitPool:get_special(count)
    local rule = self.special_rules[count]
    if not rule then
        log_debug("[RecruitPool][get_special] pool_id:{} count:{} not rule", self.pool_id, count)
    else
        log_debug("[RecruitPool][get_special] pool_id:{} count:{} rule:{}", self.pool_id, count, rule)
    end
    return rule
end

return RecruitPool
