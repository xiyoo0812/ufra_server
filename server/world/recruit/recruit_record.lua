--recruit_record.lua
--招募历史记录
local RecruitRecord = class(nil)
local dbprop = db_property(RecruitRecord, "player_recruit")
dbprop:store_value("pool_id", 0)        --招募池中详细ID
dbprop:store_value("item_type", 0)      --1npc 2道具
dbprop:store_value("item_id", 0)        --npcid或道具ID
dbprop:store_value("seq_no", 0)         --序号
dbprop:store_value("time", 0)           --招募时间(秒)


function RecruitRecord:__init(pool_id, item_type, item_id, seq_no)
    self.pool_id = pool_id
    self.item_type = item_type
    self.item_id = item_id
    self.seq_no = seq_no
    self.time = quanta.now
end

function RecruitRecord:load_db(data)
    self.time = data.time
end

--序列化
function RecruitRecord:pack2db()
    local record = {
        pool_id       = self.pool_id,
        item_type     = self.item_type,
        item_id       = self.item_id,
        seq_no        = self.seq_no,
        time          = self.time,
    }
    return record
end
--pb
function RecruitRecord:pack2client()
    local record = {
        pool_id       = self.pool_id,
        item_type     = self.item_type,
        item_id       = self.item_id,
        seq_no        = self.seq_no,
        time          = self.time,
    }
    return record
end

return RecruitRecord
