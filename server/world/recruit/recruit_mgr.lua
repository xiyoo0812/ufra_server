--recruit_mgr.lua

local log_err           = logger.err
local log_debug         = logger.debug
local qfailed           = quanta.failed
local config_mgr        = quanta.get("config_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")
local event_mgr         = quanta.get("event_mgr")

local pool_db           = config_mgr:init_table("recruit_pool", "id")
local up_db             = config_mgr:init_table("recruit_qup", "count")
local npc_db            = config_mgr:init_table("npc", "id")
local npc_star_db       = config_mgr:init_table("npc_star", "id")
local npc_lv_db         = config_mgr:init_table("npc_lv", "id")

local OBTAIN_RECRUIT    = protobuf_mgr:enum("obtain_reason", "NID_OBTAIN_RECRUIT")

local RecruitMgr = singleton()
local prop = property(RecruitMgr)
prop:reader("pools", {})       --招募池(key-招募池ID value-招募池信息)
prop:reader("rand", nil)       --随机轮盘

function RecruitMgr:__init()
    --初始化随机池
    self:init_pool()
end

--初始化随机池
function RecruitMgr:init_pool()
    self:check_npc_conf()
    event_mgr:fire_frame(function()
        self:load_recr_pool()
    end)
end

--检查npc配置
function RecruitMgr:check_npc_conf()
    for _, conf in npc_db:iterator() do
        --等级配置检查
        if conf.init_lv and not npc_lv_db:find_one(conf.init_lv) then
                log_err("[RecruitMgr][check_npc_conf] canot found npc_id:{} init_lv:{}", conf.id, conf.init_lv)
        end
        --星级配置检查
        if conf.star_lv then
            local star_id = self:get_star_id(conf.id, conf.star_lv)
            if not npc_star_db:find_one(star_id) then
                log_err("[RecruitMgr][check_npc_conf] canot found npc_id:{} star_id:{}", conf.id, star_id)
            end
        end
    end
end

--加载卡池
function RecruitMgr:load_recr_pool()
    local RecruitPool = import("world/recruit/recruit_pool.lua")
    --log_debug("[RecruitMgr][load_recr_pool] pool_db:{}", pool_db)
    for _, conf in pool_db:iterator() do
        local pool = RecruitPool(conf)
        self.pools[conf.id] = pool
    end
end

--获取招募池
function RecruitMgr:get_pool(pool_id)
    return self.pools[pool_id]
end

--产出SR物品
function RecruitMgr:random_sr(pool_id)
    local pool = self.pools[pool_id]
    if not pool then
        log_err("[RecruitMgr][randomSR] pool_id:{} not found", pool_id)
        return
    end
    return pool:random_sr()
end

--随机
function RecruitMgr:random(pool_id, cnt)
    local pool = self.pools[pool_id]
    if not pool then
        log_err("[RecruitMgr][random] pool_id:{} not found", pool_id)
        return
    end
    --次数SSR增益
    local up = 0
    local up_conf = up_db:find_one(cnt)
    if up_conf and not pool.q_base_id then
        up = up_conf.up
        log_debug("[RecruitMgr][random] pool_id:{} cnt:{} up:{}", pool_id, cnt, up)
    end

    return pool:random(up)
end

--已有npc转为碎片和金币
function RecruitMgr:npc_repeated(player, proto_id)
    if not player then
        return
    end
    local npc = npc_db:find_one(proto_id)
    if not npc then
        log_err("[RecruitMgr][npc_repeated] proto_id:{} not found", proto_id)
        return
    end
    --返还招募商店货币
    local drops = { drop = npc.rec_item, reason = OBTAIN_RECRUIT }
    local ok, code = player:execute_drop(drops, true)
    if qfailed(code, ok) then
        log_err("[RecruitMgr][npc_repeated] allot failed!", proto_id)
        return
    end
end

--获取等级ID
function RecruitMgr:get_lv_id(proto_id, limit_lv, lv)
    return proto_id * 10000 + limit_lv * 100 + lv
end

--获取星级ID
function RecruitMgr:get_star_id(proto_id, start_lv)
    return proto_id * 100 + start_lv
end

-- export
quanta.recruit_mgr = RecruitMgr()

return RecruitMgr
