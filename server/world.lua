--quanta
import("kernel.lua")
local env_addr  = environ.addr

quanta.startup(function()
    --创建客户端网络管理
    local NetServer = import("network/net_server.lua")
    local ip, port = env_addr("QUANTA_WORLD_ADDR")
    local client_mgr = NetServer("world")
    client_mgr:setup(ip, port, true)
    quanta.client_mgr = client_mgr
    --初始化world
    import("world/init.lua")
end)
