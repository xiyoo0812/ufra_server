--quanta
import("kernel.lua")

quanta.startup(function()
    --初始化bilog
    import("network/http_client.lua")
    import("bilog/bi_dao.lua")
    import("bilog/bilog_mgr.lua")
    import("bilog/worker/safe_agent.lua")
    import("bilog/bilog_servlet.lua")
    import("bilog/push_servlet.lua")
    import("agent/gm_agent.lua")
    import("bilog/gm/bi_gm.lua")
end)
