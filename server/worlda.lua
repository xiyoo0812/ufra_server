--worlda.lua
import("kernel.lua")

quanta.startup(function()
    import("common/constant.lua")
    import("worlda/init.lua")
end)