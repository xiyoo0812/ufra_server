--account.lua
local tsize         = qtable.size
local tinsert       = table.insert
local guid_new      = codec.guid_new

local store_mgr     = quanta.get("store_mgr")

local Account = class()
local prop = property(Account)
prop:reader("open_id", "")    --open_id

local dprop = db_property(Account, "account", true)
dprop:store_value("token", 0)       --token
dprop:store_value("lobby", 0)       --lobby
dprop:store_value("user_id", 0)     --user_id
dprop:store_value("device_id", 0)   --device_id
dprop:store_value("create_time", 0) --create_time
dprop:store_value("channel","")     --channel
dprop:store_values("params", {})    --params
dprop:store_values("characters", {})     --characters

function Account:__init(open_id)
    self.open_id = open_id
end

function Account:create(token, device_id, params, channel)
    self.token = token
    self.params = params
    self.device_id = device_id
    self.create_time = quanta.now
    self.channel = channel or "default"
    self.user_id = guid_new(quanta.service, quanta.index)
    self:flush_account_db(true)
    return true
end

function Account:is_newbee()
    return self.create_time == 0
end

function Account:update_params(params)
    for key, value in pairs(params) do
        self:save_params_field(key, value)
    end
end

function Account:get_character(character_id)
    return self.characters[character_id]
end

function Account:get_character_count()
    return tsize(self.characters)
end

function Account:load()
    return store_mgr:load(self, self.open_id, "account")
end

function Account:on_db_account_load(data)
    if data.open_id then
        self.token = data.token
        self.lobby = data.lobby
        self.params = data.params
        self.user_id = data.user_id
        self.device_id = data.device_id
        self.create_time = data.create_time
        self.channel = data.channel or "default"
        self.characters = data.characters or {}
    end
end

function Account:del_character(character_id)
    local character = self.characters[character_id]
    if character then
        self:del_characters_field(character_id)
        return true
    end
    return false
end

function Account:pack2client()
    local characters = {}
    for character_id, character in pairs(self.characters or {}) do
        tinsert(characters, { character_id = character_id, gender = character.gender, nick = character.nick, facade = character.facade })
    end
    return {
        characters = characters,
        error_code = 0,
        user_id = self.user_id,
    }
end

return Account
