--login_agent.lua

local log_err       = logger.err
local log_debug     = logger.debug
local sformat       = string.format

local scheduler     = quanta.get("scheduler")

local LOGIN_WORKER  = environ.number("QUANTA_LOGIN_WORKER", 2)
-- 消息类型
local SAFE_MSG_NICK = quanta.enum("SafeMsgType", "NICK")

local LoginAgent    = singleton()
local prop = property(LoginAgent)
prop:reader("workers", {})
prop:reader("index", 0)

function LoginAgent:__init()
    --启动登录线程
    for i = 1, LOGIN_WORKER do
        local name = sformat("login%s", i)
        scheduler:startup(name, "lobby.worker.login")
        self.workers[i] = name
    end
end

function LoginAgent:choose_worker()
    local index = self.index + 1
    local worker = self.workers[index]
    self.index = index >= LOGIN_WORKER and 0 or index
    return worker
end

-- 字节登录请求
function LoginAgent:bytedance_login_verify(token)
    local worker = self:choose_worker()
    local ok, status, res = scheduler:call(worker, "bytedance_login_verify", token)
    if not ok or status >= 300 then
        log_err("[LoginAgent][bytedance_login_verify] failed!  err: {} status:{} res:{}", res, status, res)
        return false, ok and res
    end
    log_debug("[LoginAgent][bytedance_login_verify] login token({}) res({}) ", token, res)
    return true, res
end

-- 字节准入限制
function LoginAgent:bytedance_login_limit(token, channel_id, channel, region, language, pac_code)
    local worker = self:choose_worker()
    local ok, status, res = scheduler:call(worker, "bytedance_login_limit", token,channel_id, channel,region, language, pac_code)
    if not ok or status >= 300 then
        log_err("[LoginAgent][bytedance_login_limit] failed!  err: {} status:{} res:{}", res, status, res)
        return false, ok and res
    end
    log_debug("[LoginAgent][bytedance_login_limit] limit token({}) res({}) ", token, res)
    return true, res
end

-- pathea准入限制
function LoginAgent:pathea_login_limit(token, ip, region, language, pac_code)
    local worker = self:choose_worker()
    local ok, status, res = scheduler:call(worker, "pathea_login_limit", token, ip, region, language, pac_code)
    if not ok or status >= 300 then
        log_err("[LoginAgent][pathea_login_limit] failed!  err: {} status:{} res:{}", res, status, res)
        return false, ok and res
    end
    log_debug("[LoginAgent][pathea_login_limit] limit token({})  res({}) ", token, res)
    return true, res
end

--字节安全服务
function LoginAgent:bytedance_safe_check(ip, language, text, dev_plat)
    local worker = self:choose_worker()
    local ok, status, res, code = scheduler:call(worker, "bytedance_safe_check", ip, language, text, dev_plat, SAFE_MSG_NICK)
    if not ok or status >= 300 then
        log_err("[LoginAgent][safe_check] failed! err: {} status:{} res:{}", res, status, res)
        return false, ok and res, code
    end
    return true, res, code
end

quanta.login_agent = LoginAgent()

return LoginAgent
