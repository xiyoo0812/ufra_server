--login_worker.lua
import("network/http_client.lua")

local sformat           = string.format

local log_debug         = logger.debug
local json_decode       = json.decode
local lhex_encode       = ssl.hex_encode
local lhmac_sha256      = ssl.hmac_sha256
local ltime             = timer.time

local event_mgr         = quanta.get("event_mgr")
local http_client       = quanta.get("http_client")

local CLUSTER           = environ.number("QUANTA_CLUSTER")
local REGION            = environ.number("QUANTA_REGION")
local APP_ID            = environ.number("QUANTA_APP_ID")
local SECRET_KEY        = environ.get("QUANTA_SECRET_KEY")
local ACCESS_KEY        = environ.get("QUANTA_ACCESS_KEY")
local SDK_EXPIRE_TIME   = environ.get("QUANTA_SDK_EXPIRE_TIME")
local SDK_SANDBOX       = environ.status("QUANTA_SDK_SANDBOX")
local SDK_URL           = sformat("%s/verify_user", environ.get("QUANTA_SDK_URL"))
local CHECK_URL         = sformat("%s/access_check", environ.get("QUANTA_SDK_URL"))
local REPORT_URL        = sformat("%s/verify_user", environ.get("QUANTA_REPORT_URL"))
local SANDBOX_SDK_URL   = sformat("%s/verify_user", environ.get("QUANTA_SANDBOX_SDK_URL"))
local SANDBOX_CHECK_URL = sformat("%s/access_check", environ.get("QUANTA_SANDBOX_SDK_URL"))

local LoginWorker = singleton()

function LoginWorker:__init()
    event_mgr:add_listener(self, "bytedance_login_verify")
    event_mgr:add_listener(self, "bytedance_login_limit")
    event_mgr:add_listener(self, "pathea_login_limit")

    log_debug("[LoginWorker][init] SDK_URL:{}", SDK_URL)
    log_debug("[LoginWorker][init] CHECK_URL:{}", CHECK_URL)
    log_debug("[LoginWorker][init] REPORT_URL:{}", REPORT_URL)
    log_debug("[LoginWorker][init] SDK_SANDBOX:{}", SDK_SANDBOX)
    log_debug("[LoginWorker][init] SANDBOX_SDK_URL:{}", SANDBOX_SDK_URL)
    log_debug("[LoginWorker][init] SANDBOX_CHECK_URL:{}", SANDBOX_CHECK_URL)
end

-- 字节登录
function LoginWorker:bytedance_login_verify(token)
    local ts = quanta.now
    local post_url = SDK_SANDBOX and SANDBOX_SDK_URL or SDK_URL
    local data = sformat("app_id=%s&access_token=%s&server_id=%s&ts=%s", APP_ID, token, REGION, ts)
    -- 计算签名
    local signKeyInfo = sformat("auth-v2/%s/%d/%d", ACCESS_KEY, ts, SDK_EXPIRE_TIME);
    local signKey = lhex_encode(lhmac_sha256(SECRET_KEY, signKeyInfo))
    local signature = lhex_encode(lhmac_sha256(signKey, data))
    local result = sformat("%s/%s", signKeyInfo, signature)

    -- 构造http请求
    local headers = {
        ["Content-type"] = "application/x-www-form-urlencoded;charset=UTF-8",
        ["Agw-Auth"] = result
    }
    local send_ms = ltime()
    local ok, status, res = http_client:call_post(post_url, data, headers, data)
    local escape_ms = ltime() - send_ms
    -- 上报登录接口健康度
    self:report_verify(escape_ms, status, res)
    return ok and status or 404, res
end

-- 字节准入限制
function LoginWorker:bytedance_login_limit(token, channel_id, channel, region, language, pac_code)
    local ts = quanta.now
    local post_url = SDK_SANDBOX and SANDBOX_CHECK_URL or CHECK_URL
    local data = sformat("app_id=%s&access_token=%s&server_id=%s&channel_id=%s&channel=%s&region=%s&language=%s&ts=%s",
                APP_ID, token, REGION, channel_id, channel, region, language, ts)

    if pac_code then
        data = sformat('%s&pac_code=%s', data, pac_code)
    end
    -- 计算签名
    local signKeyInfo = sformat("auth-v2/%s/%d/%d", ACCESS_KEY, ts, SDK_EXPIRE_TIME);
    local signKey = lhex_encode(lhmac_sha256(SECRET_KEY, signKeyInfo))
    local signature = lhex_encode(lhmac_sha256(signKey, data))
    local result = sformat("%s/%s", signKeyInfo, signature)

    -- 构造http请求
    local headers = {
        ["Content-type"] = "application/x-www-form-urlencoded;charset=UTF-8",
        ["Agw-Auth"] = result
    }

    local ok, status, res = http_client:call_post(post_url, data, headers, data)
    return ok and status or 404, res
end

-- pathea准入限制
function LoginWorker:pathea_login_limit(token, ip, region, language, pac_code)
    local ts = quanta.now
    local post_url = SDK_URL
    local data = sformat("token=%s&server_id=%s&region=%s&language=%s&ts=%s&ip=%s",
                 token, CLUSTER,  region, language, ts, ip)

    if pac_code then
        data = sformat('%s&pac_code=%s', data, pac_code)
    end
    -- 计算签名
    local signKeyInfo = sformat("auth-v2/%s/%d/%d", ACCESS_KEY, ts, SDK_EXPIRE_TIME);
    local signKey = lhex_encode(lhmac_sha256(SECRET_KEY, signKeyInfo))
    local signature = lhex_encode(lhmac_sha256(signKey, data))
    local result = sformat("%s/%s", signKeyInfo, signature)

    -- 构造http请求
    local headers = {
        ["Content-type"] = "application/x-www-form-urlencoded;charset=UTF-8",
        ["Agw-Auth"] = result
    }
    log_debug("[LoginWorker][pathea_login_limit] post_url:{} data:{}", post_url, data)
    local ok, status, res = http_client:call_post(post_url, data, headers, data)
    log_debug("[LoginWorker][pathea_login_limit] res:{}", res)
    return ok and status or 404, res
end

-- 上报登录接口健康度
function LoginWorker:report_verify(escape_ms, status, res)
    local code
    if status ~= 200 then
        code = -999
    else
        local res_data = json_decode(res)
        code = res_data.code
    end
    local params = {
        server_id     = REGION,
        code          = code,
        latency_ms    = escape_ms
    }

    log_debug("[LoginWorker][report_verify] params:{}", params)
    http_client:call_post(REPORT_URL, params)
end

quanta.login_worker = LoginWorker()
