-- plat_servlet.language
local str_len               = string.len
local log_debug             = logger.debug
local qfailed               = quanta.failed
local LIMIT_STATE           = environ.status("QUANTA_SDK_LIMIT_STATE")

local event_mgr             = quanta.get("event_mgr")
local config_mgr            = quanta.get("config_mgr")
local login_agent           = quanta.get("login_agent")
local protobuf_mgr          = quanta.get("protobuf_mgr")
local check_agent           = quanta.get("safe_check_agent")
local login_dao             = quanta.get("login_dao")
local utility_db            = config_mgr:init_table("utility", "key")
-- 消息类型
local SAFE_MSG_NICK         = quanta.enum("SafeMsgType", "NICK")

local FRAME_SUCCESS         = protobuf_mgr:error_code("FRAME_SUCCESS")
local FRAME_FAILED          = protobuf_mgr:error_code("FRAME_FAILED")
local PLATFORM_ERROR        = protobuf_mgr:error_code("LOGIN_PLATFORM_ERROR")
local SAFE_MAX_WORD_ERR     = protobuf_mgr:error_code("SAFE_MAX_WORD_ERR")
local ACCOUTN_MAX_ERR       = protobuf_mgr:error_code("LOGIN_ACCOUTN_MAX")
local ROLE_NAME_ERR         = protobuf_mgr:error_code("LOGIN_ROLE_NAME_ERR")

local PLAT_BYTEDANCE        = protobuf_mgr:enum("platform_type", "PLATFORM_BYTEDANCE")
local PLATFORM_PATHEA       = protobuf_mgr:enum("platform_type", "PLATFORM_PATHEA")
local PLATFORM_GUEST        = protobuf_mgr:enum("platform_type", "PLATFORM_GUEST")
local errcode_db            = config_mgr:init_table("sdk_errcode", "channel", "sdk_errcode")
local NAME_LENGTH           = utility_db:find_number("value", "role_name_length")

local PlatServlet = singleton()
local prop = property(PlatServlet)
prop:reader("account_max", {})  --账号上限(key-渠道 value-当前上限)

function PlatServlet:__init()
    -- 事件监听
    event_mgr:add_listener(self, "on_platform_login")
    -- 事件监听
    event_mgr:add_listener(self, "on_safe_text")
    -- BGIP修改进入人数
    event_mgr:add_listener(self, "rpc_bgip_account_limit")
end

--修改上限
function PlatServlet:rpc_bgip_account_limit(package_channel, num)
    log_debug("[PlatServlet][rpc_bgip_account_limit] package_channel:{} num:{}", package_channel, num)
    self.account_max[package_channel] = num
    return FRAME_FAILED, {msg = "success"}
end

function PlatServlet:on_account_limit(package_channel)
    if not package_channel or package_channel == "" then
        return FRAME_SUCCESS
    end
    --是否已加载限制条件
    if not self.account_max[package_channel] then
        local db_ac_limit = login_dao:load_ac_limit(package_channel)
        if db_ac_limit then
            self.account_max[package_channel] = tonumber(db_ac_limit) or 0
        else
            self.account_max[package_channel] = 0
        end
    end
    local max_num = self.account_max[package_channel]
    --设置为0不限制
    if  max_num == 0 then
        return FRAME_SUCCESS
    end

    local ok, cur_num = login_dao:account_count(package_channel)
    log_debug("[PlatServlet][on_account_limit] ok:{} package_channel:{} cur_num:{} max_num:{}", ok,package_channel, cur_num, max_num)
    if not ok then
        return FRAME_SUCCESS
    end
    if cur_num >= max_num then
        return ACCOUTN_MAX_ERR
    end
    return FRAME_SUCCESS
end

function PlatServlet:on_safe_text(user_id, text)
    --长度校验不超过NAME_LENGTH字符
    log_debug("[PlatServlet][on_safe_text] length:{} max:{}",str_len(text), NAME_LENGTH)
    if str_len(text) > NAME_LENGTH then
        return SAFE_MAX_WORD_ERR
    end
    local ok, res_text =  check_agent:on_safe_check(user_id, text, SAFE_MSG_NICK)
    if ok ~= FRAME_SUCCESS then
        return ROLE_NAME_ERR
    end
    return ok, res_text
end

function PlatServlet:on_platform_login(platform, open_id, token, body, account_params, newbee)
    --构建登录参数
    account_params.lang       = body.language
    account_params.dev_plat   = body.dev_platform
    account_params.package_channel = body.package_channel
    log_debug("[PlatServlet][on_platform_login] platform:{}", platform)
    if platform == PLAT_BYTEDANCE then
        return self:on_bytedance_login(token, body, newbee)
    elseif platform == PLATFORM_PATHEA or platform == PLATFORM_GUEST then
        return self:on_pathea_login(token, body, newbee)
    end
    return PLATFORM_ERROR
end

function PlatServlet:on_bytedance_login(token, body, newbee)
    local ok, res = login_agent:bytedance_login_verify(token)
    if not ok then
        return FRAME_FAILED
    end
    -- 三方错误码处
    local res_code = res.code
    if qfailed(res_code) then
        -- 如果是特定错误码则转码
        local game_errcode = errcode_db:find_integer("game_errcode", PLAT_BYTEDANCE, res_code)
        return game_errcode or res_code
    end
    local code = self:bytedance_limit(token, body)
    if qfailed(code) then
        local game_errcode = errcode_db:find_integer("game_errcode", PLAT_BYTEDANCE, code)
        return game_errcode or code
    end
    --账号注册数量限制
    if newbee then
        local limit_code = self:on_account_limit(body.package_channel)
        if limit_code ~= FRAME_SUCCESS then
            return limit_code
        end
    end
    return FRAME_SUCCESS, res.data.sdk_open_id, res.data.device_id
end

--字节准入限制
function PlatServlet:bytedance_limit(token, body)
    -- 准入限制状态(0开启 1关闭 空开启)
    if LIMIT_STATE then
        return FRAME_SUCCESS
    end

    local channel_id, channel, region, language, pac_code = body.channel_id, body.channel, body.region, body.language, body.pac_code
    local ok, res = login_agent:bytedance_login_limit(token, channel_id, channel, region, language, pac_code)
    if not ok then
        return FRAME_FAILED
    end
    local res_code = res.code
    if qfailed(res_code) then
        return res_code
    end
    return FRAME_SUCCESS
end

-- pathea验证
function PlatServlet:on_pathea_login(token, body, newbee)
    local code, res = self:pathea_limit(token, body)
    if qfailed(code) then
        local game_errcode = errcode_db:find_integer("game_errcode", PLATFORM_PATHEA, code)
        log_debug("[PlatServlet][on_pathea_login] game_errcode:{} code:{}", game_errcode, code)
        return game_errcode or code
    end
    --账号注册数量限制
    if newbee then
        local limit_code = self:on_account_limit(body.package_channel)
        if limit_code ~= FRAME_SUCCESS then
            log_debug("[PlatServlet][on_pathea_login] limit_code:{}", limit_code)
            return limit_code
        end
    end
    log_debug("[PlatServlet][on_pathea_login] res:{}", res)
    return FRAME_SUCCESS, res.data.sdk_open_id, res.data.device_id
end

-- password准入限制
function PlatServlet:pathea_limit(token, body)
    local region, language, pac_code, ip =body.region, body.language, body.pac_code, body.ip
    local ok, res = login_agent:pathea_login_limit(token, ip, region, language,  pac_code)
    if not ok then
        return FRAME_FAILED
    end
    local res_code = res.code
    if qfailed(res_code) then
        return res_code
    end
    return FRAME_SUCCESS, res
end

quanta.plat_servlet = PlatServlet()

return PlatServlet
