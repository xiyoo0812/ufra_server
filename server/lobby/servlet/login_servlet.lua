--login_servlet.lua

local log_err               = logger.err
local log_info              = logger.info
local log_debug             = logger.debug
local qfailed               = quanta.failed
local tunpack               = table.unpack
local guid_encode           = codec.guid_encode

local login_dao             = quanta.get("login_dao")
local event_mgr             = quanta.get("event_mgr")
local client_mgr            = quanta.get("client_mgr")
local protobuf_mgr          = quanta.get("protobuf_mgr")

local FRAME_FAILED          = protobuf_mgr:error_code("FRAME_FAILED")
local ACCOUTN_INLINE        = protobuf_mgr:error_code("LOGIN_ACCOUTN_INLINE")
local VERIFY_FAILED         = protobuf_mgr:error_code("LOGIN_VERIFY_FAILED")
local ROLE_NOT_EXIST        = protobuf_mgr:error_code("LOGIN_ROLE_NOT_EXIST")
local ROLE_NUM_LIMIT        = protobuf_mgr:error_code("LOGIN_ROLE_NUM_LIMIT")
local ACCOUTN_OFFLINE       = protobuf_mgr:error_code("LOGIN_ACCOUTN_OFFLINE")

local PLATFORM_PASSWORD     = protobuf_mgr:enum("platform_type", "PLATFORM_PASSWORD")

local LoginServlet = singleton()

function LoginServlet:__init()
    -- cs协议监听
    --protobuf_mgr:register(self, "NID_LOGIN_RANDOM_NAME_REQ", "on_random_name_req")
    protobuf_mgr:register(self, "NID_LOGIN_CHAR_CREATE_REQ", "on_char_create_req")
    protobuf_mgr:register(self, "NID_LOGIN_CHAR_DELETE_REQ", "on_char_delete_req")
    protobuf_mgr:register(self, "NID_LOGIN_ACCOUNT_LOGIN_REQ", "on_account_login_req")
    protobuf_mgr:register(self, "NID_LOGIN_ACCOUNT_RELOAD_REQ", "on_account_reload_req")
end


--账号登陆
function LoginServlet:on_account_login_req(session, cmd_id, body, session_id)
    local open_id, access_token, platform, channel = body.openid, body.session, body.platform, body.package_channel
    log_debug("[LoginServlet][on_account_login_req] open_id({}) token({}) body:{} login req!", open_id, access_token, body)
    if session.account then
        return client_mgr:callback_errcode(session, cmd_id, ACCOUTN_INLINE, session_id)
    end
    local account_params = {}
    local device_id = body.device_id
    --加载账号信息
    local account = login_dao:load_account(open_id)
    if not account then
        log_err("[LoginServlet][on_account_login_req] load account failed! open_id: {} token:{}", open_id, access_token)
        return client_mgr:callback_errcode(session, cmd_id, FRAME_FAILED, session_id)
    end

    if platform ~= PLATFORM_PASSWORD then
        --登录验证
        body.ip = session.ip
        local result = event_mgr:notify_listener("on_platform_login", platform, open_id, access_token, body, account_params, account:is_newbee())
        local ok, code, sdk_open_id, sdk_device_id = tunpack(result)
        local login_failed, login_code = qfailed(code, ok)
        if login_failed then
            log_err("[LoginServlet][on_account_login_req] verify failed! open_id: {} token:{} code:{}", open_id, access_token, login_code)
            client_mgr:callback_errcode(session, cmd_id, login_code, session_id)
            return false
        end
        -- 三方信息
        open_id = sdk_open_id
        device_id = sdk_device_id
    end

    --创建账号
    if account:is_newbee() then
        if not account:create(access_token, device_id, account_params, channel) then
            log_err("[LoginServlet][on_account_login_req] open_id({}) create account failed!", open_id)
            return client_mgr:callback_errcode(session, cmd_id, FRAME_FAILED, session_id)
        end
        session.account = account
        event_mgr:notify_listener("on_account_create", account, device_id, session.ip,
                                    account_params.lang, account_params.dev_plat)
        client_mgr:callback_by_id(session, cmd_id, account:pack2client(), session_id)
        log_info("[LoginServlet][on_account_login_req] newbee success! open_id: {}", open_id)
        return
    end
    --密码验证
    if platform == PLATFORM_PASSWORD and account:get_token() ~= access_token then
        log_err("[LoginServlet][on_password_login] verify failed! open_id: {} token: {}-{}", open_id, access_token, account:get_token())
        return client_mgr:callback_errcode(session, cmd_id, VERIFY_FAILED, session_id)
    end
    session.account = account
    account:save_token(access_token)
    account:save_device_id(device_id)
    account:update_params(account_params)
    event_mgr:notify_listener("on_account_login", account:get_user_id(), open_id, device_id, session.ip, account_params.lang, account_params.dev_plat)
    if not client_mgr:callback_by_id(session, cmd_id, account:pack2client(), session_id) then
        log_info("[LoginServlet][on_account_login_req] callback failed! open_id: {}, user_id: {}", open_id, account:get_user_id())
        return
    end
    log_info("[LoginServlet][on_account_login_req] success! open_id: {}, user_id: {}", open_id, account:get_user_id())
end

--创建角色
function LoginServlet:on_char_create_req(session, cmd_id, body, session_id)
    local user_id, name = body.user_id, body.nick
    log_debug("[LoginServlet][on_char_create_req] user({}) name({}) create role req!", user_id, name)
    local account = session.account
    if not account or account.user_id ~= user_id then
        log_err("[LoginServlet][on_char_create_req] user_id({}) need login!", user_id)
        return client_mgr:callback_errcode(session, cmd_id, ACCOUTN_OFFLINE, session_id)
    end
    log_debug("account:account.user_id :{}",account.user_id)
    if account:get_character_count() > 5 then
        log_err("[LoginServlet][on_char_create_req] user_id({}) role num limit!", user_id)
        return client_mgr:callback_errcode(session, cmd_id, ROLE_NUM_LIMIT, session_id)
    end
    --检查名称合法性
    local ok, codatas = login_dao:check_character(user_id, name)
    log_debug("[LoginServlet][on_char_create_req] ok:{} codatas:{}", ok, codatas)
    if not ok then
        return client_mgr:callback_errcode(session, cmd_id, codatas, session_id)
    end
    --创建角色
    local character_id = codatas[2]
    if not login_dao:create_character(account:get_open_id(), character_id, body) then
        log_err("[LoginServlet][on_char_create_req] user_id({}) create role failed!", user_id)
        return client_mgr:callback_errcode(session, cmd_id, FRAME_FAILED, session_id)
    end
    account:save_characters_field(character_id, body)
    event_mgr:notify_listener("on_role_create", user_id, character_id, body)
    local rdata = { character_id = character_id, gender = body.gender, nick = body.nick, facade = body.facade }
    if not client_mgr:callback_by_id(session, cmd_id, { error_code = 0, role = rdata }, session_id) then
        log_info("[LoginServlet][on_char_create_req] user_id({}) create role {} callback failed!", user_id, name)
        return
    end
    log_info("[LoginServlet][on_char_create_req] user_id({}) create role {} success!", user_id, name)
end

--删除角色
function LoginServlet:on_char_delete_req(session, cmd_id, body, session_id)
    local user_id, character_id = body.user_id, body.character_id
    log_debug("[LoginServlet][on_role_delete_req] user_id({}) character_id({}) delete req!", user_id, character_id)
    local account = session.account
    if not account or account:get_user_id() ~= user_id then
        log_err("[LoginServlet][on_role_delete_req] user_id({}) need login!", user_id)
        return client_mgr:callback_errcode(session, cmd_id, ACCOUTN_OFFLINE, session_id)
    end
    if not account:del_character(character_id) then
        log_err("[LoginServlet][on_role_delete_req] user_id({}) character_id({}) role not exist!", user_id, character_id)
        return client_mgr:callback_errcode(session, cmd_id, ROLE_NOT_EXIST, session_id)
    end
    if not client_mgr:callback_by_id(session, cmd_id, { error_code = 0, character_id = character_id }, session_id) then
        log_info("[LoginServlet][on_role_delete_req] user_id({}) character_id({}) callback failed!", user_id, character_id)
        return
    end
    log_info("[LoginServlet][on_role_delete_req] user_id({}) character_id({}) delete success!", user_id, character_id)
end

--账号重登
function LoginServlet:on_account_reload_req(session, cmd_id, body, session_id)
    local open_id, token, device_id = body.openid, body.session, body.device_id
    log_debug("[LoginServlet][on_account_reload_req] openid({}) token({}) device_id({}) reload req!",
            open_id, token, device_id)
    if session.account then
        return client_mgr:callback_errcode(session, cmd_id, ACCOUTN_INLINE, session_id)
    end
    --验证token
    local account = login_dao:load_account(open_id)
    if not account then
        log_err("[LoginServlet][on_account_login_req] load account failed! open_id: {} token:{}", open_id, token)
        return client_mgr:callback_errcode(session, cmd_id, FRAME_FAILED, session_id)
    end
    if account:is_newbee() then
        log_err("[LoginServlet][on_account_reload_req] open_id({}) load account status failed!", open_id)
        return client_mgr:callback_errcode(session, cmd_id, FRAME_FAILED, session_id)
    end
    local old_token = account:get_token()
    local old_device_id = account:get_device_id()
    if token ~= old_token or device_id ~= old_device_id then
        log_err("[LoginServlet][on_account_reload_req] verify failed! open_id: {}, token: {}-{} device_id:{}-{}",
        open_id, token, old_token, device_id, old_device_id)
        return client_mgr:callback_errcode(session, cmd_id, VERIFY_FAILED, session_id)
    end
    session.account = account
    if not client_mgr:callback_by_id(session, cmd_id, account:pack2client(), session_id) then
        log_info("[LoginServlet][on_account_reload_req] callback failed! open_id: {}", open_id)
        return
    end
    log_info("[LoginServlet][on_account_reload_req] success! open_id: {}, user_id: {}", open_id, account:get_user_id())
end

--随机名字
function LoginServlet:on_random_name_req(session, cmd_id, body, session_id)
    local rname = guid_encode()
    log_debug("[LoginServlet][on_random_name_req] open_id({}) randname: {}!", session.open_id, rname)
    local callback_data = { error_code = 0, name = rname }
    if not client_mgr:callback_by_id(session, cmd_id, callback_data, session_id) then
        log_info("[LoginServlet][on_random_name_req] callback failed! open_id: {}", session.open_id)
    end
end

quanta.login_servlet = LoginServlet()

return LoginServlet
