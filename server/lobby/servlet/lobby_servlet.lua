--lobby_servlet.lua

local log_err               = logger.err
local log_info              = logger.info
local log_debug             = logger.debug
local qfailed               = quanta.failed
local sformat               = string.format
local new_guid              = codec.guid_new

local client_mgr            = quanta.get("client_mgr")
local protobuf_mgr          = quanta.get("protobuf_mgr")
local router_mgr            = quanta.get("router_mgr")

local FRAME_SUCCESS         = protobuf_mgr:error_code("FRAME_SUCCESS")

local LobbyServlet = singleton()

function LobbyServlet:__init()
    -- cs协议监听
    protobuf_mgr:register(self, "NID_WORLD_CREATE_REQ", "on_world_create_req")
    protobuf_mgr:register(self, "NID_WORLD_LIST_REQ",   "on_world_list_req")
    protobuf_mgr:register(self, "NID_WORLD_LOGIN_REQ",  "on_world_login_req")
    protobuf_mgr:register(self, "NID_WORLD_DELETE_REQ", "on_world_delete_req")
    protobuf_mgr:register(self, "NID_WORLD_EDIT_REQ",   "on_world_edit_req")
end

--创建世界
function LobbyServlet:on_world_create_req(session, cmd_id, body, session_id)
    local account = session.account
    log_debug("[LobbyServlet][on_world_create_req] user_id({}) body({}) create world req!", account.user_id, body)
    local name, type, max_count, password, agent_url, setting, private = body.name, body.type, body.max_count,
            body.password, body.agent_url, body.setting, body.private or false
    local user_id = account.user_id
    --通知WorldManager创建世界
    local id = new_guid()
    local params = {
        id = id,
        type = type,
        name = name,
        max_count = max_count,
        password = password,
        agent_url = sformat("http://%s/request-world",agent_url),
        user_id = user_id,
        setting = setting,
        private = private,
    }

    local ok, code, world = router_mgr:call_worldm_hash(id, "rpc_world_create", user_id, params)
    if qfailed(code, ok) then
        log_err("[LobbyServlet][on_world_create_req] user_id({}) create world:{} failed ok:{} code:{}",
         account.user_id, name, ok, code)
        return client_mgr:callback_errcode(session, cmd_id, code, session_id)
    end

    --创建world成功后返回的数据
    local world_data = {
        error_code = FRAME_SUCCESS,
        id = world.id,
        type = world.type,
        name = world.name,
        token = world.token,
        setting = world.setting,
        invcode = world.invcode,
        user_id = world.user_id,
        mirror_addr = world.mirror_addr,
    }

    if not client_mgr:callback_by_id(session, cmd_id, world_data, session_id) then
        log_info("[LobbyServlet][on_world_create_req] user_id({}) create world:{} failed!", account.user_id, name)
        return
    end
    log_info("[LoginServlet][on_role_create_req] user_id({}) create role {} success!", account.user_id, name)
end

--世界列表
function LobbyServlet:on_world_list_req(session, cmd_id, body, session_id)
    local account = session.account
    log_debug("[LobbyServlet][on_world_list_req] user_id({}) body:{} list req!", account.user_id, body)
    local user_id = account.user_id
    local type, name, page, is_self = body.type, body.name, 1, body.is_self
    local ok, code, worlds = router_mgr:call_worldm_hash(0, "rpc_world_list", user_id, {type = type, name = name, page = page, is_self=is_self})
    if qfailed(code, ok) then
        log_err("[LobbyServlet][on_world_list_req] user_id({}) world_list failed!", user_id)
        return client_mgr:callback_errcode(session, cmd_id, code, session_id)
    end

    local world_list = {}
    for _, world in pairs(worlds) do
        world_list[world.id] = world
    end
    if not client_mgr:callback_by_id(session, cmd_id,{error_code = FRAME_SUCCESS, worlds = world_list}, session_id) then
        log_info("[LobbyServlet][on_world_list_req] callback failed! user_id: {}", account.user_id)
        return
    end
    log_info("[LobbyServlet][on_world_list_req] success! user_id: {}", account.user_id)
end

--登录世界
function LobbyServlet:on_world_login_req(session, cmd_id, body, session_id)
    local account = session.account
    log_debug("[LobbyServlet][on_world_login_req] user_id({}) body:{} list req!", account.user_id, body)
    local id, invcode, user_id = body.id, body.invcode, account.user_id
    -- 通过邀请码登录
    if (not id or id == 0) and (invcode and invcode ~= "") then
        local ok, code, world_id = router_mgr:call_center_master("rpc_invcode_query", invcode)
        if qfailed(code, ok) then
            log_err("[LobbyServlet][on_world_login_req] rpc_invcode_query is fail user_id:{} ok:{} code:{} world_id:{} invcode:{}",
                user_id,ok,code,world_id,invcode)
            return client_mgr:callback_errcode(session, cmd_id, code, session_id)
        end
        id = world_id
        log_debug("[LobbyServlet][on_world_login_req] user_id{} invcode:{} id:{}", user_id, invcode, id)
    end
    --通知worldmanager
    local ok, code, world = router_mgr:call_worldm_hash(id, "rpc_world_login", user_id, {id = id, invcode=invcode})
    if qfailed(code, ok) then
        log_err("[LobbyServlet][on_world_login_req] user_id({}) login failed!", user_id)
        return client_mgr:callback_errcode(session, cmd_id, code, session_id)
    end
    local res_data = {
        error_code = FRAME_SUCCESS,
        id = world.id,
        type = world.type,
        name = world.name,
        user_id = world.user_id,
        max_count = world.max_count,
        agent_url = world.agent_url,
        token = world.token,
        setting = world.setting,
        invcode = world.invcode,
        mirror_addr = world.mirror_addr
    }
    if not client_mgr:callback_by_id(session, cmd_id, res_data, session_id) then
        log_info("[LobbyServlet][on_world_login_req] user_id({}) callback failed!", account.user_id)
        return
    end
    log_info("[LobbyServlet][on_world_login_req] user_id({}) world({}) login success!", account.user_id, id)
end

--删除世界
function LobbyServlet:on_world_delete_req(session, cmd_id, body, session_id)
    local account = session.account
    log_debug("[LobbyServlet][on_world_delete_req] user_id({}) body:{} delete req!", account.user_id, body)
    local id, user_id = body.id, account.user_id

    --通知worldmanager
    local ok, code = router_mgr:call_worldm_hash(id, "rpc_world_delete", user_id, {id = id})
    if qfailed(code, ok) then
        log_err("[LobbyServlet][on_world_login_req] user_id({}) login failed!", user_id)
        return client_mgr:callback_errcode(session, cmd_id, code, session_id)
    end

    if not client_mgr:callback_by_id(session, cmd_id, {error_code = FRAME_SUCCESS, id = id}, session_id) then
        log_info("[LobbyServlet][on_world_delete_req] user_id({}) callback failed!", account.user_id)
        return
    end
    log_info("[LobbyServlet][on_world_delete_req] user_id({}) world({}) delete success!", account.user_id, id)
end

--编辑世界
function LobbyServlet:on_world_edit_req(session, cmd_id, body, session_id)
    local account = session.account
    log_debug("[LobbyServlet][on_world_edit_req] user_id({}) body:{} edit req!", account.user_id, body)
    local id, name, max_count, password, pw_valid, agent_url, setting = body.id, body.name, body.max_count,
            body.password, body.pw_valid, body.agent_url, body.setting
    local user_id = account.user_id
    --通知worldmanager
    local message = {
        id = id,
        name = name,
        max_count = max_count,
        password = password,
        agent_url = agent_url,
        pw_valid = pw_valid,
        setting = setting
    }
    local ok, code, data = router_mgr:call_worldm_hash(id, "rpc_world_edit", user_id, message)
    if qfailed(code, ok) then
        log_err("[LobbyServlet][on_world_edit_req] user_id({}) login failed!", user_id)
        return client_mgr:callback_errcode(session, cmd_id, code, session_id)
    end

    --创建world成功后返回的数据
    local world_data = {
        id = data.id,
        type = data.type,
        name = data.name,
        password = data.password,
        setting = data.setting
    }
    if not client_mgr:callback_by_id(session, cmd_id, {error_code = FRAME_SUCCESS, info = world_data}, session_id) then
        log_info("[LobbyServlet][on_world_edit_req] user_id({}) callback failed!", account.user_id)
        return
    end
    log_info("[LobbyServlet][on_world_edit_req] user_id({}) world({}) edit success!", account.user_id, id)
end

quanta.lobby_servlet = LobbyServlet()

return LobbyServlet
