--quanta
import("kernel.lua")

local env_addr  = environ.addr
quanta.startup(function()
    --创建客户端网络管理
    local NetServer = import("network/net_server.lua")
    local client_mgr = NetServer("lobby")
    client_mgr:setup(env_addr("QUANTA_LOGIN_ADDR"))
    quanta.client_mgr = client_mgr
    --初始化world
    import("lobby/init.lua")
end)
