--bgip_servlet.lua
local log_info      = logger.info
local sformat       = string.format
local json_encode   = json.encode
local qfailed       = quanta.failed

local cache_mgr     = quanta.get("cache_mgr")
local event_mgr     = quanta.get("event_mgr")
local protobuf_mgr  = quanta.get("protobuf_mgr")
local config_mgr    = quanta.get("config_mgr")

local cache_db      = config_mgr:init_table("cache", "sheet")
local FRAME_FAILED  = protobuf_mgr:error_code("FRAME_FAILED")
local FRAME_SUCCESS = protobuf_mgr:error_code("FRAME_SUCCESS")

-- bgip服务
local BgipCache = singleton()
function BgipCache:__init()
    self:setup()
end

function BgipCache:setup()
    -- rpc消息
    event_mgr:add_listener(self, "rpc_bgip_querycache")
    event_mgr:add_listener(self, "rpc_bgip_delcache")
    event_mgr:add_listener(self, "rpc_bgip_updatecache")
    event_mgr:add_listener(self, "rpc_bgip_copycache")
    event_mgr:add_listener(self, "rpc_bgip_onekeycopy")
    event_mgr:add_listener(self, "rpc_bgip_signed")
end

--注销角色
function BgipCache:rpc_bgip_signed(role_id)
    log_info("[BgipCache][rpc_bgip_signed] role_id={} ", role_id)
    --查询open_id
    local p_code, player_doc = cache_mgr:load_document("player", role_id)
    if qfailed(p_code) then
        log_info("[BgipCache][rpc_bgip_signed] load_document failed! role_id={}", role_id)
        return FRAME_FAILED, {msg = "not found role_id"}
    end

    local open_id = player_doc:get_wholes().open_id
    if not open_id then
        return  FRAME_FAILED, {msg = "not found open_id"}
    end
    log_info("[BgipCache][rpc_bgip_signed] unroll role_id:{} open_id:{}", role_id, open_id)
    local code, doc = cache_mgr:load_document("account", open_id)
    if qfailed(code) then
        log_info("[BgipCache][rpc_bgip_signed] load_document failed! open_id={}", open_id)
        return FRAME_FAILED, {msg = "failed"}
    end
    local wholes = doc:get_wholes()
    wholes.roles = {}
    doc:update_wholes(wholes)
    return FRAME_SUCCESS, {msg = "success"}
end

--复制玩家单表数据
function BgipCache:rpc_bgip_copycache( dst_role_id, src_role_id, coll_name)
    log_info("[BgipCache][rpc_bgip_copycache] src_role_id={} dst_role_id={} coll_name={}", src_role_id, dst_role_id, coll_name)
    local coll_cfg = cache_db:find_one(coll_name)
    if not coll_cfg then
        return  FRAME_FAILED, {msg = sformat("%s not found cache", coll_name)}
    end
    if not coll_cfg.copyable then
        return  FRAME_FAILED, {msg = sformat("%s cant copy", coll_name)}
    end

    local ok = cache_mgr:rpc_cache_copy(dst_role_id, src_role_id, coll_name)
    if not ok then
        return FRAME_FAILED, {msg = "failed"}
    end
    return FRAME_SUCCESS, {msg = "success"}
end

--更新玩家单表数据
function BgipCache:rpc_bgip_updatecache(role_id, coll_name, data)
    log_info("[BgipCache][rpc_bgip_updatecache] role_id={} coll_name={}, data:{}", role_id, coll_name, data)
    local coll_cfg = cache_db:find_one(coll_name)
    if not coll_cfg then
        return  FRAME_FAILED, {msg = sformat("%s not found cache", coll_name)}
    end
    local ok = cache_mgr:rpc_cache_flush(role_id, coll_name, data)
    if not ok then
        return FRAME_FAILED, {msg = "failed"}
    end
    return FRAME_SUCCESS, {msg = "success"}
end

--复制玩家数据
function BgipCache:rpc_bgip_onekeycopy(dst_role_id, src_role_id)
    log_info("[BgipCache][rpc_bgip_copycache] src_role_id={} dst_role_id={} ", src_role_id, dst_role_id)
    for _, conf in cache_db:iterator() do
        if conf.copyable then
            self:rpc_bgip_copycache(dst_role_id, src_role_id, conf.sheet)
        end
    end
    return FRAME_SUCCESS, {msg = "success"}
end

--删除缓存
function BgipCache:rpc_bgip_delcache(primary_id, coll_name)
    log_info("[BgipCache][rpc_bgip_delcache] primary_id={} coll_name={}", primary_id, coll_name)
    local ok = cache_mgr:rpc_cache_delete(primary_id, coll_name)
    if not ok then
        return FRAME_FAILED, {msg = sformat("failed code:%s", ok)}
    end
    return FRAME_SUCCESS, {msg = "success"}
end

--查询缓存
function BgipCache:rpc_bgip_querycache(primary_id, coll_name)
    log_info("[BgipCache][rpc_bgip_querycache] primary_id={} coll_name={}", primary_id, coll_name)
    local ok, doc = cache_mgr:load_document(coll_name, primary_id)
    if not ok or not doc then
        return FRAME_FAILED, {msg = "cache not find"}
    end
    return FRAME_SUCCESS, {datas = json_encode(doc:get_wholes())}
end

quanta.bgip_cache = BgipCache()

return BgipCache