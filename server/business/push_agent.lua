--push_agent.lua
local log_debug         = logger.debug
local log_err           = logger.err
local log_warn          = logger.warn
local qfailed           = quanta.failed

local router_mgr        = quanta.get("router_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")
local config_mgr        = quanta.get("config_mgr")
local event_mgr         = quanta.get("event_mgr")

local SYSTEM            = quanta.enum("PushNotifKind", "SYSTEM")
local PNT_DAY           = protobuf_mgr:enum("push_notif_type", "PNT_DAY")

local pushnotif_db      = config_mgr:init_table("push_notif", "id")
local PUSH_CHECK        = environ.status("QUANTA_PUSH")
local FRAME_SUCCESS     = protobuf_mgr:error_code("FRAME_SUCCESS")

local PushAgent = singleton()

function PushAgent:__init()
    event_mgr:add_trigger(self, "on_login_success")
end

--按角色推送
--local push_agent = quanta.get("push_agent")
--测试推送 push_agent:on_push_role(player_id, 6868559925258261850, {player_id,player_id+1,player_id+2}, {}, {})
function PushAgent:on_push_role(hash_key, template_id, ids, push_arg, push_msg)
    -- 是否开启推送
    if not PUSH_CHECK then
        return FRAME_SUCCESS
    end
    --rpc到bilog
    local rpc_ok, code, res_text = router_mgr:send_bilog_hash(hash_key, "rpc_push_role_id",  ids, template_id,push_arg, push_msg)
    if qfailed(code, rpc_ok) then
        log_err("[PushAgent][on_push_role] rpc_push_role_id rpc_ok:{}, code:{}, res_text:{}", rpc_ok, code, res_text)
        return code
    end
    return FRAME_SUCCESS, res_text
end

--按sdk_open_id推送
function PushAgent:on_push_sdk(hash_key, template_id, ids, push_arg, push_msg)
    -- 是否开启推送
    if not PUSH_CHECK then
        return FRAME_SUCCESS
    end
    --rpc到bilog
    local rpc_ok, code, res_text = router_mgr:send_bilog_hash(hash_key, "rpc_push_sdk_id", ids, template_id, push_arg, push_msg)
    if qfailed(code, rpc_ok) then
        log_err("[PushAgent][rpc_push_sdk_id] rpc_push_sdk_id rpc_ok:{}, code:{}, res_text:{}", rpc_ok, code, res_text)
        return code
    end
    return FRAME_SUCCESS, res_text
end

-- 推送到客户端
function PushAgent:push_to_client(player, id, push_time)
    local config = pushnotif_db:find_one(id)
    if config == nil then
        log_err("[PushAgent][push_to_client] config error:{}", id)
        return
    end
    local message = {
        id = id,
        push_time = push_time
    }
    if config.client then
        player:send("NID_PUSH_MESSAGE_NTF", message)
        log_debug("[PushAgent][push_to_client]player:{} id:{} push_time:{}", player:get_id(), id, push_time)
        return true
    end
end

-- 获取推送时间
function PushAgent:get_push_time(conf, offset_time)
    log_debug("[PushAgent][get_push_time] conf:{} offset_time:{}", conf, offset_time)
    local year,month,day,hour,min,sec = conf.push_time:match("(%d+)/(%d+)/(%d+) (%d+):(%d+):(%d+)")
    if not year then
        local date = os.date("*t")
        year = date.year
        month = date.month
        day = date.day
        hour, min, sec = conf.push_time:match("(%d+):(%d+):(%d+)")
    end
    local timestamp = os.time({
        year = year,
        month = month,
        day = day,
        hour = hour,
        min = min,
        sec = sec
    })
    timestamp = timestamp + (offset_time or 0)
    if conf.type == PNT_DAY and timestamp < quanta.now then
        -- 过期放到第二天推送
        timestamp = timestamp + 3600 * 24
    end
    return timestamp
end

-- 系统推送
function PushAgent:on_system_notif(player)
    local now = quanta.now
    for id, conf in pushnotif_db:iterator() do
        if not conf.client then
            goto continue
        end

        if conf.kind ~= SYSTEM then
            goto continue
        end

        if now < conf.start_time or now >= conf.end_time then
            goto continue
        end

        local push_time = self:get_push_time(conf)
        if push_time <= now then
            goto continue
        end

        self:push_to_client(player, id, push_time)
        :: continue ::
    end
end

-- 推送通知
function PushAgent:push_notif(player, id, offset_time)
    local conf = pushnotif_db:find_one(id)
    if not conf then
        log_warn("[PushAgent][push_notif] config is nil player:{} id:{}", player:get_id(), id)
        return
    end
    local now = quanta.now
    local push_time = self:get_push_time(conf, offset_time)
    if push_time < now then
        log_warn("[PushAgent][push_notif] push_time < quanta.now player:{} id:{} {},{}", player:get_id(), id, push_time, now)
        return
    end
    self:push_to_client(player, id, push_time)
end

-- 登录成功
function PushAgent:on_login_success(player_id, player)
    -- 系统通知
    self:on_system_notif(player)
end

quanta.push_agent = PushAgent()

return PushAgent
