--safe_mgr.lua

import("feature/worker.lua")
import("network/http_client.lua")

local sformat          = string.format
local tinsert          = table.insert
local guid_new         = codec.guid_new
local ltime            = timer.time
local log_debug        = logger.debug
local log_fatal        = logger.fatal
local json_encode      = json.encode
local json_decode      = json.decode
local qfailed          = quanta.failed

local event_mgr        = quanta.get("event_mgr")
local http_client      = quanta.get("http_client")
local config_mgr       = quanta.get("config_mgr")
local protobuf_mgr     = quanta.get("protobuf_mgr")

local errcode_db       = config_mgr:init_table("sdk_errcode", "channel", "sdk_errcode")
local PLAT_BYTEDANCE   = protobuf_mgr:enum("platform_type", "PLATFORM_BYTEDANCE")

local FRAME_SUCCESS    = protobuf_mgr:error_code("FRAME_SUCCESS")
local FRAME_FAILED     = protobuf_mgr:error_code("FRAME_FAILED")

local REGION           = environ.number("QUANTA_REGION")
local APP_ID           = environ.number("QUANTA_APP_ID")
local SDK_SANDBOX      = environ.status("QUANTA_SDK_SANDBOX")
local SAFE_URL         = sformat("%s/review/submit", environ.get("QUANTA_SAFE_URL"))
local REPORT_URL       = sformat("%s/content_review", environ.get("QUANTA_REPORT_URL"))
local SANDBOX_SAFE_URL = sformat("%s/review/submit", environ.get("QUANTA_SANDBOX_SAFE_URL"))

local SafeMgr = singleton()
function SafeMgr:__init()
    event_mgr:add_listener(self, "bytedance_safe_check")

    log_debug("[SafeMgr][init] SAFE_URL:{}", SAFE_URL)
    log_debug("[SafeMgr][init] REPORT_URL:{}", REPORT_URL)
    log_debug("[SafeMgr][init] SANDBOX_SAFE_URL:{}", SANDBOX_SAFE_URL)
end

--构建安全验证参数
function SafeMgr:build_safe_params(ip, language, texts, dev_plat, msg_type, extension)
    local params = {
        sendTime = quanta.now,
        appId = APP_ID,
        serverId = REGION,
        msgType = msg_type, --与字节约定的msgType
        msgId = sformat("%s", guid_new()),
        senderOpenId = extension.send_open_id and tostring(extension.send_open_id) or "0",
        senderRoleId = extension.send_role_id and tostring(extension.send_role_id) or "0",
        recvOpenId = extension.recv_open_id and tostring(extension.recv_open_id) or "0",
        recvRoleId = extension.recv_role_id and tostring(extension.recv_role_id) or "0",
        regionISO = ip,
        language = language,
        contents = {},
        extra = {
            ip = ip,
            contentCombineType = 1,
            devicePlatform = dev_plat
        }
    }

    if type(texts) == "string" then
        local content = {
            content = texts,
            contentId = sformat("%s", guid_new()),
            contentType = 1
        }
        tinsert(params.contents, content)
    elseif type(texts) == "table" then
        for _,text in pairs(texts) do
            local content = {
                content = text,
                contentId = sformat("%s", guid_new()),
                contentType = 1
            }
            tinsert(params.contents, content)
        end
    end

    params = json_encode(params)
    log_debug("[SafeMgr][build_safe_params] params:{}", params)
    return params
end

-- 获取错误码
function SafeMgr:get_error_code(res)
    local code = FRAME_SUCCESS
    local result = json_decode(res)
    local res_code = result.code
    -- 三方错误码处
    if qfailed(res_code) then
        local game_errcode = errcode_db:find_integer("game_errcode", PLAT_BYTEDANCE, res_code)
        code = game_errcode or res_code
        return result, code
    end

    --内容检查code在协议中
    local check_code = result.data.decision.errCode
    if qfailed(check_code) then
        local game_errcode = errcode_db:find_integer("game_errcode", PLAT_BYTEDANCE, check_code)
        code = game_errcode or check_code
        return result, code
    end
    return result, code
end

--内容安全检测
function SafeMgr:bytedance_safe_check(ip, language, text, dev_plat, msg_type, extension)
    log_debug("[SafeMgr][bytedance_safe_check] ip:{}, language:{}, text:{}, dev_plat:{}", ip, language, text, dev_plat)

    local post_url = SDK_SANDBOX and SANDBOX_SAFE_URL or SAFE_URL
    local params = self:build_safe_params(ip, language, text, dev_plat, msg_type, extension and extension or {})

    -- 构造http请求
    local headers = {
        ["Content-type"] = "application/json",
    }

    local send_ms = ltime()
    local ok, status, res = http_client:call_post(post_url, params, headers)
    local escape_ms = ltime() - send_ms
    log_debug("[SafeMgr][bytedance_safe_check] ok:{}, status:{}, escape_ms={}, post_url:{} res:{} ", ok, status, escape_ms, post_url, res)
    self:report_safe(escape_ms, status, res)
    if not ok then
        log_fatal("[SafeMgr][bytedance_safe_check] is fail ok:{}, status:{}, escape_ms={}, res:{}", ok, status, escape_ms, res)
        return ok and status or 404, nil, FRAME_FAILED
    end

    -- 获取错误码
    local result, code = self:get_error_code(res)
    return ok and status or 404, result, code
end

-- 上报接口健康度
function SafeMgr:report_safe(escape_ms, status, res)
    local code
    if status ~= 200 then
        code = -999
    else
        local res_data = json_decode(res)
        code = res_data.code
    end
    local params = {
        app_id     = APP_ID,
        server_id  = REGION,
        code       = code,
        latency_ms = escape_ms
    }

    log_debug("[SafeMgr][report_safe] params:{}", params)
    http_client:call_post(REPORT_URL, params)
end

quanta.safe_mgr = SafeMgr()
