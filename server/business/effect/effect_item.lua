--effect_item.lua
local log_err           = logger.err
local mtointeger        = math.tointeger

local protobuf_mgr      = quanta.get("protobuf_mgr")

local ITEM_IS_FULL      = protobuf_mgr:error_code("PACKET_ITEM_IS_FULL")
local EFFECT_CONF_ERR   = protobuf_mgr:error_code("PACKET_EFFECT_CONF_ERR")

local EffectItem = class()

function EffectItem:execute(player, args, count, target)
    local item_id = mtointeger(args[1])
    local item_num = mtointeger(args[2]) or 1
    if not item_id then
        log_err("[EffectItem][execute] effect args ({}) err!", args)
        return false, EFFECT_CONF_ERR
    end
    local drop_items = {[item_id] = item_num * count}
    if not player:allot_items(drop_items) then
        return false, ITEM_IS_FULL
    end
    return true
end

function EffectItem:revert(player, args, count, target)
    return true
end

return EffectItem
