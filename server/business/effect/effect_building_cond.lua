--effect_building_cond.lua
--在A类建筑上工作时
local log_debug         = logger.debug
local mtointeger        = math.tointeger

local EffectBuildingCond = class()

function EffectBuildingCond:execute(player, args, count, building)
    log_debug("[EffectBuildingCond][execute]....")
    local product_type = mtointeger(args[1])
    local effect_id = mtointeger(args[2])
    local building_proto = building:get_prototype()
    --是否触发效果
    if product_type ~= building_proto.product_type then
        return false
    end

    building:add_effect(effect_id)
    return true
end

function EffectBuildingCond:revert(player, args, count, building)
    local product_type = mtointeger(args[1])
    local effect_id = mtointeger(args[2])
    local building_proto = building:get_prototype()
    --是否触发效果
    if product_type ~= building_proto.product_type then
        return false
    end

    building:remove_effect(effect_id)
    return true
end

return EffectBuildingCond
