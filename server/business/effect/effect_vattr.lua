--effect_vattr.lua
local log_err           = logger.err
local qmax              = qmath.max
local mtointeger        = math.tointeger

local protobuf_mgr      = quanta.get("protobuf_mgr")
local event_mgr         = quanta.get("event_mgr")

local EFFECT_CONF_ERR   = protobuf_mgr:error_code("PACKET_EFFECT_CONF_ERR")
local OBTAIN_ITEM       = protobuf_mgr:enum("obtain_reason", "NID_OBTAIN_ITEM")

local EffectVAttr = class()

function EffectVAttr:execute(player, args, count, target)
    local attr_id = mtointeger(args[1])
    local addv = mtointeger(args[2])
    if not attr_id or not addv then
        log_err("[EffectVAttr][execute] effect args ({}) err!", args)
        return false, EFFECT_CONF_ERR
    end
    local total_change = 0
    for i = 1, count do
        local cur = target:get_attr(attr_id)
        target:set_attr(attr_id, qmax(cur + addv, 0))
        total_change = total_change + addv
    end
    --属性改变事件
    if total_change > 0 then
        event_mgr:notify_trigger("on_attritem_add", target, 0, attr_id, total_change, OBTAIN_ITEM)
    elseif total_change < 0 then
        event_mgr:notify_trigger("on_attritem_cost", target, 0, attr_id, -total_change, OBTAIN_ITEM)
    end
    return true
end

function EffectVAttr:revert(player, args, count, target)
    local attr_id = mtointeger(args[1])
    local addv = mtointeger(args[2])
    if not attr_id or not addv then
        log_err("[EffectVAttr][revert] effect args ({}) err!", args)
        return false, EFFECT_CONF_ERR
    end
    for i = 1, count do
        local cur = target:get_attr(attr_id)
        target:set_attr(attr_id, qmax(cur - addv, 0))
    end
    return true
end

return EffectVAttr
