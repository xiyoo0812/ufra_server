--effect_building_attr.lua
--建筑属性
local log_debug         = logger.debug
local mtointeger        = math.tointeger
local unserialize       = luakit.unserialize

local EffectBuildingAttr = class()

function EffectBuildingAttr:execute(player, args, count, building, effect_id)
    log_debug("[EffectBuildingAttr][execute]....building_id:{} effect_id:{}", building:get_id(), effect_id)
    --命中概率
    local hit_per = mtointeger(args[1])
    --值(产出物提升百分比/生产抵扣百分比/产能提高点数/进度完成百分比)
    local value = mtointeger(args[2])
    --属性值
    local attr = args[3]
    --额外道具(抵扣为减免,产出为额外获得)
    local items = unserialize(args[4] or "{}")
    --属性类型
    if attr == 'cost_per' then
        local t_effect = {
            hit_per = hit_per or 100,
            percent = value or 0,
            items = items or {},
            num = 1
        }
        building:add_cost_per(effect_id, t_effect)
    elseif attr == 'prod_per' then
        local t_effect = {
            hit_per = hit_per or 100,
            percent = value or 0,
            items = items or {},
            num = 1
        }
        building:add_prod_per(effect_id, t_effect)
    elseif attr == 'prog_per' then
        building:add_prog_per(value)
    elseif attr == 'capacity_num' then
        building:add_capacity_num(value)
    end

    return true
end

function EffectBuildingAttr:revert(player, args, count, building, effect_id)
    log_debug("[EffectBuildingAttr][revert]....building_id:{} effect_id:{}", building:get_id(), effect_id)
    --值(产出物提升百分比/生产抵扣百分比/产能提高点数/进度完成百分比)
    local value = mtointeger(args[2])
    local attr = args[3]
    --属性类型
    if attr == 'cost_per' then
        building:reduce_cost_per(effect_id)
    elseif attr == 'prod_per' then
        building:reduce_prod_per(effect_id)
    elseif attr == 'prog_per' then
        building:add_prog_per(-value)
    elseif attr == 'capacity_num' then
        building:add_capacity_num(-value)
    end
    return true
end


return EffectBuildingAttr
