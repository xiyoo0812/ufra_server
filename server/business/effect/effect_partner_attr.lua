--effect_partner_attr.lua
--npc属性
local log_debug         = logger.debug
local mtointeger        = math.tointeger

local EffectPartnerAttr = class()

function EffectPartnerAttr:execute(partner, args, count)
    log_debug("[EffectPartnerAttr][execute]....")
    local value = mtointeger(args[2])
    local attr = args[3]
    --属性类型
    if attr == 'nfskill_per' then
        partner:add_nfskill_per(value)
    elseif attr == 'affinity_per' then
        partner:add_affinity_per(value)
    end

    return true
end

function EffectPartnerAttr:revert(partner, args, count)
    log_debug("[EffectPartnerAttr][revert]....")
    local value = mtointeger(args[2])
    local attr = args[3]
    --属性类型
    if attr == 'nfskill_per' then
        partner:add_nfskill_per(-value)
    elseif attr == 'affinity_per' then
        partner:add_affinity_per(-value)
    end
    return true
end

return EffectPartnerAttr
