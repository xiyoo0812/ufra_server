--effect_box.lua
import("business/item/dropor.lua")

local log_err           = logger.err
local mtointeger        = math.tointeger

local dropor            = quanta.get("dropor")
local protobuf_mgr      = quanta.get("protobuf_mgr")

local ITEM_IS_FULL      = protobuf_mgr:error_code("PACKET_ITEM_IS_FULL")
local ITEM_DROP_ERR     = protobuf_mgr:error_code("PACKET_ITEM_DROP_ERR")
local EFFECT_CONF_ERR   = protobuf_mgr:error_code("PACKET_EFFECT_CONF_ERR")

local EffectBox = class()

function EffectBox:execute(player, args, count, target)
    local drop_id = mtointeger(args[1])
    if not drop_id then
        log_err("[EffectBox][execute] effect args ({}) err!", args)
        return false, EFFECT_CONF_ERR
    end
    local drop_items = {}
    for i = 1, count do
        if not dropor:execute(drop_id, drop_items) then
            log_err("[EffectBox][execute] drop_id ({}) failed!", drop_id)
            return false, ITEM_DROP_ERR
        end
    end
    if not player:allot_items(drop_items) then
        return false, ITEM_IS_FULL
    end
    return true
end

function EffectBox:revert(player, args, count, target)
    return true
end

return EffectBox
