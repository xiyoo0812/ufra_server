--effect_affinity.lua
--local log_err           = logger.err
--local qfailed           = quanta.failed

local EffectAffinity = class()

function EffectAffinity:execute(player, args, count, target)
    -- local npc_id = target
    -- local num = args[2]
    -- local player_id = player:get_id()
    -- local ok, code = player:call_service("scene", "rpc_affinity_change", npc_id, num)
    -- local rpc_failed, rpc_code = qfailed(code, ok)
    -- if rpc_failed then
    --     log_err("[EffectAffinity][execute] active taffinity_change failed, player: {}", player_id)
    --     return false, rpc_code
    -- end
    return true
end

function EffectAffinity:revert(player, args, count, target)
    return true
end

return EffectAffinity
