--effect_rattr.lua
local log_err           = logger.err
local qclamp            = qmath.clamp
local mtointeger        = math.tointeger

local protobuf_mgr      = quanta.get("protobuf_mgr")

local EFFECT_CONF_ERR   = protobuf_mgr:error_code("PACKET_EFFECT_CONF_ERR")

local EffectRAttr = class()

function EffectRAttr:execute(player, args, count, target)
    local attr_id = mtointeger(args[1])
    local max_attr_id = mtointeger(args[2])
    local rate = mtointeger(args[3])
    if not attr_id or not max_attr_id or not rate then
        log_err("[EffectRAttr][execute] effect args ({}) err!", args)
        return false, EFFECT_CONF_ERR
    end
    for i = 1, count do
        local cur = target:get_attr(attr_id)
        local max = target:get_attr(max_attr_id)
        local modi = qclamp(cur + (max * rate) // 100, 0, max)
        target:set_attr(attr_id, modi)
    end
    return true
end

function EffectRAttr:revert(player, args, count, target)
    return true
end

return EffectRAttr
