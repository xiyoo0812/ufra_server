--effect_buff.lua
local log_err           = logger.err
local mtointeger        = math.tointeger

local protobuf_mgr      = quanta.get("protobuf_mgr")

local EFFECT_CONF_ERR   = protobuf_mgr:error_code("PACKET_EFFECT_CONF_ERR")

local EffectBuff = class()

function EffectBuff:execute(player, args, count, target)
    local buff_id = mtointeger(args[1])
    if not buff_id then
        log_err("[EffectBox][execute] effect args ({}) err!", args)
        return false, EFFECT_CONF_ERR
    end
    return target:add_buff(buff_id)
end

function EffectBuff:revert(player, args, count, target)
    return true
end

return EffectBuff
