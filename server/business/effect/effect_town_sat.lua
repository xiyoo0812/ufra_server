--effect_town_sat.lua
local log_err           = logger.err
local qfailed           = quanta.failed

local EffectSat = class()

function EffectSat:execute(player, args, count, target)
    local num = args[2]
    local player_id = player:get_id()
    local ok, code = player:call_service("scene", "rpc_sat_change", num)
    local rpc_failed, rpc_code = qfailed(code, ok)
    if rpc_failed then
        log_err("[EffectSat][execute] active sat_change failed, player: {}", player_id)
        return false, rpc_code
    end
    return true
end

function EffectSat:revert(player, args, count, target)
    return true
end

return EffectSat
