--effect_mgr.lua
local log_warn          = logger.warn
local config_mgr        = quanta.get("config_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")
local effect_db         = config_mgr:init_table("effect", "id")

local EffectBox         = import("business/effect/effect_box.lua")
local EffectBuff        = import("business/effect/effect_buff.lua")
local EffectItem        = import("business/effect/effect_item.lua")
local EffectRAttr       = import("business/effect/effect_rattr.lua")
local EffectVAttr       = import("business/effect/effect_vattr.lua")
local EffectDrawing     = import("business/effect/effect_drawing.lua")
local EffectTownExp     = import("business/effect/effect_town_exp.lua")
local EffectNpc         = import("business/effect/effect_npc.lua")
local EffectAffinity    = import("business/effect/effect_affinity.lua")
local EffectSat         = import("business/effect/effect_town_sat.lua")

local EffectBuildingCond    = import("business/effect/effect_building_cond.lua")
local EffectBuildGroupCond  = import("business/effect/effect_build_group_cond.lua")
local EffectBuildingAttr    = import("business/effect/effect_building_attr.lua")
local EffectPartnerAttr     = import("business/effect/effect_partner_attr.lua")

local EffectType        = enum("EffectType")

local FRAME_SUCCESS     = protobuf_mgr:error_code("FRAME_SUCCESS")
local EFFECT_CONF_ERR   = protobuf_mgr:error_code("PACKET_EFFECT_CONF_ERR")

local EffectMgr = singleton()
local prop = property(EffectMgr)
prop:reader("effect_doers", {})

function EffectMgr:__init()
    self.effect_doers[EffectType.BOX]           = EffectBox()
    self.effect_doers[EffectType.BUFF]          = EffectBuff()
    self.effect_doers[EffectType.ITEM]          = EffectItem()
    self.effect_doers[EffectType.VATTR]         = EffectVAttr()
    self.effect_doers[EffectType.RATTR]         = EffectRAttr()
    self.effect_doers[EffectType.DRAWING]       = EffectDrawing()
    self.effect_doers[EffectType.TOWN_EXP]      = EffectTownExp()
    self.effect_doers[EffectType.NPC]           = EffectNpc()
    self.effect_doers[EffectType.AFFINITY]      = EffectAffinity()
    self.effect_doers[EffectType.SAT]           = EffectSat()

    self.effect_doers[EffectType.BUILDING_COND] = EffectBuildingCond()
    self.effect_doers[EffectType.BUILD_GROUP_COND] = EffectBuildGroupCond()
    self.effect_doers[EffectType.BUILDING_ATTR] = EffectBuildingAttr()
    self.effect_doers[EffectType.PARTNER_ATTR] = EffectPartnerAttr()
end

function EffectMgr:execute(player, effect_id, count, target)
    local effect = effect_db:find_one(effect_id)
    if not effect then
        log_warn("[EffectMgr][execute] effect({}) not found", effect_id)
        return EFFECT_CONF_ERR
    end
    local doer = self.effect_doers[effect.sub_type]
    if not doer then
        log_warn("[EffectMgr][execute] effect.sub_type({}) not found", effect.sub_type)
        return EFFECT_CONF_ERR
    end
    local ok, code = doer:execute(player, effect.args, count, target, effect_id)
    if not ok then
        return code
    end
    return FRAME_SUCCESS
end

function EffectMgr:execute_doer(player, effect_type, target, ...)
    local doer = self.effect_doers[effect_type]
    if not doer then
        log_warn("[EffectMgr][execute] effect_type({}) not found", effect_type)
        return EFFECT_CONF_ERR
    end
    local ok, code = doer:execute(player, {...}, 1, target)
    if not ok then
        return code
    end
    return FRAME_SUCCESS
end

function EffectMgr:revert(player, effect_id, count, target)
    local effect = effect_db:find_one(effect_id)
    if not effect then
        return EFFECT_CONF_ERR
    end
    local doer = self.effect_doers[effect.sub_type]
    if not doer then
        return EFFECT_CONF_ERR
    end
    local ok, code = doer:revert(player, effect.args, count, target, effect_id)
    if not ok then
        return code
    end
    return FRAME_SUCCESS
end

-- export
quanta.effect_mgr = EffectMgr()

return EffectMgr
