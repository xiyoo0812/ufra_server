--effect_npc.lua
local log_err           = logger.err
local qfailed           = quanta.failed

local config_mgr        = quanta.get("config_mgr")

local utility_db        = config_mgr:get_table("utility")
local NPC_START_ID      = utility_db:find_number("value", "npc_start_id")

local EffectNpc = class()

function EffectNpc:execute(player, args, count, target)
    local npc_id = args[1] % NPC_START_ID
    local reason = args[3]
    local player_id = player:get_id()
    local ok, code = player:call_service("scene", "rpc_obtain_npc", npc_id, reason)
    local rpc_failed, rpc_code = qfailed(code, ok)
    if rpc_failed then
        log_err("[EffectNpc][execute] obtain npc failed, player: {} npc_id:{}", player_id, npc_id)
        return false, rpc_code
    end
    return true
end

function EffectNpc:revert(player, args, count, target, target_args)
    return true
end

return EffectNpc
