--effect_build_id_cond.lua
--在A建筑上工作时
local log_debug         = logger.debug
local mtointeger        = math.tointeger

local EffectBuildGroupCond = class()

function EffectBuildGroupCond:execute(player, args, count, building)
    log_debug("[EffectBuildGroupCond][execute] build_id:{}.... effect_id:{}", building:get_id(), args[2])
    local group = mtointeger(args[1])
    local effect_id = mtointeger(args[2])
    local building_proto = building:get_prototype()
    --是否触发效果
    if group ~= building_proto.group then
        return false
    end

    building:add_effect(effect_id)
    return true
end

function EffectBuildGroupCond:revert(player, args, count, building)
    local group = mtointeger(args[1])
    local effect_id = mtointeger(args[2])
    local building_proto = building:get_prototype()
    --是否触发效果
    if group ~= building_proto.group then
        return false
    end

    building:remove_effect(effect_id)
    return true
end

return EffectBuildGroupCond
