--effect_drawing.lua
local log_err           = logger.err
local qfailed           = quanta.failed
local mtointeger        = math.tointeger

local protobuf_mgr      = quanta.get("protobuf_mgr")

local EFFECT_CONF_ERR   = protobuf_mgr:error_code("PACKET_EFFECT_CONF_ERR")

local EffectDrawing = class()

local DRAWING_LIST = {
    formula = "rpc_active_formula",
    drawing = "rpc_active_drawing",
    blueprint = "rpc_active_blueprint",
}

function EffectDrawing:execute(player, args, count, target)
    local drawing_type = args[1]
    local drawing_id = mtointeger(args[2])
    if not drawing_type or not drawing_id  then
        log_err("[EffectDrawing][execute] effect args ({}) err!", args)
        return false, EFFECT_CONF_ERR
    end
    local rpc_func_name = DRAWING_LIST[drawing_type]
    if not rpc_func_name then
        log_err("[EffectDrawing][execute] drawing_type args ({}) err!", args)
        return false, EFFECT_CONF_ERR
    end
    local player_id = player:get_id()
    local ok, code = player:call_service("scene", rpc_func_name, drawing_id)
    local rpc_failed, rpc_code = qfailed(code, ok)
    if rpc_failed then
        log_err("[EffectDrawing][execute] active drawing failed, player: {}", player_id)
        return false, rpc_code
    end
    return true
end

function EffectDrawing:revert(player, args, count, target)
    return true
end

return EffectDrawing
