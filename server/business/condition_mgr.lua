--condition_mgr.lua
local tunpack       = table.unpack
local qfailed       = quanta.failed

local event_mgr     = quanta.get("event_mgr")
local player_mgr    = quanta.get("player_mgr")

local Condition     = enum("Condition")

local SUCCESS       = quanta.enum("KernCode", "SUCCESS")
local FAILED        = quanta.enum("KernCode", "FAILED")

local ConditionMgr = singleton()
local prop = property(ConditionMgr)
prop:reader("cond_checkers", {})

function ConditionMgr:__init()
    --注册条件参数
    self.cond_checkers[Condition.NPC]       = { "on_check_own_npc", "scene" }
    self.cond_checkers[Condition.TOWN]      = { "on_check_town_level", "local" }
    self.cond_checkers[Condition.TASK]      = { "on_check_finish_task", "lobby" }
    self.cond_checkers[Condition.LEVEL]     = { "on_check_player_level", "local" }
    self.cond_checkers[Condition.BUILDING]  = { "on_check_own_building", "scene" }
    self.cond_checkers[Condition.ASSIGN]    = { "on_check_assign_npc", "scene" }
    --注册条件检查
    for _, check_info in pairs(self.cond_checkers) do
        event_mgr:add_listener(self, check_info[1])
    end
end

function ConditionMgr:check_condition(player, conditions)
    for condition_id, args in pairs(conditions or {}) do
        local check_info = self.cond_checkers[condition_id]
        if check_info then
            local player_id = player:get_id()
            local rpc, service = tunpack(check_info)
            if service == "local" or quanta.service_name == service then
                local result = event_mgr:notify_listener(rpc, player_id, args, player)
                if qfailed(result[2], result[1]) then
                    return false
                end
                goto continue
            end
            local ok, code = player:call_service(service, rpc, args)
            if qfailed(code, ok) then
                return false
            end
        end
        :: continue ::
    end
    return true
end

function ConditionMgr:on_check_own_npc(player_id, npc_list, player)
    local master = player or player_mgr:get_entity(player_id)
    if master then
        for _, npc_id in ipairs(npc_list) do
            if not master:has_partner(npc_id) then
                return FAILED
            end
        end
        return SUCCESS
    end
    return FAILED
end

function ConditionMgr:on_check_town_level(player_id, level, player)
    local master = player or player_mgr:get_entity(player_id)
    if master then
        if master:get_level() >= level then
            return SUCCESS
        end
    end
    return FAILED
end

function ConditionMgr:on_check_finish_task(player_id, task_id, player)
    local master = player or player_mgr:get_entity(player_id)
    if master then
        if master:has_finish_task(task_id) then
            return SUCCESS
        end
    end
    return FAILED
end

function ConditionMgr:on_check_player_level(player_id, level, player)
    local master = player or player_mgr:get_entity(player_id)
    if master then
        if master:get_level() >= level then
            return SUCCESS
        end
    end
    return FAILED
end

function ConditionMgr:on_check_own_building(player_id, building_id, player)
    local master = player or player_mgr:get_entity(player_id)
    if master then
        local town = master:get_scene()
        if town and town:get_building(building_id) then
            return SUCCESS
        end
    end
    return FAILED
end

--args = { npc_id, building_id }
function ConditionMgr:on_check_assign_npc(player_id, args, player)
    local master = player or player_mgr:get_entity(player_id)
    if master then
        local npc_id, building_id = tunpack(args)
        if master:is_building_worker(npc_id, building_id) then
            return SUCCESS
        end
    end
    return FAILED
end


-- export
quanta.condition_mgr = ConditionMgr()

return ConditionMgr
