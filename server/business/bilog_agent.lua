--bilog_agent.lua
local RpcClient     = import("network/rpc_client.lua")

local log_info      = logger.info
local log_debug     = logger.debug
local log_err       = logger.err
local guid_new      = codec.guid_new
local json_encode   = json.encode
local tsize         = qtable.size
local tinsert       = table.insert
local env_addr      = environ.addr
local sformat       = string.format

local config_mgr    = quanta.get("config_mgr")
local monitor       = quanta.get("monitor")
local router_mgr    = quanta.get("router_mgr")
local event_mgr     = quanta.get("event_mgr")

local item_db       = config_mgr:init_table("item", "id")
local npc_db        = config_mgr:init_table("npc", "id")

local AttrID        = enum("AttrID")
local BI_STATUS     = environ.status("QUANTA_BI_STATUS")
local DAY_S         = quanta.enum("PeriodTime", "DAY_S")
local COMMIT        = quanta.enum("BuildingOpt", "COMMIT")
local REGION        = environ.number("QUANTA_REGION")

local BilogAgent = singleton()
local prop = property(BilogAgent)
prop:reader("client", nil)

function BilogAgent:__init()
    --创建连接
    -- 是否开启BI
    if BI_STATUS then
        local ip, port = env_addr("QUANTA_BILOG_ADDR")
        self.client = RpcClient(self, ip, port)
    end
    --监听事件
    event_mgr:add_trigger(self, "on_task_receive")
    event_mgr:add_trigger(self, "on_task_finished")
    event_mgr:add_trigger(self, "on_task_upgraded")
    event_mgr:add_trigger(self, "on_item_cost")
    event_mgr:add_trigger(self, "on_item_add")
    event_mgr:add_trigger(self, "on_attritem_cost")
    event_mgr:add_trigger(self, "on_attritem_add")
    event_mgr:add_trigger(self, "on_town_exp_change")
    event_mgr:add_trigger(self, "on_level_up")
    event_mgr:add_trigger(self, "on_login_success")
    event_mgr:add_trigger(self, "on_logout_success")
    event_mgr:add_trigger(self, "on_player_count")
    event_mgr:add_trigger(self, "on_product_receive")
    event_mgr:add_trigger(self, "on_product_cancel")
    event_mgr:add_trigger(self, "on_product_blueprint")
    event_mgr:add_trigger(self, "on_resource_gather")
    event_mgr:add_trigger(self, "on_shop_buy")
    event_mgr:add_trigger(self, "on_mail_opt")
    event_mgr:add_trigger(self, "on_sign_in")
    event_mgr:add_trigger(self, "on_unlock_package")
    event_mgr:add_trigger(self, "on_recr_result")
    event_mgr:add_trigger(self, "on_unlock_land")
    event_mgr:add_trigger(self, "on_building_stage")
    event_mgr:add_trigger(self, "on_battle_end")
    event_mgr:add_trigger(self, "on_achievement_flow")
    event_mgr:add_trigger(self, "on_npc_flow")
    event_mgr:add_trigger(self, "on_npc_dispatch")
    event_mgr:add_trigger(self, "on_friend_flow")
    event_mgr:add_trigger(self, "on_visit_flow")

    event_mgr:add_listener(self, "on_role_create")
    event_mgr:add_listener(self, "on_account_login")
    event_mgr:add_listener(self, "on_account_create")

    monitor:watch_service_ready(self, "bilog")
end

--发送事件
function BilogAgent:rpc_send(event, ...)
    if self.client then
        self.client:send(event, ...)
    end
end

-- 连接关闭回调
function BilogAgent:on_socket_error(client, token, err)
    log_info("[BilogAgent][on_socket_error]: connect lost!")
end

-- 连接成回调
function BilogAgent:on_socket_connect(client)
    log_info("[BilogAgent][on_socket_connect]: connect bilog success!")
    client:register()
end

--社交埋点(好友)
function BilogAgent:on_friend_flow(player, action, target_role_id)
    log_info("[BilogAgent][on_friend_flow] player:{} action:{}, target_role_id:{}", player:get_id(),
            action, target_role_id)
    local event = "social_flow"
    --social_type 1-加好友相关，2：进入他人家园相关
    local social_type = 1
    local target_role_guild_id = 0
    local flow_uid
    if player:get_id() > target_role_id then
        flow_uid = sformat("%s-%s", target_role_id, player:get_id())
    else
        flow_uid = sformat("%s-%s", player:get_id(), target_role_id)
    end

    return router_mgr:send_bilog_hash(player:get_user_id(), "rpc_bilog", player:get_id(),
                                    event, action, social_type, flow_uid, target_role_id, target_role_guild_id)
end

--社交埋点(拜访)
function BilogAgent:on_visit_flow(player, action, target_role_id)
    log_info("[BilogAgent][on_visit_flow] player:{} action:{}, target_role_id:{}", player:get_id(),
            action, target_role_id)
    local event = "social_flow"
    --social_type 1-加好友相关，2：进入他人家园相关
    local social_type = 2
    local flow_uid = sformat("%s-%s", player:get_id(), target_role_id)
    local target_role_guild_id = 0

    return router_mgr:send_bilog_hash(player:get_user_id(), "rpc_bilog", player:get_id(),
                                    event, action, social_type, flow_uid, target_role_id, target_role_guild_id)
end

--npc派遣埋点
function BilogAgent:on_npc_dispatch(player, action, partner, dispatch_type, detail_id)
    log_info("[BilogAgent][on_npc_dispatch] player:{} action:{}, partner:{}, dispatch_type:{}, detail_id:{}",
    player:get_id(), action, partner:get_proto_id(), dispatch_type, detail_id)
    --action 1派遣 2召回
    local event = "npc_dispatch"
    local npc_id = partner:get_proto_id()
    local npc_level = partner:get_star_lv()

    return router_mgr:send_bilog_hash(player:get_user_id(), "rpc_bilog", player:get_id(),
                                    event, action, npc_id, npc_level, dispatch_type, detail_id)
end

--npc养成埋点
function BilogAgent:on_npc_flow(player, action, partner, exp_amount, lv_amount, reason )
    --action 1npc经验增加 2npc好感度增加 3获得npc 4失去npc
    log_info("[BilogAgent][on_npc_flow] player:{} action:{}, partner:{}, exp_amount:{}, reason:{}",
     player:get_user_id(), action, partner:get_id(), exp_amount, reason)
    if not player:get_user_id() then
        return
    end
    local event = "npc_flow"
    local npc_id = partner:get_proto_id()
    local npc_reason = partner:get_from()
    local npc_level = partner:get_star_lv()
    local original_exp = 0
    local current_exp = 0
    local is_levelup = 0
    local original_level = 0
    local current_level = 0
    reason = reason or ""
    lv_amount = lv_amount or 0
    exp_amount = exp_amount or 0

    if action == 2 then
        original_exp =  partner:get_affinity() - exp_amount
        current_exp = partner:get_affinity()
        if lv_amount ~= 0 then
            is_levelup = 1
        end
        original_level = partner:get_affinity_lv() - lv_amount
        current_level = partner:get_affinity_lv()
    elseif action == 3 or action == 4 then
        original_exp = partner:get_affinity()
        current_exp = original_exp
        original_level = partner:get_affinity_lv()
        current_level = original_level
    end

    return router_mgr:send_bilog_hash(player:get_user_id(), "rpc_bilog", player:get_id(),
                                    event, action, npc_id, npc_reason, npc_level,reason,
                                    original_exp,exp_amount, current_exp, is_levelup,
                                    original_level, current_level)
end

--成就流水
function BilogAgent:on_achievement_flow(player, achive_lv)
    log_debug("[BilogAgent][on_achievement_flow] player:{} achive_lv:{}", player:get_id(), achive_lv)
    local event = "achievement_flow"
    local achievement_id = 0
    local achievement_level = achive_lv

    return router_mgr:send_bilog_hash(player:get_user_id(), "rpc_bilog", player:get_id(),
                                    event, achievement_id, achievement_level)
end

--战斗流水
function BilogAgent:on_battle_end(player, action, enemy_id)
    log_debug("[BilogAgent][on_battle_end] player:{} action:{} enemy_id:{}", player:get_id(), action, enemy_id)
    local event = "battle_end_flow"
    local weapon_id = player:get_weapon()
    local game_scene_id = sformat("%s+%s",player:get_map_id() , 1)
    local position = json_encode({x = player:get_pos_x(), y = player:get_pos_y(), z = player:get_pos_z()})

    return router_mgr:send_bilog_hash(player:get_user_id(), "rpc_bilog", player:get_id(),
                                    event, action, enemy_id, weapon_id, game_scene_id, position)
end

--建筑流水
function BilogAgent:on_building_stage(player, building_type, building_uid, building_conf, real_time, costs)
    log_debug("[BilogAgent][on_building_stage] player_id:{}", player:get_id())
    local event = "building_stage_flow"
    local building_id = building_conf.id
    local building_time = building_conf.build_time
    local building_item = "{}"
    if building_type == COMMIT then
        building_item = json_encode(costs)
    end
    local building_level = building_conf.level
    local event_seq = guid_new()
    return router_mgr:send_bilog_hash(player:get_user_id(), "rpc_bilog", player:get_id(), event,
                                    building_type, building_id, building_uid, building_level,
                                    building_time, building_item, event_seq, real_time)
end

--解锁地皮
function BilogAgent:on_unlock_land(player, land_conf)
    log_debug("[BilogAgent][on_unlock_land] player_id:{}", player:get_id())
    local event = "unlock_land"
    local unlock_type = land_conf.type
    local land_id = land_conf.id
    local size = {
        x = land_conf.point_x,
        y = land_conf.point_y
    }
    return router_mgr:send_bilog_hash(player:get_user_id(), "rpc_bilog", player:get_id(), event,
                                    unlock_type,land_id, json_encode(size))
end


--npc招募
function BilogAgent:on_recr_result(player, pool_id, is_free, type, p_used_cnt, costs, results)
    log_debug("[BilogAgent][on_npc_recruit] player_id:{}, pool_id:{}, is_free:{}, type:{}, p_used_cnt:{} costs:{} result:{}",
                player:get_id(), pool_id, is_free, type, p_used_cnt, costs, results)
    local event = "npc_recruit"
    local json_costs = json_encode(costs or {})

    local item_logs = {}
    for _, item in pairs(results or {}) do
        local quality
        --npc
        if item.proto_type == 1 then
            local npc_conf = self:load_npc_conf(item.proto_id)
            quality = npc_conf.quality
        else
            local item_conf = self:load_config(item.proto_id)
            quality = item_conf.quality
        end
        tinsert(item_logs,
        {
            proto_type = item.proto_type,
            proto_id = item.proto_id,
            rep_npc = item.rep_npc,
            quality = quality,
            rule = item.rule}
        )
    end
    local json_result = json_encode(item_logs or {})

    return router_mgr:send_bilog_hash(player:get_user_id(), "rpc_bilog", player:get_id(), event,
            pool_id,is_free, type, p_used_cnt, json_costs, json_result)
end

--解锁背包
function BilogAgent:on_unlock_package(player, unlock_package_space, after_package_space)
    log_debug("[BilogAgent][on_unlock_package] player_id:{}", player:get_id())
    local event = "unlock_package"
    return router_mgr:send_bilog_hash(player:get_user_id(), "rpc_bilog", player:get_id(), event, unlock_package_space, after_package_space)
end

--签到操作
function BilogAgent:on_sign_in(player, sign_id, sign_in_seq)
    log_debug("[BilogAgent][on_sign_in] player_id:{}", player:get_id())
    local event = "sign_in_flow"
    return router_mgr:send_bilog_hash(player:get_user_id(), "rpc_bilog", player:get_id(), event, sign_id, sign_in_seq)
end

--邮件操作
function BilogAgent:on_mail_opt(player, mail, status)
    log_debug("[BilogAgent][on_mail_opt] player_id:{}", player:get_id())
    local event = "mail_flow"
    local title = mail:get_title()
    local content = mail:get_content()
    local id = mail:get_uuid()
    local mail_type = mail:get_source()
    local is_annex = 2
    local annex_detail = {}
    if tsize(mail:get_attaches()) > 0 then
        is_annex = 1
        for item_id, item_count in pairs(mail:get_attaches()) do
            local item_conf = self:load_config(item_id)
            local item_info = {
                item_id = item_id,
                item_name = item_conf.name,
                item_count = item_count
            }
            tinsert(annex_detail, item_info)
        end
    end
    return router_mgr:send_bilog_hash(player:get_user_id(), "rpc_bilog", player:get_id(), event,
                                    title, content, id, mail_type, is_annex, status, json_encode(annex_detail))
end

--商店流水
function BilogAgent:on_shop_buy(player, shop_id, goods, num)
    log_debug("[BilogAgent][on_shop_buy] player:{}", player:get_id())
    local event = "shop_flow"
    local event_seq = guid_new()
    local item_proto_type = self:load_config(goods.item_id)
    local shop_type = shop_id
    local product_real_price = goods.price
    local product_unit_price = goods.price
    local discount = 0
    local product_type = item_proto_type.type
    local product_total_price = product_unit_price * num
    local product_name = item_proto_type.name
    local product_id = goods.item_id
    local product_count = num
    local item_effect_days = -1
    if item_proto_type.expire_time ~= 0 then
        local days = math.floor(item_proto_type.expire_time / DAY_S)
        item_effect_days = days
    end
    local price_type = goods.coin

    return router_mgr:send_bilog_hash(player:get_user_id(), "rpc_bilog", player:get_id(), event, event_seq,
                                        shop_type, product_real_price, product_unit_price,
                                        discount, product_type, product_total_price, product_name, product_id, product_count,
                                        item_effect_days, price_type)
end

--子任务更新
function BilogAgent:on_task_upgraded(player, task, next_id)
    log_debug("[BilogAgent][on_task_upgraded] player:{} task.child_id:{}", player:get_id(), task:get_child_id())
    local event = "task_flow"
    local event_seq = guid_new()
    local proto_type = task:get_prototype()
    local task_type = proto_type.type
    local task_name = proto_type.name or "" --普通订单任务无名字 这里留空兼容日志
    local task_id   = proto_type.id
    local task_sub_id = next_id
    local target_ids = ""
    local target_items = ""
    if task_type == 7 or task_type == 8 then
        target_items = self:contact_task_items(task)
    else
        target_ids = self:contact_task_targets(task)
    end

    local task_status = 3
    local real_time = quanta.now - task:get_create_time()

    return router_mgr:send_bilog_hash(player:get_user_id(), "rpc_bilog", player:get_id(), event,
                                event_seq, task_type, task_name, task_id, task_status, real_time, task_sub_id,
                                target_ids, target_items)
end

--拼接任务需求道具
function BilogAgent:contact_task_items(task)
    local result = {}
    local targets = task:get_targets() or {}
    for _, target in pairs(targets or {}) do
        local items = target:get_prototype().args or {}
        tinsert(result, items)
    end
    return json_encode(result or {})
end

--拼接任务target_id
function BilogAgent:contact_task_targets(task)
    local result = {}
    local targets = task:get_targets()
    for _, target in ipairs(targets) do
        local conf = target:get_prototype()
        tinsert(result, conf.target_id)
    end
    return json_encode(result or {})
end

--接受任务
function BilogAgent:on_task_receive(player, task)
    log_debug("[BilogAgent][on_task_receive] player:{} task.child_id:{}", player:get_id(), task:get_child_id())
    local event = "task_flow"
    local event_seq = guid_new()
    local proto_type = task:get_prototype()
    local task_type = proto_type.type
    local task_name = proto_type.name or "" --普通订单任务无名字 这里留空兼容日志
    local task_id   = proto_type.id
    local task_sub_id = task:get_child_id()
    local task_status = 1
    local real_time = 0
    local target_ids = ""
    local target_items = ""
    if task_type == 7 or task_type == 8 then
        target_items = self:contact_task_items(task)
    else
        target_ids = self:contact_task_targets(task)
    end

    return router_mgr:send_bilog_hash(player:get_user_id(), "rpc_bilog", player:get_id(), event,
                                event_seq, task_type, task_name, task_id, task_status, real_time, task_sub_id,
                                target_ids, target_items)
end

--完成任务
function BilogAgent:on_task_finished(player, task)
    log_debug("[BilogAgent][on_task_finished] player:{} task.child_id:{}", player:get_id(), task:get_child_id())
    local event = "task_flow"
    local event_seq = guid_new()
    local proto_type = task:get_prototype()
    local task_type = proto_type.type
    local task_name = proto_type.name
    local task_id   = proto_type.id
    local task_sub_id = 0
    local task_status = 2
    local real_time = quanta.now - task:get_create_time()
    local target_ids = ""
    local target_items = ""
    if task_type == 7 or task_type == 8 then
        target_items = self:contact_task_items(task)
    else
        target_ids = self:contact_task_targets(task)
    end

    return router_mgr:send_bilog_hash(player:get_user_id(), "rpc_bilog", player:get_id(), event,
                                event_seq, task_type, task_name, task_id, task_status, real_time, task_sub_id,
                                target_ids, target_items)
end

--资源采集
function BilogAgent:on_resource_gather(player, resource, drop, gather, time, floor)
    --仅掉血无产出
    if not drop then
        return
    end
    local event = "collect_flow"
    local event_seq = guid_new()
    --组装参数
    local collection_type = gather.type
    local collection_id = resource:get_proto_id()
    local collection_uid = resource:get_id()
    local tool_item_id = resource:find_equip(player, gather)
    local item_list = json_encode(drop.drop)
    local game_scene_id = sformat("%s+%s", resource:get_map_id(),  floor or 1)
    local position = json_encode({x = player:get_pos_x(), y = player:get_pos_y(), z = player:get_pos_z()})
    local collect_duration = time
    return router_mgr:send_bilog_hash(player:get_user_id(), "rpc_bilog", player:get_id(), event,
                                        collection_type,collection_id,
                                        collection_uid, tool_item_id,item_list,game_scene_id,
                                        position,collect_duration,event_seq)
end

--生产蓝图
function BilogAgent:on_product_blueprint(player, machine, blueprint, count)
    local produce_item = self:load_config(blueprint.produce_id)

    local event = "produce_item_flow"
    --玩家行为：1.开始生产，2.领取完成生产道具，3.取消生产，4.机器升级
    local action = 1
    local machine_level = machine:get_prototype().level
    local item_type = produce_item.type
    local produce_item_id = blueprint.produce_id
    local produce_num = count
    local speedup_type = 0
    local coin1_use = 0
    --蓝图没有时间
    local produce_time = 0
    local produce_item_json = json_encode(blueprint.makings)
    local event_seq = guid_new()
    --实际时长，完成建造或者完成升级时上报。
    local real_time = 0
    return router_mgr:send_bilog_hash(player:get_user_id(), "rpc_bilog", player:get_id(), event,
                                    action, machine_level, item_type, produce_item_id,
                                    produce_num, speedup_type, coin1_use, produce_time,
                                    produce_item_json, event_seq, real_time)
end

--生产取消
function BilogAgent:on_product_cancel(player, machine, product)
    local product_conf = product:get_prototype()
    local event = "produce_item_flow"
    --玩家行为：1.开始生产，2.领取完成生产道具，3.取消生产，4.机器升级
    local action = 3
    local machine_level = machine:get_prototype().level
    local item_type = product_conf.type
    local produce_item_id = product_conf.produce_id
    local produce_num = product.fin_count
    local speedup_type = 0
    local coin1_use = 0
    local produce_time = 0
    if product_conf.time then
        produce_time = product_conf.time * produce_num
    end
    local produce_item_json = "{}"
    local event_seq = guid_new()
    --实际时长，完成建造或者完成升级时上报。
    local real_time = produce_time--quanta.now - product:get_basetime()

    return router_mgr:send_bilog_hash(player:get_user_id(), "rpc_bilog", player:get_id(), event,
                                    action, machine_level, item_type, produce_item_id,
                                    produce_num, speedup_type, coin1_use, produce_time,
                                    produce_item_json, event_seq, real_time)
end

--生产完成
function BilogAgent:on_product_receive(player, machine, product)
    local product_conf = product:get_prototype()
    local event = "produce_item_flow"
    --玩家行为：1.开始生产，2.领取完成生产道具，3.取消生产，4.机器升级
    local action = 2
    local machine_level = machine:get_prototype().level
    local item_type = product_conf.type
    local produce_item_id = product_conf.produce_id
    local speedup_type = 0
    local coin1_use = 0
    local produce_time = 0
    --读取产出配置
    local produce_num = 1
    if product.fin_count then
        produce_num = product.fin_count
    end
    --蓝图没有时间
    if product_conf.time then
        produce_time = product_conf.time * produce_num
    end
    --读取消耗配置
    local produce_item_json
    if product_conf.costs then
        produce_item_json = json_encode(product_conf.costs)
    else
        produce_item_json = json_encode(product_conf.makings)
    end

    local event_seq = guid_new()
    --实际时长，完成建造或者完成升级时上报。
    local real_time = produce_time--quanta.now - product:get_basetime()

    return router_mgr:send_bilog_hash(player:get_user_id(), "rpc_bilog", player:get_id(), event,
                                    action, machine_level, item_type, produce_item_id,
                                    produce_num, speedup_type, coin1_use, produce_time,
                                    produce_item_json, event_seq, real_time)
end

--每分钟上报服务器在线
function BilogAgent:on_player_count(index, cur_player_cnt, max_player_cnt, min_player_cnt)
    self:rpc_send("rpc_server_player_cnt", index, cur_player_cnt, max_player_cnt, min_player_cnt)
end

--更新bilog对象
function BilogAgent:on_update_bilog_player(user_id, role_id, params)
    log_debug("[BilogAgent][on_update_bilog_player] user_id:{}, role_id:{}, params:{}", user_id, role_id, params)
    router_mgr:send_bilog_hash(user_id, "rpc_update_player", user_id, role_id, params)
end

--更新bilog的user对象
function BilogAgent:on_update_bilog_user(user_id, device_id, open_id,
                                        is_user_first, ip, lang, dev_plat)
    local server_id = REGION
    router_mgr:send_bilog_hash(user_id, "rpc_update_user", user_id, device_id, open_id,
                                is_user_first, server_id, ip, lang, dev_plat)
end

--创建游戏角色
function BilogAgent:on_role_create(user_id, role_id, role)
    log_debug("[BilogAgent][on_role_create] user_id:{} role_data:{}", user_id, role_id)
    --创建时间取当前时间相差不会超过1s
    self:on_update_bilog_player( user_id, role_id, {user_id = user_id, role_name = role.name, role_id = role_id, create_role_time = quanta.now})
    --创建成功
    self:create_role_success(user_id, role_id)
    --捏脸
    local event = "character_building"
    router_mgr:send_bilog_hash(user_id,"rpc_bilog", role_id, event, role.gender, role.custom)
end

--创建账号
function BilogAgent:on_account_create(account, device_id, ip, lang, dev_plat)
    local user_id = account:get_user_id()
    local open_id = account:get_open_id()
    local is_user_first = 1
    self:on_update_bilog_user(user_id, device_id, open_id,  is_user_first, ip, lang, dev_plat)
end

--登陆账号
function BilogAgent:on_account_login(user_id, open_id, device_id, ip, lang, dev_plat)
    local is_user_first = 0
    self:on_update_bilog_user(user_id, device_id, open_id, is_user_first, ip, lang, dev_plat)
end

--角色登陆
function BilogAgent:on_login_success(player_id, player)
    --通知bilog
    local params = {
        role_id             = player_id,
        role_level          = player:get_level(),
        role_name           = player:get_name(),
        coin2               = player:get_coin(),
        coin1               = player:get_diamond(),
        create_role_time    = player:get_create_time(),
        user_unique_id      = player:get_open_id(),
        user_id             = player:get_user_id()
    }
    self:on_update_bilog_player(player:get_user_id(), player_id, params)
    self:role_login_success(player:get_user_id(), player)
end

--角色登出
function BilogAgent:on_logout_success(player_id, player)
    --通知billog
    local position = {
        x = player:get_pos_x(),
        y = player:get_pos_y(),
        z = player:get_pos_z()
    }
    local event         = "role_logout"
    local is_real       = 1
    local duration      = quanta.now - player:get_login_time()
    local coin1         = player:get_diamond()
    local coin2         = player:get_coin()
    local total_dur     = player:get_online_time() + duration
    local position_json = json_encode(position)

    router_mgr:send_bilog_hash(player:get_user_id(),"rpc_bilog", player:get_id(), event, is_real, duration, coin1, coin2, total_dur, position_json)
    return router_mgr:send_bilog_hash(player:get_user_id(), "rpc_logout_player",player:get_user_id(), player:get_id())
end


--玩家道具消耗
function BilogAgent:on_item_cost(role, item_id, num, reason)
    local event = "item_flow"
    local event_seq = guid_new()
    local prototype = item_db:find_one(item_id)
    local item_type = prototype.type
    local item_name = prototype.name
    --当前背包数-变动数
    local count = role:get_item_num(item_id)
    local before_amount = count + num
    local amount = num
    local remain_amount = count
    local action = 2
    local sub_reason = 0
    local item_effect_days
    if prototype.expire_time ~= 0 then
        local days = math.floor(prototype.expire_time / DAY_S)
        item_effect_days = days
    else
        item_effect_days = -1
    end
    return router_mgr:send_bilog_hash(role:get_user_id(), "rpc_bilog", role:get_id(), event,
                                     event_seq, item_type, item_name, item_id, before_amount,
                                     amount, remain_amount, action, sub_reason, item_effect_days, reason)
end

--玩家道具增加
function BilogAgent:on_item_add(role, item_id, num, reason)
    local event = "item_flow"
    local event_seq = guid_new()
    local prototype = item_db:find_one(item_id)
    local item_type = prototype.type
    local item_name = prototype.name
    local count = role:get_item_num(item_id)
    local before_amount = count - num
    local amount = num
    local remain_amount =count
    local action = 1
    local sub_reason = 0
    local item_effect_days
    if prototype.expire_time ~= 0 then
        local days = math.floor(prototype.expire_time / DAY_S)
        item_effect_days = days
    else
        item_effect_days = -1
    end
    return router_mgr:send_bilog_hash(role:get_user_id(), "rpc_bilog", role:get_id(), event,
                                     event_seq, item_type, item_name, item_id, before_amount,
                                     amount, remain_amount, action, sub_reason, item_effect_days, reason)
end

--玩家属增加事件
function BilogAgent:on_attritem_add(role, item_id, attr_id, num, reason)
    --经验事件
    if attr_id == AttrID.ATTR_EXP then
        self:exp_flow(role, reason, num)
        return
    end
    --货币事件
    if attr_id == AttrID.ATTR_COIN or attr_id == AttrID.ATTR_DIAMOND  then
        self:money_flow(role, attr_id, num, reason, 1)
        return
    end
    --能量事件
    if attr_id == AttrID.ATTR_ENERGY  then
        self:energy_flow(role, 1, num, reason)
    end
end

--玩家属性减少事件
function BilogAgent:on_attritem_cost(role, item_id,attr_id, num, reason)
    --经验事件
    if attr_id == AttrID.ATTR_EXP then
        return
    end
    --货币事件
    if attr_id == AttrID.ATTR_COIN or attr_id == AttrID.ATTR_DIAMOND  then
        self:money_flow(role, attr_id, num, reason, 2)
        return
    end
    --能量事件
    if attr_id == AttrID.ATTR_ENERGY  then
        self:energy_flow(role, 2, num, reason)
    end
end

--能量增加流水
function BilogAgent:energy_flow(role, action, amount, reason)
    local event = "energy_flow"
    local after_amount = role:get_energy()
    return router_mgr:send_bilog_hash(role:get_user_id(), "rpc_bilog", role:get_id(),
                                    event, action, amount, after_amount, reason)
end

--货币增加流水
function BilogAgent:money_flow(role, coin_type, amount, reason, action)
    local event = "money_flow"
    local event_seq = guid_new()
    local after_amount
    if coin_type == AttrID.ATTR_COIN then
        after_amount = role:get_coin()
        coin_type = 2
    else
        after_amount = role:get_diamond()
        coin_type = 1
    end
    local sub_reason = 0
    return router_mgr:send_bilog_hash(role:get_user_id(), "rpc_bilog", role:get_id(), event,
                                        coin_type, action, amount, after_amount, reason, event_seq, sub_reason)
end

--等级变动(经验流水)
function BilogAgent:on_level_up(role, level, level_up, exp, exp_cur)
    local event = "exp_flow"
    local event_seq = guid_new()
    local exp_type = 1
    --升级埋点起始经验值为0
    local original_exp = 0
    local current_exp = exp_cur
    local exp_amount =  exp
    local is_levelup = 1
    --当前等级-变动等级
    local original_level = level - level_up
    local current_level = level
    local total_time = quanta.now - role:get_upgrade_time()
    local reason = 1
    --同步等级属性
    self:on_update_bilog_player(role:get_user_id(),role:get_id(), {role_level = level})
    return router_mgr:send_bilog_hash(role:get_user_id(), "rpc_bilog", role:get_id(), event, event_seq,
                                        exp_type, original_exp, current_exp, is_levelup,
                                        original_level, current_level, total_time,reason,exp_amount)
end

--经验流水
function BilogAgent:exp_flow(role, reason, exp_amount)
    local event = "exp_flow"
    local event_seq = guid_new()
    local exp_type = 1
    local original_exp = role:get_exp() - exp_amount
    local current_exp = role:get_exp()
    --未升级
    local is_levelup = 0
    local level = role:get_level()
    local original_level = level
    local current_level =level
    local total_time = 0
    return router_mgr:send_bilog_hash(role:get_user_id(), "rpc_bilog", role:get_id(), event, event_seq,
                                        exp_type, original_exp, current_exp, is_levelup,
                                        original_level, current_level, total_time,reason,exp_amount)
end

--玩家城镇繁荣度改变
function BilogAgent:on_town_exp_change(role, amount, after_amount, reason)
    local sub_reason = 0
    local event = "money_flow"
    local coin_type = 3                     --繁荣度
    local event_seq = guid_new()
    local action = (amount > 0) and 1 or 2  --增加/消耗
    return router_mgr:send_bilog_hash(role:get_user_id(), "rpc_bilog", role:get_id(), event,
            coin_type, action, amount, after_amount, reason, event_seq, sub_reason)
end

--创建角色
function BilogAgent:create_role_success(user_id, role_id)
    local event = 'create_role_success'
    -- 非公参
    local is_user_first = 1
    return router_mgr:send_bilog_hash(user_id, "rpc_bilog", role_id, event, is_user_first)
end

--角色登录
function BilogAgent:role_login_success(user_id, player)
    local event = 'role_login_success'
    -- 非公参
    local is_real = 1
    local coin2 = player:get_coin()
    local total_dur = player:get_online_time()
    local guild_id = 0
    local total_friends = 0
    local coin1 = player:get_diamond()
    return router_mgr:send_bilog_hash(user_id, "rpc_bilog", player:get_id(), event, is_real,
                                        guild_id, total_friends, coin1, coin2, total_dur, user_id)
end


-- 服务已经ready
function BilogAgent:on_service_ready(id, service_name)
    log_debug("[BilogAgent][on_service_ready]->id:{}, service_name:{}", id, service_name)
end

---------------------------------------辅助查询------------------------------------------
function BilogAgent:load_config(item_id)
    local conf =  item_db:find_one(item_id)
    if not conf then
        log_err("[BilogAgent][load_config] load item({}) config failed", item_id)
        return
    end
    return conf
end

function BilogAgent:load_npc_conf(proto_id)
    local conf = npc_db:find_one(proto_id)
    if not conf then
        log_err("[BilogAgent][load_npc_conf] load npc({}) config failed", proto_id)
        return
    end
    return conf
end

quanta.bilog = BilogAgent()

return BilogAgent
