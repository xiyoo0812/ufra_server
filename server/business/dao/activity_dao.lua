import("agent/mongo_agent.lua")

local log_debug   = logger.debug
local log_err     = logger.err
local qfailed     = quanta.failed
local mceil       = math.ceil
local tinsert     = table.insert

local mongo_agent = quanta.get("mongo_agent")

local doc_survey  = "player_surveys"
local doc_cdkey   = "cdkey_record"

local ActivityDao   = singleton()
function ActivityDao:__init()
end

-- 加载活动列表
function ActivityDao:load_activity_list()
    local ok, code, data = mongo_agent:find({ "activity_list", { } },nil)
    if not ok or qfailed(code) then
        return false, code
    end
    return ok, data
end

-- 加载活动配置
function ActivityDao:load_activity_config()
    local ok, code, data = mongo_agent:find({ "activity_config", { } },nil)
    if not ok or qfailed(code) then
        return false, code
    end
    return ok, data
end

-- 获取调查问卷数量
function ActivityDao:get_survery_count(id)
    local selector = {
        ["$and"] = {
            { ["activity_id"]  = id}
        }
    }
    local query = {doc_survey, selector}
    local ok, code, count = mongo_agent:count(query)
    if qfailed(code, ok) then
        log_debug("[ActivityDao][get_survery_count] document:{} code:{}", doc_survey, code)
        return false, code
    end
    return true, count
end

-- 加载问卷列表
function ActivityDao:load_survery_list(id, size, page)
    if page and size and page > 0 and size > 0 then
        local skip = (page - 1) * size
        local selector = {
            ["$and"] = {
                { ["activity_id"]  = id}
            }
        }
        local query = { doc_survey, selector, nil, nil, size, skip }
        local ok, code, data = mongo_agent:find(query)
        if qfailed(code, ok) then
            log_debug("[ActivityDao][load_survery_list] find document:{} code:{}", doc_survey, code)
            return false, code
        end

        local c_ok, count = self:get_survery_count(id)
        if not c_ok then
            log_debug("[ActivityDao][load_survery_list] get_survery_count document:{}")
            return false
        end

        return true, {
            list = data,
            total = mceil(count/size),
        }
    else
        return false, -1
    end
end

-- 问卷搜索
function ActivityDao:survey_search(id, player_id, size, page)
    if player_id then
        local selector = {
            ["$and"] = {
                { ["activity_id"]  = id},
                { ["player_id"]  = player_id}
            }
        }
        local skip = (page - 1) * size
        local query = { doc_survey, selector, nil, nil, size, skip }
        local ok, code, data = mongo_agent:find(query)
        if not ok or qfailed(code) then
            return false, code
        end
        return ok, data
    else
        return false, -1
    end
end

-- 添加问卷记录
function ActivityDao:add_survery_record(activity_id, player_id, surveys)
    local data = {
        activity_id = activity_id,
        player_id = player_id,
        surveys = surveys
    }
    log_debug("[ActivityDao][add_survery_record] doc_survey:{} data:{}", doc_survey, data)
    local query = { doc_survey, { player_id = player_id } }
    local ok, code, count = mongo_agent:count(query)
    if qfailed(code, ok) then
        log_err("[ActivityDao][add_survery_record] count doc_survey:{} data:{} code:{}", doc_survey, data, code)
        return false
    end
    -- 添加数据
    if count <= 0 then
        query = { doc_survey, data }
        ok, code = mongo_agent:insert(query)
        if qfailed(code, ok) then
            log_err("[ActivityDao][add_survery_record] insert doc_survey:{} data:{} code:{}", doc_survey, data, code)
            return false
        end
        return true
    end
    -- 更新数据
    query = { doc_survey, data, { player_id = player_id } }
    ok, code = mongo_agent:update(query)
    if qfailed(code, ok) then
        log_err("[ActivityDao][add_survery_record] update doc_survey:{} data:{} code:{}", doc_survey, data, code)
        return false
    end
    log_debug("[ActivityDao][add_survery_record] success doc_survey:{} data:{}", doc_survey, data)
    return true
end

-- 获取cdkey数量
function ActivityDao:get_cdkey_count(id, start_time, end_time)
    local selector = {activity_id=id}
    if start_time and end_time then
        selector = {
            ["$and"] = {
                { ["activity_id"]  = id},
                { ["time"]  = {["$gte"] = start_time}},
                { ["time"]  = {["$lt"] = end_time}}
            }
        }
    end
    local query = {doc_cdkey,selector}
    local ok, code, count = mongo_agent:count(query)
    if qfailed(code, ok) then
        log_debug("[ActivityDao][get_cdkey_count] document:{} code:{}", doc_cdkey, code)
        return false, code
    end
    return true, count
end

-- 加载cdkey列表
function ActivityDao:load_cdkey_list(id, start_time, end_time, size, page)
    if page and size and page > 0 and size > 0 and start_time and end_time then
        local skip = (page - 1) * size
        local selector = {
            ["$and"] = {
                { ["activity_id"]  = id},
                { ["time"]  = {["$gte"] = start_time}},
                { ["time"]  = {["$lt"] = end_time}}
            }
        }
        local query = { doc_cdkey, selector, nil, nil, size, skip }
        local f_ok, f_code, data = mongo_agent:find(query)
        if not f_ok or qfailed(f_code) then
            return false, f_code
        end

        local c_ok, count = self:get_cdkey_count(id, start_time, end_time)
        if not c_ok then
            log_debug("[ActivityDao][get_cdkey_count] document:{} code:{}", doc_cdkey)
            return false
        end

        return true, {
            list = data,
            total = count,
        }
    else
        return false, -1
    end
end

-- 加载cdkey记录
function ActivityDao:load_cdkey_record()
    local ok, code, data = mongo_agent:find({ doc_cdkey })
    if not ok or qfailed(code) then
        return false, code
    end
    return true, data
end

-- cdkey搜索
function ActivityDao:cdkey_search(id, name, size, page)
    if name then
        local player_id = tonumber(name)
        local selector = {
            ["$and"] = {
                {["activity_id"] = id}
            },
            ["$or"] = {
                {["device_id"] = name},
                {["key"] = name},
            }
        }
        if player_id then
            tinsert( selector["$or"], {["player_id"] = player_id})
        end
        local skip = (page - 1) * size
        local query = { doc_cdkey, selector, nil, nil, size, skip }
        local ok, code, data = mongo_agent:find(query)
        if not ok or qfailed(code) then
            return false, code
        end

        local c_ok, c_code, c_count = mongo_agent:count(query)
        if qfailed(code, c_ok) then
            log_debug("[ActivityDao][cdkey_search] document:{} code:{}", doc_cdkey, c_code)
            return false, c_code
        end

        return true, {
            list = data,
            total = c_count,
        }
    else
        return false, -1
    end
end

-- cdkey信息
function ActivityDao:cdkey_info()
    local query = { doc_cdkey, nil, nil, nil, nil, nil }
    local ok, code, data = mongo_agent:find(query)
    if not ok or qfailed(code) then
        return false, code
    end
    return ok, data
end

-- 添加记录
function ActivityDao:add_cdkey_record(player_id, device_id, activity_id, type, key)
    local data = {
        player_id = player_id,
        device_id = device_id,
        activity_id = activity_id,
        type = type,
        key = key,
        time = quanta.now,
    }
    log_debug("[ActivityDao][add_cdkey_record] doc_cdkey:{} data:{}", doc_cdkey, data)
    local query = { doc_cdkey, data }
    local ok, code = mongo_agent:insert(query)
    if qfailed(code, ok) then
        log_err("[ActivityDao][add_cdkey_record] insert doc_cdkey:{} data:{} code:{}", doc_cdkey, data, code)
        return false
    end
    log_debug("[ActivityDao][add_cdkey_record] success doc_cdkey:{} data:{}", doc_cdkey, data)
    return true
end
quanta.activity_dao = ActivityDao()
return ActivityDao
