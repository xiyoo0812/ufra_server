--safe_check_agent.lua
local log_err           = logger.err
local qfailed           = quanta.failed
local router_mgr        = quanta.get("router_mgr")
local protobuf_mgr      = quanta.get("protobuf_mgr")

local SAFE_CHECK        = environ.status("QUANTA_SAFE_CHECK")
local FRAME_SUCCESS     = protobuf_mgr:error_code("FRAME_SUCCESS")

local SafeCheckAgent = singleton()
function SafeCheckAgent:__init()
end

--内容安全检查
function SafeCheckAgent:on_safe_check(user_id, text, msg_type, replace, extension)
    -- 是否开启检查
    if not SAFE_CHECK then
        return FRAME_SUCCESS, text
    end
    --rpc到bilog
    local rpc_ok, code, res_text = router_mgr:call_bilog_hash(user_id, "rpc_safe_check", user_id, text, msg_type, replace, extension)
    if qfailed(code, rpc_ok) then
        log_err("[SafeCheckAgent][on_safe_check] rpc_safe_check rpc_ok:{}, code:{}", rpc_ok, code)
        return code
    end
    return FRAME_SUCCESS, res_text
end

--带决策的内容安全检查
function SafeCheckAgent:on_safe_check_decision(user_id, text, msg_type, extension)
    -- 是否开启检查
    if not SAFE_CHECK then
        local res = self:build_pass(text)
        return true, res, FRAME_SUCCESS
    end
    --rpc到bilog
    local rpc_ok, code, res = router_mgr:call_bilog_hash(user_id, "rpc_safe_check_decision", user_id, text, msg_type, extension)
    if qfailed(code, rpc_ok) then
        log_err("[SafeCheckAgent][on_safe_check_decision] rpc_safe_check_decision rpc_ok:{}, code:{}", rpc_ok, code)
        return false, res, code
    end
    return true, res, code
end

--内部调用
------------------------------------------------
--构建成功决策
function SafeCheckAgent:build_pass(text)
    local res = {
        data = {
            decision = {
                errCode = 0,
                errMsg = "pass"
            },
            contents = {
                {
                    content = text,
                    filteredText = text,
                    decision = {
                        errCode = 0,
                        errMsg = "pass"
                    },
                }
            }
        }
    }
    return res
end

quanta.safe_check_agent = SafeCheckAgent()

return SafeCheckAgent
