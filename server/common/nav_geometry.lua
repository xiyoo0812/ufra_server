--nav_geometry.lua
local matan   = math.atan
local mrad    = math.rad
local mdeg    = math.deg
local mcos    = math.cos
local msin    = math.sin
local msqrt   = math.sqrt
local sformat = string.format
local mpi     = math.pi
local mrandom = math.random

---@class nav_geometry
local nav_geometry = {}

function nav_geometry.custom_atan2(y, x)
    local angle

    if x > 0 then
        angle = matan(y / x)
    elseif x < 0 and y >= 0 then
        angle = matan(y / x) + mpi
    elseif x < 0 and y < 0 then
        angle = matan(y / x) - mpi
    elseif x == 0 and y > 0 then
        angle = mpi / 2
    elseif x == 0 and y < 0 then
        angle = -mpi / 2
    else
        angle = 0
    end
    return angle
end

--- 度数转换为弧度
function nav_geometry.degree_to_radian(angle)
    -- return angle * mpi / 180
    return mrad(angle)
end

--- 弧度转换为度数
function nav_geometry.radian_to_degree(angle)
    -- return angle * 180 / mpi
    return mdeg(angle)
end

---使用xz的朝向计算y轴的欧拉角(基于z朝向)
---@param dir vector3
---@return number
function nav_geometry.dir_to_euler_angle(dir)
    -- forward向量
    local forward_x = 0
    local forward_z = 1
    -- 计算弧度
    local rad = nav_geometry.custom_atan2(dir.x * forward_z - dir.z * forward_x, dir.x * forward_x + dir.z * forward_z)
    -- 弧度转角度
    local degree = nav_geometry.radian_to_degree(rad)
    -- 确保角度在 -180 到 180 之间
    if degree > 180 then
        degree = degree - 360
    elseif degree < -180 then
        degree = degree + 360
    end
    return degree
end

---y方向的欧拉角转朝向(y为0)
---@param angle number
---@return vector3
function nav_geometry.euler_angle_to_dir(angle)
    angle = angle % 360
    if angle < 0 then
        angle = angle + 360
    end
    local rad = nav_geometry.degree_to_radian(angle)
    return {x = msin(rad), y= 0, z= mcos(rad) }
end

---@param vec vector3
function nav_geometry.vector3_to_str(vec)
    return sformat("{x=%f, y=%f, z=%f}", vec.x, vec.y, vec.z)
end

---空间两个点之间的距离
---@param pos1 vector3
---@param pos2 vector3
function nav_geometry.calc_distance(pos1, pos2)
    return msqrt((pos2.x - pos1.x)^2 + (pos2.y - pos1.y)^2 + (pos2.z - pos1.z)^2)
end

---空间点到另一个点的方向
---@param pos1 vector3
---@param pos2 vector3
function nav_geometry.calc_dir(pos1, pos2)
    -- 方向向量
    local dir = {
        x = pos2.x - pos1.x,
        y = pos2.y - pos1.y,
        z = pos2.z - pos1.z,
    }
    -- 方向向量长度
    local len = msqrt(dir.x^2 + dir.y^2 + dir.z^2)
    if len ~= 0 then
        -- 单位化
        dir.x = dir.x / len
        dir.y = dir.y / len
        dir.z = dir.z / len
    end
    return dir
end

---空间点到另一个点的的连线上，离另一个的近似的一个位置
---@param pos1 vector3
---@param pos2 vector3
---@param offset number
---@param random_angle boolean  是否随机旋转角度
function nav_geometry.calc_approximate(pos1, pos2, offset, random_angle)
    -- 计算向量 vec12
    local vec12 = {
        x = pos2.x - pos1.x,
        y = pos2.y - pos1.y,
        z = pos2.z - pos1.z
    }
    -- 单位化 vec12,得到 dir
    local length = math.sqrt(vec12.x^2 + vec12.y^2 + vec12.z^2)
    local dir = {
        x = vec12.x / length,
        y = vec12.y / length,
        z = vec12.z / length
    }
    -- 随机旋转角度
    if random_angle then
        local angle = mrandom(0, 90)
        local is_clockwise = quanta.now_ms % 2 == 0
        dir = nav_geometry.rotate_dir(dir, angle, is_clockwise)
    end
    -- 计算向量 vec23, 长度为 offset
    local vec23 = {
        x = dir.x * offset,
        y = dir.y * offset,
        z = dir.z * offset
    }
    -- 计算投影向量 proj
    local dot_product = vec12.x * vec23.x + vec12.y * vec23.y + vec12.z * vec23.z
    local proj = {
        x = dir.x * dot_product,
        y = dir.y * dot_product,
        z = dir.z * dot_product
    }
    -- 缩放投影向量为 offset, 得到向量 vec23
    length = msqrt(proj.x^2 + proj.y^2 + proj.z^2)
    vec23.x = proj.x / length * offset
    vec23.y = proj.y / length * offset
    vec23.z = proj.z / length * offset
    -- 计算点 pos3 的坐标
    return {
        x = pos2.x + vec23.x,
        y = pos2.y + vec23.y,
        z = pos2.z + vec23.z
    }
end

---方向旋转指定的角度
function nav_geometry.rotate_dir(dir, angle, is_clockwise)
    angle = is_clockwise and angle or -angle
    local rad = nav_geometry.degree_to_radian(angle)
    local x
    local y = dir.y -- y不变
    local z
    x = dir.x * mcos(rad) + dir.z * msin(rad)
    z = dir.z * mcos(rad) - dir.x * msin(rad)
    return {x=x, y=y, z=z}
end

---空间点到另一个点的的连线的中点
---@param pos1 vector3
---@param pos2 vector3
function nav_geometry.middle(pos1, pos2)
    -- 计算向量 vec12
    local vec12 = {
        x = pos2.x - pos1.x,
        y = pos2.y - pos1.y,
        z = pos2.z - pos1.z
    }

    -- 计算向量 vec12 的一半
    local half_vec12 = {
        x = vec12.x / 2,
        y = vec12.y / 2,
        z = vec12.z / 2
    }

    -- 计算中点 pos3 的坐标
    return {
        x = pos1.x + half_vec12.x,
        y = pos1.y + half_vec12.y,
        z = pos1.z + half_vec12.z
    }
end

---空间点到另一个点的的直线上, 距离第一个点指定距离的点
---@param pos_from vector3 第一个点
---@param pos_to vector3 另一个点
---@param distance number 距离第一个点的距离
---@param is_from_base boolean 是否以第一个点为基准
---@return vector3 距离第一个点指定距离的点
function nav_geometry.online_at_pos(pos_from, pos_to, distance, is_from_base)
    -- 第一个点到另一个点的方向
    local dir = nav_geometry.calc_dir(pos_from, pos_to)
    local base_point = pos_from
    if not is_from_base then
        base_point = pos_to
    end
    -- 计算C点的坐标
    return { x = base_point.x + dir.x * distance, y = base_point.y + dir.y * distance, z = base_point.z + dir.z * distance, }
end

---环形移动
---@param pos vector3 当前位置
---@param center vector3 运动圆心
---@param radius number 运动半径
---@param speed number 运动速度
---@param duration number 运动持续时间
---@param is_clockwise boolean 是否顺时针运动
---@return vector3 终点位置
function nav_geometry.circular_move(pos, center, radius, speed, duration, is_clockwise)
    -- 计算初始角度
    local start_angle = nav_geometry.radian_to_degree(nav_geometry.custom_atan2(pos.z - center.z, pos.x - center.x))

    -- 根据顺时针或逆时针设置角速度
    local angular_speed = speed / radius
    if not is_clockwise then
        angular_speed = -angular_speed
    end

    -- 计算怪物的位置
    local angle = nav_geometry.degree_to_radian(start_angle + duration * angular_speed)
    -- 计算怪物的新位置
    local x = center.x + radius * mcos(angle)
    local z = center.z + radius * msin(angle)
    return { x = x, y = center.y, z = z }
end

function nav_geometry.circular_move2(pos, center, radius, speed, duration, is_clockwise)
    local dir =  nav_geometry.calc_dir(center, pos)
    local cur_angle = nav_geometry.dir_to_euler_angle(dir)

    -- 根据顺时针或逆时针设置角速度
    local angular_speed = speed / radius
    if not is_clockwise then
        angular_speed = -angular_speed
    end
    local new_angle = cur_angle +  nav_geometry.radian_to_degree(angular_speed * duration)
    logger.dump('cur_angle: {}, new_angle: {}', cur_angle, new_angle)

    local new_dir = nav_geometry.euler_angle_to_dir(new_angle)
    local new_pos = {
        x = center.x + new_dir.x * radius,
        y = center.y + new_dir.y * radius,
        z = center.z + new_dir.z * radius,
    }
    return new_pos
end

function nav_geometry.circular_move3(pos, center, radius, speed, duration, is_clockwise)
    local dir =  nav_geometry.calc_dir(center, pos)
    local cicle = 2*math.pi*radius
    local angle = speed * duration / cicle * 360
    local new_dir = nav_geometry.rotate_dir(dir, angle, is_clockwise)
    local new_pos = {
        x = center.x + new_dir.x * radius,
        y = center.y + new_dir.y * radius,
        z = center.z + new_dir.z * radius,
    }
    return new_pos
end


return nav_geometry