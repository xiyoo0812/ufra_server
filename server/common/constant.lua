--constant.lua
local rawget   = rawget

--数据加载状态
local MapType  = enum("MapType", 0)
MapType.PUBLIC = 1      --公共场景
MapType.COPY   = 2      --副本场景


--道具类型
local ItemType                    = enum("ItemType", 0)
ItemType.MONEY                    = 1 --货币
ItemType.ITEM                     = 2 --道具
ItemType.EQUIP                    = 3 --装备
ItemType.SUIT                     = 4 --时装
ItemType.UTENSIL                  = 5 --机器
ItemType.TOKEN                    = 6 --招募命
ItemType.HEAD                     = 7 --头像
ItemType.BUILDING                 = 8 --建筑
ItemType.FURN                     = 9 --家具
ItemType.NOCOST                   = 10 --非消耗道具
ItemType.EFFECT                   = 11 --效果道具

--资源效果类型
local ResEftType                  = enum("ResEftType", 0)
ResEftType.HP                     = 1 --hp
ResEftType.COUNT                  = 2 --次数
ResEftType.CD                     = 3 --读条

--物件类型
local UtensilType                 = enum("UtensilType", 0)
UtensilType.WORK                  = 1 --工作台
UtensilType.PACK                  = 2 --组装台
UtensilType.MELT                  = 3 --熔炉
UtensilType.STORE                 = 4 --储物箱
UtensilType.HOUSE                 = 5 --民居
UtensilType.FURN                  = 6 --家具
UtensilType.PLUG                  = 7 --挂件
UtensilType.RECR                  = 8 --招募处
UtensilType.COMPANY               = 9 --建筑公司
UtensilType.SHOP                  = 10 --商店
UtensilType.OTHER                 = 11 --其他
UtensilType.HALL                  = 12 --市政厅
UtensilType.PROD                  = 13 -- 生产
UtensilType.GATHER                = 14 -- 采集
UtensilType.BANK                  = 15 -- 仓库

--物件结构
local StrtctType                  = enum("StrtctType", 0)
StrtctType.STRUCT                 = 1 --结构件
StrtctType.BUILDING               = 2 --建筑
StrtctType.UTENSIL                = 3 --用具(家具/摆件/挂件)

--效果类型
local EffectType                  = enum("EffectType", 0)
EffectType.VATTR                  = 1 --属性值修改
EffectType.BOX                    = 2 --宝箱/礼包
EffectType.BUFF                   = 3 --buff
EffectType.DRAWING                = 4 --图纸
EffectType.RATTR                  = 5 --属性比例修改
EffectType.ITEM                   = 6 --获得道具
EffectType.TOWN_EXP               = 7 --繁荣度改变
EffectType.NPC                    = 8 --获得npc
EffectType.AFFINITY               = 9 --好感度
EffectType.PARTNER_ATTR           = 13 --npc属性
EffectType.BUILDING_ATTR          = 14 --建筑属性
EffectType.BUILDING_COND          = 15 --在x类建筑上工作
EffectType.SAT                    = 16 --满意度
EffectType.BUILD_GROUP_COND       = 17 --在A建筑上工作

--效果触发类型，取值范围1-8
local TriggerType                 = enum("TriggerType", 0)
TriggerType.START                 = 1 --激活触发效果，可叠加
TriggerType.PERIOD                = 2 --周期触发效果，可叠加
TriggerType.STOP                  = 4 --结束触发效果，可叠加
TriggerType.TEMP                  = 8 --激活触发效果，结束需要还原，不可叠加

--buff叠加类型
local OverlapType                 = enum("OverlapType", 0)
OverlapType.NONE                  = 1 --放弃
OverlapType.COVER                 = 2 --覆盖
OverlapType.SHARE                 = 3 --共存
OverlapType.TIME                  = 4 --时间叠加

-- 行为类型定义
-- from: CCCMobile\Client_Mobile\MobileClient\u3dclient\Assets\Script\ActionModule\Core\ActionClass.cs
--       `enum Pathea.ActionNs.ActionType`
local ActionType                  = enum("ActionType", 0)
ActionType.Move                   = 0 -- 移动
ActionType.Jump                   = 1 -- 跳跃
ActionType.Rotate                 = 2 -- 旋转
ActionType.Translate              = 3 -- 传送
ActionType.AnimOnce               = 4 -- 动作（单次）
ActionType.AnimLoop               = 5 -- 动作（循环）
ActionType.Timeline               = 6 -- 受timeline控制状态
ActionType.Hold                   = 7 -- 抱
ActionType.Roll                   = 8 -- 翻滚
ActionType.PutDown                = 9 -- 放东西
ActionType.Interact               = 10 -- 功能对象交互
ActionType.Pickup                 = 11 -- 拿东西
ActionType.Melee                  = 12 -- 近战
ActionType.HitHeavy               = 13 -- 重被击
ActionType.HitLight               = 14 -- 轻被击
ActionType.Death                  = 15 -- 死亡
ActionType.IK                     = 16 -- IK
ActionType.Expression             = 17 -- 表情
ActionType.HitStrong              = 18 -- 强被击
ActionType.FreeClimb              = 19 -- 自由攀爬
ActionType.Parachute              = 20 -- 滑翔
ActionType.Max                    = rawget(ActionType, "__vmax")

-- 邮件模版标识
local EmailTemplateId             = enum("EmailTemplateId", 0)
EmailTemplateId.REISSUE           = 100101 -- 补发(背包满了的情况下补发)

--战斗状态
local CombatState                 = enum("CombatState", 0)
CombatState.NORMAL                = 1 -- 普通(脱战)
CombatState.COMBAT                = 2 -- 战斗
CombatState.DYING                 = 3 -- 重伤

--(战斗)技能结果类型
local MeleeResultType             = enum("MeleeResultType", 0)
MeleeResultType.DAMAGE            = 1 -- 普通伤害
MeleeResultType.CRITICAL          = 2 -- 暴击伤害
MeleeResultType.UNUSUAL           = 3 -- 异常伤害
MeleeResultType.RESTORE           = 4 -- 生命回复

--阵营
local Faction                     = enum("Faction", 0)
Faction.PLAYER                    = 1 -- 玩家角色
Faction.NPC                       = 2 -- NPC
Faction.FOLLOWER                  = 3 -- 跟随玩家的随从
Faction.ACTIVE_MONSTER            = 4 -- 主动型怪物
Faction.PASSIVE_MONSTER           = 5 -- 被动型怪物

--阵营关系
local FactionRelation             = enum("FactionRelation", 0)
FactionRelation.FRIENDLY          = 1 -- 友好
FactionRelation.NORMAL            = 2 -- 中立
FactionRelation.HOSTILE           = 3 -- 敌对

--邮件操作
local EmailOpt                    = enum("EmailOpt", 0)
EmailOpt.RECV                     = 1
EmailOpt.READ                     = 2
EmailOpt.TAKE                     = 3
EmailOpt.FAV                      = 4
EmailOpt.DEL                      = 5

--建筑操作
local BuildingOpt                 = enum("BuildingOpt", 0)
BuildingOpt.PLACE                 = 1
BuildingOpt.COMMIT                = 2
BuildingOpt.FINISH                = 3
BuildingOpt.UPGRADE               = 4

--技能状态(仅供Ai使用)
local SkillStatus                 = enum("SkillStatus", 0)
SkillStatus.AVAILABLE             = 1 -- 可用
SkillStatus.UNAVAILABLE           = 2 -- 不可用
SkillStatus.USE_COMPLETED         = 3 -- 释放完成

--行为同步限制标记
local ActionLimit                 = enum("ActionLimit", 0)
ActionLimit.WALK                  = 1 -- 走
ActionLimit.RUN                   = 2 -- 跑
ActionLimit.FASTRUN               = 3 -- 快跑
ActionLimit.ROLL_MOVE             = 4 -- 翻滚移动
ActionLimit.ADJUST_MOVE           = 5 -- 近战吸附移动
ActionLimit.FREE_CLIMB            = 6 -- 自由攀爬
ActionLimit.PARACHUTE             = 7 -- 滑翔
ActionLimit.FALLING               = 8 -- 下落

--重伤救治类型
local DyingCureType               = enum("DyingCureType", 0)
DyingCureType.CURE_START          = 1 --开始救治
DyingCureType.CURE_ABORT          = 2 --中止(取消)救治
DyingCureType.CURE_TRANS          = 3 --(放弃求救)立即传送回安全点

--在线状态
local OnlineState                 = enum("OnlineState", 0)
OnlineState.ONLINE                = 1
OnlineState.OFFLINE               = 2

--受击动作
local BeHitAction                 = enum("BeHitAction", 0)
BeHitAction.NONE                  = 1 -- 未破韧
BeHitAction.NORMAL                = 2 -- 普通受击
BeHitAction.REPEL                 = 3 -- 击退受击
BeHitAction.FLY                   = 4 -- 击飞受击

-- 安全验证消息类型
local SafeMsgType                 = enum("SafeMsgType", 0)
SafeMsgType.WHIS                  = 3
SafeMsgType.WORLD                 = 4
SafeMsgType.CHCOM_RMRK            = 5
SafeMsgType.SCENE                 = 8
SafeMsgType.NICK                  = 11 --昵称
SafeMsgType.CHCOM_NAME            = 12
SafeMsgType.STORE_BOX             = 39 --储物箱

--社交埋点动作
local SocialAction                = enum("SocialAction", 0)
SocialAction.APPLAY               = 1
SocialAction.AGREE                = 2
SocialAction.REJECT               = 3
SocialAction.DEL                  = 4
SocialAction.BLACK                = 5
SocialAction.UNBLACK              = 6
SocialAction.VISIT                = 7
SocialAction.LEAVE                = 8

--全局锁类型
local GlobalLockKind              = enum("GlobalLockKind", 0)
GlobalLockKind.CHCOM_JOIN         = 1 -- 商会-加入
GlobalLockKind.CHCOM_AUDIT        = 2 -- 商会-审核
GlobalLockKind.CHCOM_REPLY        = 3 -- 商会-回复邀请

---对峙行为(移动方向)
local StandoffBehavior            = enum("StandoffBehavior", 0)
StandoffBehavior.FORWARD          = 1 -- 前进
StandoffBehavior.BACK             = 2 -- 后退
StandoffBehavior.LEFT             = 3 -- 左移
StandoffBehavior.RIGHT            = 4 -- 右移
StandoffBehavior.STAY             = 5 -- 待在原地

---技能(组)类型
-- from: CCCMobile\Client_Mobile\MobileClient\u3dclient\Assets\Script\Actor\LogicModule\ActionCmpt.cs
--       `enum Pathea.ActorNs.SkillGroupType`
local SkillType                   = enum("SkillType", 0)
SkillType.Default                 = 0 -- 默认技能
SkillType.Weapon                  = 1 -- 武器
SkillType.Picjaxe                 = 2 -- 镐子
SkillType.Hatchet                 = 3 -- 斧子
SkillType.Shovel                  = 4 -- 铲子
SkillType.Collection              = 5 -- 采集
SkillType.Kick                    = 6 -- 踢
SkillType.PickUp                  = 7 -- 捡东西

--建筑产出类型
local BuildingProdType            = enum("BuildingProdType", 0)
BuildingProdType.PREDUCE          = 1 --生产类型
BuildingProdType.COLLECT          = 2 --采集类型

--生活技能作用目标
local NfSkillTarget               = enum("NfSkillTarget", 0)
NfSkillTarget.NPC                 = 1 --npc
NfSkillTarget.ROLE                = 2 --role
NfSkillTarget.BUILDING            = 3 --建筑

--行为-滑翔状态
local ParachuteState              = enum("ParachuteState", 0)
ParachuteState.None               = 0 -- 无状态
ParachuteState.Open               = 1 -- 开始开伞
ParachuteState.Stay               = 2 -- 保持状态
ParachuteState.Close              = 3 -- 关闭

--行为-自由攀爬状态
local FreeClimbState              = enum("FreeClimbState", 0)
FreeClimbState.None               = 0 -- 无状态
FreeClimbState.EnterClimb         = 1 -- 进入攀爬
FreeClimbState.ClimbStay          = 2 -- 攀爬中未移动
FreeClimbState.ClimbMove          = 3 -- 攀爬移动
FreeClimbState.ClimbJump          = 4 -- 攀爬跳
FreeClimbState.ClimbUp            = 5 -- 登顶过程
FreeClimbState.ExitClimb          = 6 -- 退出攀爬

--场景效果物体类型
local SceneEffectType             = enum("SceneEffectType", 0)
SceneEffectType.AWALYS            = 1 --持续类型
SceneEffectType.INACTION          = 2 --交互类型

--满意度奖励状态
local SatRewardState              = enum("SatRewardState", 0)
SatRewardState.ING                = 1 --未完成
SatRewardState.WAIT               = 2 --待领取
SatRewardState.DONE               = 3 --已完成

--副本类型
local CopyType                    = enum("CopyType", 0)
CopyType.TEMPORARY                = 0 -- 临时副本(InMemory)
CopyType.ONCE                     = 1 -- 一次性副本(通关后 无法再次进入)
CopyType.PERIOD                   = 2 -- 周期性副本()

--副本关卡类型
local CopyLevelType               = enum("CopyLevelType", 0)
CopyLevelType.PLOT                = 0 -- 剧情关卡
CopyLevelType.NORMAL              = 1 -- 普通关卡
CopyLevelType.BOSS                = 2 -- BOSS关卡

local PushNotifKind               = enum("PushNotifKind", 0)
PushNotifKind.SYSTEM              = 0 -- 系统
PushNotifKind.MOUDLE              = 1 -- 模块

--系统ID
local SystemId                    = enum("SystemId", 0)
SystemId.SAT                      = 137 --满意度

--兑换码状态
local CdKeyState                  = enum("CdKeyState", 0)
CdKeyState.FREE                   = 0 -- 空闲
CdKeyState.LOCK                   = 1 -- 锁定
CdKeyState.ALLOT                  = 2 -- 分配

--离线时间
local OfflineTime                 = enum("OfflineTime", 0)
OfflineTime.HOUR                  = 23 -- 小时
OfflineTime.NORMAL                = 45000 -- 正常
OfflineTime.LONG                  = 3600000 -- 长
