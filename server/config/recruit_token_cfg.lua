--recruit_token_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local recruit_token = config_mgr:get_table("recruit_token")

--导出配置内容
recruit_token:upsert({
    id=1,
    rec_token_id=102021,
    star_lv=1
})

recruit_token:upsert({
    id=2,
    rec_token_id=102022,
    star_lv=2
})

recruit_token:upsert({
    id=3,
    rec_token_id=102023,
    star_lv=3
})

recruit_token:upsert({
    id=4,
    rec_token_id=102024,
    star_lv=4
})

recruit_token:upsert({
    id=5,
    npc_id=20006,
    rec_token_id=102025
})

recruit_token:update()
