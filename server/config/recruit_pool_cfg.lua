--recruit_pool_cfg.lua
--source: 3_recruit_pool_招募表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local recruit_pool = config_mgr:get_table("recruit_pool")

--导出配置内容
recruit_pool:upsert({
    base_ten=false,
    cnt_ten=1,
    cost_item=103026,
    desc='LC_label_ui_Card_type_001_dec',
    dis_ten=5,
    free_cd=1800,
    free_cnt=1,
    id=1,
    name='LC_label_ui_Card_type_001_name',
    pool_list=2,
    show_prob=0,
    single=true
})

recruit_pool:upsert({
    base_ten=true,
    cnt_ten=1,
    cost_item=103027,
    desc='LC_label_ui_Card_type_002_dec',
    dis_ten=5,
    free_cd=10800,
    free_cnt=1,
    id=2,
    name='LC_label_ui_Card_type_002_name',
    pool_list=1,
    q_base_id=1,
    show_prob=1,
    single=true,
    special_rules={
        [1]=2009,
        [6]=2003,
        [11]=2005,
        [14]=2002,
        [16]=2008,
        [20]=2010,
        [27]=2001
    }
})

recruit_pool:update()
