--faction_relation_cfg.lua
--source: 9_阵营关系表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local faction_relation = config_mgr:get_table("faction_relation")

--导出配置内容
faction_relation:upsert({
    id=1,
    object=1,
    relation=1,
    subject=1
})

faction_relation:upsert({
    id=2,
    object=2,
    relation=1,
    subject=1
})

faction_relation:upsert({
    id=3,
    object=3,
    relation=1,
    subject=1
})

faction_relation:upsert({
    id=4,
    object=4,
    relation=3,
    subject=1
})

faction_relation:upsert({
    id=5,
    object=5,
    relation=2,
    subject=1
})

faction_relation:upsert({
    id=6,
    object=1,
    relation=1,
    subject=2
})

faction_relation:upsert({
    id=7,
    object=2,
    relation=1,
    subject=2
})

faction_relation:upsert({
    id=8,
    object=3,
    relation=1,
    subject=2
})

faction_relation:upsert({
    id=9,
    object=4,
    relation=1,
    subject=2
})

faction_relation:upsert({
    id=10,
    object=5,
    relation=1,
    subject=2
})

faction_relation:upsert({
    id=11,
    object=1,
    relation=1,
    subject=3
})

faction_relation:upsert({
    id=12,
    object=2,
    relation=1,
    subject=3
})

faction_relation:upsert({
    id=13,
    object=3,
    relation=1,
    subject=3
})

faction_relation:upsert({
    id=14,
    object=4,
    relation=2,
    subject=3
})

faction_relation:upsert({
    id=15,
    object=5,
    relation=2,
    subject=3
})

faction_relation:upsert({
    id=16,
    object=1,
    relation=3,
    subject=4
})

faction_relation:upsert({
    id=17,
    object=2,
    relation=1,
    subject=4
})

faction_relation:upsert({
    id=18,
    object=3,
    relation=2,
    subject=4
})

faction_relation:upsert({
    id=19,
    object=4,
    relation=1,
    subject=4
})

faction_relation:upsert({
    id=20,
    object=5,
    relation=1,
    subject=4
})

faction_relation:upsert({
    id=21,
    object=1,
    relation=2,
    subject=5
})

faction_relation:upsert({
    id=22,
    object=2,
    relation=1,
    subject=5
})

faction_relation:upsert({
    id=23,
    object=3,
    relation=2,
    subject=5
})

faction_relation:upsert({
    id=24,
    object=4,
    relation=1,
    subject=5
})

faction_relation:upsert({
    id=25,
    object=5,
    relation=1,
    subject=5
})

faction_relation:update()
