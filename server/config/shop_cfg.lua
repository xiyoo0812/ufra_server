--shop_cfg.lua
--source: 13_shop_游戏商店表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local shop = config_mgr:get_table("shop")

--导出配置内容
shop:upsert({
    close=22,
    global=false,
    id=100101,
    open=8,
    petty =0,
    refresh_cost=1,
    refresh_count=10,
    refresh_items={
        [100001]=1000
    },
    refresh_time=240,
    refresh_type=1,
    shop_id=1001,
    shop_level=1,
    ui_open=false,
    unlock=true
})

shop:upsert({
    close=22,
    global=false,
    id=100102,
    open=8,
    petty =0,
    refresh_cost=1,
    refresh_count=10,
    refresh_items={
        [100001]=3000
    },
    refresh_time=360,
    refresh_type=1,
    shop_id=1001,
    shop_level=2,
    ui_open=false,
    unlock=true
})

shop:upsert({
    close=22,
    global=false,
    id=100103,
    open=8,
    petty =0,
    refresh_cost=1,
    refresh_count=10,
    refresh_items={
        [100001]=5000
    },
    refresh_time=480,
    refresh_type=1,
    shop_id=1001,
    shop_level=3,
    ui_open=false,
    unlock=true
})

shop:upsert({
    close=22,
    global=false,
    id=100201,
    open=8,
    petty =0,
    refresh_cost=1,
    refresh_count=10,
    refresh_items={
        [100001]=1000
    },
    refresh_time=480,
    refresh_type=1,
    shop_id=1002,
    shop_level=1,
    ui_open=false,
    unlock=true
})

shop:upsert({
    close=22,
    global=false,
    id=100301,
    open=8,
    petty =0,
    refresh_cost=1,
    refresh_count=10,
    refresh_items={
        [100001]=1000
    },
    refresh_time=480,
    refresh_type=1,
    shop_id=1003,
    shop_level=1,
    ui_open=false,
    unlock=true
})

shop:upsert({
    close=22,
    global=false,
    id=100401,
    open=8,
    petty =0,
    refresh_cost=1,
    refresh_count=10,
    refresh_items={
        [100001]=1000
    },
    refresh_time=480,
    refresh_type=1,
    shop_id=1004,
    shop_level=1,
    ui_open=false,
    unlock=true
})

shop:upsert({
    close=22,
    global=false,
    id=100501,
    open=8,
    petty =0,
    refresh_cost=2,
    refresh_count=10,
    refresh_items={
        [100001]=1000
    },
    refresh_time=480,
    refresh_type=1,
    shop_id=1005,
    shop_level=1,
    ui_open=false,
    unlock=true
})

shop:upsert({
    close=22,
    global=false,
    id=100601,
    open=8,
    petty =0,
    refresh_cost=2,
    refresh_count=10,
    refresh_items={
        [100001]=1000
    },
    refresh_time=480,
    refresh_type=1,
    shop_id=1006,
    shop_level=1,
    ui_open=false,
    unlock=true
})

shop:upsert({
    close=22,
    global=false,
    id=100701,
    open=8,
    petty =0,
    refresh_cost=2,
    refresh_count=10,
    refresh_items={
        [100001]=1000
    },
    refresh_time=480,
    refresh_type=1,
    shop_id=1007,
    shop_level=1,
    ui_open=false,
    unlock=true
})

shop:upsert({
    close=22,
    global=false,
    id=100801,
    open=8,
    petty =0,
    refresh_cost=2,
    refresh_count=10,
    refresh_items={
        [100001]=1000
    },
    refresh_time=480,
    refresh_type=1,
    shop_id=1008,
    shop_level=1,
    ui_open=false,
    unlock=true
})

shop:upsert({
    close=22,
    global=false,
    id=100901,
    open=8,
    petty =0,
    refresh_cost=2,
    refresh_count=10,
    refresh_items={
        [100001]=1000
    },
    refresh_time=480,
    refresh_type=1,
    shop_id=1009,
    shop_level=1,
    ui_open=false,
    unlock=true
})

shop:upsert({
    close=22,
    global=false,
    id=101001,
    open=8,
    petty =0,
    refresh_cost=2,
    refresh_count=10,
    refresh_items={
        [100001]=1000
    },
    refresh_time=480,
    refresh_type=1,
    shop_id=1010,
    shop_level=1,
    ui_open=false,
    unlock=true
})

shop:upsert({
    close=22,
    global=false,
    id=200101,
    open=8,
    petty =0,
    refresh_clock={
        1200,
        2100
    },
    refresh_cost=2,
    refresh_count=10,
    refresh_items={
        [100001]=1000
    },
    refresh_type=1,
    shop_id=2001,
    shop_level=1,
    ui_open=true,
    unlock=true
})

shop:upsert({
    close=22,
    global=false,
    id=200201,
    open=8,
    petty =0,
    refresh_clock={
        1200,
        2100
    },
    refresh_cost=2,
    refresh_count=10,
    refresh_items={
        [100001]=1000
    },
    refresh_type=1,
    shop_id=2002,
    shop_level=1,
    ui_open=true,
    unlock=true
})

shop:upsert({
    close=22,
    global=false,
    id=300101,
    open=8,
    petty =0,
    refresh_cost=1,
    refresh_count=10,
    refresh_items={
        [100001]=20
    },
    refresh_time=480,
    refresh_type=1,
    shop_id=3001,
    shop_level=1,
    ui_open=false,
    unlock=false
})

shop:update()
