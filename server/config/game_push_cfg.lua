--game_push_cfg.lua
--source: 17-推送通知配置表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local game_push = config_mgr:get_table("game_push")

--导出配置内容
game_push:upsert({
    id=1,
    level=100,
    template='LC_label_ui_Game_pushbell_1'
})

game_push:upsert({
    id=2,
    level=99,
    template='LC_label_ui_Game_pushbell_2'
})

game_push:update()
