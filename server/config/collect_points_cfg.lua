--collect_points_cfg.lua
--source: 7_collect_points_采集点表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local collect_points = config_mgr:get_table("collect_points")

--导出配置内容
collect_points:upsert({
    formulas={
        1001,
        1011
    },
    id=1001
})

collect_points:upsert({
    area_id=1002,
    formulas={
        1014,
        1002
    },
    id=1002
})

collect_points:upsert({
    formulas={
        1003,
        1013
    },
    id=1003
})

collect_points:upsert({
    area_id=1003,
    formulas={
        1004
    },
    id=1004
})

collect_points:upsert({
    formulas={
        1005,
        1012
    },
    id=1005
})

collect_points:upsert({
    area_id=1001,
    formulas={
        1006,
        1015
    },
    id=1006
})

collect_points:upsert({
    area_id=1001,
    formulas={
        1007
    },
    id=1007
})

collect_points:upsert({
    formulas={
        1008
    },
    id=1008
})

collect_points:upsert({
    area_id=1000,
    formulas={
        1009
    },
    id=1009
})

collect_points:upsert({
    area_id=1000,
    formulas={
        1010
    },
    id=1010
})

collect_points:update()
