--condition_cfg.lua
--source: 5_condition_任务条件定义表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local condition = config_mgr:get_table("condition")

--导出配置内容
condition:upsert({
    enum_key='LEVEL',
    id=1,
    name='等级'
})

condition:upsert({
    enum_key='BUILDING',
    id=2,
    name='建筑'
})

condition:upsert({
    enum_key='TOWN',
    id=3,
    name='城镇等级'
})

condition:upsert({
    enum_key='TASK',
    id=4,
    name='任务'
})

condition:upsert({
    enum_key='NPC',
    id=5,
    name='NPC'
})

condition:upsert({
    enum_key='CHAIN',
    id=6,
    name='任务链'
})

condition:upsert({
    enum_key='ASSIGN',
    id=7,
    name='派驻'
})

condition:upsert({
    enum_key='ASSIGN_GROUP',
    id=8,
    name='派驻npc到某个建筑组'
})

condition:upsert({
    enum_key='NPC_LIKING',
    id=9,
    name='npc好感等级'
})

condition:upsert({
    enum_key='FINISH_TARGET',
    id=10,
    name='完成目标'
})

condition:upsert({
    enum_key='NPC_COUNT',
    id=11,
    name='npc数量'
})

condition:upsert({
    enum_key='RESIDENT_NUM',
    id=12,
    name='居民数量'
})

condition:upsert({
    enum_key='GROUP_COUNT',
    id=13,
    name='建筑组数量'
})

condition:update()
