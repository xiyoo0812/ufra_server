--sdk_errcode_cfg.lua
--source: sdk_errcode.xlsm
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local sdk_errcode = config_mgr:get_table("sdk_errcode")

--导出配置内容
sdk_errcode:upsert({
    channel=3,
    game_errcode=10002,
    id=1,
    sdk_errcode=-1001
})

sdk_errcode:upsert({
    channel=3,
    game_errcode=10003,
    id=2,
    sdk_errcode=-1002
})

sdk_errcode:upsert({
    channel=3,
    game_errcode=10004,
    id=3,
    sdk_errcode=-1003
})

sdk_errcode:upsert({
    channel=3,
    game_errcode=10005,
    id=4,
    sdk_errcode=-1011
})

sdk_errcode:upsert({
    channel=3,
    game_errcode=10006,
    id=5,
    sdk_errcode=-1014
})

sdk_errcode:upsert({
    channel=3,
    game_errcode=10007,
    id=6,
    sdk_errcode=-1015
})

sdk_errcode:upsert({
    channel=3,
    game_errcode=10008,
    id=7,
    sdk_errcode=-5000
})

sdk_errcode:upsert({
    channel=3,
    game_errcode=10009,
    id=8,
    sdk_errcode=-6001
})

sdk_errcode:upsert({
    channel=3,
    game_errcode=10010,
    id=9,
    sdk_errcode=-6002
})

sdk_errcode:upsert({
    channel=3,
    game_errcode=10011,
    id=10,
    sdk_errcode=-6003
})

sdk_errcode:upsert({
    channel=3,
    game_errcode=10012,
    id=11,
    sdk_errcode=-6004
})

sdk_errcode:upsert({
    channel=3,
    game_errcode=10013,
    id=12,
    sdk_errcode=-6005
})

sdk_errcode:upsert({
    channel=3,
    game_errcode=10014,
    id=13,
    sdk_errcode=-6006
})

sdk_errcode:upsert({
    channel=3,
    game_errcode=10015,
    id=14,
    sdk_errcode=-6007
})

sdk_errcode:upsert({
    channel=3,
    game_errcode=10016,
    id=15,
    sdk_errcode=-6008
})

sdk_errcode:upsert({
    channel=3,
    game_errcode=10017,
    id=16,
    sdk_errcode=-6009
})

sdk_errcode:upsert({
    channel=3,
    game_errcode=10018,
    id=17,
    sdk_errcode=-6010
})

sdk_errcode:upsert({
    channel=3,
    game_errcode=10019,
    id=18,
    sdk_errcode=-6011
})

sdk_errcode:upsert({
    channel=3,
    game_errcode=10020,
    id=19,
    sdk_errcode=-6012
})

sdk_errcode:upsert({
    channel=3,
    game_errcode=10021,
    id=20,
    sdk_errcode=-6013
})

sdk_errcode:upsert({
    channel=2,
    game_errcode=10002,
    id=21,
    sdk_errcode=10002
})

sdk_errcode:upsert({
    channel=2,
    game_errcode=10003,
    id=22,
    sdk_errcode=10003
})

sdk_errcode:upsert({
    channel=2,
    game_errcode=10004,
    id=23,
    sdk_errcode=10004
})

sdk_errcode:upsert({
    channel=2,
    game_errcode=10005,
    id=24,
    sdk_errcode=10005
})

sdk_errcode:upsert({
    channel=2,
    game_errcode=10006,
    id=25,
    sdk_errcode=10006
})

sdk_errcode:upsert({
    channel=2,
    game_errcode=10007,
    id=26,
    sdk_errcode=10007
})

sdk_errcode:upsert({
    channel=2,
    game_errcode=10008,
    id=27,
    sdk_errcode=10008
})

sdk_errcode:upsert({
    channel=2,
    game_errcode=10009,
    id=28,
    sdk_errcode=10009
})

sdk_errcode:upsert({
    channel=2,
    game_errcode=10010,
    id=29,
    sdk_errcode=10010
})

sdk_errcode:upsert({
    channel=2,
    game_errcode=10011,
    id=30,
    sdk_errcode=10011
})

sdk_errcode:upsert({
    channel=2,
    game_errcode=10012,
    id=31,
    sdk_errcode=10012
})

sdk_errcode:upsert({
    channel=2,
    game_errcode=10013,
    id=32,
    sdk_errcode=10013
})

sdk_errcode:upsert({
    channel=2,
    game_errcode=10014,
    id=33,
    sdk_errcode=10014
})

sdk_errcode:upsert({
    channel=2,
    game_errcode=10015,
    id=34,
    sdk_errcode=10015
})

sdk_errcode:upsert({
    channel=2,
    game_errcode=10016,
    id=35,
    sdk_errcode=10016
})

sdk_errcode:upsert({
    channel=2,
    game_errcode=10017,
    id=36,
    sdk_errcode=10017
})

sdk_errcode:upsert({
    channel=2,
    game_errcode=10018,
    id=37,
    sdk_errcode=10018
})

sdk_errcode:upsert({
    channel=2,
    game_errcode=10019,
    id=38,
    sdk_errcode=10019
})

sdk_errcode:upsert({
    channel=2,
    game_errcode=10020,
    id=39,
    sdk_errcode=10020
})

sdk_errcode:upsert({
    channel=2,
    game_errcode=10021,
    id=40,
    sdk_errcode=10021
})

sdk_errcode:upsert({
    channel=3,
    game_errcode=5,
    id=41,
    sdk_errcode=-4005
})

sdk_errcode:upsert({
    channel=3,
    game_errcode=5,
    id=42,
    sdk_errcode=400
})

sdk_errcode:upsert({
    channel=3,
    game_errcode=5,
    id=43,
    sdk_errcode=401
})

sdk_errcode:upsert({
    channel=3,
    game_errcode=5,
    id=44,
    sdk_errcode=500
})

sdk_errcode:upsert({
    channel=3,
    game_errcode=5,
    id=45,
    sdk_errcode=-4001
})

sdk_errcode:upsert({
    channel=3,
    game_errcode=2108,
    id=46,
    sdk_errcode=-4002
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=10002,
    id=47,
    sdk_errcode=10002
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=10003,
    id=48,
    sdk_errcode=10003
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=10004,
    id=49,
    sdk_errcode=10004
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=10005,
    id=50,
    sdk_errcode=10005
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=10006,
    id=51,
    sdk_errcode=10006
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=10007,
    id=52,
    sdk_errcode=10007
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=10008,
    id=53,
    sdk_errcode=10008
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=10009,
    id=54,
    sdk_errcode=10009
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=10010,
    id=55,
    sdk_errcode=10010
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=10011,
    id=56,
    sdk_errcode=10011
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=10012,
    id=57,
    sdk_errcode=10012
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=10013,
    id=58,
    sdk_errcode=10013
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=10014,
    id=59,
    sdk_errcode=10014
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=10015,
    id=60,
    sdk_errcode=10015
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=10016,
    id=61,
    sdk_errcode=10016
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=10017,
    id=62,
    sdk_errcode=10017
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=10018,
    id=63,
    sdk_errcode=10018
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=10019,
    id=64,
    sdk_errcode=10019
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=10020,
    id=65,
    sdk_errcode=10020
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=10021,
    id=66,
    sdk_errcode=10021
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=5,
    id=67,
    sdk_errcode=5
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=5,
    id=68,
    sdk_errcode=5
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=5,
    id=69,
    sdk_errcode=5
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=5,
    id=70,
    sdk_errcode=5
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=5,
    id=71,
    sdk_errcode=5
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=2108,
    id=72,
    sdk_errcode=2108
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=10022,
    id=73,
    sdk_errcode=10022
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=10023,
    id=74,
    sdk_errcode=10023
})

sdk_errcode:upsert({
    channel=1,
    game_errcode=10024,
    id=75,
    sdk_errcode=10024
})

sdk_errcode:update()
