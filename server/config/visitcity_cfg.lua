--visitcity_cfg.lua
--source: 7_visitcity_visitcity地图信息表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local visitcity = config_mgr:get_table("visitcity")

--导出配置内容
visitcity:upsert({
    dynamic=true,
    id=10,
    name='桦树-1',
    pos={
        -2310,
        1,
        -2730
    },
    proto_id=20033,
    refresh_time=5,
    type=4
})

visitcity:update()
