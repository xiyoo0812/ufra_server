--map_trans_cfg.lua
--source: 7_map_trans_场景传送表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local map_trans = config_mgr:get_table("map_trans")

--导出配置内容
map_trans:upsert({
    id=1005001,
    index=1,
    tar_dir={
        0,
        9316,
        0
    },
    tar_map=1005,
    tar_pos={
        -2571,
        1893,
        207
    }
})

map_trans:upsert({
    id=1012001,
    index=1,
    tar_dir={
        0,
        30141,
        0
    },
    tar_map=1012,
    tar_pos={
        7835,
        3991,
        8888
    }
})

map_trans:update()
