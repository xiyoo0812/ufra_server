--teleport_cfg.lua
--source: 7_teleport_传送点表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local teleport = config_mgr:get_table("teleport")

--导出配置内容
teleport:upsert({
    id=1,
    tar_map_id=1003,
    tar_pos={
        68711,
        -14521,
        97442
    }
})

teleport:upsert({
    id=2,
    tar_map_id=1003,
    tar_pos={
        147443,
        11314,
        73702
    }
})

teleport:upsert({
    id=3,
    tar_map_id=1003,
    tar_pos={
        -2300,
        -499000,
        -450
    },
    tar_subscene='Minecaves_1_In'
})

teleport:upsert({
    id=4,
    tar_map_id=1003,
    tar_pos={
        43690,
        23535,
        -34314
    },
    tar_subscene='Minecaves_1_Out'
})

teleport:upsert({
    id=5,
    tar_map_id=1003,
    tar_pos={
        27552,
        85812,
        3934
    },
    tar_subscene='TentInside'
})

teleport:upsert({
    id=6,
    tar_map_id=1003,
    tar_pos={
        131308,
        12459,
        83328
    }
})

teleport:upsert({
    id=7,
    tar_map_id=1008,
    tar_pos={
        -284,
        4,
        148
    },
    tar_subscene='TentInside'
})

teleport:upsert({
    id=8,
    tar_map_id=1003,
    tar_pos={
        5653,
        85879,
        4150
    },
    tar_subscene='RoomTest1'
})

teleport:upsert({
    id=9,
    tar_map_id=1003,
    tar_pos={
        16612,
        85879,
        4364
    },
    tar_subscene='RoomTest2'
})

teleport:update()
