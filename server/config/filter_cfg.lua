--filter_cfg.lua
--source: filter.xlsm
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local filter = config_mgr:get_table("filter")

--导出配置内容
filter:upsert({
    id=1,
    log=true,
    name='NID_ACTOR_ACTION_REQ'
})

filter:upsert({
    id=2,
    log=true,
    name='NID_ACTOR_ACTION_NTF'
})

filter:upsert({
    id=3,
    log=true,
    name='NID_BUILDING_UTENSIL_NTF'
})

filter:upsert({
    id=4,
    log=true,
    name='NID_UTILITY_BILOG_NTF'
})

filter:upsert({
    id=5,
    log=true,
    name='NID_UTILITY_SYNC_REDDOT_NTF'
})

filter:upsert({
    id=6,
    log=true,
    name='NID_UTILITY_NEWBEE_SYNC_NTF'
})

filter:upsert({
    id=7,
    name='NID_LOGIN_ROLE_LOGIN_REQ',
    proto=true
})

filter:upsert({
    id=8,
    name='NID_PRODUCE_PKBSET_REQ',
    proto=true
})

filter:upsert({
    id=9,
    name='NID_PRODUCE_PKBCMT_REQ',
    proto=true
})

filter:upsert({
    id=10,
    name='NID_PRODUCE_PKBCAN_REQ',
    proto=true
})

filter:upsert({
    id=11,
    name='NID_PRODUCE_PKBGET_REQ',
    proto=true
})

filter:upsert({
    id=12,
    name='NID_BUILDING_PLACE_REQ',
    proto=true
})

filter:upsert({
    id=13,
    name='NID_BUILDING_FINISH_REQ',
    proto=true
})

filter:upsert({
    id=14,
    name='NID_BUILDING_UNLOCK_REQ',
    proto=true
})

filter:upsert({
    id=15,
    name='NID_BLOCK_ACCEL_REQ',
    proto=true
})

filter:upsert({
    id=16,
    name='NID_BUILDING_UPGRADE_REQ',
    proto=true
})

filter:upsert({
    id=17,
    name='NID_BUILDING_SPEEDUP_REQ',
    proto=true
})

filter:upsert({
    id=18,
    name='NID_BLOCK_UNLOCK_REQ',
    proto=true
})

filter:upsert({
    id=19,
    name='NID_BLOCK_FINISH_REQ',
    proto=true
})

filter:upsert({
    id=20,
    name='NID_BUILDING_TAKEBACK_REQ',
    proto=true
})

filter:upsert({
    id=21,
    name='NID_BUILDING_ITEM_RECYCLE_REQ',
    proto=true
})

filter:upsert({
    id=22,
    name='NID_BUILDING_CHECKIN_REQ',
    proto=true
})

filter:upsert({
    id=23,
    name='NID_BUILDING_HOUSENAME_REQ',
    proto=true
})

filter:upsert({
    id=24,
    name='NID_BUILDING_CHECKOUT_REQ',
    proto=true
})

filter:upsert({
    id=25,
    name='NID_BUILDING_CHANGEHOUSE_REQ',
    proto=true
})

filter:upsert({
    id=26,
    name='NID_PRODUCE_MAKE_REQ',
    proto=true
})

filter:upsert({
    id=27,
    name='NID_PRODUCE_CANCEL_REQ',
    proto=true
})

filter:upsert({
    id=28,
    name='NID_PRODUCE_RECEIVE_REQ',
    proto=true
})

filter:upsert({
    id=29,
    name='NID_PRODUCE_UNQUEUE_REQ',
    proto=true
})

filter:upsert({
    id=30,
    name='NID_PRODUCE_SORT_REQ',
    proto=true
})

filter:upsert({
    id=31,
    name='NID_PRODUCE_SPEEDUP_REQ',
    proto=true
})

filter:upsert({
    id=32,
    name='NID_PRODUCE_UNDELIVE_REQ',
    proto=true
})

filter:upsert({
    id=33,
    name='NID_PRODUCE_SWDELIVE_REQ',
    proto=true
})

filter:upsert({
    id=34,
    name='NID_PRODUCE_GATHER_REQ',
    proto=true
})

filter:upsert({
    id=35,
    name='NID_PARTNER_GIFT_REQ',
    proto=true
})

filter:upsert({
    id=36,
    name='NID_PARTNER_UP_STAR_REQ',
    proto=true
})

filter:upsert({
    id=37,
    name='NID_PARTNER_UP_LV_REQ',
    proto=true
})

filter:upsert({
    id=38,
    name='NID_PARTNER_UN_LV_REQ',
    proto=true
})

filter:upsert({
    id=39,
    name='NID_RECR_START_REQ',
    proto=true
})

filter:upsert({
    id=40,
    name='NID_TOWN_SAT_REWARD_REQ',
    proto=true
})

filter:upsert({
    id=41,
    name='NID_TOWN_CHAIRMAN_REWARD_REQ',
    proto=true
})

filter:upsert({
    id=42,
    name='NID_SKILL_GATHER_REQ',
    proto=true
})

filter:upsert({
    id=43,
    name='NID_MAIL_TAKE_REQ',
    proto=true
})

filter:upsert({
    id=44,
    name='NID_PACKET_ITEM_USE_REQ',
    proto=true
})

filter:upsert({
    id=45,
    name='NID_UTILITY_GM_COMMAND_REQ',
    proto=true
})

filter:upsert({
    id=46,
    name='NID_ACTIVITY_SURVEY_SUBMIT_REQ',
    proto=true
})

filter:update()
