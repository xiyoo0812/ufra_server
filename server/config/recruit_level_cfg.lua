--recruit_level_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local recruit_level = config_mgr:get_table("recruit_level")

--导出配置内容
recruit_level:upsert({
    accel_cost={
        [100001]=500
    },
    costs={
        [100001]=160
    },
    daily_cost={
        [100001]=100
    },
    daily_recr_time=3600,
    inter_size=4,
    level=1,
    recr_time=14400
})

recruit_level:upsert({
    accel_cost={
        [100001]=500
    },
    costs={
        [100001]=160
    },
    daily_cost={
        [100001]=100
    },
    daily_recr_time=3600,
    inter_size=6,
    level=2,
    recr_time=14400
})

recruit_level:upsert({
    accel_cost={
        [100001]=500
    },
    costs={
        [100001]=160
    },
    daily_cost={
        [100001]=100
    },
    daily_recr_time=3600,
    inter_size=8,
    level=3,
    recr_time=14400
})

recruit_level:upsert({
    accel_cost={
        [100001]=500
    },
    costs={
        [100001]=160
    },
    daily_cost={
        [100001]=100
    },
    daily_recr_time=3600,
    inter_size=10,
    level=4,
    recr_time=14400
})

recruit_level:update()
