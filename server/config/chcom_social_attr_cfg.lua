--chcom_social_attr_cfg.lua
--source: attribute.xlsm
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local chcom_social_attr = config_mgr:get_table("chcom_social_attr")

--导出配置内容
chcom_social_attr:upsert({
    back=false,
    key='ATTR_NAME',
    range=0,
    save=false
})

chcom_social_attr:upsert({
    back=false,
    key='ATTR_AVATAR',
    range=0,
    save=false
})

chcom_social_attr:upsert({
    back=false,
    key='ATTR_AVTFRM',
    range=0,
    save=false
})

chcom_social_attr:upsert({
    back=false,
    key='ATTR_GENDER',
    range=0,
    save=false
})

chcom_social_attr:upsert({
    back=false,
    key='ATTR_LEVEL',
    range=0,
    save=false
})

chcom_social_attr:upsert({
    back=false,
    key='ATTR_CHCOM_ID',
    range=0,
    save=false
})

chcom_social_attr:update()
