--activity_progress_cfg.lua
--source: 12_activity_attribute_属性活动进度.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local activity_progress = config_mgr:get_table("activity_progress")

--导出配置内容
activity_progress:upsert({
    activity_id=1030,
    append_rewards={},
    attr_id=121,
    attr_name='ATTR_DAY_ACTY',
    child_id=1,
    id=1,
    rewards={
        [100101]=1
    },
    value=10000
})

activity_progress:upsert({
    activity_id=1030,
    append_rewards={},
    attr_id=121,
    attr_name='ATTR_DAY_ACTY',
    child_id=2,
    id=2,
    rewards={
        [100101]=2
    },
    value=20000
})

activity_progress:upsert({
    activity_id=1030,
    append_rewards={},
    attr_id=121,
    attr_name='ATTR_DAY_ACTY',
    child_id=3,
    id=3,
    rewards={
        [100101]=3
    },
    value=30000
})

activity_progress:upsert({
    activity_id=1030,
    append_rewards={},
    attr_id=121,
    attr_name='ATTR_DAY_ACTY',
    child_id=4,
    id=4,
    rewards={
        [100101]=4
    },
    value=40000
})

activity_progress:upsert({
    activity_id=1031,
    append_rewards={},
    attr_id=122,
    attr_name='ATTR_WEEK_ACTY',
    child_id=1,
    id=5,
    rewards={
        [103020]=1
    },
    value=10000
})

activity_progress:upsert({
    activity_id=1031,
    append_rewards={},
    attr_id=122,
    attr_name='ATTR_WEEK_ACTY',
    child_id=2,
    id=6,
    rewards={
        [100001]=500
    },
    value=20000
})

activity_progress:upsert({
    activity_id=1031,
    append_rewards={},
    attr_id=122,
    attr_name='ATTR_WEEK_ACTY',
    child_id=3,
    id=7,
    rewards={
        [103020]=2
    },
    value=30000
})

activity_progress:upsert({
    activity_id=1031,
    append_rewards={},
    attr_id=122,
    attr_name='ATTR_WEEK_ACTY',
    child_id=4,
    id=8,
    rewards={
        [100001]=2000
    },
    value=40000
})

activity_progress:update()
