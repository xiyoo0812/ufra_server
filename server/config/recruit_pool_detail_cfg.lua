--recruit_pool_detail_cfg.lua
--source: 3_recruit_pool_招募表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local recruit_pool_detail = config_mgr:get_table("recruit_pool_detail")

--导出配置内容
recruit_pool_detail:upsert({
    id=1001,
    item_id=103024,
    item_type=2,
    pool_id=1,
    weight=1800
})

recruit_pool_detail:upsert({
    id=1002,
    item_id=103025,
    item_type=2,
    pool_id=1,
    weight=700
})

recruit_pool_detail:upsert({
    id=1007,
    item_id=103027,
    item_type=2,
    pool_id=1,
    weight=800
})

recruit_pool_detail:upsert({
    id=1008,
    item_id=103030,
    item_type=2,
    pool_id=1,
    weight=2500
})

recruit_pool_detail:upsert({
    id=1010,
    item_id=130002,
    item_type=2,
    pool_id=1,
    weight=1000
})

recruit_pool_detail:upsert({
    id=1019,
    item_id=101095,
    item_type=2,
    pool_id=1,
    weight=200
})

recruit_pool_detail:upsert({
    id=1020,
    item_id=101096,
    item_type=2,
    pool_id=1,
    weight=200
})

recruit_pool_detail:upsert({
    id=1021,
    item_id=101097,
    item_type=2,
    pool_id=1,
    weight=200
})

recruit_pool_detail:upsert({
    id=1022,
    item_id=101099,
    item_type=2,
    pool_id=1,
    weight=400
})

recruit_pool_detail:upsert({
    id=1023,
    item_id=101100,
    item_type=2,
    pool_id=1,
    weight=400
})

recruit_pool_detail:upsert({
    id=1024,
    item_id=101101,
    item_type=2,
    pool_id=1,
    weight=200
})

recruit_pool_detail:upsert({
    id=1025,
    item_id=101103,
    item_type=2,
    pool_id=1,
    weight=200
})

recruit_pool_detail:upsert({
    id=1026,
    item_id=101106,
    item_type=2,
    pool_id=1,
    weight=200
})

recruit_pool_detail:upsert({
    id=1027,
    item_id=101107,
    item_type=2,
    pool_id=1,
    weight=200
})

recruit_pool_detail:upsert({
    id=1028,
    item_id=101108,
    item_type=2,
    pool_id=1,
    weight=200
})

recruit_pool_detail:upsert({
    id=1029,
    item_id=101110,
    item_type=2,
    pool_id=1,
    weight=200
})

recruit_pool_detail:upsert({
    id=1030,
    item_id=101113,
    item_type=2,
    pool_id=1,
    weight=200
})

recruit_pool_detail:upsert({
    id=1031,
    item_id=101114,
    item_type=2,
    pool_id=1,
    weight=200
})

recruit_pool_detail:upsert({
    id=1032,
    item_id=101117,
    item_type=2,
    pool_id=1,
    weight=200
})

recruit_pool_detail:upsert({
    id=2001,
    item_id=20038,
    item_type=1,
    pool_id=2,
    weight=130
})

recruit_pool_detail:upsert({
    id=2002,
    item_id=20044,
    item_type=1,
    pool_id=2,
    weight=130
})

recruit_pool_detail:upsert({
    id=2003,
    item_id=20009,
    item_type=1,
    pool_id=2,
    weight=280
})

recruit_pool_detail:upsert({
    id=2004,
    item_id=20010,
    item_type=1,
    pool_id=2,
    weight=130
})

recruit_pool_detail:upsert({
    id=2005,
    item_id=20002,
    item_type=1,
    pool_id=2,
    weight=280
})

recruit_pool_detail:upsert({
    id=2006,
    item_id=20003,
    item_type=1,
    pool_id=2,
    weight=280
})

recruit_pool_detail:upsert({
    id=2007,
    item_id=20011,
    item_type=1,
    pool_id=2,
    weight=280
})

recruit_pool_detail:upsert({
    id=2008,
    item_id=20007,
    item_type=1,
    pool_id=2,
    weight=280
})

recruit_pool_detail:upsert({
    id=2009,
    item_id=20055,
    item_type=1,
    pool_id=2,
    weight=130
})

recruit_pool_detail:upsert({
    id=2010,
    item_id=20034,
    item_type=1,
    pool_id=2,
    weight=280
})

recruit_pool_detail:upsert({
    id=2011,
    item_id=20001,
    item_type=1,
    pool_id=2,
    weight=390
})

recruit_pool_detail:upsert({
    id=2012,
    item_id=20004,
    item_type=1,
    pool_id=2,
    weight=390
})

recruit_pool_detail:upsert({
    id=2013,
    item_id=20008,
    item_type=1,
    pool_id=2,
    weight=390
})

recruit_pool_detail:upsert({
    id=2014,
    item_id=20039,
    item_type=1,
    pool_id=2,
    weight=390
})

recruit_pool_detail:upsert({
    id=2015,
    item_id=20040,
    item_type=1,
    pool_id=2,
    weight=390
})

recruit_pool_detail:upsert({
    id=2016,
    item_id=20018,
    item_type=1,
    pool_id=2,
    weight=390
})

recruit_pool_detail:upsert({
    id=2017,
    item_id=20019,
    item_type=1,
    pool_id=2,
    weight=390
})

recruit_pool_detail:upsert({
    id=2018,
    item_id=20015,
    item_type=1,
    pool_id=2,
    weight=390
})

recruit_pool_detail:upsert({
    id=2019,
    item_id=20017,
    item_type=1,
    pool_id=2,
    weight=390
})

recruit_pool_detail:upsert({
    id=2020,
    item_id=20014,
    item_type=1,
    pool_id=2,
    weight=390
})

recruit_pool_detail:upsert({
    id=2021,
    item_id=20016,
    item_type=1,
    pool_id=2,
    weight=390
})

recruit_pool_detail:upsert({
    id=2022,
    item_id=20048,
    item_type=1,
    pool_id=2,
    weight=390
})

recruit_pool_detail:upsert({
    id=2023,
    item_id=20022,
    item_type=1,
    pool_id=2,
    weight=390
})

recruit_pool_detail:upsert({
    id=2024,
    item_id=20023,
    item_type=1,
    pool_id=2,
    weight=390
})

recruit_pool_detail:upsert({
    id=2025,
    item_id=20026,
    item_type=1,
    pool_id=2,
    weight=390
})

recruit_pool_detail:upsert({
    id=2026,
    item_id=20027,
    item_type=1,
    pool_id=2,
    weight=390
})

recruit_pool_detail:upsert({
    id=2027,
    item_id=20025,
    item_type=1,
    pool_id=2,
    weight=390
})

recruit_pool_detail:upsert({
    id=2028,
    item_id=20029,
    item_type=1,
    pool_id=2,
    weight=390
})

recruit_pool_detail:upsert({
    id=2029,
    item_id=20031,
    item_type=1,
    pool_id=2,
    weight=390
})

recruit_pool_detail:upsert({
    id=2030,
    item_id=20032,
    item_type=1,
    pool_id=2,
    weight=390
})

recruit_pool_detail:upsert({
    id=3001,
    item_id=20038,
    item_type=1,
    pool_id=3,
    weight=80
})

recruit_pool_detail:upsert({
    id=3002,
    item_id=20044,
    item_type=1,
    pool_id=3,
    weight=40
})

recruit_pool_detail:upsert({
    id=3003,
    item_id=20009,
    item_type=1,
    pool_id=3,
    weight=300
})

recruit_pool_detail:upsert({
    id=3004,
    item_id=20010,
    item_type=1,
    pool_id=3,
    weight=40
})

recruit_pool_detail:upsert({
    id=3005,
    item_id=20002,
    item_type=1,
    pool_id=3,
    weight=300
})

recruit_pool_detail:upsert({
    id=3006,
    item_id=20003,
    item_type=1,
    pool_id=3,
    weight=300
})

recruit_pool_detail:upsert({
    id=3007,
    item_id=20011,
    item_type=1,
    pool_id=3,
    weight=300
})

recruit_pool_detail:upsert({
    id=3008,
    item_id=20007,
    item_type=1,
    pool_id=3,
    weight=300
})

recruit_pool_detail:upsert({
    id=3009,
    item_id=20055,
    item_type=1,
    pool_id=3,
    weight=40
})

recruit_pool_detail:upsert({
    id=3010,
    item_id=20034,
    item_type=1,
    pool_id=3,
    weight=300
})

recruit_pool_detail:upsert({
    id=3011,
    item_id=20001,
    item_type=1,
    pool_id=3,
    weight=800
})

recruit_pool_detail:upsert({
    id=3012,
    item_id=20008,
    item_type=1,
    pool_id=3,
    weight=800
})

recruit_pool_detail:upsert({
    id=3013,
    item_id=20040,
    item_type=1,
    pool_id=3,
    weight=800
})

recruit_pool_detail:upsert({
    id=3014,
    item_id=20019,
    item_type=1,
    pool_id=3,
    weight=800
})

recruit_pool_detail:upsert({
    id=3015,
    item_id=20017,
    item_type=1,
    pool_id=3,
    weight=800
})

recruit_pool_detail:upsert({
    id=3016,
    item_id=20016,
    item_type=1,
    pool_id=3,
    weight=800
})

recruit_pool_detail:upsert({
    id=3017,
    item_id=20022,
    item_type=1,
    pool_id=3,
    weight=800
})

recruit_pool_detail:upsert({
    id=3018,
    item_id=20026,
    item_type=1,
    pool_id=3,
    weight=800
})

recruit_pool_detail:upsert({
    id=3019,
    item_id=20025,
    item_type=1,
    pool_id=3,
    weight=800
})

recruit_pool_detail:upsert({
    id=3020,
    item_id=20031,
    item_type=1,
    pool_id=3,
    weight=800
})

recruit_pool_detail:update()
