--language_cfg.lua
--source: 语言转换接口表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local language = config_mgr:get_table("language")

--导出配置内容
language:upsert({
    id=1,
    lang='en',
    lang_mail=3,
    lang_safe=2
})

language:upsert({
    id=2,
    lang='zh-Hans',
    lang_mail=1,
    lang_safe=1
})

language:upsert({
    id=3,
    lang='th',
    lang_mail=5,
    lang_safe=4
})

language:upsert({
    id=4,
    lang='vi',
    lang_mail=11,
    lang_safe=6
})

language:upsert({
    id=5,
    lang='id',
    lang_mail=8,
    lang_safe=5
})

language:upsert({
    id=6,
    lang='ja',
    lang_mail=4,
    lang_safe=3
})

language:upsert({
    id=7,
    lang='ar',
    lang_mail=6,
    lang_safe=14
})

language:upsert({
    id=8,
    lang='de',
    lang_mail=14,
    lang_safe=11
})

language:upsert({
    id=9,
    lang='es',
    lang_mail=9,
    lang_safe=9
})

language:upsert({
    id=10,
    lang='fr',
    lang_mail=15,
    lang_safe=12
})

language:upsert({
    id=11,
    lang='it',
    lang_mail=16,
    lang_safe=16
})

language:upsert({
    id=12,
    lang='ko',
    lang_mail=7,
    lang_safe=8
})

language:upsert({
    id=13,
    lang='pt',
    lang_mail=10,
    lang_safe=15
})

language:upsert({
    id=14,
    lang='ru',
    lang_mail=12,
    lang_safe=10
})

language:upsert({
    id=15,
    lang='tr',
    lang_mail=13,
    lang_safe=21
})

language:upsert({
    id=16,
    lang='zh-Hant',
    lang_mail=2,
    lang_safe=1
})

language:update()
