--tutorial_cfg.lua
--source: 0_tutorial_新手引导.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local tutorial = config_mgr:get_table("tutorial")

--导出配置内容
tutorial:upsert({
    id=100
})

tutorial:upsert({
    id=101
})

tutorial:upsert({
    id=200
})

tutorial:upsert({
    id=201
})

tutorial:upsert({
    id=300
})

tutorial:upsert({
    id=301
})

tutorial:upsert({
    id=302
})

tutorial:upsert({
    id=400
})

tutorial:upsert({
    id=401
})

tutorial:upsert({
    id=500
})

tutorial:upsert({
    id=501
})

tutorial:upsert({
    id=502
})

tutorial:upsert({
    id=503
})

tutorial:upsert({
    id=600
})

tutorial:upsert({
    id=601
})

tutorial:upsert({
    id=700
})

tutorial:upsert({
    id=701
})

tutorial:upsert({
    id=800
})

tutorial:upsert({
    id=801
})

tutorial:upsert({
    id=802
})

tutorial:upsert({
    id=803
})

tutorial:upsert({
    id=804
})

tutorial:upsert({
    id=900
})

tutorial:upsert({
    id=901
})

tutorial:upsert({
    id=1000
})

tutorial:upsert({
    id=1001
})

tutorial:upsert({
    id=1002
})

tutorial:upsert({
    id=1003
})

tutorial:upsert({
    id=1004
})

tutorial:upsert({
    id=1100
})

tutorial:upsert({
    id=1101
})

tutorial:upsert({
    id=1102
})

tutorial:upsert({
    id=1103
})

tutorial:upsert({
    id=1104
})

tutorial:upsert({
    id=1201
})

tutorial:upsert({
    id=1202
})

tutorial:upsert({
    id=1203
})

tutorial:upsert({
    id=1204
})

tutorial:upsert({
    id=1300
})

tutorial:upsert({
    id=1301
})

tutorial:upsert({
    id=1302
})

tutorial:upsert({
    id=1303
})

tutorial:upsert({
    id=1401
})

tutorial:upsert({
    id=1402
})

tutorial:upsert({
    id=1403
})

tutorial:upsert({
    id=1404
})

tutorial:update()
