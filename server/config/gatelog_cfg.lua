--gatelog_cfg.lua
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local gatelog = config_mgr:get_table("gatelog")

--导出配置内容
gatelog:upsert({
    id=1,
    name='NID_ACTOR_ACTION_REQ'
})

gatelog:upsert({
    id=2,
    name='NID_ACTOR_ACTION_NTF'
})

gatelog:upsert({
    id=3,
    name='NID_BUILDING_UTENSIL_NTF'
})

gatelog:upsert({
    id=4,
    name='NID_UTILITY_BILOG_NTF'
})

gatelog:upsert({
    id=5,
    name='NID_UTILITY_SYNC_REDDOT_NTF'
})

gatelog:upsert({
    id=6,
    name='NID_UTILITY_NEWBEE_SYNC_NTF'
})

gatelog:update()
