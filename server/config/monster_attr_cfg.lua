--monster_attr_cfg.lua
--source: attribute.xlsm
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local monster_attr = config_mgr:get_table("monster_attr")

--导出配置内容
monster_attr:upsert({
    back=false,
    key='ATTR_HP',
    limit='ATTR_HP_MAX',
    range=16,
    save=false
})

monster_attr:upsert({
    back=false,
    key='ATTR_HP_MAX',
    range=16,
    save=false
})

monster_attr:upsert({
    back=false,
    key='ATTR_PROTO_ID',
    range=16,
    save=false
})

monster_attr:upsert({
    back=false,
    key='ATTR_LEVEL',
    range=16,
    save=false
})

monster_attr:upsert({
    back=false,
    key='ATTR_NAME',
    range=0,
    save=false
})

monster_attr:upsert({
    back=false,
    key='ATTR_ATTACK',
    range=0,
    save=false
})

monster_attr:upsert({
    back=false,
    key='ATTR_DEFENCE',
    range=0,
    save=false
})

monster_attr:upsert({
    back=false,
    key='ATTR_CRITICAL_RATE',
    range=0,
    save=false
})

monster_attr:upsert({
    back=false,
    key='ATTR_CRITICAL_HURT',
    range=0,
    save=false
})

monster_attr:upsert({
    back=false,
    key='ATTR_ATTACK_MAX',
    range=0,
    save=false
})

monster_attr:upsert({
    back=false,
    key='ATTR_MAP_ID',
    range=0,
    save=false
})

monster_attr:upsert({
    back=false,
    key='ATTR_POS_X',
    range=0,
    save=false
})

monster_attr:upsert({
    back=false,
    key='ATTR_POS_Y',
    range=0,
    save=false
})

monster_attr:upsert({
    back=false,
    key='ATTR_POS_Z',
    range=0,
    save=false
})

monster_attr:upsert({
    back=false,
    key='ATTR_DIR_Y',
    range=0,
    save=false
})

monster_attr:upsert({
    back=false,
    key='ATTR_SPEED',
    range=0,
    save=false
})

monster_attr:upsert({
    back=false,
    key='ATTR_COMBAT_STATE',
    range=16,
    save=false
})

monster_attr:upsert({
    back=false,
    key='ATTR_FACTION',
    range=16,
    save=false
})

monster_attr:update()
