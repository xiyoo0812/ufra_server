--scene_effect_attr_cfg.lua
--source: attribute.xlsm
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local scene_effect_attr = config_mgr:get_table("scene_effect_attr")

--导出配置内容
scene_effect_attr:upsert({
    back=false,
    key='ATTR_PROTO_ID',
    range=16,
    save=false
})

scene_effect_attr:upsert({
    back=false,
    key='ATTR_LEVEL',
    range=0,
    save=false
})

scene_effect_attr:upsert({
    back=false,
    key='ATTR_NAME',
    range=0,
    save=false
})

scene_effect_attr:upsert({
    back=false,
    key='ATTR_MAP_ID',
    range=0,
    save=false
})

scene_effect_attr:upsert({
    back=false,
    key='ATTR_POS_X',
    range=0,
    save=false
})

scene_effect_attr:upsert({
    back=false,
    key='ATTR_POS_Y',
    range=0,
    save=false
})

scene_effect_attr:upsert({
    back=false,
    key='ATTR_POS_Z',
    range=0,
    save=false
})

scene_effect_attr:upsert({
    back=false,
    key='ATTR_DIR_Y',
    range=0,
    save=false
})

scene_effect_attr:update()
