--push_cfg.lua
--source: 17-推送.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local push = config_mgr:get_table("push")

--导出配置内容
push:upsert({
    client=true,
    id=100101,
    template='尊敬的{1},给你推送一个消息'
})

push:upsert({
    client=true,
    id=100102,
    template='给你推送第二个消息{1}'
})

push:update()
