--sow_area_cfg.lua
--source: 9_撒点刷怪表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local sow_area = config_mgr:get_table("sow_area")

--导出配置内容
sow_area:upsert({
    area_center={
        114182,
        11288,
        107250
    },
    area_radius=1000,
    id=1,
    map_id=1003
})

sow_area:upsert({
    area_center={
        116319,
        11509,
        104047
    },
    area_radius=1000,
    id=2,
    map_id=1003
})

sow_area:upsert({
    area_center={
        119282,
        11293,
        109182
    },
    area_radius=1000,
    id=3,
    map_id=1003
})

sow_area:upsert({
    area_center={
        112377,
        11141,
        93363
    },
    area_radius=1500,
    id=4,
    map_id=1003
})

sow_area:upsert({
    area_center={
        111320,
        11089,
        83170
    },
    area_radius=1000,
    id=5,
    map_id=1003
})

sow_area:upsert({
    area_center={
        116932,
        11843,
        83270
    },
    area_radius=1200,
    id=6,
    map_id=1003
})

sow_area:upsert({
    area_center={
        120440,
        11836,
        77040
    },
    area_radius=1200,
    id=7,
    map_id=1003
})

sow_area:upsert({
    area_center={
        123932,
        11927,
        74607
    },
    area_radius=1200,
    id=8,
    map_id=1003
})

sow_area:upsert({
    area_center={
        132760,
        11583,
        72420
    },
    area_radius=2000,
    id=9,
    map_id=1003
})

sow_area:upsert({
    area_center={
        139272,
        16836,
        104760
    },
    area_radius=1500,
    id=10,
    map_id=1003
})

sow_area:upsert({
    area_center={
        141960,
        16989,
        95380
    },
    area_radius=1000,
    id=11,
    map_id=1003
})

sow_area:upsert({
    area_center={
        138150,
        17819,
        98390
    },
    area_radius=1000,
    id=12,
    map_id=1003
})

sow_area:upsert({
    area_center={
        67431,
        -15422,
        94089
    },
    area_radius=1500,
    id=13,
    map_id=1003
})

sow_area:upsert({
    area_center={
        153072,
        12976,
        92177
    },
    area_radius=1200,
    id=14,
    map_id=1003
})

sow_area:upsert({
    area_center={
        157970,
        12200,
        75554
    },
    area_radius=2000,
    id=15,
    map_id=1003
})

sow_area:upsert({
    area_center={
        149790,
        14586,
        80190
    },
    area_radius=2000,
    id=16,
    map_id=1003
})

sow_area:upsert({
    area_center={
        144260,
        11346,
        73390
    },
    area_radius=1500,
    id=17,
    map_id=1003
})

sow_area:upsert({
    area_center={
        0,
        0,
        0
    },
    area_radius=5000,
    id=1,
    map_id=1005
})

sow_area:upsert({
    area_center={
        -90,
        0,
        12230
    },
    area_radius=5000,
    id=2,
    map_id=1005
})

sow_area:upsert({
    area_center={
        37,
        50,
        -1039
    },
    area_radius=1000,
    id=1,
    map_id=1011
})

sow_area:upsert({
    area_center={
        9489,
        50,
        -518
    },
    area_radius=1000,
    id=2,
    map_id=1011
})

sow_area:upsert({
    area_center={
        14961,
        50,
        -11390
    },
    area_radius=1500,
    id=3,
    map_id=1011
})

sow_area:upsert({
    area_center={
        3850,
        -19,
        9863
    },
    area_radius=1500,
    id=1,
    map_id=1012
})

sow_area:upsert({
    area_center={
        -1832,
        -1417,
        -3061
    },
    area_radius=1500,
    id=2,
    map_id=1012
})

sow_area:upsert({
    area_center={
        -2105,
        -3656,
        -16778
    },
    area_radius=1500,
    id=3,
    map_id=1012
})

sow_area:upsert({
    area_center={
        -2576,
        -3665,
        -16758
    },
    area_radius=2000,
    id=1,
    map_id=1006
})

sow_area:update()
