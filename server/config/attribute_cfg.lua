--attribute_cfg.lua
--source: attribute.xlsm
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local attribute = config_mgr:get_table("attribute")

--导出配置内容
attribute:upsert({
    enum_key='ATTR_HP',
    id=1,
    increase=true,
    nick='hp',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_ENERGY',
    id=2,
    increase=true,
    nick='energy',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_STAMINA',
    id=3,
    increase=true,
    nick='stamina',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_EXP',
    id=4,
    increase=true,
    nick='exp',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_HP_MAX',
    id=5,
    increase=true,
    nick='hp_max',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_ENERGY_MAX',
    id=6,
    increase=true,
    nick='energy_max',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_STAMINA_MAX',
    id=7,
    increase=true,
    nick='stamina_max',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_EXP_MAX',
    id=8,
    increase=true,
    nick='exp_max',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_LEVEL',
    id=9,
    increase=false,
    nick='level',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_COIN',
    id=10,
    increase=true,
    nick='coin',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_NAME',
    id=11,
    increase=true,
    nick='name',
    type='string'
})

attribute:upsert({
    enum_key='ATTR_DIAMOND',
    id=12,
    increase=true,
    nick='diamond',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_PROTO_ID',
    id=13,
    increase=false,
    nick='proto_id',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_LUCKY',
    id=14,
    increase=true,
    nick='lucky',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_GENDER',
    id=15,
    increase=false,
    nick='gender',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_GTIME',
    id=16,
    increase=false,
    nick='gtime',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_CUSTOM',
    id=17,
    increase=false,
    nick='custom',
    type='bytes'
})

attribute:upsert({
    enum_key='ATTR_VERSION',
    id=18,
    increase=false,
    nick='version',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_ONLINE_TIME',
    id=19,
    increase=true,
    nick='online_time',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_ATTACK',
    id=21,
    increase=true,
    nick='attack',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_DEFENCE',
    id=22,
    increase=true,
    nick='defence',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_CRITICAL_RATE',
    id=23,
    increase=true,
    nick='critical_rate',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_CRITICAL_HURT',
    id=24,
    increase=true,
    nick='critical_hurt',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_ATTACK_MAX',
    id=25,
    increase=true,
    nick='attack_max',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_BIRTHDAY',
    id=26,
    increase=false,
    nick='birthday',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_AVATAR',
    id=27,
    increase=false,
    nick='avatar',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_AVTFRM',
    id=28,
    increase=false,
    nick='avtfrm',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_TITLE',
    id=29,
    increase=false,
    nick='title',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_GPROGRESS',
    id=31,
    increase=false,
    nick='gprogress',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_GCOUNT',
    id=32,
    increase=true,
    nick='gcount',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_LOGIN_TIME',
    id=33,
    increase=false,
    nick='login_time',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_HEAD',
    id=51,
    increase=false,
    nick='head',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_FACE',
    id=52,
    increase=false,
    nick='face',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_CLOTH',
    id=53,
    increase=false,
    nick='cloth',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_TROUSERS',
    id=54,
    increase=false,
    nick='trousers',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_SHOES',
    id=55,
    increase=false,
    nick='shoes',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_NECK',
    id=56,
    increase=false,
    nick='neck',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_RING',
    id=57,
    increase=false,
    nick='ring',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_PICJAXE',
    id=58,
    increase=false,
    nick='picjaxe',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_HATCHET',
    id=59,
    increase=false,
    nick='hatchet',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_WEAPON',
    id=60,
    increase=false,
    nick='weapon',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_SHOVEL',
    id=61,
    increase=false,
    nick='shovel',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_POLE',
    id=62,
    increase=false,
    nick='pole',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_SUITABLE',
    id=71,
    increase=false,
    nick='suitable',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_TOPSHOW',
    id=72,
    increase=false,
    nick='topshow',
    type='string'
})

attribute:upsert({
    enum_key='ATTR_EXP_PER',
    id=80,
    increase=false,
    nick='exp_per',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_COIN_PER',
    id=81,
    increase=false,
    nick='coin_per',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_DIR_Y',
    id=100,
    increase=false,
    nick='dir_y',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_MAP_ID',
    id=101,
    increase=false,
    nick='map_id',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_POS_X',
    id=102,
    increase=false,
    nick='pos_x',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_POS_Y',
    id=103,
    increase=false,
    nick='pos_y',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_POS_Z',
    id=104,
    increase=false,
    nick='pos_z',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_SPEED',
    id=106,
    increase=false,
    nick='speed',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_COMBAT_STATE',
    id=109,
    increase=false,
    nick='combat_state',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_DYING_TIMING',
    id=110,
    increase=false,
    nick='dying_timing',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_TREAT_TIMING',
    id=111,
    increase=false,
    nick='treat_timing',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_FACTION',
    id=112,
    increase=false,
    nick='faction',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_WORLD_LINE',
    id=117,
    increase=false,
    nick='world_line',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_CHCOM_ID',
    id=118,
    increase=false,
    nick='chcom_id',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_ACTY',
    id=120,
    increase=true,
    nick='acty',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_DAY_ACTY',
    id=121,
    increase=true,
    nick='day_acty',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_WEEK_ACTY',
    id=122,
    increase=true,
    nick='week_acty',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_TOWN_LEVEL',
    id=129,
    increase=true,
    nick='town_level',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_TOWN_EXP',
    id=130,
    increase=true,
    nick='town_exp',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_SP',
    id=131,
    increase=true,
    nick='sp',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_SP_MAX',
    id=132,
    increase=false,
    nick='sp_max',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_RECYCLE_END_TIME',
    id=137,
    increase=false,
    nick='recycle_end_time',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_DAY_TALK_LIKING',
    id=138,
    increase=true,
    nick='day_talk_liking',
    type='int'
})

attribute:upsert({
    enum_key='ATTR_OWNER_ID',
    id=139,
    increase=false,
    nick='owner_id',
    type='int'
})

attribute:update()
