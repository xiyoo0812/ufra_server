--dungeon_01_cfg.lua
--source: 7_dungeon_01地图信息表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local dungeon_01 = config_mgr:get_table("dungeon_01")

--导出配置内容
dungeon_01:upsert({
    dir={
        0,
        0,
        0
    },
    dynamic=true,
    id=1001,
    name='铜矿_01_1230测试',
    pos={
        -557,
        53,
        -753
    },
    proto_id=20019,
    refresh_limit=0,
    refresh_time=259200,
    scale={
        100,
        100,
        100
    },
    status=true,
    type=4,
    visit_gather=true
})

dungeon_01:update()
