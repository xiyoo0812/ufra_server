--player_attr_cfg.lua
--source: attribute.xlsm
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local player_attr = config_mgr:get_table("player_attr")

--导出配置内容
player_attr:upsert({
    back=false,
    key='ATTR_HP',
    limit='ATTR_HP_MAX',
    range=16,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_ENERGY',
    range=1,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_STAMINA',
    limit='ATTR_STAMINA_MAX',
    range=1,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_EXP',
    range=1,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_HP_MAX',
    range=16,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_ENERGY_MAX',
    range=1,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_STAMINA_MAX',
    range=1,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_EXP_MAX',
    range=1,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_LEVEL',
    range=16,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_COIN',
    range=1,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_NAME',
    range=16,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_DIAMOND',
    range=1,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_PROTO_ID',
    range=16,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_LUCKY',
    range=1,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_GENDER',
    range=16,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_GTIME',
    range=1,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_CUSTOM',
    range=16,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_VERSION',
    range=0,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_ONLINE_TIME',
    range=1,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_ATTACK',
    range=1,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_DEFENCE',
    range=1,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_CRITICAL_RATE',
    range=1,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_CRITICAL_HURT',
    range=1,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_ATTACK_MAX',
    range=1,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_MAP_ID',
    range=0,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_POS_X',
    range=0,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_POS_Y',
    range=0,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_POS_Z',
    range=0,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_DIR_Y',
    range=0,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_SPEED',
    range=0,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_HEAD',
    range=16,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_FACE',
    range=16,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_CLOTH',
    range=16,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_TROUSERS',
    range=16,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_SHOES',
    range=16,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_NECK',
    range=0,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_RING',
    range=0,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_PICJAXE',
    range=16,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_HATCHET',
    range=16,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_WEAPON',
    range=16,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_SHOVEL',
    range=16,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_POLE',
    range=16,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_SUITABLE',
    range=1,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_EXP_PER',
    range=0,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_COIN_PER',
    range=0,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_ACTY',
    range=1,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_DAY_ACTY',
    range=1,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_WEEK_ACTY',
    range=1,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_CHCOM_ID',
    range=1,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_BIRTHDAY',
    range=1,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_AVATAR',
    range=1,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_AVTFRM',
    range=1,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_TITLE',
    range=1,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_TOWN_LEVEL',
    range=1,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_TOWN_EXP',
    range=1,
    save=true
})

player_attr:upsert({
    back=true,
    key='ATTR_SP',
    limit='ATTR_SP_MAX',
    range=1,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_SP_MAX',
    range=0,
    save=false
})

player_attr:upsert({
    back=true,
    key='ATTR_STAMINA',
    limit='ATTR_STAMINA_MAX',
    range=1,
    save=true
})

player_attr:upsert({
    back=true,
    key='ATTR_STAMINA_MAX',
    range=1,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_RECYCLE_END_TIME',
    range=1,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_DAY_TALK_LIKING',
    range=0,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_LOGIN_TIME',
    range=0,
    save=true
})

player_attr:upsert({
    back=false,
    key='ATTR_COMBAT_STATE',
    range=16,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_DYING_TIMING',
    range=16,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_TREAT_TIMING',
    range=16,
    save=false
})

player_attr:upsert({
    back=false,
    key='ATTR_FACTION',
    range=16,
    save=false
})

player_attr:update()
