--activity_timed_reward_cfg.lua
--source: 12_activity_timed_reward_定时投放奖励配置.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local activity_timed_reward = config_mgr:get_table("activity_timed_reward")

--导出配置内容
activity_timed_reward:upsert({
    activity_id=1010,
    child_id=1,
    id=1,
    rewards={
        [103026]=5,
        [103027]=2
    },
    times={
        [0]='09:00:00',
        [1]='14:00:00'
    }
})

activity_timed_reward:upsert({
    activity_id=1010,
    child_id=2,
    id=2,
    rewards={
        [103026]=5,
        [103027]=2
    },
    times={
        [0]='18:00:00',
        [1]='23:00:00'
    }
})

activity_timed_reward:update()
