--monster_ai_cfg.lua
--luacheck: ignore 631

--怪物AI配置表
local config_mgr = quanta.get("config_mgr")
local monster_ai = config_mgr:get_table("monster_ai")

monster_ai:upsert({
	monster_id = 30003,
	remark = '河狸工人',
	nearby_enemy_distance = 200,
	nearby_spawn_distance = 50,
	wander = true,
	amuse = false,
	standoff_duration = { 3000,6000 },
	standoff_back_distance = 250,
	standoff_back_precision = 20,
	standoff_move_duration = { 900,2000 },
	standoff_move_radius = 300,
	wander_radius = 700,
	hate_attenuate = 0,
	return_spawn_speed = 2000,
	chase_speed = 400,
	chase_distance = 1500,
	chase_scope = 2000,
	guard_circle_radius = 500,
	guard_sector_radius = 1500,
	guard_sector_angle = 180,
	combat_delivery_radius = 1,
	combat_delivery_duration = 1,
	combat_delivery_amount = 1,
	patrol_type = 0,
	patrol_path = { },
	state_duration = {30,70 },
	actions = {
		{
			name = '激活(AI)',
			priority = 1,
			cost = 1,
			code = 'activate',
			precondition ={
	['_or'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='inactivite',
},
		{
	owner = 'entity',
	property ='is_activated',
	operator = '!=',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='state',
	operator = '!=',
	value ='inactive',
},
	},
},
		},
		{
			name = '待机站立',
			priority = 2,
			cost = 1,
			code = 'standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='rest_idle',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = '漫游移动',
			priority = 2,
			cost = 1,
			code = 'move_wander',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='wander_idle',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = '向敌人移动',
			priority = 3,
			cost = 1,
			code = 'move_to_enemy',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='chase',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '向出生点移动',
			priority = 4,
			cost = 2,
			code = 'move_to_spawn',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='goto_spawn',
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '!=',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '对峙后退',
			priority = 6,
			cost = 1,
			code = 'standoff_back',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='standoff',
},
		{
	owner = 'entity',
	property ='can_standoff_back',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='can_standoff_horizontal',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '对峙横移',
			priority = 6,
			cost = 1,
			code = 'standoff_horizontal',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='standoff',
},
		{
	owner = 'entity',
	property ='can_standoff_horizontal',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '攻击敌人',
			priority = 6,
			cost = 2,
			code = 'attack_enemy',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='use_skill',
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
	},
},
		},
		{
			name = '生命值回满',
			priority = 4,
			cost = 1,
			code = 'fully_hp',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='goto_spawn',
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '战斗站立',
			priority = 5,
			cost = 1,
			code = 'combat_standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='combat_idle',
},
	},
},
			effect ={
	['_or'] = {
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '沉默站立',
			priority = 5,
			cost = 1,
			code = 'silence_standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='hit_silence',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
		},
	},
	states = {
		{
			name = 'inactive',
			description = '未激活',
			priority = 1,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '!=',
	value =true,
},
	},
},
			postcondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'rest_idle',
			description = '休闲待机',
			priority = 2,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	['_or'] = {
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
	},
},
	},
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = 'wander_idle',
			description = '漫游待机',
			priority = 2,
			weight = 2,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_wander',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = 'chase',
			description = '追击',
			priority = 3,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '!=',
	value =true,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='is_in_standoff_distance',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='is_skill_using',
	operator = '==',
	value =false,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'goto_spawn',
			description = '回出生点',
			priority = 3,
			weight = 2,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	['_or'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	['_or'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '!=',
	value =true,
},
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '!=',
	value =true,
},
	},
},
	},
},
	},
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '!=',
	value =true,
},
	},
},
	},
},
	},
},
			postcondition ={
	['_or'] = {
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
	},
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'use_skill',
			description = '使用技能',
			priority = 4,
			weight = 4,
			precondition ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
	},
},
		},
		{
			name = 'combat_idle',
			description = '战斗待机',
			priority = 4,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =false,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'hit_silence',
			description = '受击沉默',
			priority = 4,
			weight = 3,
			precondition ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
		},
	},
})

monster_ai:upsert({
	monster_id = 30002,
	remark = '粉色羊驼',
	nearby_enemy_distance = 200,
	nearby_spawn_distance = 50,
	wander = true,
	amuse = false,
	standoff_duration = { 3000,5000 },
	standoff_back_distance = 250,
	standoff_back_precision = 30,
	standoff_move_duration = { 900,2000 },
	standoff_move_radius = 300,
	wander_radius = 700,
	hate_attenuate = 0,
	return_spawn_speed = 2000,
	chase_speed = 400,
	chase_distance = 1500,
	chase_scope = 2000,
	guard_circle_radius = 500,
	guard_sector_radius = 1500,
	guard_sector_angle = 180,
	combat_delivery_radius = 1,
	combat_delivery_duration = 1,
	combat_delivery_amount = 1,
	patrol_type = 0,
	patrol_path = { },
	state_duration = {30,70 },
	actions = {
		{
			name = '激活(AI)',
			priority = 1,
			cost = 1,
			code = 'activate',
			precondition ={
	['_or'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='inactivite',
},
		{
	owner = 'entity',
	property ='is_activated',
	operator = '!=',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='state',
	operator = '!=',
	value ='inactive',
},
	},
},
		},
		{
			name = '待机站立',
			priority = 2,
			cost = 1,
			code = 'standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='rest_idle',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = '漫游移动',
			priority = 2,
			cost = 1,
			code = 'move_wander',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='wander_idle',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = '向敌人移动',
			priority = 3,
			cost = 1,
			code = 'move_to_enemy',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='chase',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '向出生点移动',
			priority = 4,
			cost = 2,
			code = 'move_to_spawn',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='goto_spawn',
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '!=',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '对峙后退',
			priority = 6,
			cost = 1,
			code = 'standoff_back',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='standoff',
},
		{
	owner = 'entity',
	property ='can_standoff_back',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='can_standoff_horizontal',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '对峙横移',
			priority = 6,
			cost = 1,
			code = 'standoff_horizontal',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='standoff',
},
		{
	owner = 'entity',
	property ='can_standoff_horizontal',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '攻击敌人',
			priority = 6,
			cost = 2,
			code = 'attack_enemy',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='use_skill',
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
	},
},
		},
		{
			name = '生命值回满',
			priority = 4,
			cost = 1,
			code = 'fully_hp',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='goto_spawn',
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '战斗站立',
			priority = 5,
			cost = 1,
			code = 'combat_standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='combat_idle',
},
	},
},
			effect ={
	['_or'] = {
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '沉默站立',
			priority = 5,
			cost = 1,
			code = 'silence_standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='hit_silence',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
		},
	},
	states = {
		{
			name = 'inactive',
			description = '未激活',
			priority = 1,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '!=',
	value =true,
},
	},
},
			postcondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'rest_idle',
			description = '休闲待机',
			priority = 2,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	['_or'] = {
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
	},
},
	},
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = 'wander_idle',
			description = '漫游待机',
			priority = 2,
			weight = 2,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_wander',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = 'chase',
			description = '追击',
			priority = 3,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '!=',
	value =true,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='is_in_standoff_distance',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='is_skill_using',
	operator = '==',
	value =false,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'goto_spawn',
			description = '回出生点',
			priority = 3,
			weight = 2,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	['_or'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	['_or'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '!=',
	value =true,
},
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '!=',
	value =true,
},
	},
},
	},
},
	},
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '!=',
	value =true,
},
	},
},
	},
},
	},
},
			postcondition ={
	['_or'] = {
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
	},
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'use_skill',
			description = '使用技能',
			priority = 4,
			weight = 4,
			precondition ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
	},
},
		},
		{
			name = 'combat_idle',
			description = '战斗待机',
			priority = 4,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =false,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'hit_silence',
			description = '受击沉默',
			priority = 4,
			weight = 3,
			precondition ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
		},
	},
})

monster_ai:upsert({
	monster_id = 30001,
	remark = '蝙蝠熊猫',
	nearby_enemy_distance = 200,
	nearby_spawn_distance = 50,
	wander = true,
	amuse = false,
	standoff_duration = { 0,6000 },
	standoff_back_distance = 250,
	standoff_back_precision = 0,
	standoff_move_duration = { 0,3000 },
	standoff_move_radius = 300,
	wander_radius = 700,
	hate_attenuate = 0,
	return_spawn_speed = 2000,
	chase_speed = 400,
	chase_distance = 1500,
	chase_scope = 2000,
	guard_circle_radius = 500,
	guard_sector_radius = 1500,
	guard_sector_angle = 180,
	combat_delivery_radius = 1,
	combat_delivery_duration = 1,
	combat_delivery_amount = 1,
	patrol_type = 0,
	patrol_path = { },
	state_duration = {30,70 },
	actions = {
		{
			name = '激活(AI)',
			priority = 1,
			cost = 1,
			code = 'activate',
			precondition ={
	['_or'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='inactivite',
},
		{
	owner = 'entity',
	property ='is_activated',
	operator = '!=',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='state',
	operator = '!=',
	value ='inactive',
},
	},
},
		},
		{
			name = '待机站立',
			priority = 2,
			cost = 1,
			code = 'standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='rest_idle',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = '漫游移动',
			priority = 2,
			cost = 1,
			code = 'move_wander',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='wander_idle',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = '向敌人移动',
			priority = 3,
			cost = 1,
			code = 'move_to_enemy',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='chase',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '向出生点移动',
			priority = 4,
			cost = 2,
			code = 'move_to_spawn',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='goto_spawn',
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '!=',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '攻击敌人',
			priority = 6,
			cost = 2,
			code = 'attack_enemy',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='use_skill',
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
	},
},
		},
		{
			name = '生命值回满',
			priority = 4,
			cost = 1,
			code = 'fully_hp',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='goto_spawn',
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '战斗站立',
			priority = 5,
			cost = 1,
			code = 'combat_standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='combat_idle',
},
	},
},
			effect ={
	['_or'] = {
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '沉默站立',
			priority = 5,
			cost = 1,
			code = 'silence_standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='hit_silence',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
		},
	},
	states = {
		{
			name = 'inactive',
			description = '未激活',
			priority = 1,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '!=',
	value =true,
},
	},
},
			postcondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'rest_idle',
			description = '休闲待机',
			priority = 2,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	['_or'] = {
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
	},
},
	},
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = 'wander_idle',
			description = '漫游待机',
			priority = 2,
			weight = 2,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_wander',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = 'chase',
			description = '追击',
			priority = 3,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '!=',
	value =true,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='is_in_standoff_distance',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='is_skill_using',
	operator = '==',
	value =false,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'goto_spawn',
			description = '回出生点',
			priority = 3,
			weight = 2,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	['_or'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	['_or'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '!=',
	value =true,
},
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '!=',
	value =true,
},
	},
},
	},
},
	},
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '!=',
	value =true,
},
	},
},
	},
},
	},
},
			postcondition ={
	['_or'] = {
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
	},
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'use_skill',
			description = '使用技能',
			priority = 4,
			weight = 4,
			precondition ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
	},
},
		},
		{
			name = 'combat_idle',
			description = '战斗待机',
			priority = 4,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =false,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'hit_silence',
			description = '受击沉默',
			priority = 4,
			weight = 3,
			precondition ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
		},
	},
})

monster_ai:upsert({
	monster_id = 30005,
	remark = '土匪（高瘦）',
	nearby_enemy_distance = 250,
	nearby_spawn_distance = 50,
	wander = true,
	amuse = false,
	standoff_duration = { 1000,3000 },
	standoff_back_distance = 200,
	standoff_back_precision = 20,
	standoff_move_duration = { 900,3000 },
	standoff_move_radius = 300,
	wander_radius = 700,
	hate_attenuate = 0,
	return_spawn_speed = 1000,
	chase_speed = 400,
	chase_distance = 3500,
	chase_scope = 4000,
	guard_circle_radius = 1000,
	guard_sector_radius = 3000,
	guard_sector_angle = 360,
	combat_delivery_radius = 1,
	combat_delivery_duration = 1,
	combat_delivery_amount = 1,
	patrol_type = 0,
	patrol_path = { },
	state_duration = {30,70 },
	actions = {
		{
			name = '激活(AI)',
			priority = 1,
			cost = 1,
			code = 'activate',
			precondition ={
	['_or'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='inactivite',
},
		{
	owner = 'entity',
	property ='is_activated',
	operator = '!=',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='state',
	operator = '!=',
	value ='inactive',
},
	},
},
		},
		{
			name = '待机站立',
			priority = 2,
			cost = 1,
			code = 'standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='rest_idle',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = '漫游移动',
			priority = 2,
			cost = 1,
			code = 'move_wander',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='wander_idle',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = '向敌人移动',
			priority = 3,
			cost = 1,
			code = 'move_to_enemy',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='chase',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '向出生点移动',
			priority = 4,
			cost = 2,
			code = 'move_to_spawn',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='goto_spawn',
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '!=',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '对峙后退',
			priority = 6,
			cost = 1,
			code = 'standoff_back',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='standoff',
},
		{
	owner = 'entity',
	property ='can_standoff_back',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='can_standoff_horizontal',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '对峙横移',
			priority = 6,
			cost = 1,
			code = 'standoff_horizontal',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='standoff',
},
		{
	owner = 'entity',
	property ='can_standoff_horizontal',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '攻击敌人',
			priority = 6,
			cost = 2,
			code = 'attack_enemy',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='use_skill',
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
	},
},
		},
		{
			name = '生命值回满',
			priority = 4,
			cost = 1,
			code = 'fully_hp',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='goto_spawn',
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '战斗站立',
			priority = 5,
			cost = 1,
			code = 'combat_standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='combat_idle',
},
	},
},
			effect ={
	['_or'] = {
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '沉默站立',
			priority = 5,
			cost = 1,
			code = 'silence_standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='hit_silence',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
		},
	},
	states = {
		{
			name = 'inactive',
			description = '未激活',
			priority = 1,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '!=',
	value =true,
},
	},
},
			postcondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'rest_idle',
			description = '休闲待机',
			priority = 2,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	['_or'] = {
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
	},
},
	},
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = 'wander_idle',
			description = '漫游待机',
			priority = 2,
			weight = 2,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_wander',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = 'chase',
			description = '追击',
			priority = 3,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '!=',
	value =true,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='is_in_standoff_distance',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='is_skill_using',
	operator = '==',
	value =false,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'goto_spawn',
			description = '回出生点',
			priority = 3,
			weight = 2,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	['_or'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	['_or'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '!=',
	value =true,
},
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '!=',
	value =true,
},
	},
},
	},
},
	},
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '!=',
	value =true,
},
	},
},
	},
},
	},
},
			postcondition ={
	['_or'] = {
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
	},
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'use_skill',
			description = '使用技能',
			priority = 4,
			weight = 4,
			precondition ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
	},
},
		},
		{
			name = 'combat_idle',
			description = '战斗待机',
			priority = 4,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =false,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'hit_silence',
			description = '受击沉默',
			priority = 4,
			weight = 3,
			precondition ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
		},
	},
})

monster_ai:upsert({
	monster_id = 30006,
	remark = '土匪（高壮）',
	nearby_enemy_distance = 350,
	nearby_spawn_distance = 50,
	wander = true,
	amuse = false,
	standoff_duration = { 1000,3000 },
	standoff_back_distance = 200,
	standoff_back_precision = 20,
	standoff_move_duration = { 900,3000 },
	standoff_move_radius = 300,
	wander_radius = 700,
	hate_attenuate = 0,
	return_spawn_speed = 500,
	chase_speed = 400,
	chase_distance = 3500,
	chase_scope = 4000,
	guard_circle_radius = 1000,
	guard_sector_radius = 3000,
	guard_sector_angle = 360,
	combat_delivery_radius = 1,
	combat_delivery_duration = 1,
	combat_delivery_amount = 1,
	patrol_type = 0,
	patrol_path = { },
	state_duration = {30,70 },
	actions = {
		{
			name = '激活(AI)',
			priority = 1,
			cost = 1,
			code = 'activate',
			precondition ={
	['_or'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='inactivite',
},
		{
	owner = 'entity',
	property ='is_activated',
	operator = '!=',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='state',
	operator = '!=',
	value ='inactive',
},
	},
},
		},
		{
			name = '待机站立',
			priority = 2,
			cost = 1,
			code = 'standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='rest_idle',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = '漫游移动',
			priority = 2,
			cost = 1,
			code = 'move_wander',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='wander_idle',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = '向敌人移动',
			priority = 3,
			cost = 1,
			code = 'move_to_enemy',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='chase',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '向出生点移动',
			priority = 4,
			cost = 2,
			code = 'move_to_spawn',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='goto_spawn',
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '!=',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '对峙后退',
			priority = 6,
			cost = 1,
			code = 'standoff_back',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='standoff',
},
		{
	owner = 'entity',
	property ='can_standoff_back',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='can_standoff_horizontal',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '对峙横移',
			priority = 6,
			cost = 1,
			code = 'standoff_horizontal',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='standoff',
},
		{
	owner = 'entity',
	property ='can_standoff_horizontal',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '攻击敌人',
			priority = 6,
			cost = 2,
			code = 'attack_enemy',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='use_skill',
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
	},
},
		},
		{
			name = '生命值回满',
			priority = 4,
			cost = 1,
			code = 'fully_hp',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='goto_spawn',
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '战斗站立',
			priority = 5,
			cost = 1,
			code = 'combat_standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='combat_idle',
},
	},
},
			effect ={
	['_or'] = {
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '沉默站立',
			priority = 5,
			cost = 1,
			code = 'silence_standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='hit_silence',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
		},
	},
	states = {
		{
			name = 'inactive',
			description = '未激活',
			priority = 1,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '!=',
	value =true,
},
	},
},
			postcondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'rest_idle',
			description = '休闲待机',
			priority = 2,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	['_or'] = {
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
	},
},
	},
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = 'wander_idle',
			description = '漫游待机',
			priority = 2,
			weight = 2,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_wander',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = 'chase',
			description = '追击',
			priority = 3,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '!=',
	value =true,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='is_in_standoff_distance',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='is_skill_using',
	operator = '==',
	value =false,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'goto_spawn',
			description = '回出生点',
			priority = 3,
			weight = 2,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	['_or'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	['_or'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '!=',
	value =true,
},
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '!=',
	value =true,
},
	},
},
	},
},
	},
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '!=',
	value =true,
},
	},
},
	},
},
	},
},
			postcondition ={
	['_or'] = {
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
	},
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'use_skill',
			description = '使用技能',
			priority = 4,
			weight = 4,
			precondition ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
	},
},
		},
		{
			name = 'combat_idle',
			description = '战斗待机',
			priority = 4,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =false,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'hit_silence',
			description = '受击沉默',
			priority = 4,
			weight = 3,
			precondition ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
		},
	},
})

monster_ai:upsert({
	monster_id = 40001,
	remark = '蝙蝠熊猫-新AI(基于-30001)',
	nearby_enemy_distance = 200,
	nearby_spawn_distance = 50,
	wander = true,
	amuse = false,
	standoff_duration = { 0,6000 },
	standoff_back_distance = 250,
	standoff_back_precision = 0,
	standoff_move_duration = { 0,3000 },
	standoff_move_radius = 300,
	wander_radius = 700,
	hate_attenuate = 0,
	return_spawn_speed = 2000,
	chase_speed = 400,
	chase_distance = 1500,
	chase_scope = 2000,
	guard_circle_radius = 500,
	guard_sector_radius = 1500,
	guard_sector_angle = 180,
	combat_delivery_radius = 1,
	combat_delivery_duration = 1,
	combat_delivery_amount = 1,
	patrol_type = 0,
	patrol_path = { },
	state_duration = {30,70 },
	actions = {
		{
			name = '激活(AI)',
			priority = 1,
			cost = 1,
			code = 'activate',
			precondition ={
	['_or'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='inactivite',
},
		{
	owner = 'entity',
	property ='is_activated',
	operator = '!=',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='state',
	operator = '!=',
	value ='inactive',
},
	},
},
		},
		{
			name = '待机站立',
			priority = 2,
			cost = 1,
			code = 'standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='rest_idle',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = '漫游移动',
			priority = 2,
			cost = 1,
			code = 'move_wander',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='wander_idle',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = '向敌人移动',
			priority = 3,
			cost = 1,
			code = 'move_to_enemy',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='chase',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '向出生点移动',
			priority = 4,
			cost = 2,
			code = 'move_to_spawn',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='goto_spawn',
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '!=',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '攻击敌人',
			priority = 6,
			cost = 2,
			code = 'attack_enemy',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='use_skill',
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
	},
},
		},
		{
			name = '生命值回满',
			priority = 4,
			cost = 1,
			code = 'fully_hp',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='goto_spawn',
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '战斗站立',
			priority = 5,
			cost = 1,
			code = 'combat_standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='combat_idle',
},
	},
},
			effect ={
	['_or'] = {
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '沉默站立',
			priority = 5,
			cost = 1,
			code = 'silence_standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='hit_silence',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
		},
	},
	states = {
		{
			name = 'inactive',
			description = '未激活',
			priority = 1,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '!=',
	value =true,
},
	},
},
			postcondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'rest_idle',
			description = '休闲待机',
			priority = 2,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	['_or'] = {
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
	},
},
	},
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = 'wander_idle',
			description = '漫游待机',
			priority = 2,
			weight = 2,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_wander',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = 'chase',
			description = '追击',
			priority = 3,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '!=',
	value =true,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='is_in_standoff_distance',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='is_skill_using',
	operator = '==',
	value =false,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'goto_spawn',
			description = '回出生点',
			priority = 3,
			weight = 2,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	['_or'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	['_or'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '!=',
	value =true,
},
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '!=',
	value =true,
},
	},
},
	},
},
	},
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '!=',
	value =true,
},
	},
},
	},
},
	},
},
			postcondition ={
	['_or'] = {
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
	},
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'use_skill',
			description = '使用技能',
			priority = 4,
			weight = 4,
			precondition ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
	},
},
		},
		{
			name = 'combat_idle',
			description = '战斗待机',
			priority = 4,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =false,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'hit_silence',
			description = '受击沉默',
			priority = 4,
			weight = 3,
			precondition ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
		},
	},
})

monster_ai:upsert({
	monster_id = 40002,
	remark = '蝙蝠熊猫-新AI(基于-30001)',
	nearby_enemy_distance = 200,
	nearby_spawn_distance = 50,
	wander = true,
	amuse = false,
	standoff_duration = { 0,6000 },
	standoff_back_distance = 250,
	standoff_back_precision = 0,
	standoff_move_duration = { 0,3000 },
	standoff_move_radius = 300,
	wander_radius = 700,
	hate_attenuate = 0,
	return_spawn_speed = 2000,
	chase_speed = 400,
	chase_distance = 1500,
	chase_scope = 2000,
	guard_circle_radius = 500,
	guard_sector_radius = 1500,
	guard_sector_angle = 180,
	combat_delivery_radius = 1,
	combat_delivery_duration = 1,
	combat_delivery_amount = 1,
	patrol_type = 0,
	patrol_path = { },
	state_duration = {30,70 },
	actions = {
		{
			name = '激活(AI)',
			priority = 1,
			cost = 1,
			code = 'activate',
			precondition ={
	['_or'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='inactivite',
},
		{
	owner = 'entity',
	property ='is_activated',
	operator = '!=',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='state',
	operator = '!=',
	value ='inactive',
},
	},
},
		},
		{
			name = '待机站立',
			priority = 2,
			cost = 1,
			code = 'standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='rest_idle',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = '漫游移动',
			priority = 2,
			cost = 1,
			code = 'move_wander',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='wander_idle',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = '向敌人移动',
			priority = 3,
			cost = 1,
			code = 'move_to_enemy',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='chase',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '向出生点移动',
			priority = 4,
			cost = 2,
			code = 'move_to_spawn',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='goto_spawn',
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '!=',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '攻击敌人',
			priority = 6,
			cost = 2,
			code = 'attack_enemy',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='use_skill',
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
	},
},
		},
		{
			name = '生命值回满',
			priority = 4,
			cost = 1,
			code = 'fully_hp',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='goto_spawn',
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '战斗站立',
			priority = 5,
			cost = 1,
			code = 'combat_standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='combat_idle',
},
	},
},
			effect ={
	['_or'] = {
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '沉默站立',
			priority = 5,
			cost = 1,
			code = 'silence_standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='hit_silence',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
		},
	},
	states = {
		{
			name = 'inactive',
			description = '未激活',
			priority = 1,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '!=',
	value =true,
},
	},
},
			postcondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'rest_idle',
			description = '休闲待机',
			priority = 2,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	['_or'] = {
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
	},
},
	},
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = 'wander_idle',
			description = '漫游待机',
			priority = 2,
			weight = 2,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_wander',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = 'chase',
			description = '追击',
			priority = 3,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '!=',
	value =true,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='is_in_standoff_distance',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='is_skill_using',
	operator = '==',
	value =false,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'goto_spawn',
			description = '回出生点',
			priority = 3,
			weight = 2,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	['_or'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	['_or'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '!=',
	value =true,
},
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '!=',
	value =true,
},
	},
},
	},
},
	},
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '!=',
	value =true,
},
	},
},
	},
},
	},
},
			postcondition ={
	['_or'] = {
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
	},
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'use_skill',
			description = '使用技能',
			priority = 4,
			weight = 4,
			precondition ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
	},
},
		},
		{
			name = 'combat_idle',
			description = '战斗待机',
			priority = 4,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =false,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'hit_silence',
			description = '受击沉默',
			priority = 4,
			weight = 3,
			precondition ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
		},
	},
})

monster_ai:upsert({
	monster_id = 30007,
	remark = '土匪（高瘦）-新AI(基于-30005)',
	nearby_enemy_distance = 250,
	nearby_spawn_distance = 50,
	wander = true,
	amuse = false,
	standoff_duration = { 1000,3000 },
	standoff_back_distance = 300,
	standoff_back_precision = 20,
	standoff_move_duration = { 900,3000 },
	standoff_move_radius = 300,
	wander_radius = 700,
	hate_attenuate = 0,
	return_spawn_speed = 1000,
	chase_speed = 400,
	chase_distance = 2000,
	chase_scope = 2500,
	guard_circle_radius = 1000,
	guard_sector_radius = 2000,
	guard_sector_angle = 360,
	combat_delivery_radius = 1,
	combat_delivery_duration = 1,
	combat_delivery_amount = 1,
	patrol_type = 0,
	patrol_path = { },
	state_duration = {30,70 },
	actions = {
		{
			name = '激活(AI)',
			priority = 1,
			cost = 1,
			code = 'activate',
			precondition ={
	['_or'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='inactivite',
},
		{
	owner = 'entity',
	property ='is_activated',
	operator = '!=',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='state',
	operator = '!=',
	value ='inactive',
},
	},
},
		},
		{
			name = '待机站立',
			priority = 2,
			cost = 1,
			code = 'standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='rest_idle',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = '漫游移动',
			priority = 2,
			cost = 1,
			code = 'move_wander',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='wander_idle',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = '向敌人移动',
			priority = 3,
			cost = 1,
			code = 'move_to_enemy',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='chase',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '向出生点移动',
			priority = 4,
			cost = 2,
			code = 'move_to_spawn',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='goto_spawn',
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '!=',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '对峙后退',
			priority = 6,
			cost = 1,
			code = 'standoff_back',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='standoff',
},
		{
	owner = 'entity',
	property ='can_standoff_back',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='can_standoff_horizontal',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '对峙横移',
			priority = 6,
			cost = 1,
			code = 'standoff_horizontal',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='standoff',
},
		{
	owner = 'entity',
	property ='can_standoff_horizontal',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '攻击敌人',
			priority = 6,
			cost = 2,
			code = 'attack_enemy',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='use_skill',
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
	},
},
		},
		{
			name = '生命值回满',
			priority = 4,
			cost = 1,
			code = 'fully_hp',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='goto_spawn',
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '战斗站立',
			priority = 5,
			cost = 1,
			code = 'combat_standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='combat_idle',
},
	},
},
			effect ={
	['_or'] = {
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '沉默站立',
			priority = 5,
			cost = 1,
			code = 'silence_standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='hit_silence',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
		},
	},
	states = {
		{
			name = 'inactive',
			description = '未激活',
			priority = 1,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '!=',
	value =true,
},
	},
},
			postcondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'rest_idle',
			description = '休闲待机',
			priority = 2,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	['_or'] = {
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
	},
},
	},
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = 'wander_idle',
			description = '漫游待机',
			priority = 2,
			weight = 2,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_wander',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = 'chase',
			description = '追击',
			priority = 3,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '!=',
	value =true,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='is_in_standoff_distance',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='is_skill_using',
	operator = '==',
	value =false,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'goto_spawn',
			description = '回出生点',
			priority = 3,
			weight = 2,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	['_or'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	['_or'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '!=',
	value =true,
},
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '!=',
	value =true,
},
	},
},
	},
},
	},
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '!=',
	value =true,
},
	},
},
	},
},
	},
},
			postcondition ={
	['_or'] = {
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
	},
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'use_skill',
			description = '使用技能',
			priority = 4,
			weight = 4,
			precondition ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
	},
},
		},
		{
			name = 'combat_idle',
			description = '战斗待机',
			priority = 4,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =false,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'hit_silence',
			description = '受击沉默',
			priority = 4,
			weight = 3,
			precondition ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
		},
	},
})

monster_ai:upsert({
	monster_id = 30008,
	remark = '土匪（高壮）-新AI(基于-30006)',
	nearby_enemy_distance = 350,
	nearby_spawn_distance = 50,
	wander = true,
	amuse = false,
	standoff_duration = { 1000,3000 },
	standoff_back_distance = 350,
	standoff_back_precision = 20,
	standoff_move_duration = { 900,3000 },
	standoff_move_radius = 300,
	wander_radius = 700,
	hate_attenuate = 0,
	return_spawn_speed = 500,
	chase_speed = 400,
	chase_distance = 2000,
	chase_scope = 2500,
	guard_circle_radius = 1000,
	guard_sector_radius = 2000,
	guard_sector_angle = 360,
	combat_delivery_radius = 1,
	combat_delivery_duration = 1,
	combat_delivery_amount = 1,
	patrol_type = 0,
	patrol_path = { },
	state_duration = {30,70 },
	actions = {
		{
			name = '激活(AI)',
			priority = 1,
			cost = 1,
			code = 'activate',
			precondition ={
	['_or'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='inactivite',
},
		{
	owner = 'entity',
	property ='is_activated',
	operator = '!=',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='state',
	operator = '!=',
	value ='inactive',
},
	},
},
		},
		{
			name = '待机站立',
			priority = 2,
			cost = 1,
			code = 'standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='rest_idle',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = '漫游移动',
			priority = 2,
			cost = 1,
			code = 'move_wander',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='wander_idle',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = '向敌人移动',
			priority = 3,
			cost = 1,
			code = 'move_to_enemy',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='chase',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '向出生点移动',
			priority = 4,
			cost = 2,
			code = 'move_to_spawn',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='goto_spawn',
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '!=',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '对峙后退',
			priority = 6,
			cost = 1,
			code = 'standoff_back',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='standoff',
},
		{
	owner = 'entity',
	property ='can_standoff_back',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='can_standoff_horizontal',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '对峙横移',
			priority = 6,
			cost = 1,
			code = 'standoff_horizontal',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='standoff',
},
		{
	owner = 'entity',
	property ='can_standoff_horizontal',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '攻击敌人',
			priority = 6,
			cost = 2,
			code = 'attack_enemy',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='use_skill',
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
	},
},
		},
		{
			name = '生命值回满',
			priority = 4,
			cost = 1,
			code = 'fully_hp',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='goto_spawn',
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '战斗站立',
			priority = 5,
			cost = 1,
			code = 'combat_standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='combat_idle',
},
	},
},
			effect ={
	['_or'] = {
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = '沉默站立',
			priority = 5,
			cost = 1,
			code = 'silence_standby',
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='state',
	operator = '==',
	value ='hit_silence',
},
	},
},
			effect ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
		},
	},
	states = {
		{
			name = 'inactive',
			description = '未激活',
			priority = 1,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '!=',
	value =true,
},
	},
},
			postcondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'rest_idle',
			description = '休闲待机',
			priority = 2,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	['_or'] = {
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
	},
},
	},
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = 'wander_idle',
			description = '漫游待机',
			priority = 2,
			weight = 2,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_wander',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
	},
},
		},
		{
			name = 'chase',
			description = '追击',
			priority = 3,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '!=',
	value =true,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='is_in_standoff_distance',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='is_skill_using',
	operator = '==',
	value =false,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '!=',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'goto_spawn',
			description = '回出生点',
			priority = 3,
			weight = 2,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_activated',
	operator = '==',
	value =true,
},
		{
	['_or'] = {
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '!=',
	value =true,
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	['_or'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '!=',
	value =true,
},
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='can_wander',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '!=',
	value =true,
},
	},
},
	},
},
	},
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '!=',
	value =true,
},
	},
},
	},
},
	},
},
			postcondition ={
	['_or'] = {
		{
	['_and'] = {
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
	},
},
		{
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'enemy',
	property ='is_in_chase_distance',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_in_spawn_scope',
	operator = '==',
	value =true,
},
	},
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_hp_full',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_spawn',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'use_skill',
			description = '使用技能',
			priority = 4,
			weight = 4,
			precondition ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_nearby_enemy',
	operator = '!=',
	value =true,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
	},
},
		},
		{
			name = 'combat_idle',
			description = '战斗待机',
			priority = 4,
			weight = 1,
			precondition ={
	['_and'] = {
		{
	owner = 'entity',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =false,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =false,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
		{
	owner = 'entity',
	property ='can_use_skill',
	operator = '==',
	value =true,
},
	},
},
			enable_goal ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='can_standoff',
	operator = '==',
	value =true,
},
	},
},
		},
		{
			name = 'hit_silence',
			description = '受击沉默',
			priority = 4,
			weight = 3,
			precondition ={
	['_and'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '!=',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =true,
},
	},
},
			postcondition ={
	['_or'] = {
		{
	owner = 'enemy',
	property =nil,
	operator = '==',
	value =nil,
},
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
			enable_goal ={
	['_and'] = {
		{
	owner = 'entity',
	property ='is_silence',
	operator = '==',
	value =false,
},
	},
},
		},
	},
})

monster_ai:update()
