--utility_cfg.lua
--source: 0_utility_全局常量表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local utility = config_mgr:get_table("utility")

--导出配置内容
utility:upsert({
    id=1,
    key='flush_day_hour',
    value='0'
})

utility:upsert({
    id=2,
    key='flush_week_day',
    value='1'
})

utility:upsert({
    id=7,
    key='bag_capacity',
    value='4000'
})

utility:upsert({
    id=8,
    key='max_capacity',
    value='60000'
})

utility:upsert({
    id=9,
    key='shut_capacity',
    value='8'
})

utility:upsert({
    id=10,
    key='equip_capacity',
    value='12'
})

utility:upsert({
    id=11,
    key='bank_capacity',
    value='6000'
})

utility:upsert({
    id=20,
    key='server_refresh_time',
    value='1661979599'
})

utility:upsert({
    id=21,
    key='server_open_time',
    value='1661997600'
})

utility:upsert({
    id=22,
    key='game_open_time',
    value='1661986800'
})

utility:upsert({
    id=23,
    key='time_elapse_rate',
    value='{0,0,6,20}'
})

utility:upsert({
    id=24,
    key='common_cd_time',
    value='300'
})

utility:upsert({
    id=31,
    key='coin_id',
    value='100001'
})

utility:upsert({
    id=32,
    key='energy_id',
    value='100002'
})

utility:upsert({
    id=33,
    key='diamond_id',
    value='100003'
})

utility:upsert({
    id=34,
    key='exp_id',
    value='100004'
})

utility:upsert({
    id=41,
    key='100001',
    value='31'
})

utility:upsert({
    id=43,
    key='100003',
    value='33'
})

utility:upsert({
    id=44,
    key='100004',
    value='34'
})

utility:upsert({
    id=45,
    key='mail_box_limit',
    value='100'
})

utility:upsert({
    id=46,
    key='fav_box_limit',
    value='100'
})

utility:upsert({
    id=47,
    key='del_mail_time',
    value='14'
})

utility:upsert({
    id=48,
    key='npc_recruit_time',
    value='3600'
})

utility:upsert({
    id=49,
    key='npc_recruit_wait',
    value='1800'
})

utility:upsert({
    id=50,
    key='custompoint_limit',
    value='50'
})

utility:upsert({
    id=51,
    key='block_work_limit',
    value='99'
})

utility:upsert({
    id=52,
    key='energy_resume_time',
    value='600'
})

utility:upsert({
    id=53,
    key='town_exp_id',
    value='100006'
})

utility:upsert({
    id=54,
    key='npc_start_id',
    value='100000'
})

utility:upsert({
    id=55,
    key='affinity_id',
    value='100007'
})

utility:upsert({
    id=56,
    key='world_chat_count',
    value='500'
})

utility:upsert({
    id=57,
    key='chat_world_count',
    value='3'
})

utility:upsert({
    id=58,
    key='chat_world_time',
    value='60'
})

utility:upsert({
    id=59,
    key='chat_scene_count',
    value='3'
})

utility:upsert({
    id=60,
    key='chat_scene_time',
    value='30'
})

utility:upsert({
    id=61,
    key='chat_whis_limit',
    value='10'
})

utility:upsert({
    id=62,
    key='chat_whis_time',
    value='604800'
})

utility:upsert({
    id=63,
    key='mail_item_limit',
    value='10'
})

utility:upsert({
    id=64,
    key='common_recruit',
    value='1001'
})

utility:upsert({
    id=65,
    key='friend_formal_limit',
    value='200'
})

utility:upsert({
    id=66,
    key='friend_black_limit',
    value='100'
})

utility:upsert({
    id=67,
    key='friend_apply_limit',
    value='100'
})

utility:upsert({
    id=68,
    key='friend_apply_time',
    value='2592000'
})

utility:upsert({
    id=69,
    key='npc_generate_pos',
    value='{\n    { pos={121504,12167,88388}, dir={0,-9224,0}, },\n { pos={121504,12167,88188}, dir={0,-9224,0}, },\n { pos={121504,12167,88288}, dir={0,-9224,0}, },\n { pos={121504,12167,88488}, dir={0,-9224,0}, },\n { pos={121504,12167,88588}, dir={0,-9224,0}, },\n { pos={121504,12167,88688}, dir={0,-9224,0}, },\n { pos={121504,12167,88788}, dir={0,-9224,0}, },\n { pos={121504,12167,88888}, dir={0,-9224,0}, },\n { pos={121504,12167,88988}, dir={0,-9224,0}, },\n}'
})

utility:upsert({
    id=70,
    key='acty_id',
    value='100008'
})

utility:upsert({
    id=71,
    key='card_cache_limit',
    value='10000'
})

utility:upsert({
    id=72,
    key='card_page_size',
    value='12'
})

utility:upsert({
    id=73,
    key='card_lv_limit',
    value='100'
})

utility:upsert({
    id=74,
    key='card_lang_limit',
    value='100'
})

utility:upsert({
    id=75,
    key='chat_report_count',
    value='20'
})

utility:upsert({
    id=76,
    key='chat_report_time',
    value='86400'
})

utility:upsert({
    id=77,
    key='world_chat_line',
    value='5'
})

utility:upsert({
    id=78,
    key='world_chat_count',
    value='500'
})

utility:upsert({
    id=79,
    key='realtime_mission_require',
    value='1016'
})

utility:upsert({
    id=80,
    key='chcom_name_len',
    value='30'
})

utility:upsert({
    id=81,
    key='chcom_code_len',
    value='5'
})

utility:upsert({
    id=82,
    key='chcom_notice_len',
    value='600'
})

utility:upsert({
    id=83,
    key='chcom_reclist_count',
    value='5'
})

utility:upsert({
    id=84,
    key='chcom_apply_count',
    value='5'
})

utility:upsert({
    id=85,
    key='chcom_apply_time',
    value='86400'
})

utility:upsert({
    id=86,
    key='chcom_leave_time',
    value='30'
})

utility:upsert({
    id=87,
    key='chcom_invite_time',
    value='86400'
})

utility:upsert({
    id=88,
    key='chcom_diss_time',
    value='60'
})

utility:upsert({
    id=89,
    key='chcom_event_count',
    value='200'
})

utility:upsert({
    id=90,
    key='max_build_height',
    value='20'
})

utility:upsert({
    id=91,
    key='floor_height',
    value='4'
})

utility:upsert({
    id=92,
    key='wall_height',
    value='1'
})

utility:upsert({
    id=93,
    key='gift_stage_1',
    value='4'
})

utility:upsert({
    id=94,
    key='gift_stage_2',
    value='8'
})

utility:upsert({
    id=95,
    key='chat_length',
    value='90'
})

utility:upsert({
    id=96,
    key='role_name_length',
    value='24'
})

utility:upsert({
    id=97,
    key='store_box_length',
    value='24'
})

utility:upsert({
    id=98,
    key='bank_history_count',
    value='200'
})

utility:upsert({
    id=99,
    key='res_pool_max',
    value='200'
})

utility:upsert({
    id=100,
    key='npc_pool_max',
    value='200'
})

utility:upsert({
    id=101,
    key='monster_pool_max',
    value='200'
})

utility:upsert({
    id=102,
    key='crcy_sp_enable',
    value='0'
})

utility:upsert({
    id=103,
    key='item_sp_enable',
    value='1'
})

utility:upsert({
    id=104,
    key='crcy_sp_conf',
    value='{[1]=60,[2]=20,[3]=60,[4]=1,[5]=1}'
})

utility:upsert({
    id=105,
    key='crcy_sp_item_id',
    value='100003'
})

utility:upsert({
    id=106,
    key='delivery_costs',
    value='{[103011]=1}'
})

utility:upsert({
    id=107,
    key='min_fallling_height',
    value='500'
})

utility:upsert({
    id=108,
    key='falling_precent',
    value='2'
})

utility:upsert({
    id=109,
    key='recovery_time',
    value='180'
})

utility:upsert({
    id=110,
    key='recovery_num',
    value='10'
})

utility:upsert({
    id=111,
    key='gather_offline_time',
    value='86400'
})

utility:upsert({
    id=112,
    key='gather_cycle_time',
    value='3600'
})

utility:upsert({
    id=113,
    key='npc_talk_max',
    value='50'
})

utility:upsert({
    id=114,
    key='sat_id',
    value='100009'
})

utility:upsert({
    id=115,
    key='fishing_low',
    value='10'
})

utility:upsert({
    id=116,
    key='fishing_middle',
    value='20'
})

utility:upsert({
    id=117,
    key='fishing_high',
    value='30'
})

utility:upsert({
    id=118,
    key='lift_stop_time',
    value='8'
})

utility:upsert({
    id=119,
    key='lift_run_time',
    value='10'
})

utility:upsert({
    id=120,
    key='skill_prod_coeff',
    value='{90,110,2}'
})

utility:upsert({
    id=121,
    key='ground_ruins_special',
    value='{30008,30009,30012,30013,30014,30015,30019}'
})

utility:upsert({
    id=122,
    key='account_max',
    value='0'
})

utility:upsert({
    id=123,
    key='tut_interval',
    value='1000'
})

utility:upsert({
    id=124,
    key='official_world_count',
    value='5'
})

utility:upsert({
    id=125,
    key='dedicated_world_count',
    value='10'
})

utility:update()
