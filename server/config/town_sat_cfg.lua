--town_sat_cfg.lua
--source: 6_town_sat_城镇满意度.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local town_sat = config_mgr:get_table("town_sat")

--导出配置内容
town_sat:upsert({
    effect_ids={
        30000002,
        30000004
    },
    end_value=20,
    id=1,
    sat_lv=1,
    start_value=0
})

town_sat:upsert({
    end_value=70,
    id=2,
    sat_lv=2,
    start_value=20
})

town_sat:upsert({
    effect_ids={
        30000001,
        30000003
    },
    end_value=100,
    id=3,
    sat_lv=3,
    start_value=70
})

town_sat:update()
