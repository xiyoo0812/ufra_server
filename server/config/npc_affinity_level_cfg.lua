--npc_affinity_level_cfg.lua
--source: 3_npc_affinity_level_NPC好感度等级.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local npc_affinity_level = config_mgr:get_table("npc_affinity_level")

--导出配置内容
npc_affinity_level:upsert({
    affinity=50,
    id=1,
    level=1,
    name='LC_attribute_att_30'
})

npc_affinity_level:upsert({
    affinity=100,
    id=2,
    level=2,
    name='LC_attribute_att_31'
})

npc_affinity_level:upsert({
    affinity=500,
    id=3,
    level=3,
    name='LC_attribute_att_32'
})

npc_affinity_level:upsert({
    affinity=1000,
    id=4,
    level=4,
    name='LC_attribute_att_33'
})

npc_affinity_level:upsert({
    affinity=2000,
    id=5,
    level=5,
    name='LC_attribute_att_34'
})

npc_affinity_level:upsert({
    affinity=5000,
    id=6,
    level=6,
    name='LC_attribute_att_35'
})

npc_affinity_level:upsert({
    affinity=8000,
    id=7,
    level=7,
    name='LC_attribute_att_36'
})

npc_affinity_level:upsert({
    affinity=8000,
    id=8,
    level=8,
    name='LC_attribute_att_37'
})

npc_affinity_level:update()
