--activity_item_exchange_cfg.lua
--source: 12_activity_item_exchange_物品兑换表.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local activity_item_exchange = config_mgr:get_table("activity_item_exchange")

--导出配置内容
activity_item_exchange:upsert({
    activity_id=1020,
    child_id=1,
    costs={
        [100001]=50
    },
    id=1,
    max_count=1,
    obtains={
        [100002]=60
    }
})

activity_item_exchange:upsert({
    activity_id=1020,
    child_id=2,
    costs={
        [100001]=200
    },
    id=2,
    max_count=1,
    obtains={
        [100002]=60
    }
})

activity_item_exchange:update()
