--activity_survey_cfg.lua
--source: 13_activity_survey_问卷调查.xlsx
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local activity_survey = config_mgr:get_table("activity_survey")

--导出配置内容
activity_survey:upsert({
    activity_id=1060,
    cdkey_type=0,
    child_id=1,
    day=1,
    id=100,
    required=true,
    submit_status=false
})

activity_survey:upsert({
    activity_id=1060,
    cdkey_type=0,
    child_id=1,
    day=1,
    id=101,
    required=true,
    submit_status=false
})

activity_survey:upsert({
    activity_id=1060,
    cdkey_type=0,
    child_id=1,
    day=1,
    id=102,
    required=true,
    submit_status=false
})

activity_survey:upsert({
    activity_id=1060,
    cdkey_type=0,
    child_id=2,
    day=2,
    id=103,
    required=true,
    submit_status=true
})

activity_survey:upsert({
    activity_id=1060,
    cdkey_type=0,
    child_id=2,
    day=2,
    id=104,
    required=true,
    submit_status=true
})

activity_survey:upsert({
    activity_id=1060,
    cdkey_type=0,
    child_id=2,
    day=2,
    id=105,
    required=true,
    submit_status=true
})

activity_survey:upsert({
    activity_id=1060,
    cdkey_type=0,
    child_id=2,
    day=2,
    id=106,
    required=true,
    submit_status=true
})

activity_survey:upsert({
    activity_id=1060,
    cdkey_type=0,
    child_id=2,
    day=2,
    id=107,
    required=true,
    submit_status=true
})

activity_survey:update()
