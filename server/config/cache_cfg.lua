--cache_cfg.lua
--source: cache.xlsm
--luacheck: ignore 631

--获取配置表
local config_mgr = quanta.get("config_mgr")
local cache = config_mgr:get_table("cache")

--导出配置内容
cache:upsert({
    copyable=false,
    count=100,
    group='account',
    id=1,
    inertable=false,
    key='open_id',
    sheet='account',
    time=600
})

cache:upsert({
    copyable=true,
    count=100,
    group='player',
    id=2,
    inertable=false,
    key='player_id',
    sheet='player',
    time=600
})

cache:upsert({
    copyable=true,
    count=200,
    group='player',
    id=3,
    inertable=false,
    key='player_id',
    sheet='player_attr',
    time=600
})

cache:upsert({
    copyable=true,
    count=200,
    group='player',
    id=4,
    inertable=false,
    key='player_id',
    sheet='player_item',
    time=600
})

cache:upsert({
    copyable=true,
    count=200,
    group='player',
    id=5,
    inertable=false,
    key='player_id',
    sheet='player_task',
    time=600
})

cache:upsert({
    copyable=true,
    count=100,
    group='lobby',
    id=6,
    inertable=false,
    key='player_id',
    sheet='player_mail',
    time=600
})

cache:upsert({
    copyable=true,
    count=100,
    group='lobby',
    id=7,
    inertable=false,
    key='player_id',
    sheet='player_newbees',
    time=600
})

cache:upsert({
    copyable=true,
    count=100,
    group='lobby',
    id=8,
    inertable=false,
    key='player_id',
    sheet='player_reddots',
    time=600
})

cache:upsert({
    copyable=true,
    count=100,
    group='lobby',
    id=9,
    inertable=false,
    key='player_id',
    sheet='player_clirewards',
    time=600
})

cache:upsert({
    copyable=true,
    count=200,
    group='lobby',
    id=10,
    inertable=false,
    key='player_id',
    sheet='player_activity',
    time=600
})

cache:upsert({
    copyable=true,
    count=100,
    group='lobby',
    id=11,
    inertable=true,
    key='player_id',
    sheet='player_talk',
    time=600
})

cache:upsert({
    copyable=true,
    count=100,
    group='lobby',
    id=12,
    inertable=true,
    key='player_id',
    sheet='player_shop',
    time=600
})

cache:upsert({
    copyable=true,
    count=100,
    group='lobby',
    id=13,
    inertable=true,
    key='player_id',
    sheet='player_settings',
    time=600
})

cache:upsert({
    copyable=true,
    count=100,
    group='lobby',
    id=14,
    inertable=false,
    key='player_id',
    sheet='player_pictorials',
    time=600
})

cache:upsert({
    copyable=true,
    count=100,
    group='lobby',
    id=15,
    inertable=false,
    key='player_id',
    sheet='player_cpositions',
    time=600
})

cache:upsert({
    copyable=true,
    count=200,
    group='lobby',
    id=16,
    inertable=false,
    key='player_id',
    sheet='player_savings',
    time=600
})

cache:upsert({
    copyable=true,
    count=200,
    group='lobby',
    id=17,
    inertable=false,
    key='player_id',
    sheet='player_open',
    time=600
})

cache:upsert({
    copyable=true,
    count=200,
    group='lobby',
    id=18,
    inertable=false,
    key='player_id',
    sheet='player_areas',
    time=600
})

cache:upsert({
    copyable=true,
    count=200,
    group='lobby',
    id=19,
    inertable=false,
    key='player_id',
    sheet='player_push',
    time=600
})

cache:upsert({
    copyable=true,
    count=100,
    group='world',
    id=20,
    inertable=false,
    key='world_id',
    sheet='home_collpoints',
    time=600
})

cache:upsert({
    copyable=true,
    count=100,
    group='world',
    id=21,
    inertable=false,
    key='world_id',
    sheet='home_ground',
    time=600
})

cache:upsert({
    copyable=true,
    count=100,
    group='world',
    id=22,
    inertable=false,
    key='world_id',
    sheet='home_building',
    time=600
})

cache:upsert({
    copyable=true,
    count=100,
    group='world',
    id=23,
    inertable=false,
    key='world_id',
    sheet='home_produce',
    time=600
})

cache:upsert({
    copyable=true,
    count=100,
    group='world',
    id=24,
    inertable=false,
    key='world_id',
    sheet='home_utensil',
    time=600
})

cache:upsert({
    copyable=false,
    count=100,
    group='copy',
    id=25,
    inertable=false,
    key='copy_id',
    sheet='copy',
    time=600
})

cache:upsert({
    copyable=true,
    count=100,
    group='player_world',
    id=26,
    inertable=false,
    key='player_id',
    sheet='player_partner',
    time=600
})

cache:upsert({
    copyable=true,
    count=100,
    group='character_world',
    id=27,
    inertable=false,
    key='uni_id',
    sheet='character_world',
    time=600
})

cache:update()
