--init.lua
import("common/constant.lua")
import("agent/gm_agent.lua")
import("agent/online_agent.lua")
import("worldm/dao/world_dao.lua")

import("worldm/object/object_factory.lua")
import("worldm/core/agent_mgr.lua")
import("worldm/core/mirror_mgr.lua")
import("worldm/core/list_mgr.lua")
import("worldm/core/world_mgr.lua")
import("worldm/servlet/world_servlet.lua")

import("worldm/gm/worldmgr_gm.lua")