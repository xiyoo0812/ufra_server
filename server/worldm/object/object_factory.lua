--world.lua
local log_err       = logger.err
local new_guid      = codec.guid_new

local protobuf_mgr  = quanta.get("protobuf_mgr")

local WD_OFFICIAL   = protobuf_mgr:enum("world_type", "WORLD_OFFICIAL")
local WD_DEDICATED  = protobuf_mgr:enum("world_type", "WORLD_DEDICATED")
local WD_AGENT      = protobuf_mgr:enum("world_type", "WORLD_AGENT")

local ObjectFactory = singleton()
function ObjectFactory:__init()
end

function ObjectFactory:create_world(id, type)
    local uid = id or new_guid()
    if type == WD_OFFICIAL then
        local world = import("worldm/object/official_world.lua")
        return world(uid)
    elseif type == WD_DEDICATED then
        local world = import("worldm/object/dedicated_world.lua")
        return world(uid)
    elseif type == WD_AGENT then
        local world = import("worldm/object/mirror_world.lua")
        return world(uid)
    end
    log_err("[ObjectFactory][create_world] type is err id:{}", id)
    return nil
end

quanta.object_fty = ObjectFactory()
return ObjectFactory
