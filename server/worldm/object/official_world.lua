--official_world.lua(官服)
local log_err       = logger.err
local log_debug     = logger.debug
local log_warn      = logger.warn
local qfailed       = quanta.failed

local router_mgr    = quanta.get("router_mgr")
local protobuf_mgr  = quanta.get("protobuf_mgr")
local agent_mgr     = quanta.get("agent_mgr")
local world_mgr     = quanta.get("world_mgr")

local FRAME_SUCCESS = protobuf_mgr:error_code("FRAME_SUCCESS")
local FRAME_FAILED  = protobuf_mgr:error_code("FRAME_FAILED")

local World         = import("worldm/object/world.lua")
local OfficialWorld = class(World)
function OfficialWorld:__init()

end

function OfficialWorld:login(user_id)
    if self:check_sync_agent() then
        log_debug("[OfficialWorld][login] check_sync_agent is true user_id:{} world:{} agent:{}!",
            user_id, self:get_id(), self.agent_srv)
        return FRAME_SUCCESS
    end

     -- 代理服已分配,且未向其同步word信息
     if self:check_agent_srv() and not self:check_sync_agent() then
        local ok, code = router_mgr:call_target(self.agent_srv, "rpc_world_login", self:pack_agent())
        if not qfailed(code, ok) then
            self:set_sync_agent(true)
            log_debug("[OfficialWorld][login] rpc_world_login success user_id:{} world:{} agent:{} code:{}!",
                user_id, self:get_id(), self.agent_srv, code)
            return FRAME_SUCCESS
        end
    end

    local agent = agent_mgr:allot()
    if not agent then
        log_err("[OfficialWorld][login] agent is nil id:{} world:{}", user_id, self:get_id())
        return FRAME_FAILED
    end

    local c_ok, code = router_mgr:call_target(agent.id, "rpc_world_login", self:pack_agent())
    if qfailed(code, c_ok) then
        log_err("[OfficialWorld][login] rpc_world_login is fail user_id:{} world:{} agent:{} code:{}!",
            user_id, self:get_id(), agent.id, code)
        return code
    end

    log_debug("[OfficialWorld][login] rpc_world_login success user_id:{} world:{} agent:{}!",
            user_id, self:get_id(), self.agent_srv)
    self:set_agent(agent.id)
    return FRAME_SUCCESS
end

function OfficialWorld:delete()
 -- 通知删除
 local ok, code = router_mgr:call_target(self.agent_srv, "rpc_world_delete")
 if qfailed(code, ok) then
    log_warn("[OfficialWorld][delete] rpc_world_delete is fail, world:{} user_id:{} agent_srv:{} code:{}",
        self.id, self.user_id, self.agent_srv, code)
    return
 end
 log_debug("[OfficialWorld][delete] success, world:{} user_id:{} agent_srv:{} code:{}",
        self.id, self.user_id, self.agent_srv, code)
end

function OfficialWorld:offline()
    World.offline(self)
    self.sync_agent = false
end

function OfficialWorld:encode_token()
    self.token = world_mgr:rsa_encode(self.password)
end

function OfficialWorld:pack_client()
    local result = World.pack_client(self)
    local agent = agent_mgr:get_agent(self.agent_srv)
    if agent then
        result.agent_url = agent.url
    end
    return result
end

return OfficialWorld
