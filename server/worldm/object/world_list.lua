--world_list.lua
local tinsert       = table.insert

local protobuf_mgr  = quanta.get("protobuf_mgr")

local WD_NULL       = protobuf_mgr:enum("world_type", "WORLD_NULL")
local WD_OFFICIAL   = protobuf_mgr:enum("world_type", "WORLD_OFFICIAL")
local WD_DEDICATED  = protobuf_mgr:enum("world_type", "WORLD_DEDICATED")
local WD_AGENT      = protobuf_mgr:enum("world_type", "WORLD_AGENT")

local WS_ONLINE     = protobuf_mgr:enum("world_state", "WS_ONLINE")
local WS_OFFLINE    = protobuf_mgr:enum("world_state", "WS_OFFLINE")

local WorldList     = class()
local prop          = property(WorldList)
prop:accessor("id", 0)
prop:accessor("type", WD_NULL)
prop:accessor("name", "")
prop:accessor("user_id", 0)
prop:accessor("max_count", 0)
prop:accessor("password", "")
prop:accessor("agent_srv", 0)
prop:accessor("agent_url", "")
prop:accessor("invcode", "")
prop:accessor("private", false)
prop:accessor("setting", {})
prop:accessor("create_time", 0)
-- cache
prop:accessor("state", WS_OFFLINE)
prop:accessor("player_count", 0)
prop:accessor("offline_time", 0)

function WorldList:__init(id)
    self.id = id;
end

function WorldList:init_from_config(data, setting)
    self.type = data.type
    self.name = data.name
    self.user_id = data.user_id
    self.max_count = data.max_count
    self.password = data.password
    self.agent_srv = data.agent_srv
    self.agent_url = data.agent_url
    self.invcode = data.invcode
    self.setting = setting
    self.private = data.private
    self.create_time = data.create_time
    self.state = WS_ONLINE
end

function WorldList:init_from_db(dbdata, setting)
    self.type = dbdata.type
    self.name = dbdata.name
    self.user_id = dbdata.user_id
    self.max_count = dbdata.max_count
    self.password = dbdata.password
    self.agent_srv = dbdata.agent_srv
    self.agent_url = dbdata.agent_url
    self.invcode = dbdata.invcode
    self.setting = setting
    self.private = dbdata.private
    self.create_time = dbdata.create_time
    if self.type == WD_OFFICIAL or self.type == WD_AGENT then
        self.state = WS_ONLINE
    end
end

function WorldList:online()
    self.state = WS_ONLINE
end

function WorldList:heartbeat()
    self.state = WS_ONLINE
end

function WorldList:offline()
    if self.type == WD_DEDICATED then
        self.state = WS_OFFLINE
    end
    self.player_count = 0
end

function WorldList:is_private()
    return self.private
end

function WorldList:reset()
    self.player_count = 0
end

function WorldList:set_attrs(world)
    local sync = false
    if self.name ~= world.name or self.max_count ~= world.max_count or self.password ~= world.password then
        sync = true
    end
    self.name = world.name
    self.max_count = world.max_count
    self.password = world.password
    if world.setting and next(world.setting) then
        for _, item in pairs(world.setting) do
            self.setting[item.type] = item
            sync = true
        end
    end
    return sync
end

function WorldList:encode_setting(setting)
    local value = setting.value
    local set_type = type(value)
    if set_type == "number" then
        return { type = setting.type, attr_i = value }
    end
    if set_type == "string" then
        return { type = setting.type, attr_s = value }
    end
    return { type = setting.type, attr_b = value }
end

function WorldList:pack_setting()
    if not self.setting or self.setting == "" then
        return {}
    end
    local result = {}
    for _, item in pairs(self.setting) do
        local setting = self:encode_setting(item)
        tinsert(result, setting)
    end
    return result
end

function WorldList:pack_service()
    local result = {
        id = self.id,
        type = self.type,
        name = self.name,
        user_id = self.user_id,
        max_count = self.max_count,
        password = self.password,
        setting = self.setting,
        create_time = self.create_time
    }
    return result
end

function WorldList:pack_mylist()
    local result = {
        id = self.id,
        name = self.name,
        type = self.type,
        max_count = self.max_count,
        user_id = self.user_id,
        pw_valid = self.password ~= "" and true or false,
        state = self.state,
        player_count = self.player_count,
        offline_time = self.offline_time,
        setting = self:pack_setting(),
        invcode = self.invcode
    }
    return result
end

function WorldList:pack_client()
    local result = {
        id = self.id,
        name = self.name,
        type = self.type,
        max_count = self.max_count,
        user_id = self.user_id,
        pw_valid = self.password ~= "" and true or false,
        state = self.state,
        player_count = self.player_count,
        offline_time = self.offline_time,
        setting = self:pack_setting()
    }
    return result
end

return WorldList
