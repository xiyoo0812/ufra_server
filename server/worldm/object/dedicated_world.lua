--dedicated_world.lua(社群)
local world_mgr    = quanta.get("world_mgr")
local World        = import("worldm/object/world.lua")

local DedicatedWorld  = class(World)
function DedicatedWorld:__init()
end

function DedicatedWorld:setup()
    World.setup(self)
    if self:is_private() then
        self:create_invcode()
    end
end


function DedicatedWorld:offline()
end

function DedicatedWorld:packdb()
    local result = World.packdb(self)
    result.agent_url = self.agent_url
    return result
end

function DedicatedWorld:encode_token()
    self.token = world_mgr:rsa_encode(self.password)
end

function DedicatedWorld:pack_client()
    local result = World.pack_client(self)
    result.agent_url = self.agent_url
    return result
end
return DedicatedWorld
