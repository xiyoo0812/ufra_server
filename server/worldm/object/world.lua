--world.lua
local log_err       = logger.err
local log_debug     = logger.debug
local qfailed       = quanta.failed

local protobuf_mgr  = quanta.get("protobuf_mgr")
local world_dao     = quanta.get("world_dao")
local online        = quanta.get("online")
local router_mgr    = quanta.get("router_mgr")

local WD_NULL       = protobuf_mgr:enum("world_type", "WORLD_NULL")
local WD_OFFICIAL   = protobuf_mgr:enum("world_type", "WORLD_OFFICIAL")
local WD_DEDICATED  = protobuf_mgr:enum("world_type", "WORLD_DEDICATED")
local WD_AGENT      = protobuf_mgr:enum("world_type", "WORLD_AGENT")

local WS_NULL       = protobuf_mgr:enum("world_state", "WS_NULL")
local WS_ONLINE     = protobuf_mgr:enum("world_state", "WS_ONLINE")
local WS_OFFLINE    = protobuf_mgr:enum("world_state", "WS_OFFLINE")

local FRAME_SUCCESS = protobuf_mgr:error_code("FRAME_SUCCESS")

local World         = class()
local prop          = property(World)
prop:accessor("id", 0)
prop:accessor("type", WD_NULL)
prop:accessor("name", "")
prop:accessor("max_count", 0)
prop:accessor("password", "")
prop:accessor("agent_url", "")
prop:accessor("setting", "")
prop:accessor("user_id", 0)
prop:accessor("create_time", 0)
prop:accessor("agent_srv", 0)
prop:accessor("offline_time", 0)
prop:accessor("state", WS_NULL)
prop:accessor("invcode", "")
prop:accessor("private", false)
prop:reader("db_updates", {})
prop:accessor("sync_agent", false)
prop:accessor("token", "")

function World:__init(id)
    self.id = id;
end

function World:init_from_config(user_id, data)
    self.type = data.type
    self.name = data.name
    self.user_id = user_id
    self.max_count = data.max_count
    self.password = data.password or ""
    self.token = self.password
    self.agent_url = data.agent_url or ""
    self.private = data.private or false
    self.setting = data.setting or {}
    self.create_time = quanta.now
    self:encode_token()
end

function World:init_from_db(dbdata)
    self.type = dbdata.type
    self.name = dbdata.name
    self.max_count = dbdata.max_count;
    self.password = dbdata.password or ""
    self.token = self.password
    self.agent_url = dbdata.agent_url or ""
    self.agent_srv = dbdata.agent_srv or 0
    self.setting = dbdata.setting or {}
    self.private = dbdata.private or false
    self.invcode = dbdata.invcode
    self.user_id = dbdata.user_id
    self.create_time = dbdata.create_time
    self:encode_token()
end

function World:setup()
end

function World:startup()
    online:login_service(self.id, "worldm", quanta.id)
end

function World:online(id)
    log_debug("[World][online] id:{}", self.id)
    self.state = WS_ONLINE
    self:update_attrs("agent_srv", id)
    self:flush_db()
end

function World:offline()
    log_debug("[World][offline] id:{}", self.id)
    self.state = WS_OFFLINE
    self.agent_srv = 0
    self.sync_agent = false
    self:update_attrs("agent_srv", 0)
    self:flush_db()
end

function World:check_sync_agent()
    return self.sync_agent
end

function World:check_agent_srv()
    if self.agent_srv and self.agent_srv ~= 0 then
        return true
    end
    return false
end

function World:is_master(user_id)
    return self.user_id == user_id
end

function World:valid(invcode)
    if invcode or invcode ~= "" then
        return true
    end
    return self.invcode == invcode
end

function World:flush_db()
    if not next(self.db_updates) then
        return
    end
    world_dao:update(self.id, self.db_updates)
    self.db_updates = {}
end

function World:update_attrs(name, val, force)
    if val ~= nil and self[name] ~= val or force then
        self[name] = val
        self.db_updates[name] = val
        -- 重新计算token
        if name == "password" then
            self:encode_token()
        end
    end
end

function World:is_official()
    return self.type == WD_OFFICIAL
end

function World:is_dedicated()
    return self.type == WD_DEDICATED
end

function World:is_agent()
    return self.type == WD_AGENT
end

function World:is_private()
    return self.private
end

function World:create_invcode()
    local ok, code, invcode = router_mgr:call_center_master("rpc_invcode_allot", self.id)
    if qfailed(code, ok) then
        log_err("[World][create_invcode] rpc_invcode_allot is fail ok:{} invcode:{} world:{}", ok, invcode, self.id)
        return
    end
    self:set_invcode(invcode)
    log_debug("[World][create_invcode] create_invcode success world:{} invcode:{}", self.id, invcode)
end

function World:edit(opts)
    self:update_attrs("name", opts.name)
    self:update_attrs("max_count", opts.max_count)
    self:update_attrs("password", opts.password)
    if self:is_dedicated() and (opts.agent_url and opts.agent_url ~= '') then
        self:update_attrs("agent_url", opts.agent_url)
    end
    if opts.setting and next(opts.setting) then
        local change = 0
        for _, item in pairs(opts.setting) do
            if not self.setting[item.type] or self.setting[item.type].value ~= item.value then
                self.setting[item.type] = item
                change = change + 1
            end
        end
        self:update_attrs("setting", self.setting, change >= 1)
    end
    self:flush_db()
end

function World:set_agent(agent_id)
    self.sync_agent = true
    self:update_attrs("agent_srv", agent_id)
    self:flush_db()
end

function World:update(now)
    -- log_debug("[World][update] world:{}", self.id);
end

function World:packdb()
    local result = {
        id = self.id,
        name = self.name,
        type = self.type,
        max_count = self.max_count,
        password = self.password,
        agent_srv = self.agent_srv,
        setting = self.setting,
        private = self.private,
        invcode = self.invcode,
        user_id = self.user_id,
        create_time = self.create_time,
    }
    return result
end

function World:pack_client()
    local result = {
        id = self.id,
        type = self.type,
        name = self.name,
        user_id = self.user_id,
        max_count = self.max_count,
        token = self.token,
        setting = self.setting,
        invcode = self.invcode
    }
    return result
end

function World:pack_agent()
    local result = {
        type = self.type,
        name = self.name,
        world_id = self.id,
        master = self.user_id,
        max_count = self.max_count,
        token = self.token,
        setting = self.setting,
        invcode = self.invcode,
    }
    return result
end

function World:pack_list()
    local result = {
        id = self.id,
        type = self.type,
        name = self.name,
        user_id = self.user_id,
        max_count = self.max_count,
        password = self.password,
        create_time = self.create_time,
        setting = self.setting,
        private = self.private,
        agent_srv = self.agent_srv,
        agent_url = self.agent_url,
        invcode = self.invcode,
    }
    return result
end

--子类实现
------------------------------------------------
function World:encode_token()
end

function World:login()
    return FRAME_SUCCESS
end

function World:delete()
end
return World
