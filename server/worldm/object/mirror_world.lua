--mirror_world.lua(中转服)
local log_err       = logger.err
local log_debug     = logger.debug
local sformat       = string.format

local protobuf_mgr  = quanta.get("protobuf_mgr")
local mirror_mgr    = quanta.get("mirror_mgr")

local FRAME_SUCCESS = protobuf_mgr:error_code("FRAME_SUCCESS")
local FRAME_FAILED  = protobuf_mgr:error_code("FRAME_FAILED")

local World         = import("worldm/object/world.lua")
local MirrorWorld   = class(World)
function MirrorWorld:__init()
end

function MirrorWorld:setup()
    World.setup(self)
    if self:is_private() then
        self:create_invcode()
    end
end

function MirrorWorld:login(user_id)
    if self:check_sync_agent() then
        log_debug("[MirrorWorld][login] check_sync_agent is true user_id:{} world:{} agent:{}!",
            user_id, self:get_id(), self.agent_srv)
        return FRAME_SUCCESS
    end

    local agent = mirror_mgr:allot()
    if not agent then
        log_err("[MirrorWorld][login] agent is nil id:{} world:{}", user_id, self:get_id())
        return FRAME_FAILED
    end

    log_debug("[MirrorWorld][login] rpc_world_login success user_id:{} world:{} agent:{}!",
        user_id, self:get_id(), agent)
    self:set_agent(agent.id)
    return FRAME_SUCCESS
end

function MirrorWorld:offline()
    World.offline(self)
    self.sync_agent = false
end

function MirrorWorld:pack_client(user_id)
    local result = World.pack_client(self)
    local agent = mirror_mgr:get_agent(self.agent_srv)
    log_debug("[MirrorWorld][pack_client] user_id:{} master:{} agent_srv:{} result:{} agent:{}", user_id, self.user_id, self.agent_srv, result, agent)
    if agent then
        result.mirror_addr = sformat("%s:%s", agent.mirror_ip, agent.mirror_port)
    end
    if not self:is_master(user_id) then
        result.agent_url = agent and agent.url or ""
        result.token = self.password
    else
        result.agent_url = agent and agent.local_url or ""
        result.token = ""
    end
    return result
end
return MirrorWorld
