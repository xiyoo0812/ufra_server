import("agent/mongo_agent.lua")
local log_err     = logger.err
local qfailed     = quanta.failed

local mongo_agent = quanta.get("mongo_agent")

local doc         = "worlds"

local WorldDao    = singleton()
function WorldDao:__init()
end

function WorldDao:load_list(fields)
    local ok, code, data = mongo_agent:find({ doc, {}, fields }, nil)
    if not ok or qfailed(code) then
        return false, code
    end
    return ok, data
end

function WorldDao:load_world(id)
    local ok, code, data = mongo_agent:find_one({ doc, {["id"]=id} }, nil)
    if not ok or qfailed(code) then
        return false, code
    end
    return ok, data
end

function WorldDao:create(dbdata)
    local query = { doc, dbdata }
    local ok, code = mongo_agent:insert(query)
    if qfailed(code, ok) then
        log_err("[WorldDao][create] insert dbdata:{} code:{}", dbdata, code)
        return false
    end
    return true
end

function WorldDao:update(id, attrs)
    local udata = { ["$set"] = attrs }
    local ok, code =  mongo_agent:update({doc, udata, {["id"] = id}, true})
    if qfailed(code, ok) then
        log_err("[WorldDao][update] update attrs:{} code:{}", attrs, code)
        return false
    end
    return true
end

function WorldDao:delete(id)
    local query = { doc, { id = id } }
    local ok, code = mongo_agent:delete(query)
    if qfailed(code, ok) then
        log_err("[WorldDao][create] insert id:{} code:{}", id, code)
        return false
    end
    return true
end

quanta.world_dao = WorldDao()
return WorldDao
