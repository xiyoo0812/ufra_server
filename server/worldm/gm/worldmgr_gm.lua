--worldmgr_gm.lua
local log_debug    = logger.debug
local new_guid     = codec.guid_new
local gm_agent     = quanta.get("gm_agent")
local worldServlet = quanta.get("world_servlet")
local SERVICE      = quanta.enum("GMType", "SERVICE")


local WorldMgr = singleton()
function WorldMgr:__init()
    local cmd_list = {
        {
            name = "world_list",
            gm_type = SERVICE,
            group = "世界",
            desc = "世界列表",
            args = "user_id|integer type|integer name|string page|integer is_self|integer",
            example = "world_list 383989987730785281 1 '' 1 0",
            tip = "user_id 账号id type 类型 name 名称 page 分页 is_self 自己的列表"
        },
        {
            name = "world_create",
            gm_type = SERVICE,
            group = "世界",
            desc = "创建世界",
            args = "user_id|integer name|string type|integer max_count|integer password|string agent_url|string setting|string",
            example = "world_create 383989987730785281 eufaula_1 1 10 '123456' '' ''",
            tip = "user_id 账号id name 世界名称 type 世界类型 max_count 最大人数 password 密码 agent_url 代理地址 setting 设置"
        },
        {
            name = "world_edit",
            gm_type = SERVICE,
            group = "世界",
            desc = "编辑世界",
            args = "user_id|integer id|integer name|string max_count|integer password|string agent_url|string setting|string",
            example = "world_edit 383989987730785281 100 eufaula_1 20 '123456' '' ''",
            tip = "user_id 账号id id 世界唯一标识 max_count 最大人数 password 密码 agent_utl 代理地址 setting 设置"
        },
        {
            name = "world_delete",
            gm_type = SERVICE,
            group = "世界",
            desc = "删除世界",
            args = "user_id|integer id|integer",
            example = "world_delete 383989987730785281 id",
            tip = "user_id 账号id id 世界唯一标识"
        },
        {
            name = "world_login",
            gm_type = SERVICE,
            group = "世界",
            desc = "登录世界",
            args = "user_id|integer id|integer password|string",
            example = "world_login 383989987730785281 100 '123456'",
            tip = "user_id 账号id id 世界唯一标识 password 密码"
        }
    }
    --注册GM
    gm_agent:insert_command(cmd_list)
    --注册GM回调
    for _, v in ipairs(cmd_list) do
        gm_agent:add_listener(self, v.name)
    end
end

function WorldMgr:world_list(user_id, type, name, page, is_self)
    log_debug("[WorldMgr][world_list] user_id:{}, type:{} name:{} page:{} is_self:{}",
        user_id, type, name, page, is_self);
    local _, list = worldServlet:rpc_world_list(user_id, { type = type, name = name, page = page, is_self=is_self });
    log_debug("[WorldMgr][world_list] list:{}", list)
    return "success"
end

function WorldMgr:world_create(user_id, name, type, max_count, password, agent_url, setting)
    log_debug("[WorldMgr][world_create] user_id:{} name:{} type:{} max_count:{} password:{} agent_url:{} setting:{}",
     user_id, name, type, max_count, password, agent_url, setting);
    setting = {
        {
            type = 1,
            value = "testsetst"
        }
    };
    local id = new_guid()
    worldServlet:rpc_world_create(user_id,
        { id = id, type = type, name = name, max_count = max_count, password = password, agent_url = agent_url, setting =
        setting });
    return "success"
end

function WorldMgr:world_edit(user_id, id, name, max_count, password, agent_url, setting)
    log_debug("[WorldMgr][world_edit] user_id:{} id:{} name:{} max_count:{} password:{} agent_url:{} setting:{}",
        user_id, id, name, max_count, password, agent_url, setting);
    setting = {
        {
            type=100,
            value="testsetst"
        }
    };
    worldServlet:rpc_world_edit(user_id, {id=id, name=name, max_count=max_count,password=password,agent_url=agent_url,setting=setting});
    return "success"
end

function WorldMgr:world_delete(user_id, id)
    log_debug("[WorldMgr][world_delete] user_id:{} id:{}", user_id, id);
    worldServlet:rpc_world_delete(user_id, { id = id });
    return "success"
end

function WorldMgr:world_login(user_id, id, password)
    log_debug("[WorldMgr][world_login] user_id:{} id:{} password:{}", user_id, id, password);
    worldServlet:rpc_world_login(user_id, { id = id, password = password });
    return "success"
end
quanta.activity_gm = WorldMgr()
return WorldMgr
