--world_servlet.lua
local log_err       = logger.err
local log_debug     = logger.debug
local qfailed       = quanta.failed

local event_mgr     = quanta.get("event_mgr")
local world_mgr     = quanta.get("world_mgr")
local list_mgr      = quanta.get("list_mgr")
local agent_mgr     = quanta.get("agent_mgr")
local router_mgr    = quanta.get("router_mgr")

local protobuf_mgr  = quanta.get("protobuf_mgr")

local FRAME_SUCCESS = protobuf_mgr:error_code("FRAME_SUCCESS")
local FRAME_PARAMS  = protobuf_mgr:error_code("FRAME_PARAMS")
local NOT_EXIST     = protobuf_mgr:error_code("WORLDM_NOT_EXIST")
local AUTH_PASS     = protobuf_mgr:error_code("WORLDM_AUTH_PASS")
local NOT_AUTH      = protobuf_mgr:error_code("WORLDM_NOT_AUTH")
local NAME_ERR      = protobuf_mgr:error_code("WORLDM_NAME_ERR")
local AGENT_URL     = protobuf_mgr:error_code("WORLDM_AGENT_URL")
local LOGIN_ERR     = protobuf_mgr:error_code("WORLDM_LOGIN_ERR")

local WD_DEDICATED  = protobuf_mgr:enum("world_type", "WORLD_DEDICATED")

local WorldServlet  = singleton()
function WorldServlet:__init()
    event_mgr:add_listener(self, "rpc_agent_register")
    event_mgr:add_listener(self, "rpc_agent_heartbeat")
    event_mgr:add_listener(self, "rpc_agent_state")
    event_mgr:add_listener(self, "rpc_agent_sync")

    event_mgr:add_listener(self, "rpc_world_list")
    event_mgr:add_listener(self, "rpc_world_login")
    event_mgr:add_listener(self, "rpc_world_create")
    event_mgr:add_listener(self, "rpc_world_edit")
    event_mgr:add_listener(self, "rpc_world_delete")
end

-- 功能函数
function WorldServlet:check_create_world(user_id, world)
    if not world.name then
        return NAME_ERR
    elseif world.type == WD_DEDICATED and (not world.agent_url or world.agent_url == '') then
        return AGENT_URL
    end
    return list_mgr:check_create_workd(user_id, world)
end

--rpc协议
-------------------------------------------------------------------------
function WorldServlet:rpc_agent_register(message, broadcast)
    log_debug("[WorldServlet][rpc_agent_register] message:{}", message);
    if not message.id then
        log_debug("[WorldServlet][rpc_agent_register] id is nil message:{}", message);
        return FRAME_PARAMS
    end
    agent_mgr:online(message.id, message.agent_url, WD_DEDICATED)
    if not broadcast then
        router_mgr:call_worldm_all("rpc_agent_register", message, true)
    end
    return FRAME_SUCCESS
end

function WorldServlet:rpc_agent_heartbeat(message, broadcast)
    log_debug("[WorldServlet][rpc_agent_heartbeat] message:{}", message);
    if not message.id then
        log_debug("[WorldServlet][rpc_agent_heartbeat] id is nil message:{}", message);
        return FRAME_PARAMS
    end
    list_mgr:sync_world(message.worlds)
    agent_mgr:heartbeat(message.id, message.agent_url)
    if not broadcast then
        router_mgr:call_worldm_all("rpc_agent_heartbeat", message, true)
    end
    return FRAME_SUCCESS
end

function WorldServlet:rpc_agent_state(message, broadcast)
    log_debug("[WorldServlet][rpc_agent_state] message:{}", message);
    if not message.id then
        log_debug("[WorldServlet][rpc_agent_state] id is nil message:{}", message);
        return FRAME_PARAMS
    end
    world_mgr:on_world_state(message.id, message.worlds)
    list_mgr:on_world_state(message.worlds)
    if not broadcast then
        router_mgr:call_worldm_all("rpc_agent_state", message, true)
    end
    return FRAME_SUCCESS
end

function WorldServlet:rpc_agent_sync(message, broadcast)
    log_debug("[WorldServlet][rpc_agent_sync] message:{}", message);
    if not message.id then
        log_debug("[WorldServlet][rpc_agent_sync] id is nil message:{}", message);
        return FRAME_PARAMS
    end
    list_mgr:sync_world(message.worlds)
    if not broadcast then
        router_mgr:call_worldm_all("rpc_agent_sync", message, true)
    end
    return FRAME_SUCCESS
end

function WorldServlet:rpc_world_list(user_id, message)
    log_debug("[WorldServlet][rpc_world_list] user:{} message:{}", user_id, message);
    local type,name,page,is_self = message.type, message.name, message.page, message.is_self
    local result
    if not is_self then
        result = list_mgr:list(type,name,page)
    else
        result = list_mgr:my_list(user_id, type)
    end
    log_debug("[WorldServlet][rpc_world_list] user:{} result:{}", user_id, result);
    return FRAME_SUCCESS, result
end

function WorldServlet:rpc_world_login(user_id, message)
    log_debug("[WorldServlet][rpc_world_login] user:{} message:{}", user_id, message);
    local world = world_mgr:get_world(message.id)
    if not world then
        log_err("[WorldServlet][rpc_world_login] world is nil user:{} message:{}", user_id, message)
        return NOT_EXIST
    end
    if not world:is_master(user_id) and not world:valid(message.invcode) then
        log_err("[WorldServlet][rpc_world_login] valid_password is false user:{} message:{}", user_id, message)
        return AUTH_PASS
    end
    local code = world:login(user_id)
    if qfailed(code) then
        log_err("[WorldServlet][rpc_world_login] login is fail user:{} message:{} code:{}", user_id, message, code)
        return LOGIN_ERR
    end
    if not world.agent_url then
        log_err("[WorldServlet][rpc_world_login] agent_url is fail user:{} world:{}", user_id, world)
        return FRAME_PARAMS
    end
    log_debug("[WorldServlet][rpc_world_login] user:{} world:{}", user_id, world:pack_client(user_id));
    return FRAME_SUCCESS, world:pack_client(user_id)
end

function WorldServlet:rpc_world_create(user_id, message)
    log_debug("[WorldServlet][rpc_world_create] user:{} message:{}", user_id, message);
    local ch_code = self:check_create_world(user_id, message)
    if ch_code ~= FRAME_SUCCESS then
        log_err("[WorldServlet][rpc_world_create] check_create_world fail user:{} message:{} ch_code:{}", user_id,
            message, ch_code)
        return ch_code
    end

    local code, world = world_mgr:create(user_id, message)
    if qfailed(code) then
        log_err("[WorldServlet][rpc_world_create] create fail user:{} message:{} code:{}", user_id, message, code)
        return code
    end
    code = world:login(user_id)
    if qfailed(code) then
        log_err("[WorldServlet][rpc_world_create] login fail user:{} message:{} code:{}", user_id, message, code)
        return LOGIN_ERR
    end
    list_mgr:create(world:pack_list())
    log_debug("[WorldServlet][rpc_world_create] user:{} world:{}", user_id, world:pack_client(user_id));
    return FRAME_SUCCESS, world:pack_client(user_id)
end

function WorldServlet:rpc_world_edit(user_id, message)
    log_debug("[WorldServlet][rpc_world_edit] user:{} message:{}", user_id, message);
    local world = world_mgr:get_world(message.id)
    if not world then
        log_err("[WorldServlet][rpc_world_edit] world is nil user:{} message:{}", user_id, message)
        return NOT_EXIST
    end
    if user_id ~= world:get_user_id() then
        log_err("[WorldServlet][rpc_world_edit] not master user:{} message:{}", user_id, message)
        return NOT_AUTH
    end
    world:edit(message)
    list_mgr:edit(world)
    log_debug("[WorldServlet][rpc_world_edit] success user:{} world:{}", user_id, world:pack_client());
    return FRAME_SUCCESS, world:pack_client()
end

function WorldServlet:rpc_world_delete(user_id, message)
    log_debug("[WorldServlet][rpc_world_delete] user:{} message:{}", user_id, message);
    local world = world_mgr:get_world(message.id)
    if not world then
        log_debug("[WorldServlet][rpc_world_delete] world is nil user:{} message:{}", user_id, message)
        return FRAME_SUCCESS
    end
    if user_id ~= world:get_user_id() then
        log_err("[WorldServlet][rpc_world_delete] not master user:{} message:{}", user_id, message)
        return NOT_AUTH
    end
    world_mgr:delete(world)
    list_mgr:delete(world.id)
    return FRAME_SUCCESS
end

quanta.world_servlet = WorldServlet()
return WorldServlet
