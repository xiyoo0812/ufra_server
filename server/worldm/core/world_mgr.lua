--world_mgr.lua
local log_err       = logger.err
local log_warn      = logger.warn
local log_debug     = logger.debug
local lb64encode    = ssl.b64_encode
local tinsert       = table.insert

local protobuf_mgr  = quanta.get("protobuf_mgr")
local FRAME_SUCCESS = protobuf_mgr:error_code("FRAME_SUCCESS")
local FRAME_FAILED  = protobuf_mgr:error_code("FRAME_FAILED")
local FRAME_PARAMS  = protobuf_mgr:error_code("FRAME_PARAMS")

local WS_ONLINE     = protobuf_mgr:enum("world_state", "WS_ONLINE")
local WS_OFFLINE    = protobuf_mgr:enum("world_state", "WS_OFFLINE")

local object_fty    = quanta.get("object_fty")
local world_dao     = quanta.get("world_dao")
local router_mgr    = quanta.get("router_mgr")
local update_mgr    = quanta.get("update_mgr")
local monitor       = quanta.get("monitor")
local event_mgr     = quanta.get("event_mgr")

local PRIVATE_KEY              = [[
MIICWwIBAAKBgQCWKUc5BTsvNKLv389mqShFhg7lHbG8SyyAiHZ5gMMMoBGayBGg
OCGXHDRDUabr0E8xFtSApu9Ppuj3frzwRDcj4Q69yXc/x1+a18Jt96DI/DJEkmkm
o/Mr+pmY4mVFk4a7pxnXpynBUz7E7vp9/XvMs84LDFqqvGiSmW/YKJfsAQIDAQAB
AoGANhfDnPJZ+izbf07gH0rTg4wB5J5YTwzDiL/f8fAlE3C8NsZYtx9RVmamGxQY
bf158aSYQ4ofTlHBvZptxJ3GQLzJQd2K15UBzBe67y2umN7oP3QD+nUhw83PnD/R
A+aTmEiujIXS9aezbfaADYGd5fFr2ExUPvw9t0Pijxjw8WMCQQDDsGLBH4RTQwPe
koVHia72LF7iQPP75AaOZIuhCTffaLsimA2icO+8/XT2yaeyiXqHn1Wzyk1ZrGgy
MTeTu9jPAkEAxHDPRxNpPUhWQ6IdPWflecKpzT7fPcNJDyd6/Mg3MghWjuWc1xTl
nmBDdlQGOvKsOY4K4ihDZjVMhBnqp16CLwJAOvaT2wMHGRtxOAhIFnUa/dwCvwO5
QGXFv/P1ypD/f9aLxHGycga7heOM8atzVy1reR/+b8z+H43+W1lPGLmaKwJAJ2zA
nPIvX+ZBsec6WRWd/5bq/09L/JhR9GGnFE6WjUsRHDLHDH+cKfIF+Bya93+2wwJX
+tW72Sp/Rc/xwU99bwJAfUw9Nfv8llVA2ZCHkHGNc70BjTyaT/TxLV6jcouDYMTW
RfSHi27F/Ew6pENe4AwY2sfEV2TXrwEdrvfjNWFSPw==
]]

local WorldMgr = singleton()
local prop = property(WorldMgr)
prop:reader("worlds", {})
prop:reader("rsa_key", nil)

function WorldMgr:__init()
    event_mgr:add_trigger(self, "on_agent_online")
    event_mgr:add_trigger(self, "on_agent_offline")
    monitor:watch_service_ready(self, "worlda")
    monitor:watch_service_close(self, "worlda")
    update_mgr:attach_frame(self)
    --rsa key
    self.rsa_key = ssl.rsa_init_prikey(PRIVATE_KEY)
end

function WorldMgr:rsa_encode(content)
    if not content or content == "" then
        return ""
    end
    local rsa_content = self.rsa_key.pri_encode(content)
    local result = lb64encode(rsa_content)
    log_debug("[WorldMgr][rsa_encode] content:{} rsa_content:{} lb64:{}", content, rsa_content, result)
    return result
end

function WorldMgr:on_agent_online(id, url)
    log_debug("[WorldMgr][on_agent_online] begin id:{} url:{} worlds:{}", id, url, self.worlds)
    for _, world in pairs(self.worlds) do
        if (id and world.agent_srv == id) or (url and world.agent_url == url) then
            log_debug("[WorldMgr][on_agent_online] world online world:{}", id)
            world:online(id)
        end
    end
end

function WorldMgr:on_agent_offline(id, url)
    log_debug("[WorldMgr][on_agent_offline] begin id:{} url:{} worlds:{}", id, url, self.worlds)
    for _, world in pairs(self.worlds) do
        if (id and world.agent_srv == id) or (url and world.agent_url == url) then
            log_debug("[WorldMgr][on_agent_offline] world offline world:{}", id)
            world:offline()
            self:remove(world)
        end
    end
end

function WorldMgr:on_world_state(id, worlds)
    if not worlds or not next(worlds) then
        return
    end
    for _, item in pairs(worlds) do
        local world = self.worlds[item.id]
        if not world then
            log_warn("[WorldMgr][on_world_state] world is nil id:{} state:{}", item.id, item.state)
            goto continue
        end
        if item.state == WS_ONLINE then
            world:online(id)
        elseif item.state == WS_OFFLINE then
            world:offline()
            self:remove(world)
        end
        :: continue ::
    end
end

function WorldMgr:on_service_ready(id, name, info)
    log_debug("[WorldMgr][on_service_ready] id:{} name:{} info:{}", id, name, info)
    self:on_agent_online(id)
end

function WorldMgr:on_service_close(id, name)
    log_debug("[WorldMgr][on_service_close] id:{} name:{}", id, name)
    self:on_agent_offline(id)
end

function WorldMgr:get_world(id)
    local world = self.worlds[id]
    if not world then
        local ok, dbdata = world_dao:load_world(id)
        if not ok or not dbdata then
            log_warn("[WorldMgr][get_world] load_world is fail id:{} dbdata:{}", id, dbdata)
            return nil
        end
        world = object_fty:create_world(id, dbdata.type);
        world:init_from_db(dbdata)
        world:startup()
        self.worlds[world:get_id()] = world
    end
    return world
end

function WorldMgr:get_agent_worlds(agent)
    local result = {}
    for _, world in pairs(self.worlds) do
        if agent.id == world.agent_srv then
            tinsert(result, world)
        end
    end
    return result
end

function WorldMgr:create(user_id, data)
    local world = object_fty:create_world(data.id, data.type);
    if not world then
        log_err("[WorldMgr][create] world is nil data:{}", data)
        return FRAME_PARAMS
    end

    world:init_from_config(user_id, data)
    world:setup()
    local db_result = world_dao:create(world:packdb())
    if not db_result then
        log_err("[WorldMgr][create] db_result is nil world:{}", world:packdb())
        return FRAME_FAILED
    end
    self.worlds[world:get_id()] = world
    return FRAME_SUCCESS, world
end

function WorldMgr:delete(world)
    world:delete()
    local db_result = world_dao:delete(world.id)
    if not db_result then
        log_err("[WorldMgr][delete] db_result is nil id:{}", world.id)
    end
    self.worlds[world.id] = nil
    router_mgr:call_center_master("rpc_invcode_delete", world.invcode)
end

function WorldMgr:remove(world)
    self.worlds[world.id] = nil
end

function WorldMgr:remove(world)
    self.worlds[world.id] = nil
end

function WorldMgr:on_frame(now)
    for _, world in pairs(self.worlds) do
        world:update(now)
    end
end

quanta.world_mgr = WorldMgr()
return WorldMgr
