--mirror_mgr.lua
local log_err       = logger.err
local log_debug     = logger.debug
local log_warn      = logger.warn
local trandom       = qtable.random
local sformat       = string.format

local monitor       = quanta.get("monitor")
local event_mgr     = quanta.get("event_mgr")
local protobuf_mgr  = quanta.get("protobuf_mgr")

local WD_AGENT      = protobuf_mgr:enum("world_type", "WORLD_AGENT")

local MirrorMgr = singleton()
local prop = property(MirrorMgr)
prop:reader("mirrors", {})

function MirrorMgr:__init()
    monitor:watch_service_ready(self, "mirror")
    monitor:watch_service_close(self, "mirror")
end

function MirrorMgr:on_service_ready(id, name, info)
    log_debug("[MirrorMgr][on_service_ready] id:{}, name:{} info:{}", id, name, info)
    self:online(id, info.ip, info.option, sformat("http://%s:%s/request-world", info.ip, info.port))
end

function MirrorMgr:on_service_close(id, name)
    log_debug("[MirrorMgr][on_service_close] node: {}-{}", name, id)
    -- self:offline(id)
end

function MirrorMgr:get_agent(id)
    log_debug("[MirrorMgr][get_agent] mirrors:{}", self.mirrors)
    return self.mirrors[id]
end

function MirrorMgr:online(id, mirror_ip, mirror_port, agent_url)
    log_debug("[MirrorMgr][online] id:{}", id)
    local agent = {
        id = id,
        mirror_ip = mirror_ip,
        mirror_port = mirror_port,
        url = agent_url,
        local_url = sformat("http://%s:%s/request-world", "127.0.0.1", 10501),
        time = quanta.now,
    }
    self.mirrors[id] = agent
    log_debug("[MirrorMgr][online] end agent:{}", agent)
    event_mgr:notify_trigger("on_agent_online", id, agent_url, WD_AGENT)
end

function MirrorMgr:offline(id)
    log_debug("[MirrorMgr][offline] id:{}", id)
    local agent = self.mirrors[id]
    if not agent then
        log_warn("[MirrorMgr][offline] agent is nil id:{}", id)
        return
    end
    self.mirrors[id] = nil
    event_mgr:notify_trigger("on_agent_offline", id, agent.agent_url, WD_AGENT)
end

function MirrorMgr:allot()
    log_debug("[MirrorMgr][allot] mirrors:{}",self.mirrors)
    if not self.mirrors or not next(self.mirrors) then
        log_err("[MirrorMgr][allot] mirrors empty")
        return nil
    end
    local index = trandom(self.mirrors)
    local agent = self.mirrors[index]
    return agent
end
quanta.mirror_mgr = MirrorMgr()
return MirrorMgr
