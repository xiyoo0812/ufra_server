--list_mgr.lua
local log_debug     = logger.debug
local log_warn      = logger.warn
local tinsert       = table.insert
local mfloor        = math.floor

local world_dao     = quanta.get("world_dao")
local event_mgr     = quanta.get("event_mgr")
local monitor       = quanta.get("monitor")
local thread_mgr    = quanta.get("thread_mgr")
local router_mgr    = quanta.get("router_mgr")
local protobuf_mgr  = quanta.get("protobuf_mgr")
local config_mgr    = quanta.get("config_mgr")

local FRAME_SUCCESS = protobuf_mgr:error_code("FRAME_SUCCESS")
local NAME_OCCUPY   = protobuf_mgr:error_code("WORLDM_NAME_OCCUPY")
local COUNT_LIMIT   = protobuf_mgr:error_code("WORLDM_COUNT_LIMIT")

local WST_PWVALID   = protobuf_mgr:enum("world_setting_type", "WST_PWVALID")

local WD_OFFICIAL   = protobuf_mgr:enum("world_type", "WORLD_OFFICIAL")
local WD_DEDICATED  = protobuf_mgr:enum("world_type", "WORLD_DEDICATED")
local WD_AGENT      = protobuf_mgr:enum("world_type", "WORLD_AGENT")

local WS_OFFLINE    = protobuf_mgr:enum("world_state", "WS_OFFLINE")

local utility_db    = config_mgr:init_table("utility", "key")
local OFF_WD_COUNT  = utility_db:find_integer("value", "official_world_count")
local DED_WD_COUNT  = utility_db:find_integer("value", "dedicated_world_count")

local WorldList     = import("worldm/object/world_list.lua")
local SECOND_3_MS   = quanta.enum("PeriodTime", "SECOND_3_MS")
local PAGE_SIZE     = 1000

local ListMgr       = singleton()
local prop          = property(ListMgr)
prop:reader("worlds", {})
prop:reader("world_names", {})

function ListMgr:__init()
    self.world_list_settings = {
        [WST_PWVALID] = true
    }
    self:install()
end

function ListMgr:install()
    monitor:watch_service_ready(self, "mongo")
    event_mgr:add_trigger(self, "on_agent_online")
    event_mgr:add_trigger(self, "on_agent_offline")
    event_mgr:add_trigger(self, "on_agent_heartbeat")

    --rpc协议
    event_mgr:add_listener(self, "rpc_worldlist_change") -- 世界变化
    event_mgr:add_listener(self, "rpc_worldlist_create") -- 创建世界
    event_mgr:add_listener(self, "rpc_worldlist_delete") -- 删除世界
end

function ListMgr:on_agent_heartbeat(id, url)
    -- log_debug("[ListMgr][on_agent_heartbeat] begin id:{} url:{} worlds:{}", id, url, self.worlds)
    if not url or url == "" then
        return
    end
    for _, world in pairs(self.worlds) do
        -- log_debug("[ListMgr][on_agent_heartbeat] log world:{}", world)
        if world.agent_url == url then
            -- log_debug("[ListMgr][on_agent_heartbeat] online world:{}", world)
            world:heartbeat()
        end
    end
end

function ListMgr:on_agent_online(id, url, world_type)
    log_debug("[ListMgr][on_agent_online] begin id:{} url:{} world_type:{} worlds:{}", id, url, world_type, self.worlds)
    for _, world in pairs(self.worlds) do
        log_debug("[ListMgr][on_agent_online] log world:{}", world)
        if world.type ~= world_type then
            goto continue
        end
        if world.type == WD_OFFICIAL or world.type == WD_DEDICATED then
            if world.agent_srv == id or (url and url~="" and world.agent_url == url) then
                log_debug("[ListMgr][on_agent_online] online world:{}", world)
                world:online()
            end
        elseif world.type == WD_AGENT then
            if world.agent_srv == id then
                log_debug("[ListMgr][on_agent_online] online world:{}", world)
                world:online()
            end
        end
        :: continue ::
    end
end

function ListMgr:on_agent_offline(id, url, world_type)
    log_debug("[ListMgr][on_agent_offline] begin id:{} url:{} world_type:{} worlds:{}", id, url, world_type, self.worlds)
    for _, world in pairs(self.worlds) do
        log_debug("[ListMgr][on_agent_offline] log world:{}", world)
        if world.type ~= world_type then
            goto continue
        end
        if world.type == WD_OFFICIAL or world.type == WD_DEDICATED then
            if world.agent_srv == id or (url and url~="" and world.agent_url == url) then
                log_debug("[ListMgr][on_agent_offline] offline world:{}", world)
                world:offline()
            end
        elseif world.type == WD_AGENT then
            if world.agent_srv == id then
                log_debug("[ListMgr][on_agent_offline] offline world:{}", world)
                world:offline()
            end
        end
        :: continue ::
    end
end

function ListMgr:on_service_ready(id, name, info)
    log_debug("[ListMgr][on_service_ready] id:{}, name:{} info:{}", id, name, info)
    thread_mgr:success_call(SECOND_3_MS, function()
        return self:load()
    end)
end

function ListMgr:load()
    local ok, dbdata = world_dao:load_list({
        ["id"] = 1,
        ["type"] = 1,
        ["user_id"]=1,
        ["name"] = 1,
        ["max_count"] = 1,
        ["password"] = 1,
        ["setting"] = 1,
        ["invcode"] = 1,
        ["agent_srv"] = 1,
        ["agent_url"] = 1,
        ["private"] = 1,
        ["create_time"] = 1
    })
    if not ok then
        return false
    end
    for _, data in pairs(dbdata) do
        local world = WorldList(data.id)
        world:init_from_db(data, self:list_setting(data))
        self.worlds[world:get_id()] = world
        self.world_names[world:get_name()] = true
    end
    return true
end

function ListMgr:check_world_count(user_id, type)
    local count = 0
    for _, world in pairs(self.worlds) do
        if world.user_id == user_id and world.type == type then
            count = count + 1
        end
    end
    if type == WD_OFFICIAL then
        return count < OFF_WD_COUNT
    elseif type == WD_DEDICATED then
        return count < DED_WD_COUNT
    elseif type == WD_AGENT then
        return true
    end
    return false
end

function ListMgr:check_create_workd(user_id, world)
    -- 社群私服或局域网代理服不限制名称
    if ((world.type ~= WD_DEDICATED and world.private) and world.type ~= WD_AGENT) and self.world_names[world.name] then
        return NAME_OCCUPY
    elseif not self:check_world_count(user_id, world.type) then
        return COUNT_LIMIT
    end
    return FRAME_SUCCESS
end

function ListMgr:on_world_state(worlds)
    for _, item in pairs(worlds) do
        local world = self:get_world(item.id)
        if not world then
            goto continue
        end
        if item.state == WS_OFFLINE then
            world:reset()
        end
        ::continue::
    end
end

function ListMgr:sync_world(worlds)
    for _, item in pairs(worlds) do
        local world = self:get_world(item.id)
        if not world then
            goto continue
        end
        if world.player_count ~= item.amount then
            world:set_player_count(item.amount or 0)
        end
        :: continue ::
    end
end

function ListMgr:rpc_worldlist_change(service_id, worlds)
    log_debug("[ChRosterMgr][rpc_worldlist_change] service_id:{} worlds:{}", service_id, worlds)
    if service_id == quanta.id then
        return
    end
    for _, world in pairs(worlds) do
        if not self.worlds[world.id] then
            self.worlds[world.id] = {}
        end
        for key, val in pairs(world) do
            self.worlds[world.id][key] = val
        end
    end
end

function ListMgr:rpc_worldlist_create(service_id, data)
    log_debug("[ChRosterMgr][rpc_worldlist_create] service_id:{} data:{}", service_id, data)
    if service_id == quanta.id then
        return
    end
    local world = WorldList(data.id)
    world:init_from_config(data, self:list_setting(data))
    self.worlds[world:get_id()] = world
    self.world_names[world:get_name()] = true
end

function ListMgr:rpc_worldlist_delete(service_id, id)
    log_debug("[ChRosterMgr][rpc_worldlist_delete] service_id:{} id:{}", service_id, id)
    if service_id == quanta.id then
        return
    end
    self.worlds[id] = nil
end

function ListMgr:get_world(id)
    return self.worlds[id]
end

function ListMgr:create(data)
    local world = WorldList(data.id)
    world:init_from_config(data, self:list_setting(data))
    self.worlds[world:get_id()] = world
    self.world_names[world:get_name()] = true
    router_mgr:call_worldm_all("rpc_worldlist_create", quanta.id, world:pack_service())
end

function ListMgr:delete(id)
    local world = self.worlds[id]
    if world then
        self.worlds[id] = nil
        self.world_names[world:get_name()] = nil
        router_mgr:call_worldm_all("rpc_worldlist_delete", quanta.id, id)
    end
end

function ListMgr:edit(world)
    local world_list = self.worlds[world.id]
    if world_list then
        if world.name and world.name ~= world_list.name then
            self.world_names[world_list.name] = nil
            self.world_names[world.name] = true
        end
        local sync = world_list:set_attrs(world)
        if sync then
            router_mgr:call_worldm_all("rpc_worldlist_change", quanta.id, { world_list:pack_client() })
        end
    else
        log_warn("[ListMgr][edit] world_list is nil world:{}", world)
    end
end

function ListMgr:list(type, name, page)
    local worlds = {}
    for _, world in pairs(self.worlds) do
        if type and type ~= 0 and type ~= world.type or world:is_private() then
            goto continue
        end
        if name and name ~= "" and type ~= world.name then
            goto continue
        end
        tinsert(worlds, world)
        :: continue ::
    end
    local result = {}
    for index, world in pairs(worlds) do
        if mfloor(index / PAGE_SIZE) == page - 1 then
            tinsert(result, world:pack_client())
        end
    end
    return result
end

function ListMgr:my_list(user_id, type)
    local worlds = {}
    for _, world in pairs(self.worlds) do
        if type and type ~= 0 and type ~= world.type then
            goto continue
        end
        if user_id ~= world.user_id then
            goto continue
        end
        tinsert(worlds, world:pack_mylist())
        :: continue ::
    end
    return worlds
end

function ListMgr:list_setting(data)
    local setting = data.setting
    if not setting or setting == "" then
        return {}
    end
    local result = {}
    for _, item in pairs(setting) do
        if self.world_list_settings[item.type] then
            tinsert(result, item)
       end
    end
    return result
end

quanta.list_mgr = ListMgr()
return ListMgr
