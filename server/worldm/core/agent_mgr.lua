--agent_mgr.lua
local log_err       = logger.err
local log_debug     = logger.debug
local log_warn      = logger.warn
local trandom       = qtable.random
local sformat       = string.format

local monitor       = quanta.get("monitor")
local update_mgr    = quanta.get("update_mgr")
local event_mgr     = quanta.get("event_mgr")
local protobuf_mgr  = quanta.get("protobuf_mgr")

local WD_OFFICIAL   = protobuf_mgr:enum("world_type", "WORLD_OFFICIAL")
local WD_DEDICATED  = protobuf_mgr:enum("world_type", "WORLD_DEDICATED")

local AgentMgr = singleton()
local prop = property(AgentMgr)
prop:reader("agents", {})
prop:reader("official_agents", {})
prop:reader("agent_urls", {})

function AgentMgr:__init()
    monitor:watch_service_ready(self, "worlda")
    monitor:watch_service_close(self, "worlda")
    update_mgr:attach_frame(self)
end

function AgentMgr:on_service_ready(id, name, info)
    log_debug("[AgentMgr][on_service_ready] id:{}, name:{} info:{}", id, name, info)
    self:online(id, sformat("http://%s:%s/request-world", info.ip, info.port), WD_OFFICIAL)
end

function AgentMgr:on_service_close(id, name)
    log_debug("[AgentMgr][on_service_close] node: {}-{}", name, id)
    self:offline(id)
end

function AgentMgr:heartbeat(id, agent_url)
    local agent = self.agents[id]
    if not agent then
        self:online(id, agent_url, WD_DEDICATED)
        log_warn("[AgentMgr][heartbeat] agent is nil id:{} agent_url:{}", id, agent_url)
        return
    end
    agent.time = quanta.now
    event_mgr:notify_trigger("on_agent_heartbeat", id, agent_url)
    log_debug("[AgentMgr][heartbeat] id:{}", id)
end

function AgentMgr:get_agent(id)
    return self.agents[id]
end

function AgentMgr:online(id, agent_url, world_type)
    log_debug("[AgentMgr][online] id:{} agent_url:{}", id, agent_url)
    local agent = self.agents[id] or self.agent_urls[agent_url]
    -- 让原来的agent下线
    if agent then
        self:offline(agent.id)
    end
    agent = {
        id = id,
        url = agent_url,
        type = world_type,
        time = quanta.now,
    }
    self.agents[id] = agent
    if world_type == WD_OFFICIAL then
        self.official_agents[id] = self.agents[id]
    end
    self.agent_urls[agent_url] = self.agents[id]
    event_mgr:notify_trigger("on_agent_online", id, agent_url, world_type)
end

function AgentMgr:offline(id, world_type)
    log_debug("[AgentMgr][offline] id:{}", id)
    local agent = self.agents[id]
    if not agent then
        log_warn("[AgentMgr][offline] agent is nil id:{}", id)
        return
    end
    self.agents[id] = nil
    self.official_agents[id] = nil
    self.agent_urls[agent.url] = nil
    event_mgr:notify_trigger("on_agent_offline", id, agent.url, world_type)
end

function AgentMgr:allot()
    log_debug("[AgentMgr][allot] official_agents:{}",self.official_agents)
    if not self.official_agents or not next(self.official_agents) then
        log_err("[AgentMgr][allot] world_agents empty")
        return false
    end
    local index = trandom(self.official_agents)
    local agent = self.official_agents[index]
    return agent
end

function AgentMgr:on_frame()
    for _,agent in pairs(self.agents) do
        -- log_debug("[AgentMgr][on_frame] log agent:{}",agent)
        if agent.type == WD_OFFICIAL then
            goto continue
        end
        -- log_debug("[AgentMgr][on_frame] log agent:{} diff:{}",agent,quanta.now - agent.time)
        if quanta.now - agent.time >= 120 then
            log_debug("[AgentMgr][on_frame] offline agent:{}",agent)
            self:offline(agent.id)
        end
        :: continue ::
    end
end
quanta.agent_mgr = AgentMgr()
return AgentMgr
