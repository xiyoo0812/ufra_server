--login_dao.lua
local log_err       = logger.err
local log_debug     = logger.debug
local tinsert       = table.insert
local sformat       = string.format
local guid_encode   = codec.guid_encode
local SUCCESS       = quanta.enum("KernCode", "SUCCESS")
local mongo_mgr     = quanta.get("mongo_mgr")

local USER_ACCOUNTS = "user_accounts"
local BLACK_LIST    = "black_list"
local SERVER_LIST   = "server_list"
local NOTICE_LIST   = "notice_list"

local LoginDao = singleton()
function LoginDao:__init()
end

--获取账号
function LoginDao:get_account(account)
    local code, udata = mongo_mgr:find_one(account, USER_ACCOUNTS,{ account = account } )
    if code ~= SUCCESS then
        log_err("[LoginDao][get_account] account: {} find failed! code: {}, res: {}", account, code, udata)
        return false
    end
    return udata
end

--通过open_id获取账号
function LoginDao:get_acc_open_id(open_id)
    local code, udata = mongo_mgr:find_one(open_id, USER_ACCOUNTS,{ open_id = open_id } )
    log_err("[LoginDao][get_acc_open_id] open_id: {} code:{} udata: {}", open_id, code, udata)
    if code ~= SUCCESS then
        log_err("[LoginDao][get_acc_open_id] open_id: {} find failed! code: {}, res: {}", open_id, code, udata)
        return false
    end
    return udata
end

--创建账号
function LoginDao:create_account(reg_info)
    local pdata = {
        open_id     = guid_encode(),
        login_type  = reg_info.login_type,
        account     = reg_info.account,
        password    = reg_info.password,
        server_id   = reg_info.server_id,
        region      = reg_info.region,
        language    = reg_info.language,
        device_id       = reg_info.device_id,
        register_time   = quanta.now,
        last_login_time = quanta.now,
        last_login_ip   = reg_info.last_login_ip,
        bind_active_code= '',
    }

    local code, udata = mongo_mgr:insert(pdata.account, USER_ACCOUNTS, pdata)
    if code ~= SUCCESS then
        log_err("[LoginDao][create_account] reg_info.account: {} create failed! code: {}, res: {}", reg_info.account, code, udata)
        return false
    end
    return pdata
end

--更新token
function LoginDao:update_login(account, token, ip)
    local pdata = {
        ["$set"] = {
            ["login_token"] = token,
            ["last_login_time"] = quanta.now,
            ["last_login_ip"] = ip }
        }
    local code, udata = mongo_mgr:update(pdata.account, USER_ACCOUNTS, pdata, {["account"] = account})
    if code ~= SUCCESS then
        log_err("[LoginDao][update_login] account: {} update_login failed! code: {}, res: {}", account, code, udata)
        return false
    end
    return true
end


--获取服务器列表
function LoginDao:get_servers()
    local code, udata = mongo_mgr:find(0, SERVER_LIST, { ["enable"]  = true})
    if code ~= SUCCESS then
        log_debug("[LoginDao][get_servers] document:server_list code:{}", code)
        return {}, code
    end
    local zone = {
        ZoneName = "",
        ZoneID = 0,
        ChannelID = "",
        ExtraInfo = "",
    }
    local server_list = {}
    --分类整理
    for _, server in pairs(udata) do
        local info = {
            ServerId = server.srv_id,
            ServerName = server.name,
            ServerEntry = server.address,
            ExtraInfo = server.custom,
            ServerStatus = server.state
        }
       tinsert(server_list, info)
    end
    zone.Servers = server_list

    local zones = {}
    tinsert(zones, zone)
    local servers = {
        result = 0,
    }
    servers.zones = zones
    return servers
end

--获取公告列表
function LoginDao:get_notices()
    local now_s = quanta.now
    local selector = {
        ["$and"] = {
            { ["end_time"]  = {["$gte"] = now_s}},
            { ["start_time"]  = {["$lt"] = now_s}}
        }
    }

    local code, udata = mongo_mgr:find(0, NOTICE_LIST, selector)
    if code ~= SUCCESS then
        log_debug("[LoginDao][get_notices] document:notice_list code:{}", code)
        return false, code
    end

    -- log_debug("[LoginDao][get_notices] udata:{}", udata)
    local server_notices = {}
    --分类整理
    for _, data in pairs(udata) do
        local index = sformat("%s_%s", data.server_id, data.type)
            local notice = {
                BID =  data.notice_id,
                Language =  "",
                Scene =  data.type,
                Title =  data.title,
                Content =  data.content,
                TargetURL =  data.target_url,
                ImageURL =  data.img_url,
                ButtonText =  "",
                StartTime =  data.start_time,
                ExpireTime =  data.end_time
            }
        server_notices[index] = notice
    end
    return server_notices
end

function LoginDao:check_black_info(open_id, ip)
    local black = self:check_black(open_id)
    if black then
        return black
    end
    if ip == nil or ip == '' then
        return
    end
    black = self:check_black(ip)
    return black
end

function LoginDao:check_black(open_id)
    if open_id == nil then
        return
    end

    local now_s = quanta.now
    local open_id_sel = {
        ["$and"] = {
            { ["end_time"]  = {["$gte"] = now_s}},
            { ["start_time"]  = {["$lt"] = now_s}},
            { ["open_id"]  = open_id}
        }
    }

    local code, udata = mongo_mgr:find_one(open_id, BLACK_LIST, open_id_sel )
    if code ~= SUCCESS then
        log_err("[LoginDao][check_black] open_id: {} find failed! code: {}, res: {}", open_id, code, udata)
        return
    end
    return udata
end

quanta.login_dao = LoginDao()

return LoginDao
