--gm_mgr.lua
local HttpServer        = import("network/http_server.lua")

local log_debug         = logger.debug
local sformat           = string.format
local ssplit            = qstring.split
local new_guid          = codec.guid_new
local guid_encode       = codec.guid_encode
local json_encode       = json.encode
local update_mgr        = quanta.get("update_mgr")
local login_dao         = quanta.get("login_dao")

local Login_Mgr = singleton()
local prop = property(Login_Mgr)
prop:reader("http_server", nil)
prop:reader("servers",{})
prop:reader("notices",{})
--token存活时间
local TOKEN_TIME = 5 * 60
--登录公告类型
local NOTICE_TYPE_LOGIN = 6

function Login_Mgr:__init()
    --创建HTTP服务器
    local server = HttpServer(environ.get("QUANTA_LOGIN_HTTP"))
    self.http_server = server
    self:register()

    --定时器
    update_mgr:attach_minute(self)
    self:on_minute()
end

--定时更新
function Login_Mgr:on_minute()
    --查询服务器列表
    self.servers = login_dao:get_servers()
    --查询公告列表
    self.notices = login_dao:get_notices()
end

--外部注册post请求
function Login_Mgr:register_post(url, handler, target)
    self.http_server:register_post(url, handler, target)
end

--外部注册get请求
function Login_Mgr:register_get(url, handler, target)
    self.http_server:register_get(url, handler, target)
end

--取消注册请求
function Login_Mgr:unregister_url(url)
    self.http_server:unregister(url)
end

function Login_Mgr:register()
    --前端
    self:register_get("/api/server_list", "on_server_list", self)
    self:register_post("/api/account_login", "on_account_login", self)
    self:register_post("/api/notice_list", "on_notice_list", self)

    --后端 账号验证
    self:register_post("/verify_user", "on_verify_user", self)
    self:register_post("/access_check", "on_access_check", self)
end

function Login_Mgr:unregister()
    self:unregister_url("/api/on_server_list")
    self:unregister_url("/api/account_login")
    self:unregister_url("/api/notice_list")
    self:unregister_url("/verify/verify_user")
    self:unregister_url("/verify/access_check")
end

--http 回调
----------------------------------------------------------------------
--服务器列表
function Login_Mgr:on_server_list(url, body, params)
    log_debug("[Login_Mgr][on_server_list] body:{}, params:{}", body, params)
    return self.servers
end

--账号登录
function Login_Mgr:on_account_login(url, body, params, headers)
    log_debug("[Login_Mgr][on_account_login]body:{}, params:{}, headers:{}", body, params, headers)
    local result = {
        code = 0,
        msg = "success",
    }
    if not body then
        result.code = -1
        result.msg = "body is nil"
        return result
    end

    local acc, pwd, type, server_id, device_id = body.account, body.pwd, body.type, body.server_id, body.device_id
    local account = login_dao:get_account(acc)
    if not account then
        --注册账号
        local reg_info = {
            login_type = type,
            account = acc,
            password = pwd,
            server_id = server_id,
            device_id = device_id
        }
        account = login_dao:create_account(reg_info)
    end

    --验证用户名密码
    if account.password ~= pwd then
        result.code = 10022
        result.msg = 'pwssword error'
        return result
    end

    --黑名单
    local black_info = login_dao:check_black(account.open_id, account.ip)
    log_debug("[Login_Mgr][on_account_login] black_info:{}", black_info)
    if black_info then
        result.code = 10005
        result.msg = black_info.reson
        return result
    end

    --生成token
    local login_token = sformat("%s_%s", account.open_id, guid_encode())
    if not login_dao:update_login(acc, login_token) then
        result.code = 10022
        result.msg = "apply token failed"
        return result
    end

    result.data = {
        OpenID      = account.open_id,
        Token       = login_token,
        Type        = account.login_type,
        DeviceID    = device_id
    }
    log_debug("[Login_Mgr][on_account_login] account:{} token:{}", acc, login_token)
    return result;
end

--公告列表
function Login_Mgr:on_notice_list(url, body, params)
    log_debug("[Login_Mgr][on_notice_list] body:{}, params:{}", body, params)
    local server_id, scene = body.ServerId, body.Scene
    local index
    if not scene and scene ~= '' then
        index = sformat("%s_%s", server_id, scene)
    else
        index = sformat("%s_%s", server_id, NOTICE_TYPE_LOGIN)
    end

    local notice = self.notices[index]
    local result = {
        result = 0,
        bulletin = {
            BulletinItems = {notice}
        }
    }
    return json_encode(result, true)
end

--验证登录TOKEN
function Login_Mgr:on_verify_user(url, body, params)
    local log_id = new_guid()
    log_debug("[Login_Mgr][verify_user] body:{}, params:{} log_id:{}", body, params, log_id)
    local _, _, _, _, _, _, token =
            params.language, params.ip, params.region, params.pac_code, params.server_id, params.ts, params.token

    local result = {
        code = 0,
        message = "success",
        log_id = log_id
    }
    --拆分token
    local token_info = ssplit(token, "_")
    if #token_info ~= 2 then
        result.code = -1003
        result.message = "accessToken not found"
        return result
    end
    log_debug("[Login_Mgr][on_verify_user] token_info:{} account:{}", token_info, token_info[1])
    local account = login_dao:get_acc_open_id(token_info[1])
    if not account then
        result.code = -1003
        result.message = "account not found"
        return result
    end
    if account.login_token ~= token then
        result.code = -1003
        result.message = "accessToken error"
        return result
    end
    if account.last_login_time + TOKEN_TIME < quanta.now then
        result.code = -1003
        result.message = "accessToken expire"
        return result
    end

    result.data = {
        sdk_open_id = account.open_id,
        device_id   = account.device_id,
        channel_id  = account.channel_id,
        nickname    = account.nickname,
        avatar_url  = "",
        login_way   = account.login_type
    }
    log_debug("[Login_Mgr][on_verify_user] result:{}", result)
    return result;
end

--准入限制
function Login_Mgr:on_access_check(url, body, params)
    log_debug("[Login_Mgr][access_check] body:{}, params:{}", body, params)
    local result = {
        code = 0,
        message = "success",
        log_id = "log_id"
    }
    return result;
end

quanta.login_mgr = Login_Mgr()

return Login_Mgr