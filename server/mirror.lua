--quanta
import("kernel.lua")

quanta.startup(function()
    --创建转发服务器
    local NetServer = import("network/net_server.lua")
    local ip, port = environ.addr("QUANTA_MIRROR_NET")
    local client_mgr = NetServer("mirror")
    client_mgr:setup(ip, port, true)
    quanta.client_mgr = client_mgr
    --初始化mirror
    import("mirror/init.lua")
end)
