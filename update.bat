@echo off

set RootDir=%~dp0

:: pull
cd ..\Excel_Mobile
git reset --hard HEAD
git pull

chcp 65001

:: 解析xlsm文件为lua
.\tool\quanta.exe --entry=convertor --input=./excel --output=./server --typline=3 --staline=6

:: copy
xcopy .\server\*.lua %RootDir%\server\config\  /y /s

pause
