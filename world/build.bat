@echo off

rmdir /Q /S eufaula
md eufaula\quanta

xcopy ..\server  .\eufaula\server\  /y /s
xcopy ..\bin\proto  .\eufaula\proto\  /y /s
xcopy ..\bin\eufaula  .\eufaula\eufaula\  /y /s
xcopy ..\quanta\script  .\eufaula\quanta\script\  /y /s
xcopy ..\quanta\server  .\eufaula\quanta\server\  /y /s

cd eufaula

7z a -tzip eufaula.zip eufaula proto quanta server

xcopy .\eufaula.zip  ..\bin  /y /s

cd ..

rmdir /Q /S eufaula

